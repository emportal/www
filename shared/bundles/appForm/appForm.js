function AppFormConstructor(options) {
	
	var domElement = document.getElementsByClassName('appFormPopup')[0];
	var optionFields = ['Company', 'Address', 'Doctor'];
	var settings = {
		'usedProtocol': '',
		'domain': (typeof options.domain != 'undefined') ? options.domain : 'local.emportal.ru',
		'companyUrl': (typeof options.companyUrl != 'undefined') ? options.companyUrl : 'medi',
		'addressUrl': (typeof options.addressUrl != 'undefined') ? options.addressUrl : '',
		'doctorIndex': 0,
		'doctorPhotoUrl': 'uploads/doctor/000004459/photo.jpg?z=1901509864',
		'doctorName': 'Науменко Николай Николаевич',
		'doctorSpeciality': 'Отоларинголог (ЛОР)',
		'selectedSpeciality': 'all',
		'selectedDate': '', //rf !! надо автоматом дату выбирать
		'data': {}, //массив с инфой о врачах, адресах и пр.
		'defOptName': 'Все специальности',
		'debug': (typeof options.domain != 'undefined') ? false : true,
		'appType': (typeof options.appType != 'undefined') ? options.appType : '',
		'callbackSuccessful': (typeof options.callbackSuccessful != 'undefined') ? options.callbackSuccessful : '',
		'pageUrl': window.location.href
	};
	
	if (settings.debug) {
		console.log('appForm settings: ');
		console.log(settings);
	}
	
	// <!-- инициализация виджета
	(function init(obj) {
		settings.usedProtocol = (document.location.protocol == "https:" ? "https://" : "http://");
		getAddresses(obj); //получаем докторов по компании/адресу
	})(this)
	// -->
	
	if (typeof options['enabledFields'] != 'undefined') {
		
		for(k in optionFields) {
			
			var display = 'none';
			if ( contains(options['enabledFields'], optionFields[k]) ) {
				display = 'table-row';
			}
			var fieldElements = document.getElementsByClassName('appForm' + optionFields[k]);
			var amount = fieldElements.length;
			for(var i = 0; i < amount; i++) {				
				fieldElements[i].style.display = display;
			}
		}
	}
	
	function contains(a, obj) {
		
		var i = a.length;
		while (i--) {
			if (a[i] === obj) return true;
		}
		return false;
	}
	
	function getAddresses(obj) {
		
		var params = {
			'companyLinkUrl': settings['companyUrl']
		};
		
		ajaxCall('getAddressList', params, function(data) {			
			
			if (typeof settings.data[settings.companyUrl] == 'undefined') {
				settings.data[settings.companyUrl] = {};
			}			
			var addressList = settings.data[ settings.companyUrl ];
			if (typeof addressList == 'undefined' || !addressList.length) {
				
				var newAddressList = {};
				addressList = {};
				for(k in data.addressList) {
					newAddressList[ data.addressList[k]['url'] ] = {
						'name': data.addressList[k]['name']
					};					
				}
				settings.data[ settings.companyUrl ] = newAddressList;
			} else {
				return false;
			}
			obj.redrawBlock('addresses');
			obj.redrawBlock('doctorsInAddress');
		});
	}
	
	function getDoctors(obj) {
		
		if (typeof settings.data[ settings.companyUrl ] == 'undefined') {
			settings.data[settings.companyUrl] = {};
		}
		if (typeof settings.data[ settings.companyUrl ][ settings.addressUrl ]) {
			
			var currentAddress = settings.data[ settings.companyUrl ][ settings.addressUrl ];
			if (settings.debug) {
				console.log('addressUrl IS SET: ');
				console.log(settings.addressUrl);
			}
			if (typeof currentAddress == 'undefined') {
				currentAddress = {};
			}
			
			var params = {};
			params['companyLinkUrl'] = settings['companyUrl'];
			params['addressLinkUrl'] = settings['addressUrl'];			
			ajaxCall('getDoctorList', params, function(data) {
				
				currentAddress['doctors'] = data.doctorList;
				obj.redrawBlock('doctors');
			});
		}
	}
	
	function getSchedule(obj) {
		
		var doctor = settings.data[ settings.companyUrl ][ settings.addressUrl ]['doctors'][ settings.doctorIndex ];
		if (!settings.selectedDate) {
			settings.selectedDate = getAppFormDate().value;
		}
		
		var params = {
			'date': settings.selectedDate,
			'type': 'forward',
			'new': 1,
			'linkUrl': settings.addressUrl,
			'doctor': doctor.link,
			'idPat': '',
			'JSON': 1,
		};	
		
		var current_doctor = getActiveDoctor();
		if (typeof current_doctor['schedules'] == 'undefined') {current_doctor['schedules'] = {};}		
		if (typeof current_doctor['schedules'][settings.selectedDate] == 'undefined') {
			
			//если запрашиваем расписание первый раз
			var current_doctors_schedule = current_doctor['schedules'][settings.selectedDate] = {};
			
			//надо отключить блок выбора времени
			var appFormTimeInput = document.getElementsByClassName('appFormTimeInput')[0];
			appFormTimeInput.className += ' isBeingLoaded';
			
			ajaxCall('getSchedule', params, function(data) {
				
				if (data.msg == 'Рабочий' || data.msg == 'Круглосуточно') {
					
					var i = 0;
					for(k in data.time) {
						
						if (data.time[k]['status'] == 'allow') {
							
							current_doctors_schedule[i] = data.time[k];
							i++;
						}
					}
				}
				//подставим значение доступного времени в соотв. селектбокс
				obj.redrawBlock('selectedTime');
				appFormTimeInput.className = appFormTimeInput.className.replace(' isBeingLoaded', '');
			});
		} else {
			
			//если ранее запрашивали расписания, берем их из кеша
			obj.redrawBlock('selectedTime');
		}
	}
	
	function showAppFormMessage(className, messageText, hideOtherFields, generalClassName) {
		
		// <!-- default values
		if (typeof generalClassName == 'undefined') generalClassName = 'appFormSystemMessage';
		if (typeof hideOtherFields == 'undefined') hideOtherFields = true;
		// -->		
		var messageBlocks = domElement.getElementsByClassName(generalClassName);	
		var messageBlockToShow = domElement.getElementsByClassName(className)[0];
		
		if (hideOtherFields == true) {
			for (k in messageBlocks) {
				if (typeof messageBlocks[k].nodeName != 'undefined' && typeof messageBlocks[k] != 'undefined') {
					messageBlocks[k].style.display = 'none';
				}
			}
		}
		if (typeof messageBlockToShow != 'undefined' && !!messageBlockToShow) {
			
			messageBlockToShow.style.display = 'block';
			if (typeof messageText != 'undefined') messageBlockToShow.innerHTML = messageText;
		}
		return true;
	}
	
	function showErrors(errors) {
				
		for (k in errors) {
			if (errors.hasOwnProperty(k)) {
				var className = errors[k]['type'] + 'Error';
				showAppFormMessage(className, errors[k]['text'], true, 'fieldError');
			}
		}
		return true;
	}
	
	function disableInputs(type) {
		var selects = domElement.getElementsByTagName('select');
		var inputs = domElement.getElementsByTagName('input');
		
		for (k in selects) {
			if (selects[k].hasOwnProperty('nodeName') && typeof selects[k] != 'undefined') {
				selects[k].disabled = type;
			}
		}
		
		for (k in inputs) {
			if (inputs[k].hasOwnProperty('nodeName') && typeof inputs[k] != 'undefined') {
				inputs[k].disabled = type;
			}
		}
	}
	
	function getActiveDoctor() {
		var currentDoctor = settings.data[ settings.companyUrl ][ settings.addressUrl ][ 'doctors' ][ settings.doctorIndex ];
		return currentDoctor;
	}
	
	function getValueOfSelect(selectClassName) {
		var elem = domElement.getElementsByClassName(selectClassName)[0];
		var i = elem.options.selectedIndex;
		if (typeof elem.options[i] === 'undefined') return false;
		value = elem.options[i].value;
		return value;
	}
	
	function getAppFormDate(options) {
		
		var dayNamesRus = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
		var monthNamesRus = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
		
		var options = (typeof options != 'undefined') ? options : {};
		var cursorDate = (typeof options.date != 'undefined') ? options.date : new Date();
		
		var month = cursorDate.getMonth() + 1;
		if (month.toString().length == 1) month = '0' + month;
		var day = cursorDate.getDate();
		if (day.toString().length == 1) day = '0' + day;				
		var dayOfWeek = cursorDate.getDay();
		
		var value = day + '.' + month + '.' + cursorDate.getFullYear();
		var valueVisible = dayNamesRus[ dayOfWeek ] + ', ' + day + ' ' + monthNamesRus[ cursorDate.getMonth() ];
		
		var output = {
			'value': value,
			'valueVisible': valueVisible
		};
		return output;
	}
	
	function ajaxCall(action, params, callback) {
		
		var nocache = new Date().getTime(),
			xmlhttp = new XMLHttpRequest(),
			ajax_query = '',		
			src_dir = settings.usedProtocol + settings.domain + "/" + "ajax/appForm",
			paramsStr = '';
		
		for (k in params) {
			paramsStr += '&' + k + '=' + params[k];
		}
		
		ajax_query = "action=" + action + paramsStr + "&nocache=" + nocache;
			
		xmlhttp.open("GET", src_dir + "?" + ajax_query, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); //we have to use proper encoding	
		xmlhttp.send();
		
		xmlhttp.onreadystatechange = function() {
			
			if (xmlhttp.readyState == 4) {
				
				var response = xmlhttp.responseText;
				var data = JSON.parse(response);
				callback(data);
			}
		}
	}
	
	
	this.toggleVisibility = function() {
		var display = domElement.style.display;
		domElement.style.display = (display == 'block') ? 'none' : 'block';
	}
	
	this.getSettings = function() {
		return settings;
	}
	
	this.getActiveDoctorData = function() {
		return getActiveDoctor();
	}
	
	this.checkFunc = function(message) {
		//getAppFormDate();
		//console.log( getActiveDoctorData() );
	}
	
	this.getElem = function(className) {
		var elem = domElement.getElementsByClassName(className)[0];
		return elem;
	}
	
	this.redrawBlock = function(type) {
	
		if (type == 'date') {
		
			var appFormDateInput = domElement.getElementsByClassName('appFormDateInput')[0];
			var optionsHTML = '';
			var currentDate = new Date();
			var currentTime = currentDate.getTime();
			var milliSecPerDay = 60*60*24*1000;			
			
			for(var i = 0; i <30; i++) {
			
				var cursorTime = currentTime + milliSecPerDay*i;
				var cursorDate = new Date(cursorTime);				
				var formattedDate = getAppFormDate({
					'type': 'formatted',
					'date': cursorDate
				});
				
				optionsHTML += '<option value="' + formattedDate.value + '">' + formattedDate.valueVisible + '</option>';
			}
			appFormDateInput.innerHTML = optionsHTML;
			return true;
		}
		
		if (type == 'addresses') {
			var addressInput = domElement.getElementsByClassName('appFormAddressInput')[0];
			var addressList = settings.data[ settings.companyUrl ];
			var currentAddress = settings.addressUrl;
			var optionsHTML = '';
			for (k in addressList) {
				
				if (addressList.hasOwnProperty(k) && k != 'undefined') {
					
					if (addressList.hasOwnProperty(k) && typeof addressList[k]['name'] == 'string') {
						var optionSelected = (currentAddress == k) ? ' selected' : '';
						optionsHTML += '<option' + optionSelected + ' value="' + k + '">' + addressList[k]['name'] + '</option>';
					}
				}
			}
			addressInput.innerHTML = optionsHTML;
		}
		
		if (type == 'doctorsInAddress') {
			var selectedAddressUrl = getValueOfSelect('appFormAddressInput');
			if (settings.debug) {
				console.log('getting doctors in address: ' + selectedAddressUrl);
				console.log('selected address in address: ');
				console.log(settings);
			}
			settings.addressUrl = selectedAddressUrl;
			getDoctors(this);
			//this.redrawBlock('specialities'); //?
		}
		
		if (type == 'doctors') {
			var doctorInput = domElement.getElementsByClassName('appFormDoctorInput')[0];
			var elem = domElement.getElementsByClassName('appFormSpecialityInput')[0];
			var selectedSpeciality = getValueOfSelect('appFormSpecialityInput');
			settings.selectedSpeciality = selectedSpeciality;
			
			var currentAddress = settings.data[ settings.companyUrl ][ settings.addressUrl ];
			var doctorList = currentAddress['doctors'];
			var noSpecialtiesInAddress = (typeof currentAddress['availableSpecialities'] === 'undefined') ? true : false;
			var availableSpecialities = {};
			var optionsHTML = '';
			for (k in doctorList) {
				
				if (doctorList.hasOwnProperty(k)) {
					
					var noInsert = (selectedSpeciality == 'all') ? 0 : 1;
					doctorSpecialities = doctorList[k]['specialities'];
						
					for (key in doctorSpecialities) {
						
						var specialityUrl = doctorSpecialities[key]['url'];
						if (selectedSpeciality == specialityUrl)
							noInsert = 0;
						if (noSpecialtiesInAddress) {
							
							availableSpecialities[ specialityUrl ] = {
								'name': doctorSpecialities[key]['name']
							};
						}
					}
					if (!noInsert) optionsHTML += '<option value="' + k + '">' + doctorList[k]['name'] + '</option>';
				}
			}
			
			if (noSpecialtiesInAddress) {
				currentAddress['availableSpecialities'] = availableSpecialities;
			}
			
			this.redrawBlock('specialities');
			
			if (optionsHTML == '') { //rm
				
				this.redrawBlock('doctors');
			} else {
			
			doctorInput.innerHTML = optionsHTML;
			this.redrawBlock('doctorInfo');
			}
		}
		
		if (type == 'doctorInfo') {
			var doctorInfo = domElement.getElementsByClassName('doctorInfo')[0];
			var newInnerHTML = '';
			var value = getValueOfSelect('appFormDoctorInput');
			var doctors = settings.data[ settings.companyUrl ][ settings.addressUrl ]['doctors'];
			var doctor = doctors[value];
			
			if (typeof doctor['specialities'] != 'undefined' && doctor['specialities'].length > 0) {
				var availableSpecialities = [];
				for (k in doctor['specialities']) {
					if (doctor['specialities'].hasOwnProperty(k) && k != 'undefined') {
						availableSpecialities.push(doctor['specialities'][k]['name']);
					}
				}
				var specialitiesHTML = availableSpecialities.join(' / ');
				newInnerHTML += '<p>' + specialitiesHTML + '</p>';
			}
			if (typeof doctor['experience'] != 'undefined' && !!doctor['experience']) {
				newInnerHTML += '<p>Стаж ' + doctor['experience'] + ' (лет)</p>';
			}
			if (typeof doctor['price'] != 'undefined' && !!doctor['price']) {
				newInnerHTML += '<p>Стоимость приема ' + doctor['price'] + ' (руб)</p>';
			}
			
			newInnerHTML = '<div>' + newInnerHTML + '</div>';
			//newInnerHTML = '<img src="' + settings.usedProtocol + 'emportal.ru/uploads/doctor/' + doctor['link'] + '/photo.jpg?z=1535881050">' + newInnerHTML;
			newInnerHTML = '<img src="' + settings.usedProtocol + 'emportal.ru/site/image?src=/uploads/doctor/' + doctor['link'] + '/photo.jpg">' + newInnerHTML;
			
			doctorInfo.innerHTML = newInnerHTML;
			var img = doctorInfo.getElementsByTagName('img')[0];
			img.onerror = function() {
				//console.log('cannot load image! so sorry!');
				img.src = settings.usedProtocol + 'emportal.ru/assets/766a70f4/images/staff-m.jpg';
			}
			settings.doctorIndex = value;
			getSchedule(this);
		}
		
		if (type == 'selectedDate') {
			var doctor = getActiveDoctor();			
			var value = getValueOfSelect('appFormDateInput');
			settings.selectedDate = value;			
			getSchedule(this);
			//console.log('selected date: ' + value);
		}
		
		if (type == 'selectedTime') {
			var doctor = getActiveDoctor();
			var todaysSchedules = doctor.schedules[ settings.selectedDate ];
			var newInnerHTML = '';
			var appFormTimeInput = domElement.getElementsByClassName('appFormTimeInput')[0];
			if (typeof todaysSchedules == 'undefined') {
				//console.log('No possible schedules today');
				return false;
			}
			for(k in todaysSchedules) {
				newInnerHTML += '<option value="' + todaysSchedules[k]['text'] + '">' + todaysSchedules[k]['text'] + '</option>';
			}
			if (!newInnerHTML) {
				
				newInnerHTML = '<option>отсутствует<option>';
				appFormTimeInput.disabled = true;
				//console.log('нет свободного времени приема');
			} else {
				appFormTimeInput.disabled = false;
			}
			appFormTimeInput.innerHTML = newInnerHTML;
		}
		
		if (type == 'specialities') {
			var availableSpecialities = settings.data[ settings.companyUrl ][ settings.addressUrl ]['availableSpecialities'];
			var appFormSpecialityInput = domElement.getElementsByClassName('appFormSpecialityInput')[0];
			var defaultOptionName = 'Все специальности';
			var defaultOption = '<option value="all">' + decodeURIComponent(encodeURIComponent(settings.defOptName)) + '</option>';
			var newInnerHTML = defaultOption;
			//console.log('availableSpecialities');
			//console.log(availableSpecialities);
			for (k in availableSpecialities) {
				if (availableSpecialities.hasOwnProperty(k) && k != 'undefined') {
					var selectedAttr = '';
					if (settings.selectedSpeciality == k) {
						selectedAttr = ' SELECTED';
						//console.log('в этом адресе есть выбранная специальность');
					}
						newInnerHTML += '<option value="' + k + '"' + selectedAttr + '>' + availableSpecialities[k]['name'] + '</option>';
				}
			}
			
			appFormSpecialityInput.innerHTML = newInnerHTML;
		}
		
		if (type == 'selectedSpeciality') {
			
			//var appFormSpecialityInput = domElement.getElementsByClassName('appFormSpecialityInput')[0];
			var value = getValueOfSelect('appFormSpecialityInput');
			getDoctors(this);
		}
	}
	
	this.submit = function(type) {
		
		var fields = {},
			errors = [],
			params = '';
		//собираем данные формы
		fields['name'] = domElement.getElementsByClassName('appFormName')[0].value;
		fields['phone'] = domElement.getElementsByClassName('appFormPhone')[0].value;
		fields['company'] = settings.companyUrl;
		fields['address'] = settings.addressUrl;
		fields['doctor'] = getActiveDoctor().doctorUrl;
		fields['date'] = getValueOfSelect('appFormDateInput');
		fields['time'] = getValueOfSelect('appFormTimeInput');
		fields['source'] = encodeURIComponent(settings.pageUrl);
		if (settings.appType.length)
			fields['appType'] = settings.appType;
		
		//валидируем имя
		if (fields['name'].length <= 2) {
			errors.push({
				'type': 'appFormName',
				'text': 'имя должно быть более 2 символов'
			});
		}		
		//валидируем телефон
		if (parseInt(fields['phone']).toString().length <= 6) {
			errors.push({
				'type': 'appFormPhone',
				'text': 'некорректный телефон'
			});
		}		
		//валидируем день и время приема
		if (!fields['date'].length) {
			errors.push({
				'type': 'appFormDate',
				'text': 'не указан день приема'
			});
		}
		if (!fields['time'].length) {
			errors.push({
				'type': 'appFormTime',
				'text': 'не указано время приема'
			});
		}
		// <!-- сначала спрячем предыдущие ошибки (если они показываются)
		showAppFormMessage('', '', false, 'fieldError');
		// -->
		if (errors.length > 0) {		
			showErrors(errors);
			return false;
		}
		
		//в этот момент надо скрывать кнопку "Далее", показывать индикацию загрузки формы		
		showAppFormMessage('appFormLoading');
		disableInputs(true);
		
		ajaxCall('addAppointment', fields, function(data) {
			
			if (settings.debug) {
				console.log('ajax app fields');
				console.log(fields);
			}
			
			if (data.result) {				
				showAppFormMessage('appFormSuccess');
				if (settings.appType == 'phone_emportal')
					document.location.href = 'view';
			} else {
				showAppFormMessage('appFormError');
				disableInputs(false);
				setTimeout(function() {showAppFormMessage('appFormSubmit');}, 5000);
			}
		});
	}
}

//window.onload = function(){
	
	var enabledFields = [/*'Company',*/ 'Address', 'Doctor'];
	var options = {
		'enabledFields': enabledFields,
		'companyUrl': window.empAppFormOptions.companyUrl,
		'addressUrl': window.empAppFormOptions.addressUrl,
		'domain': window.empAppFormOptions.domain,
		'appType': window.empAppFormOptions.appType,
		//'callbackSuccessful': window.empAppFormOptions.callbackSuccessful,
	};
	//console.log('onload event options:');
	//console.log(options);
	var appForm = new AppFormConstructor(options);
	var link = document.getElementsByClassName('appFormLink')[0];
	
	appForm.redrawBlock('date');
	
	if (link) {
		link.onclick = function() {
			appForm.toggleVisibility();
			return false;
		}
	}
	
	var appFormAddressInput = appForm.getElem('appFormAddressInput');
	appFormAddressInput.onchange = function() {
		appForm.redrawBlock('doctorsInAddress');
	}
	
	var appFormDoctorInput = appForm.getElem('appFormDoctorInput');
	appFormDoctorInput.onchange = function() {
		appForm.redrawBlock('doctorInfo');
	}
	
	var appFormDateInput = appForm.getElem('appFormDateInput');
	appFormDateInput.onchange = function() {
		appForm.redrawBlock('selectedDate');
	}
	
	var appFormSpecialityInput = appForm.getElem('appFormSpecialityInput');
	appFormSpecialityInput.onclick = function() {
		appForm.redrawBlock('doctors');
	}
	
	var appFormSubmit = appForm.getElem('appFormSubmit');
	appFormSubmit.onclick = function() {
		appForm.submit();
	}
	
	var appFormInfo = appForm.getElem('appFormInfo');
	appFormInfo.onclick = function() {
		var appFormInfoText = appForm.getElem('appFormInfoText');
		appFormInfoText.style.display = (appFormInfoText.style.display == 'block') ? 'none' : 'block';
		window.setTimeout(function() {
			appFormInfoText.style.display = 'none';
		}, 5000);
		return false;
	}
	
	window.checkdata = function() {
		console.log( appForm.checkFunc() );
		return appForm.checkFunc();
	}
//}