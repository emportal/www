//depends on jQuery library
//$(function() {
	function EmailBlockConstructor(options) {
		
		var $domElement = $('#' + options.id),
			$closeButtons = $domElement.find('.md-close'),
			$contentBox = $domElement.find('.md-form-container').first(),
			$overlay = $domElement.next(),
			mw = this;
		
		/* email subscriptions */
		
		var $emailBlock = $('#' + options.id),
			$emailSubmit = $emailBlock.find('#emailSubscriptionSubmitButton'),
			$emailInput = $emailBlock.find('#emailSubscriptionInput'),
			$emailInputForm = $emailSubmit.parent(); //$emailBlock ??
		
		if ($emailSubmit.length && $emailInput.length) {
			
			$emailSubmit.click(function() {
				
				var params = {
					'type': $emailInput.attr('data-type'),
					'email': $emailInput.val()
				};
				
				if (typeof $emailInput.attr('data-companyUrl') != 'undefined') {					
					params['companyUrl'] = $emailInput.attr('data-companyUrl');
				}
				if (typeof $emailInput.attr('data-doctorUrl') != 'undefined') {					
					params['doctorUrl'] = $emailInput.attr('data-doctorUrl');
				}
				if (typeof $emailInput.attr('data-userId') != 'undefined') {					
					params['userId'] = $emailInput.attr('data-userId');
				}
				
				$.ajax({
					url: '/ajax/subscribe',
					async: true,
					data: params,
					type: 'GET',
					success: function(data) {
						data = JSON.parse(data);
						if (data['success'] == 'false') {
							alert(data['message']);
						} else {
							var newHTML = 'ОК';
							$emailInputForm.html(newHTML);
							window.setTimeout(function(){document.location.reload(true);}, 1);
						}
					}
				});
            });
		}
	}


var emailBlogSubscribe = function(name, email){
	var vName = $('[name=' + name + ']');
	var vEmail = $('[name=' + email + ']');
	var block = false;
	//var r = /^(\w.[0-9])+@\w+\.\w{2,4}$/i;
	var r = /^[a-zA-Z]+([a-zA-Z0-9_.+-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i;

	if(vName.val() == ''){
		actEmptyElement(vName);
		block = true;
	}
	if(vEmail.val() == '' || !r.test(vEmail.val())){
		actEmptyElement(vEmail);
		block = true;
	}
	if(block) return;


	$.ajax({
		url: '/ajax/subscribe',
		async: true,
		data: {'name':vName.val(), 'email': vEmail.val(), 'type' : 1},
		type: 'GET',
		success: function(data) {
			data = JSON.parse(data);
			if (data['success'] == 'false') {
				alert(data['message']);
			} else {
				$('.md-overlay').show().attr('style', 'opacity: 0.5; visibility: visible;');
				$('.modal-subscribe').fadeIn();
				setTimeout(function(){
					$('.modal-subscribe').fadeOut();
					$('.md-overlay').hide().attr('style', 'opacity: 0; visibility: hidden;');
					vName.val('');
					vEmail.val('');
				}, 3000);
			}
		}
	});

};
var actEmptyElement = function(el){
	el.css('border', '1px solid red');
	setTimeout(function(){el.css('border', '1px solid #dddddd')}, 1000);
};

//});