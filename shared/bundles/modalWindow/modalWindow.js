//depends on jQuery library
//$(function() {
function ModalWindowConstructor(options) {
	
	var $domElement = $('#' + options.id),
		$closeButtons = $domElement.find('.md-close'),
		$contentBox = $domElement.find('.md-form-container').first(),
		$overlay = $domElement.next(),
		mw = this;
		
	//console.log('overlay:');
	//console.log($overlay);
		
	if (typeof options.templateId != 'undefined') {
		
		var $contentSource = $('#' + options.templateId);
		$contentBox.append( $contentSource );
	}
	
	function _close() {
		$domElement.removeClass('md-show');
		//setTimeout(function(){_show();}, 5000);
	}
	
	function _show() {
		$domElement.addClass('md-show');
	}
	
	this.close = function() {_close();}
	this.show = function() {_show();}
	
	$closeButtons.each(function() {		
		$closeButton = $(this);
		$closeButton.click(function(e){
			_close();
		});
	});
	
	$overlay.click(function() {
		mw.close();
	});
}
//});