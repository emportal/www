	var Emias = function() { };
	Emias.prototype.checkReferrals = function() {
		var self = this;
        $.ajax({
            url: '/site/emiasReferrals',
            async: true,
            data: { json:1 },
            type: 'GET',
            dataType: 'json',
			beforeSend: function() { },
            success: function(data) {
                if('return' in data) {
                    var selected_ok = false;
                    var selected_id = '';
                    var selected_number = '';
                    for(obj in data.return) {
                    	if(data.return[obj].id == $.cookie('OMSForm_referralId')) {
                    		selected_ok = true;
                    		selected_id = data.return[obj].id;
                    		selected_number = data.return[obj].number;
                    		$(".unselectReferral").show();
                    	}
                    }
                    if(!selected_ok) {
                        if($.cookie('OMSForm_referralId') !== null) {
                        	self.unselectReferral(true);
                        }
                    } else {
                    	self.selectReferral(selected_id,selected_number,false)
                    }
                    console.log(data);
            		$('.referralsCount').text("У вас "+(data.return.length>0 ? data.return.length : 'нет')+" направлений.");
                } else {
                	$('.referralsCount').text("");
                }
            },
            error: function(XHR,errorText,errorThrown) {  },
            complete: function() {  },
		});
	};
	Emias.prototype.showReferrals = function() {
		var self = this;
        $.ajax({
            url: '/site/emiasReferrals',
            async: true,
            data: {},
            type: 'GET',
			beforeSend: function() {
				$("#emiasContent").addClass('emiasContentLoad');
	            if($("#emiasContent").is(':hidden')) {
	                $("#emiasContent").html('');
		            $("#emiasContent").stop().slideDown('slow');
	            } else {
		            $("#emiasContent").stop().slideUp('slow');
	                $("#emiasContent").html('');
		            $("#emiasContent").slideDown('slow');
	            }
	        },
            success: function(data) {
				if($("#emiasContent").length > 0) {
	                $("#emiasContent").html(data);
				} else {
					$.fancybox({content:data})
				}
            },
            error: function(XHR,errorText,errorThrown) {
            	 $("#emiasContent").hide();
            },
            complete: function() {
				$("#emiasContent").removeClass('emiasContentLoad');
			},
		});
	};
	Emias.prototype.showReceptions = function() {
		var self = this;
        $.ajax({
            url: '/site/emiasReceptions',
            async: true,
            data: {},
            type: 'GET',
			beforeSend: function() {
				$("#emiasContent").addClass('emiasContentLoad');
	            if($("#emiasContent").is(':hidden')) {
	                $("#emiasContent").html('');
		            $("#emiasContent").stop().slideDown('slow');
	            } else {
		            $("#emiasContent").stop().slideUp('slow');
	                $("#emiasContent").html('');
		            $("#emiasContent").slideDown('slow');
	            }
	        },
            success: function(data) {
                $("#emiasContent").html(data);
            },
            error: function(XHR,errorText,errorThrown) {
            	 $("#emiasContent").hide();
            },
            complete: function() {
				$("#emiasContent").removeClass('emiasContentLoad');
			},
		});
	};
	Emias.prototype.cancelAppointment = function(id) {
		var self = this;
        $.ajax({
            url: '/site/emias',
            async: true,
            data: {
            	method: 'cancelAppointment',
            	params: {
            		attr: {
            			omsNumber: self.omsNumber,
            			omsSeries: self.omsSeries,
            			birthDate: emias.birthDate,
            			appointmentId: id,
                	},
                },
                type: 'json',
            },
            type: 'POST',
            dataType: 'json',
			beforeSend: function() { },
            success: function(data) {
                if(data['return'] == 'SUCCESS') {
	               	 $("#emiasContent").hide();
	                 this.error(null,'Запись отменена!');
                } else {
                    this.error(null,'Ошибка');
                }
            },
            error: function(XHR,errorText,errorThrown) {
                alert(errorText);
			},
            complete: function() { },
		});
	};
	Emias.prototype.selectReferral = function(id,number,refresh) {
		try{
			number = typeof number !== 'undefined' ? number : '';
			refresh = typeof refresh !== 'undefined' ? refresh : true;
			$.cookie('OMSForm_referralId',id,{expires:30,path:'/',domain:'.emportal.ru'});
			emias.referralId = id;
			$(".unselectReferral").text('');
			if(number !== '') {
				$(".unselectReferral").text('Выбрано направление № '+number);
			}
			$(".unselectReferral").show();
			$("#emiasContent").stop().slideUp('slow');
			if(refresh) {
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$("#Search_doctorSpecialtyId").select2('val',[])
		        refreshSearch(0);
			}
		} catch(e) {}
	};
	Emias.prototype.unselectReferral = function(refresh) {
		refresh = typeof refresh !== 'undefined' ? refresh : true;
		$.cookie('OMSForm_referralId',null,{expires:30,path:'/',domain:'.emportal.ru'});
		$(".unselectReferral").hide();
        if(refresh) refreshSearch(0);
	};