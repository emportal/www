	$( document ).ready(function() {
		$("#Search_companyActiviteId").select2()
			.on("select2-close", function(e) {
				try {
					$('#'+$(this).attr('id')+'_table').hide();
		        	$('.searchBoxMetroLabel').hide();
		        	$('.searchBoxCompanyActiviteLabel').hide();
	    		} catch(exc) {}
	        })
	        .on("change", function(e) {
		 		$('#select2-drop-mask').click();
	        })
	        .on("select2-loaded", function(e) {
				try {
		        	$('#'+$(this).attr('id')+'_table').find(".specialty").addClass('not-active');
		        	var loadedValues = $(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] li.select2-result").get();
		        	for(var sp in loadedValues) {
			        	if(loadedValues[sp].id.length > 0)
			        		$('#'+$(this).attr('id')+'_table').find("#" + loadedValues[sp].id).removeClass('not-active');
		        	}
	    		} catch(exc) {}
	        })
	        .on("select2-highlight", function(e) {
				try {
		        	$('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
		    		
					var val = e.val;
					if(val !== null && val.length > 0) {
						if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
						for(var v in val) {
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('highlighted');
						}
					}
	    		} catch(exc) {}
	        })
	        .on('select2-open', function(){
				try {
					if(isMobile() || (isIE() && isIE() < 11)) {
						$("div.select2-drop[sender = 's2id_"+$(this).attr('id')+"'] .select2-results").show();
						return;
					} else {
						$("div.select2-drop[sender = 's2id_"+$(this).attr('id')+"'] .select2-results").hide();
					}
		        	$('.searchBoxMetroLabel').show();
		        	$('.searchBoxSpecialtyLabel').show();
			        if(!this.firstOpening) {
			        	this.firstOpening = true;
			        	$(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] input").bind('keyup', function(){
				        	if($(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] .select2-no-results").length > 0) {
				        		$('#'+$(this).attr('id')+'_table').find(".specialty").addClass('not-active');
				        		$('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
				        	}
						});
			        }
					
			        $('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
					$('#'+$(this).attr('id')+'_table').show();
					var val = $(this).val();
					if(val !== null && val.length > 0) {
						if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
						for(var v in val) {
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('highlighted');
						}
					}
	    		} catch(exc) {}
			});
		$("#Search_doctorSpecialtyId").select2()
			.on("select2-close", function(e) {
				try {
					$('#'+$(this).attr('id')+'_table').hide();
		        	$('.searchBoxMetroLabel').hide();
		        	$('.searchBoxSpecialtyLabel').hide();
	    		} catch(exc) {} 	    		
	        })
	        .on("change", function(e) {
		 		$('#select2-drop-mask').click();
	        })
	        .on("select2-loaded", function(e) {
				try {
		        	$('#'+$(this).attr('id')+'_table').find(".specialty").addClass('not-active');
		        	var loadedValues = $(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] li.select2-result").get();
		        	for(var sp in loadedValues) {
			        	if(loadedValues[sp].id.length > 0)
			        		$('#'+$(this).attr('id')+'_table').find("#" + loadedValues[sp].id).removeClass('not-active');
		        	}
	    		} catch(exc) {}
	        })
	        .on("select2-highlight", function(e) {
				try {
		        	$('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
		    		
					var val = e.val;
					if(val !== null && val.length > 0) {
						if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
						for(var v in val) {
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('highlighted');
						}
					}
	    		} catch(exc) {}   		
	        })
	        .on('select2-open', function(){
				try {
					if(isMobile() || (isIE() && isIE() < 11)) {
						$("div.select2-drop[sender = 's2id_"+$(this).attr('id')+"'] .select2-results").show();
						return;
					} else {
						$("div.select2-drop[sender = 's2id_"+$(this).attr('id')+"'] .select2-results").hide();
					}
		        	$('.searchBoxMetroLabel').show();
		        	$('.searchBoxSpecialtyLabel').show();
			        if(!this.firstOpening) {
			        	this.firstOpening = true;
			        	$(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] input").bind('keyup', function(){
				        	if($(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] .select2-no-results").length > 0) {
				        		$('#'+$(this).attr('id')+'_table').find(".specialty").addClass('not-active');
				        		$('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
				        	}
						});
			        }
					
			        $('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
					$('#'+$(this).attr('id')+'_table').show();
					var val = $(this).val();
					if(val !== null && val.length > 0) {
						if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
						for(var v in val) {
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('highlighted');
						}
					}
	    		} catch(exc) {}
			});
        
		$("#Search_metroStationId").select2()
			.on("select2-close", function(e) {
				try {
					$('#'+$(this).attr('id')+'_table').hide();
		        	$('#s2id_Search_metroStationId input').attr('style', '');
		        	$('.searchBoxMetroLabel').hide();
		        	$('.searchBoxSpecialtyLabel').hide();
	    		} catch(exc) {}
	        })
	        .on("change", function(e) {
	        	updateDropDownMetroStation();
	        })
	        .on("select2-loaded", function(e) {
				try {
		        	$('#'+$(this).attr('id')+'_table').find(".specialty").addClass('not-active');
		        	var loadedValues = $(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] li.select2-result").get();
		        	for(var sp in loadedValues) {
			        	if(loadedValues[sp].id.length > 0) {
			        		$('#'+$(this).attr('id')+'_table').find("#" + loadedValues[sp].id).removeClass('not-active');
			        	}
		        	}
	    		} catch(exc) {}
	        })
	        .on("select2-highlight", function(e) {
				try {
		        	$('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
		    		
					var val = e.val;
					if(val !== null && val.length > 0) {
						if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
						for(var v in val) {
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('highlighted');
						}
					}
					var val = $(this).val();
					if(val !== null && val.length > 0) {
						if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
						for(var v in val) {
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('highlighted');
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('with_close_btn');
						}
					}
	    		} catch(exc) {}
	        })
	        .on('select2-open', function(){
				try {
					if(isMobile() || (isIE() && isIE() < 11)) {
						$("div.select2-drop[sender = 's2id_"+$(this).attr('id')+"'] .select2-results").show();
						return;
					} else {
						$("div.select2-drop[sender = 's2id_"+$(this).attr('id')+"'] .select2-results").hide();
					}
		        	$("#s2id_"+$(this).attr('id')+" input").attr('style', 'background: white!important');
		        	$('.searchBoxMetroLabel').show();
		        	$('.searchBoxSpecialtyLabel').show();
			        if(!this.firstOpening) {
			        	this.firstOpening = true;
						$("#s2id_"+$(this).attr('id')+" input").bind('keyup', function(){
				        	if($(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] .select2-no-results").length > 0) {
				        		$('#'+$(this).attr('id')+'_table').find(".specialty").addClass('not-active');
				        		$('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
				        	}
						});
			        }
		        	if($(".select2-drop[sender='s2id_"+$(this).attr('id')+"'] li.select2-result").get().length == 0) $('#'+$(this).attr('id')+'_table').find(".specialty").addClass('not-active');
					
		        	$('#'+$(this).attr('id')+'_table').find(".specialty").removeClass('highlighted');
		        	$('#'+$(this).attr('id')+'_table').show();
					var val = $(this).val();
					if(val !== null && val.length > 0) {
						if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
						for(var v in val) {
							$('#'+$(this).attr('id')+'_table').find("#" + val[v]).addClass('highlighted');
						}
					}
	    		} catch(exc) {}
			});
	});
	
	function select2AddValue(objSelector, value) {
		var selectedItems = $(objSelector).select2("val");
		if(Object.prototype.toString.call( selectedItems ) !== '[object Array]') {
			$(objSelector).select2("val", value, true);
		} else {
			selectedItems.push(value);
			$(objSelector).select2("val", selectedItems, true);
		}
	}
	function select2RemoveValue(objSelector, id) {
		var new_data = [];
		var selectedItems = $(objSelector).select2("val");
		$(objSelector).select2('val', new_data);
		for(var item in selectedItems) {
			if(selectedItems[item] !== id) {
				new_data.push(selectedItems[item]);
			}
		}
		$(objSelector).select2('val', new_data, true);
	}
	function updateDropDownMetroStation() {
		$('#Search_metroStationId_table').find(".specialty").removeClass('highlighted');
		$('#Search_metroStationId_table').find(".specialty").removeClass('with_close_btn');
		var val = $("#Search_metroStationId").val();
		if(val !== null) {
			if(Object.prototype.toString.call( val ) !== '[object Array]') { val = [val]; }
			for(var v in val) {
				if(val[v] != "") {
					$('#Search_metroStationId_table').find("#" + val[v]).addClass('highlighted');
					$('#Search_metroStationId_table').find("#" + val[v]).addClass('with_close_btn');
				}
			}
		}
		var newPlaceHolder = "Метро или район";
		if(Object.prototype.toString.call( val ) == '[object Array]') {
			if(val.length > 1) {
				newPlaceHolder = 'Выбрано районов: '+val.length;
			} else {
				if(val[0] != "") {
					newPlaceHolder = $('#Search_metroStationId_table').find("#" + val[0]).text().trim();
				}
			}
		}

		var isIE = function () {
	      var ua = window.navigator.userAgent;
		  var msie = ua.indexOf('MSIE ');
		  if (msie > 0) {
		    // IE 10 or older => return version number
		    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
		  }

		  var trident = ua.indexOf('Trident/');
		  if (trident > 0) {
		    // IE 11 => return version number
		    var rv = ua.indexOf('rv:');
		    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
		  }

		  var edge = ua.indexOf('Edge/');
		  if (edge > 0) {
		    // Edge (IE 12+) => return version number
		    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
		  }

		  // other browser
		  return false;
		}
	    if (isIE()) {
	    } else {
	    	$("#s2id_Search_metroStationId .select2-input").first().attr('placeholder', newPlaceHolder);
	    }
		try {
			document.createEvent("TouchEvent");
		}
		catch (e) {
			$('#Search_metroStationId').css('visibility','hidden');
		}
	}

	var GetSearchParameters = function(oncomplete) {
		var senddata = { };
		if($("#Search_doctorSpecialtyId").val()) senddata.speciality = $("#Search_doctorSpecialtyId").val().replace("specialty-", "");
		if($("#Search_companyServiceId").val()) senddata.service = $("#Search_companyServiceId").val().replace("service-", "");
		
		if(!oncomplete) oncomplete = function() {
			$('#Search_metroStationId').change();	
			if(!$("#Search_metroStationId").val()) {
				$('#Search_metroStationId').select2("open");
			}								
		};
		var metroSelector = $("#Search_metroStationId");
		var companySelector = $("#Doctor_clinicLinkUrl, #AddressServices_clinicLinkUrl");
		var metroSelectorArr = metroSelector.select2('val');
		var companySelectorArr = companySelector.select2('val');
		if(Object.prototype.toString.call( metroSelectorArr ) !== '[object Array]') metroSelectorArr = [metroSelectorArr];
		if(Object.prototype.toString.call( companySelectorArr ) !== '[object Array]') companySelectorArr = [companySelectorArr];
		var set = {
			url: "/index.php?r=ajax/GetSearchParameters",
			data: senddata,
			method:'get',
			dataType:'json',
			success: function(data, textStatus, XHR) {
				$("#Search_metroStationId option").each(function( index ) {
					if($(this).val().length > 0)
						$(this).replaceWith($('<opt value="'+$(this).val()+'"/>').html($( this ).html()));
				});
				var newMetroSelectorArr = [];
				var Metro = data['Metro'];
				for(var i in Metro) {
					for(var z in metroSelectorArr) {
						if(metroSelectorArr[z] == Metro[i]) newMetroSelectorArr.push(metroSelectorArr[z]);
					}
					var el = $('#Search_metroStationId opt[value="'+Metro[i]+'"]');
					if(el.length > 0) {
						$(el[0]).replaceWith($('<option value="'+el[0].getAttribute('value')+'"/>').html($(el[0]).html()));
					}
				}
				var Districts = data['District'];
				for(var i in Districts) {
					for(var z in metroSelectorArr) {
						if(metroSelectorArr[z] == Districts[i]) newMetroSelectorArr.push(metroSelectorArr[z]);
					}
					var el = $('#Search_metroStationId opt[value="'+Districts[i]+'"]');
					if(el.length > 0) {
						$(el[0]).replaceWith($('<option value="'+el[0].getAttribute('value')+'"/>').html($(el[0]).html()));
					}
				}
				metroSelector.select2('val', newMetroSelectorArr);

				companySelector.find("option").each(function( index ) {
					if($(this).val().length > 0)
						$(this).replaceWith($('<opt value="'+$(this).val()+'"/>').html($( this ).html()));
				});
				var newCompanySelectorArr = [];
				var Companies = data['Companies'];
				for(var i in Companies) {
					for(var z in companySelectorArr) {
						if(companySelectorArr[z] == Companies[i]) newCompanySelectorArr.push(companySelectorArr[z]);
					}
					var el = companySelector.find('opt[value="'+Companies[i]+'"]');
					if(el.length > 0) {
						$(el[0]).replaceWith($('<option value="'+el[0].getAttribute('value')+'"/>').html($(el[0]).html()));
					}
				}
				companySelector.select2('val', newCompanySelectorArr);
			},
			error: function(XHR, textStatus, errorThrown) {
				console.log(XHR);
			},
			complete: function() {
				//metroSelector.prop( "disabled", false );
				oncomplete();
			}
		};
		
		//metroSelector.prop( "disabled", true );
		$.ajax(set);
	}
	
	$( document ).ready(function() {
		var selector = $("#Search_doctorSpecialtyId, #Search_companyServiceId");
		if(selector.length > 0 && selector.first().val().length > 0) {
			GetSearchParameters( function() {
	       		updateDropDownMetroStation();
	        });
		} else {
			setTimeout(function() { updateDropDownMetroStation(); }, 100);
		}
	});
	
	$("#Search_doctorSpecialtyId, #Search_companyServiceId").bind("change", function(e) {
		GetSearchParameters();	
	});
	