$(function() {
$("input[type=text],input[type=password], textarea").not($(".area textarea")).inputBF();
});
/*!
 * jQuery Input Blur-Focus 1.0
 *
 * Copyright 2012, Alexander CSR <www.verstka.pro>
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://verstka.pro/notes/plagin-dlya-jquery-input-blur-focus.html
 */
(function(d){var e={init:function(b){settings={FocusClass:"focus"};b&&d.extend(settings,b);return this.each(function(){var a=d(this),c=a.data();a.attr("autocomplete","off");if("password"==a.attr("type")&&!0==c.visible){var b=void 0==a.attr("class")?"":a.attr("class")+" ",e=a.val();d('<input type="text" value="'+e+'" class="'+b+'clone" />').insertAfter(a);a.hide().addClass(settings.FocusClass);d(".clone").focus(function(){d(this).hide().prev().val("").show().focus()});a.blur(function(){""==a.val()&& a.hide().next().show()})}else a.focus(function(){if(void 0==c.ValueDefault)c.ValueDefault=a.val();(void 0==c.ValueBlured||c.ValueBlured==c.ValueDefault)&&a.val("").addClass(settings.FocusClass)}).blur(function(){var b=a.val();""==b?a.val(c.ValueDefault).removeClass(settings.FocusClass).removeData("ValueBlured"):c.ValueBlured=b})})}};d.fn.inputBF=function(b){if(e[b])return e[b].apply(this,Array.prototype.slice.call(arguments,1));if("object"===typeof b||!b)return e.init.apply(this,arguments)}})(jQuery);