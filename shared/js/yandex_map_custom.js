var myMap;
var myCollection;
var clusterer;
$(document).ready(function() {


    ymaps.ready(init);
    function init()
    {
    	var lastId = "map";
    	var newId = "map";
    	while($("#"+newId).length > 0) {
    		lastId = newId;
    		newId = "map_"+makeid();
    	}
    	$("#"+lastId).attr("id", newId);

        var coord=$('data[data-coord]').data('coord');
        myMap = new ymaps.Map(newId, {
            center: [coord.lat, coord.long],
            zoom: 10
        });
        myMap.controls.add('zoomControl', {top: 40, right: 20});
        myMap.container.fitToViewport();
        //myMap.behaviors.enable('scrollZoom');
        myMap.behaviors.disable('scrollZoom');

        myCollection = [];
        if(!$('data[data-search]').data('search')) {
            myMap.setCenter([coord.lat,coord.long], 15);
        }
        updateClinicPlacemarks(newId);
    }
    ;
});

function updateClinicPlacemarks(newId) {
    //set View Center 
	//
	var form = $( "#"+newId ).closest( "div" );
    //console.log(form);
    //myMap.geoObjects.removeAll();
    var clinics = $('data[data-map]').last().data('map');
    $('data[data-map]').last().attr('data-map', '');
    var i = 0;
    if(clinics !== null)
    $.each(clinics, function(index, value) {
        // console.log(value);
        value.price = value.price || '';
        myCollection[i] = new ymaps.GeoObject({
            geometry: {type: "Point", coordinates: [value.lat, value.long]},
            properties: {
                balloonContentHeader: '<a href="/clinic/'+value.url+'">' + value.name+'</a> '+ value.price,
                balloonContentBody: value.address,
            }
        }, {
            //preset: 'twirl#blueDotIcon',

            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '/images/mapicon.png',
            // Размеры метки.
            iconImageSize: [21, 30],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-11, -28],
        });
        /*var myPlacemark = new ymaps.Placemark([value.lat, value.long ],
         {
         balloonContentHeader: value.name,
         balloonContentBody: value.name
         });
         myCollection.add(myPlacemark);*/
        i = i + 1;
    });

    clusterer = new ymaps.Clusterer({preset: 'twirl#blueClusterIcons', gridSize: 64, maxZoom: 12, zoomMargin: 80});


    clusterer.add(myCollection);
    myMap.geoObjects.add(clusterer);
    /*myMap.geoObjects.add(myCollection)*/

}

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ ) text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}