/* library */

if ([].indexOf) {

    var find = function(array, value) {
        return array.indexOf(value);
    }

} else {
    var find = function(array, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === value)
                return i;
        }

        return -1;
    }
}
var findStr = function(array, value) {
    var i = 0;
    for (i = 0; i < array.length; i++) {
        if (value.indexOf(array[i]) !== -1) {
            return i;
        }
    }

    return -1;
}
/* /library */


var strictSearchUrls = [
    '/clinic', '/clinic/', '/search/clinic', '/search/clinic/',
    '/doctor', '/doctor/', '/search/doctor', '/search/doctor/',
    '/diagnostics', '/diagnostics/', '/search/diagnostics', '/search/diagnostics/',
];
var searchUrls = [
    
    '/clinic/metro', '/clinic/raion', '/clinic/profile',
    '/doctor/metro', '/doctor/raion', '/doctor/specialty',
    '/diagnostics/metro', '/diagnostics/raion', '/diagnostics/service',
];
var reloadSearchUrls = [
    '/clinic/metro', '/clinic/raion', '/clinic/profile',
    '/doctor/metro', '/doctor/raion', '/doctor/specialty',
    '/diagnostics/metro', '/diagnostics/raion', '/diagnostics/service',
];


window.countersManager = (function() {
	
	var instance,
		counters = {
			'yandex' : {
				'name' : 'yaCounter18794458'
			},
			'google' : {
				'name' : 'ga'
			}
		};
	 
	function countersMediator(options) {
		if (options['type'] == 'yandex') {
			//console.log('yandex counter started');
			if (typeof window[counters[ options['type'] ][ 'name' ]] != 'undefined') {
				window[counters[ options['type'] ][ 'name' ]].reachGoal( options['goalName'] );
				console.log('yandex goal sent: ' + options['goalName']);
				console.log(options);
			} else {
				console.log('undefined counter of type: ' + options['type']);
			}
		}
		
		if (options['type'] == 'google') {
			//console.log('google counter started');
			if (typeof window[counters[ options['type'] ][ 'name' ]] != 'undefined') {
				//console.log('trying to reach google goals');
				console.log(window[counters[ options['type'] ][ 'name' ]]);
				window[counters[ options['type'] ][ 'name' ]]( 'send', 'pageview', '/' + options['goalName'] );
				console.log('google goal sent: ' + options['goalName']);
				console.log(options);
			} else {
				console.log('undefined counter of type: ' + options['type']);
			}
		}
		//if (typeof ga != 'undefined') ga('send', 'pageview', '/click_search');
	}
	
	function reachGoalPrivate(options) {
		if (typeof options == 'undefined') {
			console.log('no options set');
			return false;
		}
		//if (options['forCounters'] == 'all') options['forCounters'] = ...; //реализация для всех счетчиков
		
		for(key in options['forCounters']) {
			var type = options['forCounters'][key];
			options['type'] = type;
			countersMediator(options);
		}
		if (typeof options['callback'] != 'undefined')
			options['callback']();
    }
	
    return {
        getInstance: function () {
            return 'getInstance';
        },		
        reachGoal: reachGoalPrivate
    };
})();



var searchParams = [];
var galleryWidth = 1;
var minMarginLeft = currentMargin = 0;
/* /vars */
$(function() {
    $(document).ready(function() {
    	window.addEventListener("popstate", function(e) {
    		try {
    			console.log(e);
	    		if(e.state && e.state.title && e.state.type == "refreshSearch") {
	    			location.reload();
	    			/*
		    		$.ajax({
		    			url: location.pathname + location.search,
		    			type: "GET",
		    			data: { defaultLayout: 1 },
		    			success: function(data) {
		    				//var newDoc = document.open("text/html", "replace");
		    				//newDoc.write(data);
		    				//newDoc.close();
		    				
		    				var body = data.match(/<body[^>]*>[\s\S]*<\/body>/gi);
		    				var elbody = $('<body></body>').html(body[0]);
		    				$('.contents').html(elbody.find('.contents').html());
		    				$('.SEARCH_BOX_HEAD').html(elbody.find('.SEARCH_BOX_HEAD').html());
		    				SearchEnd("refresh");
		    			}
		    		});
	    			 */
	    		}
    		} catch(exc) {}
    	}, false);
    	
    	SearchEnd("refresh");
    	
    });


    $(".search-result-box:odd, .search-box-service:odd").addClass("odd");
    /////////////////////////////////
    // Tabs
    /////////////////////////////////
    $(".search-tabs").delegate("li:not(.current)", "click", function() {
        $(".show_on_map.close").click();
        $(this).addClass("current").siblings().removeClass("current");
        var searchBox = $(".search").find(".search-box").hide().eq($(this).index());
        searchBox.fadeIn(140);
        if ($(".search-box:visible").hasClass("pushHistory") && location.pathname != "/") {
            refreshSearch(0, $(".search-box:visible form")[0]);
            /*searchBox.find('form').eq(0).submit();*/
        }


        var ids = searchBox.find('.cusel input').map(function() {
            return '#' + this.id;
        }).get().join(',');
        cuSelRefresh({
            refreshEl: ids,
            visRows: 10,
            scrollArrows: true
        });
    });
    $(document).ready(function() {
        $("#price-fancy").attr('href', $("#price-fancy").attr('href') + '?JSON=1');

        $('.short-search-box select').each(function() {
            var attr = $(this).attr('name');
            $(this).attr('name', attr.substr(0, attr.length - 1));
        });
    });

    $('#login-form label').inFieldLabels();
    /////////////////////////////////
    // Скругления для IE
    /////////////////////////////////
    if (window.PIE) {
        $(".custom-select, .btn, .custom-text, .btn-blue, .navigation, .info-title, .stock-item-title h3, .stock-item-title strong, .btn-green, .reviews-box").each(function() {
            PIE.attach(this);
        });
    }
    //search-service-form_text

    $('.fancybox').live('click', function() {
        $.fancybox({
            'href': this.href,
            'onComplete': function() {
                if ($("table td .fancybox:not(.noComplete)").length == 1) {
                    $("table td .fancybox:not(.noComplete)").eq(0).click();
                }
            }
        });
        return false;
    });
    $('#diagnostic').click(function() {
        $.fancybox({'href': this.href});
        return false;
    });
    $(".fancybox-thumb").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        helpers: {
            title: {
                type: 'outside'
            },
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });
    $("li.iframe a").fancybox({
        'width': '95%',
        'height': '95%',
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });
    $("a.imageupload-iframe").fancybox({
        //'width' : '95%',
        //'height' : '95%',
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        //'type' : 'iframe'
    });
    $('.choose-service').live('click', function() {
        $.fancybox.close();
        var obj = $('#search-service-form_text');
        obj.val($(this).html());
        obj.removeClass("error");
        if (location.pathname === "/") {
            $("#search-service-form_isStrictText").val(1);
            obj.closest("form").submit();
        }
        if (location.pathname.indexOf("search") !== -1) {
            refreshSearch(0);
        }
        /*$(".search-box:visible form").submit();*/
        $("#search-tab-3 .custom-select").removeClass("classDisCusel");
        return false;
    });
    //new calendar 
    $(".new-calendar select").focusout(function(){alert(10)});
    $(".new-calendar select").live('change', function(e) {
    	var selector = ".new-calendar .btn#" + escapeStr($(e.currentTarget).val());
    	$(selector).click();
    	console.log(selector);
    });
    $(".new-calendar .btn").live('click', function() {
        if ($(this).hasClass("btn-red"))
            return false;
        $(".new-calendar .btn.active").removeClass("active");
        $(this).addClass("active");
        var time = $(this).html();
        $("#AppointmentToDoctors_plannedTime").val($('#timeblock_date').val() + " " + time + ":00");
        $("#AppointmentToDoctors_extAppointmentId").val($(this).attr("id"));
        $("#appointment_VisitStart").val($(this).attr("VisitStart"));
        $("#appointment_VisitEnd").val($(this).attr("VisitEnd"));
        //console.log($("#planned_time").val());
        return false;
    });
    $('.calendar-row-admin .btn').live('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass("active");
        } else {
            $(this).addClass("active");
        }
        return false;
    });

    //Календарь времени "запись на прием"
    $(".calendar-row .btn").live('click', function() {
        if ($(this).hasClass("btn-red"))
            return false;
        $(".calendar-row .btn.active").removeClass("active");
        $(this).addClass("active");
        var time = $(this).html();
        $("#AppointmentToDoctors_plannedTime").val(new Date().getFullYear() + "-"
                + $(this).parent().find(".calendar-title").html().split(".")[1] + "-"
                + $(this).parent().find(".calendar-title").html().split(".")[0] + " " + time + ":00");
        $("#AppointmentToDoctors_extAppointmentId").val($(this).attr("id"));
        $("#appointment_VisitStart").val($(this).attr("VisitStart"));
        $("#appointment_VisitEnd").val($(this).attr("VisitEnd"));
        //console.log($("#planned_time").val());
        return false;
    });


	var assets = {};
    var form = $(".assets #userLoginForm");
    if(form.length > 0) {
    	assets.userLoginForm = form.clone();
    	form.remove();
    }
	$("#showPassword").parent().on('click', function() {
		if($("#showPassword").attr("checked")) {
			$("#LoginForm_password").attr('type', 'text');
		} else {
			$("#LoginForm_password").attr('type', 'password');
		}
	});
    $(".login").live("click",function(e) {
        //hideDropDowns('clinic');
        var form = assets.userLoginForm.clone();
        $.fancybox({
        	id: "fancyView",
        	content: form,
        });
        jQuery("#userLoginForm .niceCheck").mousedown(
        	function() {
        		changeCheck($(this));
        	}
        );
        jQuery("#userLoginForm .niceCheck input[type='checkbox']").change(
           	function() {
           		changeVisualCheck($(this))
           	}
        );
		$("#showPassword").parent().on('click', function() {
			if($("#showPassword").attr("checked")) {
				$("#LoginForm_password").attr('type', 'text');
			} else {
				$("#LoginForm_password").attr('type', 'password');
			}
		});

    	/*
        if (ob.css('display') == 'none') {
            ob.slideDown(100);
        } else {
            ob.slideUp(100);
        }
        e.stopPropagation();
        */
    });
    $(".services span").click(function() {
        $(".services span").removeClass('active');
        $(this).addClass('active');
        $(".services span").not('.active').each(function() {
            $(this).siblings('div').slideUp(300);
        })
        var div = $(this).siblings('div');
        if (div.is(':visible')) {
            div.slideUp(300);
        } else {
            div.slideDown(300);
        }
    })
    $(".for-clinics").click(function(e) {
        hideDropDowns('login');
        var ob = $(".dropdown-menu");
        if (ob.css('display') == 'none') {
            ob.slideDown(100);
        } else {
            ob.slideUp(100);
        }
        e.stopPropagation();
    });

    $('.log-in').click(function(e) {
        e.stopPropagation();
    });
    $(".dropdown-menu").click(function(e) {
        e.stopPropagation();
    });


    $('body, .roll-specialties').bind('click', function() {
        hideDropDowns();
    });
    $('input,select').live('select2-focus', function() {
        hideDropDowns();
    });
    $('.new-search-box').submit(function() {
        if (findStr(reloadSearchUrls, location.pathname) !== -1) {
            location.reload();
            return false;
        } else {
            return true;
        }
    });
    
    $('.report-clinic span').click(function() {
        var cLink = $(this).data('id');
        var button = $(this);
        $.ajax({
            url: '/ajax/reportClinic',
            async: true,
            data: {
                cLink:cLink
            },
            dataType:'json',
            type:'POST',
            success:function(resp) {                
                //button.parent().addClass('flash-notice');
                button.parent().html('<p class="flash-notice-special">Мы сожалеем, что вам не удалось записаться на прием и благодарим вас за обратную связь. Мы обязательно используем ее, чтобы сделать Единый Медицинский Портал лучше и удобнее.</p>');
				if (typeof checkRightBlockHeight != 'undefined') checkRightBlockHeight();
                $.cookie('clinic'+cLink,'1',{expires:30,path: '/',domain:''+location.hostname});
				
				if (typeof emailModal !== 'undefined') {					
					emailModal.show();
				}
            }
        });
    });
    
    $('.report-doctor span').click(function() {
        var dLink = $(this).data('id');
        var button = $(this);
        $.ajax({
            url: '/ajax/reportDoctor',
            async: true,
            data: {
                dLink:dLink
            },
            dataType:'json',
            type:'POST',
            success:function(resp) {
               //button.parent().addClass('flash-notice');
                button.parent().html('<p class="flash-notice-special">Мы сожалеем, что вам не удалось записаться на прием и благодарим вас за обратную связь. Мы обязательно используем ее, чтобы сделать Единый Медицинский Портал лучше и удобнее.</p>');                
                $.cookie('doctor'+dLink,'1',{expires:30,path: '/',domain:''+location.hostname});
				
				if (typeof emailModal !== 'undefined') {					
					emailModal.show();
				}
            }
        });
    });
    
    $(".micro_nav_cal > a,.micro_nav_cal").live('click', function() {
        $('.xdsoft_datetimepicker').fadeOut(100);
        $("#AppointmentToDoctors_plannedTime").val("");
        $("#AppointmentToDoctors_extAppointmentId").val("");
        $("#appointment_VisitStart").val("");
        $("#appointment_VisitEnd").val("");
        var type = $(this).attr('data-type');
        var date = $("#timeblock_date").val();
        var link = $("#AppointmentToDoctors_address_link").val();
        var doctorLink = $("#AppointmentToDoctors_doctorId").val();

        switch (type) {
            case 'forward':
            	updateAvailableTime(null, $("#timeblock_date"), "forward");
                break;
            case 'back':
                var myDate = new Date();
                var nowDay = myDate.getDate();
                var nowMonth = myDate.getMonth() + 1;
                var nowYear = myDate.getFullYear();
                var day = parseInt(date.split("-")[2]);
                var month = parseInt(date.split("-")[1]);
                var year = parseInt(date.split("-")[0]);
                if ((year > nowYear) || (month > nowMonth && year >= nowYear) || (day > nowDay && month >= nowMonth && year >= nowYear)) {
                	updateAvailableTime(null, $("#timeblock_date"), "back");
                }
                break;
        }

        return false;
    });
    $('.input-picker').live('click', function() {
        var inp = $('#isVkApp').val() ? $(this) : $('.picker');
        $('.xdsoft_datetimepicker').fadeIn(100);
        $('.xdsoft_datetimepicker').css({top: inp.offset().top + inp.height(), left: inp.offset().left});
    });
    $(document).bind("click", function (e) {
    	if($(e.target).closest("#calendar").length == 0) {
	    	if($(e.target).closest(".xdsoft_datetimepicker").length == 0) {
	    		$('.xdsoft_datetimepicker').fadeOut(100);
	    	}
    	}
	});
    $(".nav_cal > a").live('click', function() {
        $("#AppointmentToDoctors_plannedTime").val("");
        $("#AppointmentToDoctors_extAppointmentId").val("");
        $("#appointment_VisitStart").val("");
        $("#appointment_VisitEnd").val("");
        var type = $(this).attr('data-type');
        var date = $("#timeblock_date").val();
        var link = $("#AppointmentToDoctors_address_link").val();
        var doctorLink = $("#AppointmentToDoctors_doctorId").val();
        var backend = $("#backend").val() || 0;

    	$("#calendar_loading").fadeIn(250);
        $("#calendar").closest("div.ui-dialog").hide();
        switch (type) {
            case 'forward':
                $.ajax({
                    url: "/ajax/getDatesForCalendar",
                    async: true,
                    data: ({
                        date: date,
                        type: "forward",
                        link: link,
                        doctor: doctorLink,
                        backend: backend
                    }),
                    dataType: 'json',
                    type: "GET",
                    success: function(response) {
                        if (response.html != "Клиника не указала часы работы") {
                            $("#calendar").html(response.html);
                            $("#timeblock_date").val(new Date().getFullYear() + "-" +
                                    $(".time_block:eq(0) .calendar-title").html().split(".")[1] +
                                    "-" + $(".time_block:eq(0) .calendar-title").html().split(".")[0]);
                        } else {
                        	this.error(null,response);
                        }
                    },
        			error: function(XHR, textStatus, errorThrown) {
        				alert(JSON.stringify(textStatus));
        			},
                    complete: function() {
                        $("#calendar").closest("div.ui-dialog").show();
                    	$("#calendar_loading").fadeOut(250);
                    }
                });
                break;
            case 'back':
                var myDate = new Date();
                var nowDay = myDate.getDate();
                var nowMonth = myDate.getMonth() + 1;
                var day = parseInt(date.split("-")[2]);
                var month = parseInt(date.split("-")[1]);

                if ((day > nowDay && month === nowMonth) || month > nowMonth) {
                    $.ajax({
                        url: "/ajax/getDatesForCalendar",
                        async: true,
                        data: ({
                            date: date,
                            type: "back",
                            link: link,
                            doctor: doctorLink,
                            backend: backend
                        }),
                        dataType: 'json',
                        type: "GET",
                        success: function(response) {
                            if (response.html != "Клиника не указала часы работы") {
                                $("#calendar").html(response.html);
                                $("#timeblock_date").val(new Date().getFullYear() + "-" +
                                        $(".time_block:eq(0) .calendar-title").html().split(".")[1] +
                                        "-" + $(".time_block:eq(0) .calendar-title").html().split(".")[0]);
                            } else {
                            	this.error(null,response);
                            }
                        },
            			error: function(XHR, textStatus, errorThrown) {
            				alert(JSON.stringify(textStatus));
            			},
                        complete: function() {
                            $("#calendar").closest("div.ui-dialog").show();
                        	$("#calendar_loading").fadeOut(250);
                        }
                    });
                }
                break;
        }

        return false;
    });
    var addrLink = false;
    var doctorLink = false;
    var appointmentLink = false;
    $('[id^=time_]').live('click', function() {
        appointmentLink = $(this).attr('data-link');
        doctorLink = $(this).attr('data-doctor');
        addrLink = $(this).attr('data-address');
        var dt = new Date();
        var month = (parseInt(dt.getMonth()) + 1);
        /* console.log(dt.getFullYear() + "-" + (month > 9 ? month : "0"+month) + "-" + dt.getDate());*/
        dt.setDate(parseInt(dt.getDate()) + 1);
        var date = dt.getFullYear() + "-" + (month > 9 ? month : "0" + month) + "-" + dt.getDate();
    	var calendar_app_block = $(this).closest(".calendar_app_block");
    	$("#calendar_loading").fadeIn(250);
    	calendar_app_block.hide();
        $.ajax({
            url: "/ajax/getDatesForCalendar",
            async: true,
            data: ({
                /* date: date,
                 type: "forward",*/
                link: addrLink,
                doctor: doctorLink
            }),
            dataType: 'json',
            type: "GET",
            success: function(response) {
                if (response.html != "Клиника не указала часы работы") {
                    $('#mydialog').html('<div id="calendar">' + response.html + '</div>\n\
<input type="hidden" id="AppointmentToDoctors_plannedTime" name="AppointmentToDoctors[plannedTime]">\n\
<input type="hidden" id="AppointmentToDoctors_link" name="AppointmentToDoctors[link]" value="' + appointmentLink + '">\n\
<input type="hidden" id="AppointmentToDoctors_address_link" value="' + addrLink + '">\n\
<input type="hidden" id="AppointmentToDoctors_doctorId" value="' + doctorLink + '">\n\
<span id="saveNewDate" class="btn btn-green" style="float:right">Сохранить<span>\n\
');
                    $('#mydialog').append('<input type="hidden" id="timeblock_date" value="' + date + '">');
                } else {
                	this.error(null,response);
                }
            },
            complete: function() {
            	$("#calendar_loading").fadeOut(250);
            	calendar_app_block.show();

                $('#mydialog').dialog("open");
            },
			error: function(XHR, textStatus, errorThrown) {
				alert(JSON.stringify(textStatus));
			},
        });
    });

    $("#saveNewDate").live('click', function() {
        var plannedTime = $("#AppointmentToDoctors_plannedTime").val();
        var appointmentLink = $("#AppointmentToDoctors_link").val();

        $("#calendar").closest("div.ui-dialog").hide()
    	$("#calendar_loading").fadeIn(250);
        $.ajax({
            url: "/adminClinic/ajax/setNewDate",
            async: true,
            data: ({
                plannedTime: plannedTime,
                link: appointmentLink
            }),
            dataType: 'json',
            type: "GET",
            success: function(response) {
                if (response.success == true) {
                	$("#calendar_loading_successful").fadeIn(250);

                    setTimeout(function() {
	                    $('#mydialog').dialog("close");
	                    $(".calendar_app_close_button").trigger('click');
	                    
	                    $.ajax({
	                        url: window.location.pathname + window.location.search,
	                        async: false,
	                        type: "GET",
	                        success: function(data) {
	                        	var el = $( '<div></div>' );
	                        	el.html(data);
	                        	$(".calendar_table").html($(".calendar_table",el).html());
	                        },
	            			error: function(XHR, textStatus, errorThrown) {
	            				alert(JSON.stringify(textStatus));
	            			},
	                    });
                    }, 500);
                } else {
                	this.error(null,response);
                }
            },
			error: function(XHR, textStatus, errorThrown) {
            	$("#calendar_loading_error").fadeIn(250);

                setTimeout(function() {
			        $("#calendar").closest("div.ui-dialog").show()
					alert(JSON.stringify(textStatus));
                }, 500);
			},
			complete: function() {
		    	$("#calendar_loading").fadeOut(250);

                setTimeout(function(){
                    $("#calendar_loading_successful").fadeOut(250);
	                $("#calendar_loading_error").fadeOut(250);
            	}, 750);
			}
        });
    });
    $('[id^=seen_],[id^=accept],[id^=decline]').live('click', function() {
        var link = $(this).attr('data-link');
        var status = $(this).attr('id').split("_")[0];
        var obj = $('[id^=' + status + '_' + link + ']').closest("tr");
        var hider = obj.find(".pseudo_a");
        var accept = true;
        switch (status) {
            case "accept":
                message = "Подтвердить запись?";
                accept = confirm(message);
                break;
            case "decline":
                var reason = prompt("Укажите причину отклонения");
                //message = "Отклонить запись?";
                if (reason) {
                    accept = true;
                } else {
                    accept = false;
                }
                //confirm(message);
                break;
            case "seen":
                /*show hider*/
                var badge = $('.badge-important');
                var count = parseInt(badge.html());
                if (count > 1) {
                    badge.html(--count);
                } else {
                    badge.remove();
                }
                hider.html(hider.attr('data'));
                hider.removeClass("pseudo_a");
                break;
        }

        if (accept) {
        	$("#calendar_loading").fadeIn(250);
        	var calendar_app_block = $(this).closest(".calendar_app_block");
        	calendar_app_block.hide();
            $.ajax({
                url: "/adminClinic/ajax/setAppointmentStatus",
                async: true,
                data: ({
                    'link': link,
                    'status': status,
                    'json': true,
                    'reason': reason ? reason : ''
                }),
                dataType: 'json',
                type: "GET",
                success: function(response) {
                    if (response.success) {
                    	$("#calendar_loading_successful").fadeIn(250);

                        setTimeout(function() {
		                    obj.find(".pseudo_a").trigger('click');
		                    obj.find(".status").html(response.msg);
		                    obj.find(".actions").html(response.actions);
		                    var classList = obj.find('.calendar_app').attr('class').split(/\s+/);
		                    $.each( classList, function(index, item){ if (item.indexOf("appointment_status") >= 0) { obj.find('.calendar_app').removeClass(item); } });
		                    obj.find('.calendar_app').addClass("appointment_status"+response.statusId);
                        }, 500);
                    } else {
                    	this.error(null,response);
                    }
                },
                error: function() {
                	$("#calendar_loading_error").fadeIn(250);
	    			alert(JSON.stringify(textStatus));
                },
                complete: function() {
                	$("#calendar_loading").fadeOut(250);
                    setTimeout(function(){
	                    $("#calendar_loading_successful").fadeOut(250);
    	                $("#calendar_loading_error").fadeOut(250);
                    	calendar_app_block.show();
                	}, 750);
                }
            });
        }
    });
    $(".search-result-head .show_on_map.close").live('click', function() {
        $("#search_map").hide();
        $(".show_on_map.found").show();
        $(".show_on_map.close").hide();
    });
    $(".search-result-head .show_on_map.found").live('click', function() {
        $(".search-result-head .show_on_map.found").hide();
        $(".search-result-head .show_on_map.close").show();
        if($("#search_map").css('display') == 'none') {
			$("#search_map").html("<div id='topdoctor_loading' class='icon_loading'></div>");
			$("#search_map").show();
        }
        LoadMap(this.href);
        return false;
    });
    $(".update_price").live('click', function() {
        $(this).parent().find("span[name^=AddressServices]").trigger('click');
    });/*
     $(".delete_price").live('click', function() {
     if (confirm("Удалить услугу?")) {
     var attr = $(this).closest("tr").find('span[name^=AddressServices]').attr('name');
     var value = 0;
     $.ajax({
     type: "POST",
     url: '/adminClinic/services/ajaxUpdate',
     data: attr + '=' + value,
     });
     $(this).closest("tr").remove();
     }
     });*/
    $('.confirm_license').live('click', function() {
        if (!confirm("Вы уверены что хотите удалить скан лицензии?"))
            return false;
    });
    $(document).ready(function() {
        var val = $("#search-service-form_text").val();
        var obj = $("#search-tab-3 .custom-select");
        if (val) {
            obj.removeClass("classDisCusel");
        } else {
            obj.addClass("classDisCusel");
        }
    });
    $('#search-service-form').submit(function() {
        $("#search-service-form_text").removeClass('error');
        if (!$("#search-service-form_text").val()) {
            $("#search-service-form_text").addClass('error');
            return false;
        }
    });

    $("a.expandArea").live('click', function() {
        $("input[name='Search[expandSearchArea]']").closest(".niceCheck").parent().show();
        $("input[name='Search[expandSearchArea]']").click();
    });
    $(".page a, .next a, .previous a, .first a, .last a").live('click', function() {
        $(".show_on_map.found").attr('href', String($(this).attr('href')).replace("/clinic", "/map"));
    });
    $(".ico-calendar").click(function() {
        $("#search-clinic-form_start").focus().focus();
    });

    $('.search.patient input[type="submit"]').click(function() {
        //if (typeof yaCounter18794458 != 'undefined') yaCounter18794458.reachGoal('click_search');
        if (typeof ga != 'undefined') ga('send', 'pageview', '/click_search');
    });
    $('.search.patient_clinic input[type="submit"]').click(function() {
        //if (typeof yaCounter18794458 != 'undefined') yaCounter18794458.reachGoal('click_search_clinic');
        if (typeof ga != 'undefined') ga('send', 'pageview', '/click_search_clinic');
    });

    $(".favorites a.btnFav").click(function() {
        if (!$(this).hasClass("added"))
            $(this).addClass("added").attr("title", 'Удалить').find("div").html("Удалить");
        else
            $(this).removeClass("added").attr("title", 'В избранное').find("div").html('В избранное');
    });
    $("#closeAuthBlock").click(function() {
        $(".askAuth").fadeOut(200);
    });
    if ($('div[id $= "form_idMetroStation"]:not(.cusel-scroll-pane)').length) {
        $('div[id $= "form_idMetroStation"]:not(.cusel-scroll-pane) span').click(function() {
            select = $(this).closest('div[id $= "form_idMetroStation"]:not(.cusel-scroll-pane)').parent().find('div[id $= "form_idCityDistrict"]:not(.cusel-scroll-pane)');
            if ($(this).attr('val') !== "") {
                select.find("input").val("");
                select.find(".cuselText").html("Район города");
                select.find(".cuselActive").removeClass("cuselActive");
                select.find("span:eq(0)").addClass("cuselActive");
            }
        });
    }

    if ($('div[id $= "form_idCityDistrict"]:not(.cusel-scroll-pane)').length) {
        $('div[id $= "form_idCityDistrict"]:not(.cusel-scroll-pane) span').click(function() {
            select = $(this).closest('div[id $= "form_idCityDistrict"]:not(.cusel-scroll-pane)').parent().find('div[id $= "form_idMetroStation"]:not(.cusel-scroll-pane)');
            if ($(this).attr('val') !== "") {
                select.find("input").val("");
                select.find(".cuselText").html("Станция метро");
                select.find(".cuselActive").removeClass("cuselActive");
                select.find("span:eq(0)").addClass("cuselActive");
            }
        });
    }


    cuselShowList($("#cuselFrame-search-clinic-form_idCompanyActivite .cusel-scroll-wrap"));
    cuselShowList($("#cuselFrame-search-clinic-form_idCompanyActivite .cusel-scroll-wrap"));
    if ($("#cuselFrame-search-clinic-form_idCompanyType").length) {

        $("#cuselFrame-search-clinic-form_idCompanyType span").click(function() {

            var companyTypeId = $(this).attr('val');
            $.ajax({
                url: '/ajax/getCompanyActivitesByCompanyType',
                data: ({
                    'companyTypeId': companyTypeId
                }),
                dataType: 'JSON',
                type: "GET",
                success: function(response) {
                    var select = $("#cuselFrame-search-clinic-form_idCompanyActivite");
                    if (response == false) {
                        select.addClass("classDisCusel");
                    } else {
                        select.removeClass("classDisCusel");
                    }

                    var wrap = $("#cusel-scroll-search-clinic-form_idCompanyActivite");
                    wrap.find("span:not(:first)").remove();
                    $("#search-clinic-form_idCompanyActivite").val("");
                    wrap.find("span:first").addClass("cuselActive");
                    select.find(".cuselText").html("Профиль деятельности");
                    var count = response.length;
                    for (var i in response) {
                        wrap.append('<span val="' + response[i].id + '">' + response[i].name + '</span>');
                    }
                    var visRows = count < 10 ? (count + 1) : 10;
                    var params = {
                        refreshEl: "#search-clinic-form_idCompanyActivite",
                        visRows: visRows,
                        scrollArrows: true
                    }
                    cuSelRefresh(params);
                    cuselShowList($("#cuselFrame-search-clinic-form_idCompanyActivite .cusel-scroll-wrap"));
                    cuselShowList($("#cuselFrame-search-clinic-form_idCompanyActivite .cusel-scroll-wrap"));
                },
            });
        });
    }
    $('.contacts-button-info').toggle(function() {
        $('.wrap-contact-info').slideDown(500);
        $(this).addClass('arrow-down-opened');
    }, function() {
        $('.wrap-contact-info').slideUp(500);
        $(this).removeClass('arrow-down-opened');
    });

    $('.contacts-button-hour').toggle(function() {
        $('.whours').slideDown(500);
        $(this).addClass('arrow-down-opened');
    }, function() {
        $('.whours').slideUp(500);
        $(this).removeClass('arrow-down-opened');
    });
    $('.services-button').toggle(function() {
        $('.services').slideDown(500);
        $(this).addClass('arrow-down-opened');
    }, function() {
        $('.services').slideUp(500);
        $(this).removeClass('arrow-down-opened');
    });

    if ($('.doctor-middle-block .description').height() < 242) {
        $('.roll-description').remove();
        $('.description').css({cursor: 'default', 'margin-bottom': '10px'});
    }
    $('.doctor-middle-block .description,.roll-description').toggle(function() {
        $('.doctor-middle-block .description').css('max-height', 'inherit');
        $('.roll-description').addClass('opened');
    }, function() {
        $('.doctor-middle-block .description').css('max-height', '242px');
        $('.roll-description').removeClass('opened');
    });

    $('.roll-specialties').toggle(function() {
        $('.specialtyTable').height($('.specialtyTable')[0].scrollHeight);
        $(this).addClass('opened');
    }, function() {
        $('.specialtyTable').height(100);
        $(this).removeClass('opened');
    });
/*
    $(window).scroll(function() {
        if ($(".search-result.mid").length && $('.left-filter').length) {
            if (window.scrollY > 204) {
                var h = $('.right').height();
                if (!$('#search_map').is(':visible')) {
                    $(".search-result.mid").css('margin-top', '109px');
                }
                if (window.scrollY < ($(".search-result.mid").height() + $(".search-result.mid").offset().top - $('.left-filter').height() - (h ? h : $('.left-filter').height()))) {
                    $('.new-search-box .search').css({position: 'fixed', top: '0px', 'margin-top': '0px'});
                    $('.left-filter').css({'position': 'fixed', 'top': '95px'});
                    $('.right').css({'position': 'fixed', 'top': '95px', 'right': 'calc((100% - 950px) / 2)'});
                } else {
                    $('.new-search-box .search').css({
                        'position': 'absolute',
                        'top': $(".search-result.mid").offset().top +
                                $(".search-result.mid").height() -
                                $('.left-filter').height() -
                                (h ? h : $('.left-filter').height())
                    });

                    $('.left-filter').css({
                        'position': 'absolute',
                        'top': $('.new-search-box .search').offset().top + 100 - $('.container').offset().top
                    });

                    $('.right').css({
                        'position': 'absolute',
                        'top': $(".search-result.mid").height() - $('.right').height() + $('.left-filter').height() - 25
                    });

                }

            } else {
                $("#scroller").fadeOut(150);
                $(".search-result.mid").css({'margin-top': '0px'});
                $('.new-search-box .search').css({'margin-top': '15px', 'position': 'inherit'});
                $('.left-filter').css({'position': '', 'top': ''});
                $('.right').css({'position': '', 'top': ''});
            }
        }
    });
*/
    $('select[id*="_workingHours_"]').change(function() {
        var select = $(this);
        if (select.val() == '11111') {
            select.siblings("select").val(select.val());
        }
    });
/*
    $(".block .btn-blue").click(function() {
        $(".btn-green").addClass("btn-blue");
        $(".btn-green").removeClass("btn-green");
        $(this).addClass("btn-green");
        $(this).removeClass("btn-blue");
    });
*/
    $("#visit_change_address").live('click', function() {
        $(".list_addresses").slideToggle(300);
    });
    $(".visit_element_address").live('click', function() {
        var link = $(this).data('link');
        var linkUrl = $(this).data('linkurl');
        var text = $(this).data('text');
        var obj = $(".change_address_name");
        obj.attr('href', '/clinic/' + linkUrl);
        obj.html(text);
        $("#AppointmentToDoctors_address_link").val(link);
        $(".list_addresses").slideUp(300);
        $('a.clear-row').click();
    })

    $("#clinic_activites .btn").live('click', function() {
        var container = $("#clinic_activites_prices");
        var btnBack = '<span id="btnBack" class="btnBack btn-blue">Вернуться к выбору профилей деятельности</span>';
        var addressLink = $("#addr_link").val();
        var activityLink = $(this).data('link');
        $.ajax({
            url: '/ajax/getPricesByActivity',
            data: ({
                'link': addressLink,
                'activity': activityLink,
            }),
            type: "GET",
            success: function(response) {
                container.html(response + btnBack);
                container.show();
                $("#clinic_activites").hide();
            },
        });
        return false;
    });
    $("#clinic_activites_prices #btnBack").live('click', function() {
        var container = $("#clinic_activites_prices");
        var btnBack = '<span id="btnBack" class="btnBack btn-blue">Вернуться к выбору профилей деятельности</span>';
        container.hide();
        $("#clinic_activites").show();
    });

		$('#citySelector').live("change", function() {
			var regionSubdomain =  $(this).val();
			setRegion(regionSubdomain);
		});

		//purchasing services
		$('.btn-purchase-service').live('click', function() {
			var $purchaseButton = $(this);
			var serviceId = $purchaseButton.attr('data-id');
			var $purchaseTable = $purchaseButton.parent().parent().parent();
			//console.log('$purchaseButton serviceId = ' + serviceId);
			
			var phone = $purchaseTable.find('.purchase-service-phone').val();
			phone = phone.replace(/( )|(\-)|(\()|(\))/g, '');
			
			var dataParams = {
				'id': serviceId,
				'name': $purchaseTable.find('.purchase-service-name').val(),
				'phone': phone,
                'vk': $('#isVkApp').val(),
			};
			//console.log('dataParams: ');
			//console.log(dataParams);
			
			$purchaseButton.after('<p class="purchase-service-info">Подождите, идет обработка...</p>');
			$purchaseButton.hide();
			
			$.ajax({
				url: '/ajax/purchaseService',
				type: 'GET',
				data: dataParams,
				success: function(data) {
					data = JSON.parse(data);
					if (data.success == false) {
                        $('.purchase-service-info').hide();
                        $purchaseButton.show();
                        alert(data.msg);
						return false;
					}
					if (data.success == true) {
                        if($('#isVkApp').val()) {
                            $("#serviceSend").show();
                        }
                        else {
						  alert(data.msg);
                        }
						var $popup = $('#' + serviceId);
						if ($popup.length) {
							$popup.removeClass('md-show');
						}
						lastPageClose();
						window.setTimeout(function() {
							countersManager.reachGoal({
								'forCounters' : ['yandex', 'google'],
								'goalName' : 'appointment_from_service',
								//'callback': function() {console.log('callback: user_register');}
							});
                            if(data.guest == '0' && !$('#isVkApp').val()) {
                                window.location.href = '/user/registry';
                            }
						}, 1);
					}
					$purchaseButton.show();
					$purchaseButton.next().remove();
				},
				error: function(data) {
					console.log('error while getting response from purchaseService');
					$purchaseButton.show();
					$purchaseButton.next().remove();
				},
			});
			return false;
		});

		//search diagnostics, clinic card
		$('.service_link').live('click', function() {
			var options = {
				'id': $(this).attr('data-service-id')
			};
			if(isMobile()) {
				nextPageOpenContent('#'+$(this).attr('data-service-id'));
				return false;
			}
			var serviceModal = new ModalWindowConstructor(options);
			serviceModal.show();
			return false;
		});
});
function updateWorkingHourByDoctor() {
    var link = $("#AppointmentToDoctors_address_link").val();
    var doctorLink = $("#AppointmentToDoctors_doctorId").val();
    var date = $("#timeblock_date").val();
    var type = "doctor";
    var backend = $("#backend").val() || 0;
    $.ajax({
        url: "/ajax/getDatesForCalendar",
        async: false,
        data: ({
            link: link,
            doctor: doctorLink,
            date: date,
            type: type,
            backend: backend
        }),
        dataType: 'json',
        type: "GET",
        success: function(msg) {
            if (msg.html != "Клиника не указала часы работы") {
                $("#calendar").html(msg.html);
                $("#timeblock_date").val(new Date().getFullYear() + "-" +
                        $(".time_block:eq(0) .calendar-title").html().split(".")[1] +
                        "-" + $(".time_block:eq(0) .calendar-title").html().split(".")[0]);
            }
        },
    });
}
function generateCodeForPhone() {
    var phone = $("#User_telefon").val();
    var em_block = $("#User_telefon_em_");
    em_block.html("");
    phone = String(phone).replace(/ |\+|-|\(|\)/g, "");
    $.ajax({
        url: "/ajax/generateCodeForPhone",
        async: false,
        data: ({
            'phone': phone,
        }),
        type: "GET",
        success: function(response) {
            if (response == "error_phoneIsBusy") {
                em_block.show();
                if (!(em_block.html() == "")) {
                    em_block.append("<br class='invalid_code'>");
                }
                em_block.html("<span class='invalid_code'>Данный номер занят.</span>");
            } else {
                if (response == "true" || response == "already_sent") {
                    var actPh = $("#activatePhone");
                    actPh.html("Активировать");
                    actPh.attr('onclick', "activatePhone();return false;");
                    actPh.before('<input class="custom-text" id="phone_code"/> ');
                    em_block.html("<span>Введите код присланный на мобильный телефон.</span>");
                    em_block.show();
                    $(".invalid_code").remove();
                } else {
                    if (phone == "") {
                        em_block.show();
                        if (!(em_block.html() == "")) {
                            em_block.append("<br class='invalid_code'>");
                        }
                        em_block.html("<span class='invalid_code'>Необходимо заполнить поле Мобильный телефон.</span>");
                    }
                    console.log("error: method generateCodeForPhone");
                }
            }
        },
    });
}

function activatePhone() {
    $("#phone_code").removeClass("error");
    var phone = $("#User_telefon").val();
    var code = parseInt($("#phone_code").val());
    phone = String(phone).replace(/ |\+|-|\(|\)/g, "");
    $.ajax({
        url: "/ajax/activatePhone",
        async: false,
        data: ({
            'phone': phone,
            'code': code
        }),
        type: "GET",
        success: function(response) {
            if (response == "valid") {
                $(".invalid_code").remove();
                $("#User_telefon_em_").hide();
                $("#activatePhone").before('<span class="btn-blue">Активировано</span>');
                $("#activatePhone").remove();
                $("#phone_code").remove();
            } else {
                var em_block = $("#User_telefon_em_");
                $("#phone_code").addClass("error");
                $(".invalid_code").remove();
                em_block.show();
                if (!(em_block.html() == "")) {
                    em_block.append("<br class='invalid_code'>");
                }
                em_block.append("<span class='invalid_code'>Неверный код</span>");
            }
        },
    });
}

function getPriceByDoctorAndService() {
    var serviceId = $('#AppointmentToDoctors_serviceId').val();
    var doctorId = $('#AppointmentToDoctors_doctorId').val();
    if (serviceId && doctorId) {
        $.ajax({
            url: "/ajax/getPriceByServiceAndDoctor",
            async: false,
            data: ({
                'serviceId': serviceId,
                'doctorId': doctorId,
            }),
            type: "GET",
            success: function(response) {
                if (response)
                    $("#price_for_doctor").html("Цена: " + response);
                else
                    $("#price_for_doctor").html("Цена: не установлена");
            },
        });
    }
}
var changeButton;
function changePhoneNumber() {
    var phoneSpan = $("#phoneSpan");
    var phoneSpanValue = phoneSpan.html();
    phoneSpan.before('<input class="custom-text" style="display:none" id="phoneInput" type="text"/>');
    changeButton = $('.changePhoneNumber');
    var getCodeButton = $(".getCodeButton");
    var phoneInput = $("#phoneInput");
    phoneInput.mask("+79999999999", {placeholder: "_"});
    //phoneInput.val("");
    if (!phoneInput.is(":visible")) {
        changeButton.hide();
        phoneInput.show();
        getCodeButton.show();
        phoneSpan.hide();
    }
}

function getCodeNumber() {
    var phoneInput = $("#phoneInput");
    var phoneSpan = $("#phoneSpan");
    var phoneSpanValue = phoneSpan.html();
    var phoneInputValue = phoneInput.val();
    //changeButton = $('.changePhoneNumber');
    var getCodeButton = $(".getCodeButton");
    var acceptButton = $(".acceptButton");
    var declineButton = $(".declineButton");
    var error = 0;
    if (phoneSpanValue == phoneInputValue || !phoneInputValue) {
        phoneInput.css("border-color", "#E10000");
        setTimeout(function() {
            phoneInput.css("border-color", "#CCCCCC");
        }, 700);
    } else {
        $.ajax({
            url: '/ajax/changePhone',
            async: false,
            data: ({
                phone: phoneInputValue,
            }),
            type: "GET",
            success: function(response) {
                //console.log(response);  
            },
        });
        //console.log(phoneSpanValue +"_"+phoneInput+"_"+phoneInputValue);
        phoneSpan.html(phoneInputValue);
        phoneSpan.show();
        phoneInput.hide();
        getCodeButton.hide();
        acceptButton.before('<br/><input class="custom-text" type="text" id="code"/> ');
        acceptButton.show();
        declineButton.show();
        acceptButton.after('<br rel="rm"><span class="error phone_error">Введите код высланный на указанный телефон</span>');
    }
}

function declineCode() {
    var phoneobj = $("#phoneSpan");
    var phone = $("#phoneSpan").html();
    if (phone) {
        $.ajax({
            url: '/ajax/removePhone',
            async: false,
            data: ({
                phone: phone,
            }),
            type: "GET",
            success: function(response) {
                if (response == "ok") {
                    phoneobj.hide("fast");
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                }
            },
        });
    }
}
function acceptCode() {
    var code = $("#code").val();
    var phone = $("#phoneSpan").html();
    $(".phone_error").remove();
    $('br[rel="rm"]').remove();
    if (code && phone) {
        $.ajax({
            url: '/ajax/changePhone',
            async: false,
            data: ({
                phone: phone,
                code: code
            }),
            type: "GET",
            success: function(response) {
                if (response == "ok") {
                    $("#code").fadeOut(800);
                    $(".acceptButton").fadeOut(800);
                    $(".declineButton").fadeOut(800);
                    setTimeout(function() {
                        $(".acceptButton").after('<span class="btn-blue">Телефон подтвержден</span>');
                    }, 1000);
                    setTimeout(function() {
                        location.reload();
                    }, 2500);
                }
            },
        });
    } else {
        $(".acceptButton").after('<br rel="rm"><span class="error phone_error">Введите код высланный на указанный телефон</span>');
    }
}

function changePhoneForAppointment() {
    var phoneSpan = $("#phoneSpan");
    var phoneSpanValue = phoneSpan.html();
    phoneSpan.before('<input class="custom-text" style="display:none" id="phoneInput" type="text"/>');
    changeButton = $('.changePhoneAppointment');
    var getCodeButton = $(".getCodeButton");
    var phoneInput = $("#phoneInput");
    phoneInput.mask("+79999999999", {placeholder: "_"});
    //phoneInput.val("");
    if (!phoneInput.is(":visible")) {
        changeButton.hide();
        phoneInput.show();
        getCodeButton.show();
        phoneSpan.hide();
    }
}

var time;
setInterval(function() {
    if (time > 0) {
        time--;
        if (time === 0) {
            $('.code-lost').removeClass('blocked').html("Получить новый код").attr('onclick', '$("#phoneSpan").html("");getCodeForAppointment();');


        }
        $('.time').html(time);
    }
}, 1000);
function getCodeForAppointment() {
    var phoneInput = $("#phoneInput");
    var phoneSpan = $("#phoneSpan");
    $('br[rel="rm"],.phone_error,.phone-help,.code-lost').remove();

    var phoneSpanValue = phoneSpan.html();
    var phoneInputValue = phoneInput.val();
    phoneInputValue = String(phoneInputValue).replace(/_/g, "");
    //changeButton = $('.changePhoneNumber');
    var getCodeButton = $(".getCodeButton");
    var acceptButton = $(".acceptButton");
    if (phoneSpanValue == phoneInputValue || !phoneInputValue) {
        //if (typeof yaCounter18794458 != 'undefined') yaCounter18794458.reachGoal('phone_code');
    	if(phoneInputValue) {
            if (typeof (isGuest) != "undefined") {
                acceptButton.after('<br rel="rm"><span class="code-lost blocked">Получить новый код можно через <span class="time">' + time + '</span> с</span>');
            } else {
                phoneSpan.show();
                phoneInput.parent().remove();
                getCodeButton.hide();
                //changeButton.show();
            }
    	} else {
    		$("#AppointmentToDoctors_phone_em_").text("Введите телефон!");
    	}
    } else {  //ChangePhoneForAppointment
        $.ajax({
            url: '/ajax/changePhoneForAppointment',
            async: false,
            data: ({
                phone: phoneInputValue,
            }),
            type: "GET",
            success: function(response) {
            	var responseArray = response.split(';');
            	console.log("response: "+responseArray[0]);
            	switch(responseArray[0]) {
	            	case "error_phoneIsBusy":
	            		acceptButton.after('<br rel="rm"><span class="errorMessage phone_error">Данный телефон занят</span>');
	            		break;
	            	default:
	            		time = parseInt(response.split(';')[1]);
	            		acceptButton.after('<br rel="rm"><span class="code-lost blocked">Получить новый код можно через <span class="time">' + time + '</span> с</span>');
	            		phoneSpan.html(phoneInputValue);
	            		phoneSpan.show();
	            		phoneInput.parent().hide();
	            		getCodeButton.hide();
	            		if (!$("#code").length) {
	            			acceptButton.before('<br/><input class="custom-text" type="text" placeholder="Введите код подтверждения" id="code"/> ');
	            		}
	            		$("#code").focus();
	            		switch(responseArray[0]) {
			            	case "not_found":
				        		acceptButton.after('<br rel="rm"><span class="errorMessage phone_error">Не правильный код подтверждения</span>');
				        		break;
			            	case "already_sent":
			        			acceptButton.after('<br rel="rm"><span class="errorMessage phone_error">Код подтверждения уже отправлен</span>');
			        			break;
			            	case "true":
				        		acceptButton.after('<br rel="rm"><span class="errorMessage phone_error">Код подтверждения отправлен</span>');
				        		break;
			            	case "auto":
		            			/* автоотправка если первый раз */
		            			//console.log('response = ');
		            			//console.log(response);
		            			$("#code").val('iddqd');
		            			window.show_on_unload = 1;
		            			$( "#appointment_main" ).addClass( "appointment_hidden" );
		            			$( "#appointment_wait_while_loading" ).removeClass( "appointment_hidden" );
		            			$( "#doctor-visit" ).submit(); // !! auto submit
				        		break;
	            		}
	            		break;
            	}
            },
        });
        return false;
        //console.log(phoneSpanValue +"_"+phoneInput+"_"+phoneInputValue);      
    }
}

function acceptCodeForAppointment() {
    var code = $("#code").val();
    var phone = $("#phoneSpan").html();
    $(".phone_error,.phone-help").remove();
    $('br[rel="rm"]').remove();
    if (code && phone) {
        $.ajax({
            url: '/ajax/changePhoneForAppointment',
            async: false,
            data: ({
                phone: phone,
                code: code
            }),
            type: "GET",
            success: function(response) {
            	var responseArray = response.split(';');
        		switch(responseArray[0]) {
	            	case "not_found":
	            		$(".acceptButton").after('<br rel="rm"><span class="errorMessage phone_error">Неверный код</span>');
		        		break;
	            	case "already_sent":
	            		$(".acceptButton").after('<br rel="rm"><span class="errorMessage phone_error">Код подтверждения уже отправлен</span>');
	        			break;
	            	case "true":
	            		$(".acceptButton").after('<br rel="rm"><span class="errorMessage phone_error">Код подтверждения отправлен</span>');
		        		break;
	            	case "ok":
	                    $("#AppointmentToDoctors_phone").val(phone);
	                    $("#code").fadeOut(800);
	                    $(".acceptButton").fadeOut(800);
	                    $(".declineButton").fadeOut(800);
	                    $("#AppointmentToDoctors_phone_em_").remove();
	                    //setTimeout(function() {
	                        //if (typeof yaCounter18794458 != 'undefined') yaCounter18794458.reachGoal('phone_success');
	                        /*$(".acceptButton").after('<span class="btn-blue">Телефон подтвержден</span>');*/
	                    //}, 1000);
						$( "#appointment_main" ).addClass( "appointment_hidden" );
						$( "#appointment_wait_while_loading" ).removeClass( "appointment_hidden" );
		        		break;
        		}
            },
        });
    } else {
        $(".acceptButton").after('<br rel="rm"><span class="error phone_error">Введите код высланный на указанный телефон</span>');
    }
}

function removePhoneForAppointment() {
    $("#phoneSpan").html($("#default").val());
    $("#AppointmentToDoctors_phone").val($("#default").val());
    $(".removeButton").remove();
}

function removePhoneForAppointmentGuest() {
    if (confirm("Страница будет обновлена, введенные данные не будут сохранены. Продолжить?")) {
        location.href = location.href + "?removeGuestPhone=1";
    }
}

// Смена телефона при активации через 3 месяца
function changePhoneOnReactivation() {

    var phone = $(".new_phone").val();
    activation_sendCodeToUser();
}

function activation_sendCodeToUser(type) {
    $("span.error").html("");
    var type = $(".type");
    var phone = $(".new_phone").val();
    var new_phone = $(".new_phone");
    new_phone.css("border-color", "");
    $.ajax({
        url: '/ajax/reactivatePhone',
        async: false,
        data: ({
            type: type.val(),
            phone: phone,
        }),
        type: "GET",
        success: function(response) {
            switch (response) {
                case 'busy':
                    var errorMessage = "Данный номер занят";
                    break;
                case 'delay':
                    var errorMessage = "Вы уже запрашивали код менее 10 минут назад на другой телефон";
                    break;
            }
            if (type.val() == 'no') {
                if (response != "busy" && response != "delay") {
                    $(".activation_text").html("Введите код присланный на ваш мобильный телефон <strong>" + phone + "<strong>");
                    $(".showOnYes").show();
                    $("#PhoneVerification_code").focus();
                    $(".showOnNo").hide();
                } else {

                    new_phone.css("border-color", "red");
                    $("span.error").html(errorMessage);
                }
            }
            if (type.val() == 'yes') {

            }
            if (response == "already_sent") {
                var errorMessage = "Введите код присланный ранее";
                $("span.error").html(errorMessage);
            }
        },
    });
}

function activation_yes() {

    var type = $(".type");
    type.val("yes");
    var phone = $(".phoneReactivation").html();
    activation_sendCodeToUser();
    $(".action_buttons").hide();
    $(".showOnYes").show();
    $(".activation_text").html("Введите код присланный на ваш мобильный телефон <strong>" + phone + "<strong>");
    //высылаем код и показываем пользователю форму подтверждения кода   
}

function activation_no() {
    $(".showOnNo").show();
    var type = $(".type");
    type.val("no");
    $(".action_buttons").hide();
    var inputs = '<input type="text" class="custom-text new_phone showOnNo"/><br/>';
    $(".input_td br").remove();
    $("span.error").before(inputs);
    $(".new_phone").mask("+79999999999", {placeholder: "_"});
    $(".activation_text").html("Введите ваш мобильный телефон");
    //показываем форму ввода нового телефона       
}

function load_top_docs_containter(id, top_type, url) {
	form = $("#"+id);
	var container = form.find( ".top_" + top_type + "_containter");
	container.show();
	if(!container[0].isloaded) {
		container[0].isloaded = true;
		var top_doc_page = [];
		top_doc_page[top_type] = 0;
		container.find(".screen_loading").show();
		if(typeof container.find(".top_docs_wrapper ." + top_type + "_card")[(top_doc_page[top_type]+1)*2] == "undefined") {
			container.find(".icon_topdoctor_drop_right" ).css('visibility', 'hidden');
		} else {
			container.find(".icon_topdoctor_drop_right" ).css('visibility', 'visible');
		}
		if(typeof container.find(".top_docs_wrapper ." + top_type + "_card")[(top_doc_page[top_type]-1)*2] == "undefined") {
			container.find(".icon_topdoctor_drop_left" ).css('visibility', 'hidden');
		} else {
			container.find(".icon_topdoctor_drop_left" ).css('visibility', 'visible');
		}
		if(url != null) {
			$.ajax({
				url: url,
				data: { cardsOnly : true, topDocs : true },
				method: 'GET',
				success: function(data, textStatus, XHR) {
					var el = $( '<div></div>' );
	            	el.html(data);
	            	if(el.find(".doctor_card").length > 0 || el.find(".clinic_card").length > 0) {
	            		var new_header_section = el.find(".header_section")
	            		if(new_header_section.length > 0)
	            			form.find(".header_section").html(new_header_section.html());
						container.find(".top_docs_wrapper .top_docs_content").html(data);
					    container.find("#show_on_map,.show_on_map").not('.found').not('.close').click(function() {
					        $.fancybox({href: this.href});
					        return false;
					    }).addClass("found");
	            	} else {
	            		form.hide(500);
	            	}
				},
				error: function(XHR, textStatus, errorThrown) {
					form.hide(500);
				},
				complete: function() {
					SearchEnd();
					container.find(".screen_loading").hide();
					if(typeof container.find(".top_docs_wrapper ." + top_type + "_card")[(top_doc_page[top_type]+1)*2] == "undefined") {
						container.find(".icon_topdoctor_drop_right" ).css('visibility', 'hidden');
					} else {
						container.find(".icon_topdoctor_drop_right" ).css('visibility', 'visible');
					}
					if(typeof container.find(".top_docs_wrapper ." + top_type + "_card")[(top_doc_page[top_type]-1)*2] == "undefined") {
						container.find(".icon_topdoctor_drop_left" ).css('visibility', 'hidden');
					} else {
						container.find(".icon_topdoctor_drop_left" ).css('visibility', 'visible');
					}
				}
			});
		}

		container.find(".icon_topdoctor_drop_right" ).click(function() {
			top_doc_page[top_type] += 1;
			var targetCard = container.find(".top_docs_wrapper ." + top_type + "_card")[top_doc_page[top_type]*2];
			if(typeof targetCard == "undefined") {
				top_doc_page[top_type] -= 1;
				return false;
			}
			if(typeof container.find(".top_docs_wrapper ." + top_type + "_card")[(top_doc_page[top_type]+1)*2] == "undefined") {
				container.find(".icon_topdoctor_drop_right" ).css('visibility', 'hidden');
			}
			container.find(".icon_topdoctor_drop_left" ).css('visibility', 'visible');
			var targetPosition = targetCard.offsetLeft;
			var targetMarginLeft = parseInt($(targetCard).css('marginLeft'));
			if(!isNaN(targetMarginLeft)) targetPosition -= targetMarginLeft;
			container.find(".top_docs_wrapper" ).animate({
					scrollLeft: targetPosition,
				}, 400, function() {
			});
			return false;
		});
		container.find(".icon_topdoctor_drop_left" ).click(function() {
			top_doc_page[top_type] -= 1;
			var targetCard = container.find(".top_docs_wrapper ." + top_type + "_card")[top_doc_page[top_type]*2];
			if(typeof targetCard == "undefined") {
				top_doc_page[top_type] += 1;
				return false;
			}
			if(typeof container.find(".top_docs_wrapper ." + top_type + "_card")[(top_doc_page[top_type]-1)*2] == "undefined") {
				container.find(".icon_topdoctor_drop_left" ).css('visibility', 'hidden');
			}
			container.find(".icon_topdoctor_drop_right" ).css('visibility', 'visible');
			var targetPosition = targetCard.offsetLeft;
			var targetMarginLeft = parseInt($(targetCard).css('marginLeft'));
			if(!isNaN(targetMarginLeft)) targetPosition -= targetMarginLeft;
			container.find(".top_docs_wrapper" ).animate({
					scrollLeft: targetPosition,
				}, 400, function() {
			});
			return false;
		});
	}
}

function SearchStart() {
	$('#search_result_append_button').hide();
	$('#search_result_loading').show();
}
function SearchEnd(type) {
	try {
		$('#search_result_loading').hide();
		var search_found_count = parseInt($('.search-found-count').last().text());
		if(isNaN(search_found_count)) search_found_count = 0;
	    var page = parseInt($("#search_result_page_number").val());
		var pageCount = search_found_count/10;
		$('.menu_group_count.clinic_count').text(search_found_count);
		if(page < pageCount) $('#search_result_append_button').show();
		else $('#search_result_append_button').hide();
	} catch(e) { }

    $("#show_on_map,.show_on_map").not('.found').not('.close').click(function() {
        $.fancybox({href: this.href});
    	return false;
    }).addClass("found");

    $('.roll-more').not('.found').click(function() {
		var form = $( this ).closest( "div.information" );
		if($(this).hasClass('opened')) {
			form.find('.more').first().slideUp(500);
	        $(this).removeClass('opened');
		} else {
			form.find('.more').first().slideDown(500);
	        $(this).addClass('opened');
		}
    }).addClass("found");
    
    $('.show_all_addresses').not('.found').click(function() {
		var content = $( this ).closest( "div" ).find(".popup_content").html();
    	$.fancybox({
    		content: content,
    		width: "500px",
    		height: "500px"
    	});
    	return false;
    }).addClass("found");

    $('.show_all_reviews').not('.found').click(function() {
        var el = $( this ).closest( "div" ).find(".popup_content");
        var companyId = $(this).attr('data-company-id');

        var fancybox = function() {
            $.fancybox({
                    content: '<div style="max-height:550px;">' + el.html() + '</div>',
                    width: 500,
                    height: 550,
                    maxHeight: 550
                });
        };

        if(el.find('.review_block_item').length <= 0) {
            $.ajax({
                url: "/?r=ajax/showReviews",
                data: {
                    'attributes' : 
                        {
                            'companyId': companyId,
                        }
                },
                method:'GET',
                success: function(data, textStatus, XHR) {
                    if(data.length > 1) {
                            el.append(data);
                    }
                },
                error: function(XHR, textStatus, errorThrown) {
                    console.log(textStatus);
                },
                complete: function() {
                    fancybox();
                }
            });
        }
        else {
            fancybox();
        }
        return false;
    }).addClass("found");

	if($('.purchase-service-phone').not('.found').length > 0) { $('.purchase-service-phone').not('.found').mask("+7 (999) 999-99-99", {placeholder: "_"}).addClass("found"); }

    if(type=="refresh") {
		$('.onload_visibility').css('visibility', 'visible'); //avoid FOUC http://en.wikipedia.org/wiki/Flash_of_unstyled_content
	    $("#search_result_page_number").val("1");
    	try {
    		if($("#search_map").css('display') == 'block') {
				$(".search-result-head .show_on_map.found").hide();
				$(".search-result-head .show_on_map.close").show();
			}
			if($(".decription_action_block p").text().trim().length > 0) {
				$('#specialtyDescription').show();
			} else {
				$('#specialtyDescription').hide();
			}
	    	if($('meta[name="doctorSpecialtyId"]').attr('content').trim().length > 0 || $("#Search_metroStationId").val() !== null) {
	    		$(".SECTION.SEO").hide();
	    	} else $(".SECTION.SEO").show();
    	} catch(e) { }
    	try {
    		if($(".FIRST_SCREEN").length < 1) {
	            $(".left-filter .niceCheck, .search-row .niceCheck").not('.found').click(function() {
	            	var form = $(this).closest('form');
	            	form = (form.length > 0) ? form[0] : 0;
	                refreshSearch(0,form);
	            }).addClass("found");
	            $('.search-row select.select2-offscreen').not('.found').not('#Search_doctorSpecialtyId, #Search_companyServiceId').change(function() {
	            	if(!$(this).hasClass('disabled')) {
		            	var form = $(this).closest('form');
		            	form = (form.length > 0) ? form[0] : 0;
		                refreshSearch(0,form);
	            	}
	            }).addClass("found");
	            $(".left-filter select.select2-offscreen, .ajax_filter select.select2-offscreen, .ajax_filter input.offscreen, select.ajax_filter, .ajax_filter input[type='checkbox']").not('.found').change(function() {
	            	var attrId = $(this).attr('id').split('_');
	            	var name = attrId[1];
	                var form_input = $('#Search_' + name);
	                var value = null;
	                if($(this).attr("type") === "checkbox") {
	                	value = $(this).is(":checked") ? 1 : 0;
	                	if(attrId[2] == "not") value = Math.abs(value-1);
	                } else {
	                	value = $(this).val();
	                }
	                form_input.val(value);
	                if(!$(this).hasClass('disabled')) {
		            	var form = $(this).closest('form');
		            	form = (form.length > 0) ? form[0] : 0;
		                refreshSearch(0,form);
	                }
	            }).addClass("found");
	            $(".left-filter input.custom-text, .ajax_filter input.custom-text").not('.found').not('.disabled').each(function() {
	                var id = $(this).attr('id');
	                document.getElementById(id).oninput = function() {
	                    var txt = $("#" + id).val();
	                    if (txt.length > 2 || txt == "") {
                            if (txt == $("#" + id).val()) {
                                var name = id.split('_')[1];
                                if($('#Search_' + name).length > 0) {
                                    $('#Search_' + name).val(txt);
    	        	            	var form = $(this).closest('form');
    	        	            	form = (form.length > 0) ? form[0] : 0;
    	        	            	if(!(this.index > 0)) this.index = 1;
    	        	            	else this.index++;
        	                        setTimeout(function(self, index) {
        	                        	return function() {
	        	                        	if(self.index > index) return;
	        	                        	console.log(self.index);
	        	        	                refreshSearch(0,form);
        	                        	}
        	                        }(this, this.index), 750);
                                }
                            }
	                    }
	                }
	            }).addClass("found");
    		}
    	} catch(e) { }
    	try {
    		if(!$("#scroller").hasClass('found')) {
		        $(window).scroll(function() {
		            if (window.scrollY > 204) {
		                if ($("#scroller:visible").length == 0) {
		                    $("#scroller").fadeIn(150);
		                }
		            } else {
		                $("#scroller").fadeOut(150);
		            }
		        });
		        $("#scroller").not('.found').click(function() {
		            $('html, body').animate({scrollTop:0}, 'slow');
		            $("#scroller").fadeOut(500);
		        }).addClass("found");
    		}
    	} catch(e) { }

    	$.fancybox.init();
    }
	return false;
}
function appendSearch(obj, id, noPush, short) {
	SearchStart();
    /*Если поиск услуги и нету текста не обновлять*/
    id = id || 0;
    noPush = noPush || 0;
    
    var form;
    if (id) {
        form = $(id);
    } else if($('#isVkApp').val()) {
        form = $(".search-box form");
    } else {
        form = $(".search-box:visible form");
    }
    
	var typeSearch = form.find("#typeSearch");
	if(typeSearch.length > 0) { short = typeSearch.val(); }
	var noPushSearch = form.find("#noPushSearch");
	if(noPushSearch.length > 0) { noPush = noPushSearch.val(); }

    var url;
    if(location.pathname.split('/')[1] == "" || location.pathname.split('/')[1] == "index.php") {
    	url = location.pathname + location.search;
    } else {
	    $.ajax({
	        url: "/ajax/getCurrentSearchUrl",
	        async: false,
	        data: ({
	            'type': short ? short : ((location.pathname.split('/')[1] !== 'search') ? location.pathname.split('/')[1] : location.pathname.split('/')[2]),
	            'data': form.serializeArray(),
                'vk': $('#isVkApp').val()
	        }),
	        type: "POST",
	        success: function(response) {
	            url = response;
	        },
	    });
    }
    // запоминаем текущую страницу и их максимальное количество
    var page = parseInt($("#search_result_page_number").val());
    var pageCount;
    var loadingFlag = false;
    var container = $("#yw0");
    container.addClass("list-view-loading");
    // защита от повторных нажатий
    if (!loadingFlag)
    {
// выставляем блокировку
        loadingFlag = true;
        // отображаем анимацию загрузки
        //$('#loading').show();
        $.ajax({
            type: 'get',
            url: url,
            data: {
                // передаём номер нужной страницы методом POST
                'page': (page + 1),
                'cardsOnly': 1,
            },
            success: function(data)
            {
            	var el = $( '<div></div>' );
            	el.html(data);

                // увеличиваем номер текущей страницы и снимаем блокировку
                page++;
                $("#search_result_page_number").val(page);
                loadingFlag = false;
                // прячем анимацию загрузки
                container.removeClass("list-view-loading");

                // вставляем полученные записи после имеющихся в наш блок
                $('.search-result .items').append(el.find('.items').html());
            },
            complete: function() {
            	SearchEnd();
            }
        });
    }
    return false;
}

function refreshSearch(p, id, noPush, short) {
	SearchStart();
	$('.search-result').html("");
    /*Если поиск услуги и нету текста не обновлять*/
    id = id || 0;
    noPush = noPush || 0;

    var form;
    if (id) {
        form = $(id);
    } else if($('#isVkApp').val()) {
        form = $(".search-box form");
    } else {
        form = $(".search-box:visible form");
    }
    
	var typeSearch = form.find("#typeSearch");
	if(typeSearch.length > 0) { short = typeSearch.val(); }
	var noPushSearch = form.find("#noPushSearch");
	if(noPushSearch.length > 0) { noPush = noPushSearch.val(); }

    var url;
    if(location.pathname.split('/')[1] == "" || location.pathname.split('/')[1] == "index.php") {
    	url = location.pathname + location.search;
    } else {
	    $.ajax({
	        url: "/ajax/getCurrentSearchUrl",
	        async: false,
	        data: ({
	            'type': short ? short : ((location.pathname.split('/')[1] !== 'search') ? location.pathname.split('/')[1] : location.pathname.split('/')[2]),
	            'data': form.serializeArray(),
                'vk': $('#isVkApp').val()
	        }),
	        type: "POST",
	        success: function(response) {
	            url = response;
	        },
	    });
    }

    // запоминаем текущую страницу и их максимальное количество
    var page = parseInt(p);
    $("#search_result_page_number").val(page);
    var pageCount;
    var loadingFlag = false;
    var container = $("#yw0");
    container.addClass("list-view-loading");
    // защита от повторных нажатий
    if (!loadingFlag)
    {
// выставляем блокировку
        loadingFlag = true;
        // отображаем анимацию загрузки
        //$('#loading').show();
        $.ajax({
            type: 'get',
            url: url,
            data: {
                // передаём номер нужной страницы методом POST
                'page': (page + 1)
            },
            success: function(data)
            {
            	var el = $( '<div></div>' );
            	el.html(data);

            	if($('.doctor_section_content').length === 0) {
                	var titleText = $('[name="titleText"]', el);
                	if(titleText.length > 0) document.title = titleText.attr("content");
                	
                    if (!noPush) {
                    	try { history.pushState({ 'title': document.title, 'type':'refreshSearch' }, null, url); } catch(e) {}
                    }
                    
                	$('meta[name="doctorSpecialtyId"]').replaceWith(el.find('meta[name="doctorSpecialtyId"]'));
                	$('meta[name="description"]').replaceWith(el.find('meta[name="description"]'));
                	
                	$('#specialtyDescription',document).html($('#specialtyDescription', el).html());
            	}
                // увеличиваем номер текущей страницы и снимаем блокировку
                page++;
                $("#search_result_page_number").val(page);
                loadingFlag = false;
                
                // прячем анимацию загрузки
                container.removeClass("list-view-loading");
                $('.SEO2').replaceWith(el.find('.SEO2'));
                $('.search-result-head a.show_on_map.found').attr('href', el.find('.search-result-head a.show_on_map.found').attr('href'));
                // вставляем полученные записи после имеющихся в наш блок
                $('.search-found').replaceWith(el.find('.search-found'));
                $('.search-result').html(el.find('.search-result').html());
                $('.TEXT_PAGE').replaceWith(el.find('.TEXT_PAGE'));
                $('.SECTION.all-spec').html(el.find('.SECTION.all-spec').html());
                $('.SearchController .crumbs').html(el.find('.crumbs').html());
            },
            complete: function() {
            	SearchEnd("refresh");
            	GetSearchParameters(function(){});
            }
        });
        LoadMap(url);
    }
    return false;
}
function LoadMap(url) {
	if($("#search_map").css('display') == 'block') {
        if(url.indexOf('?') == -1) url += "?showAll=1&showOnMap=1";
        else url += "&showAll=1&showOnMap=1";
        $.ajax({
            url: url,
            data: ({
                'name': $("#search-service-form_text").val()
            }),
            type: "GET",
            success: function(response) {
                $("#search_map").html(response);
            },
            error: function (error) {
				$("#search_map").hide();
				$(".search-result-head .show_on_map.close").click();
            }
        });
	}
}

function scrollToId(id) {
    $('html,body').animate({scrollTop: $(id).offset().top - 10}, 500);
}

function sendPoll() {
    $('.one-poll input:checked').each(function() {
        var pollId = $(this).parent().attr('id');
        var pollOptionId = $(this).val();

        $.ajax({
            url: '/ajax/addPoll',
            dataType: 'JSON',
            data: ({
                'pollId': pollId,
                'pollOptionId': pollOptionId
            }),
            async: false,
            type: "POST",
            success: function(response) {

            },
        });
    });
}

function hideDropDowns(dd) {
    /* dd - dropdown */
	/*
    if (dd) {
        switch (dd) {
            case 'clinic':
                $(".dropdown-menu:visible").slideUp(100);
                break;
            case 'login':
                $(".log-in:visible").slideUp(100);
                break;

        }
        return true;
    } else {
        $(".log-in:visible").slideUp(100);
        $(".dropdown-menu:visible").slideUp(100);
    }
    */
}

var subDomains = [
	'moskva', 'nizhny-novgorod', 'novosibirsk', 'ekaterinburg', 'kazan', 'local',
	'samara', 'chelyabinsk', 'omsk', 'rostov-na-donu', 'ufa', 'krasnoyarsk', 
	'perm', 'volgograd', 'voronezh', 'irkutsk', 'saratov', 'tyumen', 'vladivostok', 'yaroslavl'
	]; //rm rf

function removeSubdomains(text) {
	
	for(key in subDomains) {
		text = text.replace(subDomains[key] + '.', '');
	}
	return text;
}

function setRegion(regionSubdomain) {
	
	var regionUsedInUrl = function() {
			for(key in subDomains) {
				if (window.location.hostname.indexOf(subDomains[key]) > -1) return subDomains[key];
			}
			return false;
		};
	
	var ajaxRequestUrl = window.location.protocol + '//' + window.location.hostname + '/site/setRegion';
	
	$.ajax({
		type: 'get',
		url: ajaxRequestUrl,
		data: {
			'regionSubdomain': regionSubdomain
		},
		success: function(data)
		{
			var subDomain,
				newLocation,
				newHostname,
				currentUsedSubDomain;
				
			subDomain = (data == 'spb') ? '' : data + '.';
			//subDomain = (data["subDomain"] == 'spb') ? '' : data["subDomain"] + '.';			
			currentUsedSubDomain = regionUsedInUrl();
			newHostname = window.location.hostname;
			newHostname = subDomain + removeSubdomains(newHostname);			
			newLocation = window.location.protocol + '//' + newHostname;
			if(		window.location.pathname.substring(0,17).toLowerCase() !== "/handbook/disease"
				&&	$('.doctor_section_content, .clinic_section_content').length < 1) newLocation += window.location.pathname + window.location.search;
			
			window.location.assign(newLocation);
		}
	});
}

function PrintElem(elem)
{
	data = $(elem).html();
    var mywindow = window.open('', 'Талон на приём к врачу', 'height=800,width=1000');
    mywindow.document.write('<html><head><title></title>');
	mywindow.document.write("<style type='text/css'>");
	mywindow.document.write(".item_name { width: 200px; display: inline-block; }");
	mywindow.document.write(".item_value { font-weight: bolder; }");
 	mywindow.document.write("</style>");
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<asp:Image ID="Image1" runat="server" ImageUrl="http://emportal.ru/images/newLayouts/LOGO-MAIN.png" Width="240px" Height="72px" />');
    mywindow.document.write('<h1>Единый Медицинский Портал</h1>');
    mywindow.document.write(data);
    mywindow.document.write('<br><a href="#">http://emportal.ru</a>');
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}

function showPurchaseStub() {

	//var $purchaseButtons = $('.btn-purchase');
	//$purchaseButtons.hide();
	
	//var $purchaseButton = $(this);
	//console.log('$purchaseButton');
	//console.log($purchaseButton);
	
	//надо отметить цель
	//countersManager.reachGoal({
	//	"forCounters" : ["yandex"],
	//	"goalName" : "service_purchase_attempt",
	//	"callback" : function() {
	//		alert('Спасибо за вашу активность! Функционал находится в разработке');
	//	}
	//});
}
function escapeStr(str) 
{
    if (str)
        return str.replace(/([ #;?%&,.+*~\':"!^$[\]()=>|\/@])/g,'\\$1');      

    return str;
}
function isIE () {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
function clearSelection() {
    if ( document.selection ) {
        document.selection.empty();
    } else if ( window.getSelection ) {
        window.getSelection().removeAllRanges();
    }
}

function pausecomp(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
}

function openSpecialtySelect() {
    var $input = $("#Search_doctorSpecialtyId_shortSearch");
	if($input.length < 1) {
        $input = $("#Search_doctorSpecialtyId");
	}
    $input.select2('open');
    return true;
}

$.fn.overflown=function(){
	var e=this[0];
	return e.scrollHeight>e.clientHeight||e.scrollWidth>e.clientWidth;}

function nextPageOpenContent(from) {
	$(from + " > *").detach().appendTo(".next_content");
	$(".main_content").hide();
	$(".search-box").hide();
	$('.mobilecrumbs > *:last-child').replaceWith("<a href='javascript:void(0)' data-from='"+from+"' onclick='lastPageClose()'>"+$('.mobilecrumbs > *:last-child').text()+"</a>");
	$('.mobilecrumbs').append("<span> &gt; </span>");
	$('.mobilecrumbs').append("<span>Назад</span>");
	window.scrollTo(0, 0);
	$(".next_content").show();
}
function lastPageClose() {
	$(".next_content > *").detach().appendTo($('.mobilecrumbs > a:last-of-type').data('from'));
	$(".next_content").hide();
	$(".search-box").show();
	$('.mobilecrumbs > *:last-child').remove();
	$('.mobilecrumbs > *:last-child').remove();
	$('.mobilecrumbs > *:last-child').replaceWith("<span>"+$('.mobilecrumbs > *:last-child').text()+"</span>");
	window.scrollTo(0, 0);
	$(".main_content").show();
}
function resetFilters() {
	$('.ajax_filter .niceChecked').removeClass("niceChecked");
	$('.ajax_filter input:checked').removeProp("checked");
	$('.ajax_filter input, .ajax_filter select').val(null);
	$('.ajax_filter input, .ajax_filter select').select2("val", "").change();
}

function isMobile() {
	return ($(window).width() < 1100);
}

function infinityList(objSender, id) {
	id = typeof id !== 'undefined' ? id : null;
	if($(objSender).is(':hidden')) {
		return false;
	}
	if(isNaN(parseInt( $.fn.yiiListView.settings[id]["page"] ))) {
	    $.fn.yiiListView.settings[id]["page"] = 2;
	}
	$(objSender).hide();
    if($(objSender).data("pagecount") < $.fn.yiiListView.settings[id]["page"]) {
        return false;
    }
	$.fn.yiiListView.update(id, { url:$.fn.yiiListView.getUrl(id), data:{News_page:$.fn.yiiListView.settings[id]["page"]}, success:function(data){
	    $("#"+escapeStr(id)+" .items").append($(data).find('.items').html());
	    $("#"+escapeStr(id)+" .keys").append($(data).find('.keys').html());
	    $.fn.yiiListView.settings[id]["page"] += 1;
	    if($(objSender).data("pagecount") >= $.fn.yiiListView.settings[id]["page"]) {
	    	$(objSender).show();
	    }
	}, complete: function(){$("#"+escapeStr(id)).removeClass("list-view-loading");} });
}


$('.filtr-ms').click(function(){
    var block = $('.short-search-box');
    if(block.hasClass('show-block')){
        block.fadeOut().removeClass('show-block');
    }else{
        block.fadeIn().addClass('show-block');
    }

});