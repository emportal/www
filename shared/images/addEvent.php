<?php
/* @var $this CampaignController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Клиенты' => 'client/index',
	'Компании',
);

$this->menu=array(
	array('label'=>'Create Campaign', 'url'=>array('create')),
	array('label'=>'Manage Campaign', 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript("toggleAlgorithm",'
	$(".toggleAlgorithm").click(function() {
	var btn = $(this);
	var id = $(this).data("id");
	var status = 0;
	if(btn.hasClass("btn-success")) {		
		status = 1;
		btn.html("Выключить");
		btn.removeClass("btn-success");		
	} else {		
		btn.html("Включить");
		btn.addClass("btn-success");
	}
	$.ajax({
		"type":"GET",
		"url":"/ajax/setAlgorithmStatus",
		"data":{
			"campaignId": id,
			"status": status
		},
		"cache":false
	});
});
		');
?>

<h1>Добавление нового события</h1>
<?php
$form = $this->BeginWidget('CActiveForm', array(	
	'method'				 => 'post',		
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('class'=>''),
));
?>
<script>
	$(document).ready(function() {
		$("#Event_typeCount").change(function() {
			var stat = $(this).val();
			if( stat == 1) {
				$(".one").show();
				$(".cycle").hide();
			} else if(stat == 2) {
				$(".cycle").show();
				$(".one").hide();
			} else {
				$(".cycle").hide();
				$(".one").hide();
			}
		});	
	});	
</script>
<table class="table table-striped table-bordered table-condensed">
	<tr>
		<td>Тип события</td>
		<td><?= $form->dropDownList($model,'type',array(0=>'Не выбрано',Event::TYPE_START => 'Старт',Event::TYPE_STOP => 'Стоп')); ?></td>
	</tr>
	<tr>
		<td>Количество повторений</td>
		<td><?= $form->dropDownList($model,'typeCount',array(0=>'Не выбрано',Event::TYPECOUNT_ONE => 'Однократно',Event::TYPECOUNT_CYCLE => 'Многократно')); ?></td>
	</tr>
	<tr class="one" style="<?=$model->typeCount == Event::TYPECOUNT_ONE ? '' : 'display:none';?>">
		<td>Дата и время</td>
		<td>
			<?php 
$form->widget('JuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'datetime',
					'language' => Yii::app()->language,
					'options' => array(
						'dateFormat'=>'yy-mm-dd',
						'timeFormat'=>'HH:mm',						
						'showSecond'=>null,
						'changeMonth'=>false,
						'changeYear'=>false,
						'tabularLevel'=>null,
						'timeOnly' => false,
					),
					'htmlOptions' => array(						
						'class' => 'custom-text',						
					),
						)
				);
?>
		</td>
	</tr>
	<tr class="cycle" style="<?=$model->typeCount == Event::TYPECOUNT_CYCLE ? '' : 'display:none';?>">
		<td>Время</td>
		<td>
			<?php
				$this->widget('JuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'time',
					'language' => Yii::app()->language,
					'options' => array(						
						'timeFormat'=>'HH:mm',						
						'showSecond'=>null,
						'changeMonth'=>false,
						'changeYear'=>false,
						'tabularLevel'=>null,
						'timeOnly' => true,
					),
					'htmlOptions' => array(						
						'class' => 'custom-text',						
					),
						)
				);
				?>
		</td>
	</tr>
	<tr class="cycle" style="<?=$model->typeCount == Event::TYPECOUNT_CYCLE ? '' : 'display:none';?>">
		<td>День недели</td>
		
		<td><?= $form->dropDownList($model,'weekday',Event::$weekdays); ?></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<button class="btn btn-success" type="submit" title="Сохранить">Сохранить</button>
		</td>
	</tr>
</table>




<?php 
$this->EndWidget();
?>