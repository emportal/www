<?php
error_reporting(E_ALL ^ E_NOTICE);
mb_internal_encoding("UTF-8");
mb_regex_encoding('UTF-8');
header('Content-Type: text/html; charset=UTF-8',true);

// change the following paths if necessary
if ( extension_loaded('apc') )
	$yii = dirname(__FILE__) . '/../framework/yiilite.php';
else
	$yii = dirname(__FILE__) . '/../framework/yii.php';

if ( file_exists(dirname(__FILE__) . '/../local') || file_exists(dirname(__FILE__) . '/../local.txt' )  ) {
	$config = dirname(__FILE__) . '/protected/config/local.php';
	$yii = dirname(__FILE__) . '/../framework/yii.php';

	// remove the following lines when in production mode
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	// specify how many levels of call stack should be shown in each log message
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

	define('YII_ENABLE_ERROR_HANDLER', true);
	define('YII_ENABLE_EXCEPTION_HANDLER', true);
}
else
	$config = dirname(__FILE__) . '/protected/config/main.php';

if(strpos($_SERVER['HTTP_HOST'], 'dev1.emportal.ru') !== false) {
	$config = dirname(__FILE__) . '/protected/config/main_dev1.php';
}
/*
if($_SERVER['HTTP_REMOTE_ADDR']=='195.182.138.46') {
	$config = dirname(__FILE__) . '/protected/config/main_test.php';
}*/

require_once($yii);
Yii::$enableIncludePath = false;
Yii::createWebApplication($config)->run();
