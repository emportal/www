User-agent: Yandex
Disallow: /register
Disallow: /login
Disallow: /recovery
Disallow: /terms_of_use.html
Disallow: /site/contact
Disallow: /ajax/
Host: <?= $_SERVER['HTTP_HOST'] ?>

User-agent: *
Disallow: /register
Disallow: /recovery
Disallow: /login
Disallow: /terms_of_use.html
Disallow: /site/contact
Disallow: /ajax/

Sitemap: http://<?= $_SERVER['HTTP_HOST'] ?>/sitemap.xml