<?php

class SendClaimsCommand extends CConsoleCommand
{
	public function run($args)
    {
		error_reporting(FALSE);
		ob_start();
        //1. получаем клиники с долгами
		$debtData = Address::getDebtDataForAllAddresses(true); //с пропуском счетов по абонентской плате, предполагая, что они всегда оплачиваются заранее
        
        //2. рассылаем претензии
        foreach($debtData['addresses'] as $index => $address) {
        	//if (empty($address->legalName)) {
			//	echo "<continue>";
            //    continue;
            //}
            $mailList = $address->getMailListForAppReports();
            $sendResult = $address->sendClaimsViaEmail($mailList);
            echo $address->legalName ." (".implode(',', $mailList).") ". ($sendResult ? '<OK>': '<NOT OK>') . PHP_EOL;
            //дубль на техническое мыло
            $mailList = ['roman.z@emportal.ru'];
            $address->sendClaimsViaEmail($mailList);
        }
        
        //  Return the contents of the output buffer
        $htmlStr = ob_get_contents();
        // Clean (erase) the output buffer and turn off output buffering
        ob_end_clean();
        
        $pathToFile = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . '';
        $fileName = 'sendClaim.log';
        $fullPathToFile = $pathToFile . $fileName;
        // Write final string to file
        file_put_contents($fullPathToFile, $htmlStr);
        echo $htmlStr;
	}
}
