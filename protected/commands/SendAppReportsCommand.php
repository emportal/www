<?php

class SendAppReportsCommand extends CConsoleCommand
{
	public function run($args) {
		error_reporting(FALSE);
		Address::$showInactive = true;
        //Company::$showRemoved = true;
		$model = AppointmentToDoctors::model();
		$model->visit_time_period = MyTools::getDateInterval('lastMonth');
		$dtOptions = [
			'manStatus' => 'r',
			//'includeFixedContracts' => 'true',
			'excludedAppType' => AppointmentToDoctors::APP_TYPE_SAMOZAPIS,
			'pageSize' => 999999,
			'ignoreRights' => 'true',
			//'companyId' => 'EMPORTAL-4864-98cb-39fa-79af7baa42a2', //тестовая клиника ("совушек и котят")
			'groupByAddressId' => 'true',
			'showServices' => 'withVisits',
		];
		$dataProvider = $model->dataProviderForNewAdminPanel($dtOptions);
		foreach($dataProvider->getData() as $data)
		{
			$clinic = $data->address;
			if (!is_object($clinic))
				continue;
			
			$mailList = $clinic->mailListForAppReports;
			//if(/*in_array($clinic->company->name, ['Клиника совушек и котят']) && */mt_rand(0,30) > 29) {
				//$mailList = ['a.f.dyachkov@gmail.com'];
				//var_dump($clinic->company->name);
				//var_dump($clinic->shortName);
				//var_dump($mailList);
				$clinic->sendAppReportsViaEmail($mailList);
			//}
		}
	}
}
