<?php

class UpdateCompanyTableCommand extends CConsoleCommand {

	public function run($args) {

		error_reporting(FALSE);

		// update removalFlag(1), if doctors == 0
		$criteria = new CDbCriteria();
		$criteria->with = ['address', 'companyType'];
		$criteria->compare('t.removalFlag', 0);
		$criteria->compare('address.samozapis', 1);
		$criteria->compare('companyType.isMedical', 1);

		$allCompany = Company::model()->findAll($criteria);
		foreach ($allCompany as $company) {
			$doctors = Doctor::model()->findAll('currentPlaceOfWorkId =:currentPlaceOfWorkId', ['currentPlaceOfWorkId' => $company->id]);
			if (count($doctors) == 0) {
				$company->removalFlag = 1;
				$company->update();
			}
		}

		// update removalFlag(0), if doctors > 0 
		$criteria = new CDbCriteria();
		$criteria->with = ['address', 'companyType'];
		$criteria->compare('t.removalFlag', 1);
		$criteria->compare('address.samozapis', 1);
		$criteria->compare('companyType.isMedical', 1);

		Company::$showRemoved = true;
		$allCompany = Company::model()->findAll($criteria);
		foreach ($allCompany as $company) {
			$doctors = Doctor::model()->findAll('currentPlaceOfWorkId =:currentPlaceOfWorkId', ['currentPlaceOfWorkId' => $company->id]);
			if (count($doctors) > 0) {
				$company->removalFlag = 0;
				$company->update();
			}
		}
	}
}