<?php

class DeletingOldLogCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
    	
    	$criteria = new CDbCriteria;
    	$criteria->addCondition('datetime < (now() - INTERVAL 3 MONTH)','AND');
    	$log = LogAddress::model()->deleteAll($criteria);
    	
    	$criteria = new CDbCriteria;
    	$criteria->addCondition('datetime < (now() - INTERVAL 3 MONTH)','AND');
    	$log = LogDoctor::model()->deleteAll($criteria);
    	
    	$criteria = new CDbCriteria;
    	$criteria->addCondition('datePublications < (now() - INTERVAL 3 MONTH)','AND');
    	$log = LogAgr::model()->deleteAll($criteria);
    	
    	exit;
    }
}

