<?php

class UpdateMainAddressCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
    	$companies = Company::model()->findAll(Company::ActiveClinicsCriteria());
    	foreach ($companies as $company) {
    		if (($company->address->userMedicals->agreementNew != 1 || !$company->address->isActive)) {
    			$oldAddress = $company->address;
	    		foreach ($company->addresses as $address) {
    				if (($address->userMedicals->agreementNew == 1 && $address->isActive)) {
    					$company->addressId = $address->id;
    					if($company->update()) {
	    					$log = [
	    						'time' => date('Y-m-dTH:i:s'),
	    						'companyId' => $company->id,
	    						'addressId_old' => $oldAddress->id,
	    						'addressId_new' => $company->addressId,
	    					];
	    					Yii::log(CJSON::encode($log), 'log', 'updateMainAddress');
	    					echo $company->name . PHP_EOL;
		    				echo " -- Old Main Address: " . $company->address->name . PHP_EOL;
		    				echo " -- New Main Address: " . $address->name . PHP_EOL;
    					}
	    				break;
    				}
	    		}
    		}
    	}
    }
}

