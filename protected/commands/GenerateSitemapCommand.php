<?php

class GenerateSitemapCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		$host = Yii::app()->params['baseUrl'];
		$host = str_replace("http://", "https://", $host);
		Address::$showInactive = true;
		$regionalLinks = [];
		foreach (Yii::app()->params['regions'] as $region) {
			$regionalLinks[$region['subdomain']] = [];
		}

		$regionalLinks["spb"][] = ["loc" => $host . '/handbook/disease', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/terms_of_use.html', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/article', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/site/contact', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/privacy_policy.html', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/about_us.html', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/pressa.html', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/news', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/team.html', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/pressa.html', "priority" => "0.7", "changefreq" => "yearly"];
		$regionalLinks["spb"][] = ["loc" => $host . '/vozmozhnosti_dlya_klinik/forma_zapisi_online', "priority" => "0.7", "changefreq" => "yearly"];

		$news = News::model()->published()->findAll();
		foreach ($news as $n) {
			$regionalLinks["spb"][] = ["loc" => ($host.'/news/'.$n->link.'.html')/*, "lastmod" => date("Y-m-d",strtotime($n->datePublications)), */, "priority" => "0.7", "changefreq" => "never"];
		}
		foreach (Yii::app()->params['regions'] as $region) {
			$city = City::model()->findByAttributes(["subdomain" => $region['subdomain']]);
			if($city) {
				$regionKeys = [];
				$regionKeys[] = $region['subdomain'];
				if($region['samozapis']) { $regionKeys[] = $region['subdomain'] . DIRECTORY_SEPARATOR . "samozapis"; }
				$metroAndDistrictOfCity = array_merge(Search::GetSearchParametersMetro($city->id),Search::GetSearchParametersDistrict($city->id));
				$serviceOfCity = Search::GetSearchParametersService($city->id);
				foreach ($regionKeys as $regionKey) {
					$samozapis = $region['samozapis'] && (strpos(DIRECTORY_SEPARATOR . "samozapis", $regionKey) > 0);
					$regionHost = str_replace('https://', "https://".(($regionKey != 'spb')?($regionKey."."):(($samozapis) ? "samozapis." : "")) , $host);
					$regionalLinks[$regionKey][] = ["loc" => $regionHost.'/', "priority" => "0.9", "changefreq" => "daily"];

					$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/doctor'), "priority" => "0.8", "changefreq" => "daily"];
					$criteria = new CDbCriteria();
					$criteria->compare('t.cityId', $city->id);
					$criteria->compare('t.samozapis', ($samozapis) ? 1 : 0);
					$criteria->group = "t.id";
					$searchDoctorSpecialty = ShortSpecialtyOfDoctor::model()->findAll($criteria);
					foreach ($searchDoctorSpecialty as $n) {
						$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/doctor/specialty-'.$n->linkUrl), "priority" => "0.8", "changefreq" => "daily"];
						$Parameters = json_decode($n->Parameters);
						if(is_object($Parameters) AND is_array($Parameters->Metro) AND is_array($Parameters->District)) {
							$metroAndDistrict = array_merge($Parameters->Metro,$Parameters->District);
							foreach ($metroAndDistrict as $linkUrl) {
								if($linkUrl === 'metro-' OR $linkUrl === 'raion-') continue;
								$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/doctor/'.$linkUrl.'/specialty-'.$n->linkUrl), "priority" => "0.8", "changefreq" => "daily"];
							}
						}
					}
					unset($searchDoctorSpecialty);

					$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/klinika'), "priority" => "0.7", "changefreq" => "daily"];
					$searchClinicSpecialty = CompanyActivite::model()->findAll('t.noindex = :noindex', array(":noindex" => 0));
					foreach ($searchClinicSpecialty as $n) {
						$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/klinika/profile-'.$n->linkUrl), "priority" => "0.7", "changefreq" => "weekly"];
						foreach ($metroAndDistrictOfCity as $linkUrl=>$name) {
							$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/klinika/'.$linkUrl.'/profile-'.$n->linkUrl), "priority" => "0.7", "changefreq" => "weekly"];
						}
						
					}
					unset($searchClinicSpecialty);

					if($regionKey === 'spb') {
						$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/diagnostics'), "priority" => "0.7", "changefreq" => "weekly"];
						$serviceLinkUrlArr = [];
						foreach ($serviceOfCity as $serviceLinkUrl=>$name) {
							$serviceLinkUrlArr[] = preg_replace("/service-/", "", $serviceLinkUrl, 1);
						}
						$criteria = new CDbCriteria;
						$criteria->with = [
							'service' => [
								'together' => true,
							],
							'address' => [
								'together' => true,
								'with' => [
									'userMedicals' => [
										'together' => true,
									],
									'company' => [
										'together' => true,
									],
								],
							],
						];
						$criteria->addInCondition('service.linkUrl',$serviceLinkUrlArr);
						$criteria->compare("userMedicals.agreementNew", 1);
						$criteria->compare("address.cityId", $city->id);
						$criteria->compare("address.samozapis",($samozapis) ? 1 : 0);
						$modelAddressServices = AddressServices::model()->findAll($criteria);
						$diagnosticLinks = [];
						foreach ($modelAddressServices as $addressservice) {
							$metroAndDistrict = [];
							foreach ($addressservice->address->metroStations as $metroStation) {
								$metroAndDistrict[] = 'metro-'.$metroStation->linkUrl;
							}
							$metroAndDistrict[] = 'raion-'.$addressservice->address->cityDistrict->linkUrl;
							foreach ($metroAndDistrict as $metrolinkUrl) {
								if($metrolinkUrl === 'metro-' OR $metrolinkUrl === 'raion-') continue;
								$loc = ($regionHost.'/diagnostics/'.$metrolinkUrl.'/'.$addressservice->service->linkUrl);
								if(!in_array($loc, $diagnosticLinks)) {
									$diagnosticLinks[] = $loc;
									$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/diagnostics/'.$metrolinkUrl.'/service-'.$addressservice->service->linkUrl), "priority" => "0.7", "changefreq" => "weekly"];
								}
							}
						}
						unset($modelAddressServices);
					}
				}
				unset($metroAndDistrictOfCity,$serviceOfCity);
			}
			unset($city);
		}

		$sModel = new Search();
		$criteria = new CDbCriteria();
		$criteria->select = 't.id,t.name,t.link,t.linkUrl,t.samozapis';
		$criteria->with = [
			'company' => [
				'select' => 'id,name,linkUrl',
				'together' => true,
				'condition' => "`company`.`categoryId` = 'a3969945e0f59fa84e0e7740c2e8cc87' OR `company`.`categoryId` = '8f7f0e92e866321d4ac5f2a5378a4668'",
				'with' => [
					/*
					'companyContracts'=> [
						'select' => false,
						'together' => true,
						'joinType' => 'INNER JOIN'
					],
					*/
					'companyType' => [
						'select' => 'id,name',
						'together' => true
					]
				]
			],
			'doctors' => [
				'select' => 'id,link,linkUrl',
				'together' => true
			],
			/*
			'userMedicals' => [
				'select' => false,
				'joinType' => 'INNER JOIN',
				'together' => true
			],
			*/
			'city' => [
				'select' => 'subdomain',
				'joinType' => 'INNER JOIN',
				'together' => true
			]
		];
		//$criteria->compare('t.isActive', 1);
		//$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->compare('company.removalFlag', 0);
		$criteria->compare('companyType.isMedical', 1);

		$clinics = Address::model()->findAll($criteria);
		error_reporting(TRUE);
		foreach ($clinics as $cl) {
			if(!empty($cl->city->subdomain)) {
				$regionHost = str_replace('https://', "https://".(($cl->city->subdomain != 'spb')?($cl->city->subdomain."."):(($cl->samozapis == 1) ? Yii::app()->params['regions'][$cl->city->subdomain]['samozapisSubdomain']."." : "")) , $host);
				$regionKey = $cl->city->subdomain . (($cl->samozapis == 1) ? (DIRECTORY_SEPARATOR . "samozapis") : "");
				$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/klinika/'.$cl->company->linkUrl.'/'.$cl->linkUrl), "priority" => "0.7", "changefreq" => "weekly"];
				foreach ($cl->doctors as $doctor) {
					$regionalLinks[$regionKey][] = ["loc" => ($regionHost.'/klinika/'.$cl->company->linkUrl.'/'.$cl->linkUrl.'/'.$doctor->linkUrl), "priority" => "0.7", "changefreq" => "weekly"];
				}
			}
		}

		$basepath = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;

		foreach ($regionalLinks as $regionKey=>$links) {
			$path = $basepath . 'uploads' . DIRECTORY_SEPARATOR . 'regions' . DIRECTORY_SEPARATOR . $regionKey . DIRECTORY_SEPARATOR;
			echo $regionKey . " :: " . self::writeContent($path,$links) . PHP_EOL;
		}
	}

	protected static function writeContent($path,$links) {
		mkdir($path);
		$filename = $path . 'sitemap.xml';
		$content  = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
		$content .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
		foreach ($links as $link) {
			// if(self::getStatusPage($link['loc']) != '200') {
			// 	continue;
			// }
			$content .= "\t<url>\n";
			foreach ($link as $key=>$value) {
				$content .= "\t\t<".$key.">" . $value . "</".$key.">\n";
			}
			$content .= "\t</url>\n";
		}
		$content .= '</urlset>';
		
		$handle = fopen($filename, 'w'); // Открываем файл только для записи; помещаем указатель в начало файла и обрезаем файл до нулевой длинны. Если файл не существует - пробует его создать..
		if (!$handle) {
			return "Не могу открыть файл ($filename)";
		} else {
			if(!is_writable($filename)) {
				return "Не доступен для записи файл ($filename)";
			} else {
				if (fwrite($handle, $content) === FALSE) { // Записываем $content в наш открытый файл.
					return "Не могу произвести запись в файл ($filename)";
				}
				else {
					fclose($handle);
					return "OK";
				}
			}
		}
	}

	protected static function getStatusPage($url) {
		$status = 404;
	  	if($curl = curl_init() ) {
		    curl_setopt($curl,CURLOPT_URL,$url);
		    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($curl,CURLOPT_NOBODY,true);
		    curl_setopt($curl,CURLOPT_HEADER,true);
		    curl_exec($curl);
		    $status = curl_getinfo($curl,CURLINFO_HTTP_CODE );
		    curl_close($curl);
	  	}
	  	return $status;
	}
}
