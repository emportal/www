<?php

class RemindToPayCommand extends CConsoleCommand
{
	public function run($args)
	{
		error_reporting(FALSE);
        if (((int) date('w')) !== 2 OR ((int) date('j')) < 15)
            return false;
		
        //1. выборка клиник-неплательщиков
        $debtDataForAllAddresses = Address::getDebtDataForAllAddresses(true);
		$addresses = $debtDataForAllAddresses['addresses'];
		
		//2. сделать рассылку уведомлений неплательщикам
		foreach($addresses as $address)
		{
			$mailList = $address->mailListForReminderToPay;
			$debtData = $address->debtData;
			$allReportMonths = [];
			if(isset($debtData['notPayedBills'])) {
				$notPayedBills = $debtData['notPayedBills'];
				if(is_array($notPayedBills)) {
					foreach ($notPayedBills as $notPayedBill) {
						if(isset($notPayedBill['reportMonth'])) {
							$allReportMonths[] = $notPayedBill['reportMonth'];
						}
					}
				}
			}
			if (!empty($mailList)) {
				$address->sendReminderToPayViaEmail($mailList, $allReportMonths);
			}
            //для контроля
            $address->sendReminderToPayViaEmail(['alex.f.d@list.ru'], $allReportMonths);
			//$address->sendReminderToPayViaEmail(['alexander.d@emportal.ru'], $allReportMonths);
		}
		
		//3. для отладки и тестов
		//var_dump([
		//	'reportMonth' => $reportMonth,
		//	'billsCount' => count($bills),
		//]);
	}
}
