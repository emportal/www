<?php

class GenerateBillsCommand extends CConsoleCommand
{
	public function run($args) {
		error_reporting(FALSE);
		Address::$showInactive = true;
		Company::$showRemoved = true;
		
		$model = AppointmentToDoctors::model();
		$model->visit_time_period = MyTools::getDateInterval('lastMonth');
		
		$reportMonth = MyTools::convertIntervalToReportMonth($model->visit_time_period);
		print_r([
		'reportMonth' => $reportMonth,
		'$model->visit_time_period' => $model->visit_time_period,
		]);
		
		//rf: address->getInfoForAppointmentReports
		$dtOptions = [
			'manStatus' => 'r',
			'excludedAppType' => AppointmentToDoctors::APP_TYPE_SAMOZAPIS,
			'pageSize' => false,
			'ignoreRights' => 'true',
			'groupByAddressId' => 'true',
			'showServices' => 'withVisits',
		];
		$dataProvider = $model->dataProviderForNewAdminPanel($dtOptions);
        echo "Count: ".count($dataProvider->getData()).PHP_EOL;
		foreach($dataProvider->getData() as $data)
		{
			$bill = $data->address->getBill($reportMonth);
			if(is_object($bill)) {
				echo $bill->reportMonth ." : ". $bill->address->company->name .", ". $bill->address->name . " : " .$bill->sum .PHP_EOL;
			}
			//$bill->updateFigures();
		}
	}
}
