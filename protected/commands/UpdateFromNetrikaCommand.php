﻿<?php

class UpdateFromNetrikaCommand extends CConsoleCommand
{
	public function run($args) {
		error_reporting(FALSE);
		
		if(isset($args["action"])) $action = $args["action"];
		else $action = 1;
		/*
		$lastUpdateAddress = strtotime(ExtAddress::model()->find(['select' => 'MIN(lastUpdate) lastUpdate', 'condition' => 'lastUpdate > 0'])->lastUpdate);
		$lastUpdateSpeciality = strtotime(ExtSpeciality::model()->find(['select' => 'MIN(lastUpdate) lastUpdate', 'condition' => 'lastUpdate > 0'])->lastUpdate);
		$lastUpdateDoctor = strtotime(ExtDoctor::model()->find(['select' => 'MIN(lastUpdate) lastUpdate', 'condition' => 'lastUpdate > 0'])->lastUpdate);
		$lastUpdateDistrict = strtotime(ExtDistrict::model()->find(['select' => 'MIN(lastUpdate) lastUpdate', 'condition' => 'lastUpdate > 0'])->lastUpdate);
		*/

		$loader = new NetrikaLoader();

		switch (intval($action)) {
			case 1:
				$loader->populateLPUList(); // получить ЛПУ, наполнить extAddress
				//break;
			case 2:
				$loader->populateSpecialityList(); // получить специальности, наполнить extSpeciality
				//break;
			case 3:
				$loader->populateDoctorList(); // получить врачей, наполнить extDoctors
				//break;
			case 4:
				$loader->populateDistrictList(); // получить районы, наполнить extDistrict
				//break;
			case 5: 
				// Сопоставление специальностей
				$extCriteria = new CDbCriteria;
				$extCriteria->group="extName";
				$extCriteria->addCondition("sysTypeId=".Yii::app()->db->quoteValue($loader->systype));
				$allSpeciality = ExtSpeciality::model()->findAll($extCriteria);
				echo PHP_EOL."Сопоставление специальностей: ".count($allSpeciality) . PHP_EOL;
				foreach ($allSpeciality as $speciality) {
					if(empty($speciality->extName)) continue;
					$speciality->extName = trim($speciality->extName);
					echo PHP_EOL . "- ".$speciality->extId." : ".$speciality->extName . PHP_EOL;
					
					$extName = $speciality->extName;
					foreach (DoctorSpecialty::$specialitiesAndDuplicates as $specialityName=>$specialityArray) {
						foreach ($specialityArray as $duplicateName) {
							if($speciality->extName == $duplicateName)  {
								$extName = $specialityName;
								echo $duplicateName;
								break 2;
							}
						}
					}
					DoctorSpecialty::$Archived = TRUE;
					$criteria = new CDbCriteria;
					$criteria->addCondition('name = :name','AND');
					$criteria->params = array( ':name'=>$extName );
					$doctorSpecialty = DoctorSpecialty::model()->find($criteria);
					if(!$doctorSpecialty) {
						$criteria = new CDbCriteria;
						$criteria->addCondition('linkUrl = :linkUrl','AND');
						$criteria->params = array( ':linkUrl'=>MyTools::makeLinkUrl($extName) );
						$doctorSpecialty = DoctorSpecialty::model()->find($criteria);
					}
					if(!$doctorSpecialty) {
						$doctorSpecialty = new DoctorSpecialty;
						$doctorSpecialty->name = $extName;
						$doctorSpecialty->linkUrl = MyTools::makeLinkUrl($extName);
						if(!$doctorSpecialty->save()) {
							$doctorSpecialty = NULL;
						} else {
							echo " _NEW_ ";
						}
					}
					if($doctorSpecialty) {
						ExtSpeciality::model()->updateAll(["specialtyId"=>$doctorSpecialty->id], ["condition"=>"extName=".Yii::app()->db->quoteValue($speciality->extName)." AND "."sysTypeId=".Yii::app()->db->quoteValue($loader->systype)]);
						echo " == ". $doctorSpecialty->name;
					} else {
						ExtSpeciality::model()->updateAll(["specialtyId"=>NULL], ["condition"=>"extName=".Yii::app()->db->quoteValue($speciality->extName)." AND "."sysTypeId=".Yii::app()->db->quoteValue($loader->systype)]);
						echo " == NULL";
					}
				}
				//break;
			case 6:
				// Добавление компаний
				$allCompanies = ExtCompany::model()->findAll(["condition" => "sysTypeId=".Yii::app()->db->quoteValue($loader->systype)]);
				echo PHP_EOL."<b>Добавление компаний: ".count($allCompanies)."</b>" . PHP_EOL;
				foreach ($allCompanies as $key=>$extCompany) {
					if(empty($extCompany->companyId)) {
						echo $extCompany->extId.": ".$extCompany->name . PHP_EOL;
						$data = [ "name" => $extCompany->name, "nameRUS" => $extCompany->name, "categoryId" => "a3969945e0f59fa84e0e7740c2e8cc87", "companyTypeId" => "468a3543-0d50-11e2-a1cd-e840f2aca94f", "cityId" => "534bd8b8-e0d4-11e1-89b3-e840f2aca94f", "IsOMS" => 1, "ratingSystem" => 3.5, "ratingUsers" => 3.5, "ratingSite" => 3.5, "rating" => 3.5];
						$response[0] = CompanyManager::AddCompany($data);
						echo " > ".$response[0]["status"] . ": " . $response[0]["text"];
						if($response[0]["status"] == "OK") {
							$extCompany->companyId = $response[0]["model"]["id"];
							if(!$extCompany->update()) {
								echo "Ошибка при сохранении идентификатора компании! ";
								MyTools::showErrors($extCompany->getErrors());
								continue;
							}
							$modelContract = new CompanyContract();
							$modelContract->contractNumber = 'КО-1/' . (Yii::app()->db->createCommand('SELECT MAX(indexNum) FROM `' . CompanyContract::model()->tableName() . '`')->queryScalar() + 1);
							$modelContract->name = $extCompany->name;
							//$modelContract->companyTypeId = "d9c0f168-9bd2-11e2-856f-e840f2aca94f";
							//$modelContract->typeId = "852b5020b64a5134413f6d12be1610e1";
							$modelContract->employeeId = "b44192f5-a19d-415d-8992-40edffb48cee";
							$modelContract->contractTypeId = "d9c0f168-9bd2-11e2-856f-e840f2aca94f";
							$modelContract->companyId = $extCompany->companyId;
							$modelContract->periodOfValidity = "2999-01-11 00:00:00";
							$modelContract->indexNum = explode('/', $modelContract->contractNumber)[1];
							if(!$modelContract->save()) {
								echo "Ошибка при создании контракта компании! ".PHP_EOL;
								MyTools::showErrors($extCompany->getErrors());
								continue;
							}
						} else {
							continue;
						}
					}
				}
				//break;
			case 7: 
				// Добавление адресов
				$allAddresses = ExtAddress::model()->findAll(["condition"=>"sysTypeId=".Yii::app()->db->quoteValue($loader->systype)]);
				echo PHP_EOL."Добавление адресов: ".count($allAddresses).PHP_EOL;
				foreach ($allAddresses as $key=>$extAddress) {
					echo PHP_EOL.$extAddress->extId.": ".$extAddress->name.PHP_EOL;
					if(empty($extAddress->addressId)) {
						$extCompany = ExtCompany::model()->find(["condition"=>"extId=".Yii::app()->db->quoteValue($extAddress->extId)." AND sysTypeId=".Yii::app()->db->quoteValue($extAddress->sysTypeId)]);
						if(!$extCompany) {
							echo " ОШИБКА";
							continue;
						}
						$companyId = $extCompany->companyId;
						if(empty($companyId)) {
							echo "идентификатор компании не задан ".PHP_EOL;
							continue;
						}
						$company = Company::model()->find(["condition"=>"id=".Yii::app()->db->quoteValue($companyId)]);
						
						$data = [
								"name" => $extAddress->extShortName,
								"street" => $extAddress->name,
								"isActive" => $extAddress->isActive,
								"countryId" => "534bd8ae-e0d4-11e1-89b3-e840f2aca94f",
								"regionId" => "9892a5ad-3302-11e2-b014-e840f2aca94f",
								"cityId" => "534bd8b8-e0d4-11e1-89b3-e840f2aca94f",
								"ratingSystem" => 3.5,
								"ratingUsers" => 3.5,
								"ratingSite" => 3.5,
								"rating" => 3.5,
								"samozapis" => 1,
						];
						if(!empty($extAddress->extDistrictId)) {
							$extDistrict = ExtDistrict::model()->find(["condition"=>"sysTypeId=".Yii::app()->db->quoteValue($loader->systype)." AND extId=".Yii::app()->db->quoteValue($extAddress->extDistrictId)]);
							if($extDistrict) $data["cityDistrictId"] = $extDistrict->cityDistrictId;
						}
						$response[0] = CompanyManager::AddAddress($companyId, $data);
						echo " > ".$response[0]["status"] . ": " . $response[0]["text"];
						if($response[0]["status"] == "OK") {
							$extAddress->addressId = $response[0]["model"]["id"];
							if(!$extAddress->update()) {
								echo "Ошибка при сохранении идентификатора адреса!! ";
								MyTools::showErrors($extAddress->getErrors());
								continue;
							}
							$extAddressModel = $response[0]["model"];
							$extAddressModel->name = $extAddress->name;
							if(!$extAddressModel->update()) {
								echo "Ошибка при сохранении идентификатора адреса! ";
								MyTools::showErrors($extAddressModel->getErrors());
								continue;
							}
							$response[1] = CompanyManager::CreateAccount($response[0]["model"], ["email" => "roman.z@emportal.ru", "password" => "6485", "phoneForAlert" => "", "agreement" => 1, "agreementNew" => 1, "createDate" => date('Y-m-d H:i:s'), "commonRegistry" => 1 ]);
							echo " > ".$response[1]["status"] . ": " .$response[1]["text"];
							if(!$company) {
								echo "Ошибка при сохранении основного адреса компании!(Компания не найдена) ";
								continue;
							}
							$company->attributes = ["addressId"=>$extAddress->addressId];
							if(!$company->update()) {
								echo "Ошибка при сохранении основного адреса компании! ";
								MyTools::showErrors($extAddress->getErrors());
								continue;
							}
						} else {
							continue;
						}
					} else {
						$data = [
								//"name" => $extAddress->extShortName,
								//"street" => $extAddress->name,
								"isActive" => $extAddress->isActive,
						];
						$response[0] = CompanyManager::EditAddress($extAddress->addressId, $data);
						echo " > ".$response[0]["status"] . ": " . $response[0]["text"] . PHP_EOL;
						if($response[0]["status"] !== "OK") {
							echo "-- Ошибка при обновлении адреса компании! ";
							continue;
						} else {
							echo "-- Успешное обновление адреса компании! ";
						}
					}
				}
				//break;
			case 8: 
				// Добавление врачей
				$allDoctors = ExtDoctor::model()->findAll(["condition"=>"sysTypeId=".Yii::app()->db->quoteValue($loader->systype)]);
				echo PHP_EOL."Добавление врачей: ".count($allDoctors).PHP_EOL;
				foreach ($allDoctors as $key=>$extDoctor) {
					echo PHP_EOL.$extDoctor->extId.": ".$extDoctor->extFio.PHP_EOL;
					if(empty($extDoctor->doctorId)) {
						$extAddress = ExtAddress::model()->find(["condition"=>"extId=".Yii::app()->db->quoteValue($extDoctor->extAddressId)." AND sysTypeId=".Yii::app()->db->quoteValue($extDoctor->sysTypeId)]);
						if(!$extAddress) {
							echo "ExtAddress не найден ".PHP_EOL;
							continue;
						}
						$address = Address::model()->find(["condition"=>"id=".Yii::app()->db->quoteValue($extAddress->addressId)]);
						if(!$address) {
							echo "Адресс не найден ".PHP_EOL;
							continue;
						}
						$extCompany = ExtCompany::model()->find(["condition"=>"extId=".Yii::app()->db->quoteValue($extAddress->extId)." AND sysTypeId=".Yii::app()->db->quoteValue($extAddress->sysTypeId)]);
						if(!$extCompany) {
							echo " ОШИБКА";
							continue;
						}
						$companyId = $extCompany->companyId;
						if(empty($companyId)) {
							echo "идентификатор компании не задан ".PHP_EOL;
							continue;
						}
					
						$data = [ "name" => $extDoctor->extFio, "sexId" => "ae11c45d855a991340c5f59edcb404da", "birthday" => "0000-00-00 00:00:00", "staffUnitId" => "b694644c-0334-11e2-930f-e840f2aca94f", "currentPlaceOfWorkId" => $companyId, "currentPosition" => "1bd00355-82c7-4c96-8a9a-c53028166ca3", "ratingSystem" => 3.5, "ratingUsers" => 3.5, "ratingSite" => 3.5, "rating" => 3.5 ];
						
						if(is_array($FIO = MyTools::FIOExplode($extDoctor->extFio))) $data = array_merge($data,$FIO);
						
						$data["address"] = $address;
						$response[0] = CompanyManager::AddDoctor($data);
						echo " > ".$response[0]["status"] . ": " . $response[0]["text"];
						if($response[0]["status"] == "OK") {
							echo " id: ".$response[0]["model"]["id"];
							$extDoctor->doctorId = $response[0]["model"]["id"];
							if(!$extDoctor->update()) {
								echo "Ошибка при сохранении идентификатора врача! ";
								MyTools::showErrors($extDoctor->getErrors());
								continue;
							}
						}
					} else {
						$data = [
								"name" => $extDoctor->extFio,
						];
						if(is_numeric($extDoctor->deleted)) $data["deleted"] = $extDoctor->deleted;
						$data = array_merge($data, MyTools::FIOExplode($extDoctor->extFio));
						$response[0] = CompanyManager::EditDoctor($extDoctor->doctorId, $data);
						echo " > ".$response[0]["status"] . ": " . $response[0]["text"] . PHP_EOL;
						if($response[0]["status"] !== "OK") {
							continue;
						}
					}
				}
				//break;
			case 9: 
				// Добавление специальностей врачей
				$allDoctors = ExtDoctor::model()->findAll(["condition"=>"doctorId IS NOT NULL AND doctorId != '' AND sysTypeId=".Yii::app()->db->quoteValue($loader->systype)]);
				echo PHP_EOL."Добавление специальностей врачей: ".count($allDoctors).PHP_EOL;
				foreach ($allDoctors as $key=>$doctor) {
					echo PHP_EOL.$doctor->extId.": ".$doctor->extFio;
					SpecialtyOfDoctor::model()->deleteAll("doctorId = ".Yii::app()->db->quoteValue($doctor->doctorId));
					$extCriteria = new CDbCriteria;
					$extCriteria->addCondition("specialtyId IS NOT NULL AND specialtyId != '' AND sysTypeId = ".$doctor->sysTypeId." AND extId=".Yii::app()->db->quoteValue($doctor->extSpecialityId)." AND extAddressId=".Yii::app()->db->quoteValue($doctor->extAddressId));
					$extCriteria->group="extName";
					$allSpeciality = ExtSpeciality::model()->findAll($extCriteria);
					$specialities = [];
					foreach ($allSpeciality as $speciality) {
						$specialities[] = ['doctorId' => $doctor->doctorId,'doctorSpecialtyId' => $speciality->specialtyId,'doctorCategoryId' => "7395e4d6-89d8-4d54-893f-56532b8f7502"];
					}
					echo " > добавить ".count($specialities);
					if(count($specialities)>0) {
						$doc = Doctor::model()->find(["condition"=>"id=".Yii::app()->db->quoteValue($doctor->doctorId)]);
						if($doc) {
							try {
								$doc->setSpecialtyOfDoctors($specialities);
							} catch (Exception $e) {
								echo PHP_EOL."Ошибка: ".$e->getMessage().PHP_EOL;
							}
						}
					}
				}
				//break;
			case 10:
				// Обновление районов адреса
				$allAddresses = ExtAddress::model()->findAll(["condition"=>"sysTypeId=".Yii::app()->db->quoteValue($loader->systype)]);
				echo "Обновление районов адреса: ".count($allAddresses).PHP_EOL;
				foreach ($allAddresses as $key=>$extAddress) {
					echo PHP_EOL.$extAddress->extId.": ".$extAddress->name.PHP_EOL;
					$existingAddress = Address::model()->findByAttributes(['id' => $extAddress->addressId]);
					if ($existingAddress) {
						if(!empty($extAddress->extDistrictId)) {
							$extDistrict = ExtDistrict::model()->find(["condition"=>"sysTypeId=".Yii::app()->db->quoteValue($loader->systype)." AND extId=".Yii::app()->db->quoteValue($extAddress->extDistrictId)]);
							if($extDistrict) {
								$existingAddress->cityDistrictId = $extDistrict->cityDistrictId;
								if($existingAddress->update()) {
									echo "Обновил адрес!".PHP_EOL;
								} else {
									echo "Ошибки при обновлении адреса: ";
									MyTools::showErrors($existingAddress->getErrors());
									continue;
								}
							} else {
								echo "Район адреса не найден! ";
								continue;
							}
						} else {
							echo "Идентификатор района не указан! ";
							continue;
						}
					} else {
						echo "Адрес не найден! ";
						continue;
					}
				}
				break;
		}
		echo PHP_EOL;
	}
}