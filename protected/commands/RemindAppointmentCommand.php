<?php

class RemindAppointmentCommand extends CConsoleCommand
{

	public function run($args) {

		error_reporting(FALSE);
		$dateStart = date("Y-m-d H:i:s", strtotime(date('Y-m-d 00:00:00')) + 3600 * 24 * 1);
		$dateFinish = date("Y-m-d H:i:s", strtotime(date('Y-m-d 00:00:00')) + 3600 * 24 * 2);
		$data = AppointmentToDoctors::model()->findAll("plannedTime > '".$dateStart."' AND plannedTime < '" . $dateFinish . "' AND statusId=:stat", array(
			/* ":stat"		=> AppointmentToDoctors::SEND_TO_REGISTER, */
			":stat" => AppointmentToDoctors::ACCEPTED_BY_REGISTER
		));
		foreach ($data as $row) {

			
			$sms = new SmsGate();
			$sms->phone = $row->phone;

			$sms->setMessage('Завтра,:date Вас ожидает на прием врач :doctor:address. emportal.ru'
					, array(
				#':stat' => AppointmentToDoctors::$STATUSES[$this->statusId],
				':date' => strtotime($row->plannedTime) != false ? " в ".date("H:i", strtotime($row->plannedTime))."," : '',
				':doctor' => (isset($row->doctor) ? $row->doctor->surName : '')." ".(!empty($row->doctor->firstName) ? mb_substr($row->doctor->firstName, 0, 1, 'UTF-8').'.' : '' ).(!empty($row->doctor->fatherName) ? mb_substr($row->doctor->fatherName, 0, 1, 'UTF-8').'.' : ''),
				':address' => ($row->address ? ' по адресу: ' . $row->address->street.', '.$row->address->houseNumber : '')
			));
			$sms->send();


			#$row->save(false);
		}
	}

}
