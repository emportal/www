﻿<?php

class UpdateCompanysCitySubdomainCommand extends CConsoleCommand {

	public function run($args) {
		
		error_reporting(FALSE);
		//для каждого города, который "включен" в конфиге, делаем обсчет компаний, принадлежащих к этому городу
		$regions = Yii::app()->params['regions'];
		foreach($regions as $region) {
			
			if ( ($args[0] != 'all') && ($region['subdomain'] != $args[0]) ) {
				continue;
			}
			
			$city = City::model()->findByAttributes([ 'subdomain' => $region['subdomain'] ]);
			if (!$city) continue;
			
			$criteria = new CDbCriteria;
			$criteria->with = [
				'address' => [
					'together' => true,
					'select' => false,
					'with' => [
						'city' => [
							'select' => 'link',
							'together' => true,
						]
					]
				],
			];
			//$criteria->addCondition('city.link = 2');
			$criteria->compare('city.id', $city->id, false);
			$companiesInthisCity = Company::model()->findAll($criteria);
			
			foreach($companiesInthisCity as $companyInthisCity) {
				$companyInthisCity->denormCitySubdomain = $region['subdomain'];
				$companyInthisCity->update();
			}
			
			echo 'companies updated in ' . $region['subdomain'] . ' region: ' . count($companiesInthisCity) . ';' . PHP_EOL;
		}
		
		exit;
	}
}