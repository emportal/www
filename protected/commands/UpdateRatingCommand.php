<?php

class UpdateRatingCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
		/* Обработка рейтинга */
		$cities = [];
		foreach (Yii::app()->params['regions'] as $region) {
			$cityModel = City::model()->findByLink($region['link']);
			if ($cityModel)
                $cities[] = $cityModel->id;
		}
		
		$criteria = new CDbCriteria();
		$criteria->limit = 10;
		$criteria->order = "addresses.ratingUpdateTime ASC";
		$criteria->compare('companyType.isMedical', 1);
		$criteria->compare('addresses.cityId', $cities);
		#$criteria->addNotInCondition('companyType.id', Address::$arrGMU);
		#$criteria->together = true;
		$criteria->compare('addresses.isActive', 1);
		$criteria->compare('t.removalFlag', 0);
		$criteria->compare('t.categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87');
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->with = array(
			'addresses' => array(
				'together' => true,
				'with' => array(
					'userMedicals' => array(
						'together' => true,
					),
				),
			),
			'companyType' => array(
				'together' => true,
			),
		);
		
		$companies = Company::model()->findAll($criteria);
		
		/* Обработка рейтинга клиники  */
		foreach ($companies as $company) {
			$company->updateRating();
			echo $company->name . ' :: ' . $company->rating . PHP_EOL;
		}
    }
}

