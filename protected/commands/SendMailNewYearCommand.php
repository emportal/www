<?php

class SendMailNewYearCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		$criteria = new CDbCriteria();
		$criteria->with = [
			'userMedicals' => [
				'together' => true,
			],
			'addresses' => [
				'together' => true,
			],				
		];
		$criteria->compare('userMedicals.agreementNew', '1');
		$criteria->compare('addresses.samozapis', '0');
		$data = Company::model()->findAll($criteria);

		foreach ($data as $row) {
			$mail = new YiiMailer();
			$mail->setView('new_year');
			$mail->Subject = 'Emportal поздравляет Вас с наступающим Новым годом!';
			$mail->setData([ 'model' => $row]);
			$mail->render();
			$mail->FromEmail = 'info@emportal.ru';
			$mail->FromName = Yii::app()->name;
			$emails = $row->mailListForAppReports;
			if(is_array($emails)) {
				foreach($emails as $mailAddress)
				{
					$mail->AddAddress($mailAddress);
					echo $mailAddress."\n";
				}
			}
			$mail->Send();
		}

		$mail = new YiiMailer();
		$mail->setView('new_year');
		$mail->Subject = 'Emportal поздравляет Вас с наступающим Новым годом!';
		$mail->setData([ 'model' => $row]);
		$mail->render();
		$mail->FromEmail = 'info@emportal.ru';
		$mail->FromName = Yii::app()->name;
		$mail->AddAddress('dima90-dima@mail.ru');
		$mail->Send();
	}

	// public function run2($args) {
	// 	error_reporting(FALSE);
	// 	$data = UserMedical::model()->findAll('agreementNew = 1 AND email <> ""');
	// 	$emails = [];
	// 	foreach ($data as $row) {
	// 		if(!$row->company->denormCitySubdomain) {
	// 			continue;
	// 		}
	// 		if(in_array($row->email, $emails)) {
	// 			continue;
	// 		}

	// 		$emails[] = $row->email;

	// 		$mail = new YiiMailer();
	// 		$mail->setView('update_offerta');
	// 		$mail->Subject = 'Уведоление об изменении порядка исчисления стоимости услуг Единого медицинского портала';
	// 		$mail->setData([ 'model' => $row]);
	// 		$mail->render();
	// 		$mail->FromEmail = 'info@emportal.ru';
	// 		$mail->FromName = Yii::app()->name;
	// 		$mail->AddAddress($row->email);
	// 		$mail->Send();

	// 		echo $row->email."\n";

	// 	}

	// 	$mail = new YiiMailer();
	// 	$mail->setView('update_offerta');
	// 	$mail->Subject = 'Уведоление об изменении порядка исчисления стоимости услуг Единого медицинского портала';
	// 	$mail->setData([ 'model' => $row]);
	// 	$mail->render();
	// 	$mail->FromEmail = 'info@emportal.ru';
	// 	$mail->FromName = Yii::app()->name;
	// 	$mail->AddAddress('dima90-dima@mail.ru');
	// 	$mail->Send();
	// }
}
