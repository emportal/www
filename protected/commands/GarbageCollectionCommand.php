<?php

class GarbageCollectionCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
    	
    	$criteria = new CDbCriteria;
    	$criteria->addCondition('plannedTime < now()','AND');
    	$criteria->compare('statusId',AppointmentToDoctors::DISABLED);
    	$log = AppointmentToDoctors::model()->deleteAll($criteria);
    	
    	exit;
    }
}

