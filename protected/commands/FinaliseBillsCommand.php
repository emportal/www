<?php

/**
 * Команда финализирует (bill.isFinal = 1) счета клиник за прошедшие периоды.
 * Финализированный счет считается "окончательным" и перестает обсчитываться при запросе через address->getBill()
 * (перестают обновляться поля appointmentTotal, appointmentDeclinedTotal, appointmentDeclinedByClinic, sum, salesContractTypeId)
 */

class FinaliseBillsCommand extends CConsoleCommand
{
	public function run($args)
    {
		error_reporting(FALSE);
		Address::$showInactive = true;
		Company::$showRemoved = true;
        $criteria = new CDbCriteria();
        $criteria->compare('t.isFinal', '0', false);
        $criteria->addCondition("UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(t.reportMonth, '-01'), '%Y-%m-%d')) < UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(:maxReportMonth, '-01'), '%Y-%m-%d'))");
        $criteria->params += [':maxReportMonth' => date('Y-m')];
        
        $bills = Bill::model()->findAll($criteria);
        echo "Count: ".count($bills).PHP_EOL;
        foreach($bills as $index=>$bill) {
			if(is_object($bill)) {
				echo $bill->reportMonth ." : ". $bill->address->company->name .", ". $bill->address->name . " : " .$bill->sum .PHP_EOL;
			}
        	/*
            print_r([
                'id' => $bill->id,
                'reportMonth' => $bill->reportMonth,
                'isFinal' => $bill->isFinal,
                'sum' => $bill->sum,
            ]);
            */
        	$bill->updateFigures();
            $bill->scenario = 'finaliseBills';
            $bill->isFinal = 1;
            if($bill->update()) {
	            $appModel = AppointmentToDoctors::model();
	            $appModel->visit_time_period = MyTools::getDateInterval($bill->reportMonth);
	            $appInfo = $bill->address->getInfoForAppointmentReports($appModel, null, ['manStatus'=>'r']);
	            $appointments = $appInfo['dataProvider']->getData();
				foreach($appointments as $appointment) {
					if($appointment->getWillBePayedFor() AND ($appointment->refererId !== null AND trim($appointment->refererId) !== "")) {
						$referer = User::model()->findByAttributes(["id"=>$appointment->refererId]);
						if(is_object($referer)) {
							$referer->giveAnAward();
							try {
								$log = new LogVarious();
								$log->attributes = [
								    'type' => 'referralAppointment',
								    'value' => CJSON::encode([
								        'refererId' => $appointment->refererId,
								        'userId' => $appointment->userId,
								        'appointmentId' => $appointment->id,
								    ])
								];
								$log->save();
							} catch (Exception $e) { }
						}
					}
				}
            }
        }
	}
}
