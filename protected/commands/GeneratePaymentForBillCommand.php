<?php

class GeneratePaymentForBillCommand extends CConsoleCommand
{
	public function run($args)
    {
		error_reporting(FALSE);
		Address::$showInactive = true;
		Company::$showRemoved = true;
        
		$criteria = new CDbCriteria();
		$criteria->compare('t.paymentStatusId',Bill::MANAGER_STATUS_PAID);
        $bills = Bill::model()->findAll($criteria);
        
        echo "count of bills: ". count($bills) .PHP_EOL;
        
        foreach($bills as $bill) {
        	echo $bill->id;
        	if($bill->getIsValid() && $bill->sum > 0 && !$bill->isPrepaid) {
	        	if(!Payment::model()->exists('billId=:billId',[':billId'=>$bill->id])) {
					$createDateTime = strtotime("+1 month",strtotime($bill->reportMonth."-15"));
					$createDateTime = (($createDateTime < time()) ? $createDateTime : time());
		            $payment = new Payment();
		            $payment->createDate = ($bill->isFinal) ? $bill->lastUpdate : date("Y-m-d", $createDateTime);
		            $payment->addressId = $bill->addressId;
		            $payment->billId = $bill->id;
		            $payment->sum = $bill->sum;
		            $payment->purpose = "Оплата по счету №".$bill->id;
		            $payment->logId = NULL;
		            $payment->statusId = Payment::STATUS_APPROVED;
					$payment->sourceId = "GeneratedForPaidBill";
		            if($payment->save()) {
	        			echo ':OK';
		            } else {
	        			echo ':ERROR';
	        			print_r($payment->getErrors());
		            }
		            unset($payment);
	        	} else {
	        		echo ':exists';
	        	}
        	} else {
	        	echo ':skip';
        	}
        	echo PHP_EOL;
        }
	}
}
