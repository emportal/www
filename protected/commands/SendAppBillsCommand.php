<?php

class SendAppBillsCommand extends CConsoleCommand
{
	public function run($args) {
		error_reporting(FALSE);
        Address::$showInactive = true;
        //Company::$showRemoved = true;
		$model = AppointmentToDoctors::model();
		$model->visit_time_period = MyTools::getDateInterval('lastMonth');
		$dtOptions = [
			'manStatus' => 'r',
			//'includeFixedContracts' => 'true',
			'excludedAppType' => AppointmentToDoctors::APP_TYPE_SAMOZAPIS,
			'pageSize' => false,
			'ignoreRights' => 'true',
			//'companyId' => 'EMPORTAL-4864-98cb-39fa-79af7baa42a2', //тестовая клиника ("совушек и котят")
			'groupByAddressId' => 'true',
			'showServices' => 'withVisits',
		];
		$dataProvider = $model->dataProviderForNewAdminPanel($dtOptions);
		foreach($dataProvider->getData() as $data)
		{
			$clinic = $data->address;
			if (!is_object($clinic))
				continue;
            
			$mailList = $clinic->mailListForAppReports;
			//var_dump([
			//	'company: ' => $clinic->company->name,
			//	'address: ' => $clinic->shortName,
			//	'mailList: ' => $mailList,
			//]);
			//$clinic->sendAppBillsViaEmail(['roman.z@emportal.ru']);
			$clinic->sendAppBillsViaEmail($mailList);
		}
	}
}
