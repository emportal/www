<?php

class UpdateFromEmiasCommand extends CConsoleCommand
{
	public function run($args) {
		error_reporting(FALSE);
		$stack = [1];
		
		if(isset($args["action"])) {
			$action = $args["action"];
			if(is_array($action)) {
				$stack = $action;
			} else {
				$stack = [$action];
			}
		}	
		
		$loader = new EmiasLoader();

		while (count($stack) > 0) {
			$action = array_shift($stack);
			switch ($action) {
				case 1:
					$allLpusInfo = $loader->getAllLpusInfo();
					$response = $loader->populateLPUList($allLpusInfo->return, true, true); // получить ЛПУ, наполнить extAddress
					break;
				case 2:
					// получить специальности, наполнить extSpeciality
					break;
				case 3:
					// получить врачей, наполнить extDoctors
					break;
				case 4:
					// получить районы, наполнить extDistrict
					break;
				case 5: 
					// Сопоставление специальностей
					break;
				case 6:
					// Добавление компаний
				case 7: 
					// Добавление адресов
				case 8: 
					// Добавление врачей
					break;
				case 9: 
					// Добавление специальностей врачей
					break;
				case 10:
					// Обновление районов адреса
					break;
			}
		}
		echo PHP_EOL;
	}
}