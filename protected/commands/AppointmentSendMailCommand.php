<?php

class AppointmentSendMailCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		Company::$showRemoved = true;
		Address::$showInactive = true;
		$this->sendThanksMail();
		$this->sendTimeDoctor();
		$this->sendReceiveBonus();
	}

	public function sendThanksMail() {
		$data = AppointmentToDoctors::model()->findAll('isSentThanksMail = 0 AND email <> "" AND (plannedTime < NOW()-INTERVAL 1 DAY AND (managerStatusId = 3 OR statusId = 4 OR managerStatusId = 4 OR statusId = 2 OR statusId = 3))');
		foreach ($data as $row) {
			if (!empty($row->email)) {

				$cancelCode = $this->getSubscriptionCancelCode($row->email);
				if(!$cancelCode) {
					continue;
				}

				$row->isSentThanksMail = 1;
				$row->isNotVisitedHash = md5(join("_", $row->attributes));

				$mail = new YiiMailer();

				if($row->managerStatusId == 3 || $row->statusId == 4) {
					$mail->setView('appointment_motivate_review');
					$mail->Subject = 'Врач ждёт вашего отзыва на Едином Медицинском Портале';
				}
				elseif($row->managerStatusId == 4 || $row->statusId == 2 || $row->statusId == 3) {
					$mail->setView('appointment_cancel_reason');
					$mail->Subject = 'Запись принята. Но вы её отменили. Есть пожелания?';
				}

				$mail->setData([ 'model' => $row, 'cancelCode' => $cancelCode ]);
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->AddAddress($row->email);
				
				echo $row->email . " : ";
				if ($mail->Send()) {
					$row->save(false);
					echo "OK";
				} else {
					echo "ERROR";
				}
				echo PHP_EOL;
			}
		}
	}

	public function sendTimeDoctor() {
		$data = AppointmentToDoctors::model()->findAll('isSentThanksMail = 1 AND email <> "" AND (plannedTime < NOW()-INTERVAL 3 MONTH AND (managerStatusId = 4 OR statusId = 2 OR statusId = 3))');
		foreach ($data as $row) {
			if (!empty($row->email)) {

				$cancelCode = $this->getSubscriptionCancelCode($row->email);
				if(!$cancelCode) {
					continue;
				}

				$row->isSentThanksMail = 2;
				$row->isNotVisitedHash = md5(join("_", $row->attributes));

				$mail = new YiiMailer();
				$mail->setView('time_of_the_doctor');
				$mail->Subject = 'Время проверить зубы! Успейте до повышения цен';		
				$mail->setData([ 'model' => $row, 'cancelCode' => $cancelCode ]);
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->AddAddress($row->email);
				
				echo $row->email . " : ";
				if ($mail->Send()) {
					$row->save(false);
					echo "OK";
				} else {
					echo "ERROR";
				}
				echo PHP_EOL;
			}
		}
	}

	public function sendReceiveBonus() {
		$data = AppointmentToDoctors::model()->findAll('isSentBonusMail = 0 AND email <> "" AND (plannedTime < NOW()-INTERVAL 2 MONTH AND (managerStatusId = 3 OR statusId = 4 OR managerStatusId = 4 OR statusId = 2 OR statusId = 3))');
		foreach ($data as $row) {
			if (!empty($row->email)) {

				$cancelCode = $this->getSubscriptionCancelCode($row->email);
				if(!$cancelCode) {
					continue;
				}			
					
				$row->isSentBonusMail = 1;
				$row->isNotVisitedHash = md5(join("_", $row->attributes));
				$mail = new YiiMailer();
				$mail->setView('receive_bonus');
				$mail->Subject = 'Получайте бонусы и оплачивайте ими приём к врачу!';				
				$mail->setData([ 'model' => $row, 'cancelCode' => $cancelCode ]);
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->AddAddress($row->email);
				
				echo $row->email . " : ";
				if ($mail->Send()) {
					$row->save(false);
					echo "OK";
				} else {
					echo "ERROR";
				}
				echo PHP_EOL;
			}
		}
	}

	public function getSubscriptionCancelCode($email = '', $type = 5) {
		$searchAttributes = [
				'type' => $type,
				'email' => $email,
		];
		if ($emailSubscription = EmailSubscription::model()->findByAttributes($searchAttributes)) {
			return $emailSubscription->cancelCode;
		}
		else {
			return false;
		}
	}
}
