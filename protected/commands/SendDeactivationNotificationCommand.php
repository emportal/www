<?php

class SendDeactivationNotificationCommand extends CConsoleCommand
{
	public function run($args)
    {
		error_reporting(FALSE);
		//Address::$showInactive = true;
        
        //1. получаем клиники с долгами
		$debtData = Address::getDebtDataForAllAddresses();
        $renderData = [];
        
        //2. раздаем тумаки
        foreach($debtData['addresses'] as $index => $address) {
            if (!$address->isActive)
                continue;
            
            //все адреса отсюда подлежат бану с уведомлениями
            $mailList = $address->getMailListForAppReports();
            $address->sendDeactivationNotificationViaEmail($maillist);
            $address->isActive = 0;
            $address->update();
            
            $log = new LogVarious();
            $log->attributes = [
                'type' => $log::TYPE_DEBT_CLINIC_DEACTIVATION,
                'value' => CJSON::encode([
                    'addressId' => $address->id,
                    'debtSum' => $address->debtSum,
                ])
            ];
            $log->save();
            
            /*echo('<br>id: ' . $bill->id . '; month: ' . $bill->reportMonth . '; sum: ' . $bill->sum . '; '<br>debtSum: ' . $address->debtSum
             . '; addressId: ' . $address->id . '; address name: ' . $address->name . '; url: ' . $address->getPagesUrls(true)
            );*/
            //var_dump('mailList', $mailList);
            
            //$chunk['companyName'] = $address->company->name;
            //$chunk['name'] = $address->shortName;
            //$chunk['url'] = $address->getPagesUrls(true);
            //$chunk['debtSum'] = $debtData['debtSums'][$index];
            //$chunk['debtMonths'] = [];
            //foreach($address->debtData['notPayedBills'] as $notPayedBill)
            //    $chunk['debtMonths'][] = $notPayedBill['reportMonth'];
            //$chunk['debtMonths'] = implode(', ', $chunk['debtMonths']);
            //$chunk['mailList'] = implode(', ', $mailList);
            //
            //$render_data[] = $chunk;
        }
        
        //$c = new Controller('Default');
        //$c->widget('zii.widgets.grid.CGridView', array(
        //    'dataProvider' => new CArrayDataProvider($render_data, [
        //        'pagination'=>array(
        //            'pageSize'=>1000,
        //        ),
        //    ]),
        //    'columns' => array(
        //        'companyName',
        //        'name',
        //        'url',
        //        'debtSum',
        //        'debtMonths',
        //        'mailList',
        //    ),
        //));
        //echo 'addresses count: ', count($debtData['addresses']), '; ovr_debt = ' . $debtData['ovrDebt'];
	}
}
