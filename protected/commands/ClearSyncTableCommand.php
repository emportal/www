<?php

class ClearSyncTableCommand extends CConsoleCommand {
    public function run($args) {
		Yii::app()->db->createCommand('DELETE FROM `syncTables` WHERE `done` = 1')->execute();
	}
}

