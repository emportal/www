﻿<?php

class UpdateShortSpecialtyOfDoctorCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		/* Генерация уникальных значений linkUrl
		$doctorSpecialtyModel = DoctorSpecialty::model()->findAll();
		foreach ($doctorSpecialtyModel as $doctorSpecialtyValue) {
			if(count(DoctorSpecialty::model()->findAllByAttributes(['linkUrl'=>$doctorSpecialtyValue->linkUrl])) > 1) $doctorSpecialtyValue->linkUrl = null;
			$doctorSpecialtyValue->save();
		}
		$serviceModel = Service::model()->findAll();
		foreach ($serviceModel as $serviceValue) {
			if(count(Service::model()->findAllByAttributes(['linkUrl'=>$serviceValue->linkUrl])) > 1) $serviceValue->linkUrl = null;
			$serviceValue->save();
		}
		*/
		foreach (DoctorSpecialty::$specialitiesAndDuplicates as $key=>$duplicates) {
			$dsmodel = DoctorSpecialty::model()->findAllByAttributes(['name'=>$key]);
			if(isset($dsmodel[0])) {
				echo $dsmodel[0]->name;
				foreach ($duplicates as $name) {
					$duplicatemodel = DoctorSpecialty::model()->findAllByAttributes(['name'=>$name]);
					foreach ($duplicatemodel as $model) {
						if(SpecialtyOfDoctor::model()->updateAll(["doctorSpecialtyId"=>$dsmodel[0]->id], ["condition"=>"doctorSpecialtyId=".Yii::app()->db->quoteValue($model->id)])) {
							echo "::Обновил";
						} else {
							echo "::Не обновил";
						}
						DoctorSpecialtyDisease::model()->deleteAll(["condition"=>"doctorSpecialtyId=".Yii::app()->db->quoteValue($model->id)]);
						try {
							if($model->delete()) echo "::Удалил дубль!";
						} catch (Exception $e) {
							echo "::Не удалил дубль!";
						} 
					}
				}
				echo "<br>";
			}
		}
		
		ShortSpecialtyOfDoctor::UpdateSelf();
		exit;
	}
}