<?php

class UpdateReviewsCountCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
		/* подсчёт количества отзывов */
		$cities = [];
		foreach (Yii::app()->params['regions'] as $region) {
			$cityModel = City::model()->findByLink($region['link']);
			if ($cityModel)
                $cities[] = $cityModel->id;
		}
		
		$criteria = new CDbCriteria();
		//$criteria->limit = 10;
		//$criteria->order = "addresses.ratingUpdateTime ASC";
		$criteria->compare('companyType.isMedical', 1);
		$criteria->compare('addresses.cityId', $cities);
		$criteria->compare('addresses.isActive', 1);
		$criteria->compare('t.removalFlag', 0);
		$criteria->compare('t.categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87');
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->with = array(
			'addresses' => array(
				'together' => true,
				'with' => array(
					'userMedicals' => array(
						'together' => true,
					),
				),
			),
			'companyType' => array(
				'together' => true,
			),
		);
		
		$companies = Company::model()->findAll($criteria);
		$reviewsTotalCount = 0;
        
		/* подсчёт количества отзывов клиники  */
		foreach ($companies as $company) {
			$company->updateReviewsCount();
            $reviewsTotalCount += $company->reviewsCount;
			echo $company->name . ' :: ' . $company->reviewsCount . PHP_EOL;
		}
        
        $configStats = Config::model()->findByAttributes(["name" => "Общее количество отзывов"]);
        $configStats->value = $reviewsTotalCount;
        $configStats->save();
    }
}

