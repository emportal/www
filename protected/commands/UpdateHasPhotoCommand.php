<?php

class UpdateHasPhotoCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
		$startTime = time();
		
    	$criteria = new CDbCriteria;
    	$criteria->addCondition('t.hasPhoto IS NULL');
    	$criteria->limit = 60;
    	$doctors = Doctor::model()->findAll($criteria);
    	
    	$num = 1;
    	foreach ($doctors as $model) {
	    	$logRow = [$num++, "UpdateHasPhoto"];
	    	if(!empty($model)) {
	    		$doctor = Doctor::model()->find("t.link = " . Yii::app()->db->quoteValue($model->link));
	    		$logRow[] = "Врач найден: " . $doctor->link;
	    		if(is_file(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . "/uploads/doctor/".$model->link."/photo.jpg")) {
	    			$doctor->hasPhoto = 1;
	    			$logRow[] = "Врач имеет фотографию!";
	    		} else {
	    			$doctor->hasPhoto = 0;
	    			$logRow[] = "Врач не имеет фотографию!";
	    		}
	    		if($doctor->update()) {
	    			$logRow[] = "Врач обновлён!";
	    		} else {
	    			$logRow[] = "Ошибка обновления врача!";
	    		}
	    	} else {
	    		$logRow[] = "Ошибка - Врач не найден!";
	    	}
	    	echo implode(" > ", $logRow) . PHP_EOL;
	    	if ((time() - $startTime) >= 59) { break; }
	    	sleep(1);
    	}
    	
    	exit;
    }
}

