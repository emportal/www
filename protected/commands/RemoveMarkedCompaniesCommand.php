<?php

class RemoveMarkedCompaniesCommand extends CConsoleCommand {

	public function run($args) {
		try
		{
			Company::model()->updateAll(array('removalFlag' => 1),'removalFlag = -1');
			echo "OK";
		}
		catch(Exception $e)
		{
			echo "Error!";
		    throw $e;
		}
		exit;
	}

}
