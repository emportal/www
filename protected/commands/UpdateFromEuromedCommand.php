﻿<?php

class UpdateFromEuromedCommand extends CConsoleCommand
{
	public function run($args) {
		error_reporting(FALSE);
		(new EuromodelLoader())->updateAppointmentsToDoctors();
	}
}