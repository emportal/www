﻿<?php

class GeocoderCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		
		Yii::import('application.controllers.AjaxController');
		$ajaxController = new AjaxController("ajax");
		$list = $ajaxController->actionGetAddressToGeocoder('php');
		foreach ($list as $item) {
			$result = $ajaxController->actionDefineGeocodesOfAddress($item['linkUrl'], 'php');
			echo $item['linkUrl'] . " > " . $result['status'] . " > " . $result['text'] . PHP_EOL;
		}
		
		// Найти ближайшие станции, у которых город стнции не совпадает с городом адреса
		$criteria = new CDbCriteria();
		$criteria->with = [
				'metroStation' => [
						'select' => false,
						'together' => true,
				],
				'address' => [
						'select' => false,
						'together' => true,
				],
		];
		$criteria->addCondition("address.cityId != metroStation.cityId");
		$nearestMetroStationArr = NearestMetroStation::model()->findAll($criteria);
		echo "count(nearestMetroStationArr) = ".count($nearestMetroStationArr).PHP_EOL;
		$correctMetroArr = [];
		foreach ($nearestMetroStationArr as $nearestMetroStation) {
			// Найти метро с таким же названием, но город как у адреса чтобы был
			$correctMetro = MetroStation::model()->find("t.name=:metroName AND t.cityId=:addressCityId", [":metroName"=>$nearestMetroStation->metroStation->name, ":addressCityId"=>$nearestMetroStation->address->cityId]);
			if(!$correctMetro) {
				$correctMetro = MetroStation::model()->find("t.name LIKE :metroName AND t.cityId=:addressCityId", [":metroName"=>"%".$nearestMetroStation->metroStation->name."%", ":addressCityId"=>$nearestMetroStation->address->cityId]);
			}
			if($correctMetro) {
				// Сделать коррекцию ближайшей станции метро, заменив её на ту станцию, которая находится в том же городе, что и адрес
				$nearestMetroStation->metroStationId = $correctMetro->id;
				if($nearestMetroStation->update()) {
					echo "Corrected nearest metro ".$correctMetro->name . PHP_EOL;
				} else {
					echo "Error when correcting ".$correctMetro->name . PHP_EOL;
				}
			} else {
				echo "Delete nearest metro ".$nearestMetroStation->metroStation->name;
				if($nearestMetroStation->delete()) {
					echo " OK " . PHP_EOL;
				} else {
					echo " Error " . PHP_EOL;
				}
			}
		}
		exit;
	}
}