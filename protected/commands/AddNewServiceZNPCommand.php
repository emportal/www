<?php

class AddNewServiceZNPCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
		$serviceId = Service::model()->findByLink('zapnp')->id;
		$model = Address::model()->findAll("samozapis = 0 AND isActive = 1");
		foreach ($model as $address) {
			if($address->canGetAppointments()) {
				$address->addService($serviceId);
			}
		}
    }
}

