<?php

class UpdateNewsReferenceCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		$criteria = new CDbCriteria;
		$criteria->addCondition("(t.doctorListQuery != '' AND t.doctorListQuery IS NOT NULL) OR (t.clinicListQuery != '' AND t.clinicListQuery IS NOT NULL)");
		$criteria->limit = 5;
		$criteria->order = "t.lastReferenceUpdate ASC";
		$news_list = News::model()->findAll($criteria);
		foreach ($news_list as $index=>$news) {
			echo $news->name . PHP_EOL;
			if(!empty($news->doctorListQuery)) {
				echo " --- doctorListQuery: ";
				$ids = self::getIds($news->doctorListQuery);
				if(is_array($ids)) {
					echo " : " . count($ids);
					$news->removeNewsReference(NewsReference::TYPE_DOCTOR);
					$news->addNewsReference($ids,NewsReference::TYPE_DOCTOR);
				} else {
					echo " : ERROR";
				}
				echo PHP_EOL;
			}
			if(!empty($news->clinicListQuery)) {
				echo " --- clinicListQuery: ";
				$ids = self::getIds($news->clinicListQuery);
				if(is_array($ids)) {
					echo " : " . count($ids);
					$news->removeNewsReference(NewsReference::TYPE_COMPANY);
					$news->addNewsReference($ids,NewsReference::TYPE_COMPANY);
				} else {
					echo " : ERROR";
				}
				echo PHP_EOL;
			}
			$news->lastReferenceUpdate = date('Y-m-d H:i:s', (time()));
			$news->update();
		}
	}
	
	static function getIds($listQuery) {
		$listQuery = explode('?', $listQuery)[0] . "?onlyIds=1&limit=3&" . explode('?', $listQuery)[1];
		//$listQuery = str_replace("emportal.ru", "local.emportal.ru", $listQuery);
		//$listQuery = str_replace("https:", "http:", $listQuery);
		echo $listQuery;
		$ids = false;
		$json = file_get_contents($listQuery);
		if($json !== false) {
			$ids = CJSON::decode($json);
		}
		return $ids;
	}
	
}