<?php

class RepairOfTimeCommand extends CConsoleCommand
{
	public function run($args)
    {
		error_reporting(FALSE);
		echo "Перенос времени приёма у записей со \"сломанным\" временем.".PHP_EOL;
		try {
			$command = Yii::app()->db->createCommand("
				UPDATE appointmentToDoctors SET plannedTime = DATE_FORMAT(date_sub(now(), interval 1 month),'%Y-%m-15 14:00:00')
				WHERE statusId != 7 AND DATE_FORMAT(createdDate,'%Y-%m') = DATE_FORMAT(date_sub(now(), interval 1 month),'%Y-%m') AND DATE_FORMAT(plannedTime,'%Y') = '1970';");
			$count = $command->execute();
			echo "Затронуто строк: ".$count.PHP_EOL;
		} catch (Exception $e) {
			echo "ERROR::".$e->getMessage().PHP_EOL;
		}
	}
}
