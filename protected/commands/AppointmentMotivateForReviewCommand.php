<?php

class AppointmentMotivateForReviewCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		Company::$showRemoved = true;
		Address::$showInactive = true;
		$data = AppointmentToDoctors::model()->findAll('isSentThanksMail = 0 AND email <> "" AND ((plannedTime < NOW()-INTERVAL 1 DAY AND (managerStatusId = 3 OR statusId = 4)) OR (plannedTime < NOW()-INTERVAL 1 MONTH AND (adminClinicStatusId = 2 OR (statusId = 1 AND adminClinicStatusId = 0))))');
		foreach ($data as $row) {
			if (!empty($row->email)) {
				$row->isSentThanksMail = 1;
				$row->isNotVisitedHash = md5(join("_", $row->attributes));

				$mail = new YiiMailer();
				$mail->setView('appointment_motivate_review');
				$mail->setData([ 'model' => $row ]);
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = 'Врач ждёт вашего отзыва на Едином Медицинском Портале';
				$mail->AddAddress($row->email);
				
				echo $row->email . " : ";
				if ($mail->Send()) {
					$row->save(false);
					echo "OK";
				} else {
					echo "ERROR";
				}
				echo PHP_EOL;
			}
		}
	}

}
