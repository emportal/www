<?php

class UpdateAddressServicesCommand extends CConsoleCommand {
	
    public function run($args) {
		
		error_reporting(FALSE);
		
		$criteria = new CDbCriteria();
		$criteria->limit = 100000;
		$criteria->compare('t.isActual', 1);
		
		$timeDifference = 90 * 24 * 60 * 60; //90 суток
		$timeBorder = time() - $timeDifference;
		
		$addressServices = AddressServices::model()->findAll($criteria);
		
		foreach ($addressServices as $addressService)
		{
			if (strtotime($addressService->confirmedDate) < $timeBorder)
			{
				$addressService->isActual = 0;
				$addressService->update();
			}
		}
    }
}

