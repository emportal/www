<?php

class UpdateAppointmentComissionCommand extends CConsoleCommand {
    public function run($args) {
		error_reporting(FALSE);
		/* Обработка рейтинга */
		$criteria = new CDbCriteria();
		$criteria->addCondition("t.statusId <> " . AppointmentToDoctors::DECLINED_BY_PATIENT);
		$criteria->addCondition("statusId != '".AppointmentToDoctors::DISABLED."'");
		$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_SAMOZAPIS . '"');
		$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_FROM_APP_FORM . '"');
		$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_OWN . '"');
		$allAppointment = AppointmentToDoctors::model()->findAll($criteria);
		echo count($allAppointment).PHP_EOL;
		foreach ($allAppointment as $appointment) {
			echo $appointment->phone . ": " . $appointment->getEmpComission() . PHP_EOL;
			$appointment->update();
		}
    }
}

