<?php

class TestCommand extends CConsoleCommand
{
	public function run($args)
    {
		#error_reporting(FALSE);
		Address::$showInactive = true;
		$address = Address::model()->findByPk('EMPORTAL-d185-4837-735f-e8b918792ca0');
		
		$mailList = ['roman.z@emportal.ru'];
		$mail = new YiiMailer($view='', $data=array(), $layout='', $config=false);
		$mail->setFrom('roman.zaytsev@emportal.ru', 'Emportal.ru');
		foreach($mailList as $mailAddress)
			$mail->addAddress($mailAddress);
		$mail->setData([
				'clinic' => $address,
				'manager' => $address->company->managerRelations->user,
		]);
		$mail->setView('claim_notification');
		$mail->Subject = 'Претензия (от emportal.ru)';
		$file_claim = FileMaker::createClaim($address, true, "pdf");
		$mail->addStringAttachment($file_claim[1], $file_claim[0]);
		
		$mail->render();
		$mail->send();
	}
}
