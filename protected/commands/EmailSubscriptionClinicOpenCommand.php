<?php

class EmailSubscriptionClinicOpenCommand extends CConsoleCommand {


public function run($args) {
		error_reporting(FALSE);
		Company::$showRemoved = true;
		Address::$showInactive = true;
		$data = EmailSubscription::model()->findAll('type = 4 AND companyId <> "" AND email <> ""');
		foreach ($data as $row) {
			if (!empty($row->email)) {

				if (!$row->company->address->canGetAppointments()) {
					continue;
				}

				$mail = new YiiMailer();
				$mail->setView('entry_clinic_open');
				$mail->Subject = 'Спасибо за ожидание. Клиника '.$row->company->name.' доступна для записи';		
				$mail->setData([ 'model' => $row ]);
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->AddAddress($row->email);
				
				echo $row->email . " : ";
				if ($mail->Send()) {
					$row->delete();
					echo "OK";
				} else {
					echo "ERROR";
				}
				echo PHP_EOL;
			}
		}
	}

}
