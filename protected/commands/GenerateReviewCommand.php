<?php

class GenerateReviewCommand extends CConsoleCommand {

	public function run($args) {
		error_reporting(FALSE);
		$host = Yii::app()->params['baseUrl'];
		$host = str_replace("http://", "https://", $host);
		Address::$showInactive = true;
		$regionalLinks = [];
		foreach (Yii::app()->params['regions'] as $region) {
			$regionalLinks[$region['subdomain']] = [];
		}

		$sModel = new Search();
		$criteria = new CDbCriteria();
		$criteria->select = 't.id,t.name,t.link,t.linkUrl,t.samozapis';
		$criteria->with = [
			'company' => [
				'select' => 'id,name,linkUrl',
				'together' => true,
				'condition' => "`company`.`categoryId` = 'a3969945e0f59fa84e0e7740c2e8cc87' OR `company`.`categoryId` = '8f7f0e92e866321d4ac5f2a5378a4668'",
				'with' => [
					'companyType' => [
						'select' => 'id,name',
						'together' => true
					]
				]
			],
			'doctors' => [
				'select' => 'id,link,linkUrl',
				'together' => true
			],
			'city' => [
				'select' => 'subdomain',
				'joinType' => 'INNER JOIN',
				'together' => true
			]
		];
		//$criteria->compare('t.isActive', 1);
		//$criteria->compare('userMedicals.agreementNew', 1);
		//$criteria->compare('city.subdomain', 'perm');
		$criteria->compare('company.removalFlag', 0);
		$criteria->compare('companyType.isMedical', 1);

		$clinics = Address::model()->findAll($criteria);
		error_reporting(TRUE);
		foreach ($clinics as $cl) {

			if(!empty($cl->city->subdomain)) {
				$regionHost = str_replace('https://', "https://".(($cl->city->subdomain != 'spb')?($cl->city->subdomain."."):(($cl->samozapis == 1) ? Yii::app()->params['regions'][$cl->city->subdomain]['samozapisSubdomain']."." : "")) , $host);
				$regionKey = $cl->city->subdomain . (($cl->samozapis == 1) ? (DIRECTORY_SEPARATOR . "samozapis") : "");

				foreach ($cl->company->reviews as $review) {
					$regionalLinks[$regionKey][] = ['locale' => 'ru', 'type' => 'biz', 'url' => ($regionHost.'/klinika/'.$cl->company->linkUrl.'/'.$cl->linkUrl), 'description' => $review->reviewText, 'reviewer' => [ 'vcard' => [ 'fn' => $review->userName]], 'dtreviewed' => $review->createDate, 'item' => [ 'vcard' => [ 'fn' => $cl->company->name, 'street-address' => $cl->name]]];
				}
			}
		}

		$basepath = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;

		foreach ($regionalLinks as $regionKey=>$links) {
			echo $regionKey;
			$path = $basepath . 'uploads' . DIRECTORY_SEPARATOR . 'regions' . DIRECTORY_SEPARATOR . $regionKey . DIRECTORY_SEPARATOR;
			echo $regionKey . " :: " . self::writeContent($path,$links) . PHP_EOL;
		}
	}

	public static function arrayToXml($array, &$xml_user_info) {
	    foreach($array as $key => $value) {
	        if(is_array($value)) {
	            if(!is_numeric($key)){
	                $subnode = $xml_user_info->addChild("$key");
	                self::arrayToXml($value, $subnode);
	            }else{
	                $subnode = $xml_user_info->addChild("review");
	                self::arrayToXml($value, $subnode);
	            }
	        }else {
	            $xml_user_info->addChild("$key",htmlspecialchars("$value"));
	        }
	    }
	}

	protected static function writeContent($path,$links) {
		mkdir($path);
		$filename = $path . 'reviews.xml';
		$content  = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
		$content .= '<reviews>' . "\n";

		$xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><reviews></reviews>");

		self::arrayToXml($links,$xml_user_info);

		$xml_file = $xml_user_info->asXML($filename);
	}

	protected static function getStatusPage($url) {
		$status = 404;
	  	if($curl = curl_init() ) {
		    curl_setopt($curl,CURLOPT_URL,$url);
		    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($curl,CURLOPT_NOBODY,true);
		    curl_setopt($curl,CURLOPT_HEADER,true);
		    curl_exec($curl);
		    $status = curl_getinfo($curl,CURLINFO_HTTP_CODE );
		    curl_close($curl);
	  	}
	  	return $status;
	}
}
