<?
$this->breadcrumbs = array(
	'Личный кабинет' => '/user/index',
	'Мои бонусы'
);
?>
<style>
.user-bonuses-content h2 {
	font-size: 24px;
	font-weight: bold;
	color: #333333;
	margin: 0 0 10px 0;
}
.user-bonuses-content h3 {
	color: #777777;
	font-size: 30px;
	font-family: "ReformaGroteskBoldC";
	text-transform: uppercase;
}
.user-bonuses-content p {
    font-size: 18px;
    color: #777777;
    line-height: 1.3em;
    margin-top: 5px;
	margin-bottom: 20px;
}
.promoCode-input {
    font-size: 14px;
    width: 66% !important;
    height: 40px;
    padding: 0px 0px 0px 10px !important;
    margin: 0 !important;
    display: block;
    float: left !important;
    border: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
}
.promoCode-submit {
    width: 34%;
    height: 40px;
    display: block;
    position: relative;
    float: right;
    background: #aaaaaa;
    color: white;
    font-weight: bold;
    border: none;
    border-radius: 0px 3px 3px 0px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
}
.bonusCode {
    color: #333333;
    font-size: 24px;
    font-weight: bold;
    text-align: center;
    width: 54% !important;
    height: 60px;
    padding: 0px 0px 0px 0px !important;
    margin: 0 !important;
    display: block;
    float: left !important;
    background: #fafafa;
    border: 1px solid #dddddd;
    border-radius: 5px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
}
</style>

<?php if (Yii::app()->user->hasFlash('error')): ?>
	<div class="flash-error">
	 <?= Yii::app()->user->getFlash('error') ?>
	</div>
<?php elseif (Yii::app()->user->hasFlash('success')): ?>
	<div class="flash-success">
	 <?= Yii::app()->user->getFlash('success') ?>
	</div>
<?php endif; ?>
<div class="user-bonuses-content">
	<h2><span style="width: 24px;height: 24px;margin: 0px 9px 0px 0px;background: url(/images/icons/gift.png) no-repeat;display: inline-block;"></span>Бонусная программа</h2>
	<div class="count-bonus"><span>ваши<br>баллы</span> <span class="color2"><?= $this->model->balance ?></span><div style="clear: both"></div></div>
	<p>
        Вы можете использовать бонусы для оплаты услуг и врачей на нашем сайте. 
        <br>1 балл = 1 рублю. Баллы действуют только для клиник, участвующих в программе лояльности - их можно узнать по значку
        <a href="/doctor?Search[loyaltyProgram]=1" class="nl">
            <span class="empLoyaltyProgram" style="display: inline-block;  width: 140px; height: 24px; margin-right: 110px;"></span>
        </a>
    </p>
	<h3>У вас есть промо-код?</h3>
	<p>Скорее вводите его и получайте законные баллы!</p>
	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'reg-form',
			'action' => $this->createUrl('/user/bonuses'),
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'inputContainer' => 'div',
			),
			'htmlOptions' => array('class' => '', 'enctype' => 'multipart/form-data'),
		));
	?>
	<div class="auth-field">
		<?= $form->label($model,'promoCode',['label'=>'Введите промокод']); ?>
		<div style="width: 382px;height: 42px;border: 1px solid #dddddd;border-radius: 3px;box-sizing: border-box;margin: 5px 0 45px 0;">
			<?= $form->textField($model, 'promoCode', array('class' => 'promoCode-input', 'placeholder'=>'XXXXXX')) ?>
			<input type="submit" value="Проверить код" class="promoCode-submit"/>
			<?= $form->error($model, 'promoCode') ?>
		</div>
	</div>
	<?php
		$this->endWidget();
	?>
	<h3>Хотите получить больше баллов? Пригласите друзей!</h3>
	<p>Поделитесь с друзьями своим кодом и мы подарим каждому из них по 500 баллов на первый приём. Вы также получите по 500 баллов за каждого из друзей, записавшихся на прием через emportal.ru и фактически сходивших на прием. Максимальная сумма, которую можно накопить таким образом - 15000 баллов.</p>
	<div>
		<label class="sub-title" style="font-size: 18px;color: #333333;">Ваш код:</label>
		<div style="width: 470px;height: 60px;margin: 5px 0 45px 0; margin-top: 20px;">
            <div style="width: 0; height: 0; overflow: hidden;"><input type="text" id="refererLink" value="dsklgjksdg?df"/></div>
			<input type="text" id="promoCodeToClipboard" class="bonusCode no-select" value="<?=$model->myPromocode?>" name="User[promoCode]" readonly>
			<div style="font-size: 12px; width: 46%; height: 60px; display: block; position: relative; float: right; color: #777777; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing: border-box; box-sizing: border-box; padding: 6px 0px 12px 20px;">
				Реферальная ссылка<br>
                <input id="promoCodeToClipboard2" type="text" style="width: 360px;" value="<?= Yii::app()->request->hostInfo . '/user/bonuses?promoCode=' . $model->myPromocode ?>" readonly>
			</div>
		</div>
	</div>
	<h3>Поделитесь ссылкой в соцсетях</h3>
	<p class="text-shared-bonus">Поделитесь с друзьями ссылкой в соцсетях. Если они зарегистрируются по ссылке, то получат 500 бонусов на первый приём. Вы получите за каждого из приглашенных 500 баллов.</p>
	<p>
	<?php
	$shareUrl = "https://emportal.ru/user/bonuses?promoCode=" . $model->myPromocode;
	$shareComment = "Ссылка с промо-кодом от Единого Медицинского Портала для получения 500 баллов на первый приём.";
	?>
		<a target="_blank" class="share-button-mini-vk" href="http://vk.com/share.php?url=<?=$shareUrl?>"></a>
		<a target="_blank" class="share-button-mini-fb" href="http://www.facebook.com/sharer/sharer.php?u=<?=$shareUrl?>"></a>
		<a target="_blank" class="share-button-mini-tv" href="https://twitter.com/intent/tweet?text=<?=urlencode($shareComment)?>&url=<?=$shareUrl?>"></a>
		<a target="_blank" class="share-button-mini-ok" href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?=$shareUrl?>&st.comments=<?=urlencode($shareComment)?>"></a>
		<a target="_blank" class="share-button-mini-gp" href="https://plus.google.com/share?url=<?=$shareUrl?>"></a>
	</p>
</div>
<script>
 $(function() {
    $('#promoCodeToClipboard').on('click', function() {
        $this = $(this);
        var tempVal = $this.val();
        $this.val('<?= Yii::app()->request->hostInfo . '/' . Yii::app()->request->url ?>');
        $this.select();
        document.execCommand('copy');
        $this.val(tempVal);
    });
});
</script>