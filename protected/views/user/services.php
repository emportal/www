<?php
$this->breadcrumbs = array(
	'Личный кабинет' => '/user/index',
	'Услуги'
);
?>
<div>
	<h1>Услуги</h1>
</div>
<?php
if (count($user->purchasedServices)):
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'	 => $dataProvider,
	'itemsCssClass'	 => 'service-table',
	'columns'		 => [
		[
			'header' => 'Дата записи',
			'value' => '$data->createdDate',
		],
		[
			'header' => 'Клиника',
			'value' => function($data) {
				$address = $data->addressService->address;
				$linkHref = 'https://emportal.ru/clinic';
				$linkHref .= '/' . $address->company->linkUrl;
				$linkHref .= '/' . $address->linkUrl;
				
				$output = '';
				$output .= '<a href="' . $linkHref . '">';
				$output .= $address->company->name;
				$output .= '</a>';
				return $output;
			},
			'type' => 'raw'
		],
		[
			'header' => 'Услуга',
			'value'	=> '$data->service->name',
		],
		[
			'header' => 'Стоимость (руб.)',
			'value' => '$data->price',
		],
	],
));
else:
echo 'Услуг не найдено';
endif;
?>
