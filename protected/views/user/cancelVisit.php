<?php
/* @var $model Doctors */
/* @var $comment Comments */

$this->breadcrumbs = array(
	'Личный кабинет' => '/user/index',
	'Оставить отзыв'
);
?>

<div class="search-result">
	<?php if($model->appType === AppointmentToDoctors::APP_TYPE_SAMOZAPIS): ?>
		<span>Для отмены записи, обратитесь в регистратуру клиники.</span>
	<?php else: ?>
	<div class="flash-notice">
		<span>Данное действие приведет к отмене записи в клинику. Выполнить отмену записи?</span>
		
		
	</div>
	<?php echo CHtml::link("Нет", array("/user/registry"), array('class'=>'btn-blue'));?>
			<?php echo CHtml::link("Да, я хочу отменить запись", array("/user/cancelVisit","link"=>$model->link, 'delete'=>1), array('class'=>'btn-green'));?>
			
	<?php endif; ?>
	
</div>