<?php
/* @var $this Controller */
/* @var $model Clinic */
/* @var $company Company */

$CAN_UPLOAD_DOCUMENTS_FROM_1C = false;
foreach (Yii::app()->user->model->rights as $right) {
	if($right->zRightId == RightType::CAN_UPLOAD_DOCUMENTS_FROM_1C) {
		$CAN_UPLOAD_DOCUMENTS_FROM_1C =true;
		break;
	}
}
?>
<script>
	function runScript(e) {
		if (e.keyCode == 13) {
			searchList();
			return false;
		}
	}

	function searchList() {
		var text = $("#search").val().toLowerCase();
		
		$(".cName").each(function() {
			var rowText = $(this).html().toLowerCase();
			var parent = $(this).closest("tr");
			var ind = rowText.indexOf(text);
			if(ind != -1) {
				parent.show();
			} else {
				parent.hide();
			}
		});
	}
</script>

<!--<input class="custom-text" onkeypress="return runScript(event)" type="text" id="search"/>
<a class="btn btn-blue" onclick="searchList();return false;">Искать</a>-->
<h3>Управление адресами</h3>
<div class="company-list">
	<table class="table-custom vmargin20">
		<tr>
			<th>Название</th>
			<th>Адрес</th>
			<th>Активность</th>
			<th>Вход</th>
		</tr>
		<?php foreach($list as $row): ?>
			<tr>			
				<td class="cName" style="width:100px;"><?= $row->company->name ?> <?= empty($row->userMedicals) ? '': ''?></td>
				<td>
					<?= $row->name ?>
				</td>
				<td>
					<?= $row->isActive ? '<span class="button bg-green">Адрес активен</span>' : '<div class="button bg-red c-white">Адрес не активен <br>(счет не оплачен)</div>' ?>
				</td>
				<td><?= CHtml::link('Войти','/user/administrate?link='.urlencode($row->userMedicals->link),array('class'=>'btn btn-green')); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
