<? /* @var $model User */ ?>
<?php
$this->breadcrumbs = array(
	'Личный кабинет' => '/user/index',
	'Профиль'
);
Yii::app()->clientScript->registerPackage('jquery.maskedinput');

?>
<?php /*rf successful appointment*/ ?>
<?php if (Yii::app()->user->hasFlash('visited')): Yii::app()->user->getFlash('visited'); ?>
	<div class="flash-success" xmlns="http://www.w3.org/1999/html">
		<p>Ваша заявка на прием успешно отправлена в медицинское учреждение!</p>
		<p>
			Учреждение: <?=$lastAppointment->company->name?>, <?=$lastAppointment->address->street ." ". $lastAppointment->address->houseNumber?>
			<br>Время приема: <?=$lastAppointment->plannedTime?>
			<br>Врач: <?=$lastAppointment->doctor->name;?>
		</p>
		<p>
			Как только специалисты клиники обработают вашу заявку, вы получите подтверждение в виде смс или звонка представителя клиники. Для обеспечения высокого качества обслуживания операторы Единого Медицинского Портала будут контролировать процесс обработки вашей заявки сотрудниками клиники.
		</p>
		<p>
		<?php
		if (!$model->hasEmailNotifications(1) && $model->email)
		{
			echo '<br><h4>Подписаться на новости ЕМП</h4>';
			$emailBlock = new EmailBlock;
			$emailBlock->options = [
				'blockId' => 'trackPatientInfoEmailBlockFromAppointment',
				'emailAttributes' => [
					'data-type' => '1'
				],
				'fixedEmail' => $model->email
			];
			$emailBlock->run();
		}
		?>
		</p>
	</div>
<?php if (empty($model->email)): ?>
	<!-- лидогенерация для пациента без email'а -->
	<div id="userEmailModal" class="md-modal popup">
		<div class="md-close">X</div>
		<div class="md-content">
			<div class="md-form-container">
				<div id="#emailSubscriptionForm">
					<div>
						Укажите, пожалуйста, свой e-mail, на который мы отправим талон записи на прием. 
					</div>
					<div class="vmargin20">
					<?php
					$emailBlock = new EmailBlock;
					$emailBlock->options = [
						'blockId' => 'trackPatientInfoEmailBlockFromAppointment2',
						'emailAttributes' => [
							'data-type' => '3',
						],
					];
					$emailBlock->run();
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="md-overlay"></div>
	<script>
	var userEmailModal = {};
	$(function() {
		var options = {
			'id': 'userEmailModal'
		};
		userEmailModal = new ModalWindowConstructor(options);
		userEmailModal.show();
	});
	</script>
	<!-- лидогенерация для пациента без email'а -->

<?php endif; ?>
<?php endif; ?>
<?php if (isset(Yii::app()->session['reachedGoal_appointment_as_old_user'])): ?>
	<script>		
	$(window).load(function() {
			window.setTimeout(function() {
				countersManager.reachGoal({
					'forCounters': ['yandex', 'google'],
					'goalName': 'appointment_as_old_user',
					'callback': function() {console.log('callback: appointment_as_old_user');}
				});
			}, 100);
		});
	</script>
	<?php unset(Yii::app()->session['reachedGoal_appointment_as_old_user']); ?>
<?php endif; ?>
<?php if (isset(Yii::app()->session['reachedGoal_user_register'])): ?>
	<script>		
	$(window).load(function() {
			window.setTimeout(function() {
				countersManager.reachGoal({
					'forCounters' : ['yandex', 'google'],
					'goalName' : 'user_register',
					//'callback': function() {console.log('callback: user_register');}
				});
			}, 100);
		});		
	</script>
	<?php unset(Yii::app()->session['reachedGoal_user_register']); ?>
<?php endif; ?>


<?php if (empty($model->password)): ?>
	<div class="flash-notice">
		У вас не указан пароль от аккаунта. 
		<a class="pseudo_a_imp" onclick="scrollToId('#User_passwordNew'); return false">Укажите пароль</a>, чтобы иметь возможность в любой момент войти в личный кабинет без необходимости дополнительно подтверждать номер мобильного телефона.
	</div>
<?php endif; ?>
<?php 

if (empty($model->email)): ?>

	<div class="flash-notice">
		У вас не указан адрес электронной почты.
		<a class="pseudo_a_imp" onclick="scrollToId('#email'); return false">Укажите ваш E-mail</a>, чтобы иметь возможность получать тематическую рассылку и в числе первых узнавать об акциях и скидках наших партнеров.
	</div>

<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('success')): ?>
	<div class="flash-success"><?= Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>
<h1 class="prof-title view-hidden-ms">ПРОФИЛЬ</h1>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'post',
	'id' => 'reg-form',
	'action' => $this->createUrl('/user/profile'),
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
		'inputContainer' => 'td',
	),
	'htmlOptions' => array('class' => 'profile', 'enctype' => 'multipart/form-data'),
));
/* @var $form CActiveForm */
?>
<div class="col-1">
	<div class="personal-information row-1">
		<div class="row-prof-title view-hidden-ms">Личная информация</div>
		<div class="col col-1">
			<div class="row-1">
				<span class="row-title">ФИО</span>
					<?php
					echo $form->textField($model, 'name', array('class' => 'custom-text'));
					echo '<br/>';
					echo $form->error($model, 'name');
					?>
			</div>
			<div class="row-2">
				<span class="row-title">Дата рождения</span>
				<?php echo $form->hiddenField($model, 'subscriber', ['id' => 'subscr']); ?>
				<?= $form->textField($model, 'birthday', ['class' => 'custom-text']); ?>
				<br>
				<?= $form->error($model, 'birthday') ?>
				<script type="text/javascript">
					$(document).ready(function() {
						$("#User_birthday").datetimepicker({
							changeMonth: true,
							changeYear: true,
							lang: 'ru',
							timepicker: false,
							format: 'd.m.Y',
							closeOnDateSelect: true,
							minDate: "1900/01/01",
							maxDate: '0',
							defaultDate:'1989/12/31',
							firstDay: 1,
						});
						$("#User_birthday").mask("99.99.9999", {placeholder: "_"});
						$("#User_birthday").attr("placeholder", "31.12.1989");
					});
				</script>
			</div>
			<div style="clear: both"></div>
		</div>
		<div class="col col-2">
			<span class="row-title sex-title-ms">Пол</span>
			<?=
			$form->radioButtonList($model, 'sexId', CHtml::listData(Sex::model()->findAll(), 'id', 'name'), array(
				'separator'=>" &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ",
			))
			?><br/>
			<?= $form->error($model, 'sexId'); ?>
		</div>
		<div class="col col-3">
			<div class="row-1">
				<span class="row-title">Серия полиса ОМС</span>
					<?php
					echo $form->textField($model, 'OMSSeries', array('class' => 'custom-text'));
					echo '<br/>';
					echo $form->error($model, 'OMSSeries');
					?>
			</div>
			<div class="row-2">
				<span class="row-title">Номер полиса ОМС</span>
					<?php
					echo $form->textField($model, 'OMSNumber', array('class' => 'custom-text'));
					echo '<br/>';
					echo $form->error($model, 'OMSNumber');
					?>
			</div>
			<div style="clear: both"></div>
		</div>
	</div>
	
	<div class="change-password row-1" style="margin: 25px 0px 0px;">
		<div class="row-prof-title">Изменение пароля</div>
		<div class="col">
			<div class="row-1">
				<span class="row-title">
					<?php if (empty($model->password)): ?>
						Пароль
					<?php else: ?>
						Новый пароль
					<?php endif; ?>
				</span>
				<td class="search-result-info">
					<div class="cont-passw" >
						<?= $form->passwordField($model, 'passwordNew', array('class' => 'custom-text', 'autocomplete' => 'off')) ?>
						<div class="user_remember_block showNewPassword view-hidden-ms">
							<span class="niceCheck ">
								<input type="checkbox" name="showNewPassword"  id="showNewPassword" >
							</span>
							<label class="rememberLabel" for="showNewPassword">Показать</label>
						</div>
					</div>
					<br/>
					<?= $form->error($model, 'passwordNew') ?>
				</td>
			</div>
		</div>
		<div class="col">
			<div class="row-1">
				<span class="row-title">Подтверждение пароля</span>
				<td class="search-result-info">
					<div class="cont-passw" >
						<?= $form->passwordField($model, 'passwordConfirm', array('class' => 'custom-text')) ?>
						<div class="user_remember_block pshowNewPassword view-hidden-ms">
							<span class="niceCheck"><input type="checkbox" name="pshowNewPassword" id="pshowNewPassword" value="undefined" tabindex="undefined"></span>
							<label class="rememberLabel" for="pshowNewPassword">Показать</label>
						</div>
					</div>
					<br/>
					<?= $form->error($model, 'passwordConfirm') ?>
				</td>
			</div>
		</div>
		<div class="col">
			<div class="row-1">
				<?php if (!empty($model->password)): ?>
				<div class="desc-block info-text view-hidden-ms">Для сохранения изменений введите текущий пароль</div>
				<span class="row-title">Текущий пароль</span>
				<div class="cont-passw" >
					<?= $form->passwordField($model, 'passwordOld', array('class' => 'custom-text')) ?>
					<div class="user_remember_block showOldPassword view-hidden-ms" >
						<span class="niceCheck"><input type="checkbox" name="showOldPassword" id="showOldPassword" value="undefined" tabindex="undefined"></span>
						<label class="rememberLabel" for="showOldPassword">Показать</label>
					</div>
				</div>
				<br/>
				<?= $form->error($model, 'passwordOld') ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="col-3">
	<div class="row-1">
		<button type="submit" title="Сохранить измененния" class="btn-green big-button">Сохранить<span class="view-hidden-ms"> измененния</span></button>
	</div>
</div>
<?php $this->endWidget(); ?>

<div class="personal-contacts">
	<div class="contacts row-2">	
		<div class="row-prof-title view-hidden-ms">Контакты</div>
		<div class="abs-block-email">
			<?php
			$form = $this->beginWidget('CActiveForm', array(
				'method' => 'post',
				'id' => 'emailchange-form',
				'action' => $this->createUrl('/user/emailchange'),
				'enableAjaxValidation' => false,
				'htmlOptions' => array('class' => '', 'enctype' => 'multipart/form-data'),
			));
			/* @var $form CActiveForm */
			?>
			<span class="row-title">E-mail</span>
			<?php
			echo CHtml::textField('email', $model->email, array('class' => 'custom-text','style'=>''));
			echo CHtml::label(CHtml::linkButton('изменить',['class'=>'btn-green']), false);
			?><br/>
			<script>
				$(document).ready(function() {
					$('.subscri .niceCheck').live('click', function() {
						var sub = $('#subscr');
						if (sub.val() == 1) {
							sub.val(0);
						} else {
							sub.val(1);
						}
					});
		
					$('.showNewPassword').click(function(){
						if($(this).find('.niceCheck').hasClass('niceChecked')){
							$('#User_passwordNew').attr('type', 'text');
						}else{
							$('#User_passwordNew').attr('type', 'password');
						}
					});
					$('.pshowNewPassword').click(function(){
						if($(this).find('.niceCheck').hasClass('niceChecked')){
							$('#User_passwordConfirm').attr('type', 'text');
						}else{
							$('#User_passwordConfirm').attr('type', 'password');
						}
					});
					$('.showOldPassword').click(function(){
						if($(this).find('.niceCheck').hasClass('niceChecked')){
							$('#User_passwordOld').attr('type', 'text');
						}else{
							$('#User_passwordOld').attr('type', 'password');
						}
					});
				});
			</script>
			<?php if (!empty($model->email)): ?>
				<div class="tmargin10">
					<?php
					/* управление подписками */
					$this->renderPartial('emailSubsPartial', ['model' => $model]);
					?>
				</div>
			<?php endif; ?>
			<div class="displaynone">
				<?php
				echo $form->checkbox($model, 'subscriber', array('class' => 'niceCheck'));
				?>
				<label for="check1">Я хочу получать тематическую рассылку</label><br>
			</div>
			<?php $this->endWidget(); ?>
		</div>
		<div class="col email-block view-hidden-ms">
		<div class="desc-block view-hidden-ms">Укажите ваш E-mail, чтобы получать тематическую рассылку и в числе первых узнавать об акциях и скидках наших партнеров.</div>
		

		</div>
		<div class="abs-block-telefon">
			<span class="row-title">Мобильный телефон</span>
			<td class="search-result-info">
				<?php if ($model->phoneVerificationCount): ?>
					<span class="phone" id="phoneSpan" data-attr="telefon"><?= $model->phoneVerification->phone ?></span>
					<span class="declineButton btn-red" onclick="declineCode();">отменить</span>
					<br/>
					<input type="text" id="code" class="custom-text"/>
					<span class="acceptButton btn-green" onclick="acceptCode();">подтвердить</span>

				<?php else: ?>
					<span class="phone" id="phoneSpan" data-attr="telefon"><?= $model->telefon ?></span>
					<span style="display:none" class="declineButton btn-red" onclick="declineCode();">отменить</span>
					<span class="changePhoneNumber btn-green" onclick="changePhoneNumber();">изменить</span>
					<span style="display:none" class="getCodeButton btn-green" onclick="getCodeNumber();">получить код</span>
					<span style="display:none" class="acceptButton btn-green" onclick="acceptCode();">подтвердить</span>
				<?php endif; ?>
			</td>
		</div>
	</div>
	<div style="clear: both"></div>
</div>