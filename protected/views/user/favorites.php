<?php
/* @var $this Controller */
/* @var $model Users */
/* @var $clinics Address[] */
/* @var $doctors Doctors[] */
?>

<div class="search-result-title"><h2>Клиники</h2></div>
<div class="favorites">
<?php foreach ($clinics as $clinic): ?>
	<div class="title-row">
		<a href="<?php
		echo $this->createUrl('/clinic/view', array('link' => $clinic->link))
		?>"><?= $clinic->getName() ?></a>
		<?=
			CHtml::link(
					CHtml::tag('div', array(), $clinic->isFavorite ? 'Удалить' : 'В избранное'), array('/doctor/favorite', 'link' => $clinic->link), array(
				'class' => 'btn-green f-right btnFav user_fav_btn ' . ( $clinic->isFavorite ? 'added' : ''),
				'title' => $clinic->isFavorite ? 'Удалить из избранного' : 'Добавить в избранное',
				'ajax' => array(
					'url' => array('/clinic/favorite', 'link' => $clinic->link),
					'data' => '',
					'dataType' => 'json',
					'type' => 'POST',
					'success' => 'function(data){									
									/*
									if ( data[0] )
										$("a.btnFav").addClass("added").attr("title", data[1]).find("div").html(data[1]);
									else
										$("a.btnFav").removeClass("added").attr("title", data[1]).find("div").html(data[1]);
									*/
								}',
				)
			))
			?>
	</div>
<? endforeach; ?>
<br><br>
<div class="search-result-title"><h2>Врачи</h2></div>
<?php foreach ($doctors as $doctor): ?>
	<div class="title-row">
		<a href="<?php
		echo $this->createUrl('/doctor/view', array('link' => $doctor->link))
		?>"><?= $doctor->name ?></a>
			<?=
			CHtml::link(
					CHtml::tag('div', array(), $doctor->isFavorite ? 'Удалить' : 'В избранное'), array('/doctor/favorite', 'link' => $doctor->link), array(
				'class' => 'btn-green f-right btnFav user_fav_btn ' . ( $doctor->isFavorite ? 'added' : ''),
				'title' => $doctor->isFavorite ? 'Удалить из избранного' : 'Добавить в избранное',
				'ajax' => array(
					'url' => array('/doctor/favorite', 'link' => $doctor->link),
					'data' => '',
					'dataType' => 'json',
					'type' => 'POST',
					'success' => 'function(data){
									if ( data[0] )
										$("a.btnFav").addClass("added").attr("title", data[1]).find("div").html(data[1]);
									else
										$("a.btnFav").removeClass("added").attr("title", data[1]).find("div").html(data[1]);
								}',
				)
			))
			?>
	</div>
<? endforeach; ?>
</div>