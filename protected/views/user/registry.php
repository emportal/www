<?
/* @var $this Controller */
/* @var $model User */
/* @var $comments Companies[]|Doctors[] */

$this->breadcrumbs = array(
	'Личный кабинет' => '/user/index',
	'Регистратура'
);
?>
<?php if ( Yii::app()->user->hasFlash('registrySuccess') ): ?>
	<div class="flash-success">
		<span><?= Yii::app()->user->getFlash('registrySuccess') ?></span>
	</div>
<?php endif; ?>
<?php if (isset(Yii::app()->session['reachedGoal_appointment_as_old_user'])): ?>
	<script>
	$(window).load(function() {
			window.setTimeout(function() {
				countersManager.reachGoal({
					'forCounters': ['yandex', 'google'],
					'goalName': 'appointment_as_old_user',
					'callback': function() {console.log('callback: appointment_as_old_user');}
				});
			}, 100);
		});
	</script>
	<?php unset(Yii::app()->session['reachedGoal_appointment_as_old_user']); ?>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('visited')): Yii::app()->user->getFlash('visited'); ?>
	<div class="flash-success">
		<p>Ваша заявка на прием успешно отправлена в медицинское учреждение!</p>
		<p>
			Учреждение: <?= CHtml::encode($lastAppointment->company->name) ?>, <?= CHtml::encode($lastAppointment->address->street) ." ". CHtml::encode($lastAppointment->address->houseNumber) ?>
			<br>Время приема: <?= CHtml::encode($lastAppointment->plannedTime) ?>
			<br>Врач: <?= CHtml::encode($lastAppointment->doctor->name) ?>
		</p>
		<p>
			Как только специалисты клиники обработают вашу заявку, вы получите подтверждение в виде смс или звонка представителя клиники. Для обеспечения высокого качества обслуживания операторы Единого Медицинского Портала будут контролировать процесс обработки вашей заявки сотрудниками клиники.
		</p>
		<p>
		<?php
		if (!$model->hasEmailNotifications(1) && $model->email)
		{
			echo '<br><h4 class="vmargin10">Подписаться на новости ЕМП</h4>';
			$emailBlock = new EmailBlock;
			$emailBlock->options = [
				'blockId' => 'trackPatientInfoEmailBlockFromAppointment',
				'emailAttributes' => [
					'data-type' => '1'
				],
				'fixedEmail' => $model->email
			];
			$emailBlock->run();
		}
		?>
		</p>
	</div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('visitedService')): Yii::app()->user->getFlash('visitedService'); ?>
	<div class="flash-success">
		<p>Ваша заявка на прием успешно отправлена в медицинское учреждение!</p>
		<p>
			Учреждение: <?= CHtml::encode($lastAppointment->company->name) ?>, <?= CHtml::encode($lastAppointment->address->street) ." ". CHtml::encode($lastAppointment->address->houseNumber) ?>
			<br>Дата приема: <?= date("Y-m-d", strtotime(CHtml::encode($lastAppointment->plannedTime))) ?>
		</p>
		<p>
			Как только специалисты клиники обработают вашу заявку, вы получите подтверждение в виде смс или звонка представителя клиники. Для обеспечения высокого качества обслуживания операторы Единого Медицинского Портала будут контролировать процесс обработки вашей заявки сотрудниками клиники.
		</p>
	</div>
<?php endif; ?>



<div class="vhidden">
	<div class="f-right">
		<?= CHtml::link('Актуальные', array(''), array('class'=>'filter_link'))?>
		/
		<?= CHtml::link('Все записи', array('','filter'=>'all'), array('class'=>'filter_link'))?>
	</div>
</div>
<h1 class="registry-title view-hidden-ms">РЕГИСТРАТУРА</h1>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'	 => $dataProvider,
	'summaryText'	 => '',
	'itemsCssClass'	 => 'service-table',
	'filterSelector' =>'{filter}, a.filter_link',
	'columns'		 => array(
		/* array(
			'header'	 => 'Дата записи',
			'value'	 => 'Yii::app()->dateFormatter->formatDateTime($data->createdDate, "medium", null);',
			'type'	 => 'html',
			'filter' => ''
		), */
		array(
			'header'	 => 'm',
			'value'	 => function($data) {
				$name = $data->company->name .", <br>". $data->address->street ." ". $data->address->houseNumber;
				$urls = (is_object($data->address)) ? $data->address->getPagesUrls() : [];
				
				$dataResult = '<div class="col-date-nameclinik"><div class="date-m">'.$data->registryPlannedTime.'</div> '.CHTML::link($name,(isset($urls[0])?$urls[0]:'')).'</div><div style="clear:both"></div>';
				$dataResult .= '<div class="col-reg-table-dop">Врач/услуга: '.$data->visitTypeSubject.'</div>';
				$dataResult .= '<div class="col-reg-table-dop">Статус: '.CHtml::encode($data->getStatusText()).'</div>';
				$dataResult .= '<div class="col-reg-table-dop">Действие: '.$data->statusAction.'</div>';
				//$data = $data->registryPlannedTime.' '.$data->visitTypeSubject;
				return $dataResult;
			},
			'type'	 => 'html',
			'filter' => '',
			'htmlOptions'=>array('class'=>'mobile'),
		),
		array(
			'header'	 => 'Время приема',
			'value'	 => '$data->registryPlannedTime',
			'type'	 => 'html',
			'filter' => '',
		),
		array(
			'header' => 'Клиника',
			'value' => function($data) {
					$name = $data->company->name .", <br>". $data->address->street ." ". $data->address->houseNumber;
					$urls = (is_object($data->address)) ? $data->address->getPagesUrls() : [];
					return CHTML::link($name,(isset($urls[0])?$urls[0]:''));
				},
			'type'	 => 'raw',
		),
		array(
			'header' => 'Врач/Услуга',
			'value'	 => '$data->visitTypeSubject',
			'value' => function($data) {
					$name = $data->visitTypeSubject;
					$value = "";
					if(is_object($data->doctor)) {
						$urls = $data->doctor->getPagesUrls();
						$value = CHTML::link($name,(isset($urls[0])?$urls[0]:''));
					} else {
						$value = $name;
					}
					return $value;
				},
			'type'	 => 'raw',
		),/*
		array(
			'header' => 'Услуга',
			'type'	 => 'raw',
			'value'	 => '$data->service ? CHtml::link($data->service->name, array(
				"/clinic/price",
				"link"		 => $data->address->link,
				"activity"	 => $data->service->companyActivite->link,
					)
			) : ""',
		),*/
		array(
				'header'	 => 'Статус',
				'value'	 => 'CHtml::encode($data->getStatusText())',
				'type'	 => 'html',
		),
		array(
			'header'	 => 'Действие',
			'type'		 =>	'raw',
			'value'		 => '$data->statusAction',
			'htmlOptions' => array(
				'style'=>'width: 140px; text-align: center;'
			)
		),
		/*array(
			'class'		 => 'CButtonColumn',
			'header'	 => 'Действие',
			'template'	 => '{comment}',
			'buttons'	 => array(
				'comment' => array(
					'label'	 => 'Оставить отзыв',
					'url'	 => 'CHtml::normalizeUrl(array("/user/comment","link"=>$data->link))'
				),
			),
		),*/
	),
));


?>
