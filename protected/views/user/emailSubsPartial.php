<?php
/* $model = Yii::app()->user->model */
?>

<a href="#emailSubs" id="showEmailSubsLink" name="emailSubs">Управление подписками</a>

<!-- модальное окно -->
<div id="emailSubsModal" class="md-modal mx-show">
	<div class="md-close">X</div>
	<div class="md-content">
		<div class="md-form-container">
			
		</div>
	</div>
</div>
<div class="md-overlay mx-show"></div>
<!-- //модальное окно -->

<div style="display: none;">
<!-- подписки пользователя -->
<div id="emailSubsList">
	<?php if (!$model->hasEmailNotifications(3) || !$model->hasEmailNotifications(1) || !$model->hasEmailNotifications(5) ) : ?>
	<div class="fullwidth">
		<h4>Управление подписками</h4>
		<div class="data-table data-table-no-border vmargin10 fullwidth">
			<?php if (!$model->hasEmailNotifications(3)) : ?>
			<div>
				<div style="width: 12px;">
					<?php
					$emailBlock = new EmailBlock;
					$emailBlock->options = [
						'blockId' => 'trackPatientInfoEmailBlockPopup',
						'emailAttributes' => [
							'data-type' => '3'
						],
						'fixedEmail' => $model->email,
						'inputClasses' => 'displaynone',
					];
					$emailBlock->run();
					?>
				</div>
				<div style="text-align: left;">
					Подписаться на уведомления о записи на прием
				</div>
			</div>
			<?php endif; ?>
			<?php if (!$model->hasEmailNotifications(5)) : ?>
			<div>
				<div style="width: 12px;">
					<?php
					$emailBlock = new EmailBlock;
					$emailBlock->options = [
						'blockId' => 'empEmailNotification',
						'emailAttributes' => [
							'data-type' => '5'
						],
						'fixedEmail' => $model->email,
						'inputClasses' => 'displaynone',
					];
					$emailBlock->run();
					?>
				</div>
				<div style="text-align: left;">
					Подписаться на уведомления от ЕМП
				</div>
			</div>
			<?php endif; ?>
			<?php if (!$model->hasEmailNotifications(1)) : ?>
			<div>
				<div style="width: 12px;">
					<?php
					$emailBlock = new EmailBlock;
					$emailBlock->options = [
						'blockId' => 'empEmailSub',
						'emailAttributes' => [
							'data-type' => '1'
						],
						'fixedEmail' => $model->email,
						'inputClasses' => 'displaynone',
					];
					$emailBlock->run();
					?>
				</div>
				<div style="text-align: left;">
					Подписаться на новости ЕМП
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>
	<div class="fullwidth">
		<h4>Список ваших email подписок</h4>
	</div>
	<div class="data-table vmargin10 fullwidth">
		<div id="emailSubsTHeader">
			<div>
				Тип подписки
			</div>
			<div>
				Детали
			</div>
			<div>
				Действия
			</div>
		</div>
		
		<?php 
		//подписки пользователя
		$searchAttributes = [
			'email' => $model->email
		];
		$emailSubs = EmailSubscription::model()->findAllByAttributes($searchAttributes);
		if ( count($emailSubs) > 0 ) : 
		?>
		<?php foreach($emailSubs as $emailSub) : ?>
		<div>
			<div>
				<?= EmailSubscription::$types[ $emailSub->type ] ?>
			</div>
			<div>
				<?= ($emailSub->type == 2 || $emailSub->type == 4) ? $emailSub->company->name : 'ЕМП' ?>
			</div>
			<div>
				<a href="#" data-id="<?= $emailSub->id ?>" data-type="<?= $emailSub->type ?>" class="btn-warning btn-compact emailSignOff">отписаться</a>
			</div>
		</div>
		<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
</div>
<script>
var emailSubsModal = {}; //visisble globally

$(function() {
	
	/* иницилизация модального окна */
	var options = {
		'id': 'emailSubsModal',
		'templateId': 'emailSubsList',
	};
	emailSubsModal = new ModalWindowConstructor(options);
	$('#showEmailSubsLink').click(function() {
		emailSubsModal.show();
		//return false;
	});
	
	/* иницилизация события "отписаться" */
	var $emailSignOffButtons = $('.emailSignOff');	
	if ($emailSignOffButtons.length > 0) {
		$emailSignOffButtons.each(function(){
			$(this).click(function() {
				
				var $emailSignOffButton = $(this);
				var emailSubId = $emailSignOffButton.attr('data-id');
				var action = ($emailSignOffButton.attr('data-action') == 'subscribe') ? 'subscribe' : 'unsubscribe';
				var params = {
					'subId': emailSubId
				};
				
				$.ajax({
					url: '/ajax/' + action, /* subscribe|unsubscribe */
					async: true,
					data: params,
					type: 'POST',
					success: function(data) {
						//var newLocation = window.location.protocol + '//' + window.location.hostname + '/user/profile#emailSubs';
						//document.location.reload(newLocation);
						document.location.reload(true);
						$emailSignOffButton.parent().parent().animate(
							{
								opacity: 0
							}, 
							300, 
							function() {
								$(this).remove();
							}
						);
					}
				});
				
				return false;
			});
		});
	}
	
	if (window.location.hash == '#emailSubs') emailSubsModal.show();
});
</script>