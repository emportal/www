<?php
/* @var $this UserController */
/* @var $model Doctors */
/* @var $comment Comments */

$this->breadcrumbs = array(
	'Личный кабинет' => '/user/index',
	'Оставить отзыв о '.($this->action->id == 'comment' ? "клинике" : "докторе")
);

$path = Yii::getPathOfAlias('shared');
$pathTojRating = $path . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'jRating' . DIRECTORY_SEPARATOR;
$urlScript = Yii::app()->assetManager->publish($pathTojRating . 'jRating.jquery.js');
Yii::app()->clientScript->registerScriptFile($urlScript, CClientScript::POS_END);
$urlcss = Yii::app()->assetManager->publish($pathTojRating . 'jRating.jquery.css');
Yii::app()->clientScript->registerCssFile($urlcss);
?>

				<div class="search-result-title"><h2>Оставить отзыв о <?=$this->action->id == 'comment' ? "клинике" : "докторе"?></h2>
				<br />
				Посещение <?php echo Yii::app()->dateFormatter->formatDateTime($model->plannedTime, "medium", null);?> <?php echo $model->company->name?>
				</div>
				<?php

				$form	 = $this->beginWidget('CActiveForm',
								array(
					'id'					 => 'comment-form',
					'enableAjaxValidation'	 => false,
						));
				/* @var $form CActiveForm */
				?>
				
				<table class="service-table">
					<!--<tr>
						<td valign="middle">Ваша оценка</td>
						<td>
							<?php
							#echo $form->dropDownList($comment, 'rating', Comment::$ratingList,
							#	array('class' => 'custom-select')); 
							?>
						</td>
					</tr>-->
					
					<?php/* foreach($types as $key=>$value):*/ ?>
						<tr class="radiobuttons_horizontal">
							<td valign="middle" class="valign-mid"><?=$value?></td>

						</tr>
					<?php/* endforeach; */?>

					<tr id="ratingDoctor_block">
						<td>
							<label for="Review_ratingDoctor" class="required">Рейтинг врача <span class="required">*</span></label>
						</td>
						<td>
							<div class="jRatingView" data-average="5" data-id="1" id="Review_ratingDoctor" style="display:inline-block">
								<input name="Review[ratingDoctor]" type="hidden">
							</div>
							<div class="errorMessage" id="ratingDoctor_Message" style="display:none; color:red;">Поставьте оценку!</div>
						</td>
					</tr>
			
					
					<?php
					/*<tr>
						<td valign="middle" class="valign-mid">Рекомендую</td>
						<td valign="middle"><?=$form->checkbox($comment,'isRecomend',array(
							'style'=>'margin-top:8px;'
						));?></td>
					</tr>*/
					?>
					<tr>
						<td>Текст отзыва</td>
						<td>
							<?php
							echo $form->textArea($comment, 'reviewText',
							array('class' => 'custom-textarea'));
							?>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="submit" class="btn-green" title="Отправить">
								Отправить
							</button>
						</td>
					</tr>
				</table>
				<?php $this->endWidget(); ?>

	<script>
		$(document).ready(function(){
			$('.title-row').next('table').css('display','none');
			$('.title-row').toggle(
			function(){
				$('.title-row').next('table').hide('slow');
				$(this).next('table').show('slow');
			},
			function(){
				$(this).next('table').hide('slow');
			}
		);
			initRatings($( ".service-table" ));
		});
		function initRatings(form) {

			form.find(".jRatingView").jRating({
				step:true,
				showRateInfo:false,
				length : 5, // nb of stars
				decimalLength : 1,
				rateMax : 5,
				nbRates : 9999999999,
				sendRequest:false,
				canRateAgain:true,
				onSuccess : function(){
					jSuccess('Success : your rate has been saved :)',{
						HorizontalPosition:'center',
						VerticalPosition:'top'
					});
				},
				onError : function(){
					jError('Error : please retry');
				},
				onClick : function(element,rate) {
					if(rate <= 0 || isNaN(rate)) rate = '';
					else rate /= 5;
					$(element).find("input").val(rate);
				},
			});

			form.find('#Review_ratingDoctor input').val('');
			form.find('#Review_ratingClinic input').val('');
			form.find('#Review_ratingDoctor.rating-container .star').click(function() {
				$('#Review_ratingDoctor.rating-container .star').prop('src', '/images/Five-pointed_star_1.png');
				$(this).prevAll('.star').addBack().prop('src', '/images/Five-pointed_star_2.png');
				$('#Review_ratingDoctor input').val($(this).attr("name"));
			});
			form.find('#Review_ratingClinic.rating-container .star').click(function() {
				$('#Review_ratingClinic.rating-container .star').prop('src', '/images/Five-pointed_star_1.png');
				$(this).prevAll('.star').addBack().prop('src', '/images/Five-pointed_star_2.png');
				$('#Review_ratingClinic input').val($(this).attr("name"));
			});

			if(form.find('#Review_doctorId').val().trim() == "") {
				form.find('#ratingDoctor_block').hide();
			}

			try {
				form.find("#phonetoConfirmInput").mask("+79999999999", {placeholder: "_"});
			}
			catch(e) {}
		};
	</script>
</div>