<?
/* @var $this Controller */
/* @var $comments Companies[]|Doctors[] */
/* @var $clinics Companies[] */
/* @var $doctor Doctors[] */

$this->breadcrumbs = array(
	'Личный кабинет' => '/user/index',
	'Оставить отзыв'
);

?>
<script>
	$(document).ready(function() {
		$('.title-row').next('table').css('display', 'none');
		$('.title-row').toggle(
				function() {
					$('.title-row').next('table').hide('slow');
					$(this).next('table').show('slow');
				},
				function() {
					$(this).next('table').hide('slow');
				}
		);
	});
</script>

<h1 class="prof-title">МОИ ОТЗЫВЫ</h1>
<div class="comments-con">
	<div class="comments-row-1">
		<div class="title-row">Врачи</div>
		<ul>
			<?php foreach ($doctors as $doc):
				$r = $doc->reviews[0]->attributes['ratingDoctor'] * 5;
				?>
				<li>
					<div>
						<span class="name-elem"><?= $doc->attributes['name'] ?></span>
						<div class="star-block">
							<div class="doctor_rate_block<?= (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) ? ' vhidden' : '' ?>">
								<input type="range" min="0" max="5" value="<?= $r ?>" step="0.1" id="backing-<?= $doc->attributes['link'] ?>" style="display: none;">
								<span id="rate-<?= $doc->attributes['link'] ?>" data-rateit-value="<?= $r ?>" data-rateit-ispreset="true" style="overflow: hidden" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $doc->attributes['link'] ?>"></span>
								<button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
								<span class="rating-text"><?= $r ?></span>
							</div>
						</div>
						<div class="text-block">
							<?=  CHtml::encode($doc->reviews[0]->attributes['reviewText']); ?>
						</div>
						<div class="date-block"><?= $doc->reviews[0]->attributes['createDate'] ?></div>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
	</div>
	<div class="comments-row-2">
		<div class="title-row">Клиники</div>
		<ul>
			<?php
			foreach ($clinics as $clinic):
				$r = $clinic->attributes['ratingClinic'] * 5;
				?>
					<li>
						<div>
							<span class="name-elem"><?= $clinic->company->attributes['name'] ?></span>
							<div class="star-block">
								<div class="doctor_rate_block<?= (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) ? ' vhidden' : '' ?>">
									<input type="range" min="0" max="5" value="<?= $r ?>" step="0.1" id="backing-<?= $clinic->company->attributes['link'] ?>" style="display: none;">
									<span id="rate-<?= $clinic->company->attributes['link'] ?>" data-rateit-value="<?= $r ?>" data-rateit-ispreset="true" style="overflow: hidden" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?=$clinic->company->attributes['link'] ?>"></span>
									<button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
									<span class="rating-text"><?= $r ?></span>
								</div>
							</div>
							<div class="text-block">
								<?=  CHtml::encode($clinic->attributes['reviewText']); ?>
							</div>
							<div class="date-block"><?= $clinic->attributes['createDate'] ?></div>
						</div>
					</li>
				<? endforeach; ?>
		</ul>
	</div>
	<div style="clear: both"></div>
</div>


