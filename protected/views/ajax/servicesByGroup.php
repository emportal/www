<?php Yii::app()->clientScript->reset(); ?>
<div>
	<?php if ( count($groups) ): ?>
		<?php foreach ($groups as $key => $group): ?>
			<?= CHtml::link(CHtml::encode($group), '#', array('class' => 'choose-service', 'rel'=>$key)) ?>
		<?php endforeach; ?>
	<?php endif; ?>
</div>