<?php Yii::app()->clientScript->reset(); ?>
<div>
	<?php if ( count($groups) ): ?>
		<table id="service">
			<tr>
				<?php
				$columns = 3;
				$i		 = 1;
				?>
				<?php foreach ($groups as $key => $group): ?>
					<td>
						<?= CHtml::link(CHtml::encode($group), '#', array('class'	 => 'choose-service', 'rel'	 => $key)) ?>
					</td>
					<?= ++$i > $columns ? '</tr><tr>' . (!$i = 1) : ''; ?>
				<?php endforeach; ?>
			</tr>
		</table>
	<?php endif; ?>
</div>