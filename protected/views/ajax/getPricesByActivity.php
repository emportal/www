<div class="search-result-title">
	<h2>
		<?= CHtml::encode($activity->name) ?>
	</h2>
</div>
<table class="service-table">
	<?php if (empty($prices)): ?>
		<tr>
			<td class="service-title">нет данных</td>
		</tr>
	<?php else: ?>
		<?php foreach ($prices as $price): ?>
			<tr>
				<td class="service-title"><?= CHtml::encode($price->service->name) ?></td>
				<td class="service-price"><?= CHtml::encode($price->price) ?> р.</td>
				<td class="service-signup">
					<?=
					Html::link(
							'Записаться', array(
						'clinic/visit',
						'link' => $model->link,
						'service' => $price->service->link
							), array('class' => 'btn-green-min'))
					?>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>  
</table> 