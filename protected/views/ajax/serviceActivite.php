<?php Yii::app()->clientScript->reset(); ?>
<div>
	<table id="service">
		<tr>
			<?php $columns = 3; $i=1;?>
			<?php foreach ($groups as $key => $group): ?>
				<td>
				<?= CHtml::link(CHtml::encode($group), array('/ajax/ServiceSectionByServiceActivite', 'link' => $key), array('class' => 'fancybox')) ?>
				</td>
				<?= ++$i > $columns ? '</tr><tr>'.(!$i=1) : '';?>
			<?php endforeach; ?>
		</tr>
	</table>
</div>