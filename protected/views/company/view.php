<?php
/* @var $model Companies */
?>

<div class="search-result">
	<div class="crumbs">
		<a href="#">Главная</a> > 
		<a href="#">Каталог</a> > 
		<a href="#">Клиники</a> > 
		<span><?= CHtml::encode($model->CompaniesName) ?></span>
	</div>
	<table class="search-result-table">
		<tr class="search-result-box">
			<td class="search-result-signup">
				<img src="/shared/images/pic-9.png" width="240" height="103" alt="1">
				
				<?php if (($model->userMedicals->agreementNew != 1 || !$model->isActive) AND 1==2): ?>			

					<p style="font-size: 15px; line-height: 20px;">Дорогой пациент, запись на прием в клинику <?= CHtml::encode($model->company->name) ?> через Единый Медицинский Портал более недоступна. Рекомендуем записаться на прием в другие ближайшие к вам клиники</p>	
					<div class="clearfix"></div>
					<div class="similar-doctors">		
						<div class="specialty-type">
							<div class="row">
								<?php $count = 0; ?>
							<?php foreach($similar as $key=>$clinic): $count++; ?>
								<?php 
									$this->renderPartial('//search/_clinics_unique',[
										'data' => $clinic,
										'address' => $clinic->addresses[0],
										'addresses' => null,
										'big' => 0,					
									]);
								?>
								<?= $count % 2 == 0 && $count !== count($similar) ? '<div class="clearfix"></div></div><div class="row">' : ''; ?>
							<?php endforeach; ?>
								<div class="clearfix"></div>
							</div>
						</div>		
						<div class="clearfix"></div>
					</div>
					<div style="margin-bottom:10px">
						<div class="size-14">			
							<a style="display:inline-block;margin-top:10px" href="<?= $this->createUrl('search/clinic',['metro' => 'metro-'.$metroLink]) ?>">Посмотреть еще рядом</a>
							
						</div>
						<?php if (empty(Yii::app()->request->cookies['clinic' . $model->link])): ?>
								<div class="report-clinic">
									Не нашли ничего подходящего?
									<span data-id="<?= $model->link ?>" class="btn btn-blue" style="">
										Пожаловаться на отсутствие клиники
									</span>
								</div>
						<?php endif; ?>
					</div>
				
				<?php else: ?>
				
				
					<a href="#" title="Записаться на прием" class="btn-green">Записаться на прие1м</a>
				
				
				<?php endif; ?>
				
				<br><br>
				<a href="#" class="btn w100"  title="Описание">Описание</a><br>
				<a href="#" class="btn w100"  title="Специалисты">Специалисты</a><br>
				<a href="#" class="btn w100"  title="Стоимость услуг">Стоимость услуг</a><br>
				<a href="#" class="btn w100"  title="Отзывы">Отзывы</a><br>
				<a href="#" class="btn w100"  title="Фотогалерея">Фотогалерея</a><br>
				<a href="#" class="btn w100"  title="Акции">Акции</a>
			</td><!-- /search-result-signup -->
			<td class="search-result-info">
				<div class="search-result-title">
					<h2>
						<a href="#" title="<?= CHtml::encode($model->CompaniesName) ?>"><?= CHtml::encode($model->CompaniesName) ?></a>
						<small><?= CHtml::encode($model->companyType->CompanyTypeName) ?></small></h2>
				</div><!-- /search-result-title -->
				<table class="service-table">
					<tr>
						<td>
							<h5>
								Главный офис:<br>
								<small>
									<?= CHtml::encode(reset($model->addresses)->AddressesName) ?>
									(<a href="#">смотреть на карте</a>)
								</small>
							</h5>

							<p class="w50">
								<span class="size-14">Телефоны:</span>
								<?php foreach ($model->phones as $phone): ?>
								<br>(<?= CHtml::encode($phone->PhonesCitiesCode) ?>) <?= CHtml::encode($phone->PhonesNumber)?>
								<? endforeach; ?>
							</p> 
							<p class="w50"><span class="size-14">Часы работы:</span><br>
								ПН-ПТ с 10 до 18<br>
								СБ-ВС выходной
							</p>
							<p class="w50">
								<span class="size-14">WWW:</span> <a href="#">www.roga.ru</a><br>
								<span class="size-14">Email:</span> <a href="#">info@roga.ru</a><br>
								<strong>Дистрибьюторские договора:</strong> договор 1, договор 2, договор 3<br>
								<strong>Лицензии на деятельность:</strong> лицензия 1, лицензия 2, лицензия 3
							</p> 
							<p class="w50">
								<strong>Генеральный директор</strong><br>
								Пупкин Василий Олегович<br>
								<span class="size-14">Тел.:</span> (812) 222-22-22 доб. 145<br>
								<span class="size-14">Email:</span> <a href="#">vasya@roga.ru</a>
							</p>
						</td>
					</tr>
					<tr><td>
							<h5>Филиал Волхов:<br> 
                                <small>187403, Ленинградская обл., Волховский р-н, Волхов, ул. Новгородская, 3 (<a href="#">смотреть на карте</a>)</small></h5>

							<p class="w50">
								<span class="size-14">Телефоны:</span><br>
								(812) 222-12-20<br>
								(812) 222-12-20
							</p> 
							<p class="w50">
								<span class="size-14">Часы работы:</span><br>
								ПН-ПТ с 10 до 18<br>
								СБ-ВС выходной
							</p>
							<p class="w50">
								<span class="size-14">WWW:</span> <a href="#">www.roga.ru</a><br>
								<span class="size-14">Email:</span> <a href="#">info@roga.ru</a><br>
							</p> 
							<p class="w50">
								<strong>Контактное лицо</strong><br>
								Пупкин Василий Олегович<br>
								<span class="size-14">Тел.:</span> (812) 222-22-22 доб. 145<br>
								<span class="size-14">Email:</span> <a href="#">vasya@roga.ru</a>
							</p>
						</td>
					</tr>
					<tr>                            
						<td><strong>Год основания:</strong> 1998</td>
					</tr>
					<tr>
						<td><strong>Юридическое лицо:</strong> </td>
					</tr>
					<tr>
						<td><strong>Лицензии:</strong> </td>
					</tr><!-- /search-result-info -->  
				</table>
			</td>
		</tr>                                           
	</table>
</div>