<?php
/* @var $this NewsController */
/* @var $model News */
if(!empty(trim($model->tagDescription))) Yii::app()->clientScript->registerMetaTag($model->tagDescription, 'description');

$arrMount = [
	'01' => "ЯНВАРЯ",
	'02' => "ФЕВРАЛЯ",
	'03' => "МАРТА",
	'04' => "АПРЕЛЯ",
	'05' => "МАЯ",
	'06' => "ИЮНЯ",
	'07' => "ИЮЛЯ",
	'08' => "АВГУСТА",
	'09' => "СЕНТЯБРЯ",
	'10' => "ОКТЯБРЯ",
	'11' => "НОЯБРЯ",
	'12' => "ДЕКАБРЯ"
];
$dPublishMain = new DateTime($model->datePublications);

$this->breadcrumbs = [
		'Блог' => '/',
		$model->name, 
];
?>
<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_update_counters&params[type]=news&params[link]=<?=$model->link?>" -->
</div>
</section>
<script async src="https://usocial.pro/usocial/usocial.js?v=3.0.0" data-script="usocial" charset="utf-8"></script>
<div class= "main-img view-hidden-ms" style="background: url('<?=$model->getMainImage(); ?>'); background-position: center; background-size: cover;">
	<?php
	if($postTags > 0):
	?>
	<div class="tag-menu">
		<div class="bg-color"></div>
		<div class="clouse-tag-menu">
			<div class="ico-clouse"></div>
			<span>ЗАКРЫТЬ</span>
		</div>
		<ul class="tag-menu-list">
			<?php foreach ($postTags as $tag):
				echo "<li>".CHtml::link(mb_strtoupper(str_replace('_', ' ', $tag->name)),array((Yii::app()->params['isBlogDomen']?'':'/news').'/'.$tag->name))."</li>";
			endforeach; ?>
		</ul>
	</div>
	<?php
	endif;
	?>
	<div class="grad-block">
		<h1 class="title-main-post"><?=mb_strtoupper($model->name); ?></h1>
	</div>
	<div class="main-image-data">
		<div class="count-view"><?=$model->views?> ПРОСМОТР<?=($model->views == 1)?'':(($model->views > 1 && $model->views < 5 && $model->views != 0)?'А':'ОВ')?></div>
		<div class="datatime-main-post"><?=$dPublishMain->format('d') .' '. $arrMount[$dPublishMain->format('m')] .' '. $dPublishMain->format('Y') ?></div>
	</div>
</div>
<div class="title-col-ms hidden-md">
	<h2><?=mb_strtoupper($model->name); ?></h2>
	<img src="<?=$model->getMainImage(); ?>">
</div>
<section class="main_column detail-blog col-ms">
	<div class="content">
<table cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<div class="news-detail">
					<div class="news-info-p">
						<i style="color: rgb( 79, 79, 79 );"><strong><?=$model->preview;?></strong></i>
						<!-- uSocial -->
						<div class="uSocial-Share" data-pid="846fac4637591433fbd18e48915ca67e" data-type="share" data-options="round-rect,style1,absolute,horizontal,size48,eachCounter1,counter0,nomobile" data-social="vk,fb,twi,ok"></div>
						<!-- /uSocial -->
						<?=((!$model->mainImage)? preg_replace('/<img[^>]+\>/', '', $model->formattedText, 1):$model->formattedText); ?>
						<?php if($model->author) : ?>
							<div style="margin-bottom: 10px;">
								<hr>
								<?php if($model->author->foto) : ?>
									<img src="<?=$model->author->foto;?>" style="width:275px;">
								<?php endif; ?>
								<p>
									<a href="/?author=<?=$model->author->link;?>"><em><strong><?=$model->author->name;?></strong></em></a>
								</p>
								<?php if($model->author->info) : ?>
									<p>
										<em><?=$model->author->info;?></em>
									</p>
								<?php endif; ?>
							</div>
						<?php endif; ?>

					</div>
					<? if(count($model->tags) > 0): ?>
					<div class="block-tags hidden-ms">
						<span class="title-tag">ТЭГИ</span>
						<?php foreach ($model->tags as $tag):
							echo CHtml::link(mb_strtoupper($tag->name),array((Yii::app()->params['isBlogDomen']?'':'/news').'/'.$tag->name));
						 endforeach; ?>
					</div>
					<?php
					endif;
					?>
					<div class="shared-button" style="float: left;margin-top: -10px;">
						<!-- uSocial -->
						<div class="uSocial-Share" data-pid="846fac4637591433fbd18e48915ca67e" data-type="share" data-options="round-rect,style1,absolute,horizontal,size48,eachCounter1,counter0,nomobile" data-social="vk,fb,twi,ok"></div>
						<!-- /uSocial -->
					</div>
					<?php if ($model->urlfeed): ?>
						<div class="align-right" style="margin-top:15px"><i>Источник: <?= $model->urlfeed ?></i></div>
					<?php endif; ?>

					<style type="text/css">
						.btn-nav {
						    width: 185px;
    						background: #ffffff;
						    color: #04b0cf;
						    border: 1px solid #dddddd;
						    padding: 7px 0;
						    display: block;
						    text-align: center;
						    text-decoration: none;
						    border-radius: 5px;
						    font-size: 15px;
						}
						.btn-nav-before:before {
							content: '< предыдущая статья';
						}
						.btn-nav-after:before {
							content: 'следующая статья >';
						}	
					    @media (max-width: 1000px) {
					    	.btn-nav { 
								width: 30px;
					    	}
							.btn-nav-before:before {
								content: '<';
							}
							.btn-nav-after:before {
								content: '>';
							}								
					    }
					</style>
					<div style="margin-top: 100px;">
					<?php if($model->getBeforeNews()): ?>
						<div style="float: left;"><a class="btn-nav btn-nav-before" href="/<?=$model->getBeforeNews()->link;?>.html" title="предыдущая статья"></a></div>
					<?php endif; ?>
					<?php if($model->getAfterNews()): ?>
						<div style="float: right;"><a class="btn-nav btn-nav-after" href="/<?=$model->getAfterNews()->link;?>.html" title="следующая статья"></a></div>
					<?php endif; ?>
					</div>
					<div class="clearfix hidden-ms" style="margin-bottom: 15px;"></div>
				</div>
			</td>
			<td class="col3">
				<div class="clearfix"></div>
				<?php /* 
				<div class="news-sidebar">
					<?php if (!empty($model->doctorListQuery)) : ?>
					<div id="doctorListTitle" class="news-info">
						<span class="news-subtitle"><?= (!empty($model->doctorListTitle)) ? $model->doctorListTitle : 'Врачи из статьи' ?></span>
					</div>
					<div id="doctorList" class="news-info small_cards"></div>
					<script>
							$.ajax({
								url: "<?= explode('?', $model->doctorListQuery)[0] ?>?cardsOnly=1&absoluteLink=1&limit=3&<?= explode('?', $model->doctorListQuery)[1] ?>",
								success: function(result){
									$("#doctorList").html(result);
								}
							});
					</script>
					<?php endif; ?>
					<?php if (!empty($model->clinicListQuery)) : ?>
					<div id="clinicListTitle" class="news-info">
						<span class="news-subtitle"><?= (!empty($model->clinicListTitle)) ? $model->clinicListTitle : 'Клиники из статьи' ?></span>
					</div>
					<div id="clinicList" class="news-info small_cards"></div>
					<script>
							$.ajax({
								url: "<?= explode('?', $model->clinicListQuery)[0] ?>?cardsOnly=1&absoluteLink=1&limit=3&<?= explode('?', $model->clinicListQuery)[1] ?>",
								success: function(result){
									$("#clinicList").html(result);
								}
							});
					</script>
					<?php endif; ?>
					<div id="doctorListTitle" class="news-info">
						<span class="news-subtitle">Новости</span>
					</div>
					<?php foreach (News::getLastNews(5, 0, $model->id) as $news): ?>
						<div class="news-block">
							<img src="/images/icons/bggray.png" class="news-block-img">
							<div class="news-block-text">
							<?= Html::link($news->title, array('news/view', 'link' => $news->link)) ?>
							<br><strong><?= Yii::app()->dateFormatter->formatDateTime($news->datePublications, 'medium', NULL) ?></strong>
							</div>
							<div class="clearfix"></div>
						</div>
					<?php endforeach; ?>
				</div>
				*/ ?>
				<div class="search-clinik-form">
					<script>
						(function(o) {
							var r = new XMLHttpRequest(),
								params = '?refererId=' + o.refererId + '&width=300';
							r.onreadystatechange = function(data) {
								if (r.readyState == 4 && r.status == 200) {
									document.write(r.responseText);
									$('[name=specialty]').parent().prepend('<div class="title-form-search">НАПРАВЛЕНИЕ</div>');
									$('[name=metro]').parent().prepend('<div class="title-form-search">МЕТРО</div>');
								}
							};
							r.open('GET', 'https://' + o.source + params, false);
							r.send();
						})({
							'source': 'emportal.ru/ajax/getSearchForm',
						});
					</script>
				</div>
				<div class="news_block" style="margin-top: -20px;">
					<div class="news_block_content">
						<?php foreach (News::getLastNews(3,0,$model->id) as $data): 
								$newsExcludeId[] = $data->id;
						?>
								<div class="blog-annonce" style=" margin-right: 0px;width:288px;height: 90px;">
									<a href="<?=$data->getUrl(false,Yii::app()->params['isBlogDomen'])?>" class="link-blog">
										<div class="img-bg" style="background: url('<?=$data->getMainImage()?>'); background-position: center; background-size: cover;"></div>
										<div class="background-opc"></div>
										<div class="title-current-annonce" style="margin-top: -15px;">
											<span style="font-size: 14pt;line-height: 20px;"><?=mb_strtoupper($data->title) ?></span>
										</div>

									</a>
								</div>
						<?php endforeach;  ?>
						<div style="clear: both"></div>
					</div>
				</div>
			</td>
		</tr>
		<?php $similarNews = $model->getSimilarNews($newsExcludeId);
			if(count($similarNews)): ?>
			<tr>
				<td>				
					<style type="text/css">
						.blog-annonce-sim {
							height: 110px !important;
						}
						@media (max-width: 1000px) {
							.blog-annonce-sim {
								height: 220px !important;
							}
						}
					</style>
					<div class="news_block" style="margin-top: -10px;">
						<div class="news_block_content">
							<h3 style="text-align: center;color: #04b0cf;margin-bottom: 15px;">Читайте материалы по теме</h3>
								<?php foreach ($similarNews as $data): 
										$newsExcludeId[] = $data->id;
								?>
										<div class="blog-annonce blog-annonce-sim" style=" margin-right: 0px;width:320px;">
											<a href="<?=$data->getUrl(false,Yii::app()->params['isBlogDomen'])?>" class="link-blog">
												<div class="img-bg" style="background: url('<?=$data->getMainImage()?>'); background-position: center; background-size: cover;"></div>
												<div class="background-opc"></div>
												<div class="title-current-annonce" style="margin-top: -15px;">
													<span style="font-size: 18pt;line-height: 20px;"><?=mb_strtoupper($data->title) ?></span>
												</div>

											</a>
										</div>
								<?php endforeach;  ?>
								<div style="clear: both"></div>
							</div>
						</div>
				</td>
				<td></td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>
	</div>
</section>
<div class="subscribe-blog col-ms">
	<div class="title-subscribe">
		СВЕЖИЕ СТАТЬИ НА ПОЧТУ
	</div>
	<div class="text-data-subscribe">
		Как не болеть,  работать по 4 часа и при этом не торчать в офисе «от звонка до звонка», жить где угодно, богатеть и прочие новости от ЕМП.
	</div>
	<div class="form-sub">
		<input type="text" name="uname" placeholder="Ваше имя">
		<input type="text" name="email" placeholder="Электронная почта">
		<button class="green-button" onClick="emailBlogSubscribe('uname', 'email')">Подписаться</button>
	</div>
	<div class="md-modal md-show modal-subscribe" style="display: none;">
		<div class="md-close">X</div>
		<div class="md-content">
			Подписка оформлена.
		</div>
	</div>

</div>
<?php
$this->renderPartial('_list', array(
	'dataProvider'	=>	$dataProvider,
	'moreNewsLink'	=> '/news'
));
?>
<section class="main_column detail-blog">
	<div class="content">
