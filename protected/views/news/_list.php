<?php
ob_start();

$pageCount = ceil($dataProvider->totalItemCount / $dataProvider->pagination->pageSize);

if(isset($moreNewsLink)):
	?>
	<div class="news_list_footer" style="display: inline-block; text-align: center; width: 100%; height: 50px; padding: 50px 0 80px 0;">
		<?php
			echo CHtml::link('Показать больше новостей','/', array('class'=>'show_more_news_btn link-show-all'));
		 ?>
	</div>
	<?php
else:
	if($pageCount > 1){
		?>
		<div class="news_list_footer" style="display: inline-block; text-align: center; width: 100%; height: 50px; padding: 50px 0 80px 0;">

				<button id="news_list_append_button"
						data-pagecount="<?=$pageCount?>" data-id="news_list" class="show_more_news_btn"
						title="Показать больше новостей" onclick="infinityList(this, 'news_list');">Показать больше новостей</button>

			<div id="news_list_loading" class="icon_loading" style="display: none;"></div>
		</div>
		<script>
		var scrollToBottomStarted = false;
		$(window).scroll(function() {
		   if(!scrollToBottomStarted && $(window).scrollTop() + $(window).height() >= $(document).height()-800) {
		       scrollToBottomStarted = true;
		       $("#news_list_append_button").click();
		       if($.fn.yiiListView.settings['news_list']["page"] < 5) setTimeout(function(){scrollToBottomStarted = false;},250);
		   }
		});
		</script>
		<?php
	}
endif;
$news_list_footer = ob_get_clean();
?>
<?php
	$this->widget('zii.widgets.CListView', array(
		'dataProvider' => $dataProvider,
		'itemView' => '_news',
		'id' => 'news_list',
		'template' => "{items}\n{$news_list_footer}",
		'itemsTagName' => 'dl',
		'pagerCssClass' => 'pages',
		'htmlOptions' => array(
			'class' => 'news_block new-list'
		),
	));
?>
