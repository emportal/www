<? /* @var $data News */ ?>
<div class="blog-annonce <?=(iconv_strlen(mb_strtoupper($data->title)) > 100)?'big-text-ms':''?>" >
	<a href="<?=$data->getUrl(false,Yii::app()->params['isBlogDomen'])?>" class="link-blog">
		<div class="img-bg" style="background: url('<?=$data->getMainImage()?>') no-repeat center #04b0cf; background-position: center; background-size: cover;"></div>
		<div class="background-opc"></div>
		<div class="title-current-annonce">
			<span><?=mb_strtoupper($data->title) ?></span>
		</div>
	</a>
</div>