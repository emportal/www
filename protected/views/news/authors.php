<?
$this->breadcrumbs = [
		'Блог' => '/',
		'Авторы'
];
foreach ($authors as $author): 
	if(!count($author->authorNews)) {
		continue;
	}
	?>

	<div style="padding-top: 15px;">
		<div style="height: 150px;">
			<div style="float: left;"><img src="<?=$author->foto ? $author->foto : '/images/newLayouts/icon_user.png'?>" style="width:200px;"></div>
			<div style="padding-top: 5px;">
				<span style="padding-left: 15px;font-size: 14pt;">
					<strong><a href="/?author=<?=$author->link;?>"><?=$author->name?></a></strong>
				</span>
				<?php if($author->info) : ?>
					<br><br>
					<span style="padding-left: 15px;font-size: 12pt;">
						<em><?=$author->info?></em>
					</span>
				<?php endif; ?>
				<br><br>
				<span style="padding-left: 15px;font-size: 12pt;">
					Новостей: <?=count($author->authorNews)?>
				</span>
			</div>

		</div>
	</div>	
	
 <?php endforeach; ?>