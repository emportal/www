</section>

<?php
$arrMount = [ '01' => "ЯНВАРЯ", '02' => "ФЕВРАЛЯ", '03' => "МАРТА", '04' => "АПРЕЛЯ", '05' => "МАЯ", '06' => "ИЮНЯ", '07' => "ИЮЛЯ", '08' => "АВГУСТА", '09' => "СЕНТЯБРЯ", '10' => "ОКТЯБРЯ", '11' => "НОЯБРЯ", '12' => "ДЕКАБРЯ"];

$this->breadcrumbs = [
		'Блог'
];
if(!empty($tags)) {
	$this->breadcrumbs = [
		'Блог' => '/',
		implode(', ', $tags)
	];
}

if(!empty($author)) {
	$this->breadcrumbs = [
		'Блог' => '/',
		'Статьи автора '.$author->name
	];
}

if(isset($firstFive[0])):
$objMainPost = $firstFive[0];
$dPublishMain = new DateTime($objMainPost->datePublications);
?>
<div class= "main-img col-ms" style="background: url('<?=$objMainPost->getMainImage(); ?>'); background-position: center; background-size: cover;">
	<?php
	if(count($postTags) > 0):
		?>
		<span class="ico-open-tag-menu"></span>
		<div class="tag-menu">
			<div class="bg-color"></div>
			<div class="clouse-tag-menu">
				<div class="ico-clouse"></div>
				<span>ЗАКРЫТЬ</span>
			</div>
			<ul class="tag-menu-list">
				<?php foreach ($postTags as $tag):
					echo "<li>".CHtml::link(mb_strtoupper(str_replace('_', ' ', $tag->name)),array((Yii::app()->params['isBlogDomen']?'':'/news').'/'.str_replace(' ', '-', $tag->name)))."</li>";
				endforeach; ?>
			</ul>
		</div>
		<?php
	endif;
	?>
	<a href="<?=$objMainPost->getUrl(false,Yii::app()->params['isBlogDomen'])?>" class="link-blog">
	<div class="grad-block">
		<div class="grad-block-sm"></div>
		<h1 class="title-main-post"><?=mb_strtoupper($objMainPost->name); ?></h1>
	</div>
	</a>
	<div class="main-image-data hidden-mx">
		<div class="count-view"><?=$objMainPost->views?> ПРОСМОТР<?=($model->views == 1)?'':(($model->views > 1 && $model->views < 5 && $model->views != 0)?'А':'ОВ')?></div>
		<div class="datatime-main-post"><?=$dPublishMain->format('d') .' '. $arrMount[$dPublishMain->format('m')] .' '. $dPublishMain->format('Y') ?></div>
	</div>
</div>
	<section class="main_column col-ms">
	<?php if(isset($firstFive[1])): ?>
	<div class="news_block">
		<div class="news_block_content">
			<?php
			for($i = 1; $i <= 4; $i++) {
				if(!isset($firstFive[$i])) break;
				?>
				<div class="blog-annonce <?=(iconv_strlen(mb_strtoupper($data->title)) > 100)?'big-text-ms':''?>" " style=" <?=(($i)%2 == 0)?'margin-right: 0px':''?>">
					<a href="<?=$firstFive[$i]->getUrl(false,Yii::app()->params['isBlogDomen'])?>" class="link-blog">
						<div class="img-bg" style="background: url('<?=$firstFive[$i]->getMainImage()?>'); background-position: center; background-size: cover;"></div>
						<div class="background-opc"></div>
						<div class="title-current-annonce">
							<span><?=mb_strtoupper($firstFive[$i]->title) ?></span>
						</div>

					</a>
				</div>
				<?php
			}
			?>
			<div style="clear: both"></div>
		</div>
	</div>
	<?php endif; ?>
	</section>
	<div class="subscribe-blog col-ms">
		<div class="title-subscribe">
			СВЕЖИЕ СТАТЬИ НА ПОЧТУ
		</div>
		<div class="text-data-subscribe">
			Как не болеть,  работать по 4 часа и при этом не торчать в офисе «от звонка до звонка», жить где угодно, богатеть и прочие новости от ЕМП.
		</div>
		<div class="form-sub">
			<input type="text" name="uname" placeholder="Ваше имя">
			<input type="text" name="email" placeholder="Электронная почта">
			<button class="green-button" onClick="emailBlogSubscribe('uname', 'email')">Подписаться</button>
		</div>
		<div class="md-modal md-show modal-subscribe" style="display: none;">
			<div class="md-close">X</div>
			<div class="md-content">
				Подписка оформлена.
			</div>
		</div>
		<div class="share-buttons-blog" >
			<!-- uSocial -->
			<script async src="https://usocial.pro/usocial/usocial.js?v=3.0.0" data-script="usocial" charset="utf-8"></script>
			<div class="uSocial-Share" data-pid="846fac4637591433fbd18e48915ca67e" data-type="share" data-options="round-rect,style1,absolute,horizontal,size48,nomobile" data-social="vk,fb,twi,ok"></div>
			<!-- /uSocial -->
		</div>
	</div>
<?php endif; ?>

	<section style="margin:auto;" class="section-all-posts-ms col-ms">
	<?php
	$this->renderPartial('_list', array(
				'dataProvider'	=>	$dataProvider,
				'filter'		=>	$filter
			));
	?>

