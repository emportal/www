<?php
/* @var $model Address */
$this->breadcrumbs = array(
	'Клиники' => $this->createUrl('search/clinic'),
	$model->company->name	 => array("/clinic/view", 'companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl),
	'Фотогалерея'
);
$this->pageTitle = Yii::app()->name . ' - Фотогалерея -  ' . $model->company->name;
?>
<div class="search-result-title">
	<h1 class="h2style pseudo_a">
		<?= CHtml::encode($model->company->name) ?>		
	</h1>
	<small><?= CHtml::encode($model->company->companyType->name) ?></small>
	
</div><!-- /search-result-title -->
<?php
$photos = $model->galleryBehavior->getGalleryPhotos('gallery');
if(!empty($photos) && count($photos)):?>
	<?php foreach ($photos as $photo):?>
		<?php echo CHtml::link(CHtml::image($photo->getPreview()), $photo->getUrl(), array('class' => 'fancybox-thumb', "rel"=>"fancybox-thumb", "title"=>CHtml::encode($photo->name)));?>
	<?php endforeach;?>
<?php endif;?>