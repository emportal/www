<?php
/* @var $model Address */
if(empty($showCap)) {
	$this->breadcrumbs = array(
		'Клиники' => '/clinic',
		$company->name => array("/clinic/addresses", 'companyLink' => $company->linkUrl),
		'Все адреса'
	);
	$this->pageTitle = $company->name . ' - Адреса компании';
} else {
	$this->breadcrumbs = [
		'Клиник' => '/clinic',
		$company->name => array("/clinic/addresses", 'companyLink' => $company->linkUrl),
	];
			
}
?>
<?php if(empty($showCap)): ?>
	<div class="search-result-title">
		<h2>
			<?= CHtml::encode($company->name) ?>
			<small>
				<?= CHtml::encode($company->companyType->name) ?>
			</small>
		</h2>
	</div>
	<?php if (empty($addresses)) : ?>
		<h3 style="text-align: center; margin: 20px 0px">нет адресов</h3>
	<? else: ?>
		<table class="reviews-list">
			<tr>
				<td>
					<div class="search-result-title">
						<?php foreach ($addresses as $address): ?>
							<h2>
								<?=
								CHtml::link((!empty($address->nearestMetroStation) ? "м. ".CHtml::encode($address->nearestMetroStation->name).", " : '') . (!empty($address->street) ? " ".CHtml::encode($address->street).", ".CHtml::encode($address->houseNumber) : CHtml::encode($address->name)),
										array('/clinic/view', 'companyLink' => $company->linkUrl, 'linkUrl' => $address->linkUrl), array('title' => CHtml::encode($address->name)))
								?>
							</h2><br/>
						<? endforeach; ?>
					</div>
				</td>
			</tr>
		</table>		
		<data hidden data-search='<?=1?>'></data>
		<data hidden data-coord='<?= $coord ?>'></data>
		<data hidden data-map='<?= $data ?>'></data>					
		<div id="map" class="clinic-map" style="height:500px;"> </div>
	<? endif; ?>
<?php else: ?>
	<div class="doctor-left-block box">
		<div class="left-block" style="width: inherit;">
			<?=
					$this->_model->company->logo ?
					CHtml::image($this->_model->company->logoUrl, CHtml::encode($this->_model->company->name), array('class' => 'company-logo')) :
					CHtml::image($this->assetsImageUrl . '/icon_emp_med.jpg', '', array('class' => 'company-logo no-logo default'));
			?>
			<?=
			Html::link("Запись недоступна", array('#'), array(
				'class' => 'btn btn-red',
				'style' => 'text-align: center;width: 80%;"',
				'onclick' => 'return false;'
			))
			?>
			
			<div style="margin-top: 10px; text-align: left;">
				К сожалению, запись на прием в эту клинику через Единый Медицинский Портал отсутствует.
			</div>
			<?php if (!empty(Yii::app()->request->cookies['clinic' . $this->_model->company->link])): ?>
				<div class="report-clinic" style="font: inherit; margin-top: 10px; text-align: left;">
					Вы пациент?
					<br>
						<span data-id="<?= $this->_model->company->link ?>">
							<a href="#" onclick="return false;">Пожаловаться на отсутствие клиники</a>
						</span>				
				</div>
			<?php endif; ?>
			<div style="margin-top: 12px; text-align: left;">
				Вы представитель клиники?
				<p>
					<a href="/site/contact">Станьте партнером</a>
				</p>
			</div>
		</div>
	</div>	
	<div style="float: left; width: 75%; margin: 5px 15px 30px;">
		<p style="font-size: 15px; line-height: 20px;">Дорогой пациент, запись на прием в клинику <?= CHtml::encode($this->_model->company->name) ?> через Единый Медицинский Портал более недоступна. Рекомендуем записаться на прием в другие ближайшие к вам клиники</p>	
	</div>
		
	<div class="clearfix"></div>
	<div class="similar-doctors">		
		<div class="specialty-type">
			<div class="row">
				<?php $count = 0; ?>
			<?php foreach($capData as $key=>$clinic): $count++; ?>
				<?php 
					$this->renderPartial('//search/_clinics_unique',[
						'data' => $clinic,
						'address' => $clinic->addresses[0],
						'addresses' => null,
						'big' => 0,					
					]);
				?>
				<?= $count % 2 == 0 && $count !== count($similar) ? '<div class="clearfix"></div></div><div class="row">' : ''; ?>
			<?php endforeach; ?>
				<div class="clearfix"></div>
			</div>
		</div>		
		<div class="clearfix"></div>
	</div>
	<div style="margin-bottom:10px">
		<div class="size-14">			
			<a style="display:inline-block;margin-top:10px" href="<?= $this->createUrl('search/clinic',['metro' => 'metro-'.$metroLink]); ?>">Посмотреть еще рядом</a>
		</div>
	<?php if (empty(Yii::app()->request->cookies['clinic' . $model->link])): ?>
			<div class="report-clinic">
				Не нашли ничего подходящего?
				<span data-id="<?= $model->link ?>" class="btn btn-blue" style="">
					Пожаловаться на отсутствие клиники
				</span>
			</div>
	<?php endif; ?>
	</div>	
<?php endif; ?>
