<?php
$weekArr = array(0=>"Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб");
?>
<?php $dateArr = explode('.', date('Y.m.d')); ?>
<div class="new-calendar">
	<span class="appointment_item_name">Выберите дату приёма</span>
	<div class="picker">
		<div class="arrow-right micro_nav_cal" data-type="forward">
			<span></span>
		</div>
		<div class="arrow-left micro_nav_cal" data-type="back">
			<span></span>
		</div>
		<div class="input-picker"><?=  $weekArr[date('w', mktime(0, 0, 0, $dateArr[1], $dateArr[0], $dateArr[2]))] ?>. <?= $dateArr[0] ?> <?= MyTools::getMonth((int)$dateArr[1]) ?> <?= $dateArr[2] ?></div>
	</div>
	<input id="AppointmentToDoctors_address_link" type="hidden" value="<?=$address->link;?>">
	<input class="custom-text" id="datetimepicker" type="date" style="display:none">
	<div class="custom-text-placeholder show-in-responsive" style="display: none;">Дата приёма</div>

	<script>
		var tb_date = '<?= $dateArr[2].'-'.$dateArr[1].'-'.$dateArr[0] ?>';
	</script>

</div>
<script>
	var cr_date = new Date(),
		cr_month = (cr_date.getMonth() + 1),
		cr_day = cr_date.getDate(),
		todays_date = cr_date.getFullYear()
	;
	todays_date += '-' + ((cr_month.toString().length == 2) ? cr_month : ('0' + cr_month));
	todays_date +=  '-' + ((cr_day.toString().length == 2) ? cr_day : ('0' + cr_day));
	todays_date_arr = todays_date.split('-');

	function updateAvailableTime(cur, $input, type) {
		$('.xdsoft_datetimepicker').fadeOut(100);	
		var date = $input.val();					

		$.ajax({
			url: "/ajax/getDateByAppointmentClinic",
			async: true,
			data: ({
				date: date,	
				type: type,
			}),
			dataType: 'json',
			type: "GET",
			success: function(msg) {
				if (msg.html != "Клиника не указала часы работы") {
					$(".input-picker").html(msg.dateHtml);
					$("#timeblock_date").val(msg.date);
				}
			},
		});
		
	}
	
	
	$(document).ready(function() {
		
		var $dtInput = $('#datetimepicker');
		
		$dtInput.datetimepicker({
			lang: 'ru',
			timepicker: false,
			format: 'Y-m-d',
			closeOnDateSelect: true,
			defaultDate: todays_date_arr[0] + '/' + todays_date_arr[1] + '/' + todays_date_arr[2],
			minDate: '<?=date('Y/m/d'); ?>',				
			maxDate: '<?=date('Y'); ?>/<?=((date('m') <= 7)?'0':'').''.(date('m')+2); ?>/<?=date('d'); ?>',
			dayOfWeekStart: 1,
		});
		var currentDate = '<?=date('Y-m-d')?>';
		$dtInput.on("change",function(e) {
			if(currentDate != $(e.currentTarget).val()) {
				updateAvailableTime(null, $(e.currentTarget));
				currentDate = $(e.currentTarget).val();
			}
		});
		
	});
</script>
	