<?php
/* @var $model Address */
/* @var $comment Comment */

$this->breadcrumbs = array(
	'Клиники' => $this->createUrl('search/clinic'),
	$model->company->name => array("/clinic/view", 'companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl),
	'Отзывы'
);

$this->pageTitle = Yii::app()->name . ' - Отзывы о ' . $model->company->name;
?>



<div class="search-result-title">
	<h2>
		<a href="#" title="<?= CHtml::encode($model->company->name) ?>">
			<?= CHtml::encode($model->company->name) ?>
		</a>
		<small>
			<?= CHtml::encode($model->company->companyType->name) ?>
		</small>
	</h2></div>

<?= CHtml::link('Отзывы о клинике', array('clinic/comments', 'link' => $model->link), array('class' => 'btn clinic')) ?>
&nbsp;
<?= CHtml::link('Отзывы о врачах', array('clinic/comments_experts', 'link' => $model->link), array('class' => 'btn doctor')) ?>
<?php if (empty($model->comments)) : ?>

<? else: ?>
	<table class="reviews-list">
		<tr>
			<td>
				<br>
				<?php foreach ($model->comments as $comment): ?>
					<div class="reviews-box">
						<i class="reviews-box-arrow"></i>
						<blockquote>							
							<?= CHtml::encode($comment->review) ?>
						</blockquote>
						<p class="reviews-amount">
							<a href="#">
								<?php if ($comment->date != '0000-00-00 00:00:00'): ?>
									Отзыв от <?= Yii::app()->dateFormatter->formatDateTime($comment->date, "medium", null); ?>,
								<?php endif; ?>
								<?= $comment->user->name ? CHtml::encode($comment->user->name) : CHtml::encode($comment->name) ?>
								<?php /* $clinic->street ?><?= $clinic->houseNumber */ ?>
							</a>
						</p>
					</div>
					<?php if ($comment->answer): ?>
						<div class="answer">
							<div class="reviews-box">
								<blockquote>
									<?= CHtml::encode($comment->answer) ?>
								</blockquote>
							</div>
						</div>
					<?php endif; ?>
				<? endforeach; ?>
				<br>
			</td>
		</tr>
	</table>
<? endif; ?>

			<script type="text/javascript">
				$(document).ready(function() {
					VK.init({apiId: 4430365, onlyWidgets: true});
				});

			</script>

			<!-- Put this div tag to the place, where the Comments block will be -->
			<div id="vk_comments"></div>
			<script type="text/javascript">
				$(document).ready(function() {
					VK.Widgets.Comments("vk_comments", {limit: 15, width: "665px", attach: "*"},'<?='address_'.$this->_model->link?>');
				});
			</script>


<script>
	/*$(document).ready(function() {
		$('.title-row').next('table').css('display', 'none');
		$('.title-row').toggle(
				function() {
					$('.title-row').next('table').hide('slow');
					$(this).next('table').show('slow');
				},
				function() {
					$(this).next('table').hide('slow');
				}
		);
	});*/
</script>
