<?php
/* @var $model Address */
/* @var $this Controller */
?>
<td class="search-result-signup">
	<?php if ($model->company->logo): ?>
		<?= CHtml::image($model->company->logoUrl, CHtml::encode($model->company->name), array('class' => 'company-logo')) ?>
	<? else: ?>
		<?= CHtml::image($this->assetsImageUrl . '/1x1.png', '', array('class' => 'company-logo default')) ?>
	<? endif; ?>
	<?= Html::link('Записаться на прием', array("/clinic/visit", 'link'=>$model->link), array('class'=>"btn-green"))?><br><br>
	<?= Html::link('Описание', array("/clinic/view", 'link'=>$model->link), array('class'=>"btn w100 ".($this->action->id == 'view' ? 'active':'')))?><br>
	<?= Html::link('Специалисты', array("/clinic/experts", 'link'=>$model->link), array('class'=>"btn w100 ".($this->action->id == 'experts' ? 'active':'')))?><br>
	<?= Html::link('Стоимость услуг', array("/clinic/price", 'link'=>$model->link), array('class'=>"btn w100 ".($this->action->id == 'price' ? 'active':'')))?><br>
	<?= Html::link('Отзывы', array("/clinic/comments", 'link'=>$model->link), array('class'=>"btn w100 ".($this->action->id == 'comment' ? 'active':'')))?><br>
	<?//= Html::link('Фотогалерея', array("/clinic/fotos", 'link'=>$model->link), array('class'=>"btn w100 ".($this->action->id == 'foto' ? 'active':'')))?><br>
	<?//= Html::link('Акции', array("/clinic/actions", 'link'=>$model->link), array('class'=>"btn w100 ".($this->action->id == 'action' ? 'active':'')))?><br>
	<?//= Html::link('Все адреса', array("/clinic/addresses", 'link'=>$model->link), array('class'=>"btn w100 ".($this->action->id == 'action' ? 'active':'')))?><br>
</td><!-- /search-result-signup -->