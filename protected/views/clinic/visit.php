<? /* @var $model AppointmentToDoctors */ ?>

<?php
Yii::app()->clientScript->registerScript('search', "
	$(document).on('focus','.ui-autocomplete-input', function(){
		$(this).autocomplete('search');
	});

	$('a.clear-row').click(function(){
		$(this).closest('td').find('input').val('');		
		if( $(this).attr('type')=='doctor' ) {
			updateWorkingHourByDoctor();
		}		
		return false;
	});
		
	$(document).ready(function() {
	 if(scrollY < 395) {
		window.scrollTo(0,395);
	 }
	});
");
$this->pageTitle = Yii::app()->name . ' - Запись на прием в ' . $model->company->name;
?>
<div class="search-result">
	<div class="crumbs"><a href="/">Главная</a> > <span>Запись на прием</span></div>
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'post',
		'id' => 'doctor-visit',
		'action' => $this->createUrl('clinic/visit', array('linkUrl' => $model->address->linkUrl,'companyLink' => $model->address->company->linkUrl)),
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'inputContainer' => 'td',
			'validateOnChange' => false,
			'beforeValidate' => 'js:function(form) {
				if($("#code").val()) {
					acceptCodeForAppointment();
				}

				return true;
			}',
			'afterValidate' => 'js: function(form,data,hasError) { 
				if(hasError === false) {
					yaCounter18794458.reachGoal("priem_success");
					ga("send", "pageview", "/priem_success");
				}
				if(data["AppointmentToDoctors_phone"] !== undefined && data["AppointmentToDoctors_phone"][0] === "Телефон не активирован") {
					yaCounter18794458.reachGoal("priem_error_without_phone");
					$(".getCodeButton").click();
				}
				return true;
			 }'
		),
		'htmlOptions' => array('class' => 'zakaz relative', 'enctype' => 'multipart/form-data'),
	));
	/* @var $form CActiveForm */
	?>
	<table class="search-result-table">
		<?php if (Yii::app()->user->hasFlash('askAuth')): ?>
			<tr class="search-result-box">
				<td colspan="2">
					<div class="flash-notice askAuth">
						<?= Yii::app()->user->getFlash('askAuth'); ?>
					</div>
				</td>
			</tr>
		<?php endif; ?>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Клиника</td>
			<td class="search-result-info">
				<?= $form->hiddenField($model, 'address[link]') ?>
				<?= CHtml::link(CHtml::encode($model->address->getName()), array('clinic/view', 'companyLink' => $model->company->linkUrl, 'linkUrl' => $model->address->linkUrl), ['class' => 'change_address_name']); ?>
				<br/>
				<?php if(count($model->company->cityAddresses) > 1): ?>
					<span class="btn-blue change_address" id="visit_change_address">Изменить адрес клиники</span>
					<div class="list_addresses">
						<table>
							<?php if ($model->company->cityAddresses): ?>
								<?php foreach ($model->company->cityAddresses as $address): ?>					
									<tr>
										<td>
											<div data-text="<?= CHtml::encode($address->getName()) ?>" data-linkurl="<?= $address->company->linkUrl .'/'.$address->linkUrl ?>" data-link="<?= $address->link ?>" class="visit_element_address btn-green">
												<?= (!empty($address->nearestMetroStation) ? "м. ".CHtml::encode($address->nearestMetroStation->name).", " : '') .
												(!empty($address->street) ? " ".CHtml::encode($address->street).", ".CHtml::encode($address->houseNumber) : CHtml::encode($address->name))
												?>
											</div>
										</td>
									</tr>			
								<?php endforeach; ?>
							<?php endif; ?>					
						</table>				
					</div>
				<?php endif; ?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Услуга <span class="required">*</span></td>
			<td class="search-result-info">		
				<?php if ($showPicker): ?>					
					<?= $form->hiddenField($model, 'serviceId') ?>
					<?php
					$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
						'model' => $model,
						'name' => 'serviceName',
						//'attribute'		 => 'city.name',
						'value' => $model->service->name,
						'source' => 'js:function (request, response) {		
							
							 $.ajax({
								url: "/ajax/serviceByClinic",
								dataType: "json",
									data: {
										clinicLink: $("#' . CHtml::activeId($model, 'address[link]') . '").val(),
										term: request.term
									},
									success: response
								});
						}',
						// additional javascript options for the autocomplete plugin
						'options' => array(
							'minLength' => '0',
							/*
							  'change'	 => 'js:function( event, ui ) {
							  console.log("changed_service");
							  if ( ui.item ) {
							  $( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( ui.item.id );
							  getPriceByDoctorAndService();
							  return true;
							  }
							  else {
							  $(this).val( "" );
							  $( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( "" );
							  getPriceByDoctorAndService();
							  return false;
							  }

							  }',
							 */
							'select' => 'js:function(event,ui) {  
									if ( ui.item ) {
										$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( ui.item.id );
										getPriceByDoctorAndService();
										return true;
									} else {
										$(this).val( "" );
										$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( "" );
										getPriceByDoctorAndService();
										return false;
									}
						
								}
							'
						),
						'htmlOptions' => array(
							'class' => 'custom-text',
						),
					));
					?>				
					<label><a class="clear-row" href="#">убрать</a></label><br/>
					<?= $form->error($model, 'serviceId') ?>
				<?php else: ?>
					<?= $form->hiddenField($model, 'serviceId', array('value' => 'first')) ?>
					<span>Первоначальное обращение</span>
				<?php endif; ?>

			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Врач</td>
			<td class="search-result-info">
				<?= $form->hiddenField($model, 'doctorId') ?>
				<?php
				$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
					'model' => $model,
					'name' => 'doctorName',
					//'attribute'		 => 'city.name',
					'value' => $model->doctor->name,
					'source' => 'js:function (request, response) {
						 $.ajax({
							url: "/ajax/doctorByClinicAndService",
							dataType: "json",
								data: {
									clinicLink: $("#' . CHtml::activeId($model, 'address[link]') . '").val(),
									serviceLink: $("#' . CHtml::activeId($model, 'serviceId') . '").val(),
									term: request.term
								},
								success: response
							});
					}',
					// additional javascript options for the autocomplete plugin
					'options' => array(
						'minLength' => '0',
						'change' => 'js:function( event, ui ) {
							
							if ( ui.item ) {							
								$( "#' . CHtml::activeId($model, 'doctorId') . '" ).val( ui.item.id );									
								return true;
							}
							else {
								$(this).val( "" );
								$( "#' . CHtml::activeId($model, 'doctorId') . '" ).val( "" );		
								updateWorkingHourByDoctor();
								getPriceByDoctorAndService();
								return false;
							}							
							}',
						'select' => 'js:function(event,ui) {  
								if ( ui.item ) {								
									$( "#' . CHtml::activeId($model, 'doctorId') . '" ).val( ui.item.id );		
									updateWorkingHourByDoctor();
									getPriceByDoctorAndService();
									return true;
								} else {								
									$(this).val( "" );
									$( "#' . CHtml::activeId($model, 'doctorId') . '" ).val( "" );		
									updateWorkingHourByDoctor();
									getPriceByDoctorAndService();
									return false;
								}			
							}
						'
					),
					'htmlOptions' => array(
						'class' => 'custom-text',
					),
				));
				?>
				<label><a class="clear-row" type="doctor" href="#">убрать</a></label>
				<div id="price_for_doctor"></div>
			</td>
		</tr>
		<tr class="search-result-box calendar-row">
			<td class="search-result-signup size-14">Календарь <span class="required">*</span></td>
			<td class="search-result-info">
				<div id="calendar">
					<?php
					$this->renderPartial('/clinic/_calendarBlock', array(
						'model' => $model,
						'time' => $time,
					));
					?>
				</div>				
				<input type="hidden" id="timeblock_date" value="<?= date("Y-m-d", mktime(0, 0, 0, date('m'), date('j') + 1, date('Y'))) ?>"/>
				<?= $form->hiddenField($model, 'plannedTime'); ?>
<?= $form->error($model, 'plannedTime') ?>
			</td>
		</tr>
		<!--<tr class="search-result-box">
			<td class="search-result-signup size-14">Календарь</td>
			<td class="search-result-info">
		<?php
		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'plannedTime',
			'language' => 'ru',
			'htmlOptions' => array('class' => 'custom-text'),
			'options' => array(
				'changeMonth' => true,
				'changeYear' => false,
				'altFormat' => "dd.mm.yy",
				'dateFormat' => "yy-mm-dd",
				'maxDate' => '+6m',
				'minDate' => 0,
			),
				)
		);
		?>
			</td>
		</tr>-->	
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Ваше имя</td>
			<td class="search-result-info">
				<?= $form->textField($model, 'name', array('class' => 'custom-text')); ?><br/>
<?= $form->error($model, 'name') ?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Телефон <span class="required">*</span></td>
			<td class="search-result-info">					
				<?php if (Yii::app()->user->isGuest || empty(Yii::app()->user->model->telefon) ): ?>					
					<?=
					$form->hiddenField($model, 'phone', array(
						'value' => Yii::app()->session['appointment_phone_set'] ? Yii::app()->session['appointment_phone_set'] : ''
					));
					?>
	<?php if (Yii::app()->session['appointment_phone_set']): ?>
						<span id="phoneSpan" class="phone" style=""><?= Yii::app()->session['appointment_phone_set'] ?></span>
						<span onclick="removePhoneForAppointmentGuest();" class="btn-red" style="">отменить выбранный номер</span>			
	<?php else: ?>
						<span id="phoneSpan" class="phone" style="display:none"></span>
						<input class="custom-text" style="" id="phoneInput" name="phoneInput" type="text"/>
						<script>
						$(function() {
							$("#phoneInput").mask("+79999999999", {placeholder: "_"});
							var isGuest = 1;
						});
						</script>
						<span onclick="getCodeForAppointment();" class="getCodeButton btn-blue" style="">получить код</span>			
						<span onclick="acceptCodeForAppointment();" class="acceptButton btn-blue" style="display:none">подтвердить</span>
					<?php endif; ?>					

				<?php else: ?>
					<?php if ($pv): ?>
						<?= CHtml::hiddenField('default', $model->phone); ?>
						<?=
						CHtml::activeHiddenField($model, 'phone', array(
							'value' => $pv->phone
						));
						?>						
						<span id="phoneSpan" class="phone"><?= $pv->phone ?></span>
						<span onclick="removePhoneForAppointment();" class="removeButton btn-red">Вернуть стандартный телефон</span>
					<?php else: ?>
		<?= $form->hiddenField($model, 'phone') ?>
						<span id="phoneSpan" class="phone"><?= $model->phone ?></span>
						<span onclick="changePhoneForAppointment()" class="btn-blue changePhoneAppointment">Поменять телефон на одну запись</span>
						<span onclick="getCodeForAppointment();" class="getCodeButton btn-blue" style="display:none">получить код</span>			
						<span onclick="acceptCodeForAppointment();" class="acceptButton btn-blue" style="display:none">подтвердить</span>
					<?php endif; ?>						
<?php endif; ?>

				<br/>
				<?= $form->error($model, 'phone') ?>
				<div id="phone_tooltip">
					<div class="triangle_left"></div>
					<div class="app_tooltip flash-notice">
						Ваш номер нигде не выводится и используется <b>только</b> для записи на прием.<br/>Вся процедура бесплатна.<br><br>Пример для России: <b>+79217234455</b>
					</div>
				</div>
			</td>
		</tr>
		<!--<tr class="search-result-box">
			<td class="search-result-signup size-14">E-mail <span class="required">*</span></td>
			<td class="search-result-info">
				<?= $form->textField($model, 'email', array('class' => 'custom-text')); ?><br/>
				<?= $form->error($model, 'email') ?>
			</td>
		</tr>-->
		<tr class="search-result-box">
			<td class="search-result-signup size-14"></td>
			<td class="search-result-info">
				<button class="btn-green" type="submit">Записаться на прием</button>
			</td>
		</tr>
	</table>
<?php $this->endWidget(); ?>
</div>
