<?php 
$this->breadcrumbs = array(
	'Клиники'	=> $this->createUrl('search/clinic'),
	$model->company->name => array("/clinic/view", 'companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl),	
	'Акции'
);
$this->pageTitle = Yii::app()->name . ' - Акции - ' . $model->company->name;
?>

<div class="stock-clinic">
	<div class="search-result-title">
	<h2>
		<a href="#" title="<?= CHtml::encode($model->company->name) ?>">
			<?= CHtml::encode($model->company->name) ?>
		</a>
		<small>
			<?= CHtml::encode($model->company->companyType->name) ?>
		</small>
	</h2>
		<?= CHtml::link('смотреть все акции', array('/actions'), array('class'=>'all-link')) ?>
	</div>	
	<ul class="stock-list">
		<?php foreach ($actions as $action): ?>
		<li>
			<a href="<?= $this->createUrl('/actions/view', array('link'=>$action->link))?>" class="stock-item">
				<div class="stock-item-inner">
					<i class="ico-discount"></i>
					<div class="stock-item-title">
						<h3><?= CHtml::encode($action->name) ?></h3>
						<strong>
						<?php if($action->unlimited):?>
						Старая цена <?= CHtml::encode($action->priceOld) ?>,
						Цена по акции <?= CHtml::encode($action->priceNew) ?>
						<?php else:?>
							СРОК АКЦИИ:
							<?= Yii::app()->dateFormatter->formatDateTime(strtotime($action->start), 'short', null) ?>
							-
							<?= Yii::app()->dateFormatter->formatDateTime(strtotime($action->end), 'short', null) ?>
						<?php endif;?>
						</strong>
					</div><!-- /stock-item-title -->
					<p></p>
				</div><!-- /stock-item-inner -->
			</a>
		</li>
		<? endforeach; ?>
	</ul><!-- /stock-list -->
</div><!-- /stock -->