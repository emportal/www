<?php
/* @var $model Companies */
/* @var $specialties DoctorSpecialty[] */

$this->breadcrumbs = array(
	'Клиники' => $this->createUrl('search/clinic'),
	$model->company->name	 => array("/clinic/view", 'companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl),
	'Специалисты',
);

$this->pageTitle = Yii::app()->name . ' - ' . $model->company->name . " - Специалисты";
?>

<div class="search-result-title">
	<h2>
		<a href="#" title="<?= CHtml::encode($model->company->name) ?>">
			<?= CHtml::encode($model->company->name) ?>
		</a>
		<small>
			<?= CHtml::encode($model->company->companyType->name) ?>
		</small>
	</h2>
</div><!-- /search-result-title -->
<? if (empty($specialties)): ?>
	<p>К сожалению данный раздел не заполнен </p>
<? else: ?>
	<?php foreach ($specialties as $specialty): ?>
		<div class="title-row">
			<span class="pseudo_a"><?= CHtml::encode($specialty->name) ?></span>
			<small>(специалистов: <?= count($specialty->s_doctors) ?>)</small>
		</div>
		<table class="search-result-table" style="display:block">
			<?php foreach ($specialty->s_doctors as $doctor): ?>
				<tr class="search-result-box">
					<td class="search-result-signup">
					<?php if ($doctor->photo): ?>
						<?= CHtml::image($doctor->photoUrl, 'Врач')?>
					<? else: ?>
						<?php if ($doctor->sex && $doctor->sex->order = 1): ?>
							<?= CHtml::image($this->assetsImageUrl . '/staff-m.jpg', 'Врач')?>
						<? else: ?>
							<?= CHtml::image($this->assetsImageUrl . '/staff-f.jpg', 'Врач')?>
						<? endif; ?>
					<? endif; ?>
					</td>
					<td class="search-result-info">
						<div class="search-result-title">
							<h2>
								<?= Html::link(CHtml::encode($doctor->name), array('/doctor/view', 'linkUrl' => $doctor->linkUrl)) ?>
							</h2>
							<div class="rating" style="display: none">
								<div style="width:20%" class="rating-inner"></div><!-- /rating-inner -->
								<b class="rating-count"><span>4.</span>5</b><!-- /rating-count -->
							</div><!-- /rating -->
						</div><!-- /search-result-title -->
						<p class="size-14">
							<?php foreach ($doctor->specialtyOfDoctors as $specialtyOfDoctor) : ?>
								<?php if($specialtyOfDoctor->doctorSpecialty):?>
									<?= CHtml::encode($specialtyOfDoctor->doctorSpecialty->name) ?>, <?= CHtml::encode($specialtyOfDoctor->doctorCategory->name) ?>.<br>
								<?php endif;?>
							<? endforeach; ?>
							<?php if($doctor->getExperience()):?>Стаж: <?= $doctor->getExperience() ?><?php endif;?>
						</p>
<!-- 						<p>Дерматолог широкого профиля. Осуществляет лечение таких заболеваний как акне (угри), бородавки, лишаи, пигментные пятна, грибковые.</p> -->
<!-- 						<em class="price-box"><span class="price-box-inner">Стоимость приёма <b>2 000</b> р.</span></em> -->
					</td>
				</tr>
			<? endforeach; ?>
		</table>
	<? endforeach; ?>
<? endif; ?>

<script>
	$("table.search-result-table").slideUp(1);
	$(document).ready(function() {
		/*$('.title-row').next('table').css('display', 'none');*/
	/*	$('.title-row').toggle(
				function() {
					$('.title-row').next('table').hide();
					$(this).next('table').show('slow');
				},
				function() {
					$(this).next('table').hide();
				}
		);*/
		var duration = 200;
		
		$('.title-row').click(function() {
			/* если есть Класс show , сворачиваем и убираем, иначе сворачиваем */
			if($(this).hasClass('show')) {
				$(this).next('table').slideUp(duration);
				$(this).removeClass('show');				
			} else {	
				$('.title-row.show').each(function() {
					$(this).next('table').slideUp(duration);
					$(this).removeClass('show');
				});
				$(this).next('table').slideDown(duration);
				$(this).addClass('show');				
			}
		});
		
	});
</script>
