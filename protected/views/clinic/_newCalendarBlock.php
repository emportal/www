<?php
$weekArr = array(0=>"Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб");
if ($time):
	foreach ($time as $k => $v):
?>
<?php $dateArr = explode('.', $v['date']); ?>
<div class="new-calendar">
	<span class="appointment_item_name">Выберите дату приёма</span>
	<div class="picker">
		<div class="arrow-right micro_nav_cal" data-type="forward">
			<span></span>
		</div>
		<div class="arrow-left micro_nav_cal" data-type="back">
			<span></span>
		</div>
		<div class="input-picker"><?=  $weekArr[date('w', mktime(0, 0, 0, $dateArr[1], $dateArr[0], $dateArr[2]))] ?>. <?= $dateArr[0] ?> <?= MyTools::getMonth((int)$dateArr[1]) ?> <?= $dateArr[2] ?></div>
	</div>
	<input class="custom-text" id="datetimepicker" type="date" style="display:none !important;">
	<div class="custom-text-placeholder show-in-responsive" style="display: none !important;">Дата приёма</div>
	<?php if(!empty($v['time'])): ?>
	<span class="appointment_item_name" id="appointment_time_label">Выберите время приёма</span>
	<?php endif; ?>
	<div id="new-time_loading" style="display: none;">
		<div class="icon_loading"></div>
		<div class="appointment_item_name" style="text-align: center;">Минутку, ищем для вас<br> свободное время... <span id="new-time_procent" data-step="0">0</span>%</div>
	</div>
	<div class="new-time">
		<?php
		if (empty($v['time'])) {
			echo CHtml::encode($v['msg']);
		} else {
			$allowTimes = [];
			$recordFinished = true;
			foreach ($v['time'] as $t) {
				$timeN = strtotime($v['date'].' '.$t['text']);
				if($timeN < time()) {
					$t['status'] = 'hidden';
				}
				if(!isset($t['VisitStart'])) {
					$t['VisitStart'] = $t['text'];
				}
				if(!isset($t['id'])) {
					$t['id'] = $t['VisitStart']."-".$t['VisitEnd'];
				}
				switch ($t['status']) {
					case 'hidden':
						#echo '<a VisitStart="'.CHtml::encode($t['VisitStart']).'" VisitEnd="'.CHtml::encode($t['VisitEnd']).'" id="'.CHtml::encode($t['id']).'" title="'.$t['text'].'" class="btn '.(explode(" ",$model->plannedTime)[1] == $t['text'].":00" && explode(" ",explode("-",$model->plannedTime)[2])[0]==explode(".",$v['date'])[0] ? 'active' : '') .'" href="#">'.CHtml::encode($t['text']).'</a>';
						break;
					case 'allow':
						echo '<a VisitStart="'.CHtml::encode($t['VisitStart']).'" VisitEnd="'.CHtml::encode($t['VisitEnd']).'" id="'.CHtml::encode($t['id']).'" title="'.$t['text'].'" class="btn ' . (explode(" ", $model->plannedTime)[1] == $t['text'] . ":00" && explode(" ", explode("-", $model->plannedTime)[2])[0] == explode(".", $v['date'])[0] ? 'active' : '') . '" href="#">' . CHtml::encode($t['text']) . '</a>';
						$allowTimes[$t['id']] = $t['text'];
						$recordFinished = false;
						break;
					case 'disabled':
						echo '<a VisitStart="'.CHtml::encode($t['VisitStart']).'" VisitEnd="'.CHtml::encode($t['VisitEnd']).'" id="'.CHtml::encode($t['id']).'" onclick="return false" title="'.$t['text'].'" class="btn btn-blue' . (explode(" ", $model->plannedTime)[1] == $t['text'] . ":00" && explode(" ", explode("-", $model->plannedTime)[2])[0] == explode(".", $v['date'])[0] ? 'active' : '') . '" href="#">' . CHtml::encode($t['text']) . '</a>';
						break;
					case 'busy':
						echo '<a VisitStart="'.CHtml::encode($t['VisitStart']).'" VisitEnd="'.CHtml::encode($t['VisitEnd']).'" id="'.CHtml::encode($t['id']).'" onclick="return false" title="'.$t['text'].'" class="btn btn-red' . (explode(" ", $model->plannedTime)[1] == $t['text'] . ":00" && explode(" ", explode("-", $model->plannedTime)[2])[0] == explode(".", $v['date'])[0] ? 'active' : '') . '" href="#">' . CHtml::encode($t['text']) . '</a>';
						break;
				}
			}
			if($recordFinished && date("d.m.Y")===$v['date']) echo "<div>На сегодня прием закончен. Рекомендуем посмотреть время в другие дни</div>";
		}
		?>
		<?php if(count($allowTimes) > 0): ?>
		<select class="custom-text" style="display:none;">
			<option value="">не выбрано</option>
			<?php foreach ($allowTimes as $value=>$text): ?>
				<option value="<?=CHtml::encode($value)?>"><?=CHtml::encode($text)?></option>
			<?php endforeach; ?>
		</select>
		<div class="custom-text-placeholder show-in-responsive" style="display: none;">Время</div>
		<?php endif; ?>
		<script>
		var tb_date = '<?= $dateArr[2].'-'.$dateArr[1].'-'.$dateArr[0] ?>';
		</script>
	</div>
</div>
<?php endforeach; ?>
<script>
	var cr_date = new Date(),
		cr_month = (cr_date.getMonth() + 1),
		cr_day = cr_date.getDate(),
		todays_date = cr_date.getFullYear()
	;
	todays_date += '-' + ((cr_month.toString().length == 2) ? cr_month : ('0' + cr_month));
	todays_date +=  '-' + ((cr_day.toString().length == 2) ? cr_day : ('0' + cr_day));
	todays_date_arr = todays_date.split('-');

	var intervalProcent;
	function loadProcent() {
		var procentLoad = $('#new-time_procent').attr('data-step')*1,
		 	procentStep = 100*100/6000;

		procentLoad = Math.round(procentLoad + procentStep);
		if(procentLoad > 100) {
			procentLoad = 100;
			clearInterval(intervalProcent);
		}
		$('#new-time_procent').attr('data-step',procentLoad),
		$('#new-time_procent').html(procentLoad);
	}

	var request = null;
	function updateAvailableTime(cur, $input, type) {
		$('.xdsoft_datetimepicker').fadeOut(100);
		$("#AppointmentToDoctors_plannedTime").val("");
		if (!cur) {

			if (!$input.val())
				$input.val(todays_date);
			cur = $input.val();
		}
		
		var date = $input.val();					
		var link = $("#AppointmentToDoctors_address_link").val();
		var doctorLink = $("#AppointmentToDoctors_doctorId").val();
		var idPat = '';
		if (typeof patientOMSData !== 'undefined' && typeof patientOMSData.idPat !== 'undefined') {
			
			//console.log('назначаем idPat: ' + patientOMSData.idPat);
			idPat = patientOMSData.idPat;
		}
		
		//проверка на пациента самозаписи
		//var idPat = $("#AppointmentToDoctors_address_link").val();
		
		if(request !== null) {
			request.abort();
		}
		request = $.ajax({
			url: "/ajax/getWorkingHoursByDate",
			async: true,
			data: ({
				date: date,	
                type: type,					
				link: link,
				'new' : 1,
				doctor: doctorLink,
				idPat: idPat
			}),
			dataType: 'json',
			type: "GET",
			beforeSend: function() {
				$("#new-time_loading").show();
				$('#new-time_procent').attr('data-step','0');
				intervalProcent = setInterval(loadProcent, 100);
				//$("#appointment_time_label").hide();
				$(".new-time").hide();
			},
			success: function(msg) {
				if (msg.html != "Клиника не указала часы работы") {
					$("#calendar .new-time").html($(msg.html).find('.new-time').html());
					$("#calendar .picker").html($(msg.html).find('.picker').html());
					$("#timeblock_date").val(tb_date);
				}
			},
			complete: function() {
				//console.log("complete");
				$("#new-time_loading").hide();
				clearInterval(intervalProcent);
				$("#appointment_time_label").show();
				$(".new-time").show();
			},
		});
		
	}
	
	
	$(document).ready(function() {
		
		var $dtInput = $('#datetimepicker');
		
		$dtInput.datetimepicker({
			lang: 'ru',
			timepicker: false,
			format: 'Y-m-d',
			closeOnDateSelect: true,
			defaultDate: todays_date_arr[0] + '/' + todays_date_arr[1] + '/' + todays_date_arr[2],
			minDate: '<?=date('Y/m/d'); ?>',				
			maxDate: '<?=date('Y'); ?>/<?=((date('m') <= 7)?'0':'').''.(date('m')+2); ?>/<?=date('d'); ?>',
			dayOfWeekStart: 1,
			onSelectDate: function(cur, $input) {
				//updateAvailableTime(cur, $input); //$input = $('#datetimepicker')
			}
		});
		var currentDate = '<?=date('Y-m-d')?>';
		$dtInput.on("change",function(e) {
			if(currentDate != $(e.currentTarget).val()) {
				updateAvailableTime($(e.currentTarget).val(),$(e.currentTarget));
				currentDate = $(e.currentTarget).val();
			}
		});
		
	});
</script>
<?php else: ?>
	Клиника не указала часы работы
<?php endif; ?>
	