<?php
/* @var $model Company */
/* @var $this Controller */
/* @var $prices AddressServices[] */

$this->breadcrumbs = array(
	'Клиники' => $this->createUrl('search/clinic'),
	$model->company->name => array("/clinic/view", 'linkUrl' => $model->linkUrl, 'companyLink' => $model->company->linkUrl,),
);

$this->pageTitle = Yii::app()->name . ' - Стоимость услуг в ' . $model->company->name;

if (!$activity)
	$this->breadcrumbs[] = 'Стоимость услуг';
else {
	$this->breadcrumbs['Стоимость услуг'] = array("/clinic/price", 'linkUrl' => $model->linkUrl, 'companyLink' => $model->company->linkUrl);
	$this->breadcrumbs[] = $activity->name;
}
?>
<div style="<?= Yii::app()->request->getParam('JSON') ? 'min-width:700px;margin:10px' : '' ?>">
	<div class="search-result-title">
		<h2 class="pseudo_a">
			<?= CHtml::encode($model->company->name) ?>
		</h2>
		<small>
			<?= CHtml::encode($model->company->companyType->name) ?>
		</small>
	</div>
	<?php if ($activity): ?>
		<div class="search-result-title">
			<h2>
				<?= CHtml::encode($activity->name) ?>
			</h2>
		</div>
		<table class="service-table">
			<?php if (empty($prices)): ?>
				<tr>
					<td class="service-title">нет данных</td>
				</tr>
			<? else: ?>
				<?php foreach ($prices as $price): ?>
					<tr>
						<td class="service-title"><?= CHtml::encode($price->service->name) ?></td>
						<td class="service-price"><?= $price->price ?> р.</td>
						<td class="service-signup">
							<?=
								Html::link('Записаться', array(
									'clinic/visit',
									'linkUrl' => $model->linkUrl,
									'companyLink' => $model->company->linkUrl,
									'service' => $price->service->link
								), array('class' => 'btn-green-min'))
							?>
						</td>
					</tr>
				<? endforeach; ?>
			<? endif; ?>
		</table>
	<? else: ?>
		<?php
		echo CHtml::hiddenField('addr_link', $model->link);

		if ($model->companyActivites && $countServices):
			?>
			<div class="activites" id="clinic_activites">
				<div class="search-result-title"><h2>Профили деятельности / Отделения</h2></div>

				<?php
				$i = 0;
				$count = count($model->companyActivites) / 2;
				//var_dump($countServices);
				?>
				<?php foreach ($model->companyActivites as $activity): ?>	

					<?php if ($countServices[$activity->companyActivites->id]): ?>
						<div class="activites-column w50">
							<a data-link="<?= $activity->companyActivites->link ?>" href="<?= $this->createUrl('', array('linkUrl' => $model->linkUrl, 'companyLink' => $model->company->linkUrl, 'activity' => $activity->companyActivites->link)) ?>" class="btn" title="<?= CHtml::encode($activity->companyActivites->name) ?>">
								<?= CHtml::encode($activity->companyActivites->name) ?>
							</a>
							<?php /* if ($i < $count): ?>										
							  <? endif; */ ?>
						</div>
					<?php endif; ?>
				<? endforeach; ?>

			</div>
			<div class="activites" id="clinic_activites_prices">
				<span class="btn-blue">Вернуться к выбору профилей деятельности</span>
			</div>
		<?php else: ?>
			К сожалению данный раздел не заполнен
		<?php endif; ?>

	<? endif; ?>
</div>