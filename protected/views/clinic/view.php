<?php
/* @var $model Address */

$companyWithManyAddresses = (count($model->company->addresses) > 1);
$this->pageTitle = $this->createSeoTitle($model, $companyWithManyAddresses);
Yii::app()->clientScript->registerMetaTag($this->createSeoDescription($model, $companyWithManyAddresses), 'description');

$this->breadcrumbs = array(
	'Клиники' => '/klinika',
	$model->company->name,
);

$reviewsCount = count($reviews);

?>
<div class="doctor clinic" itemscope itemtype="http://schema.org/Organization">
	<div class="SECTION">
		<div class="main_column">
			<div class="doctor_section_content">
				<div class="doctor_section_column_sign">
					<div class="doctor_card_main_left">
						<?php if ($this->_model->company->logo): ?>
							<?= CHtml::image($this->_model->company->logoUrl, $this->_model->company->name) ?>
						<?php else: ?>
							<?= CHtml::image($this->assetsImageUrl . '/icon_emp_med.jpg', 'Клиника') ?>
						<?php endif; ?>
						<?php if (Yii::app()->params['regions'][$model->city->subdomain]['loyaltyProgram']
							&& $model->userMedicals->agreementLoyaltyProgram) : ?>
							<?= Yii::app()->params['empLoyaltyPageLink'] ?>
							<div class="empLoyaltyProgram" style="margin-top: 90px;"></div>
							</a>
						<?php endif; ?>
					</div>
					<div class="address-block address-block-mx">
						<div class="card_row">
							<span class="item_name" title="Адрес">Адрес:</span>
							<a href="<?= $this->createUrl('clinic/map', ['companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl]); ?>" class="show_on_map">
								<span><?= CHtml::encode($model->street) ?>, <?= CHtml::encode($model->houseNumber) ?></span>
							</a>
						</div>
						<?php if (!empty($model->nearestMetroStation->name)): ?>
							<div class="card_row">
								<span class="item_name" title="Метро">Метро:</span>
								<span class="item_value"><?= CHtml::encode($model->nearestMetroStation->name) ?></span>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="doctor_section_column_description">
					<h1 class="h1 doctor_section_name" itemprop="name"><?= !empty(trim($model->seoName)) ? $model->seoName : CHtml::encode($model->company->name) ?></h1>
					<span style="display: none;"><a href="<?= Yii::app()->request->requestUri ?>" itemprop="url"></a></span>
					<div class="address-block" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<div class="card_row">
							<span class="item_name" title="Адрес">Адрес:</span>
							<a href="<?= $this->createUrl('clinic/map', ['companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl]); ?>" class="show_on_map">
								<span><?= CHtml::encode($model->street) ?>, <?= CHtml::encode($model->houseNumber) ?></span>
							</a>
						</div>
						<meta content="<?= CHtml::encode($model->street) ?>, <?= CHtml::encode($model->houseNumber) ?>" itemprop="streetAddress">
    					<meta content="<?=$model->city->name;?>" itemprop="addressLocality">
						<?php if (!empty($model->nearestMetroStation->name)): ?>
							<div class="card_row">
								<span class="item_name" title="Метро">Метро:</span>
								<span class="item_value"><?= CHtml::encode($model->nearestMetroStation->name) ?></span>
							</div>
						<?php endif; ?>
					</div>

					<span style="font-family: 'Roboto'; font-size: 14px; font-weight: 500; line-height: 24px;"><?= CHtml::encode($model->company->companyType->name) ?></span>
					<?php if(!$model->getDisableRatingParameter()): ?>
						<div class="doctor_rate_block" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
							<meta itemprop="ratingValue" content="<?= $model->company->roundedRating ?>">
							<meta itemprop="bestRating" content="5">
							<meta itemprop="reviewCount" content="<?= $model->company->roundedRating ? $reviewsCount ? $reviewsCount : 1 : 0 ?>">
							<input type="range" min="0" max="5" value="<?= $model->company->roundedRating ?>" step="0.1" id="backing-<?= $model->link ?>" style="display: none;">
							<span id="rate-<?= $model->link ?>" data-rateit-value="<?= $model->company->roundedRating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $model->link ?>"></span>
							<button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
							<span class="rating-text"><?= $model->company->boldedRoundedRating; ?></span>

							<script>
								$(function() {
									jQuery('#rate-<?= $model->link ?>').rateit({max: 5, step: 0.1, starwidth: 23, starheight: 20, backingfld: '#backing-<?= $model->link ?>'});
								});
							</script>
						</div>
					<?php endif; ?>

					<?php
					$criteria = new CDbCriteria;
					$criteria->with = [
						'userMedicals' => [
							'together' => true,
						],
					];
					$criteria->compare('ownerId', $model->company->id);
					$criteria->compare('isActive', 1);
					$criteria->compare('agreementNew', 1);
					$addresses = Address::model()->findAll($criteria);
					if(count($addresses) > 1):
						?>
						<span class="item_name" title="Количество клиник"><?= count($addresses) ?></span>
						<span class="item_name"><?= (count($addresses)>=5 && count($addresses)<=20) ? 'клиник' : (count($addresses)%10 === 1 ? 'клиника' : (count($addresses)%10 <= 4 ? 'клиники' : 'клиник')) ?>:</span>
						<a href="<?= $model->getPagesUrls(true, true) ?>" class="show_all_addresses" style="position: relative;"><span>Посмотреть адреса</span></a>
						<div class="popup_content" style="display: none;">
							<div class="clinic_address<?= (count($addresses) <= 5) ? '_short' : '' ?>_popup">
								<div class=" h3 clinic_address_label"><span class="icon_hosp_mini"></span>Адреса <span class="color2">«<?= CHtml::encode($model->company->name) ?>»</span></div>
								<?php foreach ($addresses as $addr): ?>
									<div class="clinic_address_row">
										<div class="clinic_address_column">
											<div class="card_row">
												<span class="item_name" title="Метро">Метро:</span>
												<span class="item_value"><?= CHtml::encode(reset($addr->metroStations)->name) ?></span>
											</div>
											<div class="card_row">
												<span class="item_name" title="Адрес">Адрес:</span>
												<a href="<?= $addr->getPagesUrls(true, true) ?>">
													<span><?= CHtml::encode($addr->street) ?>, <?= CHtml::encode($addr->houseNumber) ?></span>
												</a>
											</div>
										</div>
										<?= Html::link('Записаться на приём', $addr->getPagesUrls(true, true), array('class' => 'btn-green clinic_address_button')) ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>

					<div class="card_row">
						<span class="item_name" title="Рабочие часы">Рабочие часы:</span>
					</div>
					<div>
						<table class="workingHours">
							<tbody>
							<?php
							$weeklyHour = $model->workingHours->getWeeklyHour();
							foreach ($weeklyHour["timetable"] as $key=>$row): ?>
								<tr>
									<td><?= $key ?></td>
									<td><?= $row ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>

				<?php if (!$model->canGetAppointments()):
					$inActiveClinic = 1;
					?>

					<div class="doctor_description">
						<div class="doctor-right-block box" style="position: absolute; top: 295px; width: 320px;">
							<div class="appointment" style="padding: 20px; width: auto; margin-top: 0px;">

								<div style="display: block; float: left; width: auto; height: 40px; margin-right: 10px; padding: 4px;">
									<img src="<?php echo Yii::app()->request->baseUrl.'/images/icons/not_available.png'?>">
								</div>

								<div class="h3 color2">Запись <br>недоступна!</div>
								<p style="font-size: 15px; line-height: 20px; padding: 12px 0;">
									Дорогой пациент, запись на прием в клинику <?= CHtml::encode($model->company->name) ?> через Единый Медицинский Портал на данный момент недоступна. Рекомендуем записаться на прием в другие ближайшие к вам клиники
								</p>

								<a href="<?= $this->createUrl('search/clinic',['Search[metroStationId]' => $metroLink]); ?>" style="text-decoration: none;">
									<div class="doctor-unavailable-block">
										Посмотреть клиники рядом
									</div>
								</a>
								<div class="clearfix"></div>

								<div style="margin-bottom:10px">

									<!--# block name="clinicView_emailBlock" -->
									<?= $this->renderPartial("//layouts/ssi_clinicView_emailBlock", ['link'=>$model->link, 'companyLinkUrl'=>$model->company->linkUrl, 'linkUrl'=>$model->linkUrl]); ?>
									<!--# endblock -->
									<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_clinicView_emailBlock&params[link]=<?=urlencode($model->link)?>&params[companyLinkUrl]=<?=urlencode($model->company->linkUrl)?>&params[linkUrl]=<?=urlencode($model->linkUrl)?>" stub="clinicView_emailBlock" -->

								</div>
							</div>
						</div>
					</div>

				<?php else : ?>

					<div class="min-price-xs">Стоимость приема от <?= $minPrice ?> р.</div>
					<?php if($model->samozapis == 1) : ?>
						<button class="btn-green btn_clinic_sign" type="submit" onclick="javascript: scrollToId('.SEARCH_RESULT'); return false;">Выбрать врача и записаться на прием</button>
					<?php else: ?>
						<div id="appointment_block" class="doctor-right-block box">
							<div id="appointment_wait_while_loading" class="appointment appointment_hidden" style="padding: 270px 0px; text-align: center;">
								<div class="h3 appointment_header">Обработка данных...</div>
								<div class="icon_loading" style="margin-top: 30px;"></div>
							</div>
							<div id="appointment_main" class="appointment">
								<!--# block name="appointmentForm" -->
								<?= $this->renderPartial("//clinic/appointmentForm", ['appointmentType'=>'visit_to_service','addressId'=>$model->id, 'serviceId' => $service->id]); ?>
								<!--# endblock -->
								<!--# include virtual="/index.php?r=ajax/renderPartial&name=//clinic/appointmentForm&params[appointmentType]=visit_to_service&params[addressId]=<?=$model->id?>&params[serviceId]=<?=$service->id?>" stub="appointmentForm" -->
								<div class="btn_securityOfAppointment">
									<a href="#securityOfAppointment" onclick="securityOfAppointment.show(); return false;">
							Почему запись на приём через ЕМП безопасна?
									</a>
								</div>
							</div>
						</div>
						<!-- <button class="btn-green btn_clinic_sign service_link" data-service-id="<?= $service->id ?>">Записаться на прием</button> -->
						<?php
						if($service) {
							// $this->renderPartial('//search/_service_popup',[
							// 	'addressServiceId'    => $service->id,
							// 	'addressServicePrice' => $service->price,
							// 	'description'         => $service->description,
							// 	'serviceName'         => $service->service->name,
							// 	'companyLink'         => $model->company->link,
							// 	'companyName'         => $model->company->name,
							// 	'addressName'         => $model->shortName,
							// 	'patientName'         => Yii::app()->user->model->name,
							// 	'patientPhone'        => Yii::app()->user->model->telefon,
							// 	'znp'                 => 1,
							// ]);
						}
						?>
					<?php endif; ?>
					
				<?php endif; ?>
			</div>
		</div>
	</div>


	<div class="static-map">
		<div class="img-map" style="background: url('https://static-maps.yandex.ru/1.x/?pt=<?=$model->longitude;?>,<?=$model->latitude;?>,comma&spn=0.001,0.001&l=map&size=640,300');"></div>
	</div>
	<div class="SECTION menu_group">
		<div class="main_column">
			<div class="menu_group_content">
				<div class="menu_group_cell menu_group_cell_2"><div class="icon_shape_mini"></div><a href="#doctor_description_section" onclick="javascript: scrollToId('.information#description');return false;">Подробное описание</a></div>
				<div class="menu_group_cell menu_group_cell_2"><div class="icon_ellipse_mini"></div><a href="#doctor_description_section" onclick="javascript: scrollToId('.information#price');return false;">Стоимость услуг</a></div>
				<?php if(!$model->getDisableReviewsParameter()): ?>
					<div class="menu_group_cell"><div class="icon_comments_mini"></div><span class="menu_group_count review_count"><?=$reviewsCount ? $reviewsCount : '' ;?></span><a class="review_comments" href="#doctor_description_section" onclick="javascript: scrollToId('.information#reviews');return false;"><?=$reviewsCount ? 'проверенных отзывов' : 'Оставьте отзыв' ;?></a></div>
				<?php endif; ?>
				<div class="menu_group_cell menu_group_cell_2"><div class="icon_doctor_mini"></div><span class="menu_group_count"><?= $dataProvider->totalItemCount ?></span><a href="#doctor_description_section" onclick="javascript: scrollToId('.SEARCH_RESULT');return false;">Специалистов</a></div>
			</div>
		</div>
	</div>


	<div class="SECTION doctor_description" id="doctor_description_section">
		<div class="main_column">
			<div class="doctor-middle-block box">
				<?php if ($model->userMedicals->agreementNew == 1 AND $model->isActive) : ?>
				<button class="btn-green information show-in-responsive" title="Записаться на приём" onclick="nextPageOpenContent('.main_content #appointment_block')" style="display:none; padding: 10px 20px 10px 10px; width: 100%; text-align: left; ">
					Записаться на приём
				</button>
				<?php endif; ?>
				<?php if ($model->description || $model->company->description): ?>
					<div class="information" id="description">
						<a class="roll-more opened"><span class="information_header"><span class="icon_shape_micro"></span>Описание</span></a>
						<div class="more" itemprop="description">
							<div class="doctor-title"><?= CHtml::encode($model->company->companyType->name) ?></div>
							<div>
								<?= nl2br($model->description ? CHtml::encode($model->description) : CHtml::encode($model->company->description)) ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php if (count($services) && $model->canGetAppointments()): ?>
					<div class="information" id="price">
						<a class="roll-more opened"><span class="information_header"><span class="icon_ellipse_micro"></span>Стоимость услуг от <?= $minPrice ?> р.</span></a>
						<?php
						$showPurchaseButton = true; //(Yii::app()->session['selectedRegion'] == 'spb');
						?>
						<div class="services more">
							<?php foreach ($services as $name => $category): ?>
								<div>
									<span><?= CHtml::encode($name) ?></span>
									<div class="list<?=(count($services)>1) ? " services-list" : ""?>">
										<?php foreach ($category as $service) {
											if($service["oldPrice"] != null) {
												echo "<div class='onlyEMPsalesList'>Только на ЕМП!</div>";
												break;
											}
										} ?>
										<table>
											<?php foreach ($category as $service): ?>
												<tr>
													<td>
														<?= CHtml::encode($service['name']) ?>
													</td>
													<td class="price<?= ($service["oldPrice"] != null) ? " stockSale" : "" ?>">
														<?php if($service["oldPrice"] != null): ?>
															<div class="oldPrice" style="text-decoration: line-through;"><?= $service["oldPrice"] ?></div>
														<?php endif; ?>
														<div class="priceShape"><?= ($service['free'] || intval($service['price'])==0) ? 'бесплатно' : $service['price'] . ' р.' ?></div>
													</td>
													<td>
														<?php if ($showPurchaseButton): ?>
															<a href="#" class="btn-purchase service_link" data-service-id="<?= $service['id'] ?>">купить</a>
															<?php
															$this->renderPartial('//search/_service_popup',[
																'addressServiceId'    => $service['id'],
																'addressServicePrice' => $service['price'],
																'description'         => $service['description'],
																'serviceName'         => $service['name'],
																'companyLink'         => $model->company->link,
																'companyName'         => $model->company->name,
																'addressName'         => $model->shortName,
																'patientName'         => Yii::app()->user->model->name,
																'patientPhone'        => Yii::app()->user->model->telefon,
															]);
															?>
														<?php endif; ?>
													</td>
												</tr>
											<?php endforeach; ?>
										</table>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>

				<?php if(!$model->getDisableReviewsParameter()): ?>
					<?php
					$this->renderPartial('//layouts/_postReview_Form',[
						'data' => array('companyId' => $model->company->id,
							'addressId' => $model->id, ),
					]);
					?>
					<?php
					$this->renderPartial('//layouts/_showReviews',[
						'data' => array('addressId' => $model->id),
						'reviews' => $reviews,
					]);
					?>
				<?php endif; ?>

				<?php
				$photos = $this->_model->galleryBehavior->getGalleryPhotos('gallery');

				$path = Yii::getPathOfAlias('shared');
				$pathTojRating = $path . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR;
				$urlScript = Yii::app()->assetManager->publish($pathTojRating . 'jquery.jcarousel.min.js');
				Yii::app()->clientScript->registerScriptFile($urlScript, CClientScript::POS_END);
				$urlScript = Yii::app()->assetManager->publish($pathTojRating . 'jcarousel.responsive.js');
				Yii::app()->clientScript->registerScriptFile($urlScript, CClientScript::POS_END);

				if (!empty($photos) && count($photos)): ?>
					<div class="information" id="photo_gallery">
						<a class="roll-more opened"><span class="information_header"><span class="icon_photogallery_micro"></span>Фотогалерея клиники</span></a>
						<div class="more jcarousel-wrapper">
							<div class="gallery jcarousel">
								<ul class="transition">
									<?php
									foreach ($photos as $photo) {
										echo '<li>' . CHtml::link(CHtml::image($photo->getPreview()), (Yii::app()->request->baseUrl . '/site/image?src=' . $photo->id . '.' . $photo->galleryExt), array('class' => 'fancybox-thumb', "rel" => "fancybox-thumb", "title" => CHtml::encode($photo->name))) . '</li>';
									}
									?>
								</ul>
							</div>
							<a href="#" class="jcarousel-control-prev"></a>
							<a href="#" class="jcarousel-control-next"></a>
						</div>
					</div>
				<?php endif; ?>

				<div class="share_buttons" style="min-width:inherit;" onclick="countersManager.reachGoal({'forCounters' : ['yandex'], 'goalName' : 'share_clinic_card'});">
					<script type="text/javascript" src="//yandex.st/share/share.js"	charset="utf-8"></script>
					<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter" ></div>
					<div class="text">Поделиться </div>
				</div>

			</div>
			<div class="klinika-right-block">
				<?php $this->widget('NewsReferenceBlock',['newsReferences'=>$model->company->newsReference()]); ?>
			</div>
		</div>
	</div>

	<?php if (!$inActiveClinic): ?>
		<div class="SECTION SEARCH_RESULT">
			<div class="main_column">
				<div>
					<div class="short-search-box">
						<div class="">

							<?php
							$form = $this->beginWidget('CActiveForm', array(
								'method' => 'get',
								'id' => 'search-doctor',
								'action' => '/doctor',
								'enableAjaxValidation' => false,
								'htmlOptions' => array('enctype' => 'multipart/form-data'),
							));
							?>
							<?= CHtml::hiddenField('shortSearch', 1); ?>
							<?= CHtml::hiddenField('typeSearch', 'doctor'); ?>
							<?= CHtml::hiddenField('noPushSearch', 1); ?>
							<?= CHtml::hiddenField('typePageResult', 1); ?>
							<div class="ajax_filter search-doctor-filter">
								<span class="item_title">Специальность врача</span>
								<?php
								echo CHtml::hiddenField(CHtml::activeName($searchModel, 'addressLink'), $model->link);
								$this->widget('ESelect2', [
									'name' => CHtml::activeName($searchModel, 'doctorSpecialtyId') . '_shortSearch',
									'data' => $doctorSpecialties,
									'value' => $searchModel->doctorSpecialtyId,
									'options' => [
										'placeholder' => 'Специальность врача',
										'allowClear' => true,
									],
									'htmlOptions' => [
										'style' => '
									margin-right:15px;
									width: 235px;
									height: 40px;
									border-radius: 3px 3px 3px 3px;
									background: white;',
									]
								]);
								?>
							</div>
							<div class="ajax_filter search-doctor-filter">
								<span class="item_title">Пол специалиста</span>
								<?php
								$this->widget('ESelect2', [
									'name' => CHtml::activeName($searchModel, 'sexId') . '_shortSearch',
									'data' => CHtml::listData(Sex::model()->findAll(), 'id', 'name'),
									'options' => [
										'placeholder' => 'Пол специалиста',
										'allowClear' => true,
										'minimumResultsForSearch' => -1,
									],
									'htmlOptions' => [
										'class' => 'search-doctor-refresh',
										'style' => '
									margin-right:15px;
									width: 235px;
									height: 40px;
									border-radius: 3px 3px 3px 3px;
									background: white;',
									]
								]);
								?>
							</div>
							<div class="ajax_filter search-doctor-filter" style="display:none">
								<span class="item_title">Стоимость обращения</span>
								<?php
								$this->widget('ESelect2', [
									'name' => CHtml::activeName($searchModel, 'price') . '_shortSearch',
									'data' => [
										1 => 'Стоимость обращения',
										2 => 'от 0 до 500',
										3 => 'от 500 до 1000',
										4 => 'от 1000 до 2000',
										5 => 'от 2000 до 3000',
										6 => 'от 3000 до 5000',
										7 => 'от 5000',
									],
									'options' => [
										'allowClear' => false,
										'placeholder' => 'Стоимость обращения',
										'minimumResultsForSearch' => -1,
									],
									'htmlOptions' => [
										'class' => 'search-doctor-refresh',
										'style' => 'margin-right:20px;',
									]
								]);
								?>
							</div>
							<div class="search-doctor-filter ajax_filter">
								<span class="item_title">Стоимость обращения (рубли)</span>
								<?= $form->hiddenField($searchModel, 'priceMin', array('class'=>'offscreen')); ?>
								<?= $form->hiddenField($searchModel, 'priceMax', array('class'=>'offscreen')); ?>
								<?php $this->widget('PriceSlider', array('id' => 'doctor_price_slider', 'formId' => 'search-doctor', 'name' => 'Search', 'inputMin' => 'priceMin', 'inputMax' => 'priceMax')); ?>
							</div>
							<?php
							$metro = Yii::app()->request->getParam('Search')['metroStationId'] ? Yii::app()->request->getParam('Search')['metroStationId'] : '';
							Yii::app()->clientScript->registerScript('select22', '
							$(document).ready(function() {		
								$("#Search_sexId1").select2("val", ["' . Yii::app()->request->getParam('Search')['sexId'] . '"]);
								$("#Search_price1").select2("val", ["' . Yii::app()->request->getParam('Search')['price'] . '"]);
							});	
						');
							$this->endWidget();
							?>
						</div>
					</div>
					<div class="search-result-head"<?=$searchResultHead_style?>>
						<span class="search-found">Специалисты клиники: <a class="search-found-count" title="<?=$dataProvider->totalItemCount?> врачей"><?=$dataProvider->totalItemCount?></a></span>
						<span class="filtr-ms address-block-mx">ФИЛЬТРЫ</span>
					</div>
					<div class="search-result">
						<!-- Результаты поисковой выдачи -->

						<?php
						Yii::app()->clientScript->registerScript('update_shortSearch', '
						$(document).ready(function() {
							var form = $("#shortSearch").closest("form");
							if(form.length > 0) {
								refreshSearch(0, form);
							}
						});	
					');
						?>
					</div>
					<div class="search_result_footer">
						<button id="search_result_append_button" class="show_more_btn btn-blue" title="Показать больше врачей" href="#show_more" onclick="appendSearch(this, '#search-doctor', true, 'doctor');">Показать больше врачей</button>
						<div id="search_result_loading" class="icon_loading" style="display: none;"></div>
						<input id="search_result_page_number" type="hidden" value="1">
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php if (!$model->canGetAppointments()): ?>
	<div class="SECTION TOP_DOCS" id="topDocsOnClinicView">
		<div class="main_column">
			<div class="top_clinic_containter">
				<div class="header_section">
					<h2 class="color1">Другие клиники</h2>
				</div>

				<a href="#topdoctor_drop_left" class="icon_topdoctor_drop_left"></a>
				<a href="#topdoctor_drop_right" class="icon_topdoctor_drop_right"></a>

				<div class="top_docs_wrapper">
					<div class="screen_loading" style="display: none;"></div>
					<div class="top_docs_content"></div>
				</div>
			</div>

			<script type="text/javascript">
				$( ".TOP_DOCS .main_column .icon_topdoctor_drop_left" ).css('visibility', 'hidden');
				$( document ).ready(function() {
					try {
						load_top_docs_containter('topDocsOnClinicView','clinic','/ajax/similarClinic?addressLinkUrl=<?= $model->linkUrl ?>');
					} catch(e) {console.log(e)}
				});
			</script>
		</div>
	</div>
<?php endif; ?>

<?php Yii::import('application.views.layouts._securityOfAppointment_modal', true); ?>

<script>
	$( document ).ready(function() {
		try {
			$.ajax({
				url: '<?= $this->createUrl('clinic/map', ['linkUrl' => $model->linkUrl, 'companyLink' => $model->company->linkUrl]); ?>',
				type: "GET",
				success: function(response) {
					try {
						$(".map_block").html(response);
					} catch(e) {}
				},
				error: function(xhr) {
					$(".map_block").first().html("<div class='h3' style='text-align:right;'>Карта не загружена</div>");
				},
			});
		} catch(e) {}
	});

	/* resize cells */

	function checkRightBlockHeight() {

		var $elem = $('.doctor-right-block').find('.appointment').first(),
			$height,
			$menu_group_cells;

		if (!$elem.length) return false;
		$height = parseInt($elem.height());
		$menu_group_cells = $('.menu_group_cell');

		if ($height>316) {
			$.each($menu_group_cells, function(i, cell) {
				$obj = $(cell);
				$obj.removeClass('menu_group_cell_2');
			});
		} else {
			$.each($menu_group_cells, function(i, cell) {
				$obj = $(cell);
				$obj.addClass('menu_group_cell_2');
			});
		}
	}

	checkRightBlockHeight();
</script>
