<?php
/* @var $model Companies */
/* @var $comment Comments */

$this->breadcrumbs = array(
	'Клиники'				 => $this->createUrl('search/clinic'),
	$model->company->name	 => array("/clinic/view", 'companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl),
	'Оставить отзыв'
);
?>

<div class="search-result">
	<table class="search-result-table">
		<tr class="search-result-box">
			<?php $this->renderPartial('_leftBlock', array('model' => $model)); ?>
			<td class="search-result-info" width="70%">
				<div class="search-result-title"><h2><?= CHtml::encode($model->company->name) ?></h2></div>
				<?php
				/* @var $form CActiveForm */
				$form = $this->beginWidget('CActiveForm',
					array(
						'id'					 => 'comment-form',
						'enableAjaxValidation'	 => false,
					));
				?>
				<table class="service-table">
					<tr>
						<td valign="middle">Ваша оценка</td>
						<td>
							<?= $form->dropDownList($comment, 'rating', Comment::$ratingList, array('class' => 'custom-select')) ?>
						</td>
					</tr>
					<tr>
						<td>Текст отзыва</td>
						<td>
							<?= $form->textArea($comment, 'content', array('class' => 'custom-textarea')) ?>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="submit" class="btn-green" title="Отправить">
								Отправить
							</button>
						</td>
					</tr>
				</table>
				<?php $this->endWidget(); ?>
				<table class="reviews-list">
					<tr>
						<td>
							<br>
							<?php foreach ($model->comments as $comment): ?>
								<div class="reviews-box">
									<i class="reviews-box-arrow"></i>
									<blockquote><?= CHtml::encode($comment->content) ?></blockquote>
									<p class="reviews-amount"><a href="#">Отзыв #<?= $comment->id?></a></p>
								</div>
							<?php endforeach; ?>
							<br>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<script>
		$(document).ready(function(){
			$('.title-row').next('table').css('display','none');
			$('.title-row').toggle(
			function(){
				$('.title-row').next('table').hide('slow');
				$(this).next('table').show('slow');
			},
			function(){
				$(this).next('table').hide('slow');
			}
		);
		});
	</script>
</div>