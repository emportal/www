
<?php if($time): ?>

<table class="calendar f-left">                                                                             
	<tbody><tr class="<?=$backend ? 'calendar-row-admin' : 'calendar-row'?>">
			<td class="calenadar-arr <?=$short ? 'micro_nav_cal' : 'nav_cal'?>"><a class="btn_nav" title="предыдущие" data-type="back" href="#">Назад</a></td>
			<?php foreach($time as $k=>$v):	?>
			<td class="time_block">
				<div class="calendar-title "><?=CHtml::encode($v['date'])?></div>    
				
				<?php 
				if(empty($v['time'])) {
					
					echo CHtml::encode($v['msg']);
				}
				foreach($v['time'] as $t): ?>	
					<?php switch($t['status']):
						case 'hidden': 
							#echo '<a title="'.CHtml::encode($t['text']).'" class="btn '.(explode(" ",$model->plannedTime)[1] == $t['text'].":00" && explode(" ",explode("-",$model->plannedTime)[2])[0]==explode(".",$v['date'])[0] ? 'active' : '') .'" href="#">'.CHtml::encode($t['text']).'</a>';
							break;
						case 'allow': 
							echo '<a title="'.CHtml::encode($t['text']).'" class="btn '.(explode(" ",$model->plannedTime)[1] == $t['text'].":00" && explode(" ",explode("-",$model->plannedTime)[2])[0]==explode(".",$v['date'])[0] ? 'active' : '') .'" href="#">'.CHtml::encode($t['text']).'</a>';
							break;
						case 'disabled': 
							echo '<a onclick="return false" title="'.CHtml::encode($t['text']).'" class="btn btn-blue'.(explode(" ",$model->plannedTime)[1] == $t['text'].":00" && explode(" ",explode("-",$model->plannedTime)[2])[0]==explode(".",$v['date'])[0] ? 'active' : '') .'" href="#">'.CHtml::encode($t['text']).'</a>';
							break;
						case 'busy': 
							echo '<a onclick="return false" title="'.CHtml::encode($t['text']).'" class="btn btn-red'.(explode(" ",$model->plannedTime)[1] == $t['text'].":00" && explode(" ",explode("-",$model->plannedTime)[2])[0]==explode(".",$v['date'])[0] ? 'active' : '') .'" href="#">'.CHtml::encode($t['text']).'</a>';
							break;
						
						
					 endswitch; ?>
					<?php /*if(!$t['hide']): ?>
						<a title="<?=CHtml::encode($t['text'])?>" class="btn <?= explode(" ",$model->plannedTime)[1] == $t['text'].":00" && explode(" ",explode("-",$model->plannedTime)[2])[0]==explode(".",$v['date'])[0] ? 'active' : '' ?>" href="#"><?=CHtml::encode($t['text'])?></a>
					<?php endif;*/?>		
					
				<?php endforeach; ?>
				
				
			</td> 
			<?php endforeach; ?>		                                       
			                                   
			<td class="calenadar-arr <?=$short ? 'micro_nav_cal' : 'nav_cal'?>"><a  class="btn_nav" title="следующие" data-type="forward" href="#">Вперед</a></td>
		</tr>                                        
	</tbody>
</table>
<?php else: ?>
	Клиника не указала часы работы
<?php endif; ?>