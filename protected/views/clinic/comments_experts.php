<?php
/* @var $model Address */
/* @var $comment Comment */

$this->breadcrumbs = array(
	'Клиники'				 => $this->createUrl('search/clinic'),
	$model->company->name	 => array("/clinic/view", 'companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl),
	'Отзывы'
);
?>



<div class="search-result-title">
	<h2>
		<a href="#" title="<?= CHtml::encode($model->company->name) ?>">
			<?= CHtml::encode($model->company->name) ?>
		</a>
		<small>
			<?= CHtml::encode($model->company->companyType->name) ?>
		</small>
	</h2></div>

<?= CHtml::link('Отзывы о клинике', array('clinic/comments','companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl), array('class'=> 'btn clinic'))?>
&nbsp;
<?= CHtml::link('Отзывы о врачах', array('clinic/comments_experts','companyLink' => $model->company->linkUrl, 'linkUrl' => $model->linkUrl), array('class'=> 'btn doctor'))?>
<?php if (empty($comments)) : ?>
	<h3 style="text-align: center; margin: 20px 0px">нет отзывов</h3>
<? else: ?>
	<table class="reviews-list">
		<tr>
			<td>
				<br>
				<?php foreach ($comments as $comment): ?>
					<div class="reviews-box">
						<i class="reviews-box-arrow"></i>
						<blockquote>
							Доктор: <?= CHtml::encode($comment->doctor->name) ?><br/>
							<?= CHtml::encode($comment->review) ?>
						</blockquote>
						<p class="reviews-amount"><a href="#">Отзыв #<?= $comment->date ?>   <?= CHtml::encode($clinic->street) ?><?= CHtml::encode($clinic->houseNumber) ?></a></p>
					</div>
					<?php if($comment->answer):?>
					<div class="answer">
						<div class="reviews-box">
							<blockquote>
								<?= CHtml::encode($comment->answer) ?>
							</blockquote>
						</div>
					</div>
					<?php endif;?>
				<? endforeach; ?>
				<br>
			</td>
		</tr>
	</table>
<? endif; ?>

<script>
	$(document).ready(function() {
		$('.title-row').next('table').css('display', 'none');
		$('.title-row').toggle(
				function() {
					$('.title-row').next('table').hide('slow');
					$(this).next('table').show('slow');
				},
				function() {
					$(this).next('table').hide('slow');
				}
		);
	});
</script>
