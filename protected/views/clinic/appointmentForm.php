<?php 
$appointment = new AppointmentToDoctors($appointmentType);

$addr = Address::model()->findByPk($addressId);
$appointment->serviceId = $serviceId;
$appointment->company = $addr->company;
$appointment->address = $addr;
$appointment->name = Yii::app()->user->model->name;
$appointment->phone = Yii::app()->user->model->telefon;
$appointment->email = Yii::app()->user->model->email;
if (Yii::app()->request->getParam(get_class($appointment))) {
	$appointment->attributes = Yii::app()->request->getParam(get_class($appointment));
}
if (!Yii::app()->user->isGuest) {
	//Если пользователь зарегистрирован - проверяем есть ли у него подтвержденный телефон для записи на 1 раз
	$pv = PhoneVerification::model()->find("status=:status AND userId = :userId", array(
		':status' => PhoneVerification::APPOINTMENT,
		':userId' => Yii::app()->user->model->id,
	));
}
?>
<?php
						$form = $this->beginWidget('CActiveForm', array(
							'method' => 'post',
							'id' => 'doctor-visit',
							'enableAjaxValidation' => true,
							'enableClientValidation' => false,
							'clientOptions' => array(
								'validateOnSubmit' => true,
								'inputContainer' => 'td',
								'validateOnChange' => false,
								'beforeValidate' => 'js:function(form) {
								
									var bv_phone = $("#phoneInput").val();
									if (bv_phone) {
										bv_phone = bv_phone.replace(/ |-|[(]|[)]/g, "");
										$("#phoneInput").val(bv_phone);
									}
									
									$( "#appointment_main" ).addClass( "appointment_hidden" );
									$( "#appointment_wait_while_loading" ).removeClass( "appointment_hidden" );
									scrollToId("#appointment_block");									
									if($("#code").val()) {
										acceptCodeForAppointment();
									}
									return true;
								}',
								'afterValidate' => 'js: function(form,data,hasError) {
									if(hasError === false) {
										//countersManager.reachGoal({
										//	"forCounters": ["yandex", "google"],
										//	"goalName": "appointment_to_doctor",
										//	"callback": function() {
										//		alert(\'ok\');
										//	}
										//});
									}
									
									//$( "#appointment_wait_while_loading" ).addClass( "appointment_hidden" );
									//$( "#appointment_main" ).removeClass( "appointment_hidden" );
									scrollToId("#appointment_block");
									console.log(data);
									if(data["external_visit"] !== undefined) {
										if(data["external_visit"]["success"]) {
											//$("#appointment_result_header").text(data["external_visit"]["text"]);
											$("#appointment_result_patientName").text(data["external_visit"]["patient"]["name"]);
											$("#appointment_result_patientBirthday").text(data["external_visit"]["patient"]["birthday"]);
											$("#appointment_result_doctorName").text(data["external_visit"]["doctor"]["name"]);
											$("#appointment_result_doctorSpesiality").text(data["external_visit"]["doctor"]["spesiality"]);
											$("#appointment_result_companyName").text(data["external_visit"]["company"]["name"]);
											$("#appointment_result_companyAddress").text(data["external_visit"]["company"]["address"]);
											$("#appointment_result_plannedTime").text(data["external_visit"]["plannedTime"]);
											$("#appointment_result_IdAppointment").text(data["external_visit"]["IdAppointment"]);
											$("#appointment_result").show();
											$("#appointment_main").hide();
											try { if(!isMobile()) setTimeout(show_pop_up_spasibo,500); } catch(e){}
										} else {
											$("#AppointmentToDoctors_plannedTime_em_").text(data["external_visit"]["text"]);
											$("#AppointmentToDoctors_plannedTime_em_").show();
										}
										$( "#appointment_wait_while_loading" ).addClass( "appointment_hidden" );
										$( "#appointment_main" ).removeClass( "appointment_hidden" );
										return false;
									}

									if((data["AppointmentToDoctors_phone"] !== undefined && data["AppointmentToDoctors_phone"][0] != "Телефон не активирован") || data["AppointmentToDoctors_name"] !== undefined ) {
										$( "#appointment_wait_while_loading" ).addClass( "appointment_hidden" );
										$( "#appointment_main" ).removeClass( "appointment_hidden" );
										return false;
									}
									
									if(data["AppointmentToDoctors_phone"] !== undefined && data["AppointmentToDoctors_phone"][0] === "Телефон не активирован") {
										$( "#appointment_wait_while_loading" ).addClass( "appointment_hidden" );
										$( "#appointment_main" ).removeClass( "appointment_hidden" );
										if($("#code:visible").length == 0 ) {
											$(".getCodeButton").click(); // = getCodeForAppointment()
										}
										return false;
									}
									
									if(form.find("#AppointmentToDoctors_phone").val() == "") {
										//return false;
									}
									
									window.show_on_unload = 1;
									//$("#doctor-visit").submit();
									document.getElementById("doctor-visit").submit();
									return true;
								}'
							),
							'htmlOptions' => array('class' => 'zakaz relative', 'enctype' => 'multipart/form-data'),
						));
						/* @var $form CActiveForm */
					?>
					<div class="h3 appointment_header">Запиcь на приём</div>
					<?= $form->hiddenField($appointment, 'address[link]') ?>
					<table class="search-result-table">
						<tr class="search-result-box">
							<td class="search-result-info">
								<div class="relative ap_cal">
									<div id="calendar">
										<?php
										$this->renderPartial('/clinic/_newCalendarBlockClinic', array(
											'address' => $address,
										));
										?>
									</div>
									<?php if(!$isSamozapis): ?>
									<script>
										$(function() {
											$(document).ready(function() {
												updateAvailableTime('', $('#datetimepicker'));
											});
										});
									</script>
									<?php endif; ?>
									<input type="hidden" id="timeblock_date" name="date" value="<?= date("Y-m-d", mktime(0, 0, 0, date('m'), date('j'), date('Y'))) ?>"/>
									<input type="hidden" name="patient[Surname]" id="patient_Surname" value=""/>
									<input type="hidden" name="patient[Name]" id="patient_Name" value=""/>
									<input type="hidden" name="patient[SecondName]" id="patient_SecondName" value=""/>
									<input type="hidden" name="patient[Birthday]" id="patient_Birthday" value=""/>
									<input type="hidden" name="patient[Surname]" id="patient_Surname" value=""/>
									<input type="hidden" name="serviceId" id="serviceId" value="<?=$serviceId?>"/>						
								</div>
							</td>
						</tr>
						<tr class="search-result-box">
							<td class="search-result-info">
								<span id="appointment_item_name_fio_title" class="appointment_item_name">Фамилия и Имя пациента</span>
								<div class="relative ap_name">
									<?= $form->textField($appointment, 'name', array('class' => 'custom-text', 'placeholder' => 'Фамилия и Имя пациента')); ?><br/>
									<?= $form->error($appointment, 'name') ?>
								</div>
							</td>
						</tr>
						<tr class="search-result-box">					
							<td class="search-result-info">
								<div class="relative ap_phone">
									<?php if (Yii::app()->user->isGuest): ?>					
										<?=
										$form->hiddenField($appointment, 'phone', array(
											'value' => Yii::app()->session['appointment_phone_set'] ? Yii::app()->session['appointment_phone_set'] : ''
										));
										?>
										<?php if (Yii::app()->session['appointment_phone_set']): ?>
											<span id="phoneSpan" class="phone" style=""><?= Yii::app()->session['appointment_phone_set'] ?></span>
											<span onclick="removePhoneForAppointmentGuest();" class="btn-red" style="">отменить номер</span>			
										<?php else: ?>
											<span id="phoneSpan" class="phone" style="display:none"></span>
											<span class="appointment_item_name">Ваш телефон
												<input type="text" placeholder="Ваш телефон" class="custom-text" id="phoneInput" name="phoneInput" />
											</span>
											<span class="phone-help appointment_item_name invisible">На телефон придет код подтверждения</span>
											<script>
											$(function() {
												$("#phoneInput").mask("+7 (999) 999-99-99", {placeholder: "_"});
												var isGuest = 1;
											});
											</script>
											<span onclick="getCodeForAppointment();" class="getCodeButton btn-green" style="display:none">получить код</span>			
											<span onclick="acceptCodeForAppointment();" class="acceptButton btn-green" style="display:none">подтвердить</span>
										<?php endif; ?>					
									<?php else: ?>
										<?php if ($pv): ?>
											<?= CHtml::hiddenField('default', $appointment->phone); ?>
											<?=
											CHtml::activeHiddenField($appointment, 'phone', array(
												'value' => $pv->phone
											));
											?>						
											<span id="phoneSpan" class="phone"><?= $pv->phone ?></span>
											<span onclick="removePhoneForAppointment();" class="removeButton btn-red">Вернуть стандартный телефон</span>
										<?php else: ?>
											<?= $form->hiddenField($appointment, 'phone') ?>
											<span id="phoneSpan" class="phone displaynone"><?= $appointment->phone ?></span>
											
											<span class="appointment_item_name">Ваш телефон
												<input type="text" value="<?= $appointment->phone ?>" class="custom-text" id="phoneInput" name="phoneInput" />
											</span>
											<script>
											$(function() {
												$("#phoneInput").mask("+7 (999) 999-99-99", {placeholder: "_"});
												var isGuest = 1;
											});
											</script>
											
											<!--<span onclick="changePhoneForAppointment()" class="btn-green changePhoneAppointment">
												Поменять телефон на одну запись
											</span>-->
											<span onclick="getCodeForAppointment();" class="getCodeButton btn-green" style="display:none">получить код</span>			
											<span onclick="acceptCodeForAppointment();" class="acceptButton btn-green" style="display:none">подтвердить</span>
										<?php endif; ?>						
									<?php endif; ?>
									<?= $form->error($appointment, 'phone') ?>
									<div style="display:none" id="phone_tooltip">
										<!--<div class="triangle_left"></div>-->
										<div class="app_tooltip flash-notice">
											Ваш номер нигде не выводится и используется <b>только</b> для записи на приём.<br/>Вся процедура бесплатна.<br><br>Пример для России: <b>+79217234455</b>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr class="search-result-box">
							<td class="search-result-info">
								<button class="btn-green btn_doctor_sign" style="display: block;" type="submit">Записаться на приём</button>
								<?php
								$phones = UserRegion::getRegionPhones();
								if ($phones['empRegistryPhone']) : 
								?>
									<div class="clearfix"></div>
								<?= $this->renderPartial("//layouts/ssi_doctorView_RegistryPhone_2", ['phones'=>$phones]); ?>
								<?php endif; ?>
								
								<input type="hidden" style="" id="noVerification" name="noVerification" value="0" type="text"/>
							</td>
						</tr>
					</table>
					<?php $this->endWidget(); ?>
					
<?php
if (Yii::app()->user->isGuest) {
	$this->renderPartial('//layouts/_pop_up_spasibo'); 
}
?>