<?php
/* @var $this DoctorsController */
/* @var $model Doctor */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idDoctors'); ?>
		<?php echo $form->textField($model,'idDoctors',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsName'); ?>
		<?php echo $form->textField($model,'DoctorsName',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UsersId'); ?>
		<?php echo $form->textField($model,'UsersId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsCreationDate'); ?>
		<?php echo $form->textField($model,'DoctorsCreationDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsSurName'); ?>
		<?php echo $form->textField($model,'DoctorsSurName',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsFirstName'); ?>
		<?php echo $form->textField($model,'DoctorsFirstName',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsFatherName'); ?>
		<?php echo $form->textField($model,'DoctorsFatherName',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsBirthday'); ?>
		<?php echo $form->textField($model,'DoctorsBirthday'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SexId'); ?>
		<?php echo $form->textField($model,'SexId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsExperience'); ?>
		<?php echo $form->textField($model,'DoctorsExperience',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsExperienceDate'); ?>
		<?php echo $form->textField($model,'DoctorsExperienceDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsExperiencePeriod'); ?>
		<?php echo $form->textField($model,'DoctorsExperiencePeriod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'StaffUnitsId'); ?>
		<?php echo $form->textField($model,'StaffUnitsId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsOnHouse'); ?>
		<?php echo $form->textField($model,'DoctorsOnHouse'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CurrentPlaceOfWork'); ?>
		<?php echo $form->textField($model,'CurrentPlaceOfWork',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DoctorsComment'); ?>
		<?php echo $form->textField($model,'DoctorsComment',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->