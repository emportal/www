<?php
$vkAddress = [];
foreach ($vkAppModel as $value) {
	if($value->addressId) {
		$vkAddress[] = $value->addressId; 
	}
}
?>
<?php
	$this->renderPartial('_vkAdmin', [
						'isVkApp'       => $isVkApp,
						'vkGroupId'     => $vkGroupId,
						'vkViewerType'  => $vkViewerType,
						'vkAppModel'    => $vkAppModel,	
						'vkClinic'      => $vkClinic,							
					]);
?>
<div style="width: 100%; text-align: center;">
	<?php 
		foreach ($addressList as $value): 
			if(!$vkAppModel[0]->addressId || in_array($value->id, $vkAddress)):
	?>
				<?php if (!$addressCap[$value->id]['service'] || !$addressCap[$value->id]['doctor']): ?>
					<div style="margin: 20px;">
						<a class="btn-green information show-in-responsive" style="width: 100%;padding: 14px;font-size: 26px;" href="/<?=!$addressCap[$value->id]['service']?'doctor':'diagnostics';?>?vk=1&address=<?=$value->id;?>"><?=$value->name;?></a>
					</div>
				<? else: ?>
					<div style="margin: 20px;">
						<button class="btn-green vkButChange" style="width: 100%;padding: 14px;font-size: 30px;"><?=$value->name;?></button>
						<div class="vkChange" style="display: none;">
							<div style="padding: 10px;font-weight: bold;font-size: 22px;"><?=$value->name;?></div>
							<a class="btn-green" style="width: 49%;padding: 14px;font-size: 26px;margin-top: 5px;" href="/doctor?vk=1&address=<?=$value->id;?>">Врачи</a>
							<a class="btn-green" style="width: 49%;padding: 14px;font-size: 26px;margin-top: 5px;" href="diagnostics?vk=1&address=<?=$value->id;?>">Услуги</a>
						</div>
					</div>
				<? endif; ?>

	<?php 
			endif;
		endforeach; 
	?>
</div>
<script type="text/javascript">
	$(".vkButChange").on('click', function() {
		$(this).hide();
		$(this).parent().children('div.vkChange').show();
	})
</script>