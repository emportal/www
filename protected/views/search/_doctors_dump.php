<?php
/* @var $data Doctor */
/* @var $this SearchController */
?>
<?php if ($_REQUEST['photo'] && !$data->photo): ?>

<?php else: ?>

	<tr class="search-result-box <?= ($index % 2) ? 'odd' : '' ?>">
		<td class="search-result-signup">
			<?php if ($data->photo): ?>
				<?= CHtml::link(CHtml::image($data->photoUrl, 'Врач'), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->company->linkUrl, 'addressLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->address->linkUrl),['class'=>'photo-doctor']); ?>
			<?php else: ?>
				<?php if ($data->sex->order == 0): ?>
					<?= CHtml::link(CHtml::image($this->assetsImageUrl . '/staff-m.jpg', 'Врач'), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->company->linkUrl, 'addressLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->address->linkUrl),['class'=>'photo-doctor'])
					?>
				<?php else: ?>
					<?= CHtml::link(CHtml::image($this->assetsImageUrl . '/staff-f.jpg', 'Врач'), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->company->linkUrl, 'addressLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->address->linkUrl),['class'=>'photo-doctor'])
					?>
				<?php endif; ?>
			<?php endif; ?>
			<?php if ($show): ?>
				<?= Html::link('Записаться на прием', array('/doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->company->linkUrl, 'addressLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->address->linkUrl), array('class' => 'btn-green')) ?>
			<?php endif; ?>
		</td>
		<td class="search-result-info">
			<div class="search-result-title">
				<h2>
					<?= Html::link(CHtml::encode($data->name), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->company->linkUrl, 'addressLinkUrl' => $doctors[$data->id]->placeOfWorks[0]->address->linkUrl)) ?>				
				</h2>			
				<br/>
				<?php /*
				  $this->widget('CStarRating', [
				  'name' => 'rating-' . $data->link,
				  'id' => 'rating-' . $data->link,
				  'value' => $data->roundedRating,
				  'ratingStepSize' => 0.5,
				  'starCount' => 5,
				  'cssFile' => false,
				  'readOnly' => true,
				  'minRating' => 0.5,
				  'maxRating' => 5,
				  ]); */
				?>
				<input type="range" min="0" max="5" value="<?= $data->roundedRating ?>" step="0.1" id="backing-<?= $data->link ?>">
				<span id="rate-<?= $data->link ?>"data-rateit-value="<?= $data->roundedRating ?>"
					  data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $data->link ?>"></span>
				<span class="rating-text"><?= $data->boldedRoundedRating; ?></span>

				<script>
				$(function() {
					jQuery('#rate-<?= $data->link ?>').rateit({max: 5, step: 0.1, backingfld: '#backing-<?= $data->link ?>'});
				});
				</script>

				<?php /* ?>
				  <div class="rating">
				  <div class="rating-inner" style="width:20%"></div><!-- /rating-inner -->
				  <b class="rating-count"><span>4.</span>5</b><!-- /rating-count -->
				  </div><!-- /rating -->
				  <? */ ?>
			</div><!-- /search-result-title -->
			<p class="size-12">
				<?php
				if (count($doctors[$data->id]->scientificDegrees)) {
					echo CHtml::encode($doctors[$data->id]->scientificDegrees[0]->name) . "<br/><br/>";
				}
				?>
				<?php if ($doctors[$data->id]->scientificTitle): ?>
					<?php $doctors[$data->id]->scientificTitle->name ? ", " . CHtml::encode($doctors[$data->id]->scientificTitle->name) . "<br/>" : '' ?>	
				<?php endif; ?>

				<?php foreach ($doctors[$data->id]->specialtyOfDoctors as $specialty) : ?>
					<?php
					if (!empty($specialty->doctorSpecialty->name)):
						echo CHtml::encode($specialty->doctorSpecialty->name);
					endif;

					if (!empty($specialty->doctorCategory->name)):
						echo CHtml::encode($specialty->doctorCategory->name) ? ", " . CHtml::encode($specialty->doctorCategory->name) . "" : '';
					endif;
					?><br/>
				<? endforeach; ?>
			</p>	
			<p class="size-12 experience">
				<?php if ($data->getExperience()): ?>
					<span title="Стаж"></span> Стаж <?= $data->getExperience() ?>
				<?php endif; ?>
			</p>

			<?php /* if ($data->appointmentCount): ?>
				<p class="size-12">Кол-во записей к врачу: <?= $data->appointmentCount; ?></p>
			<?php else: ?>			
			<?php endif;*/ ?>
			<p class="size-12 company-icon">
				<span title="Клиника"></span>
				<?= Html::link(CHtml::encode($doctors[$data->id]->placeOfWorks[0]->company->name), array('/clinic/view','#'=>'info','companyLink' => $doctors[$data->id]->placeOfWorks[0]->company->linkUrl, 'linkUrl' => $doctors[$data->id]->placeOfWorks[0]->address->linkUrl)) ?>		
			</p>
			<div>			
				<?php if(is_array($doctors[$data->id]->placeOfWorks[0]->address->metroStations) && count($doctors[$data->id]->placeOfWorks[0]->address->metroStations)): ?>
				<div style="float:left;margin-right:30px;">
					<span class="metro">
						<span title="Станция метро"></span> 				
						<?= is_array($doctors[$data->id]->placeOfWorks[0]->address->metroStations) ? '' . CHtml::encode(reset($doctors[$data->id]->placeOfWorks[0]->address->metroStations)->name) : '' ?>			
					</span>
				</div>
				<?php endif; ?>
				<div style="float:left">
					<span class="address">
						<?= Html::link(CHtml::encode($doctors[$data->id]->placeOfWorks[0]->address->street) . ', ' . CHtml::encode($doctors[$data->id]->placeOfWorks[0]->address->houseNumber), array('/clinic/view','#'=>'info','companyLink' => $doctors[$data->id]->placeOfWorks[0]->company->linkUrl, 'linkUrl' => $doctors[$data->id]->placeOfWorks[0]->address->linkUrl)); ?>
					</span>
				</div>
				<div class="clearfix"></div>
			</div>



			<?php if ($doctors[$data->id]->inspectPrice): ?>
				<em class="price-box">
					<span class="price-box-inner">
						Стоимость приема <b><?= $doctors[$data->id]->inspectPrice ?></b> р.
					</span>
				</em>
			<?php elseif($doctors[$data->id]->inspectFree): ?>
				<em class="price-box">
					<span class="price-box-inner">
						Бесплатный прием
					</span>
				</em>
			<?php endif; ?>
		</td>	
	</tr>
<?php endif; ?>