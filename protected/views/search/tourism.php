<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = "Лечебные и спортивно оздоровительные туры – Туристические фирмы – ".Yii::app()->name;

Yii::app()->clientScript->registerMetaTag(Yii::app()->name." – это полный каталог туристических компаний и врачебных организаций,"
		. " у нас вы сможете подобрать лечебные и спортивно оздоровительные туры."
		. " С помощью нашего портала вы найдете клинику или врача нужного профиля.",'description');


$this->breadcrumbs = array(
	'Search' => array('/search'),
	'Clinic',
);
?>


<div class="search-result">
	<?php if(empty($dataProvider)):?>
		<?php echo CHtml::errorSummary($searchModel); ?>
		<div class="search-result-head">
	<?php else:?>
			<span class="search-found">Найдено <a title="<?= $dataProvider->totalItemCount ?> учреждений"><?= $dataProvider->totalItemCount ?> учреждений</a></span>
			<!--<ul class="search-regularize">
				<li>Упорядочить по:</li>
				<li><a href="#" title="Рейтингу">Рейтингу</a></li>
				<li class="current"><a href="#" title="Стоимости">Стоимости</a></li>
			</ul>< /search-regularize -->
		</div><!-- /search-result-head -->
		<br />
		<?php
		$this->widget('zii.widgets.CListView',
				   array(
			'dataProvider'	 => $dataProvider,
			'itemView'		 => '_tours',
			'itemsTagName'	 => 'table',
			'template'		 => "{pager}\n{items}\n{pager}",	
			'viewData'		 => ['addr'=>$addr],
			'afterAjaxUpdate' => 'function(id,date) {
				window.scrollTo(0, 200);				
			}',
			 'pager'=>array(
				'maxButtonCount'=>5,
				'header'=>'',
				'cssFile'=>false
			),
		));
		?>
		<br>
	<?php endif;?>
</div><!-- /search-result -->