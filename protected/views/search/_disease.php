<?php
/* @var $data Disease*/
/* @var $this Controller*/
?>
<?php if($data->name && $data->link): ?>
	<?= Html::link(CHtml::encode($data->name), array('/handbook/disease', 'linkUrl'=>$data->linkUrl))?>
<?php endif; ?>