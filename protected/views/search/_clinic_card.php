<?php
if(is_array($addresses) && isset($addresses[0])) {
	$absoluteLink = Yii::app()->request->getParam('absoluteLink',false) ? MyTools::url_origin($_SERVER, false) : "";
	$subdomain = $addresses[0]->city->subdomain;
	$domain = City::getRedirectUrl($subdomain, $addresses[0]->samozapis, "");
	$viewUrl = $absoluteLink . (($domain !== MyTools::url_origin($_SERVER, false)) ? $domain : "") . Yii::app()->createUrl('klinika/'.$data->linkUrl);
	//$viewUrl = $data->getPagesUrls(true, true);
	$empLoyaltyProgram = false;
	foreach($addresses as $address) {
	    if ($address->userMedicals->agreementLoyaltyProgram == 1 && Yii::app()->params['regions'][$subdomain]['loyaltyProgram']) {
		    $empLoyaltyProgram = true;
	        break;
	    }
	}
}
?>
<div class="clinic_card">
	<a href="<?= $viewUrl ?>" class="hideInSmallCard" style="
		display: block; position: absolute;
		width: 100%; height: 100%;    
		top: 0px; left: 0px;">
	</a>
	<div class="doctor_card_content">
		<div class="doctor_card_column_sign">
			<?php if ($data->logo): ?>
				<?= CHtml::link(CHtml::image($data->logoUrl, CHtml::encode($data->name)), $viewUrl, ['class' => 'doctor_card_photo']); ?>
			<?php else: ?>
				<?= CHtml::link(CHtml::image($this->assetsImageUrl . '/icon_emp_med.jpg', 'Клиника'), $viewUrl, ['class' => 'doctor_card_photo']) ?>
			<?php endif; ?>
			<?= Html::link('Записаться на прием', $viewUrl, array('class' => 'btn-green btn-read-more hideInSmallCard')) ?>
			<?php if ($empLoyaltyProgram) : ?>
			<?= Yii::app()->params['empLoyaltyPageLink'] ?>
				<div class="empLoyaltyProgram vmargin16"></div>
			</a>
			<?php endif; ?>
		</div>
		<div class="doctor_card_column_description">
			<?= CHtml::link(CHtml::encode($data->name), $viewUrl, ['class' => 'doctor_card_name', 'style'=>"position: relative;"]); ?>
			<span style="font-family: 'Roboto'; font-size: 14px; font-weight: 500; line-height: 24px;"><?= CHtml::encode($data->companyType->name) ?></span>
			<div class="doctor_rate_block<?= (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) ? ' vhidden' : '' ?>">
				<input type="range" min="0" max="5" value="<?= $data->roundedRating ?>" step="0.1" id="backing-<?= $address->link ?>" style="display: none;">
				<span id="rate-<?= $address->link ?>" data-rateit-value="<?= $data->roundedRating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $address->link ?>"></span>
				<button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
				<span class="rating-text"><?= $data->boldedRoundedRating; ?></span>
				<a href="<?=$viewUrl?>#reviews" class="cardReviewsCount">
					<?=$data->publishedReviewsCount . " " . MyTools::createCountableForm($data->publishedReviewsCount, "отзыв", "отзыва", "отзывов");?>
				</a>
				<script>
				$(function() {
					jQuery('#rate-<?= $address->link ?>').rateit({max: 5, step: 0.1, starwidth: 23, starheight: 20, backingfld: '#backing-<?= $address->link ?>'});
				});
				</script>
			</div>
		</div>
		<div class="doctor_rank-address_block">
			<div class="address-block hideInSmallCard">
				<?php if (!empty($addresses)): ?>
				<span class="item_name" title="Количество клиник"><?= count($addresses) ?></span>
				<span class="item_name"><?= (count($addresses)>=5 && count($addresses)<=20) ? 'клиник' : (count($addresses)%10 === 1 ? 'клиника' : (count($addresses)%10 <= 4 ? 'клиники' : 'клиник')) ?>:</span>
				<a href="<?=$absoluteLink?><?= (($domain !== MyTools::url_origin($_SERVER, false)) ? $domain : "") . CHtml::normalizeUrl(array('/klinika/'.$data->linkUrl)) ?>" class="show_all_addresses" style="position: relative;"><span>Посмотреть адреса</span></a>
				
				<div class="popup_content" style="display: none;">
					<div class="clinic_address<?= (count($addresses) <= 5) ? '_short' : '' ?>_popup">
						<h3 class="clinic_address_label"><span class="icon_hosp_mini"></span>Адреса <span class="color2">«<?= CHtml::encode($data->name) ?>»</span></h3>
						<?php
						foreach ($addresses as $addr):
						$addrdomain = City::getRedirectUrl($data->placeOfWorks[0]->address->city->subdomain, $data->placeOfWorks[0]->address->samozapis, "");
						?>
						<div class="clinic_address_row">
							<div class="clinic_address_column">
								<div class="card_row">
									<span class="item_name" title="Метро">Метро:</span>
									<span class="item_value"><?= CHtml::encode(reset($addr->metroStations)->name) ?></span>
								</div>
								<div class="card_row">
									<span class="item_name" title="Адрес">Адрес:</span>
									<a href="<?=$absoluteLink?><?= $addr->getPagesUrls(true, true) ?>">
										<span><?= CHtml::encode($addr->street) ?>, <?= CHtml::encode($addr->houseNumber) ?></span>
									</a>
								</div>
							</div>
							<?= Html::link('Записаться на прием', $addr->getPagesUrls(true, true), array('class' => 'btn-green clinic_address_button')) ?>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<?php elseif ($data->address): ?>
				<div class="clinic_address_column">
					<?php if ($data->address->nearestMetroStation->name): ?>
					<div class="card_row">
						<span class="item_name" title="Метро">Метро:</span>
						<span class="item_value"><?= CHtml::encode($data->address->nearestMetroStation->name) ?></span>
					</div>
					<?php endif; ?>
					<div class="card_row">
						<span class="item_name" title="Адрес">Адрес:</span>
						<a href="<?=$absoluteLink?><?= (($domain !== MyTools::url_origin($_SERVER, false)) ? $domain : "") . $this->createUrl('/klinika/view', [ 'companyLink' => $data->linkUrl, 'linkUrl' => $data->address->linkUrl ]); ?>">
							<span><?= CHtml::encode($data->address->street) ?>, <?= CHtml::encode($data->address->houseNumber) ?></span>
						</a>
					</div>
				</div>
				<?php endif; ?>
			</div>
			
			<div class="card_row">
				<span class="item_name" title="Рабочие часы">Рабочие часы:</span>
			</div>
			<table class="workingHours">
				<tbody>
					<?php
					if(is_object($address->workingHours)):
						$weeklyHour = @$address->workingHours->getWeeklyHour();
						if(is_array(@$weeklyHour["timetable"])):
						foreach ($weeklyHour["timetable"] as $key=>$row): ?>
							<tr>
								<td><?= $key ?></td>
								<td><?= $row ?></td>
							</tr>
						<?php
						endforeach;
						endif;
					endif;
					?>
				</tbody>
			</table>
			<?= Html::link('Подробнее', $viewUrl, array('class' => 'btn-green btn-read-more small-card-submit', 'style' => 'display: none;')) ?>
			<?php if ($empLoyaltyProgram) : ?>
			<?= Yii::app()->params['empLoyaltyPageLink'] ?>
				<div class="empLoyaltyProgram hideInNormalCard" style="display: none;"></div>
			</a>
			<?php endif; ?>
		</div>
	</div>
</div>