<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Поиск по специальностям врачей - '.Yii::app()->name;

$this->breadcrumbs = array(
	'Специальности врачей'  
);
Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>



<div class="clearfix cont padding_left20px">
	<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs)); ?>
	<?php 
	$this->widget('ColumnListView', array(
		'dataProvider'	 => $dataProvider,
		'itemView'		 => '_doctorSpecialty',
		'itemsCssClass'	 => 'links',
		'pagerCssClass'	 => 'pages',
		'pager'			 => array(
			'cssFile' => false,
			'header' => 'Страницы: '
		),
		'template' => "{items}\n{pager}",
			//'columns'=>array("one","two","three","four"),
	));
	?>
</div><!-- /clearfix -->

