<?php
/* @var $data DoctorSpecialty*/
/* @var $this Controller*/
?>
<?php if($data->name && $data->link): ?>
	<?= Html::link(CHtml::encode($data->name), array('/handbook/doctorSpecialty', 'linkUrl'=>$data->linkUrl))?>
<?php endif; ?>