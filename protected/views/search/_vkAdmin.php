<div class="SECTION VK_APP">
	<?php 
	if($vkViewerType == 4): 
		foreach (Yii::app()->params['regions'] as $region) {
			$regional[$region['subdomain']] = $region['name'];
		}
		?>
		<div id="adminSet" style="display: none;margin:15px 0 0 15px;">
			<strong>Для работы приложения, укажите регион и клинику:</strong><br><br>
			<span class="item_title">Регион</span>
			<?php
				$this->widget('ESelect2', [
					'name' => 'regionVk',
					'data' => $regional,
					'value' => Yii::app()->session['selectedRegion'],
					'options' => [
						'placeholder' => 'Регион',
						'allowClear' => true,
						'minimumResultsForSearch' => -1,
					],
					'htmlOptions' => [
						'style' => '
							width: 235px;
							height: 40px;
							border-radius: 3px 3px 3px 3px;
							background: white;'
					]
				]);
			?>
			<span class="item_title">Клиника</span>
			<span id="clinicList">
			<?php
				$this->widget('ESelect2', [
					'name' => 'companyVk',
					'data' => ShortCompaniesOfDoctor::getCompanyList(),
					'value' => $vkClinic->linkUrl ? $vkClinic->linkUrl : '',
					'options' => [
						'placeholder' => 'Клиника',
						'allowClear' => true,
						'minimumResultsForSearch' => -1,
					],
					'htmlOptions' => [
						'style' => '
							width: 235px;
							height: 40px;
							border-radius: 3px 3px 3px 3px;
							background: white;'
					]
				]);
			?>
			</span>
			<span id="addressBlock" style="display: none;"><br>
				<span class="item_title">Выберите адреса</span>
				<span id="addressList"></span>
			</span>
			<span id="vkSave" style="display: none;">
				<br><button class="btn-green" style="height:30px; font-size: 14px;">Сохранить</button>
			</span>
			<span id="vkSaveOk" style="display: none;">
				<br>Изменения сохранены! Обновите страницу.
			</span>
		</div>
		<?php if($vkAppModel): ?>
			<a href="javascript:$('#adminSet').toggle();" style="position: fixed;top: 10px;left: 10px;">Изменить настройки</a>
		<?php else: ?>
			<script type="text/javascript">
				$('#adminSet').show();
			</script>
		<? endif; ?>
	<? endif; ?>

	<?php if(!count($vkAppModel) || $vkClinic->removalFlag != 0): ?>
		<style type="text/css">
			.mobilecrumbs {
				display: none !important;
			}
		</style>
		<div style="text-align: center; margin: 25px 30px 0px 30px;">
			<h2><?=$vkViewerType != 4 ? 'В данный момент запись через приложение невозможна!' : ''?></h2>
		</div>
	<? endif; ?>

</div>

<script type="text/javascript">
	$('#regionVk').on('change', function() {
		$('#addressBlock').hide();
		$('#vkSave').hide();
		$('#vkSaveOk').hide();
		$.ajax({
			url: '/ajax/vkAppClinic',
			type: "GET",
			data: { region: $(this).val() },
			success: function(data) {
				$('#clinicList').html(data);
			}
		});		
	});

	$('#companyVk').on('change', function() {
		$('#vkSave').hide();
		$('#vkSaveOk').hide();
		$.ajax({
			url: '/ajax/vkAppAddress',
			type: "GET",
			data: { company: $(this).val() },
			success: function(data) {
				$('#addressBlock').show();
				$('#addressList').html(data);
			}
		});			
	});

	if($('#companyVk').val()) {
		$('#companyVk').change();
	}

	$('#vkSave').on('click', function() {
		var checkAll = $('#addressAll').attr('checked');
		var addressIds;
		if(!checkAll) {
			addressIds = jQuery.map( $('.addressList:checked'), function(address)
		  	{
		    	return $(address).val();
		  	});
		  	addressIds = JSON.stringify(addressIds);
		 }
		$.ajax({
			url: '/ajax/vkAppSave',
			type: "GET",
			data: { 
				company: $('#companyVk').val(),
				saveAll: checkAll,
				addresses: addressIds
			},
			success: function(data) {
				$('#vkSave').hide();
				$('#vkSaveOk').show();
			}
		});			
	});
</script>