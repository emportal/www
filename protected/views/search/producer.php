<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */
/* @var $producer Producers */

$this->breadcrumbs = array(
	'Search' => array('/search'),
	'Doctor',
);
Yii::app()->clientScript->registerScript('search',
										 "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>

<div class="search-result">
	<div class="search-result-head">
		<span class="search-found">
			Найдено
			<a title="<?= $dataProvider->totalItemCount?> поставщиков" href="#">
				<?= $dataProvider->totalItemCount?> поставщиков
			</a>
		</span>
	</div>
	<table class="search-result-tab">
		<?php foreach ($dataProvider->getData() as $producer): ?>
		<tr class="search-result-box">
			<td class="search-result-signup">
				<img src="/shared/images/pic-4.png" width="240" height="81" alt="1">
				<a href="#" title="Записаться на прием" class="btn-green recomended">
					Рекомендуют (132)
					<i></i>
				</a>
				<span class="checked">Проверено<i></i></span>
			</td><!-- /search-result-signup -->
			<td class="search-result-info">
				<div class="search-result-title">
					<h2>
						<a href="#" title="<?= CHtml::encode($producer->name) ?>">
							<?= CHtml::encode($producer->name) ?>
						</a>
						<small>Поставщик шприцов</small>
					</h2>
				</div><!-- /search-result-title -->
				<p>Санкт-Петербург<br>тел: <span class="size-14">(812) 332-12-20</span></p>
			</td><!-- /search-result-info -->
			<td class="search-result-reviews">
				<img src="/shared/images/pic-5.png" width="265" height="82" alt="1">
			</td><!-- /search-result-reviews -->
		</tr>
		<? endforeach; ?>
	</table>
	<br>
	<a href="#" class="btn big">Показать еще 10 поставщиков</a>
</div>

