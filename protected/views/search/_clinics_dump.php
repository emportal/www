<?php
/* @var $dataProvider CActiveDataProvider */
/* @var $data Company */
$address = $widget->viewData['addr'][$data->id][0];
?>

<tr class="search-result-box <?= ($index % 2) ? 'odd' : '' ?>">
	<td class="search-result-signup">
		<div class="search-company-logo">
			<?php if ($data->logo): ?>
				<?= CHtml::link(CHtml::image($data->logoUrl, CHtml::encode($data->name), array('class' => 'company-logo')), array('/clinic/view','#'=>'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl)); ?>		
			<? else: ?>
				<?php #var_dump(Yii::app()->clientScript->getPackageBaseUrl('main'));?>
				<?= CHtml::link(CHtml::image($this->assetsImageUrl . '/icon_emp_med.jpg', '', array('class' => ' default')), array('/clinic/view','#'=>'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl)); ?>
			<? endif; ?>		
		</div>
		<?php /* ?>
		  <div class="rating">
		  <div class="rating-inner" style="width:80%"></div><!-- /rating-inner -->
		  <b class="rating-count">
		  <? //= $data->rating ?>
		  <span>4.</span>5
		  </b><!-- /rating-count -->
		  </div><!-- /rating -->
		  <p>Количество записей: 2
		  <br />Отзывов: 5</p>
		  <? */ ?>
		<?= Html::link('Записаться на прием', array('/clinic/view', 'companyLink' => $data->linkUrl, '#'=>'info', 'linkUrl' => $address->linkUrl), array('class' => 'btn-green')) ?>
	</td><!-- /search-result-signup -->
	<td class="search-result-info">
		<div class="search-result-title">
			<h2>			
				<?php
				echo CHtml::link(CHtml::encode($data->name), array('/clinic/view','#'=>'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl), array('class' => 'gray_link'));
				?>				
			</h2>
			<br/>
			<?php /* 						
			  $this->widget('CStarRating', [
			  'name'	=> 'rating-'.$data->link,
			  'id'	=> 'rating-'.$data->link,
			  'value'	=> $data->roundedRating,
			  'ratingStepSize' => 0.5,
			  'starCount' => 5,
			  'cssFile'=>false,
			  'readOnly'=>true,
			  'minRating' => 0.5,
			  'maxRating' => 5,
			  ]); */
			?>
			<?php #if (!in_array($data->companyTypeId, Address::$arrGMU)): ?>			
				<input type="range" min="0" max="5" value="<?= $data->roundedRating ?>" step="0.1" id="backing-<?= $address->link ?>">
				<span data-rateit-starwidth="17" data-rateit-starheight="16" id="rate-<?= $address->link ?>" data-rateit-value="<?= $data->roundedRating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $address->link ?>"></span>
				<span class="rating-text"><?= $data->boldedRoundedRating; ?></span>
				<script>
				$(function() {
					jQuery('#rate-<?= $address->link ?>').rateit({max: 5, step: 0.1, backingfld: '#backing-<?= $address->link ?>'});
				});
				</script>
			<?php #endif; ?>
			<small>								
				<?= $data->companyType->name ?><br/>
				<?php/* if ($data->appointmentCount): ?>
					Кол-во записей в клинику: <?= $data->appointmentCount; ?>
				<?php endif; */?>
			</small>
			<!--<?php if ($widget->viewData['companyContracts'][$data->id]): ?>
				<?= CHtml::link('<div>записаться</div>', array('/clinic/visit', 'link' => $address->link), array('class' => 'online-text')) ?>
			<?php endif; ?>-->

		</div><!-- /search-result-title -->
		<div>			
			<?php if (count($widget->viewData['addr']/* $data->addresses */)): ?>
				<?php foreach ($widget->viewData['addr'][$data->id] as $address): ?>
					<div style="margin-top:5px">			
						<?php if (is_array($address->metroStations) && count($address->metroStations)): ?>
							<div style="float:left;margin-right:20px;">
								<span class="metro">
									<span title="Станция метро"></span> 				
									<?= is_array($address->metroStations) ? '' . CHtml::encode(reset($address->metroStations)->name) : '' ?>			
								</span>
							</div>
						<?php endif; ?>
						<div style="float:right">
							<span class="address">
								<?= Html::link(CHtml::encode($address->street) . ', ' . CHtml::encode($address->houseNumber), array('/clinic/view','#'=>'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl)); ?>
							</span>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php
					# CHtml::link((!empty($address->nearestMetroStation) ? "м. {$address->nearestMetroStation->name}, " : '') . (!empty($address->street) ? " {$address->street}, {$address->houseNumber}" : $address->name), array('/clinic/view', 'link' => $address->link), array('title' => $address->name)) 
					?>

					<?php /*
					 * <p>
					  if (count($widget->viewData['phone'][$address->id])): ?>

					  тел: <span class="size-12">
					  <nobr>
					  <?if(count($widget->viewData['phone'][$address->id])!=1) {
					  echo implode('</nobr>, <nobr>', $widget->viewData['phone'][$address->id]);
					  } else {
					  echo $widget->viewData['phone'][$address->id][0];
					  }
					  ?>
					  </nobr>
					  </span>
					  <?php endif; */ ?>


					<?php /* ?>
					  <br>
					  пн-пт: 10:00 - 20:00, сб: 12:00 - 18:00, вс: выходной
					  <?
					  </p> */ ?>		
					<?php //var_dump($widget->viewData['services']);?>
					<?php if (count($widget->viewData['services'][$address->id])): ?>						
						<?php foreach ($widget->viewData['services'][$address->id] as $srv): ?>
							<?= CHtml::encode($srv['serviceName']) ?>, Цена: <strong><?= $srv['servicePrice']; ?></strong>
							<?= CHtml::link('<div>записаться</div>', array('/clinic/visit', 'link' => $address->link, 'AppointmentToDoctors[serviceId]' => $srv['link']), array('class' => 'online-text-service')) ?>
							<br/>
						<?php endforeach; ?>
					<?php endif; ?>
				<? endforeach; ?>
			<? elseif ($data->address): ?>
				<?= CHtml::link((!empty($data->address->nearestMetroStation) ? "м. ".CHtml::encode($data->address->nearestMetroStation->name)."," : '') . (!empty($data->address->street) ? " ".CHtml::encode($data->address->street).", ".CHtml::encode($data->address->houseNumber) : CHtml::encode($data->address->name)), array('/clinic/view', 'link' => $data->address->link), array('title' => CHtml::encode($data->address->name))) ?>
				<br>
			<? endif; ?>
		</div>


		<!--			<a href="<?php
		echo $this->createUrl('/clinic/experts', array('link' => $data->link));
		?>" title="Перечень специалистов" class="size-14">
						Перечень специалистов
					</a>-->

		<!--		<em class="price-box">
					<span class="price-box-inner">
						СТОИМОСТЬ ПЕРВИЧНОГО ОСМОТРА <b>8 000</b> р.
					</span>
				</em>-->
	</td><!-- /search-result-info -->

	

</tr>
