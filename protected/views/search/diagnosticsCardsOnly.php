			<?php
				$searchResult = $this->widget('zii.widgets.CListView', array(
					'dataProvider' => $dataProvider,
					'itemView' => '_services',
					'itemsTagName' => 'div',
					'emptyText' => $emptyText,
					'template' => "{items}",
					'beforeAjaxUpdate' => 'function(id,date) { }',
					'afterAjaxUpdate' => 'function(id,date) { }',
					'viewData' => array(
							'services' => $services,
							'addr' => $addr
					),
					'pager'=>array(
					),
				), true);
				echo preg_replace('%<div class="keys".*?</div>%s', '', $searchResult);
			?>