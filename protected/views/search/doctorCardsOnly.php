	<?php
		if ($isWidget == 1) {
			$itemView = '_doctors_for_widget';
			$searchResultHead_style = ' style="padding-left: 0; padding-top: 15px;"';
		} else {
			$itemView = '_doctors';
		}
		
		$searchResult = $this->widget('zii.widgets.CListView', array(
			'dataProvider' => $dataProvider,
			'itemView' => $itemView,
			'itemsTagName' => 'div',
			'template' => "{items}",
			'emptyText' => $emptyText,
			'afterAjaxUpdate' => 'function(id,date) { window.scrollTo(0, 0); }',
			'viewData' => array(
				'show' => $show,
				'doctors' => $doctors
			),
			'pager' => array(
				'firstPageLabel' => '<<',
				'prevPageLabel' => '< назад ',
				'nextPageLabel' => 'вперед >',
				'lastPageLabel' => '>>',
				'maxButtonCount' => 5,
				'header' => '',
				'cssFile' => false
			),
			'htmlOptions' => array(
				"class" => "search_with_pager"
			)
		), true);
		echo preg_replace('%<div class="keys".*?</div>%s', '', $searchResult);
	?>
