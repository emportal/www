<?php
$checkedAddress = [];
foreach ($vkAppModel as $value) {
	if($value->addressId) {
		$checkedAddress[] = $value->addressId; 
	}
}

if(count($vkAppModel) == 1 && !$vkAppModel[0]->addressId) {
	$checkAll = 1;
}
?>
<br><input type="checkbox" id="addressAll" value="1" class="addressIn" <?=$checkAll?'checked':''?>>Выбрать все адреса<br><br>
<?php  foreach ($addressList as $value): 
	$check = in_array($value->id, $checkedAddress) ? 1 : 0;
	?>
	<input type="checkbox" name="address[]" value="<?=$value->id;?>" <?=$check?'checked':''?> class="addressIn addressList"><?=$value->name;?><br>
<?php  endforeach; ?>

<script type="text/javascript">
	$('#addressAll').on('change', function() {
		$('#vkSaveOk').hide();
		if($(this).attr('checked')) {
			$('.addressList').attr('disabled', true);
			$('.addressList').attr('checked', 'checked');
		}
		else {
			$('.addressList').attr('disabled', false);
			$('.addressList').attr('checked', false);
		}
	});

	$('.addressIn').on('change', function() {
		$('#vkSaveOk').hide();
		if($('.addressIn:checked').length > 0) {
			$('#vkSave').show();
		}
		else {
			$('#vkSave').hide();
		}
	});

	if($('#addressAll').attr('checked')) {
		$('#addressAll').change();
	}
</script>