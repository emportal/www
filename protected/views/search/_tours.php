<?php
/* @var $dataProvider CActiveDataProvider */
/* @var $data Company */
?>
<tr class="search-result-box <?= ($index % 2) ? 'odd' : '' ?>">
	<td class="search-result-signup">

		<?php if ( $data->logo ): ?>
			<?= CHtml::image($data->logoUrl, CHtml::encode($data->name), array('class' => 'company-logo')) ?>
		<? else: ?>
			<?= CHtml::image($this->assetsImageUrl.'/icon_emp_tourism.jpg', '', array('class' => 'company-logo default')) ?>
		<? endif; ?>
		<?php /*?>
		<div class="rating">
				<div class="rating-inner" style="width:80%"></div><!-- /rating-inner -->
				<b class="rating-count">
					<? //= $data->rating ?>
					<span>4.</span>5
				</b><!-- /rating-count -->
		</div><!-- /rating -->
		<p>Количество записей: 2
		<br />Отзывов: 5</p>
		<?*/?>
	</td><!-- /search-result-signup -->
	<td class="search-result-info">
		<div class="search-result-title">
			<h2>
				<?= CHtml::encode($data->name) ?>
				<small>
					<?= CHtml::encode($data->companyType->name) ?>
				</small>
			</h2>
			<?php /*?>
			<div class="rating">
				<div class="rating-inner" style="width:80%"></div><!-- /rating-inner -->
				
				<b class="rating-count">
					<? //= $data->rating ?>
					<span>4.</span>5
				</b><!-- /rating-count -->
			</div><!-- /rating -->
			<?*/?>
		</div><!-- /search-result-title -->
		<div>
			<?php if ( count($widget->viewData['addr'][$data->id]) ): ?>			
				<?php foreach ($widget->viewData['addr'][$data->id] as $address): ?>
					<?= CHtml::link((!empty($address->nearestMetroStation) ? "м. ".CHtml::encode($address->nearestMetroStation->name).", " : '') . (!empty($address->street) ? CHtml::encode($address->street).", ".CHtml::encode($address->houseNumber) : CHtml::encode($address->name)), array('/insurance/view', 'link' => $address->link), array('title' => CHtml::encode($address->name))) ?>
					<p>
						<?php /*if(isset($address->phones[0])):?>тел: <span class="size-14"><?=$address->phones[0]->name?></span><?php endif;*/?>
						<?php /*?>
						<br>
						пн-пт: 10:00 - 20:00, сб: 12:00 - 18:00, вс: выходной
						<?*/?>
					</p>
				<? endforeach; ?>
			<? elseif($data->address): ?>
				<?= CHtml::link( (!empty($data->address->nearestMetroStation) ? "м. ".CHtml::encode($data->address->nearestMetroStation->name).", " : '') . (!empty($data->address->street) ? CHtml::encode($data->address->street).", ".CHtml::encode($data->address->houseNumber) : CHtml::encode($data->address->name)), array('/tourism/view', 'link' => $data->address->link), array('title' => CHtml::encode($data->address->name))) ?>
				<br>
			<? endif; ?>
		</div>
		<p>

			<!--			<a href="<?php
			echo $this->createUrl('/clinic/experts', array('link' => $data->link));
			?>" title="Перечень специалистов" class="size-14">
							Перечень специалистов
						</a>-->
		</p>
<!--		<em class="price-box">
			<span class="price-box-inner">
				СТОИМОСТЬ ПЕРВИЧНОГО ОСМОТРА <b>8 000</b> р.
			</span>
		</em>-->
	</td><!-- /search-result-info -->

	<td class="search-result-reviews">
	<?php /*<img width="265" height="82" alt="1" src="/html/images/pic-5.png">
		 if ( $data->comments ): ?>
		  <?php
		  $num	 = rand(0, count($data->comments));
		  $comment = $data->comments[$num];
		  ?>
		  <div class="reviews-box">
		  <i class="reviews-box-arrow"></i>
		  <blockquote><?= $comment->content ?></blockquote>
		  <p class="reviews-amount">
		  отзывов
		  <a href="<?php
		  echo
		  $this->createUrl('/clinic/comments', array('link' => $data->link));
		  ?>" title="<?= $comment->commentId ?>" class="ico-comments">
		  <?= $comment->commentId ?>
		  </a>
		  </p>
		  </div><!-- /reviews-box -->
		  <? endif; */ ?>
	</td><!-- /search-result-reviews -->

</tr>
