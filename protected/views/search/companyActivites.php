<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = "Медицинские центры Санкт-Петербурга – Запись в клинику – "
		. "Поиск больниц и запись в частные платные клиники – единая база медицинских центров";
Yii::app()->clientScript->registerMetaTag(Yii::app()->name." – это полный каталог медицинских центров и врачебных организаций."
		. " С помощью нашего портала вы найдете клинику нужного профиля. У нас вы также найдете, адреса, контакты и отзывы."
		. " Все это позволит вам записаться в медицинский центр",'description');
		
$this->breadcrumbs = array(
	'Профили клиник' 
);
Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>

	

<div class="clearfix cont padding_left20px">
	<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs)); ?>
	<H1 class="h2style">Медицинские центры</H1><br/>
	<?php
	$this->widget('ColumnListView', array(
		'dataProvider'	 => $dataProvider,
		'itemView'		 => '_companyActivites',
		'itemsCssClass'	 => 'links',
		'pagerCssClass'	 => 'pages',
		'pager'			 => array(
			'cssFile' => false,
			'header' => 'Страницы: '
		),
		'template' => "{items}\n{pager}",
			//'columns'=>array("one","two","three","four"),
	));
	?>
</div><!-- /clearfix -->

