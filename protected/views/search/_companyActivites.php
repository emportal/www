<?php
/* @var $data CompanyActivites*/
/* @var $this Controller*/
?>
<?php if($data->name && $data->linkUrl): ?>
	<?= Html::link(CHtml::encode($data->name), array('/handbook/companyActivites', 'linkUrl'=>$data->linkUrl))?>
<?php endif; ?>