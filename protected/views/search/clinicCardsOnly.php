<?php
	$searchResult = $this->widget('zii.widgets.CListView', array(
		'dataProvider'	 => $dataProvider,
		'itemView'		 => '_clinics',
		'itemsTagName'   => 'div',
		'emptyText'		 => $emptyText,
		'template'		 => '{items}',
		'viewData'		 => array('companyContracts'=>$companyContracts, 'addr'=>$addr),
	), true);
	echo preg_replace('%<div class="keys".*?</div>%s', '', $searchResult);
?>