<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Search' => array('/search'),
	'Doctor',
);
Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>

<div class="clearfix cont">
	<?php
	$this->widget('ColumnListView', array(
		'dataProvider'	 => $dataProvider,
		'itemView'		 => '_medicament',
		'itemsCssClass'	 => 'links',
		'pagerCssClass'	 => 'pages',
		'pager'			 => array(
			'cssFile' => false,
			'header' => 'Страницы: '
		),
		'template' => "{items}\n{pager}",
			//'columns'=>array("one","two","three","four"),
	));
	?>
</div><!-- /clearfix -->

