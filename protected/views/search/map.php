<?php
/* @var $this SearchController */

$this->breadcrumbs=array(
	'Search',
);
/*
Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', CClientScript::POS_END);
Yii::app()->clientScript->registerCoreScript('yandex');
Yii::app()->clientScript->registerScript('t','$(document).ready(function() {window.scrollTo(0,450) });');
 */
?>

<script type="text/javascript" src="/shared/js/yandex_map_custom.js"></script>


<data hidden data-search='<?=$search?>'></data>
<data hidden data-coord='<?=$coord?>'></data>
<data hidden data-map='<?=$data?>'></data>
<!--<data hidden data-map='[{"name":"СМ-Клиника","lat":59.949226,"long":30.465778,"url":"smclinic-spb","urlFilial":"","doctors":"54"},{"name":"БалтЗдрав на Дыбенко","lat":59.904118,"long":30.476847,"url":"baltzdrav","urlFilial":"dibenko","doctors":"50"},{"name":"Многопрофильная клиника РАМИ","lat":59.94368,"long":30.355017,"url":"rami-spb","urlFilial":"mnogoprofilnaya_kirochnaya","doctors":"42"},{"name":"Первая Невская Клиника","lat":60.030075,"long":30.33078,"url":"1-nc","urlFilial":"","doctors":"27"},{"name":"Первая сeмейная клиника Петербурга на Австрийской площади","lat":59.959301,"long":30.317404,"url":"1-clinic","urlFilial":"mnogoprofilnaya_kamennoostrovskiy","doctors":"15"},{"name":"Клиника UMC - ЮнайтедМедКлиник","lat":59.984192,"long":30.26055,"url":"umedclinic","urlFilial":"","doctors":"6"},{"name":"Мэлиора","lat":60.017689,"long":30.238348,"url":"meliora","urlFilial":"","doctors":"0"},{"name":"Профмед +","lat":59.917278,"long":30.395363,"url":"profmedplys","urlFilial":"","doctors":"0"},{"name":"БалтМед гавань (на Васильевском)","lat":59.938663,"long":30.223791,"url":"baltmed-gavan-na-vasilevskom","urlFilial":"","doctors":"0"},{"name":"Инклиник","lat":59.940838,"long":30.276485,"url":"inklinik","urlFilial":"","doctors":"0"},{"name":"Медицинский центр инновационных технологий","lat":59.941879,"long":30.272497,"url":"medicinskii-centr-innovacionnyh-tehnologii","urlFilial":"","doctors":"0"},{"name":"Риат","lat":59.942768,"long":30.269272,"url":"eleosmc","urlFilial":"","doctors":"0"},{"name":"Центр профилактической медицины \"НикаМед\"","lat":59.938961,"long":30.276163,"url":"nikamednet","urlFilial":"","doctors":"0"},{"name":"Детский ЕвроМед","lat":60.00523,"long":30.327995,"url":"deti-euromed","urlFilial":"","doctors":"0"},{"name":"ГЛОБУС","lat":60.028587,"long":30.401091,"url":"globusmed","urlFilial":"","doctors":"0"},{"name":"Аллергомед","lat":59.889774,"long":30.318752,"url":"allergomed","urlFilial":"","doctors":"0"},{"name":"Элеос","lat":59.90284,"long":30.484814,"url":"eleos","urlFilial":"","doctors":"0"},{"name":"Медицинский центр Элеос","lat":59.993835,"long":30.309757,"url":"medicinskii-centr-eleos","urlFilial":"","doctors":"0"},{"name":"Центр аллергологии и иммунологии доктора Поповича","lat":60.016563,"long":30.309454,"url":"allergy","urlFilial":"","doctors":"0"},{"name":"Консультативно-диагностический центр для детей","lat":59.835106,"long":30.42478,"url":"kdcd","urlFilial":"","doctors":"0"},{"name":"Детская больница \u211619 им. К.А. Раухфуса","lat":59.93742,"long":30.36524,"url":"gdb19","urlFilial":"","doctors":"0"},{"name":"Аллергоцентр ПЛЮС","lat":59.9492,"long":30.367001,"url":"allergocenterplus","urlFilial":"","doctors":"0"},{"name":"Городская стоматологическая поликлиника \u21161","lat":59.934933,"long":30.333797,"url":"gsp-1","urlFilial":"","doctors":"0"},{"name":"Кожно-венерологический диспансер \u211611, Отделение \u21162","lat":59.937546,"long":30.391487,"url":"kvd11","urlFilial":"moiseenko","doctors":"0"}]' data-url='/clinics/allergologija'></data>-->
<div id="map" class="clinic-map" style="height: 500px; min-width:600px"> </div>
<script>
//$(document).ready(function() { setTimeout(function() {window.scrollTo(0,150);},100);  });
</script>