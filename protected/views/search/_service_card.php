<?php
$domain = City::getRedirectUrl($data->address->city->subdomain, $data->address->samozapis, "");
$viewUrl = (($domain !== MyTools::url_origin($_SERVER, false)) ? $domain : "") . Yii::app()->createUrl('klinika/'.$data->address->company->linkUrl.'/'.$data->address->linkUrl);
$empLoyaltyProgram = false;
if(Yii::app()->params['regions'][$data->address->city->subdomain]['loyaltyProgram']) {
	$empLoyaltyProgram = ($data->address->userMedicals->agreementLoyaltyProgram) ? true : false;
}
$isVkApp = Yii::app()->request->getParam('vk');
?>
<div class="service_card">
	<div class="service_card_content">
		<div class="service_card_column_name">
            <p class="doctor_card_name service_title">
                <?/* = Html::link(CHtml::encode($data->service->name), 'klinika/' . $data->address->company->linkUrl . '/' . $data->address->linkUrl,  ['class' => 'doctor_card_name cursordefault service_title', 'onclick' => 'return false;'])  */?>
                <?= $data->service->name ?>
            </p>
			<?php if ($empLoyaltyProgram && !$isVkApp) : ?>
			<?= Yii::app()->params['empLoyaltyPageLink'] ?>
				<div class="empLoyaltyProgram serviceCard"></div>
			</a>
			<?php endif; ?>
		</div>
		<div class="service_card_column_address">
			<div class="card_row service_card_row_metro"><?php if($data->address->nearestMetroStation->name): ?><img src="/images/icon-metro-micro.png" class="metro_icon"></img><?php endif; ?><span class="metro_name"><?= $data->address->nearestMetroStation->name ?></span></div>
			<div class="card_row">
				<?php if($isVkApp): ?>
					<?= CHtml::encode($data->address->company->name) ?>
				<?php else: ?>
					<?= Html::link(CHtml::encode($data->address->company->name), $viewUrl, ['class' => '']) ?>
				<?php endif; ?>
			</div>
			<div class="card_row">
				<?php if($isVkApp): ?>
					<?= CHtml::encode($data->address->street).(!empty($data->address->houseNumber) ? ", ".CHtml::encode($data->address->houseNumber) : "")?>
				<?php else: ?>
					<?= Html::link(CHtml::encode($data->address->street).(!empty($data->address->houseNumber) ? ", ".CHtml::encode($data->address->houseNumber) : ""), $viewUrl, ['class' => '']) ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="service_card_column_price<?= ($data->oldPrice != null && !$isVkApp) ? " stockSale" : "" ?>">
			<?php if($data->oldPrice != null && !$isVkApp): ?>
				<span class="onlyEMP">Только на ЕМП</span>
			<?php endif; ?>
			<div class="price_block">
				<?php if(intval($data->price) > 0): ?>
					<span class="price_value"><?= $data->price ?> </span><span class="price_valuta">руб</span>
				<?php else: ?>
					<span class="price_value">бесплатно</span>
				<?php endif; ?>
			</div>
			<?php if($data->oldPrice != null && !$isVkApp): ?>
				<span class="instead">Вместо <?= $data->oldPrice ?> РУБ</span>
			<?php endif; ?>
		</div>
		<div class="service_card_column_sign">
			<a href="#" class="service_link" data-service-id="<?= $data->id ?>">
				<button class="btn-green sign_btn" title="Записаться на приём">
					Записаться на услугу
				</button>
			</a>
			<!--# block name="search_service_popup" -->
			<?php
			$this->renderPartial('//search/_service_popup',[
				'addressServiceId'    => $data->id, 
				'addressServicePrice' => $data->price,
				'description'         => $data->description,
				'serviceName'         => $data->service->name,
				'companyLink'         => $data->address->company->link,
				'companyName'         => $data->address->company->name,
				'addressName'         => $data->address->shortName,
				'patientName'         => Yii::app()->user->model->name,
				'patientPhone'        => Yii::app()->user->model->telefon,
			]);
			?>
			<!--# endblock -->
			<!--# include virtual="/index.php?r=ajax/renderPartial&name=//search/_service_popup&params[addressServiceId]=<?=$data->id?>&params[addressServicePrice]=<?=$data->price?>&params[description]=<?=$data->description?>&params[serviceName]=<?=$data->service->name?>&params[companyLink]=<?=$data->address->company->link?>&params[companyName]=<?=$data->address->company->name?>&params[addressName]=<?=$data->address->shortName?>&params[patientName]=<?=Yii::app()->user->model->name?>&params[patientPhone]=<?=Yii::app()->user->model->telefon?>" stub="search_service_popup" -->
		</div>
	</div>
	<?php if(!empty($data->description) && !$isVkApp): ?>
	<div style="max-width: 750px;" class="serviceDescriptionBlock">
		<span class="serviceDescriptionWrap" style="white-space: nowrap;max-width: 85%;">
			<?= $data->description ?>
		</span>
		<span class="moreServiceDescription" style="float:right;visibility:hidden;">
			<a onload="alert(10);" href="javascript:void(0)" onclick="$(this).closest('div').find('.serviceDescriptionWrap').css('white-space','').css('width',''); $(this).remove();">Подробнее</a>
		</span>
	</div>
	<style type="text/css">
		@media (max-width: 1000px) {
			.serviceDescriptionBlock {
				display: none;
			}
		}
	</style>
	<?php endif; ?>
</div>