<div class="search-block<?= $big ? ' big' : '' ?>"<?= $style ? ' style="' . $style . '"' : '' ?>>
	<div class="f-left left-side">
		<?php if ($data->logo): ?>
			<?= CHtml::link(CHtml::image($data->logoUrl, CHtml::encode($data->name), array('class' => 'company-logo')), array('/clinic/view', '#' => 'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl),['class' => 'photo-search']); ?>		
		<? else: ?>
			<?php #var_dump(Yii::app()->clientScript->getPackageBaseUrl('main'));?>
			<?= CHtml::link(CHtml::image($this->assetsImageUrl . '/icon_emp_med.jpg', '', array('class' => ' default')), array('/clinic/view', '#' => 'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl),['class' => 'photo-search']); ?>
		<? endif; ?>			
		<?= Html::link('Записаться на прием', array('/clinic/view', 'companyLink' => $data->linkUrl, '#' => 'info', 'linkUrl' => $address->linkUrl), array('class' => 'btn-green')) ?>
	</div>
	<div class="f-left right-side">			
		<h5>
			<?= CHtml::link(CHtml::encode($data->name), array('/clinic/view', '#' => 'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl)); ?>				
		</h5>			
		<input type="range" min="0" max="5" value="<?= $data->roundedRating ?>" step="0.1" id="backing-<?= $address->link ?>">
		<span data-rateit-starwidth="17" data-rateit-starheight="16" id="rate-<?= $address->link ?>" data-rateit-value="<?= $data->roundedRating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $address->link ?>"></span>
		<span class="rating-text"><?= $data->boldedRoundedRating; ?></span>
		<script>
		$(function() {
			jQuery('#rate-<?= $address->link ?>').rateit({max: 5, step: 0.1, backingfld: '#backing-<?= $address->link ?>'});
		});
		</script>
		<span class="companyType">	
			<?= CHtml::encode($data->companyType->name) ?><br/>
			<?php /* if ($data->appointmentCount): ?>
			  Кол-во записей в клинику: <?= $data->appointmentCount; ?>
			  <?php endif; */ ?>
		</span>
		<?php if (!empty($addresses)): ?>
				<?php foreach ($addresses as $address): ?>
					<div style="margin-top:5px">			
						<?php if (is_array($address->metroStations) && count($address->metroStations)): ?>
							<div style="float:left;margin-right:20px;">
								<span class="metro">
									<span title="Станция метро"></span> 				
									<?= is_array($address->metroStations) ? '' . CHtml::encode(reset($address->metroStations)->name) : '' ?>			
								</span>
							</div>
						<?php endif; ?>
						<div style="float:right">
							<span class="address">
								<?= Html::link(CHtml::encode($address->street) . ', ' . CHtml::encode($address->houseNumber), array('/clinic/view', '#' => 'info', 'companyLink' => $data->linkUrl, 'linkUrl' => $address->linkUrl)); ?>
							</span>
						</div>
						<div class="clearfix"></div>
					</div>
									
				<?php endforeach; ?>
			<?php elseif ($data->address): ?>
				<div class="address-block">							
					<div class="street">
						<a class="address show_on_map" href="<?= $this->createUrl('clinic/map', ['companyLink' => $data->placeOfWorks[0]->address->company->linkUrl, 'linkUrl' => $data->placeOfWorks[0]->address->linkUrl]); ?>">
							<span></span><?= CHtml::encode($data->placeOfWorks[0]->address->street) ?>, <?= CHtml::encode($data->placeOfWorks[0]->address->houseNumber) ?>
						</a>
					</div>
					<?php if ($data->placeOfWorks[0]->address->nearestMetroStation->name): ?>								
						<div class="metro">
							<span></span><?= CHtml::encode($data->placeOfWorks[0]->address->nearestMetroStation->name) ?>
						</div>
					<?php endif; ?>

					<div class="clearfix"></div>
				</div>				
			<?php endif; ?>		
	</div>

</div>	


