<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */

$seoText = '';
$seoTextTitle = '';

if (empty($district_metro)) {
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['title']))
		$pageTagTitle = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['title'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['h1']))
		$pageTagH1 = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['h1'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['description']))
		$pageTagDescription = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['description'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoText']))
		$seoText = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoText'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoTextTitle']))
		$seoTextTitle = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoTextTitle'];
} else {
	foreach ($district_metro as $value) {
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['title']))
			$pageTagTitle = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['title'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['h1']))
			$pageTagH1 = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['h1'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['description']))
			$pageTagDescription = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['description'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoText']))
			$seoText = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoText'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoTextTitle']))
			$seoTextTitle = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoTextTitle'];
	}
}

$this->seotype = 'hideseo';
$this->pageTitle = $pageTagTitle;
Yii::app()->clientScript->registerMetaTag($pageTagTitle, 'titleText');
Yii::app()->clientScript->registerMetaTag($pageTagDescription, 'description');

$arr['Search']=Yii::app()->request->getParam('Search');
if(empty($arr['Search'])) {
	$arr = [];
}
?>
<?php
$this->breadcrumbs = array(
	'Клиники',
);
if(!empty($spModel)) {
	$this->breadcrumbs = array(
		'Клиники' => '/klinika',
		$spModel->name
	);
	if(!empty($district_metro)) {
		$this->breadcrumbs = array(
			'Клиники' => '/klinika',
			$spModel->name => '/klinika/profile-'.$spModel->linkUrl,
			$templateRules['%metroDistrict%'],
		);
	}
} elseif(!empty($district_metro)) {
	$this->breadcrumbs = array(
		'Клиники' => '/klinika',
		$templateRules['%metroDistrict%'],
	);
}
?>
<div class="SECTION crumbs_section">
	<div class="main_column">
		<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs, 'htmlOptions' => [ 'class' => 'crumbs' ] )); ?>
	</div>
</div>
<div class="SECTION SEARCH_FILTERS onload_visibility" id="search_filters_block">
	<div class="main_column">
		<div style="display: none;" class="b-top" id="scroller">
			<span class="b-top-but">наверх</span>
		</div>
		<div class="search-clinic-filter ajax_filter">
			<span class="item_title">Название клиники</span>
			<?php
			echo CHtml::textField(CHtml::activeName($model, 'text'), Yii::app()->request->getParam('Search')['text'], array(
				'id' => 'search-doctor-form_text',
				'class' => 'custom-text w135',
				'autocomplete' => 'off',
				'placeholder' => 'Название клиники',
				'value' => ''
			));
			?>	
		</div>
		<div class="search-clinic-filter ajax_filter" style="<?=(Yii::app()->params["samozapis"]) ? "display: none;" : ""?>">
			<span class="item_title">Тип медучреждения</span>
			<?php 
			$this->widget('ESelect2',[
				'name' => CHtml::activeName($model, 'companyTypeId'),
				'value' => Yii::app()->request->getParam('Search')['companyTypeId'],
				'data' => $CompanyTypesList,	
				'options' => [
					'placeholder' => 'Не имеет значения',
					'allowClear' => true,
					'minimumResultsForSearch' => -1,				
				],
				'htmlOptions' => [
					'style' => '
						width: 242px;
						height: 40px;
						vertical-align: top;
						border-radius: 4px 0px 0px 4px;'
				]
			]);
			?>
		</div>
		<?php 
		$metro = Yii::app()->request->getParam('Search')['metroStationId'] ? Yii::app()->request->getParam('Search')['metroStationId'] : '' ;
		Yii::app()->clientScript->registerScript('select22', '
			$(document).ready(function() {		
				$("#Doctor_sexId").select2("val", ["'.Yii::app()->request->getParam('Search')['sexId'].'"]); 
				$("#Doctor_price").select2("val", ["'.Yii::app()->request->getParam('Search')['price'].'"]);
			});	
		');
		?>
		<?php if(!Yii::app()->params['samozapis'] && Yii::app()->params['regions'][City::model()->selectedCity->subdomain]['loyaltyProgram']): ?>
			<div class="search-doctor-filter ajax_filter" style="height: 30px; padding-top: 15px; padding-right: 15px; vertical-align: bottom;">
				<span class="niceCheck <?= $searchModel->loyaltyProgram ? " niceChecked" : "" ?>" id="search-form_loyaltyProgram" <?= $searchModel->loyaltyProgram ? " checked" : "" ?>></span>
				<label class="" for="search-form_loyaltyProgram">оплата бонусами ЕМП</label>
	            <a href="/loyalty.html">подробнее</a>
			</div>
            <div class="search-doctor-filter ajax_filter" style="height: 30px; padding-top: 15px; padding-right: 15px; vertical-align: bottom;">
                <span class="niceCheck <?= $searchModel->offline ? "" : " niceChecked" ?>" id="search-form_offline_not" <?= $searchModel->offline ? "" : " checked" ?>></span>
                <label class="" for="search-form_offline_not">Онлайн-запись</label>
            </div>
		<?php endif; ?>
		<div style="display: none" class="show-in-responsive">
			<div style="width:240px;display: table;margin: 0 auto;">
				<button class="btn-green" style="float: left;padding: 10px;width: 45%;background: transparent;border: 1px solid #999;color: #999 !important;" onclick="resetFilters();lastPageClose();">Сбросить</button>
				<button class="btn-green" style="float:right;padding: 10px;width: 45%;background: rgb(141,198,63);border: 1px solid rgb(141,198,63);" onclick="lastPageClose();refreshSearch(0);">Применить</button>
			</div>
		</div>
	</div>
</div>
	<div class="right" style="display: none;">
		<?php if($bestCompany): ?>
			<a href="<?=$this->createUrl('clinic/view',['link' => $bestCompany->addresses[0]->link]) ?>">
				<div class="bestCompany align-center">
					<span class="strong">Лучшее предложение в районе </span>
					<div class="logo-img"><?= CHtml::image($bestCompany->logoUrl); ?></div>
					<div class="name"><?= CHtml::encode($bestCompany->name) ?></div>
					<div class="metro"><span></span><?= CHtml::encode($bestCompany->addresses[0]->metroStations[0]->name) ?></div>
					<div class="stats">
						<div class="row"><div>Врачей:</div> <b><?= $bestCompanyData['doctors'] ?></b></div>
						<div class="row"><div>Отзывов:</div> <b><?= $bestCompanyData['reviews'] ?></b></div>
							<div class="row"><div>Записей:</div> <b><?= $bestCompanyData['appointments'] ?></b></div>
					</div>
				</div>
			</a>
		<?php else: ?>
			<div class="appointment-tip">
				<span class="strong align-center">Запись в 3 действия!</span>	
				<ol>
					<li>Найдите врача</li>
					<li>Запишетесь на удобное вам время</li>
					<li>Ожидайте звонка из клиники</li>
				</ol>
			</div>
		<?php endif; ?>
	</div>
<div id="search_map" style="display: none;"></div>
<div class="SECTION SEARCH_RESULT">
	<div class="main_column">
		<div class="search-result-head"<?=$searchResultHead_style?>>
			<h1 class="search-found">
			<?php
				echo MyTools::createFromTemplate($pageTagH1, $templateRules);
			?>
			</h1>
			<a class="show_on_map found" href="<?=$_SERVER['REQUEST_URI']?>">Показать найденные клиники на карте</a>
			<a href="javascript:void(0)" class="show_on_map close" style="display: none;">Скрыть карту</a>
			<a href="javascript:void(0)" class="show-in-responsive" style="display: none;float:right;color:gray;text-decoration: initial;width: 95px;background: url(/images/icons/arrow-right.png) no-repeat right transparent;" onclick="nextPageOpenContent('.main_content #search_filters_block')">ФИЛЬТРЫ</a>
		</div>
		<div class="search-result">
		<?= MyTools::createFromTemplate("<span class='search-found-count' style='display:none;'>%totalItemCount%</span>", $templateRules); ?>
		<?php if(empty($dataProvider)):?>
			<?php echo CHtml::errorSummary($searchModel); ?>
		<?php else:?>
			<?php
                
                if ($notificationText)
                    echo '<div class="flash-error">' . $notificationText . '</div>';
                
				$searchResult = $this->widget('zii.widgets.CListView',
					array(
					'dataProvider'	 => $dataProvider,
					'itemView'		 => '_clinics',
					'itemsTagName'   => 'div',
					'emptyText'		 => ( (empty($notificationText)) ? ("<div class='flash-notice'>" . $emptyText . "</div>") : "" ),
					'template'		 => '{items}',								   
					'viewData'		 => array('companyContracts'=>$companyContracts, 'addr'=>$addr),
					'beforeAjaxUpdate' => 'function(id,date) { }',
					'afterAjaxUpdate' => 'function(id,date) { }',
					'pager'=>array(
					),
					'htmlOptions'=>array(
						"class" => "search_with_pager"
					)
				), true);
				echo preg_replace('%<div class="keys".*?</div>%s', '', $searchResult);
			?>
		<?php endif;?>
		</div>
		<div class="search_result_footer">
			<button id="search_result_append_button" class="show_more_btn btn-blue" title="Показать больше клиник" href="#show_more" onclick="appendSearch(this);">Показать больше клиник</button>
			<div id="search_result_loading" class="icon_loading" style="display: none;"></div>
			<input id="search_result_page_number" type="hidden" value="1">
		</div>
		<div class="definition">
			<?php $description = ((is_object($spModel)) ? $spModel->getParameter('description', City::model()->selectedCityId) : NULL); ?>
			<div class="decription_action_block" id="specialtyDescription">
				<?php if(!empty($description)): ?>
				<div class="decription_action_block_content">
					<h3 class="decription_action_block_header"><?= $spModel->name ?></h3>
					<p> <?= $description ?></p>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php
$seo2NoIndex = ($seoText) ? false : true;
$seoTextArr = [];
foreach(explode ("\n", $seoText) as $key => $value) {
	if(!empty(trim($value))) {
		if(count($seoTextArr) < 3) {
			$seoTextArr[] = $value;
		}
		else {
			$seoTextArr[2] .= "<br/>".$value;
		}
	}
}
?>
<div class="SECTION SEO2" style="display: <?= (empty($seoText)) ? "none" : "block" ?>;">
<?php if (!$seo2NoIndex): ?>
	<div class="main_column">
		<div class="seo_header_section">
			<p class="h1 color2 align-center">
				<?= $seoTextTitle ?>
			</p>
		</div>
		<div class="seo_block">
			<div class="seo_block_header">
				<div class="icon_drug"></div>
			</div>
			<div class="seo_block_content">
				<p><?= (isset($seoTextArr[0])) ? $seoTextArr[0] : "" ?></p>
			</div>
		</div>
		<div class="seo_block">
			<div class="seo_block_header">
				<div class="icon_ambulance"></div>
			</div>
			<div class="seo_block_content">
				<p><?= (isset($seoTextArr[1])) ? $seoTextArr[1] : "" ?></p>
			</div>
		</div>
		<div class="seo_block">
			<div class="seo_block_header">
				<div class="icon_ukol"></div>
			</div>
			<div class="seo_block_content">
				<p><?= (isset($seoTextArr[2])) ? $seoTextArr[2] : "" ?></p>
			</div>
		</div>
	</div>
<?php endif; ?>
</div>
