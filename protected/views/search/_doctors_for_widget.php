<?php
/* @var $data Doctor */
/* @var $this SearchController */
/*
 * if $big == 1 
 * 		> данные по врачу лежат в $data и в $doctors[$data->id] - инкапсулируем в переменные - $model и $info соотвественно
 * else
 * 		> все лежит в $doc (view blocked)
 * 		>
 */
?>
<?php if ($_REQUEST['photo'] && !$data->photo): ?>

<?php else: ?>

	<tr class="search-result-box <?= ($index % 2) ? 'odd' : '' ?>">
		<td class="search-result-info">
			<?php 
				$this->renderPartial('//search/_doctor_unique_for_widget',[
					'data' => $data,
					'groupedData' => $doctors,
					'big' => 1,					
				]);
			?>					
		</td>	
	</tr>
<?php endif; ?>