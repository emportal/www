<?php
/* @var $this SearchController */
$seoText = '';
$seoTextTitle = '';

if (empty($district_metro)) {
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['title']))
		$pageTagTitle = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['title'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['h1']))
		$pageTagH1 = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['h1'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['description']))
		$pageTagDescription = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['description'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoText']))
		$seoText = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoText'];
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoTextTitle']))
		$seoTextTitle = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoTextTitle'];
} else {
	foreach ($district_metro as $value) {
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['title']))
			$pageTagTitle = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['title'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['h1']))
			$pageTagH1 = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['h1'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['description']))
			$pageTagDescription = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['description'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoText']))
			$seoText = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoText'];
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoTextTitle']))
			$seoTextTitle = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoTextTitle'];
	}
}
$this->seotype = 'hideseo';
$this->pageTitle = $pageTagTitle;
Yii::app()->clientScript->registerMetaTag($pageTagTitle, 'titleText');
Yii::app()->clientScript->registerMetaTag($pageTagDescription, 'description');

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function() {
		$('#doctors-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
");

Yii::app()->clientScript->registerMetaTag($searchModel->doctorSpecialtyId, 'doctorSpecialtyId');
$isVkApp = Yii::app()->request->getParam('vk');
?>

<?php
$this->breadcrumbs = array(
	'Услуги',
);
if(!empty($spModel)) {
	$this->breadcrumbs = array(
		'Услуги' => '/diagnostics',
		$spModel->name
	);
	if(!empty($district_metro)) {
		$this->breadcrumbs = array(
			'Услуги' => '/diagnostics',
			$spModel->name => '/diagnostics/service-'.$spModel->linkUrl,
			$templateRules['%metroDistrict%'],
		);
	}
} elseif(!empty($district_metro)) {
	$this->breadcrumbs = array(
		'Услуги' => '/diagnostics',
		$templateRules['%metroDistrict%'],
	);
}
?>
<div class="SECTION crumbs_section">
	<div class="main_column">
	<?php 
	if(!$isVkApp): ?>
		<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs, 'htmlOptions' => [ 'class' => 'crumbs' ] )); ?>
	<?php endif; ?>
	</div>
</div>
<div class="SECTION SEARCH_FILTERS" id="search_filters_block">
	<div class="main_column">
		<div style="display: none;" class="b-top" id="scroller">
			<span class="b-top-but">наверх</span>
		</div>
		<?php if(!$isVkApp): ?>
			<div class="search-doctor-filter ajax_filter" style="<?=$isVkApp ? 'display: none;': '';?>">
				<span class="item_title">Клиника</span>
				<?php
					$this->widget('ESelect2', [
						'name' => CHtml::activeName($model, 'clinicLinkUrl'),
						'data' => $companyList,
						'value' => $searchModel->clinicLinkUrl,
						'options' => [
							'placeholder' => 'Любая клиника',
							'allowClear' => true,
							'minimumResultsForSearch' => -1,
						],
						'htmlOptions' => [
									'style' => '
										width: 235px;
										height: 40px;
										border-radius: 3px 3px 3px 3px;
										background: white;'
								]
					]);
				?>
			</div>
		<?php endif; ?>
		<?php if(!Yii::app()->params["samozapis"]): ?>
		<div class="search-doctor-filter ajax_filter">
			<span class="item_title">Стоимость обращения (рубли)</span>
			<?php $this->widget('PriceSlider', array('id' => 'doctor_price_slider', 'type' => 'diagnostics', 'formId' => 'search-doctor-form', 'name' => 'Search', 'inputMin' => 'priceMin', 'inputMax' => 'priceMax')); ?>	
		</div>
		<?php endif; ?>
		<?php
			$metro = Yii::app()->request->getParam('Search')['metroStationId'] ? Yii::app()->request->getParam('Search')['metroStationId'] : '';
			Yii::app()->clientScript->registerScript('select22', '
				$(document).ready(function() {		
					$("#Doctor_sexId").select2("val", ["' . Yii::app()->request->getParam('Search')['sexId'] . '"]); 
					$("#Doctor_price").select2("val", ["' . Yii::app()->request->getParam('Search')['price'] . '"]); 
					$("#Doctor_clinicLinkUrl").select2("val", ["' . Yii::app()->request->getParam('Search')['clinicLinkUrl'] . '"]); 
				});	
			');
		?>
			<div class="search-doctor-filter ajax_filter" style="height: 30px; padding-right: 15px; vertical-align: bottom;">
				<span class="niceCheck <?= $searchModel->onlySale ? " niceChecked" : "" ?>" id="search-form_onlySale" <?= $searchModel->onlySale ? " checked" : "" ?>></span>
				<label class="" for="search-form_onlySale">только со скидкой</label>
			</div>
		<?php if(!Yii::app()->params['samozapis'] && Yii::app()->params['regions'][City::model()->selectedCity->subdomain]['loyaltyProgram']): ?>
			<?php if(!$isVkApp): ?>
				<div class="search-doctor-filter ajax_filter" style="height: 30px;padding-top: 15px; padding-right: 15px; vertical-align: bottom;">
					<span class="niceCheck <?= $searchModel->loyaltyProgram ? " niceChecked" : "" ?>" id="search-form_loyaltyProgram" <?= $searchModel->loyaltyProgram ? " checked" : "" ?>></span>
					<label class="" for="search-form_loyaltyProgram">оплата бонусами ЕМП</label>
		            <a href="/loyalty.html">подробнее</a>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<div style="display: none" class="show-in-responsive">
			<div style="width:240px;display: table;margin: 0 auto;">
				<button class="btn-green" style="float: left;padding: 10px;width: 45%;background: transparent;border: 1px solid #999;color: #999 !important;" onclick="resetFilters();lastPageClose();">Сбросить</button>
				<button class="btn-green" style="float:right;padding: 10px;width: 45%;background: rgb(141,198,63);border: 1px solid rgb(141,198,63);" onclick="lastPageClose();refreshSearch(0);">Применить</button>
			</div>
		</div>
	</div>
</div>
<div id="search_map" style="display: none;"></div>
<div class="SECTION SEARCH_RESULT">
	<div class="main_column" >
		<div class="search-result-head"<?=$searchResultHead_style?>>
			<?php if(!$isVkApp): ?>
				<h1 class="search-found">
				<?= $pageTagH1 ?>
				</h1>
			<?php else: ?>
				<a href="/appVk?vk=1">К списку адресов</a>
			<?php endif; ?>
			
			<div class="none-in-responsive" style="display: inline-block; float: right;">
				<span class="icon_showOnMap"></span>
				<a class="show_on_map found" href="<?=$_SERVER['REQUEST_URI']?>">Показать на карте</a>
				<a href="javascript:void(0)" class="show_on_map close" style="display: none;">Скрыть карту</a>
				<div class="sort_search">
					<span style="margin: 0 4px 0 0;">Сортировать:</span>
					<?= 
						CHtml::dropDownList(CHtml::activeName($model, 'sort'), $searchModel->sort,
							[
									null => 'без сортировки',
									'price asc'=>'цена от маленькой к большой',
									'price desc'=>'цена от большой к маленькой',
							],
							[
									'class' => 'ajax_filter',
									'style' => '
									    padding: 2px 5px 2px 7px;
									    background: url(/images/newLayouts/icon_search_drop.png) no-repeat 97% 55% white !important;
									    background-size: 12px 12px !important;
									    appearance: none !important;
									    -moz-appearance: none !important;
									    -webkit-appearance: none !important;
									    border: 1px solid #ddd !important;
									'
						]);
					?>
				</div>
			</div>
			<a href="javascript:void(0)" class="show-in-responsive" style="display: none;float:right;color:gray;text-decoration: initial;width: 95px;background: url(/images/icons/arrow-right.png) no-repeat right transparent;" onclick="nextPageOpenContent('.main_content #search_filters_block')">ФИЛЬТРЫ</a>
		</div>
		<div style="color: rgb(100,157,22); margin: 13px; font-weight: bold; text-align: center;font-size: 19px; display: none;" id="serviceSend">
							Ваша заявка на услугу отправленна!
						</div>
		<div class="search-result">
		<?= MyTools::createFromTemplate("<span class='search-found-count' style='display:none;'>%totalItemCount%</span>", $templateRules); ?>
			<?php if (empty($dataProvider)): ?>
				<?php echo CHtml::errorSummary($searchModel); ?>
			<?php else: ?>
			<?php
				$searchResult = $this->widget('zii.widgets.CListView', array(
					'dataProvider' => $dataProvider,
					'itemView' => '_services',
					'itemsTagName' => 'div',
					'emptyText' => "<div class='flash-notice'>" . $emptyText . "</div>",
					'template' => "{items}",
					'beforeAjaxUpdate' => 'function(id,date) { }',
					'afterAjaxUpdate' => 'function(id,date) { }',
					'viewData' => array(
							'services' => $services,
							'addr' => $addr
					),
					'pager'=>array(
					),
					'htmlOptions' => array(
						"class" => "search_with_pager"
					)
				), true);
				echo preg_replace('%<div class="keys".*?</div>%s', '', $searchResult);
			endif;
			?>
			<script>
			$(function(){
				$('.serviceDescriptionWrap').not('.found').each(function(i,e){
					$(this).addClass('found');
					if(!$(this).overflown()) {
						$(this).closest('div').find('.moreServiceDescription a').click();
					} else {
						$(this).closest('div').find('.moreServiceDescription').css('visibility','visible');
					}
				})
			});
			</script>
			<br>
		</div>
		<div class="search_result_footer">
			<button id="search_result_append_button" class="show_more_btn btn-blue" title="Показать больше врачей"onclick="appendSearch(this);">Показать больше услуг</button>
			<div id="search_result_loading" class="icon_loading" style="display: none;"></div>
			<input id="search_result_page_number" type="hidden" value="1">
		</div>
		<div class="definition">
			<?php $description = ((is_object($spModel)) ? $spModel->getParameter('description', City::model()->selectedCityId) : NULL); ?>
			<?php
				$noindexDpecialtyDescription = (Yii::app()->session['selectedRegion'] != 'spb' || Yii::app()->params['samozapis'] || count($district_metro) > 0 || (count(explode('.', $_SERVER['HTTP_HOST'])) > 2));
				if ($noindexDpecialtyDescription) echo '<noindex>';
			?>
			<div class="decription_action_block" id="specialtyDescription">
				<?php if(!empty($description)): ?>
				<div class="decription_action_block_content">
					<h3 class="decription_action_block_header"><?= $spModel->name ?></h3>
					<p> <?= $description ?></p>
				</div>
				<?php endif; ?>
			</div>
			<?php
				if ($noindexDpecialtyDescription) echo '</noindex>';
			?>
			<?php if(!$isVkApp): ?>
			<div class="card_action_block">
				<div class="card_action_block_label">Легче лёгкого</div>
				<div class="card_action_block_content" style="background: url(/images/newLayouts/action_card_block.png?v=002) no-repeat scroll center 65px;width: 100%; height: 100%; ">
					<h3 class="color2" style="text-align:center; padding-top: 25px; padding-bottom: 10px;">Запись в 3 действия</h3>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if($isVkApp): ?>
		<input type="hidden" id="isVkApp" value="1">
		<div style="height: 15px;"></div>
	<?php endif; ?>
</div>
<?php
if(!$isVkApp):
	$seo2NoIndex = false; ($seoText) ? false : true;
	$seoTextArr = [];
	foreach(explode ("\n", $seoText) as $key => $value) {
		if(!empty(trim($value))) {
			if(count($seoTextArr) < 3) {
				$seoTextArr[] = $value;
			}
			else {
				$seoTextArr[2] .= "<br/>".$value;
			}
		}
	}
	?>
	<div class="SECTION SEO2" style="display: <?= (empty($seoText)) ? "none" : "block" ?>;">
	<?php if (!$seo2NoIndex): ?>
		<div class="main_column">
			<div class="seo_header_section">
				<p class="h1 color2 align-center">
					<?= $seoTextTitle ?>
				</p>
			</div>
			<div class="seo_block">
				<div class="seo_block_header">
					<div class="icon_drug"></div>
				</div>
				<div class="seo_block_content">
					<p><?= (isset($seoTextArr[0])) ? $seoTextArr[0] : "" ?></p>
				</div>
			</div>
			<div class="seo_block">
				<div class="seo_block_header">
					<div class="icon_ambulance"></div>
				</div>
				<div class="seo_block_content">
					<p><?= (isset($seoTextArr[1])) ? $seoTextArr[1] : "" ?></p>
				</div>
			</div>
			<div class="seo_block">
				<div class="seo_block_header">
					<div class="icon_ukol"></div>
				</div>
				<div class="seo_block_content">
					<p><?= (isset($seoTextArr[2])) ? $seoTextArr[2] : "" ?></p>
				</div>
			</div>
		</div>
	<?php endif; ?>
	</div>
<?php endif; ?>