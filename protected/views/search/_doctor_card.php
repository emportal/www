<?php
$isVkApp = Yii::app()->request->getParam('vk');
if(is_array($data->placeOfWorks) && isset($data->placeOfWorks[0])) {
	$absoluteLink = Yii::app()->request->getParam('absoluteLink',false) ? MyTools::url_origin($_SERVER, false) : "";
	$subdomain = $data->placeOfWorks[0]->address->city->subdomain;
	$domain = City::getRedirectUrl($subdomain, $data->placeOfWorks[0]->address->samozapis, "");
	$viewUrl = $absoluteLink . (($domain !== MyTools::url_origin($_SERVER, false)) ? $domain : "") . Yii::app()->createUrl('doctor/view', array('linkUrl' => $data->linkUrl, 'companyLinkUrl' => $data->placeOfWorks[0]->address->company->linkUrl, 'addressLinkUrl' => $data->placeOfWorks[0]->address->linkUrl));
	if($isVkApp) {
		$viewUrl .= '?vk=1&address='.Yii::app()->request->getParam('address');
	}
	$empLoyaltyProgram = false;
	if(Yii::app()->params['regions'][$subdomain]['loyaltyProgram']) {
		$empLoyaltyProgram = $data->getIsLoyaltyProgramParticipant();
	}
}
?>
<div class="doctor_card">
	<a href="<?= $viewUrl ?>" class="hideInSmallCard" style="
		display: block; position: absolute;
		width: 100%; height: 100%;    
		top: 0px; left: 0px;">
	</a>
	<div class="doctor_card_content">
		<div class="doctor_card_column_sign">
			<?php if ($data->photo): ?>
				<?= CHtml::link(CHtml::image($absoluteLink.$data->photoUrl, false), $viewUrl, ['class' => 'doctor_card_photo']); ?>
			<?php else: ?>
				<?= CHtml::link(CHtml::image($absoluteLink.$this->assetsImageUrl . '/staff-'. ($data->sex->order==0 ? 'm' : 'f') .'.jpg', false, ['style' => 'width: 140px;']), $viewUrl, ['class' => 'doctor_card_photo']) ?>
			<?php endif; ?>
			<div class="sign_button_block">
			<?= Html::link('Подробнее', $viewUrl, array('class' => 'btn-green btn-read-more hideInSmallCard')) ?>
			<?php if ($empLoyaltyProgram) : ?>
				<?php if (!$isVkApp) : ?>
					<a href="<?=$absoluteLink?>/loyalty.html" target="_blank" title="Программа лояльности для пациентов ЕМП" class="loyaltyLink">
						<div class="empLoyaltyProgram vmargin16"></div>
					</a>
				<?php endif; ?>
			<?php endif; ?>
			</div>
		</div>
		<div class="doctor_card_column_description">
			<?= Html::link(CHtml::encode($data->name), $viewUrl, ['class' => 'doctor_card_name', 'style'=>"position: relative;"]) ?>
			
			<div class="doctor_rate_block<?= (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) ? ' vhidden' : '' ?>">
				<input type="range" min="0" max="5" value="<?= $data->roundedRating ?>" step="0.1" id="backing-<?= $data->link ?>" style="display: none;">
				<span id="rate-<?= $data->link ?>" data-rateit-value="<?= $data->roundedRating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $data->link ?>"></span>
				<button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
				<span class="rating-text"><?= $data->boldedRoundedRating ?></span>
				<a href="<?=$viewUrl?>#reviews" class="cardReviewsCount">
					<?=$data->publishedReviewsCount . " " . MyTools::createCountableForm($data->publishedReviewsCount, "отзыв", "отзыва", "отзывов");?>
				</a>
				<script>
				$(function() {
					jQuery('#rate-<?= $data->link ?>').rateit({max: 5, step: 0.1, starwidth: 23, starheight: 20, backingfld: '#backing-<?= $data->link ?>'});
				});
				</script>
			</div>
		</div>
		<div class="doctor_rank-address_block">
			<div class="doctor_rank_block">
				<?php if(count($data->scientificDegrees) && $big): ?>
				<div class="card_row">
					<span class="item_value"><?= (count($data->scientificDegrees) && $big) ? CHtml::encode($data->scientificDegrees[0]->name) : "" ?></span>
					<span class="item_name">
						<?= (!empty($data->scientificTitle) && $big && !empty($data->scientificTitle->name)) ? CHtml::encode($data->scientificTitle->name) : "" ?>
					</span>	
				</div>
				<?php endif; ?>
				<?php if(!empty($groupedData[$data->id]->specialtyOfDoctors)): ?>
					<?php foreach ($groupedData[$data->id]->specialtyOfDoctors as $specialty) : ?>
						<div class="card_row">
							<span class="item_value">
								<?= (!empty($specialty->doctorSpecialty->name)) ? $specialty->doctorSpecialty->name : ""; ?>
							</span>
							<span class="item_name">
								<?= (!empty($specialty->doctorCategory->name)) ? CHtml::encode($specialty->doctorCategory->name) : "" ?>
							</span>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="address-block">
				<?php if ($data->getExperience()): ?>
					<div class="card_row hideInSmallCard">
						<span class="item_name" title="Стаж">Стаж:</span><span class="item_value"><?= $data->getExperience() ?></span>
					</div>
				<?php endif; ?>
				<div class="card_row">								
					<span class="item_name" title="Стаж">Место работы:</span>
					<?php if($isVkApp): ?>
						<span><?= CHtml::encode($data->placeOfWorks[0]->address->company->name) ?></span>
					<?php else: ?>
						<a href="<?=$absoluteLink?><?= $data->placeOfWorks[0]->address->getPagesUrls(true, true) ?>" style="position: relative;"><span><?= CHtml::encode($data->placeOfWorks[0]->address->company->name) ?></span></a>
					<?php endif; ?>
					&#187;
					<?php foreach ($data->placeOfWorks as $placeOfWork): ?>
						<?php if($isVkApp): ?>
							<span><?= CHtml::encode($placeOfWork->address->street) ?>, <?= CHtml::encode($placeOfWork->address->houseNumber) ?></span>
						<?php else: ?>
							<a href="<?=$absoluteLink?><?= (($domain !== MyTools::url_origin($_SERVER, false)) ? $domain : "") . $this->createUrl('clinic/map', ['companyLink' => $placeOfWork->address->company->linkUrl, 'linkUrl' => $placeOfWork->address->linkUrl]); ?>" class="show_on_map"  style="position: relative;display: inline-block;">
								<span><?= CHtml::encode($placeOfWork->address->street) ?>, <?= CHtml::encode($placeOfWork->address->houseNumber) ?></span>
							</a>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<?php
				$nearestMetroStation = $data->placeOfWorks[0]->address->nearestMetroStation;
				if ($nearestMetroStation->name):
				?>	
				<div class="card_row hideInSmallCard">
					<img src="/images/icon-metro-micro.png" class="metro_icon">
					<?php if($isVkApp): ?>
						<?= CHtml::encode($nearestMetroStation->name)?>
					<?php else: ?>
						<?= Html::link(CHtml::encode($nearestMetroStation->name), $absoluteLink."/doctor/metro-".$nearestMetroStation->linkUrl, array('style'=>"position: relative;")) ?>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div>
			<?php if ( !(new NetrikaLoader)->checkDoctor($data->id, $data->placeOfWorks[0]->address->id) ): ?>
				<div class="price-box">
					<span class="price-box-inner">
						<?php if ($groupedData[$data->id]->inspectPrice || $groupedData[$data->id]->InspectFree): ?>
							Стоимость приёма <?= (!$groupedData[$data->id]->InspectFree ? $groupedData[$data->id]->inspectPrice . ' Р.' : 'бесплатно') ?>
						<?php else: ?>
							Стоимость первичного приёма не указана
						<?php endif; ?>
					</span>
				</div>
			<?php endif; ?>
			<?= Html::link('Подробнее', $viewUrl, array('class' => 'btn-green btn-read-more small-card-submit', 'style' => 'display: none;')) ?>
			<?php if ($empLoyaltyProgram && !$isVkApp) : ?>
			<a href="<?=$absoluteLink?>/loyalty.html" target="_blank" title="Программа лояльности для пациентов ЕМП" class="loyaltyLink">
				<div class="empLoyaltyProgram hideInNormalCard" style="display: none;"></div>
			</a>
			<?php endif; ?>
			<div class="card_row">
				<span class="item_name">К этому врачу <?= MyTools::createCountableForm($data->countRecords,"записался","записалось","записалось") ?> уже<span class="rating-text"><?= intval($data->countRecords) ?></span>
					<?= MyTools::createCountableForm($data->countRecords,"человек","человека","человек") ?>
				</span>
			</div>
		</div>
		<div class="doctor_card_column_price" style="display:none; position: absolute; width: 280px; right: 20px; top: 20px;">
			<a title="Подробнее" href="<?=$this->createUrl($viewUrl)?>">
				<?php if (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']): ?>
					<button class="btn-green btn-read-more hideInSmallCard" style="height:55px; font-size: 18px;">Записаться</button>
				<?php else: ?>
					<button class="btn-green btn-read-more hideInSmallCard" style="height:55px; font-size: 18px;">Подробнее о враче</button>
				<?php endif; ?>
			</a>
			<?php if ( !(new NetrikaLoader)->checkDoctor($data->id, $data->placeOfWorks[0]->address->id) ): ?>
				<div class="price-box" style="border: none; margin: 22px 0 7px 0; width: auto; ">
					<span class="price-box-inner" style="display: table-row; line-height: 1em; ">
						<span style="display: table-cell;width: 100%;"></span>
						<?php if ($groupedData[$data->id]->inspectPrice || $groupedData[$data->id]->InspectFree): ?>
							<span style="display: table-cell;width: 100px;padding: 0 10px 0 0;font-weight: bold;text-align: right;vertical-align: middle;text-transform: uppercase;font-size: 10px;letter-spacing: 1px;">
								Стоимость приёма
							</span>
							<?php if(!$groupedData[$data->id]->InspectFree): ?>
								<span style="display: table-cell; vertical-align: middle; ">
									<span style="color: #333; font-weight: bold; font-size: 40px; line-height: 21px; font-family: 'ReformaGroteskBoldC';">
										<?= $groupedData[$data->id]->inspectPrice ?>
									</span>
								</span>
								<span style="display: table-cell; vertical-align: bottom; ">
									Р.
								</span>
							<?php else: ?>
								<span style="color: #333; font-weight: bold; font-size: 40px; line-height: 21px; display: block; text-align: right; font-family: 'ReformaGroteskBoldC';">
									бесплатно
								</span>
							<?php endif; ?>
						<?php else: ?>
							<span style="display: block;width: 250px;font-weight: bold;text-align: right;vertical-align: middle;text-transform: uppercase;font-size: 10px;letter-spacing: 1px;">
								Стоимость первичного приёма
							</span>
							<span style="font-weight: bold; font-size: 14px; color: black; display: block; text-align: right;">
								Не указана
							</span>
						<?php endif; ?>
					</span>
				</div>
			<?php endif; ?>
			<?php if ($empLoyaltyProgram && !$isVkApp) : ?>
				<a href="<?=$absoluteLink?>/loyalty.html" target="_blank" title="Программа лояльности для пациентов ЕМП" class="loyaltyLink">
					<div class="empLoyaltyProgram" style="width: 100%;float: right;"></div>
				</a>
			<?php endif; ?>
		</div>
	</div>
</div>