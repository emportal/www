<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */

//$tagTitle = strip_tags($titleText) . " - %appName%";
$baseTitle = preg_replace('!\s+!', ' ', MyTools::createFromTemplate(strip_tags($titleText), $templateRules));
$tagTitle = $baseTitle;

if (empty($district_metro)) {
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['title'])) {
		$tagTitle = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['title'];
	}
}
foreach ($district_metro as $value) {
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['title'])) {
		$tagTitle = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['title'];
		break;
	}
}
if(mb_stripos($tagTitle, 'портал') === false) $tagTitle = $tagTitle . ' - Единый медицинский портал';
$this->pageTitle = $tagTitle;

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function() {
		$('#doctors-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
");

Yii::app()->clientScript->registerMetaTag($this->pageTitle, 'titleText');
Yii::app()->clientScript->registerMetaTag($searchModel->doctorSpecialtyId, 'doctorSpecialtyId');

?>
<?php
$this->breadcrumbs = array(
	'Врачи',
);
if(!empty($spModel)) {
	$this->breadcrumbs = array(
		'Врачи' => '/doctor',
		$spModel->name
	);
	if(!empty($district_metro)) {
		$this->breadcrumbs = array(
			'Врачи' => '/doctor',
			$spModel->name => '/doctor/specialty-'.$spModel->linkUrl,
			$templateRules['%metroDistrict%'],
		);
	}
} elseif(!empty($district_metro)) {
	$this->breadcrumbs = array(
		'Врачи' => '/doctor',
		$templateRules['%metroDistrict%'],
	);
}
?>
<div class="SECTION crumbs_section">
	<div class="main_column">
		<?php 
			if(!$isVkApp) {
				$this->widget('Breadcrumbs', array('links' => $this->breadcrumbs, 'htmlOptions' => [ 'class' => 'crumbs' ]));
			}
		?>
	</div>
</div>

<div class="SECTION TEXT_PAGE <?=(!is_array($searchModel['metroStationId']))?'geo-hidden':'' ?>">
<?php //if(!empty($specInfo)):
 	if(0): ?>
		<div class="conteiner">
			<h1 class="title-block">
				<?=MyTools::createFromTemplate($titleH1, $templateRules); ?>
			</h1>
			<div class="left-block">
				<duv class="text-block">
					<?=$specInfo; ?>
				</duv>
			</div>
			<div class="right-block">
				<div class="left-list-block">
					<div class="disease">
						<div class="title-list-block">
							Распространенные заболевания:
						</div>
						<ul>
							<?php
							if (isset($disease)):
								foreach ($disease as $val):
									?>
									<li><a href="<?='/handbook/disease/'.$val->attributes['linkUrl']?>"><?=$val->attributes['name']?></a></li>
									<?php
								endforeach;
							endif;
							?>
						</ul>
					</div>
				</div>
				<div class="right-list-block">
					<?
					if(count($doctorsMetro) > 0):
					?>
					<div class="doctors-metro">
						<div class="title-list-block">
							Доктора рядом с метро:
						</div>
						<ul>
							<?php
							foreach ($doctorsMetro as $key => $val):
							?>
								<li><?=CHtml::link($val->attributes['name'], array(
										"/doctor/metro-".$val->attributes['linkUrl']."/specialty-".$spModel->attributes['linkUrl']
									)); ?>
								</li>
							<?php
							endforeach;
							?>
						</ul>
					</div>
					<?
					endif;
					if(count($doctorsRegion) > 0):
					?>
					<div class="doctors-regions">
						<div class="title-list-block">
							Доктора в соседних районах:
						</div>
						<ul>
							<?php
							foreach ($doctorsRegion as $key => $val):
							?>
								<li><?=CHtml::link($val->attributes['name'], array(
										"/doctor/raion-".$val->attributes['linkUrl']."/specialty-".$val->attributes['linkUrl']
									)); ?>
								</li>
							<?php
							endforeach;
							?>
						</ul>
					</div>
					<?php
					endif;
					?>
				</div>
			</div>
		</div>
<?php endif; ?>
</div>
<div class="SECTION SEARCH_FILTERS" id="search_filters_block">
	<div class="main_column">
		<div style="display: none;" class="b-top" id="scroller">
			<span class="b-top-but">наверх</span>
		</div>
		<div class="search-doctor-filter ajax_filter" style="padding-right: 22px;">
			<span class="item_title">фио</span>
			<?php
			echo CHtml::textField(CHtml::activeName($model, 'text'), Yii::app()->request->getParam('Search')['text'], array(
				'id' => 'search-doctor-form_text',
				'class' => 'custom-text',
				'autocomplete' => 'off',
				'placeholder' => 'Не имеет значения',
				'value' => ''
			));
			?>
		</div>
		<div class="search-doctor-filter ajax_filter" style="<?=(Yii::app()->params["samozapis"]) ? "display: none;" : ""?>">
			<span class="item_title">Стоимость обращения (рубли)</span>
			<?php $this->widget('PriceSlider', array('id' => 'doctor_price_slider', 'formId' => 'search-doctor-form', 'name' => 'Search', 'inputMin' => 'priceMin', 'inputMax' => 'priceMax')); ?>	
		</div>
		<?php if (!Yii::app()->params['samozapis']) : ?>
		<div class="search-doctor-filter ajax_filter displaynone none-in-responsive">
			<span class="item_title">Пол специалиста</span>
			<?php
				$this->widget('ESelect2', [
					'name' => CHtml::activeName($model, 'sexId'),
					'data' => $sexTypesList,
					'options' => [
						'placeholder' => 'Пол специалиста',
						'allowClear' => true,
						'minimumResultsForSearch' => -1,
					],
					'htmlOptions' => [
						'style' => '
							width: 235px;
							height: 40px;
							border-radius: 3px 3px 3px 3px;
							border: 1px solid rgb(220,220,220);'
					]
				]);
			?>
		</div>
		<?php endif; ?>

			<div class="search-doctor-filter ajax_filter" style="padding-left: 15px;padding-right: 0px;<?=$isVkApp?'display: none;':'';?>">
				<span class="item_title">Клиника</span>
				<?php
					$this->widget('ESelect2', [
						'name' => CHtml::activeName($model, 'clinicLinkUrl'),
						'data' => $companyList,
						'value' => $searchModel->clinicLinkUrl,
						'options' => [
							'placeholder' => 'Любая клиника',
							'allowClear' => true,
							'minimumResultsForSearch' => -1,
						],
						'htmlOptions' => [
							'style' => '
								width: 235px;
								height: 40px;
								border-radius: 3px 3px 3px 3px;
								background: white;'
						]
					]);
				?>
			</div>
			<?php if($isVkApp): ?>
				<div class="search-doctor-filter ajax_filter" style="padding-left: 15px;padding-right: 0px;">
					<span class="item_title">Специальность</span>
						<?php						
							$this->widget('ESelect2',[
								'name' => 'Doctor[doctorSpecialty]',						
								'data' =>  $doctorSpecialties,
								'value' => $searchModel->doctorSpecialtyId,
								'options'=>[
									'placeholder'=>'Специальность врача',
									'allowClear'=>true,	
									'minimumResultsForSearch' => -1,						
								],
								'htmlOptions' => [
									'style' => '
										width: 235px;
										height: 40px;
										border-radius: 3px 3px 3px 3px;
										background: white;'
								]
								
							]);
						?>
				</div>
			<?php endif; ?>
        <?php if(!Yii::app()->params['samozapis'] && Yii::app()->params['regions'][City::model()->selectedCity->subdomain]['loyaltyProgram']): ?>
        	<?php if(!$isVkApp): ?>
	            <div class="search-doctor-filter ajax_filter" style="height: 30px; padding-top: 15px; padding-right: 15px; vertical-align: bottom;">
	                <span class="niceCheck <?= $searchModel->loyaltyProgram ? " niceChecked" : "" ?>" id="search-form_loyaltyProgram" <?= $searchModel->loyaltyProgram ? " checked" : "" ?>></span>
	                <label class="" for="search-form_loyaltyProgram">оплата бонусами ЕМП</label>
	                 <a href="/loyalty.html">подробнее</a> 
	            </div>
            <?php endif; ?>
            <?php if(!$isVkApp): ?>
	            <div class="search-doctor-filter ajax_filter" style="height: 30px; padding-top: 15px; padding-right: 15px; vertical-align: bottom;">
	                <span class="niceCheck <?= $searchModel->offline ? "" : " niceChecked" ?>" id="search-form_offline_not" <?= $searchModel->offline ? "" : " checked" ?>></span>
	                <label class="" for="search-form_offline_not">Онлайн-запись</label>
	            </div>
        	<?php endif; ?>
        <?php endif; ?>
		<div class="search-doctor-filter ajax_filter" style="display:none;">
			<span class="item_title">Стоимость обращения (рубли)</span>
			<?php
				/*
				$this->widget('ESelect2', [
					'name' => CHtml::activeName($model, 'price'),
					'data' => [			
						2 => 'от 0 до 500',
						3 => 'от 500 до 1000',
						4 => 'от 1000 до 2000',
						5 => 'от 2000 до 3000',
						6 => 'от 3000 до 5000',
						7 => 'от 5000',
					],
					'options' => [
						'allowClear' => true,
						'placeholder' => 'Стоимость обращения',
						'minimumResultsForSearch' => -1,
					]
				]);
				*/
			?>
		</div>
		<?php
			$metro = Yii::app()->request->getParam('Search')['metroStationId'] ? Yii::app()->request->getParam('Search')['metroStationId'] : '';
			Yii::app()->clientScript->registerScript('select22', '
				$(document).ready(function() {		
					$("#Doctor_sexId").select2("val", ["' . Yii::app()->request->getParam('Search')['sexId'] . '"]); 
					$("#Doctor_price").select2("val", ["' . Yii::app()->request->getParam('Search')['price'] . '"]); 
					$("#Doctor_clinicLinkUrl").select2("val", ["' . Yii::app()->request->getParam('Search')['clinicLinkUrl'] . '"]); 
				});	
			');
		?>
		<div style="display: none" class="show-in-responsive">
			<div style="width:240px;display: table;margin: 5px auto;">
				<button class="btn-green" style="float: left;padding: 10px;width: 45%;background: transparent;border: 1px solid #999;color: #999 !important;" onclick="resetFilters();lastPageClose();">Сбросить</button>
				<button class="btn-green" style="float:right;padding: 10px;width: 45%;background: rgb(141,198,63);border: 1px solid rgb(141,198,63);" onclick="lastPageClose();refreshSearch(0);">Применить</button>
			</div>
		</div>
	</div>
</div>
<?php if(Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']): ?>
<div class="SECTION" style="background-color: rgb(248,248,248);">
<?php if(isset($_GET['cancelAppointment'])): ?>
	<?php if($searchModel->OMSFormData_check()): ?>
		<script type="text/javascript">
			$(window).load(function() {
				emias.showReceptions();
			});
		</script>
	<?php else: ?>
		<script type="text/javascript">
			$(window).load(function() {
				showOMSpopupView();
			});
		</script>
	<?php endif; ?>
<?php elseif(isset($_GET['showReferrals'])): ?>
	<?php if($searchModel->OMSFormData_check()): ?>
		<script type="text/javascript">
			$(window).load(function() {
				emias.showReferrals();
			});
		</script>
	<?php else: ?>
		<script type="text/javascript">
			$(window).load(function() {
				showOMSpopupView();
			});
		</script>
	<?php endif; ?>
<?php endif; ?>
	<div class="main_column" >
		<div style="border-radius: 5px;padding: 5px;margin: 0px 0px 0px 0px;line-height: 1.5em;/* background: rgb(222, 235, 239); */">
			<?php if($searchModel->OMSFormData_check()): ?>
			<b>
			<?= $searchModel->OMSForm_surName . " " . $searchModel->OMSForm_firstName . " " . $searchModel->OMSForm_fatherName . " " . (new DateTime($searchModel->OMSForm_birthday))->format( 'd.m.Y' ) ?>. 
			</b>
			<a style="font-weight: bold;" href="javascript:void(0)" onclick="showOMSpopupView(); return false;" class="normal-link">Изменить ОМС данные.</a>
			<br>
			<ul style="padding: 5px 0px 5px 25px; margin: 0 0 0 0;">
				<li>
					<a style="font-weight: bold;" href="javascript:void(0)" onclick="emias.showReceptions(); return false;" class="normal-link">Посмотреть или отменить</a> Мои записи.
				</li>
				<li>
					<a style="font-weight: bold;" href="javascript:void(0)" onclick="openSpecialtySelect(); return false;" class="normal-link">Выбрать</a> специальность врача.
				</li>
				<li>
					<a style="font-weight: bold;" href="javascript:void(0)" onclick="emias.showReferrals(); return false;" class="normal-link">Выбрать</a> направление к врачу. <span class="referralsCount"></span> <span class="unselectReferral"></span>
				</li>
			</ul>
			<?php endif; ?>
		</div>
		<style>
		.emiasContentLoad {
			background: url(/images/loading.gif) no-repeat scroll center transparent;
		}
		</style>
		<div id="emiasContent" style="display: table; width:100%; min-height:40px; display: none;">
			
		</div>
	</div>
</div>
<?php endif; ?>
<div id="search_map" style="display: none;"></div>
<div class="SECTION SEARCH_RESULT">
	<div class="main_column" >
		<div class="search-result-head"<?=$searchResultHead_style?>>

			<?php
			if(isset($tagH2) and !empty($tagH2)):
				?>
				<h2 class="search-found">
					<?=$tagH2; ?>
				</h2>
				<?php
			else:
				if(!$isVkApp) {
					?>
					<?= (!empty($specInfo)) ? "<h2 class='search-found'>" : "<h1 class='search-found'>" ?>
					<?php
						//H1 тег для города по умолчанию
						if (empty($district_metro)) {
							if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['h1'])) {
								$tagH1 = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['h1'];
							}
						}
						foreach ($district_metro as $value) {
							if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['h1'])) {
								$tagH1 = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['h1'];
								break;
							}
						}
						
							echo MyTools::createFromTemplate($tagH1, $templateRules);
					}
				?>
				<?= (!empty($specInfo)) ? "</h2>" : "</h1>" ?>
				<?php 					
					if($isVkApp) {
						echo '<a href="/appVk?vk=1">К списку адресов</a>';
					}
				?>
			<?php
			endif;
			?>
			<div class="none-in-responsive" style="display: inline-block; float: right;">
			<span class="icon_showOnMap"></span>
			<a class="show_on_map found" href="<?=$_SERVER['REQUEST_URI']?>">Показать на карте</a>
			<a href="javascript:void(0)" class="show_on_map close" style="display: none;">Скрыть карту</a>
			<div class="group_icon_views">
				<span style="margin: 0 4px 0 0;">Показывать:</span>
				<button class="icon_change_view_full view_selected" data-view="view_full"></button>
				<button class="icon_change_view_tiles" data-view="view_tiles"></button>
				<script>
			    $(function() {
				    function changeView() {
			    		$('.group_icon_views button').each(function(key,obj) {
				    		if($(obj).hasClass('view_selected')) {
					    		$('.search-result').addClass($(obj).data('view'));
				    		} else {
					    		$('.search-result').removeClass($(obj).data('view'));
				    		}
			    		});
				    }
			    	$('.group_icon_views button').on('click', function(e) {
			    		$('.group_icon_views button.view_selected').each(function(key,obj) {
				    		$('.search-result').removeClass($(obj).data('view'));
			    		});
			    		$('.group_icon_views button').removeClass('view_selected');
			    		$(this).addClass('view_selected');
			    		$('.search-result').addClass($(e.toElement).data('view'));
			    		changeView();
				    });
			    	changeView();
				});
				</script>
			</div>
			<div class="sort_search">
				<span style="margin: 0 4px 0 0;">Сортировать:</span>
				<?= 
					CHtml::dropDownList(CHtml::activeName($model, 'sort'), $searchModel->sort,
						[
								null => 'рейтинг врачей по убыванию',
								'rating asc' => 'рейтинг врачей по возрастанию',
								'price asc'=>'цена от маленькой к большой',
								'price desc'=>'цена от большой к маленькой',
								'experience asc'=>'стаж по убыванию',
								'experience desc'=>'стаж по возрастанию',
								'countRecords' => 'по популярности',
								'countCommentAsc' => 'количество отзывов по возрастанию',
								'countCommentDesc' => 'количество отзывов по убыванию',
						],
						[
								'class' => 'ajax_filter',
								'style' => '
								    padding: 2px 5px 2px 7px;
								    background: url(/images/newLayouts/icon_search_drop.png) no-repeat 97% 55% white !important;
								    background-size: 12px 12px !important;
								    appearance: none !important;
								    -moz-appearance: none !important;
								    -webkit-appearance: none !important;
								    border: 1px solid #ddd !important;
								'
					]);
				?>
			</div>
			</div>
			<a href="javascript:void(0)" class="show-in-responsive" style="display: none;float:right;color:gray;text-decoration: initial;width: 95px;background: url(/images/icons/arrow-right.png) no-repeat right transparent;" onclick="nextPageOpenContent('.main_content #search_filters_block')">ФИЛЬТРЫ</a>
		</div>
		<div class="search-result view_full">
		<?= MyTools::createFromTemplate("<span class='search-found-count' style='display:none;'>%totalItemCount%</span>", $templateRules); ?>
		<?php if (empty($dataProvider)): ?>
			<?php echo CHtml::errorSummary($searchModel); ?>
		<?php else: ?>
			<?php
			
			if ($isWidget == 1) {
				$itemView = '_doctors_for_widget';
				$searchResultHead_style = ' style="padding-left: 0; padding-top: 15px;"';
			} else {
				$itemView = '_doctors';
			}
            
            if ($notificationText)
                echo '<div class="flash-error">' . $notificationText . '</div>';
			
			$searchResult = $this->widget('zii.widgets.CListView', array(
				'dataProvider' => $dataProvider,
				'itemView' => $itemView,
				'itemsTagName' => 'div',
				'template' => "{items}",
				'emptyText' => ( (empty($notificationText)) ? ("<div class='flash-notice'>" . $emptyText . "</div>") : "" ),
				'beforeAjaxUpdate' => 'function(id,date) { }',
				'afterAjaxUpdate' => 'function(id,date) { }',
				'viewData' => array(
					'show' => $show,
					'doctors' => $doctors
				),
				'htmlOptions' => array(
					"class" => "search_with_pager"
				)
			), true);
			echo preg_replace('%<div class="keys".*?</div>%s', '', $searchResult);
			?>
		<?php endif; ?>	
		</div>
		<div class="search_result_footer">
			<button id="search_result_append_button" class="show_more_btn btn-blue" title="Показать больше врачей"onclick="appendSearch(this);">Показать больше врачей</button>
			<div id="search_result_loading" class="icon_loading" style="display: none;"></div>
			<input id="search_result_page_number" type="hidden" value="1">
		</div>
	</div>
	<?php if($isVkApp): ?>
		<input type="hidden" id="isVkApp" value="1">
		<div style="height: 15px;"></div>
	<?php endif; ?>
</div>
<?php
if(!$isVkApp):
if(count($dataSpecList) > 0):
	?>
	<div class="SECTION all-spec">
		<div class="title-all-spec"><?=MyTools::createFromTemplate($title_all_spec, $templateRules) ?></div>
		<div class="lists-all-spac">
			<ul class="list-all-spec">
				<?php
				$searchData = [];
				$dataIndex = 0;
				foreach ($district_metro as $value) {
					$searchData[$dataIndex] = ["name" => 'Search[metroStationId][]', "value" => $value];
					$dataIndex++;
				}
				$countColumns = 4;
				$countLines = ceil(count($dataSpecList)/$countColumns);
				$cElements = count($dataSpecList);
				$i = 1;
				foreach ($dataSpecList as $key => $val):
					if($val->attributes['samozapis'] == 1){
						$url = 'https://samozapis.emportal.ru';
					}else{
						$url = '';
					}
					$searchData[$dataIndex] = ["name" => 'Search[doctorSpecialtyId]', "value" => 'specialty-'.$val->attributes['linkUrl']];
					?>
					<li ><a href="<?=$url?><?=MyTools::GetSearchUrl('doctor',$searchData)?>"><?=$val->attributes['name'] ?></a></li>
					<?php
					if($i >= $cElements) break;
					if($i%$countLines == 0) echo "</ul><ul class='list-all-spec'>";
					$i++;
				endforeach;
				?>
			</ul>
		</div>
	</div>
	<?php
endif;
?>
<div class="SECTION" style="background: #f8f8f8;padding: 20px 0 0 0;">
	<div class="main_column">
		<div class="definition">
			<?php $description = ((is_object($spModel)) ? $spModel->getParameter('description', City::model()->selectedCityId) : NULL); ?>
			<div class="card_action_block">
				<div class="card_action_block_label">Легче лёгкого</div>
				<div class="card_action_block_content" style="background: url(/images/newLayouts/action_card_block.png?v=002) no-repeat scroll center 65px;width: 100%; height: 100%; ">
					<h3 class="color2" style="text-align:center; padding-top: 25px; padding-bottom: 10px;">Запись в 3 действия</h3>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$seo2NoIndex = true;
$seoText = $spModel->seoText;

if (empty($district_metro)) {
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoText'])) {
		$seoText = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoText'];
		$seo2NoIndex = false;
	}
}
foreach ($district_metro as $value) {
	if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoText'])) {
		$seoText = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoText'];
		$seo2NoIndex = false;
		break;
	}
}
//if(!Yii::app()->params['devMode'])
//	if(count(explode('.', $_SERVER['HTTP_HOST'])) > 2)
//		$seo2NoIndex = true;
$seoText = MyTools::createFromTemplate($seoText,$templateRules);
$seoTextArr = [];
foreach(explode ("\n",$seoText) as $key=>$value) {
	if(!empty(trim($value))) {
		if(count($seoTextArr) < 3)
			$seoTextArr[] = $value;
		else
			$seoTextArr[2] .= "<br/>".$value;
	}
}
?>
<div class="SECTION SEO2" style="display: <?= (empty($seoText)) ? "none" : "block" ?>;">
<?php if (!$seo2NoIndex): ?>
	<div class="main_column">
		<div class="seo_header_section">
			<p class="h1 color2 align-center">
				<?php
				$seoTextTitle = ''; //(!empty($spModel->tagH1)) ? $spModel->tagH1 : $spModel->name;
				if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoTextTitle'])) {
					$seoTextTitle = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['seoTextTitle'];
				}
				foreach ($district_metro as $value) {
					if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoTextTitle'])) {
						$seoTextTitle = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['seoTextTitle'];
						break;
					}
				}
				echo $seoTextTitle;
				?>
			</p>
		</div>
		<div class="seo_block">
			<div class="seo_block_header">
				<div class="icon_drug"></div>
			</div>
			<div class="seo_block_content">
				<p><?= (isset($seoTextArr[0])) ? $seoTextArr[0] : "" ?></p>
			</div>
		</div>
		<div class="seo_block">
			<div class="seo_block_header">
				<div class="icon_ambulance"></div>
			</div>
			<div class="seo_block_content">
				<p><?= (isset($seoTextArr[1])) ? $seoTextArr[1] : "" ?></p>
			</div>
		</div>
		<div class="seo_block">
			<div class="seo_block_header">
				<div class="icon_ukol"></div>
			</div>
			<div class="seo_block_content">
				<p><?= (isset($seoTextArr[2])) ? $seoTextArr[2] : "" ?></p>
			</div>
		</div>
	</div>
<?php endif; ?>
</div>
<?php
	if(empty($tagDescription)) {
		$tagDescription = $baseTitle . ', доступны отзывы и онлайн запись на прием.';
	}
	if(!empty($spModel->tagDescription))
		$tagDescription = $spModel->tagDescription;
	if (empty($district_metro)) {
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId]['default']['description'])) {
			$tagDescription = $spModel->seoCombinationArr[City::model()->selectedCityId]['default']['description'];
		}
	}
	foreach ($district_metro as $value) {
		if(!empty($spModel) && !empty($spModel->seoCombinationArr[City::model()->selectedCityId][$value]['description'])) {
			$tagDescription = $spModel->seoCombinationArr[City::model()->selectedCityId][$value]['description'];
			break;
		}
	}
	Yii::app()->clientScript->registerMetaTag(MyTools::createFromTemplate($tagDescription, $templateRules), 'description');
	
	if(is_array($keywords)) {
		$tagDescription = mb_strtolower(implode(', ', $keywords));
		Yii::app()->clientScript->registerMetaTag(MyTools::createFromTemplate($tagDescription, $templateRules), 'keywords');
	}

endif;
?>
