<?php
	$this->widget('ESelect2', [
		'name' => 'companyVk',
		'data' => $companyList,
		'value' => '',
		'options' => [
			'placeholder' => 'Клиника',
			'allowClear' => true,
			'minimumResultsForSearch' => -1,
		],
		'htmlOptions' => [
			'style' => '
				width: 235px;
				height: 40px;
				border-radius: 3px 3px 3px 3px;
				background: white;'
		]
	]);
?>
<script type="text/javascript">
	$('#companyVk').on('change', function() {
		$('#vkSave').hide();
		$('#vkSaveOk').hide();
		$.ajax({
			url: '/ajax/vkAppAddress',
			type: "GET",
			data: { company: $(this).val() },
			success: function(data) {
				$('#addressBlock').show();
				$('#addressList').html(data);
			}
		});			
	});
</script>