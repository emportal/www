<?php
/* @var $dataProvider CActiveDataProvider */
/* @var $data Company */
$address = $widget->viewData['addr'][$data->id][0];
?>

<?php 
	$this->renderPartial('//search/_clinic_card',[
		'data' => $data,
		'address' => $address,
		'addresses' =>$widget->viewData['addr'][$data->id],
		'big' => 1,					
	]);
?>
