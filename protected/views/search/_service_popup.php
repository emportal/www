<?php
// $addressServiceId;
// $addressServicePrice;
// $serviceName;
// $companyLink;
// $companyName;
// $addressName;
// $patientName;
// $patientPhone;
// $znp;
$isVkApp = Yii::app()->request->getParam('vk');
?>
<div id="<?= $addressServiceId ?>" class="md-modal popup">
	<div class="md-close">X</div>
	<div class="md-content">
		<div class="md-form-container">
			<div>
				<div>
					<table class="service-table no-border no-margin">
						<tr>
							<td colspan="2" class="no-border">
								<?php if($znp == 1): ?>
									<div class="h3"><?= CHtml::encode($serviceName)?></div class="h3">
								<?php else: ?>
									<div class="h3">Запись на услугу <?= CHtml::encode($serviceName) ?>, <?= $addressServicePrice ?> руб.</div class="h3">
								<?php endif; ?>
							</td>
						</tr>
						<?php if(!empty($description)): ?>
						<tr><td><?= $description ?></td></tr>
						<?php endif; ?>
					</table>
				</div>
				<div>
					<table class="service-table no-border no-margin">
						<tr>
							<td class="no-border" style="vertical-align: middle;">
								<img class="service-logo-image" src="https://emportal.ru/uploads/company/<?= $companyLink ?>/logo.png"  onError="this.onerror=null;this.src='<?=htmlentities($this->assetsImageUrl.'/icon_emp_med.jpg')?>';">
							</td>
							<td class="no-border" style="vertical-align: middle;">
								<?= CHtml::encode($companyName) ?>, 
								<?= CHtml::encode($addressName) ?>
							</td>
						</tr>
						<tr>
							<td class="no-border item_name">
								Ваше имя
							</td>
							<td class="no-border">
								<input type="text" class="purchase-service-name" value="<?=$patientName;?>">
								<input type="hidden" name="vkViewerId" id="vkViewerId" value="<?=Yii::app()->session['vkViewerId'];?>">
							</td>
						</tr>
						<tr>
							<td class="no-border item_name">
								Ваш телефон
							</td>
							<td class="no-border">
								<input type="text" class="purchase-service-phone" value="<?=$patientPhone;?>">
							</td>
						</tr>
						<tr>
							<td class="no-border">
								<a href="#" data-id="<?= $addressServiceId ?>" class="btn-green btn-purchase-service /*border-radius-zero*/ tmargin30">
									Записаться
								</a>
							</td>
							<td class="no-border"></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
<?php if($isVkApp): ?>
	<script type="text/javascript">
		$.ajax({
			url: '/ajax/vkUserId',
			type: 'GET',
			success: function(msg) {
				$(document).ready(function() {
					VK.api("users.get", {"user_ids": msg}, function (data) {
						$(".purchase-service-name").val(data.response[0].last_name + ' ' + data.response[0].first_name);
						$(".purchase-service-phone").val('');
					});
				});
			},
		});

	</script>	
<?php endif; ?>	