<div class="search-block<?= $big ? ' big' : '' ?>"<?= $style ? ' style="'.$style.'"' : '' ?>>
	<div class="f-left left-side">
		<?php if ($data->photo): ?>
			<?= CHtml::link(CHtml::image($data->photoUrl, 'Врач'), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $data->placeOfWorks[0]->address->company->linkUrl, 'addressLinkUrl' => $data->placeOfWorks[0]->address->linkUrl), ['class' => 'photo-search']); ?>
		<?php else: ?>
			<?php if ($data->sex->order == 0): ?>
				<?= CHtml::link(CHtml::image($this->assetsImageUrl . '/staff-m.jpg', 'Врач'), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $data->placeOfWorks[0]->address->company->linkUrl, 'addressLinkUrl' => $data->placeOfWorks[0]->address->linkUrl), ['class' => 'photo-search doctor-logo']) ?>
			<?php else: ?>
				<?= CHtml::link(CHtml::image($this->assetsImageUrl . '/staff-f.jpg', 'Врач'), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $data->placeOfWorks[0]->address->company->linkUrl, 'addressLinkUrl' => $data->placeOfWorks[0]->address->linkUrl), ['class' => 'photo-search doctor-logo']) ?>
			<?php endif; ?>
		<?php endif; ?>				
		<?= Html::link('Записаться на прием', array('/doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $data->placeOfWorks[0]->address->company->linkUrl, 'addressLinkUrl' => $data->placeOfWorks[0]->address->linkUrl), array('class' => 'btn-green')) ?>				
	</div>
	<div class="f-left right-side">			
		<h5>
			<?= Html::link(CHtml::encode($data->name), array('doctor/view', 'linkUrl' => $data->linkUrl, 'companyLinkUrl' => $data->placeOfWorks[0]->address->company->linkUrl, 'addressLinkUrl' => $data->placeOfWorks[0]->address->linkUrl)) ?>				
		</h5>			
		
			<div class="doctor_rate_block">
				<input type="range" min="0" max="5" value="<?= $data->roundedRating ?>" step="0.1" id="backing-<?= $data->link ?>" style="display: none;">
				<span id="rate-<?= $data->link ?>" data-rateit-value="<?= $data->roundedRating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $data->link ?>"></span>
				<button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
				<span class="rating-text"><?= $data->boldedRoundedRating; ?></span>
				
				<script>
				$(function() {
					jQuery('#rate-<?= $data->link ?>').rateit({max: 5, step: 0.1, starwidth: 23, starheight: 20, backingfld: '#backing-<?= $data->link ?>'});
				});
				</script>
			</div>
			
		<p class="size-12">
			<?php			
			if (count($data->scientificDegrees) && $big) {
				echo CHtml::encode($data->scientificDegrees[0]->name) . "<br/><br/>";
			}
			?>
			<?php if (!empty($data->scientificTitle) && $big): ?>
				<?php !empty($data->scientificTitle->name) ? ", " . CHtml::encode($data->scientificTitle->name) . "<br/>" : '' ?>	
			<?php endif; ?>
			
			<?php if(!empty($groupedData[$data->id]->specialtyOfDoctors)): ?>
				<?php foreach ($groupedData[$data->id]->specialtyOfDoctors as $specialty) : ?>
					<?php
					if (!empty($specialty->doctorSpecialty->name)):
						echo CHtml::encode($specialty->doctorSpecialty->name);
					endif;

					if (!empty($specialty->doctorCategory->name)):
						echo $specialty->doctorCategory->name ? ", " . CHtml::encode($specialty->doctorCategory->name) . "" : '';
					endif;
					?><br/>
				<?php endforeach; ?>
			<?php endif; ?>
			
		</p>	
		<div class="address-block">
			<div class="size-12 experience">
				<?php if ($data->getExperience()): ?>
					<span title="Стаж"></span> Стаж <?= $data->getExperience() ?>
				<?php endif; ?>
			</div>
			<div class="company-info-name">								
				<a href="<?= $this->createUrl('clinic/view', ['companyLink' => $data->placeOfWorks[0]->address->company->linkUrl, 'linkUrl' => $data->placeOfWorks[0]->address->linkUrl]) ?>" class="company-icon">
					<span></span><?= CHtml::encode($data->placeOfWorks[0]->address->company->name) ?>									
				</a>
			</div>	
			<div class="street">
				<a class="address show_on_map" href="<?= $this->createUrl('clinic/map', ['companyLink' => $data->placeOfWorks[0]->address->company->linkUrl, 'linkUrl' => $data->placeOfWorks[0]->address->linkUrl]); ?>">
					<span></span><?= CHtml::encode($data->placeOfWorks[0]->address->street) ?>, <?= CHtml::encode($data->placeOfWorks[0]->address->houseNumber) ?>
				</a>
			</div>
			<?php if ($data->placeOfWorks[0]->address->nearestMetroStation->name): ?>								
				<div class="metro">
					<span></span><?= CHtml::encode($data->placeOfWorks[0]->address->nearestMetroStation->name) ?>
				</div>
			<?php endif; ?>

			<div class="clearfix"></div>
		</div>

		<?php if ($groupedData[$data->id]->inspectPrice): ?>
			<em class="price-box">
				<span class="price-box-inner">
					Стоимость приема <b><?= $groupedData[$data->id]->inspectPrice ?></b> р.
				</span>
			</em>			
		<?php elseif ($groupedData[$data->id]->inspectFree): ?>
			<em class="price-box">
				<span class="price-box-inner">
					Бесплатный прием
				</span>
			</em>
		<?php endif; ?>	
	</div>

</div>	