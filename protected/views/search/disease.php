<?php
/* @var $this SearchController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle = 'Справочник болезней - '.Yii::app()->name;

Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
		
$this->breadcrumbs = array(
	'Болезни'
);
?>
            <div class="content" style="padding-top: 10px; margin-top: 0px;">
                <div class="content-inner">
                    <div class="tabs-wrapper">
                        <div class="tab-line"></div>
                        <div class="tabs">
                            <div class="tab active">
                                <a class="tab-name no_underline" href="/handbook/disease">Болезни</a>
                                <div class="tab-content tab-diseases">
                                    <div class="letter-filter">
										<?php
											$selectedChar = mb_strtoupper($selectedChar);
											$russianChars = []; //range("192","223");
											foreach(range(chr(0xC0), chr(0xDF)) as $b) {
												
												$russianChars[] = iconv('CP1251', 'UTF-8', $b);
											}
											$charsToRemove = ['Ы', 'Й', 'Й', 'Й', 'Ъ', 'Ь'];
											$russianChars = array_diff($russianChars, $charsToRemove);
											
											foreach($russianChars as $char) {
											
												$isActive = ($char == $selectedChar) ? ' class="active no_underline"' : '';
												echo ' <a href="?Search%5Bchars%5D='.$char.'"'.$isActive.'><span'.$isActive.'>'.$char.'</span></a>';
											}
										?>
                                    </div>
                                    <div class="col3">
                                        <div class="clearfix">
											<?php
											$this->widget('ColumnListView', array(
												'dataProvider' => $dataProvider,
												'itemView' => '_disease',
												'itemsCssClass' => 'links diseases',
												'pagerCssClass' => 'pages',
												'pager' => array(
													'cssFile' => false,
													'header' => 'Страницы: '
												),
												'template' => "{items}\n{pager}",
													//'columns'=>array("one","two","three","four"),
											));
											?>
										</div><!-- /clearfix -->
                                        <div class="spacer"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="tab">
                                <div class="tab-name">Специальности врачей</div>
                            </div>
                            <div class="tab">
                                <div class="tab-name">Профили деятельности клиник</div>
                            </div> -->
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
