<?php
$this->layout = '//layouts/column2';
$this->breadcrumbs = ['API Единого Медицинского Портала'];
$seoTags = [
	'title' => 'Краткая документация API Единого Медицинского Портала',
	/*'description' => 'Команда сотрудников Единого Медицинского Портала - Делаем платную медицину доступнее',
	'seo_block' => [
		'Команда Единого Медицинского Портала работает над созданием российского сервиса записи в частные клиники России. ',
		'Создавая общую площадку для всех частных клиник, мы стараемся сделать платную медицину доступнее. ',
		'Не забываем и про удобство пользования и доступность. Сервис записи в частные клиники доступен жителям России абсолютно БЕСПЛАТНО.',
	]*/
];
$this->registerSeoTags($seoTags);
$this->seotype = 'hideseo';
?>
	<div class="content" style="padding-top: 0px;">
		<div class="news-info doc-api">
			<h1>О решении</h1>
			<p>
				<?= $this->breadcrumbs[0] ?> - функциональность, предназначенная для использования во внешних программных продуктах. 
				API позволяет внешним информационным системам обмениваться данными с emportal.ru в автоматическом режиме.
			</p>
			<h1>Подключение</h1>
			<p>
				API использует протокол аутентификации HTTP 1.0.
				<br>Всем пользователям API выдается уникальный авторизационный токен, использующийся в качестве пароля.
				<br>Для получения токена напишите на <a href="mailto:alexander.d@emportal.ru">alexander.d@emportal.ru</a>, в теме письма указав "Подключение к API emportal.ru".
			</p>
			<h1>Сценарий взаимодействия с внешней системой. <br>Интеграция с МИС.</h1>
			<p>
				1. Внешняя система, запрашивая ресурс API "Адреса", получает информацию об адресах и компаниях, представленных на портале.
				<br>2. На стороне внешней системы происходит сопоставление собственных адресов и компаний с адресами и компаниями, представленными на портале.
				<br>3. Внешняя система, запрашивая через API ресурс "Врачи", получает информацию о врачах, представленных на портале.
				<br>4. На стороне внешней системы происходит сопоставление собственных врачей и врачей, представленных на портале. Для улучшения точности сопоставления можно использовать информацию, полученную после выполнения п.2
				<br>5. Используя метод добавления ресурса API "Приемы" а также информацию, полученную после выполнения п.2 и п.4, внешняя система заполняет расписание соответствующих врачей по мере необходимости.
				<br>6. Для проверки корректности заполнения расписания и проверки наличия приемов врача на портале можно использовать метод получения связанных ресурсов "Приемы" у ресурса "Врач".
				<br><br>Далее идет описание основных ресурсов API.
			</p>
			<h2>Адреса</h2>
			<h4>Получение ресурса</h4>
			<p>
				GET https://emportal.ru/api/v1/addresses
			</p>
			<h4>Поля запроса</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>json</td>
					<td>0..1</td>
					<td>Int [0..1]</td>
					<td>определяет, будет ли ответ возвращен в формате json</td>
				</tr>
				<tr>
					<td>/</td>
					<td>region</td>
					<td>0..1</td>
					<td>String [Regions]</td>
					<td>определяет регион (город) выдачи. По умолчанию используется Санкт-Петербург.</td>
				</tr>
				<tr>
					<td>/</td>
					<td>id</td>
					<td>0..1</td>
					<td>String</td>
					<td>возможный идентификатор адреса.</td>
				</tr>
			</table>
			<h4>Поля ответа</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>id</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор адреса</td>
				</tr>
				<tr>
					<td>/</td>
					<td>company</td>
					<td>1..1</td>
					<td>String</td>
					<td>латиноязычный идентификатор организации</td>
				</tr>
				<tr>
					<td>/</td>
					<td>address</td>
					<td>1..1</td>
					<td>String</td>
					<td>латиноязычный идентификатор адреса</td>
				</tr>
				<tr>
					<td>/</td>
					<td>companyName</td>
					<td>1..1</td>
					<td>String</td>
					<td>название компании</td>
				</tr>
				<tr>
					<td>/</td>
					<td>addressName</td>
					<td>1..1</td>
					<td>String</td>
					<td>название адреса</td>
				</tr>
				<tr>
					<td>/</td>
					<td>coordinates</td>
					<td>0..1</td>
					<td>Container</td>
					<td>географические координаты</td>
				</tr>
				<tr>
					<td>/coordinates</td>
					<td>lon</td>
					<td>1..1</td>
					<td>String</td>
					<td>географическая долгота (longitude), на которой находится адрес</td>
				</tr>
				<tr>
					<td>/coordinates</td>
					<td>lat</td>
					<td>1..1</td>
					<td>String</td>
					<td>географическая широта (latitude), на которой находится адрес</td>
				</tr>
				<tr>
					<td>/</td>
					<td>region</td>
					<td>1..1</td>
					<td>String</td>
					<td>регион</td>
				</tr>
				<tr>
					<td>/</td>
					<td>cityDistrict</td>
					<td>0..1</td>
					<td>Container</td>
					<td>район города, в котором находится адрес</td>
				</tr>
				<tr>
					<td>/cityDistrict</td>
					<td>name</td>
					<td>1..1</td>
					<td>String</td>
					<td>название района города, в котором находится адрес</td>
				</tr>
				<tr>
					<td>/</td>
					<td>metroStations</td>
					<td>0..1</td>
					<td>Container</td>
					<td>ближайшие станции метро</td>
				</tr>
				<tr>
					<td>/metroStations</td>
					<td>metroStation</td>
					<td>1..1</td>
					<td>Container</td>
					<td>ближайшая станция метро</td>
				</tr>
				<tr>
					<td>/metroStations/metroStation</td>
					<td>name</td>
					<td>1..1</td>
					<td>String</td>
					<td>название станции метро</td>
				</tr>
				<tr>
					<td>/</td>
					<td>street</td>
					<td>1..1</td>
					<td>String</td>
					<td>название улицы, на которой расположен адрес</td>
				</tr>
				<tr>
					<td>/</td>
					<td>houseNumber</td>
					<td>0..1</td>
					<td>String</td>
					<td>номер дома</td>
				</tr>
				<tr>
					<td>/</td>
					<td>specialties</td>
					<td>0..1</td>
					<td>Container</td>
					<td>специальности, представленные в данном адресе</td>
				</tr>
				<tr>
					<td>/specialties</td>
					<td>specialty</td>
					<td>1..1</td>
					<td>Container</td>
					<td>специальность врача</td>
				</tr>
				<tr>
					<td>/specialties/specialty</td>
					<td>name</td>
					<td>1..1</td>
					<td>String</td>
					<td>название специальности, представленной в данном адресе</td>
				</tr>
			</table>
			<h2>Врачи</h2>
			<h4>Получение ресурса</h4>
			<p>
				GET https://emportal.ru/api/v1/doctors
				<br>GET https://emportal.ru/api/v1/doctors/<?= htmlspecialchars('<id>') ?>
			</p>
			<h4>Поля запроса</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>json</td>
					<td>0..1</td>
					<td>Int [0..1]</td>
					<td>определяет, будет ли ответ возвращен в формате json</td>
				</tr>
				<tr>
					<td>/</td>
					<td>region</td>
					<td>0..1</td>
					<td>String [Regions]</td>
					<td>определяет регион (город) выдачи. По умолчанию используется Санкт-Петербург.</td>
				</tr>
				<tr>
					<td>/</td>
					<td>addressId</td>
					<td>0..1</td>
					<td>String</td>
					<td>возможный идентификатор адреса. Если значение параметра не задано, то запрос вернёт информацию о всех врачах доступных адресов.</td>
				</tr>
			</table>
			<h4>Поля ответа</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>id</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>surName</td>
					<td>1..1</td>
					<td>String</td>
					<td>фамилия врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>firstName</td>
					<td>1..1</td>
					<td>String</td>
					<td>имя врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>fatherName</td>
					<td>1..1</td>
					<td>String</td>
					<td>отчество врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>linkUrl</td>
					<td>1..1</td>
					<td>String</td>
					<td>латиноязычный идентификатор врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>photoUrl</td>
					<td>1..1</td>
					<td>String</td>
					<td>ссылка на фотографию врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>rating</td>
					<td>1..1</td>
					<td>Float</td>
					<td>рейтинг врача по версии emportal.ru</td>
				</tr>
				<tr>
					<td>/</td>
					<td>company</td>
					<td>1..1</td>
					<td>Container</td>
					<td>Клиника</td>
				</tr>
				<tr>
					<td>/</td>
					<td>addresses</td>
					<td>0..1</td>
					<td>Container</td>
					<td>адреса, в которых принимает врач</td>
				</tr>
				<tr>
					<td>/addresses</td>
					<td>address</td>
					<td>0..1</td>
					<td>Container</td>
					<td>адрес, в котором принимает врач</td>
				</tr>
				<tr>
					<td>/addresses/address</td>
					<td>id</td>
					<td>1..1</td>
					<td>String</td>
					<td>идентификатор адреса</td>
				</tr>
				<tr>
					<td>/addresses/address</td>
					<td>linkUrl</td>
					<td>1..1</td>
					<td>String</td>
					<td>латиноязычный идентификатор адреса</td>
				</tr>
				<tr>
					<td>/</td>
					<td>specialties</td>
					<td>0..1</td>
					<td>Container</td>
					<td>специальности врача</td>
				</tr>
				<tr>
					<td>/specialties</td>
					<td>specialty</td>
					<td>0..1</td>
					<td>Container</td>
					<td>специальность врача</td>
				</tr>
				<tr>
					<td>/specialties/specialty</td>
					<td>name</td>
					<td>1..1</td>
					<td>String</td>
					<td>название специальности врача</td>
				</tr>
				<tr>
					<td>/specialties/specialty</td>
					<td>linkUrl</td>
					<td>1..1</td>
					<td>String</td>
					<td>латиноязычный идентификатор специальности</td>
				</tr>
				<tr>
					<td>/specialties/specialty</td>
					<td>category</td>
					<td>0..1</td>
					<td>String</td>
					<td>название категории врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>birthDate</td>
					<td>0..1</td>
					<td>Date</td>
					<td>дата рождения врача в формате Y-m-d</td>
				</tr>
				<tr>
					<td>/</td>
					<td>receptionPrice</td>
					<td>0..1</td>
					<td>Int</td>
					<td>стоимость первичного приема врача в рублях (если 0 - бесплатный прием)</td>
				</tr>
				<tr>
					<td>/</td>
					<td>practiceStartYear</td>
					<td>0..1</td>
					<td>Int</td>
					<td>год начала работы для вычисления стажа</td>
				</tr>
				<tr>
					<td>/</td>
					<td>scientificTitle</td>
					<td>0..1</td>
					<td>String</td>
					<td>ученое звание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>scientificDegree</td>
					<td>0..1</td>
					<td>String</td>
					<td>ученая степень</td>
				</tr>
				<tr>
					<td>/</td>
					<td>sex</td>
					<td>0..1</td>
					<td>Int</td>
					<td>Пол (0 - женский, 1 - мужской)</td>
				</tr>
				<tr>
					<td>/</td>
					<td>description</td>
					<td>0..1</td>
					<td>String</td>
					<td>Описание врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>educations</td>
					<td>0..1</td>
					<td>Container</td>
					<td>Образование</td>
				</tr>
				<tr>
					<td>/educations/</td>
					<td>education</td>
					<td>0..*</td>
					<td>Container</td>
					<td>Учебное заведение</td>
				</tr>
				<tr>
					<td>/educations/education/</td>
					<td>name</td>
					<td>1..1</td>
					<td>String</td>
					<td>Название учебного заведения</td>
				</tr>
				<tr>
					<td>/educations/education/</td>
					<td>type</td>
					<td>1..1</td>
					<td>String</td>
					<td>Вид образовательной программы</td>
				</tr>
				<tr>
					<td>/educations/education/</td>
					<td>year</td>
					<td>0..1</td>
					<td>Int</td>
					<td>Год выпуска</td>
				</tr>
			</table>
			<h3>Получение связанных ресурсов: приемы врача</h3>
			<p>
				GET https://emportal.ru/api/v1/doctors/<?= htmlspecialchars('<id>') ?>/receptions
			</p>
			<h4>Поля запроса</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>json</td>
					<td>0..1</td>
					<td>Int [0..1]</td>
					<td>определяет, будет ли ответ возвращен в формате json</td>
				</tr>
				<tr>
					<td>/</td>
					<td>date</td>
					<td>0..1</td>
					<td>Date</td>
					<td>день приема в формате Y-m-d, если не задан - устанавливается сегодняшним днем</td>
				</tr>
			</table>
			<h4>Поля ответа</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>datetime</td>
					<td>1..1</td>
					<td>Datetime</td>
					<td>время начала приема в формате Y-m-d H:i</td>
				</tr>
				<tr>
					<td>/</td>
					<td>addressId</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор адреса, в котором может состояться данный прием</td>
				</tr>
			</table>
			<h3>Получение связанных ресурсов: отзывы о враче</h3>
			<p>
				GET https://emportal.ru/api/v1/doctors/<?= htmlspecialchars('<id>') ?>/reviews
			</p>
			<h4>Поля запроса</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>json</td>
					<td>0..1</td>
					<td>Int [0..1]</td>
					<td>определяет, будет ли ответ возвращен в формате json</td>
				</tr>
			</table>
			<h4>Поля ответа</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>id</td>
					<td>1..1</td>
					<td>Int</td>
					<td>уникальный идентификатор отзыва</td>
				</tr>
				<tr>
					<td>/</td>
					<td>addressId</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор адреса, связанного с отзывом</td>
				</tr>
				<tr>
					<td>/</td>
					<td>createDate</td>
					<td>1..1</td>
					<td>Datetime</td>
					<td>время создания отзыва в формате Y-m-d H:i</td>
				</tr>
				<tr>
					<td>/</td>
					<td>authorName</td>
					<td>0..1</td>
					<td>String</td>
					<td>имя автора отзыва</td>
				</tr>
				<tr>
					<td>/</td>
					<td>text</td>
					<td>0..1</td>
					<td>String</td>
					<td>текст отзыва</td>
				</tr>
				<tr>
					<td>/</td>
					<td>ratingDoctor</td>
					<td>1..1</td>
					<td>Float [0..1]</td>
					<td>оценка врача (0 - наименьшая оценка, 1 - наивысшая оценка)</td>
				</tr>
				<tr>
					<td>/</td>
					<td>ratingClinic</td>
					<td>1..1</td>
					<td>Float [0..1]</td>
					<td>оценка клиники (0 - наименьшая оценка, 1 - наивысшая оценка)</td>
				</tr>
			</table>
			<h2>Возможные приемы врача</h2>
			<h4>Добавление ресурса</h4>
			<p>
				POST https://emportal.ru/api/v1/receptions
			</p>
			<h4>Поля запроса</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>receptions</td>
					<td>1..1</td>
					<td>Container</td>
					<td>приемы врача</td>
				</tr>
				<tr>
					<td>/receptions/</td>
					<td>doctorId</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор врача</td>
				</tr>
				<tr>
					<td>/receptions/</td>
					<td>addressId</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор адреса</td>
				</tr>
				<tr>
					<td>/receptions/</td>
					<td>datetime</td>
					<td>1..1</td>
					<td>Datetime</td>
					<td>время начала приема врача в формате Y-m-d H:i</td>
				</tr>
			</table>
			<h4>Поля ответа</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>success</td>
					<td>1..1</td>
					<td>Boolean</td>
					<td>сообщение об успешности проведенной операции</td>
				</tr>
				<tr>
					<td>/</td>
					<td>addressId</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор адреса</td>
				</tr>
				<tr>
					<td>/</td>
					<td>doctorId</td>
					<td>0..1</td>
					<td>String</td>
					<td>уникальный идентификатор врача</td>
				</tr>
				<tr>
					<td>/</td>
					<td>datetime</td>
					<td>0..1</td>
					<td>Datetime</td>
					<td>время начала добавленного приема в формате Y-m-d H:i</td>
				</tr>
				<tr>
					<td>/</td>
					<td>errors</td>
					<td>0..1</td>
					<td>Container</td>
					<td>пояснения возникших ошибок</td>
				</tr>
			</table>
			<p>
				<b>Внимание! При добавлении приемов на определенную дату все предыдущие приемы врача 
				<br>в соотв. адресе на эту дату удаляются.</b>
			</p>
            <h2>Записи на прием</h2>
			<h4>Добавление ресурса</h4>
			<p>
				POST https://emportal.ru/api/v1/appointments
			</p>
			<h4>Поля запроса</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>appointments</td>
					<td>1..1</td>
					<td>Container</td>
					<td>записи на прием</td>
				</tr>
				<tr>
					<td>/appointments/</td>
					<td>addressId</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор адреса</td>
				</tr>
				<tr>
					<td>/appointments/</td>
					<td>patient</td>
					<td>1..1</td>
					<td>Container</td>
					<td>пациент</td>
				</tr>
                <tr>
					<td>/appointments/patient</td>
					<td>name</td>
					<td>1..1</td>
					<td>String</td>
					<td>ФИО пациента</td>
				</tr>
                <tr>
					<td>/appointments/patient</td>
					<td>phone</td>
					<td>1..1</td>
					<td>String</td>
					<td>Телефон пациента c кодом страны и города</td>
				</tr>
                <tr>
					<td>/appointments/</td>
					<td>doctorId</td>
					<td>0..1</td>
					<td>String</td>
					<td>уникальный идентификатор врача</td>
				</tr>
                <tr>
					<td>/appointments/</td>
					<td>sourcePageUrl</td>
					<td>0..1</td>
					<td>String</td>
					<td>URL страницы, с которой произошла запись</td>
				</tr>
				<tr>
					<td>/appointments/</td>
					<td>datetime</td>
					<td>0..1</td>
					<td>Datetime</td>
					<td>время начала приема врача в формате Y-m-d H:i. Если время не указано, запись считается заявкой (лидом) и в дальнейшем время будет уточнено клиникой.</td>
				</tr>
			</table>
			<h4>Поля ответа</h4>
			<table>
				<tr>
					<td>Контейнер</td>
					<td>Параметры</td>
					<td>Обязательность</td>
					<td>Тип</td>
					<td>Описание</td>
				</tr>
				<tr>
					<td>/</td>
					<td>success</td>
					<td>1..1</td>
					<td>Boolean</td>
					<td>сообщение об успешности проведенной операции</td>
				</tr>
				<tr>
					<td>/</td>
					<td>id</td>
					<td>1..1</td>
					<td>String</td>
					<td>уникальный идентификатор записи на прием</td>
				</tr>
				<tr>
					<td>/</td>
					<td>errors</td>
					<td>0..1</td>
					<td>Container</td>
					<td>пояснения возникших ошибок</td>
				</tr>
			</table>
			<h1>Примеры работы с API</h1>
			<p>
				Пример использования API внешним сервисом доступен по ссылке ниже.
				<br>Для запуска примера потребуется <a href="http://php.net/manual/ru/install.php" rel="nofollow">PHP интерпретатор</a> с установленной библиотекой <a href="http://php.net/manual/ru/book.curl.php" rel="nofollow">cURL</a>.
			</p>
			<p>
				Скачать <a href="/uploads/documentation/api/Example.rar">Example.rar</a>
			</p>
		</div>
	</div>