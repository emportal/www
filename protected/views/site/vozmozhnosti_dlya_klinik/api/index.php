<?php
$this->pageTitle = 'API Единого Медицинского Портала';
$this->breadcrumbs=array(
		'API Единого Медицинского Портала',
);
Yii::app()->clientScript->registerMetaTag('Дополнительные возможности для клиник-партнеров Единого Медицинского Портала.', 'description');
?>
<style>
<!--
.vozmozhnostiApi_first_screen {
    display: block;
    width: 100%;
    margin: 0;
    z-index: 155;
    background: url(/images/vozmozhnosti/api/bg_top.jpg) #F7F7F7 50% no-repeat;
    -ms-filter: "progid:DXImagedivansform.Microsoft.AlphaImageLoader(src='/images/vozmozhnosti/api/bg_top.jpg', sizingMethod='scale')";
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
.vozmozhnostiApi_btn-start {
	width: 445px;
    height: 61px;
    font: 21px/25px 'Roboto', sans-serif;
    font-weight: 400;
    cursor: pointer;
    border: 0;
    background: rgb(141,198,63);
    box-sizing: border-box;
    word-spacing: normal;
    vertical-align: top;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    color: #fff !important;
    text-decoration: none;
    text-align: center;
    text-shadow: 0 0 0 !important;
    text-transform: none;
    white-space: normal !important;
    display: block;
    position: relative;
}
.vozmozhnostiApi_slogan_long {
    font: 16px/24px 'Roboto', sans-serif;
    font-weight: 400;
    color: white;
}
.vozmozhnostiApi_first_column {
	min-height:464px;
}
.vozmozhnostiApi_icons_bottom {
	width: 100%;
	min-height: 384px;
    background: #f8f8f8;
    text-align: center;
}
.vozmozhnostiApi_actions_screen {
    padding: 0px 0px 25px 0px;
    background: #f8f8f8;
}
.vozmozhnostiApi_actions_header_section {
    text-align: center;
    padding: 25px 0px 30px 0px;
}
.vozmozhnostiApi_actions_header_section h2 {
    font-size: 24px;
    color: #005061;
}

.vozmozhnostiApi_action_block {
    width: 25%;
    float: left;
    text-align: center;
}
.vozmozhnostiApi_action_header {
    font-size: 18px;
    line-height: 21px;
    font-weight: normal;
    text-align: left;
    width: 253px;
    color: #4f4f4f;
}
.vozmozhnostiApi_action_article {
    font-size: 16px;
    color: #4f4f4f;
    text-align: center;
}
.vozmozhnostiApi_review_screen {
    display: block;
    width: 100%;
    min-height: 550px;
    margin: 0;
    z-index: 155;
    background: url(/images/vozmozhnosti_bg_review.jpg) #FFF 50% no-repeat;
    -ms-filter: "progid:DXImagedivansform.Microsoft.AlphaImageLoader(src='/images/vozmozhnosti_bg_review.jpg', sizingMethod='scale')";
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position-y: 0;
}
.vozmozhnostiApi_icon_check {
    width: 32px;
    height: 32px;
    background: url(/images/vozmozhnosti/api/icons/check.png) no-repeat center;
    position: absolute;
    display: block;
    margin: 0px 0px 0px -7px;
}
.vozmozhnostiApi_layout_action_block {
    width: 50%;
    text-align: center;
    vertical-align: text-top;
    float: left;
}
.vozmozhnostiApi_warning_screen {
    display: block;
    width: 100%;
    min-height: 346px;
    margin: 0;
    z-index: 155;
    background: url(/images/vozmozhnosti/api/warning_screen.jpg) #f8f8f8 50% no-repeat;
    background-size: 1200px;
    background-position-y: 0;
}
-->
</style>
<?php $this->renderPartial('//layouts/_header'); ?>
<div class="SECTION crumbs_section">
	<div class="main_column">
		<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs, 'htmlOptions' => [ 'class' => 'crumbs' ] )); ?>
	</div>
</div>
<div class="SECTION vozmozhnostiApi_first_screen">
	<div class="main_column vozmozhnostiApi_first_column">
		<h1 style="font-size: 48px;font-weight: bold;text-align: center;color: #3ec1d9;padding: 70px 0 5px 0;">
			Хотите еще больше пациентов?
		</h1>
		<p class="vozmozhnostiApi_slogan_long" style="margin: 15px 0px 0px 455px;font-size: 36px;font-weight: bold;color: #4f4f4f;">
			Интегрируйтесь!
		</p>
		<p class="vozmozhnostiApi_slogan_long" style="margin: 150px 20px 0px 0px;text-align: right;float: right;">
			Интеграция с emportal.ru позволяет обеспечить 
			<br>автоматическое обновление информации 
			<br>на страницах клиник и показывать 
			<br>актуальное расписание приема врачей!
		</p>
	</div>
</div>
<div class="SECTION vozmozhnostiApi_icons_bottom">
	<div class="main_column">
		<div class="vozmozhnostiApi_actions_header_section">
			<h2>Преимущества интеграции с ЕМП</h2>
		</div>
		<div class="vozmozhnostiApi_action_block">
			<img src="/images/vozmozhnosti/api/icons/calendrier.png" style="height:80px;">
			<p class="vozmozhnostiApi_action_article">
				<span class="vozmozhnostiApi_icon_check"></span>
				Клиенты клиники видят
				<br>актуальное расписание
				<br>и могут сами выбрать
				<br>подходящее им время
			</p>
		</div>
		<div class="vozmozhnostiApi_action_block">
			<img src="/images/vozmozhnosti/api/icons/clock.png" style="height:80px;">
			<p class="vozmozhnostiApi_action_article">
				<span class="vozmozhnostiApi_icon_check"></span>
				Пациент хочет попасть в
				<br>клинику «здесь и сейчас».
				<br>Благодаря интеграции они
				<br>смогут видеть ближайшее
				<br>время и Вы получите
				<br>максимальную загрузку
				<br>кабинетов.
			</p>
		</div>
		<div class="vozmozhnostiApi_action_block">
			<img src="/images/vozmozhnosti/api/icons/phone24.png" style="height:80px;">
			<p class="vozmozhnostiApi_action_article">
				<span class="vozmozhnostiApi_icon_check"></span>
				Нагрузка на Call-центр
				<br>клиники снизится - ведь
				<br>можно не звонить пациенту!
			</p>
		</div>
		<div class="vozmozhnostiApi_action_block">
			<img src="/images/vozmozhnosti/api/icons/patients.png" style="height:80px;">
			<p class="vozmozhnostiApi_action_article">
				<span class="vozmozhnostiApi_icon_check"></span>
				Увеличение лояльности
				<br>пациентов и привлечение
				<br>продвинутой платежеспособной
				<br>аудитории
			</p>
		</div>
	</div>
</div>
<div class="SECTION vozmozhnostiApi_warning_screen">
	<div class="main_column">
		<div style="">
			<div style="width: 300px;height: 100%;display: table-cell;vertical-align: middle;">
			</div>
			<div style="display: table-cell;vertical-align: middle;padding: 0 0px 0 65px;">
				<p style="font-size: 24px;color: #005061;min-height: 50px;margin: 0px;padding: 18px 0px 0px 0px;">и снова:</p>
				<p style="font-size: 34px;color: white;margin: 60px 0px 0px 90px;">Это абсолютно бесплатно!</p>
				<p style="font-size: 16px;color: white;margin: 0px 0px 0px 90px;">
				<br>Вы не платите за интеграцию с ЕМП. 
				<br>Все записи тарифицируются по вашему стандартному тарифу!</p>
			</div>
		</div>
	</div>
</div>
<div class="SECTION vozmozhnostiApi_actions_screen">
	<div class="main_column">
		<div class="vozmozhnostiApi_actions_header_section" style="padding: 80px 0px 20px 0px;">
			<h2 style="font-size: 48px;font-weight: bold;color: #04b0cf;">
				Как интегрироваться с ЕМП
			</h2>
			<p style="margin: 25px auto 25px auto;text-align: center;font-size: 18px;color: #4f4f4f;;">
				Мы привлекаем новых пациентов без каких-либо затрат с Вашей стороны!
				<br>
				<span style="font-size: 24px;">
					Вы оплачиваете только
					<span style="font-weight: bold;text-transform: uppercase;color: #04b0cf;">
						результат!
					</span>
				</span>
			</p>
		</div>
		<div style="display: inline-block; width: 100%;">
			<div class="vozmozhnostiApi_layout_action_block">
				<table style="margin: 0px 75px 0px auto;">
					<tbody>
						<tr>
							<td colspan=2>
								<img src="/images/vozmozhnosti/api/icons/partner.png">
							</td>
						</tr>
						<tr>
							<td style="width: 40px;font-size: 50px;font-weight: bold;vertical-align: middle;color: #c8c8c8;text-align: left;">
								1
							</td>
							<td>
								<h3 class="vozmozhnostiApi_action_header">
									Станьте партнером ЕМП если еще не являетесь
								</h3>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<a href="/site/contact" target="blank"><button class="btn-green btn-eld" title="Зарегистрировать клинику" style="width: 300px;padding: 19px 0px 19px 0px;margin: 25px 0 0 0;font-size: 18px;">
									Зарегистрировать клинику
								</button></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="vozmozhnostiApi_layout_action_block">
				<table style="margin: 0px auto 0px 75px;">
					<tbody>
						<tr>
							<td colspan=2>
								<img src="/images/vozmozhnosti/api/icons/tech.png">
							</td>
						</tr>
						<tr>
							<td style="width: 40px;font-size: 50px;font-weight: bold;vertical-align: middle;color: #c8c8c8;text-align: left;">
								2
							</td>
							<td>
								<h3 class="vozmozhnostiApi_action_header">
									Отправьте Вашему ИТ отделу ссылку на эту страницу
								</h3>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<a href="/vozmozhnosti_dlya_klinik/api/documentation.html" target="blank"><button class="btn-green btn-eld" title="Посмотреть техническую документацию" style="width: 300px;padding: 10px 0px 10px 0px;margin: 25px 0 0 0;font-size: 18px;">
									Посмотреть 
									<br>техническую документацию
								</button></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div style="font-size: 18px;font-weight: bold;color: #04b0cf;text-align: center;padding: 40px 15px 40px 15px;">
			Все! Теперь и Ваши пациенты могут  записываться онлайн!
		</div>
	</div>
</div>
<!-- 
<div class="SECTION vozmozhnostiApi_review_screen">
	<div class="main_column">
		<div style="min-height: 150px;">
			<h2 style="font-size: 30px; text-align: center; padding: 35px 0px 5px 0px;">
				Первые клиники уже интегрированы.
			</h2>
			<p style="text-align: center;font-size: 18px;font-weight: bold;color: #005061;">Вот что они говорят:</p>
		</div>
		<p style="font-size: 16px; line-height: 24px; color: black; margin: 0px 175px 0px 175px;">
			С ЕМП мы уже больше года. И самой главной проблемой всегда была конверсия из заявок не дошедших пациентов
			- очень много заявок не поддерживалось, потому что на сайте Единого Медицинского Портала не было актуального расписания.
			Как только мы интегрировались с проектом  - эта конверсия исчезла! <b>Пациент записывается сразу на свободное время  и он точно знает,
			<br>что попадет в клинику!</b> Это замечательно!  Плюс пациенту не всегда удобно звонить - и он записывается онлайн на свободное время! 
		</p>
		<div class="vozmozhnostiApi_review_block" style="text-align: center;">
			<table style="margin: auto;"><tbody><tr>
				<td style="vertical-align: middle;">
					<div class="vozmozhnostiApi_review_leftButton displaynone" style="cursor: pointer; width: 50px; height: 50px; background: url(/images/drop_left_butt.png) white no-repeat center;border-radius: 50%;"></div>
				</td>
				<td style="vertical-align: middle;">
					<div class="vozmozhnostiApi_review_authorAvatar" style="padding: 10px 30px 10px 30px;">
						<img src="/images/vozmozhnosti/api/icons/review-authorAvatar.png">
					</div>
				</td>
				<td style="vertical-align: middle;">
					<div class="vozmozhnostiApi_review_rightButton displaynone" style="cursor: pointer; width: 50px; height: 50px; background: url(/images/drop_right_butt.png) white no-repeat center; border-radius: 50%;"></div>
				</td>
			</tr></tbody></table>
			<div class="vozmozhnostiApi_review_authorName" style="font-size: 18px;">
				Петя Катина,
			</div>
			<div class="vozmozhnostiApi_review_authorSpecilty" style="font-size: 15px; font-weight: bold; color: #005061; text-transform: uppercase; margin: 10px 0px 8px 0px;">
				МЕнеджер по рекламе
			</div>
			<a href="//emportal.ru/klinika/evromed-klinik">
				<div class="vozmozhnostiApi_review_authorCompany" style="font-size: 18px; font-weight: bold; color: #005061; text-decoration: underline; text-align: center;">
					Клиника «Евромед»
				</div>
			</a>
		</div>
	</div>
</div>
-->
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>