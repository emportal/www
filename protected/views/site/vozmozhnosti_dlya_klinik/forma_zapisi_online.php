<?php
$this->pageTitle = 'Возможности для клиник-партнеров Единого Медицинского Портала';
Yii::app()->clientScript->registerMetaTag('Дополнительные возможности для клиник-партнеров Единого Медицинского Портала.', 'description');
?>
<style>
<!--
.vozmozhnosti_first_screen {
    display: block;
    width: 100%;
    margin: 0;
    z-index: 155;
    background: url(/images/vozmozhnosti_bg_top.jpg) #F7F7F7 50% no-repeat;
    -ms-filter: "progid:DXImagedivansform.Microsoft.AlphaImageLoader(src='/images/vozmozhnosti_bg_top.jpg', sizingMethod='scale')";
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
.vozmozhnosti_btn-start {
	width: 445px;
    height: 61px;
    font: 21px/25px 'Roboto', sans-serif;
    font-weight: 400;
    cursor: pointer;
    border: 0;
    background: rgb(141,198,63);
    box-sizing: border-box;
    word-spacing: normal;
    vertical-align: top;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    color: #fff !important;
    text-decoration: none;
    text-align: center;
    text-shadow: 0 0 0 !important;
    text-transform: none;
    white-space: normal !important;
    display: block;
    position: relative;
}
.vozmozhnosti_slogan_long {
	font: 18px/30px 'Roboto', sans-serif;
}
.vozmozhnosti_first_column {
	min-height:550px;
}
.vozmozhnosti_icons_bottom {
	width: 100%;
    background: #f8f8f8;
    text-align: center;
}
.vozmozhnosti_actions_screen {
    padding: 0px 0px 25px 0px;
}
.vozmozhnosti_actions_header_section {
    text-align: center;
    padding: 35px 0px 0px 0px;
}
.vozmozhnosti_action_block {
    width: 25%;
    float: left;
    text-align: left;
}
.vozmozhnosti_action_header {
    font-size: 16px;
    line-height: 21px;
    font-weight: normal;
    text-align: left;
    width: 210px;
}
.vozmozhnosti_action_article {
	font-size: 14px;
    color: black;
    text-align: left;
    margin: 15px 6px 5px 0px;
}
.vozmozhnosti_review_screen {
    display: block;
    width: 100%;
    min-height: 490px;
    margin: 0;
    z-index: 155;
    background: url(/images/vozmozhnosti_bg_review.jpg) #FFF 50% no-repeat;
    -ms-filter: "progid:DXImagedivansform.Microsoft.AlphaImageLoader(src='/images/vozmozhnosti_bg_review.jpg', sizingMethod='scale')";
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position-y: 0;
}
-->
</style>
<?php $this->renderPartial('//layouts/_white_head'); ?>
<div class="SECTION vozmozhnosti_first_screen">
	<div class="main_column vozmozhnosti_first_column">
		<table style="width: 100%;">
		<tr>
			<td>
				<a href="/" title="Единый Медицинский Портал">
					<img src="/images/newLayouts/LOGO-MAIN-DARK.png" alt="Единый Медицинский Портал" class="LOGO_MAIN" style="margin-top: 25px;">
				</a>
				<div style="padding: 10px 10px 10px 10px; width: 600px;">
					<h2 style="font-size: 30px;margin: 40px 0px 15px 0px;">Форма онлайн-записи на приём<br>для вашего сайта!</h2>
					<p class="vozmozhnosti_slogan_long" style="margin: 15px 0px 20px 0px;">Установите нашу форму для онлайн-записи на приём на сайт<br>и получайте записи на приём в своём личном кабинете</p>
					<div class="vozmozhnosti_btn-start_container">
						<button class="btn-green vozmozhnosti_btn-start" type="submit" onclick="countersManager.reachGoal({'forCounters' : ['yandex'], 'goalName' : 'vozmozhnosti_dlya_klinik_forma_zapisi_online', 'callback': function(){document.location.href = '/adminClinic/default/etc';}});" title="Нажмите, чтобы установить">Установить форму на свой сайт</button>
					</div>
					<p class="vozmozhnosti_slogan_long" style="margin: 10px 0px 10px 0px;">Это абсолютно бесплатно!</p>
				</div>
			</td>
			<td>
				<div style="margin: 10px 0px 10px 10px;">
					<span id="empAppFormSpan"></span>
					<script>
					(function(o) {
					var r = new XMLHttpRequest();
					r.onreadystatechange = function(data) {
					if (r.readyState == 4 && r.status == 200) {
					var insertSpan = document.getElementById("empAppFormSpan");
					insertSpan.innerHTML = r.responseText;
					var insertedScript = insertSpan.getElementsByTagName('script')[0];
					eval(insertedScript.innerHTML);
					}
					}
					var params = '?id=' + o.company;
					if (o.appType)
					params += '&at=' + o.appType;
					if (o.address) params += '&a=' + o.address;
					r.open('GET', (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//' + o.source + params, true);
					r.send();
					})({
					'source': 'emportal.ru/ajax/getAppForm',
					'company': 'klinika-doktora-voyta',
					'address': 'furshtatskaya-20'
					})
					</script>
				</div>
			</td>
		</tr>
		</table>
	</div>
</div>
<div class="SECTION vozmozhnosti_icons_bottom">
	<div class="main_column">
		<div class="vozmozhnosti_actions_header_section">
			<h2 style="font-size: 24px; color: #003C61;">Преимущества нашей формы</h2>
		</div>
		<div class="vozmozhnosti_action_block">
			<img src="/images/icons/vozmozhnosti_icon_check.png" style="">
			<p class="vozmozhnosti_action_article">Клиенты получат возможность записываться онлайн на вашем сайте</p>
		</div>
		<div class="vozmozhnosti_action_block">
			<img src="/images/icons/vozmozhnosti_icon_shape_2.png" style="">
			<p class="vozmozhnosti_action_article">Администраторы клиники смогут работать с расписанием и заявками пациентов в едином удобном интерфейсе</p>
		</div>
		<div class="vozmozhnosti_action_block">
			<img src="/images/icons/vozmozhnosti_icon_mobi.png" style="">
			<p class="vozmozhnosti_action_article">Клиент будет получать уведомления о статусе заявки по смс и электронной почте, также как и при записи через Единый Медицинский портал</p>
		</div>
		<div class="vozmozhnosti_action_block">
			<img src="/images/icons/vozmozhnosti_icon_shape_4.png" style="">
			<p class="vozmozhnosti_action_article">Увеличение лояльности пациентов и конверсии в запись на вашем сайте</p>
		</div>
		<table style="margin: 20px 45px 30px 0px;display: inline-block;">
		<tbody><tr>
			<td style="vertical-align: middle; padding: 0px 25px 0px 0px;">
				<img src="/images/icons/vozmozhnosti_icon_pig.png" style="">
			</td>
			<td>
				<div style="font-size: 18px; font-weight: bold; color: #005061;text-align: center;">И самое важное это абсолютно бесплатно!</div>
				<p style="font-size: 14px; color: black; text-align: left; margin: 5px 5px 5px 1px;">Вы не платите ни за использование формы,<br>ни за пациентов, записавшихся через неё</p>
			</td>
		</tr>
		</tbody></table>
	</div>
</div>
<div class="SECTION vozmozhnosti_actions_screen">
	<div class="main_column">
		<div class="vozmozhnosti_actions_header_section">
			<h2 style="font-size: 30px;">Как начать использовать бесплатную<br>форму онлайн-записи на приём</h2>
			<p style="margin: 8px auto 25px auto; text-align: center; font-size: 14px; width: 530px;color: black;">Мы привлекаем новых пациентов без каких-либо затрат с вашей стороны!<br>Вы оплачиваете только результат</p>
		</div>
		<div class="layout_action_block">
			<table style="margin: 0px auto 10px auto;">
				<tbody><tr>
					<td style="width: 40px; font-size: 48px; vertical-align: middle; color: #eeeeee;text-align: left;">
						1
					</td>
					<td>
						<h3 class="vozmozhnosti_action_header">Авторизуйтесь в личном кабинете клиники на сайте</h3>
					</td>
					<td style="width:20px;"></td>
				</tr>
			</tbody></table>
			<img src="/images/icons/vozmozhnosti_layer_2.png" style="min-width: 208px; min-height: 208px;">
		</div>
		<div class="layout_action_block">
			<table style="margin: 0px auto 10px auto;">
				<tbody><tr>
					<td style="width: 40px; font-size: 48px; vertical-align: middle; color: #eeeeee;text-align: left;">
						2
					</td>
					<td>
						<h3 class="vozmozhnosti_action_header">В разделе «Форма записи» скопируйте код формы</h3>
					</td>
					<td style="width:20px;"></td>
				</tr>
			</tbody></table>
			<img src="/images/icons/vozmozhnosti_layer_3.png" style="min-width: 208px; min-height: 208px;">
		</div>
		<div class="layout_action_block">
			<table style="margin: 0px auto 10px auto;">
				<tbody><tr>
					<td style="width: 40px; font-size: 48px; vertical-align: middle; color: #eeeeee;text-align: left;">
						3
					</td>
					<td>
						<h3 class="vozmozhnosti_action_header">Вставьте код формы на ваш сайт</h3>
					</td>
					<td style="width:20px;"></td>
				</tr>
			</tbody></table>
			<img src="/images/icons/vozmozhnosti_layer_4.png" style="min-width: 208px; min-height: 208px;">
		</div>
		<div style="font-size: 18px; font-weight: bold; color: #005061;text-align: center;padding: 30px 15px 15px 15px;">Всё! Теперь и ваши пациенты могут записываться онлайн!</div>
	</div>
</div>

<div class="SECTION vozmozhnosti_review_screen">
	<div class="main_column">
		<div>
			<h2 style="font-size: 30px; text-align: center; padding: 35px 0px 30px 0px;">Отзывы об онлайн-форме</h2>
		</div>
		<p style="font-size: 16px; line-height: 24px; color: black; margin: 0px 175px 0px 175px;">
			Установили форму записи к нам на сайт, так как доверяем ЕМП и успешно с ними работаем. Примерно через неделю после установки люди начали записываться, при этом нередко отмечают, что это действительно удобно. Признаться, не ожидали такого эффекта. 
			<br>Установка формы прошла как по маслу, ничего не глючит, всё работает как часы. Посоветовал бы её тем клиникам, у которых нет собственного интерфейса записи – пациентам будет куда удобнее записываться на приём.
		</p>
		<div class="vozmozhnosti_review_block" style="text-align: center;">
			<table style="margin: auto;"><tbody><tr>
				<td style="vertical-align: middle;">
					<div class="vozmozhnosti_review_leftButton displaynone" style="cursor: pointer; width: 50px; height: 50px; background: url(/images/drop_left_butt.png) white no-repeat center;border-radius: 50%;"></div>
				</td>
				<td style="vertical-align: middle;">
					<div class="vozmozhnosti_review_authorAvatar" style="padding: 10px 30px 10px 30px;">
						<!--<img src="/images/vozmozhnosti_review_authorAvatar.png">-->
					</div>
				</td>
				<td style="vertical-align: middle;">
					<div class="vozmozhnosti_review_rightButton displaynone" style="cursor: pointer; width: 50px; height: 50px; background: url(/images/drop_right_butt.png) white no-repeat center; border-radius: 50%;"></div>
				</td>
			</tr></tbody></table>
			<div class="vozmozhnosti_review_authorName" style="font-size: 18px;">Воловьев А.А.</div>
			<div class="vozmozhnosti_review_authorSpecilty" style="font-size: 15px; font-weight: bold; color: #005061; text-transform: uppercase; margin: 10px 0px 8px 0px;">Директор клиники</div>
			<a href="//nizhny-novgorod.emportal.ru/klinika/alibus-dent/stragh-revolyucii-84_1">
				<div class="vozmozhnosti_review_authorCompany" style="font-size: 18px; font-weight: bold; color: #005061; text-decoration: underline; text-align: center;">
					«Алибус Дент»
				</div>
			</a>
		</div>
	</div>
</div>

<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>