<?php
/* @var $this Controller */
$this->breadcrumbs = array(
	'Вход на сайт',
);
?>
<div class="content">
	<?php if ( Yii::app()->user->hasFlash('activate') ): ?>
		<p><?= Yii::app()->user->getFlash('activate') ?></p>
	<?php else: ?>
		<?php if ( Yii::app()->user->hasFlash('registerSuccess') ): ?>
		<p style="text-align: center;"><?= Yii::app()->user->getFlash('registerSuccess') ?></p>
		<?php endif; ?>
		<?php $this->widget('UserLogin', array('onlyForm' => true, 'visible' => Yii::app()->user->isGuest, 'id' => 'userLoginPage')); ?>
	<? endif; ?>
</div>