<?php
$this->breadcrumbs = array(
	'Восстановление пароля',
);

Yii::app()->clientScript->registerScript('login-page', '
	$("#recovery-form").submit(function() {
		var err_block = $("#err");
		var key=$("#key");
		var er=0;
		
		err_block.html("");
		err_block.removeClass("error");
		key.removeClass("error");
		
		
		if(key.val()=="") {
			er = 1;
		}
		if(er) {
			err_block.html("Необходимо заполнить поле Код активации");
			key.addClass("error");
			err_block.addClass("error");
			return false;
		}
	});
	var time,sec,min;
	$(document).ready(function() {
		if($(".time").length) {
			min = parseInt($(".time").html().split(":")[0]);
			sec = parseInt($(".time").html().split(":")[1]);

			function tick() {
				if(sec == 0 && min == 0) {
					$(".remain_button").show();
					return false;
				}
				if(sec < 1) {
					min--;
					sec = 59;
				} else {
					sec--;
				}
				time = min + ":" + (sec < 10 ? "0"+sec : sec);
				$(".time").html(time);
			}

			setInterval(tick,1000);
			$(".remain_button").click(function() {
				location.href = "/recoveryByPhone";
			});
		}
	});
	
', CClientScript::POS_READY)
?>
<div class="content">
	<div class="cont">
		<div class="middle-auth w800">
			<?php //$form = $this->beginWidget('CActiveForm', array('id' => 'recovery-form'));  ?>
			<?php
			$form = $this->beginWidget('CActiveForm', array(
				'method' => 'post',
				'id' => 'recovery-form',
				'action' => '/recoveryByPhone',
				'enableAjaxValidation' => true,
				'enableClientValidation' => FALSE,
				'htmlOptions' => array('class' => 'recovery', 'enctype' => 'multipart/form-data'),
				'clientOptions' => array(
					'validateOnSubmit' => true,
					'inputContainer' => 'td',
				),
			));
			?>
			<?php if (Yii::app()->user->hasFlash('recoveryByPhone')): ?>
				<p><?= Yii::app()->user->getFlash('recoveryByPhone') ?></p>
			<?php endif; ?>
				<fieldset>
					<div class="row">
						<?php if(Yii::app()->session['recoveryByPhone']['isSent']):?>
							<p class="size-16 lh26">Введите код подтверждения. </p>							
							<?php echo CHtml::textField('key','',['class' => 'custom-text '.($errors?'error':'')]); ?>
							<input id="recovery-button" type="submit" class="btn" name="action" value="Отправить"><br />
							<div class="error" id="err">
								<?php if($errors): ?>
									<?php foreach($errors as $error): ?>
										<?=$error?><br/>
									<?php endforeach;?>
								<?php endif; ?>
							</div>		
							<?php if($remain): ?>
							<span class="remaining_time">Получить код заново можно будет через <span class="time"><?=$remain?></span></span>
							<div style="display:none;" class="remain_button btn-green">получить код</div>
							<?php endif; ?>
						<?php else: ?>
							<p class="size-16 lh26">Укажите номер телефона, для которого вы хотите восстановить пароль.</p>												
							<?php $this->widget('CMaskedTextField', array(
								'model' => $model,
								'attribute' => 'telefon',
								'mask' => '+79999999999',
								'placeholder' => '_',
								'htmlOptions' => array('class' => 'custom-text'),
							));	?>			
							<input id="recovery-button" type="submit" class="btn" name="action" value="Отправить"><br />
						<?php endif; ?>		
						<?php echo $form->error($model, 'telefon'); ?>		
							
					</div><!-- /row -->
				</fieldset>
			
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div><!-- form -->