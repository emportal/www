<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2>Error <?php echo $code; ?></h2>

<div class="error">
<?php echo CHtml::encode($message); ?>
</div>

<br/><button id="callBackButton" class="btn btn-red" style="display: none; width: 100px;" onclick="callBackUrl(); return false;">Назад</button>

<script>
	var getParameterByName = (function(a) {
	    if (a == "") return {};
	    var b = {};
	    for (var i = 0; i < a.length; ++i)
	    {
	        var p=a[i].split('=', 2);
	        if (p.length == 1)
	            b[p[0]] = "";
	        else
	            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	    }
	    return b;
	})(window.location.search.substr(1).split('&'));

	if(getParameterByName['callback']) {
		$("#callBackButton")[0].style.display = "";
	}
	
	function callBackUrl() {
		if(getParameterByName['callback']) {
			location.href = getParameterByName['callback'];
		}
	}
</script>