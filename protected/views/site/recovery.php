<?php
$this->breadcrumbs = array(
	'Восстановление пароля',
);
?>
<div class="seaction">
<div class="wrapper">
	<div class="recovery-box vmargin20">
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'recovery-form',
			'action' => '/recovery',
			'enableAjaxValidation' => true,
			'enableClientValidation' => FALSE,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'inputContainer' => 'td',
			),
		));
		?>
		<?php if (Yii::app()->user->hasFlash('recovery')): ?>
		<p>
			<?= Yii::app()->user->getFlash('recovery') ?>
		</p>
		<?php else: ?>
			<h2>E-mail</h2>
			<p class="size-16 lh26">Вы можете восстановить пароль с помощью Email.<br/>
				На указанный почтовый ящик будет выслано письмо для восстановления.</p>												
			<?php echo $form->textField($model, 'email', array('class' => 'custom-text')); ?>
			<br>
			<input id="recovery-button" type="submit" class="btn-green vmargin10" name="action" value="Отправить"><br/>
			<?php echo $form->error($model, 'email'); ?>
		<? endif; ?>
		<?php $this->endWidget(); ?>
	</div>
	<div class="recovery-box vmargin20 tmargin30">
		<?php
		$form1 = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'recovery-form',
			'action' => '/recoveryByPhone',
			'enableAjaxValidation' => true,
			'enableClientValidation' => FALSE,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'inputContainer' => 'td',
			),
		));
		?>
		<h2>Телефон</h2>
		<p class="size-16 lh26">Также вы можете восстановить пароль с помощью мобильного телефона.<br/>
			Укажите номер телефона, для которого вы хотите восстановить пароль.</p>
		<?php
		$this->widget('CMaskedTextField', array(
			'model' => $model,
			'attribute' => 'telefon',
			'mask' => '+79999999999',
			'placeholder' => '_',
			'htmlOptions' => array('class' => 'custom-text'),
		));
		?>
		<br>
		<input id="recovery-button" type="submit" class="btn-green vmargin10" name="action" value="Отправить">
		<br>
		<?php echo $form1->error($model, 'telefon'); ?>		
		<?php $this->endWidget(); ?>
	</div>
	<!--<a href="/recoveryByPhone">Восстановить пароль по номеру телефона</a>-->
</div><!-- form -->
</div>