<?php
$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>

<h1>Оставить отзыв</h1>


<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>
	<p class="note">Вы можете отправить нам свои замечания или предложения по поводу мобильного приложения через форму ниже</p>
	<p class="note">Поля, отмеченные <span class="required">*</span> обязательны к заполнению.</p>

	<?php echo $form->errorSummary($model); ?>
	<table class="feedback  search-result-table"> 

	<tr class="search-result-box">
		<td class="search-result-signup size-14"><?php echo $form->labelEx($model,'subject'); ?></td>
		<td class="search-result-info"><?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128, 'class'=>'custom-text w280')); ?></td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14"><?php echo $form->labelEx($model,'email'); ?></td>
		<td class="search-result-info"><?php echo $form->textField($model,'email', array('class'=>'custom-text w280')); ?></td>
	</div>
	<tr class="search-result-box">
		<td class="search-result-signup size-14"><?php echo $form->labelEx($model,'body'); ?></td>
		<td class="search-result-info"><?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?></td>
	</tr>

	<?php if(CCaptcha::checkRequirements()): ?>
	<tr class="search-result-box">
		<?php //echo $form->labelEx($model,'verifyCode'); ?>
		<td class="search-result-signup size-14 align-center">
		<?php $this->widget('CCaptcha', array(
			'clickableImage'=>true,
			'showRefreshButton'=>true, 
			'buttonLabel' => CHtml::image(Yii::app()->baseUrl . '/images/reload.png', '',array(
				'style'=>'border:none;width:32px',
				'alt'=>'Обновить код',
				'title'=>'Обновить код')
			),
			'imageOptions'=>array('style'=>'/*display:block;*/border:none;',
			/*'height'=>'40px',*/
			'alt'=>'Картинка с кодом валидации',
			'title'=>'Чтобы обновить картинку, нажмите на нее'))); ?>
		</td>
		<td class="search-result-info"><?php echo $form->textField($model,'verifyCode', array('class'=>'custom-text')); ?><br /><div class="hint">Пожалуйста, введите символы на картинке</div></td>
		
	</tr>
	<?php endif; ?>
									<tr class="search-result-box">
                                        <td class="search-result-signup size-14"></td> 
                                        <td class="search-result-info"><?php echo CHtml::submitButton('Отправить', array('class'=>'btn-green')); ?></td>
                                    </tr>

	</table>
<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>