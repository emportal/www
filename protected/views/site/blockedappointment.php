<?php	
/* @var $this Controller */
$this->breadcrumbs = array(
	'Активация мобильного телефона',
);
?>
<div class="content">
	<?php if ( Yii::app()->user->hasFlash('activated') ): ?>
	<div class="flash-success" style="text-align: center;"><?= Yii::app()->user->getFlash('activated') ?></div>
	<?php else: ?>
		<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'method'				 => 'post',
			'id'					 => 'reg-form',				
		));
		/* @var $form CActiveForm */
		?>
		<?php if(Yii::app()->user->hasFlash('send_code')): ?>
	<div class="flash-success" style="text-align: center;"><?= Yii::app()->user->getFlash('send_code') ?></div>
		<?php endif; ?>
		<?php if(Yii::app()->user->hasFlash("error")):?>
			<div class="flash-error" style="text-align:center"><?=Yii::app()->user->getFlash("error");?></div>
		<?php endif; ?>
		<table class="search-result-table">
			<tr class="search-result-box">
				<td class="search-result-signup size-14">
					<?php if($sent==1): ?>
						<p class="activation_text">В целях защиты от спама и подозрительной активности аккаунта просим вас ввести код присланный на ваш номер телефона: <strong><?=Yii::app()->user->model->telefon?></strong></p>
					<?php else: ?>
						<p class="activation_text">В целях защиты от спама и подозрительной активности аккаунта просим вас ввести код присланный на ваш номер телефона ранее: <strong><?=Yii::app()->user->model->telefon?></strong></p>
					<?php endif;?>
					
				</td>
				<td class="search-result-info input_td">
					<?php
					echo $form->textField($model, 'code', array('class' => 'custom-text'));
					?><br/>
					<?php echo $form->error($model, 'code');?>
					
				</td>
			</tr>		
			<tr class="search-result-box">
				<td class="search-result-signup size-14"></td>
				<td class="search-result-info">					
					<button type="submit" class="btn-green " title="Подтвердить">Подтвердить код</button>
				</td>
			</tr>
		</table>
		<?php $this->endWidget(); ?>
	<? endif; ?>
</div>