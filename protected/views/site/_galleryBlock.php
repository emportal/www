<?=
	CHtml::link(
		CHtml::image($data->getPreview()),
		$data->getUrl(),
		array(	"class"	=> "fancybox-thumb",
				"rel"	=> "fancybox-thumb",
				"title" => CHtml::encode($data->name))
	)
?>
