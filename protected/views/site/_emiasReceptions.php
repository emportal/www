<div style="text-align:right;">
	<button onclick="$('#emiasContent').stop().hide();$('#emiasContent').html('');" style="">Закрыть</button>
</div>
<?php if(isset($data->return)): ?>
	<ul id="receptions_list" class="receptions_list list-group list-my-appointments" style="list-style-type: none !important;list-style-image: none !important;">
		<?php foreach ($data->return as $item): ?>
            <li id="appointment_reception_<?=$item->id?>" class="j-appointment_reception appointment_reception um--collapsable" data-emias-appointmentid="<?=$item->id?>" data-emias-referralid="0" data-emias-referral_type="TO_DOCTOR" data-emias-specialityid="2" data-emias-viewldp="0" data-emias-availableresourceid="11166668" data-emias-complexresourceid="11089974" data-emias-receptiondate="2016-04-07T17:12:00+03:00" data-emias-starttime="2016-04-07T17:12:00+03:00" data-emias-endtime="2016-04-07T17:24:00+03:00" data-emias-receptiontypecodeorldptypecode="1161" data-emias-enableshift="1" data-emias-enablecanceleration="1">
                <div class="list-group-item-info">
                    <h5 style="margin: 20px 5px 5px 10px !important;" class="um--item_title pull-left">Запись № <b class="js-tpl_number"><?=htmlentities($item->number)?></b></h5>
                    <br style="clear: both" class="clear clearfix">
                </div>
                <div class="id--appointment_card_body list-group-item">
					<div class="col-xs-12">
						<div class="row">
							<div style="float: left;width: 200px;">
								<span class="js-tpl_date-appointment"><?= date("d.m.Y",strtotime(explode("+", $item->dateBegin)[0])) ?></span>
								<span class="week_day"><?= MyTools::getRussianDayNames()[ date("w",strtotime(explode("+", $item->dateBegin)[0])) -1 ] ?></span>
								<br><i class="fa fa-clock-o text-muted"></i> <strong class="js-tpl_time-appointment"><?= date("H:i",strtotime(explode("+", $item->dateBegin)[0])) ?></strong>
							</div>
							<div style="float: left;">
								<a data-toggle="collapse" data-target="#appointment-1">
									<span class="js-tpl_fio-appointment">
										<?= $item->appointmentReceptionToDoctor->doctorFio ?>
									</span>
									<br>
									<small class="js-tpl_speciality-appointment text-warning">
										<?= $item->appointmentReceptionToDoctor->doctorSpeciality ?>
									</small>
									<br>
									<div class="text-muted collapsed id--details" data-toggle="collapse" data-target="#appointment-<?=$item->id?>">
										<small class="js-tpl_lb-fullname"><?= $item->nameLpu ?></small>
									</div>
								</a>
							</div>
							<div>
								<div style="text-align:right;" class="print-not appoinment-control-buttons">
									<button type="button" class="btn-blue" onclick="PrintElem($('#appointment_result_<?=$item->id?> .appointment_print_form').get(0)); return false;">
										Распечатать
									</button>
									<button type="button" class="btn-red" name="appointment_id" value="<?=$item->id?>" onclick="emias.cancelAppointment(this.value); return false;">
										Отменить
									</button>
								</div>
							</div>
							<div class="id--process_indicator_target process_indicator_target pull-right"></div>
						</div>
                    </div>
                    <div class="clearfix"></div>
                </div>
					<div id="appointment_result_<?=$item->id?>" class="appointment" style="display: none;">
						<div class="search-result-table">
							<div class="appointment_print_form">
							<h2 id="appointment_result_header" class="appointment_header">Талон на приём к врачу</h2>
							<div class="card_row">
								<span class="item_name" title="Название ЛПУ">Название ЛПУ:</span>
								<span id="appointment_result_companyName" class="item_value"><?= $item->nameLpu ?></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Адрес">Адрес:</span>
								<span id="appointment_result_companyAddress" class="item_value"><?= $item->lpuAddress ?></span>
							</div>
							<?php if($item->appointmentReceptionToDoctor->room): ?>
							<div class="card_row">
								<span class="item_name" title="Адрес">Кабинет:</span>
								<span id="appointment_result_companyAddress" class="item_value"><?= $item->appointmentReceptionToDoctor->room ?></span>
							</div>
							<?php endif; ?>
							<div class="card_row">
								<span class="item_name" title="Имя врача">Имя врача:</span>
								<span id="appointment_result_doctorName" class="item_value"><?= $item->appointmentReceptionToDoctor->doctorFio ?></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Специальность">Специальность:</span>
								<span id="appointment_result_doctorSpesiality" class="item_value"><?= $item->appointmentReceptionToDoctor->doctorSpeciality ?></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Время приёма">Время приёма:</span>
								<span id="appointment_result_plannedTime" class="item_value"><?= date("d.m.Y H:i",strtotime(explode("+", $item->dateBegin)[0])) ?></span>
							</div>
							<div>
								<span class="item_name" title="Идентификатор талона">ID талона:</span>
								<span id="appointment_result_IdAppointment" class="item_value"><?= $item->number ?></span>
							</div>
							<div class="card_row">
								<span style="margin-top: 10px;">Если вы не можете посетить прием в указанное время, отмените запись на прием на портале http://emportal.ru или по телефону 8-495-539-3000</span>
							</div>
							</div>
						</div>
					</div>
            </li>
		<?php endforeach; ?>
    </ul>
<?php else: ?>
	<div style="text-align:center;">
	Запрос не дал результатов!
	</div>
<?php endif; ?>
<style>
a.list-group-item.active > .badge, .nav-pills > .active > a > .badge {
	color: #404041;
	background-color: #fff
}

a.thumbnail:hover, a.thumbnail:focus, a.thumbnail.active {
	border-color: #404041
}

.list-group {
	margin-bottom: 20px;
	padding-left: 0
}

.list-group-item {
	position: relative;
	display: block;
	padding: 10px 15px;
	margin-bottom: -1px;
	background-color: #fff;
	border: 1px solid #ddd
}

.list-group-item:first-child {
	border-top-right-radius: 0;
	border-top-left-radius: 0
}

.list-group-item:last-child {
	margin-bottom: 0;
	border-bottom-right-radius: 0;
	border-bottom-left-radius: 0
}

.list-group-item > .badge {
	float: right
}

.list-group-item > .badge + .badge {
	margin-right: 5px
}

a.list-group-item {
	color: #555
}

a.list-group-item .list-group-item-heading {
	color: #333
}

a.list-group-item:hover, a.list-group-item:focus {
	text-decoration: none;
	color: #555;
	background-color: #f5f5f5
}

.list-group-item.disabled, .list-group-item.disabled:hover, .list-group-item.disabled:focus {
	background-color: #eee;
	color: #777
}

.list-group-item.disabled .list-group-item-heading, .list-group-item.disabled:hover .list-group-item-heading, .list-group-item.disabled:focus .list-group-item-heading {
	color: inherit
}

.list-group-item.disabled .list-group-item-text, .list-group-item.disabled:hover .list-group-item-text, .list-group-item.disabled:focus .list-group-item-text {
	color: #777
}

.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
	z-index: 2;
	color: #fff;
	background-color: #404041;
	border-color: #404041
}

.list-group-item.active .list-group-item-heading, .list-group-item.active:hover .list-group-item-heading, .list-group-item.active:focus .list-group-item-heading, .list-group-item.active .list-group-item-heading > small, .list-group-item.active:hover .list-group-item-heading > small, .list-group-item.active:focus .list-group-item-heading > small, .list-group-item.active .list-group-item-heading > .small, .list-group-item.active:hover .list-group-item-heading > .small, .list-group-item.active:focus .list-group-item-heading > .small {
	color: inherit
}

.list-group-item.active .list-group-item-text, .list-group-item.active:hover .list-group-item-text, .list-group-item.active:focus .list-group-item-text {
	color: #a6a6a7
}

.list-group-item-success {
	color: #3c763d;
	background-color: #dff0d8
}

a.list-group-item-success {
	color: #3c763d
}

a.list-group-item-success .list-group-item-heading {
	color: inherit
}

a.list-group-item-success:hover, a.list-group-item-success:focus {
	color: #3c763d;
	background-color: #d0e9c6
}

a.list-group-item-success.active, a.list-group-item-success.active:hover, a.list-group-item-success.active:focus {
	color: #fff;
	background-color: #3c763d;
	border-color: #3c763d
}

.list-group-item-info {
	color: #31708f;
	background-color: #d9edf7
}

a.list-group-item-info {
	color: #31708f
}

a.list-group-item-info .list-group-item-heading {
	color: inherit
}

a.list-group-item-info:hover, a.list-group-item-info:focus {
	color: #31708f;
	background-color: #c4e3f3
}

a.list-group-item-info.active, a.list-group-item-info.active:hover, a.list-group-item-info.active:focus {
	color: #fff;
	background-color: #31708f;
	border-color: #31708f
}

.list-group-item-warning {
	color: #8a6d3b;
	background-color: #fcf8e3
}

a.list-group-item-warning {
	color: #8a6d3b
}

a.list-group-item-warning .list-group-item-heading {
	color: inherit
}

a.list-group-item-warning:hover, a.list-group-item-warning:focus {
	color: #8a6d3b;
	background-color: #faf2cc
}

a.list-group-item-warning.active, a.list-group-item-warning.active:hover, a.list-group-item-warning.active:focus {
	color: #fff;
	background-color: #8a6d3b;
	border-color: #8a6d3b
}

.list-group-item-danger {
	color: #a94442;
	background-color: #f2dede
}

a.list-group-item-danger {
	color: #a94442
}

a.list-group-item-danger .list-group-item-heading {
	color: inherit
}

a.list-group-item-danger:hover, a.list-group-item-danger:focus {
	color: #a94442;
	background-color: #ebcccc
}

a.list-group-item-danger.active, a.list-group-item-danger.active:hover, a.list-group-item-danger.active:focus {
	color: #fff;
	background-color: #a94442;
	border-color: #a94442
}

.list-group-item-heading {
	margin-top: 0;
	margin-bottom: 5px
}

.list-group-item-text {
	margin-bottom: 0;
	line-height: 1.3
}

.pull-right {
	float: right !important
}

.pull-left {
	float: left !important
}
</style>