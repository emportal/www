<table width="750" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="3"><img style="display:block" width="750" height="581" src="http://emportal.ru/images/subscribe_img.png"/></td>
	</tr>
	<tr>
		<td width="25"></td>
		<td width="700">
			<table width="100%" cellpadding="0" cellspacing="0" border="0" >				
				<tr>
					<td height="10">&nbsp;</td>
				</tr>
			</table>
			<p style="margin-top:5px;font-size:15px;color:#004557"><font face="Tahoma, Arial, sans-serif;">Бесплатная и быстрая запись на прием к врачу — такова основная идея Единого Медицинского Портала.
				Зайдя на <a style="color:#00aecf;text-decoration:none" href="http://emportal.ru">emportal.ru</a>, вы можете выбрать любого из 3000 врачей частных клиник Петербурга и в пару кликов записаться на прием.</font></p>
			<p style="margin-top:15px;font-size:15px;color:#004557"><font face="Tahoma, Arial, sans-serif;">Чтобы назначить встречу с врачом, больше не нужно ожидать на телефоне, искать сайт учреждения или лично посещать клинику.
				Если у вас возникнет необходимость в консультации или лечении, <a style="color:#00aecf;text-decoration:none" href="http://emportal.ru">посетите Единый Медицинский Портал</a>, выберите специализацию клиники или врача и запишитесь бесплатно на прием.</font></p>
			<p style="margin-top:15px;margin-bottom:15px;font-size:15px;color:#004557"><font face="Tahoma, Arial, sans-serif;">Чтобы вам было проще выбрать, сравнивайте клиники и врачей по рейтингам, отзывам,
				изучайте дополнительную информацию и сужайте поиск до конкретных районов города.</font></p>
			<table width="100%" cellpadding="0" cellspacing="0" border="0" >
				<tr>
					<td align="center">
						<span style="text-align:center;color:#f05871;font-size:20px;margin:0;">Внимание, акция!</span>
					</td>
				</tr>
				<tr>
					<td height="10"> </td>
				</tr>
			</table>
			
			<p style="margin-bottom:15px;margin-top: 0;font-size:15px;color:#004557"><font face="Tahoma, Arial, sans-serif;">Только до конца октября при записи на прием в 
				<a style="color:#00aecf;text-decoration:none" href="http://emportal.ru/clinic/milano/nab-kanala-griboedova-18-20">клинику эстетической медицины и пластической хирургии «Милано»</a> через Единый Медицинский Портал <b>вы получаете скидку 30% на любую услугу ДОТ-омоложения</b> (лазерного омоложения кожи)!
				Чтобы воспользоваться скидкой, при посещении клиники не забудьте сообщить кодовую фразу: я люблю Единый Медицинский Портал и здоровый образ жизни.</font></p>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="center">
						<a href="http://emportal.ru/clinic/milano/nab-kanala-griboedova-18-20">	
							<img style="display:block;margin:0 auto;" width="467" height="44" src="http://emportal.ru/images/subscribe_btn.png"/>
						</a>
					</td>					
				</tr>
				<tr>
					<td height="10" align="center">&nbsp;</td>
				</tr>
			</table>			
			<p style="margin-top:15px;font-size:15px;color:#004557;"><font face="Tahoma, Arial, sans-serif;">P.S. Читатели, использующие устройства Apple, наверняка оценят по достоинству мобильное приложение «Единый Медицинский Портал».
				C ним вы можете бесплатно записаться к врачу прямо со смартфона!</font></p>
			<table width="100%" cellpadding="0" cellspacing="0" border="0" >
				<tr>
					<td align="center">
						<a href="https://itunes.apple.com/ru/app/edinyj-medicinskij-portal/id887827260">				
							<img style="display:block;margin:0 auto;" width="180" height="62" src="http://emportal.ru/images/appstore.png"/>
						</a>
					</td>					
				</tr>
			</table>
			
		</td>
		<td width="25"></td>
	</tr>	
</table>