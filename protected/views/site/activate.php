<?php

$this->breadcrumbs = array(
	'Повторная активация',
);

/*Yii::app()->clientScript->registerScript('login-page', '
	$("html, body").animate({
    scrollTop: $("div.search-result").offset().top()
}, 100);
', CClientScript::POS_READY)
*/?>
<div class="content">
	<div class="cont">
		<div class="middle-auth w630">
			<?php
			$form = $this->beginWidget('CActiveForm', array(
					'method'				 => 'post',
					'id'					 => 'activate-form',
					'action'				 => '/activate',
					'enableAjaxValidation'	 => true,
					'enableClientValidation' => false,
					'htmlOptions'			 => array('class'=> 'activate', 'enctype' => 'multipart/form-data'),
					'clientOptions'			 => array(
							'validateOnSubmit'	 => true,
							'inputContainer'	 => 'td',
					),
			));
			?>
			<?php if ( Yii::app()->user->hasFlash('recovery') ): ?>
				<p><?=  Yii::app()->user->getFlash('recovery')?></p>
			<?php else: ?>
				<fieldset>
					<div class="row">
						<p style="font-size: 18px">
							Укажите email, для которого вы хотите повторить активацию.
							<br>
							На указанный почтовый ящик будет выслано письмо для восстановления.
						</p>
						<?php echo $form->error($model, 'email');	?><br />
						<?php echo $form->textField($model, 'email', array('class' => 'custom-text')); ?>
						
						<input id="recovery-button" type="submit" class="btn" name="action" value="Отправить">
					</div><!-- /row -->
				</fieldset>
			<? endif; ?>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div><!-- form -->