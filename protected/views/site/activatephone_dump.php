<?php	
/* @var $this Controller */
$this->breadcrumbs = array(
	'Активация мобильного телефона',
);
?>
<div class="content">
	<?php if ( Yii::app()->user->hasFlash('activated') ): ?>
	<div class="flash-success" style="text-align: center;"><?= Yii::app()->user->getFlash('activated') ?></div>
	<?php else: ?>
		<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'method'				 => 'post',
			'id'					 => 'reg-form',
			'action'				 => '/activatephone',			
		));
		/* @var $form CActiveForm */
		?>
		<?php if(Yii::app()->user->hasFlash('send_code')): ?>
	<div class="flash-success" style="text-align: center;"><?= Yii::app()->user->getFlash('send_code') ?></div>
		<?php endif; ?>
		<?php if(Yii::app()->user->hasFlash("error")):?>
			<div class="flash-error" style="text-align:center"><?=Yii::app()->user->getFlash("error");?></div>
		<?php endif; ?>
		<table class="search-result-table">
			<tr class="search-result-box">
				<td class="search-result-signup size-14">
					<p class="activation_text">Это ваш телефон?  <strong><span class="phoneReactivation"><?= Yii::app()->user->model->telefon ?></span></strong></p>
					<span class="btn-green w60 action_buttons" onclick="activation_yes();">да</span>
					<span class="btn-red w60 action_buttons" onclick="activation_no();">нет</span>
				</td>
				<td class="search-result-info input_td">
					<?php
					echo $form->textField($model, 'code', array('class' => 'custom-text showOnYes','style'=>'display:none'));
					?><br/>
					<?php echo $form->error($model, 'code');?>
					<?php echo $form->hiddenField($model, 'type',array('class'=>'type')); ?>
					<span class="error"></span>
				</td>
			</tr>		
			<tr class="search-result-box">
				<td class="search-result-signup size-14"></td>
				<td class="search-result-info">
					<span onclick="changePhoneOnReactivation();" style="display:none" class="showOnNo btn-green">Подтвердить</span>
					<button type="submit" style="display:none" class="btn-green showOnYes" title="Подтвердить">Подтвердить код</button>
				</td>
			</tr>
		</table>
		<?php $this->endWidget(); ?>
	<? endif; ?>
</div>