<?php
$this->pageTitle=Yii::app()->name . ' - Пресса';
$this->breadcrumbs=array(
	'Пресса',
);
Yii::app()->clientScript->registerMetaTag('Пресса о проекте Единый Медицинский Портал', 'description');
?>
<style>
.about_us_image {
	width: 130px;
}
.content {
	padding-top: 0 !important;
}
.col3 {
	padding-top: 30px;
}
.img-outer {
	padding-top: 0 !important;
}
.img-outer img {
	max-width: 120px !important;
}
</style>
<div class="content">
    <div class="content-inner">
		<h1>Пресса о нас</h1>
		<div class="col2">
		<?php
		$pressa = Pressa::model()->findAll();
		foreach ($pressa as $item): ?>
			<div class="item">
				<div class="img-outer">
					<img src="<?= $item->photo ?>">
				</div>
				<div class="info">
					<div class="name">
						<a rel="nofollow" name="pressa<?= $item->id ?>" href="<?= $item->url ?>" target="_blank"><?= $item->name ?></a>
					</div>
					<div><?= $item->description ?></div>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
		<div class="spacer" style="padding: 40px 0;"></div>
	</div>
</div>