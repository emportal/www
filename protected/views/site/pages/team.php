<?php
$this->breadcrumbs = array(
	'Команда',
);
$seoTags = [
	'title' => 'Команда сотрудников Единого Медицинского Портала',
	'description' => 'Команда сотрудников Единого Медицинского Портала - Делаем платную медицину доступнее',
	'seo_block' => [
		'Команда Единого Медицинского Портала работает над созданием российского сервиса записи в частные клиники России. ',
		'Создавая общую площадку для всех частных клиник, мы стараемся сделать платную медицину доступнее. ',
		'Не забываем и про удобство пользования и доступность. Сервис записи в частные клиники доступен жителям России абсолютно БЕСПЛАТНО.',
	]
];
$this->registerSeoTags($seoTags);
?>
	<div class="content" style="padding-top: 10px;">
		<div class="content-inner">
			<h1>Команда</h1>
			<div class="team">
				<div class="item">
					<div class="image">
						<?= CHtml::image('images/team/MMA_6186.jpg', "Владимир Митрофанов") ?>
					</div>
					<div class="info">
						<div class="name">Владимир Митрофанов</div>
						<div class="job">Руководитель проекта</div>
						<div class="email"><a href="mailto:info@emportal.ru">info@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="item">
					<div class="image">
							<?= CHtml::image('images/team/MMA_1182.jpg', "Мария Милославская") ?>
					</div>
					<div class="info">
						<div class="name">Мария Милославская</div>
						<div class="job">Операционный директор</div>
						<div class="email"><a href="mailto:maria.m@emportal.ru">maria.m@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="item">
					<div class="image">
							<?= CHtml::image('images/team/MMA_0871.jpg', "Александр Дьячков") ?>
					</div>
					<div class="info">
						<div class="name">Александр Дьячков</div>
						<div class="job">Технический директор</div>
						<div class="email"><a href="mailto:alexander.d@emportal.ru">alexander.d@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="item">
					<div class="image">
						<?= CHtml::image('images/team/MMA_0983.jpg', "Роман Зайцев") ?>
					</div>
					<div class="info">
						<div class="name">Роман Зайцев</div>
						<div class="job">Разработчик</div>
						<div class="email"><a href="mailto:roman.z@emportal.ru">roman.z@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="item">
					<div class="image">
						<?= CHtml::image('images/team/MMA_5909.jpg', "Сергей Скобелев") ?>
					</div>
					<div class="info">
						<div class="name">Сергей Скобелев</div>
						<div class="job">Главный редактор сайта</div>
						<div class="email"><a href="mailto:adm@emportal.ru">adm@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="item">
					<div class="image">
						<?= CHtml::image('images/team/MMA_6036.jpg', "Анастасия Тихонова") ?>
					</div>
					<div class="info">
						<div class="name">Анастасия Тихонова</div>
						<div class="job">Менеджер по работе с клиентами</div>
						<div class="email"><a href="mailto:ta@emportal.ru">ta@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="item">
					<div class="image">
						<?= CHtml::image('images/team/MMA_OZEL.jpg', "Оксана Зеленова") ?>
					</div>
					<div class="info">
						<div class="name">Оксана Зеленова</div>
						<div class="job">Менеджер по работе с клиентами</div>
						<div class="email"><a href="mailto:oz@emportal.ru">oz@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="item">
					<div class="image">
						<?= CHtml::image('images/team/MMA_6243.jpg', "Ислам Бжихатлов") ?>
					</div>
					<div class="info">
						<div class="name">Ислам Бжихатлов</div>
						<div class="job">SEO-специалист</div>
						<div class="email"><a href="mailto:seo@emportal.ru">seo@emportal.ru</a></div>
					</div>
					<div class="spacer"></div>
				</div>
				<div class="spacer"></div>
			</div>
		</div>
	</div>