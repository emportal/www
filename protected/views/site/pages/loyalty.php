<?php
$this->breadcrumbs = array(
	'Программа лояльности ЕМП',
);
$this->seotype = 'hideseo';
$this->layout = NULL;
$this->pageTitle = "Бонусная система Единого медицинского портала";
?>
<style>
<!--
.loyalty_first_screen {
    display: block;
    width: 100%;
    margin: 0;
    z-index: 155;
    background: url(/images/loyalty/bg_top.jpg) #857155 50% no-repeat;
    -ms-filter: "progid:DXImagedivansform.Microsoft.AlphaImageLoader(src='/images/loyalty/bg_top.jpg', sizingMethod='scale')";
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
.loyalty_btn-start {
	width: 445px;
    height: 61px;
    font: 21px/25px 'Roboto', sans-serif;
    font-weight: 400;
    cursor: pointer;
    border: 0;
    background: rgb(141,198,63);
    box-sizing: border-box;
    word-spacing: normal;
    vertical-align: top;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    color: #fff !important;
    text-decoration: none;
    text-align: center;
    text-shadow: 0 0 0 !important;
    text-transform: none;
    white-space: normal !important;
    display: block;
    position: relative;
}
.loyalty_slogan_long {
	font: 18px/30px 'Roboto', sans-serif;
}
.loyalty_first_column {
	min-height:322px;
}
.loyalty_icons_bottom {
	width: 100%;
    background: #f8f8f8;
    text-align: center;
}
.loyalty_actions_screen {
    z-index: 160;
}
.loyalty_actions_header_section {
    text-align: center;
    padding: 40px 0px 40px 0px;
}
.loyalty_action_block {
    width: 50%;
    float: left;
    text-align: left;
}
.loyalty_action_header {
    font-size: 16px;
    line-height: 21px;
    font-weight: normal;
    text-align: left;
    width: 210px;
}
.loyalty_action_article {
    font-weight: 400;
    font-size: 16px;
    font-style: italic;
    line-height: 1.5em;
    color: black;
    text-align: left;
    margin: 6px 0px 20px 15px;
}
.loyalty_review_screen {
    display: block;
    width: 100%;
    min-height: 326px;
    margin: 0;
    margin-top: -75px;
    z-index: 155;
    background: url(/images/loyalty/bg_review.jpg) #FFF 50% no-repeat;
    -ms-filter: "progid:DXImagedivansform.Microsoft.AlphaImageLoader(src='/images/loyalty/bg_review.jpg', sizingMethod='scale')";
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position-y: 0;
}
.loyalty_warning_screen {
    display: block;
    width: 100%;
    min-height: 169px;
    margin: 0;
    z-index: 155;
    background: url(/images/loyalty/warning_screen.jpg) #FFF 50% no-repeat;
    -ms-filter: "progid:DXImagedivansform.Microsoft.AlphaImageLoader(src='/images/loyalty/warning_screen.jpg', sizingMethod='scale')";
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position-y: 0;
}
-->
</style>
<?php $this->renderPartial('//layouts/_header'); ?>
<div class="SECTION crumbs_section">
	<div class="main_column">
		<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs, 'htmlOptions' => [ 'class' => 'crumbs' ] )); ?>
	</div>
</div>
<div class="SECTION loyalty_first_screen">
	<div class="main_column loyalty_first_column">
		<table style="width: 100%;">
			<tbody>
				<tr>
					<td style="width: 50%;vertical-align: top;">
						<div style="
						    margin: 45px 0px 0px 0px;
						    text-align: center;
						    float: left;
						">
							<p class="loyalty_slogan_long" style="text-align: center;font-size: 24px;color: white;margin-left: -55px;font-weight: 400;line-height: 25px;">
								Хотите получать еще <span style="text-transform: uppercase;font-weight: bold;">больше скидок</span><br>на медицинские услуги?
							</p>
							<p class="loyalty_slogan_long" style="
							    text-align: center;
							    font-size: 24px;
							    font-weight: bolder;
							    color: white;
							    margin: 15px 0px 0 100px;
							">
								Ходить к врачам <span style="text-transform: uppercase;font-weight: bold;">бесплатно</span>?
							</p>
							<button class="icon_search_drop_white" style="margin: 20px 0px 0px 240px; border: none;" onclick="javascript: scrollToId('.loyalty_icons_bottom'); return false;"></button>
						</div>
					</td>
					<td style="width: 50%;vertical-align: top;">
						<div style="margin: 0px -15px 0px 0px; text-align: center;float: right;padding: 30px 0px 0px 0px;border-radius: 0px 0px 10px 10px; background: white; background: rgba(255, 255, 255, 0.75);width: 505px;height: 100px;">
							<h1 style="font-size: 48px; color: #3ec1d9; font-weight: bold; line-height: 31px;">Бонусная система<br><span style="font-size: 27px; color: #1d6470;">Единого медицинского портала</span></h1>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="SECTION loyalty_icons_bottom">
	<div class="main_column">
		<div class="loyalty_actions_header_section">
			<h2 style="font-size: 18px;font-weight: 600;color: black;line-height: 1.3em;">Единый медицинский портал сделал для Вас<br><span style="text-transform: uppercase;color: #f2b077;">бонусную систему</span></h2>
		</div>
		<div style="display: flex;">
			<div class="loyalty_action_block">
				<div class="icon_search_drop_big" style="float: left;"></div>
				<div style="display: flex;">
					<p class="loyalty_action_article">Каждый новый пользователь, зарегистрировавшийся на сайте <span style="color:#04b0cf;">emportal.ru</span>, имеет возможность указать промо-код, который автоматически начисляет бонусы на личный счет пациента! </p>
				</div>
			</div>
			<div class="loyalty_action_block">
				<div class="icon_search_drop_big" style="float: left;"></div>
				<div style="display: flex;">
					<p class="loyalty_action_article">Сумма бонусов зависит от вида промо-кода. Начисленные бонусы пациент всегда может увидеть в своем личном кабинете, авторизовавшись на сайте.</p>
				</div>
			</div>
		</div>
		<div style="display: flex;">
			<div class="loyalty_action_block">
				<div class="icon_search_drop_big" style="float: left;"></div>
				<div style="display: flex;">
					<p class="loyalty_action_article">
					Бонусы можно потратить на оплату услуг тех клиник, которые представлены для онлайн-записи на нашем сайте и согласились принимать участие в  программе.
					Такие клиники, их врачи и услуги помечены специальным значком <span style="color:#04b0cf;">«Бонусы ЕМП»</span>.
					<br>
					<span style="width: 120px;display: block;">
						<span class="empLoyaltyProgram" style=""></span>
					</span>
					</p>
				</div>
			</div>
			<div class="loyalty_action_block">
				<div class="icon_search_drop_big" style="float: left;"></div>
				<div style="display: flex;">
					<p class="loyalty_action_article">Списание бонусов происходит автоматически. То есть, если у Вас на счету есть бонусы и Вы записываетесь на прием  в клинику, которая принимает такие бонусы  — они автоматически спишутся в счет оплаты  услуги.</p>
				</div>
			</div>
		</div>
		<div style="margin: -20px 0 20px 0; text-align: center;">
			<a href="/register" target="blank"><button class="btn-green btn-eld" title="Зарегистрироваться" style="width: 300px; padding: 20px 0px 20px; font-size: 20px;">
				Зарегистрироваться
			</button></a>
		</div>
	</div>
</div>
<div class="SECTION loyalty_warning_screen">
	<div class="main_column">
		<div style="padding: 25px 0px 0px 90px;">
			<div style="
			    background: url(images/loyalty/icon_warning.png) no-repeat center;
			    width: 71px;
			    height: 71px;
			    display: table-cell;
			    vertical-align: middle;
			">
			</div>
			<div style="display: table-cell; vertical-align: middle; padding: 0 0px 0 40px;">
				<p style="color:white;font-size: 18px;">Обязательным условием получения скидки</p>
				<p style="color:white;font-size: 16px;">
					по бонусам ЕМП является предъявление на приеме в клинике распечатанного или<br>отображенного на экране талона
					на запись на прием (сайт автоматически формирует<br>его как только клиника подтверждает ваш визит)
				</p>
			</div>
		</div>
	</div>
</div>
<div class="SECTION loyalty_actions_screen">
	<div>
		<div class="loyalty_actions_header_section">
			<h2 style="font-size: 30px;">Пример</h2>
		</div>
		<table style="width: 1100px;margin: auto;">
			<tbody>
				<tr style="height: 140px;">
					<td style="background: #04b0cf;width: 10px;"></td>
					<td style="width: 49%; padding: 0 40px 0 0; font-size: 48px; vertical-align: middle; color: #eeeeee;text-align: left; background: #f8f8f8;">
						<p style="color: #4f4f4f; font-size: 16px; text-align: right;">
							Николай получил на сайте .../ нашел в интернете промо-код
							<br>GTYMBH, который  дает 500 рублей на первый визит
							<br>к врачу при записи на прием через <span style="color:#04b0cf;">emportal.ru</span>.
						</p>
					</td>
					<td>
						<img src="/images/loyalty/action_1.png" style="margin: -15px 0px 15px -5px; float: left;">
					</td>
					<td style="width: 10px;"></td>
				</tr>
				<tr style="height: 140px;">
					<td style="width: 10px;"></td>
					<td>
						<img src="/images/loyalty/action_2.png" style="margin: 5px -5px 15px 0px; float: right;">
					</td>
					<td style="width: 49%; padding: 0 0 0 40px; font-size: 48px; vertical-align: middle; color: #eeeeee;text-align: left; background: #f8f8f8;">
						<p style="color: #4f4f4f; font-size: 16px; text-align: left;">
							Он регистрируется на сайте и при регистрации <span style="color:#04b0cf;">вводит свой
							<br>промо-код</span> в специальное поле. Сразу после регистрации
							<br>на сайте 500 бонусов начисляются Николаю на его
							<br>бонусный счет в личном кабинете.
						</p>
					</td>
					<td style="background: #04b0cf;width: 10px;"></td>
				</tr>
				<tr style="height: 140px;">
					<td style="background: #04b0cf;width: 10px;"></td>
					<td style="width: 49%; padding: 0 40px 0 0; font-size: 48px; vertical-align: middle; color: #eeeeee;text-align: left; background: #f8f8f8;">
						<p style="color: #4f4f4f; font-size: 16px; text-align: right;">
							Николай давно хотел сходить на прием к стоматологу.
							<br>Он находит ближайшего к нему стоматолога, который работает
							<br>в клинике со <span style="color:#04b0cf;">значком «Бонус ЕМП»</span> и записывается к нему
							<br>на прием. Стоимость приема стоматолога в обычное время
							<br>составляет 1000 рублей.
						</p>
					</td>
					<td>
						<img src="/images/loyalty/action_3.png" style="margin: 5px 0px 15px -5px; float: left;">
					</td>
					<td style="width: 10px;"></td>
				</tr>
				<tr style="height: 140px;">
					<td style="width: 10px;"></td>
					<td>
						<img src="/images/loyalty/action_4.png?v=2" style="margin: 5px -5px 15px 0px; float: right;">
					</td>
					<td style="width: 49%; padding: 0 0 0 40px; font-size: 48px; vertical-align: middle; color: #eeeeee;text-align: left; background: #f8f8f8;">
						<p style="color: #4f4f4f; font-size: 16px; text-align: left;">
							Но за счет того, что у Николая было 500 бонусов ЕМП, стоимость
							<br>приема для него составит всего 500 рублей! Еще 500 рублей
							<br>автоматически спишутся в виде бонусов сразу, как только
							<br>клиника подтвердить дату и время приема. Об этом Николаю
							<br><span style="color:#04b0cf;">придет уведомление на почту</span>, и он сразу может сохранить талон
							<br>на прием в своем смартфоне и идти в клинику!
						</p>
					</td>
					<td style="background: #04b0cf;width: 10px;"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="SECTION loyalty_review_screen">
	<div class="main_column">
		<div>
			<h2 style="font-size: 30px;font-weight: bold;text-align: center;padding: 150px 0px 0px 0px;color: #005061;">Теперь для Вас<br><span style="text-transform: uppercase;">медицинские услуги еще доступнее!</span></h2>
		</div>
		<div style="margin: 20px; text-align: center;">
			<a href="/register" target="blank"><button class="btn-green btn-eld" title="Зарегистрироваться" style="width: 300px; padding: 20px 0px 20px; font-size: 20px;">
				Зарегистрироваться
			</button></a>
		</div>
	</div>
</div>

<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>