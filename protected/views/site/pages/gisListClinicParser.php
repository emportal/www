<?php 
if (!method_exists(Yii::app()->user->model,"hasRight") OR !Yii::app()->user->model->hasRight('ACCESS_NEW_ADMIN'))
{
	throw new CHttpException(403,'You are not authorized to per-form this action.');
	exit;
}
?>
<script type="text/javascript">
function Parser(id,listUrl) {
	var self = this;
	self.form = $("#"+id);
	self.form.append( "<div id='head'>Подождите...</div><hr>" );
	self.form.append( "Запустить проверку ссылок <br>" );
	self.form.append( "<form action=''>" );
	self.form.append( "<label>Начать с номера</label>" );
	self.form.append( "<input id = 'begini' type='number' value = '0'><br>" );
	self.form.append( "<label>Лимит</label>" );
	self.form.append( "<input id = 'limit' type='number' value = '0'><br>" );
	self.form.append( "<input id='startbutton' type='button' value='Старт'>" );
	self.form.append( "</form>" );
	self.form.append( "<table id='tableList' border='1' cellpadding=3 style='border-style:none; border-collapse: collapse; background: #ffff94;'></table>" );
	self.URLList = [];
	self.number = 0;
	self.SelectAll(listUrl);
	self.startbutton = self.form.find("#startbutton");
	self.tableList = self.form.find("#tableList");
	self.limit = self.form.find("#limit");
	self.begini = self.form.find("#begini");
	self.head = self.form.find("#head");
	self.startbutton.click(function() {
		self.Start()
	});
}
Parser.prototype.LoadURL = function(link)
{
	var self = this;
	console.log(link);
    $.ajax({
        type: "GET",
        url: link,
        success: function(result)
	    {
        	var access = "Недоступна";
    	    if(result.indexOf("недоступна") < 0 && (result.indexOf("btn_doctor_sign") >= 0 || result.indexOf("btn_clinic_sign") >= 0)) {
    	    	access = "Доступна";
    	    }
        	self.tableList.append( "<tr style='background: #c8ffc8'><td>" + self.number + "</td><td>" + link + "</td><td>" + access + "</td></tr>" );
		},
		error: function(result) {
			self.tableList.append( "<tr style='background: #ffe6f0'><td>" + self.number + "</td><td>" + link + "</td><td>" + result.statusText + "</td></tr>" );
		},
		complete: function() {
			self.number++;
			self.begini.val(self.number * 1);
	        if(self.startbutton[0].disabled == true)
	        {
	        	self.startbutton[0].value = 'Возобновить';
	        	self.startbutton[0].disabled = false;
	        }
	        else
	        {
	            if(self.number < self.URLList.length & self.number < self.limit.val()) self.LoadURL(self.URLList[self.number]);
	            else self.startbutton[0].value = 'Старт';
	        }
		}
    });
}
Parser.prototype.Start = function()
{
	var self = this;
	var obj = self.startbutton[0];
    if(obj.value == 'Старт')
    {
    	self.tableList.html( "<th>Номер</th><th>Ссылка</th><th>Статус</th>" );
        self.number = self.begini.val() * 1;
        self.LoadURL(self.URLList[self.number]);
        obj.value = 'Стоп';
    }
    else if(obj.value == 'Стоп')
    {
        obj.disabled = true;
    }
    else
    {
    	self.number = self.begini.val() * 1;
        self.LoadURL(self.URLList[self.number]);
        obj.value = 'Стоп';
    }
}
Parser.prototype.SelectAll = function(listUrl)
{
	var self = this;
    $.ajax(listUrl, {
	    success: function(dataXml) {
	    	var data = $(dataXml).find("offers offer");
	        if(data.length > 0) {
		        $.each(data, function(key,value) {
			        var url = "/clinic/" + $(value).find("company").html() + "/" + $(value).find("address").html();
	        		self.URLList.push(url);
		        });
		        self.head.html( "Найдено " + self.URLList.length + " ссылок<br>" );
	            self.limit.val(self.URLList.length);
	        } else {
		        self.head.html( "Ссылки не найдены" )
			};
	    }
    });
}
</script>
</head>
<body>
<div id='gislistParser'></div><hr>
<script type="text/javascript">
	var sitemapParser = new Parser("gislistParser","/site/GisList.xml?type=feed&region=<?= City::model()->findByPk(City::model()->selectedCityId)->subdomain ?>");
</script>
