<?php
$this->pageTitle	 = Yii::app()->name . ' - Юридическая информация';
$this->breadcrumbs	 = array(
	'Юридическая информация',
);
Yii::app()->clientScript->registerMetaTag('Единый Медицинский Портал: пользовательское соглашение', 'description');
?>
<h1>Пользовательское
	соглашение сервисов сайта «Единый
	Медицинский Портал», принадлежащего
	ООО «Первая информационная компания»</h1>
<p>
</p><p>
</p><p><b>1.</b>	<b>Общие
		положения</b></p>
<p>
</p><p>1.1.	ООО
	«Первая информационная компания» (далее
	– ООО «ПИК») предлагает пользователю
	Единого Медицинского Портала (далее –
	Пользователь) использовать сервисы
	Единого Медицинского Портала на условиях,
	изложенных в настоящем Пользовательском
	соглашении (далее – «Соглашение»).
	Соглашение вступает в силу с момента
	выражения пользователем согласия с его
	условиями в порядке, предусмотренном
	п. 1.4 соглашения.</p>
<p>1.2.	ООО
	«ПИК» предлагает пользователям доступ
	к широкому спектру сервисов Единого
	Медицинского Портала (http://emportal.ru/).
	Все существующие на данный момент
	сервисы, а также любое развитие их и/или
	добавление новых является предметом
	настоящего соглашения.</p>
<p>1.3.	Использование
	сервисов Единого Медицинского Портала
	регулируется настоящим соглашением,
	(http://emportal.ru/terms_of_use),
	политикой конфиденциальности
	(http://emportal.ru/privacy_policy),
	а также условиями использования отдельных
	сервисов. Соглашение может быть изменено
	ООО «ПИК» без какого-либо специального
	уведомления, новая редакция соглашения
	вступает в силу с момента ее размещения
	в сети Интернет по указанному в настоящем
	абзаце адресу, если иное не предусмотрено
	новой редакцией соглашения. Действующая
	редакция соглашения всегда находится
	на странице по адресу http://emportal.ru/terms_of_use.</p>
<p>1.4.	Начиная
	использовать какой-либо сервис/его
	отдельные функции, либо пройдя процедуру
	регистрации, пользователь считается
	принявшим условия соглашения в полном
	объеме, без всяких оговорок и исключений.
	В случае несогласия пользователя с
	какими-либо из положений соглашения,
	пользователь не в праве использовать
	сервисы Единого Медицинского Портала.
	В случае, если ООО «ПИК» были внесены
	какие-либо изменения в соглашение в
	порядке, предусмотренном пунктом 1.3
	соглашения, с которыми пользователь не
	согласен, он обязан прекратить
	использование сервисов Единого
	Медицинского Портала.</p>
<p>
</p><p><b>2.	Регистрация
		Пользователя. Учетная запись Пользователя</b></p>
<p>
</p><p>2.1.	Для
	того чтобы воспользоваться некоторыми
	сервисами Единого Медицинского Портала
	или некоторыми отдельными функциями
	сервисов, пользователю необходимо
	пройти процедуру регистрации, в результате
	которой для пользователя будет создана
	уникальная учетная запись.</p>
<p>2.2.	Для
	регистрации пользователь обязуется
	предоставить достоверную и полную
	информацию о себе по вопросам, предлагаемым
	в форме регистрации, и поддерживать эту
	информацию в актуальном состоянии. Если
	пользователь предоставляет неверную
	информацию или у ООО «ПИК» есть основания
	полагать, что предоставленная пользователем
	информация неполна или недостоверна,
	ООО «ПИК»  имеет право по своему
	усмотрению заблокировать либо удалить
	учетную запись пользователя и отказать
	пользователю в использовании сервисов
	Единого Медицинского Портала (либо их
	отдельных функций).</p>
<p>2.3.	Персональная
	информация пользователя, содержащаяся
	в учетной записи пользователя, хранится
	и обрабатывается ООО «ПИК» в соответствии
	с условиями политики конфиденциальности
	(http://emportal.ru/privacy_policy).</p>
<p>2.4.	Для
	доступа к учетной записи пользователя
	применяется пароль. При регистрации
	пользователь самостоятельно указывает
	свой действующий e-mail.
	Пароль автоматически генерируется
	системой и высылается на электронную
	почту пользователя. ООО «ПИК» вправе
	запросить подтверждение введенного
	пользователем e-mail
	путем отправки сообщения контрольной
	строки.</p>
<p>2.5.	Пользователь
	самостоятельно несет ответственность
	за безопасность своего почтового ящика,
	а также самостоятельно обеспечивает
	конфиденциальность своего пароля.
	Пользователь самостоятельно несет
	ответственность за все действия (а также
	их последствия) в рамках или с использованием
	сервисов Единого Медицинского Портала,
	произведенные под учетной записью
	пользователя, включая случаи добровольной
	передачи пользователем данных для
	доступа к учетной записи пользователя
	третьим лицам на любых условиях (в том
	числе по договорам или соглашениям).
	При этом все действия, производимые в
	рамках или с использованием сервисов
	Единого Медицинского Портала под учетной
	записью пользователя, считаются
	произведенными самим пользователем,
	за исключением случаев, когда пользователь,
	в порядке, предусмотренном п. 2.6., уведомил
	ООО «ПИК» о несанкционированном  доступе
	к сервисам Единого Медицинского Портала
	с использованием учетной записи
	пользователя и/или о любом нарушении
	(подозрениях о нарушении) конфиденциальности
	своего пароля.</p>
<p>2.6.	Пользователь
	обязан немедленно уведомить ООО «ПИК»
	о любом случае несанкционированного
	(не разрешенного пользователем) доступа
	к сервисам Единого Медицинского Портала
	с использованием учетной записи
	пользователя и/или о любом нарушении
	(подозрениях о нарушении) конфиденциальности
	своего пароля. В целях безопасности,
	пользователь обязан самостоятельно
	осуществлять безопасное завершение
	работы под своей учетной записью (кнопка
	«Выход») по окончании каждой сессии
	работы с сервисами Единого Медицинского
	Портала. ООО «ПИК» не отвечает за
	возможную потерю или порчу данных, а
	также другие последствия любого
	характера, которые могут произойти
	из-за нарушения пользователем положений
	этой части соглашения.</p>
<p>2.7.	Использование
	пользователем своей учетной записи.
	Пользователь не вправе воспроизводить,
	повторять и копировать, продавать и
	перепродавать, а также использовать
	для каких-либо коммерческих целей
	какие-либо части сервисов Единого
	Медицинского Портала, или доступ к ним,
	кроме тех случаев, когда пользователь
	получил такое разрешение от ООО «ПИК»,
	либо когда это прямо предусмотрено
	пользовательским соглашением какого-либо
	сервиса.
</p>
<p>2.8.	Прекращение
	регистрации. ООО «ПИК» вправе заблокировать
	или удалить учетную запись пользователя,
	а также запретить доступ с использованием
	какой-либо учетной записи к определенным
	сервисам Единого Медицинского Портала
	и удалить любую информацию без объяснения
	причин, в том числе в случае  нарушения
	пользователем условий соглашения или
	условий иных документов, предусмотренных
	п. 1.3. соглашения, а также в случае
	неиспользования соответствующего
	сервиса, если пользователь не пользовался
	ими более 12 месяцев.</p>
<p>2.9.	Удаление
	учетной записи пользователя.</p>
<p>2.9.1.	Пользователь
	вправе в любой момент удалить свою
	учетную запись на всех сервисах Единого
	Медицинского Портала или прекратить
	ее действие в отношении некоторых из
	них, воспользовавшись соответствующей
	функцией в персональном разделе.</p>
<p>2.9.2.	Удаление
	учетной записи Единого Медицинского
	Портала осуществляется в следующем
	порядке:</p>
<p>2.9.2.1.	учетная
	запись блокируется на срок один месяц,
	в течение которого размещенные
	пользовательские данные не удаляются,
	однако доступ к ним становится невозможен
	как для пользователя – владельца учетной
	записи, так и для других пользователей;</p>
<p>2.9.2.2.	если
	в течение указанного выше срока учетная
	запись пользователя будет восстановлена,
	доступ к указанным данным возобновляется
	в объеме, существовавшем на момент
	блокирования;
</p>
<p>2.9.2.3.	если
	в течение указанного выше срока учетная
	запись пользователя не будет восстановлена,
	вся информация, размещенная с ее
	использованием, будет удалена, а
	e-mail будет
	доступен для новой регистрации данного
	пользователя с присвоением другого
	пароля. С этого момента восстановление
	учетной записи, какой-либо информации,
	относящейся к ней, а равно доступов к
	сервисам Единого Медицинского Портала
	с использованием прежней учетной
	записи невозможны.</p>
<p>2.9.3.	Приведенный
	в пп. 2.9.2.1., 2.9.2.2., 2.9.2.3. соглашения (за
	исключением доступности логина для
	использования другими пользователями)
	порядок применим также к запрету доступа
	с использованием какой-либо учетной
	записи к определенным сервисам.</p>
<p>
</p><p>
</p><p><b>3.	Условия
		использования сервисов Единого
		Медицинского Портала</b></p>
<p>
</p><p>3.1.	Пользователь
	самостоятельно несет ответственность
	перед третьими лицами за свои действия,
	связанные с использованием сервиса, в
	том числе, если такие действия приведут
	к нарушению прав и законных интересов
	третьих лиц, а также за соблюдение
	законодательства при использовании
	сервиса.</p>
<p>3.2.	При
	использовании сервисов Единого
	Медицинского Портала пользователь не
	вправе:</p>
<p>3.2.1.
	загружать, посылать, передавать или
	любым другим способом размещать и/или
	распространять информацию, которая
	является незаконным, вредоносным,
	клеветническим, оскорбляет нравственность,
	демонстрирует (или является пропагандой)
	насилия и жестокости, нарушает права
	интеллектуальной собственности,
	пропагандирует ненависть и/или
	дискриминацию людей по расовому,
	этническому, половому, религиозному,
	социальному признакам, содержит
	оскорбления в адрес каких-либо лиц или
	организаций, содержит элементы (или
	является пропагандой) порнографии,
	детской эротики, представляет собой
	рекламу (или является пропагандой) услуг
	сексуального характера (в том числе под
	видом иных услуг), разъясняет порядок
	изготовления, применения или иного
	использования наркотических веществ
	или их аналогов, взрывчатых веществ или
	иного оружия;</p>
<p>3.2.2.	нарушать
	права третьих лиц, в том числе
	несовершеннолетних лиц и/или причинять
	им вреда в любой форме;</p>
<p>3.2.3.	выдавать
	себя за другого человека или представителя
	организации и/или сообщества без
	достаточных на то прав, в том числе за
	сотрудников ООО «ПИК», за модераторов
	форумов, за владельца сайта, а также
	применять любые другие формы и способы
	незаконного представительства других
	лиц в сети, а также вводить пользователей
	или ООО «ПИК» в заблуждение относительно
	свойств и характеристик каких-либо
	субъектов или объектов;
</p>
<p>3.2.4.	загружать,
	посылать, передавать или любым другим
	способом размещать и/или распространять
	информацию, при отсутствии прав на такие
	действия согласно законодательству
	или каким-либо договорным отношениям;</p>
<p>3.2.6.	загружать,
	посылать, передавать или любым другим
	способом размещать и/или распространять
	какие-либо материалы, содержащие вирусы
	или другие компьютерные коды, файлы или
	программы, предназначенные для нарушения,
	уничтожения либо ограничения
	функциональности любого компьютерного
	или телекоммуникационного оборудования
	или программ, для осуществления
	несанкционированного доступа, а также
	серийные номера к коммерческим программным
	продуктам и программы для их генерации,
	логины, пароли и прочие средства для
	получения несанкционированного доступа
	к платным ресурсам в Интернете, а также
	размещения ссылок на вышеуказанную
	информацию;</p>
<p>3.2.7.	несанкционированно
	собирать и хранить персональные данные
	других лиц;</p>
<p>3.2.8.	нарушать
	нормальную работу веб-сайтов и сервисов
	сайта «Единый Медицинский Портал» и
	ООО «ПИК»;</p>
<p>3.2.9.	размещать
	ссылки на ресурсы сети, содержание
	которых противоречит действующему
	законодательству РФ;</p>
<p>3.2.10.	содействовать
	действиям, направленным на нарушение
	ограничений и запретов, налагаемых
	соглашением;</p>
<p>3.2.11.	другим
	образом нарушать нормы законодательства,
	в том числе нормы международного права.</p>
<p>
</p><p><b>4.</b>	<b>Исключительные
		права на содержание сервисов Единого
		Медицинского Портала</b></p>
<p>
</p><p>4.1.	Все
	объекты, доступные при помощи сервисов
	Единого Медицинского Портала, в том
	числе элементы дизайна, текст, графические
	изображения, иллюстрации, видео, программы
	для ЭВМ, базы данных, музыка, звуки и
	другие объекты (далее – содержание
	сервисов), а также любая информация,
	размещенная на сервисах Единого
	Медицинского Портала, являются объектами
	исключительных прав ООО «ПИК»,
	пользователей Единого Медицинского
	портала и других правообладателей.</p>
<p>4.2.	Использование
	любых элементов сервисов возможно
	только в рамках функционала, предлагаемого
	тем или иным сервисом. Никакие элементы
	содержания сервисов Единого Медицинского
	Портала не могут быть использованы иным
	образом без предварительного разрешения
	правообладателя. Под использованием
	подразумеваются, в том числе:
	воспроизведение, копирование, переработка,
	распространение на любой основе,
	отображение во фрейме и т.д. Исключение
	составляют случаи, прямо предусмотренные
	законодательством РФ или условиями
	использования того или иного сервиса
	Единого Медицинского Портала.</p>
<p>Использование
	пользователем элементов содержания
	сервисов для личного некоммерческого
	использования, допускается при условии
	сохранения всех знаков охраны авторского
	права,  смежных прав, товарных знаков,
	других уведомлений об авторстве,
	сохранения имени (или псевдонима)
	автора/наименования правообладателя
	в неизменном виде, сохранении
	соответствующего объекта в неизменном
	виде. Исключение составляют случаи,
	прямо предусмотренные законодательством
	РФ или пользовательскими соглашениями
	того или иного сервиса Единого Медицинского
	Портала.</p>
<p>
</p><p><b>5.</b>	<b>Сайты
		третьих лиц</b></p>
<p>
</p><p>5.1.	Сервисы
	Единого Медицинского Портала могут
	содержать ссылки на другие сайты в сети
	Интернет (сайты третьих лиц). Указанные
	третьи лица и их наполнение не проверяются
	ООО «ПИК» на соответствие тем или иным
	требованиям (достоверности, полноты,
	законности и т.п.). ООО «ПИК»  не несет
	ответственность за любую информацию,
	материалы, размещенные на сайтах третьих
	лиц, к которым пользователь получает
	доступ с использованием сервисов, в том
	числе, за любые мнения или утверждения,
	выраженные на сайтах третьих лиц, рекламу
	и т.п., а также за доступность таких
	сайтов и последствия их использования
	Пользователем.</p>
<p>5.2.	Ссылка
	(в любой форме) на любой сайт, продукт,
	услугу, любую информацию коммерческого
	или некоммерческого характера, размещенная
	на сайте, не является одобрением или
	рекомендацией данных продуктов (услуг,
	деятельности) со стороны ООО «ПИК», за
	исключением случаев, когда на это прямо
	указывается на ресурсах Единого
	Медицинского Портала.</p>
<p>
</p><p><b>6.	Реклама
		на сервисах Единого Медицинского Портала</b></p>
<p>
</p><p>6.1.	ООО
	«ПИК» несет ответственность за рекламу,
	размещенную на сервисах Единого
	Медицинского Портала в пределах,
	установленных законодательством РФ.</p>
<p>
</p><p><b>7.	Отсутствие
		гарантий, ограничение ответственности</b></p>
<p>
</p><p>7.1.	Пользователь
	использует сервисы Единого Медицинского
	портала на свой собственный риск. Сервисы
	предоставляются «как есть». ООО «ПИК»
	не принимает на себя никакой ответственности,
	в том числе за соответствие сервисов
	целям пользователя.</p>
<p>7.2.	ООО
	«ПИК» не гарантирует, что: сервисы
	соответствуют/будут соответствовать
	требованиям пользователя; сервисы будут
	предоставляться непрерывно, быстро,
	надежно и без ошибок; результаты, которые
	могут быть получены с использованием
	сервисов, будут точными и надежными и
	могут использоваться для каких-либо
	целей или в каком-либо качестве (например,
	для установления и/или подтверждения
	каких-либо фактов); качество какого-либо
	продукта, услуги, информации и пр.,
	полученных с использованием сервисов,
	будет соответствовать ожиданиям
	пользователя.</p>
<p>7.3.	Любую
	информацию и/или материалы (в том числе
	загружаемое ПО, письма, какие-либо
	инструкции и руководства к действию и
	т.д.), доступ к которым пользователь
	получает с использованием сервисов
	Единого Медицинского Портала, пользователь
	может использовать на свой собственный
	страх и риск и самостоятельно несет
	ответственность за возможные последствия
	использования указанной информации
	и/или материалов, в том числе за ущерб,
	который может быть причинен компьютеру
	пользователя или третьим лицам, за
	потерю данных или любой другой вред;</p>
<p>7.4.	ООО
	«ПИК» на сайте «Единый Медицинский
	Портал» упрощает запись в медицинское
	учреждение, но не несет ответственности
	за качество оказываемых пользователю
	услуг, за отзывы о медицинском учреждении
	и его специалистах, не несет
	ответственности за любые виды убытков,
	произошедшие вследствие использования
	пользователем сервисов Единого
	Медицинского Портала или отдельных
	частей/функций сервисов;</p>
<p>7.5.	При
	любых обстоятельствах ответственность
	ООО «ПИК» в соответствии со статьей 15
	Гражданского кодекса России ограничена
	10 000 (Десять тысяч) рублей РФ и возлагается
	на него при наличии в его действиях
	вины.</p>
<p>
</p><p><b>8.	Иные
		положения</b></p>
<p>
</p><p>8.1.	Настоящее
	соглашение представляет собой договор
	между пользователем и ООО «ПИК»
	относительно порядка использования
	сервисов и заменяет собой все предыдущие
	соглашения между пользователем и ООО
	«ПИК».</p>
<p>8.2.	Настоящее
	соглашение регулируется и толкуется в
	соответствии с законодательством
	Российской Федерации. Вопросы, не
	урегулированные настоящим соглашением,
	подлежат разрешению в соответствии с
	законодательством Российской Федерации.
	Все возможные споры, вытекающие из
	отношений, регулируемых настоящим
	соглашением, разрешаются в порядке,
	установленном действующим законодательством
	Российской Федерации, по нормам
	российского права. Везде по тексту
	настоящего соглашения, если явно не
	указано иное, под термином «законодательство»
	понимается как законодательство
	Российской Федерации, так и законодательство
	места пребывания пользователя.</p>
<p>8.3.	Ввиду
	безвозмездности услуг, оказываемых в
	рамках настоящего Соглашения, нормы о
	защите прав потребителей, предусмотренные
	законодательством Российской Федерации,
	не могут быть применимы к отношениям
	между пользователем и ООО «ПИК».</p>
<p>8.4.	Ничто
	в соглашении не может пониматься как
	установление между пользователем и ООО
	«ПИК» агентских отношений, отношений
	товарищества, отношений по совместной
	деятельности, отношений личного найма,
	либо каких-то иных отношений, прямо не
	предусмотренных соглашением.</p>
<p>8.5.	Если
	по тем или иным причинам одно или
	несколько положений настоящего соглашения
	будут признаны недействительными или
	не имеющими юридической силы, это не
	оказывает влияния на действительность
	или применимость остальных положений
	соглашения.</p>
<p>8.6.	Бездействие
	со стороны ООО «ПИК» в случае нарушения
	пользователем либо иными пользователями
	положений соглашений не лишает ООО
	«ПИК» права предпринять соответствующие
	действия в защиту своих интересов
	позднее, а также не означает отказа ООО
	«ПИК» от своих прав в случае совершения
	в последующем подобных либо сходных
	нарушений.</p>
<p>8.7.	Настоящее
	соглашение составлено на русском языке
	и в некоторых случаях может быть
	предоставлено пользователю для
	ознакомления на другом языке. В случае
	расхождения русскоязычной версии
	соглашения и версии соглашения на ином
	языке, применяются положения русскоязычной
	версии настоящего соглашения.</p>
<p>
</p><p>Все
	предложения или вопросы по поводу
	настоящей политики следует сообщать в
	службу поддержки пользователей Единого
	медицинского портала (http://emportal.ru/),
	либо по адресу 195009 , Санкт-Петербург,
	Свердловская набережная, дом 4, литер
	А, ООО «ПИК».</p><p>
</p><br><p></p>

