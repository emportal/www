<?php	
/* @var $this Controller */
$this->breadcrumbs = array(
	'Активация мобильного телефона',
);
?>
<div class="content">
	<?php if ( Yii::app()->user->hasFlash('activated') ): ?>
	<div class="flash-success" style="text-align: center;"><?= Yii::app()->user->getFlash('activated') ?></div>
	<?php else: ?>
		<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'method'				 => 'post',
			'id'					 => 'activate-form',
			'action'				 => '/activatephone',			
		));
		/* @var $form CActiveForm */
		?>
		<?php if(Yii::app()->user->hasFlash('send_code')): ?>
	<div class="flash-success" style="text-align: center;"><?= Yii::app()->user->getFlash('send_code') ?></div>
		<?php endif; ?>
		<?php if(Yii::app()->user->hasFlash("error")):?>
			<div class="flash-error" style="text-align:center"><?=Yii::app()->user->getFlash("error");?></div>
		<?php endif; ?>
		<table class="search-result-table">
			<tr class="search-result-box" >
				<td class="search-result-signup size-14">
					Вы можете изменить телефон или подтвердить текущий
				</td>
				<td class="search-result-info">
					<?php if($showCodeRow): ?>
						<span class="size-14"><?=$model->phone?></span>
						<?php
						$this->widget('CMaskedTextField', array(
							'model' => $model,
							'attribute' => 'phone',
							'mask' => '+79999999999',
							'placeholder' => '_',
							'htmlOptions' => array('class' => 'custom-text','style'=>'display:none'),
						));
						?>
					<?php else: ?>
						<?php
						$this->widget('CMaskedTextField', array(
							'model' => $model,
							'attribute' => 'phone',
							'mask' => '+79999999999',
							'placeholder' => '_',
							'htmlOptions' => array('class' => 'custom-text'),
						));
						?><br/>
						<?php echo $form->error($model, 'phone');?>	
					<?php endif; ?>
					
				</td>
			</tr>
			<tr class="search-result-box codeRow" <?=$showCodeRow ? '' : 'style="display:none"'?>>
				<td class="search-result-signup size-14">
					<p class="activation_text">Введите код подтверждения </p>				
				</td>
				<td class="search-result-info input_td">					
					
					<?php
					echo $form->textField($model, 'code', array('class' => 'custom-text'));
					?><br/>
					<?php echo $form->error($model, 'code');?>		
				</td>
			</tr>		
			<tr class="search-result-box">
				<td class="search-result-signup size-14"></td>
				<td class="search-result-info">
					<!--<span onclick="" style="" class="btn-green">Подтвердить</span>-->
					<button type="submit" class="btn-green" title="Подтвердить"><?= $showCodeRow ? 'Подтвердить код' : 'Подтвердить номер телефона' ?></button>
				</td>
			</tr>
		</table>
		<?php $this->endWidget(); ?>
	<? endif; ?>
</div>