<?php
$this->breadcrumbs = array(	
	'Фотогалерея',
);

if(!empty($album)) {
	
	$this->breadcrumbs = array(
		'Фотогалерея' => $this->createUrl('gallery'),
		$album->title
	);
}

/* Yii::app()->clientScript->registerScript('login-page', '
  $("html, body").animate({
  scrollTop: $("div.search-result").offset().top()
  }, 100);
  ', CClientScript::POS_READY)
 */
?>
<div class="content">

	
		<?php if (empty($name)): ?>
			<?php 
				/* 
				в этом случае просто редиректим на главную
				т.к. этот view используется еще и в модуле /admin (default/gallery), и вот там он нужен
				*/
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /', '/');
			?>
			<h1 class="better_h1">Фотогалерея Единого Медицинского портала</h1>
			<div class="albums">			
				<?php foreach (!empty($albums) ? $albums : array() as $al): ?>
				<?php $img = null;
						if($al->galleryBehavior->getGalleryPhotos("gallery",1000)) {
							$img = CHtml::image($al->galleryBehavior->getGalleryPhotos("gallery",1000)[0]->getPreview());
						}
					?>
				<?php if($img !== null): ?>
				<div class="album">
					<?= CHtml::link($img, $this->createUrl('gallery', ['name' => CHtml::encode($al->name)])); ?>
					<br/>
					<?= $al->title ?>					
				</div>
				<?php endif; ?>
			<?php endforeach; ?>
			</div>
		<?php else: ?>
			<div class="photos">
			<?= CHtml::link('Вернуться к выбору альбома', $this->createUrl('gallery'),['class'=>'back_to_album']); ?>
			<?php
			$this->widget('zii.widgets.CListView', [
				'dataProvider'	=> $dataProvider,
				'itemView'		=> '_galleryBlock',
				'template'		 => "{pager}\n{items}\n{pager}",
				'pager'=>array(
					'maxButtonCount'=>8,
					'header'=>'',
					'cssFile'=>false
				),
				'afterAjaxUpdate'=>"function(id,date) {
					$('.fancybox-thumb').fancybox({
						prevEffect: 'none',
						nextEffect: 'none',
						helpers: {
							title: {
								type: 'outside'
							},
							thumbs: {
								width: 50,
								height: 50
							}
						}
					});
				}
				",
				'htmlOptions'=>array(
					"class"=>"photos_justify"
				)
			]);
			?>
			<?php /*foreach (!empty($photos) ? $photos : array() as $photo): ?>
				<?php echo CHtml::link(CHtml::image($photo->getPreview()), $photo->getUrl(), array('class' => 'fancybox-thumb', "rel" => "fancybox-thumb", "title" => $photo->name)); ?>
			<?php endforeach; */?>
			<?= CHtml::link('Вернуться к выбору альбома', $this->createUrl('gallery'),['class'=>'back_to_album']); ?>
			</div>
		<?php endif; ?>
	

</div><!-- form -->