<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
    <meta http-equiv="refresh" content="2;URL=/" />
</head>
<body class="mistake">
    <div class="m-top">
        <div class="wrapper">
            <div class="m-logo">
				<?= CHtml::link(CHtml::image('/images/newLayouts/LOGO-MAIN-DARK.png', "Единый медицинский портал"), '/')?>
			</div>
            <div class="m-patch"></div>
            <div class="clearfix"></div>
            <div class="m-content">
                <div class="m-title"><?=$pageText;?></div>
                <p>Вернуться на <a href="/">главную</a></p>
            </div>
        </div>
    </div>
</body>
</html>