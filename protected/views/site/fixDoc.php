<script>
	
		function delPhoto(obj,link) {
			$.ajax({
					url: "/site/fixDoctor",
					async: false,
					data: ({					
						ajax: 1,
						link: link,						
					}),
					dataType: 'json',
					type: "GET",
					success: function(msg) {
						if(msg.success) {							
							$(obj).remove();
						}
					},
				});
		}
	
</script>
<?php foreach($doctors as $doctor): ?>
<div style="<?= $doctor->photoUrl ? 'margin-bottom:20px;': 'display:none'?>">
	<a href="/doctor/<?=$doctor->link?>.html"><?= CHtml::encode($doctor->name) ?></a><br/>
	<img src="<?= $doctor->photoUrl ?>"/><br/>
	<a style="cursor:pointer" onclick="delPhoto(this,'<?=$doctor->link?>');">удалить фото</a>
</div>
<?php endforeach; ?>
