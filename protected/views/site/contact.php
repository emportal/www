<?php
//////////////////////////////////////////////////////////////////////////////
//    Подсчёт количества поключенных частных клиник
$CountOfCompanies = Address::model()->count(Company::ActiveClinicAddressesCriteria());

$numberOfPatientsPerMonth = Config::model()->findByAttributes(["name" => "Количество пациентов в месяц"]);
$numberOfPatientsPerMonthValue = -1;
if($numberOfPatientsPerMonth) {
	if(is_numeric($numberOfPatientsPerMonth->value)) {
		$numberOfPatientsPerMonthValue = intval($numberOfPatientsPerMonth->value);
	}
}
if($numberOfPatientsPerMonthValue < 0) $numberOfPatientsPerMonthValue = AppointmentToDoctors::model()->count('createdDate > (NOW() - INTERVAL 1 MONTH) AND statusId<>\''.AppointmentToDoctors::DISABLED.'\'');

Yii::app()->clientScript->registerMetaTag('Предложение для клиник: станьте партнером проекта Единый Медицинский Портал', 'description');
?>
<style>
<!--
-->
</style>
<?php $this->renderPartial('//layouts/_white_head'); ?>
<div class="SECTION landing_first_screen">
	<div class="main_column landing_first_column">
		<a href="/" title="Единый Медицинский Портал">
			<img src="/images/newLayouts/LOGO-MAIN.png" alt="Единый Медицинский Портал" class="LOGO_MAIN">
		</a>
		<h1 class="landing_slogan">Регистрация клиники на портале:</h1>
		<p class="landing_slogan_long">Начните привлекать еще больше пациентов уже сейчас!<br/>
		Добавьте вашу клинику на emportal.ru — это <b><?= $replaceWord ?></b>, <b>быстро</b> и <b>безопасно</b>.</p>
		<div class="landing_btn-start_container">
			<button class="landing_btn-start" type="submit" onclick="javascript: scrollToId('#signup_form'); return false;" title="Нажмите, чтобы добавить">Добавить клинику на Портал</button>
		</div>
	</div>
	<div class="SECTION ICONS_BOTTOM" id="stats">
		<div class="main_column">
			<div class="stats_block">
				<div class="stats_icon_user"></div>
				<div class="stats_value"><?= $numberOfPatientsPerMonthValue ?></div>
				<div class="stats_description"><span>Пациентов<br>в месяц</span></div>
			</div>
			<div class="stats_block">
				<div class="stats_icon_hosp"></div>
				<div class="stats_value"><?=$CountOfCompanies?></div>
				<div class="stats_description"><span>Клиник<br>по всей России</span></div>
			</div>
			<div class="stats_block">
				<div class="stats_icon_russia"></div>
				<div class="stats_value"><?= count(Yii::app()->params['regions']); ?></div>
				<div class="stats_description"><span>Регионов<br>присутствия</span></div>
			</div>
		</div>
	</div>
</div>
<div class="SECTION layouts_actions_screen">
	<div class="main_column">
		<div class="layouts_actions_header_section">
			<h2 style="color: rgb(0,60,97); font-size: 30px; width: 700px;margin: 32px auto 8px;">Почему Единый медицинский портал эффективен и удобен</h2>
			<p style="margin: auto; text-align: center; font-size: 14px; color: rgb(136,136,136); width: 530px;">Хотите упростить жизнь себе и пациентам? Стремитесь привлечь новых клиентов? Тогда самое время стать партнёром Единого Медицинского Портала. Все продвинутые клиники 
			<?php
			echo (!empty(City::getSelectedCity()->locative)) ? ' в ' . City::getSelectedCity()->locative : '';
			?>
			уже оценили преимущества обновлённого сайта для записи на приём к врачу.</p>
		</div>
		<table style="width: 100%;">
			<tr>
				<td class="layout_action_row">
					<img src="/images/landing_icon_diagram.png" style="height: 180px;">
					<h3 class="action_header">Постоянно растущий поток пациентов</h3>
				</td>
				<td class="layout_action_row">
					<img src="/images/landing_icon_dashboard.png" style="height: 145px;">
					<h3 class="action_header">Удобный личный кабинет клиники</h3>
				</td>
				<td class="layout_action_row">
					<img src="<?= $replaceImageSrc ?>" style="height: 180px;">
					<h3 class="action_header"><?= $replacePhrase ?></h3>
				</td>
				<td class="layout_action_row">
					<img src="/images/landing_icon_doctor.png" style="height: 180px;">
					<h3 class="action_header">Бесплатная форма онлайн-записи</h3>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="SECTION landing_form_screen">
	<div class="main_column">
		<div class="landing_form_container">
			<div class="landing_form">
				<?php if (City::isSelfRegisterCity()) :
					$newCompany->run();
				else : ?>
				<?php $form=$this->beginWidget('CActiveForm', array(
							'method' => 'post',
							'id' => 'signup_form',
							'action' => '#signup_form',
						)
					);
				?>
				<h2 class="header">Регистрация клиники на портале</h2>
				
				<?php if(Yii::app()->user->hasFlash('contact')): ?>
				<div class="flash-success">
					<?php echo Yii::app()->user->getFlash('contact'); ?>
					<script>		
						$(document).ready(function() {
							window.setTimeout(function() {
								countersManager.reachGoal({
									'forCounters': ['yandex', 'google'],
									'goalName': 'company_registration',
									//'callback': function() {console.log('callback: company_registration');}
								});
							}, 100);
						});
					</script>
				</div>
				<?php else: ?>
				<?php echo $form->errorSummary($model); ?>
				<div class="landing-form-box">
					<div class="landing-form-signup"><?php echo $form->label($model,'subject'); ?></div>
					<div class="landing-form-info"><?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128, 'class'=>'custom-text landing-form-fullweight',placeholder=>'Например, "Американская медицинская клиника"')); ?></div>
				</div>
				
				<div class="landing-form-box">
					<div class="landing-form-signup"><?php echo $form->label($model,'city'); ?></div>
					<div class="landing-form-info"><?php echo $form->textField($model,'city', array('class'=>'custom-text landing-form-halfweight',placeholder=>'Санкт-Петербург')); ?></div>
				</div>
			
				<div class="landing-form-box">
					<div class="landing-form-signup"><?php echo $form->label($model,'address'); ?></div>
					<div class="landing-form-info"><?php echo $form->textField($model,'address',array('rows'=>6, 'cols'=>50, 'class'=>'custom-text landing-form-fullweight',placeholder=>'пр. Художников, д. 28, корпус 4')); ?></div>
				</div>
				
				<div class="landing-form-box" style="float: left;">
					<div class="landing-form-signup"><?php echo $form->label($model,'phone'); ?></div>
					<div class="landing-form-info"><?php echo $form->textField($model,'phone', array('class'=>'custom-text landing-form-halfweight',placeholder=>'+7 (ххх) ххх-хх-хх')); ?></div>
				</div>
				
				<div class="landing-form-box" style="float: right;">
					<div class="landing-form-signup"><?php echo $form->label($model,'email'); ?></div>
					<div class="landing-form-info"><?php echo $form->textField($model,'email', array('class'=>'custom-text landing-form-halfweight',placeholder=>'address@clinic.ru')); ?></div>
				</div>
	<!-- 	 -->
				<div class="landing-form-box">
					<div class="landing-form-signup"><?php echo $form->label($model,'name'); ?></div>
					<div class="landing-form-info"><?php echo $form->textField($model,'name', array('class'=>'custom-text landing-form-fullweight',placeholder=>'Юлия Юрьевна Петрушкина')); ?></div>
				</div>
			
				<div class="landing-form-box">
					<div class="landing-form-signup"><?php echo $form->label($model,'body'); ?></div>
					<div class="landing-form-info"><?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50, 'class'=>'landing-form-fullweight',placeholder=>'Пример комментария')); ?></div>
				</div>
				<?php if(CCaptcha::checkRequirements()): ?>
				<div class="landing-form-box" style="visibility: hidden; height: 0px;">
					<?php //echo $form->label($model,'verifyCode'); ?>
					<div style="display: inline-block;vertical-align: top;">
					<?php $this->widget('CCaptcha', array('clickableImage'=>divue, 'showRefreshButton'=>divue, 'buttonLabel' => CHtml::image(Yii::app()->baseUrl . '/images/reload_white.png', '', array('style'=>'border:none; margin: 0px 15px 0px 15px;')),'imageOptions'=>array('style'=>'/*display:block;*/border:none;', /*'height'=>'40px',*/ 'alt'=>'Картинка с кодом валидации', 'title'=>'Чтобы обновить картинку, нажмите на нее'))); ?>
					</div>
					<div style="display: inline-block;vertical-align: top;">
						<?php echo $form->textField($model,'verifyCode', array('class'=>'custom-text')); ?>
						<br>
						<?php echo $form->label($model,'verifyCode', array('label'=>'Пожалуйста, введите символы на картинке',placeholder=>'Код с картинки')); ?>
					</div>
				</div>
				<?php endif; ?>
				<div class="landing-form-box">
						<button class="landing_btn-start" type="submit" title="Нажмите, чтобы добавить">Добавить клинику на Портал</button>
				</div>
				<?php endif; ?>
				<?php $this->endWidget(); ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="text_with_brush">
			<span><?= $replaceText ?></span>
		</div>
	</div>
</div>
<?php Yii::app()->clientScript->registerCoreScript('jquery.maskedinput'); ?>
<script>
$(function() {
	$("#ContactForm_phone, #phoneInput").mask("+7 (999) 999-99-99", {placeholder: "_"});
});
</script>

<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>