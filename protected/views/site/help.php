<?php
/* @var $this Controller */
$this->breadcrumbs = array(
	'Поможем подобрать врача',
);
?>
<style>
.inline {display: inline !important;}
.search-result-signup {
	width: 300px !important;
	max-width: auto !important;
}
.custom-text {
	width: 100% !important;
}
.custom-textarea {
	height: 70px;
	border-radius: 3px;
    border: 1px solid rgb(220,220,220);
}
.wrapper .content td.search-result-signup {
	padding: 0;
}
.btn-green.btn-eld {
	width: 300px;
	padding: 20px 0px 20px;
	font-size: 20px;
}
</style>
<div class="wrapper vpadding10">
<div class="content" style="padding: 0 10px 0 10px;margin: auto;display: table;">
	<h1>Поможем подобрать врача</h1>
	<? if(!$result) : ?>
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'help-form',
			'action' => '/help',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'htmlOptions' => array('class' => 'register', 'enctype' => 'multipart/form-data'),
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'inputContainer' => 'td',
				'beforeValidate' => 'js:function(form) {								
										var bv_phone = $("#Help_phone").val();
										if (bv_phone) {
											bv_phone = bv_phone.replace(/ |-|[(]|[)]/g, "");
											$("#Help_phone").val(bv_phone);
										}
										return true;
									}',
			),
		));
		?>
		<table class="search-result-table" style="width: 400px; margin-top: 15px;">
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'name'); ?>
					<?php
					echo $form->textField($model, 'name', array('class' => 'custom-text'));
					?><br>
					<?php echo $form->error($model, 'name'); ?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'phone'); ?>
					<?php echo $form->textField($model, 'phone', array('class' => 'custom-text', 'id' => 'Help_phone'));?>			
					<?php echo $form->error($model, 'phone'); ?>
					<script>
						$(function() {
							$("#Help_phone").mask("+7 (999) 999-99-99", {placeholder: "_"});
							var isGuest = 1;
						});
					</script>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'city'); ?>
					<?php
					echo $form->textField($model, 'city', array('class' => 'custom-text'));
					?><br>
					<?php echo $form->error($model, 'city'); ?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'comment'); ?>
					<?php
					echo $form->textArea($model, 'comment', array('class' => 'custom-text custom-textarea'));
					?><br>
					<?php echo $form->error($model, 'comment'); ?>
					<div style="margin-top: 5px; margin-bottom: 5px;">При необходимости, дополните вашу заявку словами</div>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14">
					<button type="submit" class="btn-green btn-eld" style="width:100%;margin-top: 10px;" title="Отправить">Отправить</button>
				</td>
			</tr>
		</table>
		<?php $this->endWidget(); ?>
	<? else: ?>
		<div style="margin-top: 20px; margin-bottom: 20px; font-size: 12pt;">Спасибо, ваша заявка отправлена!</div>
	<? endif; ?>
</div>
</div>