<?php
/* @var $this Controller */
$this->breadcrumbs = array(
	'Регистрация',
);
?>
<style>
.inline {display: inline !important;}
.search-result-signup {
	width: 300px !important;
	max-width: auto !important;
}
.custom-text {
	width: 100% !important;
}
.wrapper .content td.search-result-signup {
	padding: 0;
}
.btn-green.btn-eld {
	width: 300px;
	padding: 20px 0px 20px;
	font-size: 20px;
}
</style>
<div class="wrapper vpadding10">
<div class="content" style="padding: 0 10px 0 10px;margin: auto;display: table;">
	<h1>Регистрация нового пользователя</h1>
	<div class="register_login_via_social vpadding10">
		<div class="social-head">Войти через социальные сети</div>
		<?php Yii::app()->eauth->renderWidget(); ?>	
	</div>	
	<?php if (Yii::app()->user->hasFlash('registerSuccess')): ?>
		<p style="text-align: center;"><?= Yii::app()->user->getFlash('registerSuccess') ?></p>
	<?php else: ?>
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'reg-form',
			'action' => '/register',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'htmlOptions' => array('class' => 'register', 'enctype' => 'multipart/form-data'),
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'inputContainer' => 'td',
				'afterValidate' => 'js: function(form,data,hasError) { 
					if(hasError === false) {
						yaCounter18794458.reachGoal("user_register");
						ga("send", "pageview", "/user_register");
					}
					return true;
				 }'
			),
		));
		?>
		<table class="search-result-table">
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'email'); ?>
					<?php
					echo $form->emailField($model, 'email', array('class' => 'custom-text','autocomplete'=>'off'));
					?><br>
					<?php echo $form->error($model, 'email'); ?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'name'); ?>
					<?php
					echo $form->textField($model, 'name', array('class' => 'custom-text','autocomplete'=>'off'));
					?><br>
					<?php echo $form->error($model, 'name'); ?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'password'); ?>
					<?php
					echo $form->passwordField($model, 'password', array('class' => 'custom-text','autocomplete'=>'off'));
					?><br>
					<?php echo $form->error($model, 'password'); ?>
				</td>
			</tr>
			<?php if (1==2) : //rm?>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14">Подтверждение пароля <span class="">*</span>
					<?php
					echo $form->passwordField($model, 'passwordConfirm', array('class' => 'custom-text','autocomplete'=>'off'));
					?><br>
					<?php echo $form->error($model, 'passwordConfirm'); ?>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'birthday'); ?>
					<?php
					$form->widget('zii.widgets.jui.CJuiDatePicker', array(
						'model' => $model,
						'attribute' => 'birthday',
						'language' => 'ru',
						'htmlOptions' => array('class' => 'custom-text'),
						'options' => array(
							'changeMonth' => true,
							'changeYear' => true,
							'altFormat' => "dd.mm.yy",
							'dateFormat' => "dd.mm.yy",
							'defaultDate' => '01.01.90',
							'yearRange' => '-80:+0',
						),
							)
					);
					echo '<br/>';
					echo $form->error($model, 'birthday');
					?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td></td>
				<td></td>
			</tr>
			<!--<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'telefon'); ?>
					<?php
					$this->widget('CMaskedTextField', array(
						'model' => $model,
						'attribute' => 'telefon',
						'mask' => '+79999999999',
						'placeholder' => '_',
						'htmlOptions' => array('class' => 'custom-text'),
					));
					?>
					<?php // echo $form->textField($model, 'telefon', array('class' => 'custom-text'));?>
					<div class="btn-blue" id="activatePhone" onclick="generateCodeForPhone();
								return false">Получить код</div><br/>
			<?php echo $form->error($model, 'telefon'); ?>
				</td>
			</tr>-->

			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14"><?=  CHtml::activeLabelEx($model,  'promoCode'); ?>
					<?php echo $form->textField($model, 'promoCode', array('class' => 'custom-text','autocomplete'=>'off')); ?>
					<br>
					<?php echo $form->error($model, 'promoCode'); ?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14" style="padding: 10px 0 5px 0;">
					<?php echo $form->checkBox($model, 'subscriber', array('class' => 'niceCheck')); ?>
					<label for="User_subscriber" class="inline">Я хочу получать тематическую рассылку</label>
					<br>
					<?php echo $form->error($model, 'subscriber'); ?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14" style="padding: 5px 0 10px 0;">
					<?php echo $form->checkBox($model, 'confirmation', array('class' => 'niceCheck')); ?>
					<label for="User_confirmation" class="inline">Я согласен(-на) на обработку личных данных</label><br/>
					<br>
					<?php echo $form->error($model, 'confirmation'); ?>
				</td>
			</tr>
			<tr class="search-result-box" colspan="2">
				<td class="search-result-signup size-14">
					<button type="submit" class="btn-green btn-eld" style="width:100%;" title="Зарегистрироваться">Зарегистрироваться</button>
				</td>
			</tr>
		</table>
	<?php $this->endWidget(); ?>
<?php endif; ?>
</div>
</div>