<?php
/* @var $this SearchController */
/* @var $model Disease */
$this->pageTitle = $model->name . " — лечение, симптомы, профилактика";
Yii::app()->clientScript->registerMetaTag(CHtml::encode($model->name)." - Симптомы и первые признаки заболевания, профилактика и лечение болезни(как не заболеть).");

Yii::app()->clientScript->registerMetaTag("лечение " . mb_strtolower($model->genitive), 'keywords');

		
$this->breadcrumbs = array(
	'Болезни' => array('handbook/disease'),
	$model->name,
);
Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>

<style>
.search-block {
	min-height: 280px!important;
}
</style>

<div class="SECTION">
	<div class="wrapper">
		<div class="disease-description">
			<h1 class="news-title"><?= $model->name ?></h1>
			<?=$model->noindex ? '<!--noindex-->':''?>
			<p>
				<?= $model->description ?>
			</p>
			<?=$model->noindex ? '<!--/noindex-->':''?>	
		</div>
	</div>
</div>

<div class="SECTION SEARCH_RESULT">
	<div class="wrapper">
		<div style="padding: 1px 0px 1px 0px;">
			<?php if (!empty($clinics)): ?>
				<div class="clinics">
					<h2>Клиники</h2>
					<table>
						<?php foreach ($clinics as $cl): ?>
							<?php 
								$this->renderPartial('//search/_clinics_unique',[
									'data' => $cl[0]->company,
									'address' => $cl[0],
									'addresses' => null,
									'big' => 0,	
									'style' => 'margin-bottom:25px'
								]);
							?>
						<?php endforeach; ?>
					</table>
				</div>
			<?php endif; ?>
			<?php if (!empty($doctors)): ?>
				<div class="doctors">
					<h4 class="diseases-header">Врачи, лечащие это заболевание:</h4>
						<?php foreach ($doctors as $doctor): ?>
							<?php
							$groupedData = [
								$doctor->id => $doctor
							];
							$this->renderPartial('//search/_doctor_card', [
								'data' => $doctor,
								'groupedData' => $groupedData,
								'style' => 'margin-bottom:25px'
							]);
							?>
						<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<div class="both"></div>
		</div>
	</div>
</div>

<?php
	$templateRules = [
		"%болезнь%" => CHtml::encode($model->name),
		"%appName%" => Yii::app()->name,
		"\"\"" => " ",
		"  " => " ",
		" ," => ",",
	];
	$tagDescription = "%болезнь% - первые симптомы, профилактика и лечение.";
	if(!empty($model->tagDescription))
		$tagDescription = $model->tagDescription;
	Yii::app()->clientScript->registerMetaTag(MyTools::createFromTemplate($tagDescription,$templateRules), 'description');
?>
