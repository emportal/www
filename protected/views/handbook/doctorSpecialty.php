<?php
/* @var $this SearchController */
/* @var $model CompanyActivite */

/* $this->pageTitle = "Клиники {$model->genName} – $city – Цены, отзывы, адреса, запись в клинику {$model->genName} – ".Yii::app()->name;
  Yii::app()->clientScript->registerMetaTag("Запись в клинику {$model->genName} - $city."
  . " Здесь собраны все адреса и отзывы по выбранному направлению."
  . " ".Yii::app()->name." – клиники {$model->genName}",'description'); */
$this->pageTitle = "Запись к ".$model->datName." – $city – Консультация врача ".$model->datName." – "
		. "Цены и время приема врачей, отзывы, адреса, онлайн запись – " . Yii::app()->name;
Yii::app()->clientScript->registerMetaTag("Запись на консультацию к ".CHtml::encode($model->datName)." - $city."
		. " Здесь собраны все контакты, адреса и отзывы по выбранной специализации врачей."
		. " " . Yii::app()->name . " – Запись к врачу ".CHtml::encode($model->datName), 'description');
$this->breadcrumbs = array(
	'Специальности врачей' => array('handbook/doctorSpecialty'),
	"Запись на прием" /* $model->name, */
);
?>

<div class="cont handbook">
	<h1>Запись к специалисту "<?= $model->name ?>"</h1>
	<?= $model->noindex ? '<!--noindex-->' : '' ?>
	<p><?= $model->description ?></p>
	<?= $model->noindex ? '<!--/noindex-->' : '' ?>	
	<div>
		<div>
			<?php if (!empty($clinics)): ?>		
				<div class="clinics">				
					<h2>Клиники</h2>
					<table>
						<?php foreach ($clinics as $cl): ?>
							<?php 
								$this->renderPartial('//search/_clinics_unique',[
									'data' => $cl[0]->company,
									'address' => $cl[0],
									'addresses' => null,
									'big' => 0,	
									'style' => 'margin-bottom:25px'
								]);
							?>
						<?php endforeach; ?>
					</table>				
				</div>
			<?php endif; ?>
			<?php if (!empty($doctors)): ?>			
				<div class="doctors">
					<h2>Врачи</h2>
					<table>
						<?php foreach ($doctors as $doctor): ?>		
							<?php
							$groupedData = [
								$doctor->id => $doctor
							];
							$this->renderPartial('//search/_doctor_unique', [
								'data' => $doctor,
								'groupedData' => $groupedData,
								'style' => 'margin-bottom:25px'
							]);
							?>							
						<?php endforeach; ?>
					</table>				
				</div>	
			<?php endif; ?>
			<div class="both"></div>
		</div>
		<?php if (!empty($clinics)): ?>		
			<div class="clinics">
				<strong>
					<?= CHtml::link('Перейти ко всем клиникам', $this->createUrl('/search/clinic', ['profile' => 'profile-' . $model->companyActivite->linkUrl])); ?>			
				</strong>
			</div>
		<?php endif; ?>
		<?php if (!empty($doctors)): ?>		
			<div class="doctors">
				<strong>
					<?= CHtml::link('Перейти ко всем врачам', $this->createUrl('/search/doctor', ['specialty' => 'specialty-' . $model->shortSpecialtyOfDoctor->linkUrl])); ?>				
				</strong>
			</div>
		<?php endif; ?>
		<?php if (empty($clinics) && empty($doctors)): ?>
			<strong>Перейти к поиску врачей по специальности <a href="<?= $this->createUrl('search/doctor', array('specialty' => 'specialty-' .$model->shortSpecialtyOfDoctor->linkUrl)) ?>">
				<?= CHtml::encode($model->name) ?></a></strong>
		<?php endif; ?>
		<div class="both"></div>	
		<?php if ($model->doctorSpecialtyDiseases || $model->companyActivite): ?>
			<table class="useful_links <?= empty($clinics) && empty($doctors) ? 'mgtop10' : 'mgtop30' ?>">
				<thead>
					<tr>
						<td>Профиль деятельности</td>
						<td>Болезни, которые лечит этот врач</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php
							if ($model->companyActivite) {
								echo CHtml::Link(CHtml::encode($model->companyActivite->name), $this->createUrl('handbook/companyActivites', array('linkUrl' => $model->companyActivite->linkUrl))) . "<br>";
							}
							?>
						</td>
						<td>
							<?php if ($model->doctorSpecialtyDiseases): ?>
								<?php foreach ($model->doctorSpecialtyDiseases as $dsd): ?>
									<?php echo CHtml::Link(CHtml::encode($dsd->diseaseGroup->name), $this->createUrl('handbook/disease', array('linkUrl' => $dsd->diseaseGroup->linkUrl))) . "<br>"; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</td>					
					</tr>
				</tbody>			
			</table>
		<?php endif; ?>
	</div>	

</div>

