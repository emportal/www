<?php
/* @var $this SearchController */
/* @var $model CompanyActivite */

$this->pageTitle = "Клиники ".$model->genName." – $city – Цены, отзывы, адреса, запись в клинику ".$model->genName." – " . Yii::app()->name;
Yii::app()->clientScript->registerMetaTag("Запись в клинику ".CHtml::encode($model->genName)." - $city."
		. " Здесь собраны все адреса и отзывы по выбранному направлению."
		. " " . Yii::app()->name . " – клиники ".CHtml::encode($model->genName), 'description');

$this->breadcrumbs = array(
	'Профили клиник' => array('/handbook/companyActivity'),
	$model->name,
);
Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>

<div class="cont handbook">
	<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs)); ?>
	<div>
		<h1>Клиники <?= $model->genName ?> в <?= !empty($cityPrep) ? $cityPrep : $city ?></h1>
		<?= $model->noindex ? '<!--noindex-->' : '' ?>
		<p><?= $model->description ?></p>
		<?= $model->noindex ? '<!--/noindex-->' : '' ?>
		<!--<b>Перейти к поиску клиник с профилем деятельности <a href="<?= $this->createUrl('search/clinic', array('Search[companyActiviteId]' => $model->id)) ?>"><?= CHtml::encode($model->name) ?></a></b>-->
		<div>
			<?php if (!empty($clinics)): ?>		
				<div class="clinics">				
					<h2>Клиники</h2>
					<table>
						<?php foreach ($clinics as $cl): ?>							
							<?php 
								$this->renderPartial('//search/_clinics_unique',[
									'data' => $cl[0]->company,
									'address' => $cl[0],
									'addresses' => null,
									'big' => 0,	
									'style' => 'margin-bottom:25px'
								]);
							?>
						<?php endforeach; ?>
					</table>				
				</div>
			<?php endif; ?>
			<?php if (!empty($doctors)): ?>			
				<div class="doctors">
					<h2>Врачи</h2>
					<table>
						<?php foreach ($doctors as $doctor): ?>	
							<?php
							$groupedData = [
								$doctor->id => $doctor
							];
							$this->renderPartial('//search/_doctor_unique', [
								'data' => $doctor,
								'groupedData' => $groupedData,
								'style' => 'margin-bottom:25px'
							]);
							?>
						<?php endforeach; ?>
					</table>				
				</div>	
			<?php endif; ?>
			<div class="both"></div>
		</div>
		<?php if (!empty($clinics)): ?>		
			<div class="clinics">
				<strong>
					<?= CHtml::link('Перейти ко всем клиникам', $this->createUrl('/search/clinic', ['Search[companyActiviteId]' => $model->id])); ?>			
				</strong>
			</div>
		<?php endif; ?>
		<?php if (!empty($doctors)): ?>		
			<div class="doctors">
				<strong>			
					<?= CHtml::link('Перейти ко всем врачам', $this->createUrl('/search/doctor', $doctorSpecialtiesSearchArray)); ?>				
				</strong>
			</div>
		<?php endif; ?>
		<?php if (empty($clinics) && empty($doctors) && !empty($doctorSpecialtiesSearchArray)): ?>
			<strong>Перейти к поиску врачей по профилю деятельности <a href="<?= $this->createUrl('search/doctor', $doctorSpecialtiesSearchArray) ?>"><?= CHtml::encode($model->name) ?></a></strong>
		<?php endif; ?>
		<div class="both"></div>
	</div>
	<?php if ($doctorSpecialties): ?>
		<table class="useful_links <?= empty($clinics) && empty($doctors) ? 'mgtop10' : 'mgtop30' ?>">
			<thead>
				<tr>
					<td>Врачи, которые относятся к профилю</td>
					<td>Болезни</td>
				</tr>
			</thead>			
			<tbody>
				<?php foreach ($doctorSpecialties as $d): ?>	
					<tr>
						<td><?= CHtml::link(CHtml::encode($d->name), $this->createUrl('handbook/doctorSpecialty', array('linkUrl' => $d->linkUrl)), array('class' => 'normalizeA')) ?></td>
						<td>
							<?php
							foreach ($d->doctorSpecialtyDiseases as $dsd):
								echo CHtml::Link($dsd->diseaseGroup->name, $this->createUrl('handbook/disease', array('linkUrl' => $dsd->diseaseGroup->linkUrl))) . "<br>";
							endforeach;
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>

		</table>
	<?php endif; ?>
	<!--	<h2>Полезные ссылки</h2>
		<ul>
			<li><a href="#">Врачи, лечащие заболевание</a></li>
			<li><a href="#">Лекарства, применяемые для лечения заболевания</a></li>
			<li><a href="#">Какие органы поражает</a></li>
		</ul>-->
</div>

