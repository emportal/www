<?php
/* @var $this SearchController */
/* @var $model DoctorSpecialty */
$this->pageTitle = "Запись к ".$model->datName." – $city – Консультация врача ".$model->genName." – "
		. "Цены и время приема врачей, отзывы, адреса, онлайн запись – ".Yii::app()->name;
Yii::app()->clientScript->registerMetaTag("Запись на консультацию к ".CHtml::encode($model->datName)." - ".CHtml::encode($city)
		. " Здесь собраны все контакты, адреса и отзывы по выбранной специализации врачей."
		. " ".Yii::app()->name." – Запись к врачу ".CHtml::encode($model->datName),'description');
		
$this->breadcrumbs = array(
	'Специальности врачей' => array('/search/doctorSpecialty'),
	$model->name,
);
Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$('#doctors-grid').yiiGridView('update', {
		data: $(this).serialize()
});
		return false;
});
		");
?>


<div class="cont padding_left20px">
	<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs)); ?>
<div class="handbook">
	<h1>Запись к <?= $model->datName ?> - <?= $city ?></h1>
	<!--noindex-->
	<p><?= $model->description ?></p>
	<!--/noindex-->	
	<strong>Перейти к поиску врачей по специальности <a href="<?=$this->createUrl('search/doctor',array('Search[doctorSpecialtyId]'=>$model->id))?>"><?=CHtml::encode($model->name)?></a></strong>
</div>
	
	
	<?php if($model->doctorSpecialtyDiseases || $model->companyActivite): ?>
			<table class="useful_links">
				<thead>
					<tr>
						<td>Профиль деятельности</td>
						<td>Болезни, которые лечит этот врач:</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php 
							if($model->companyActivite){
								echo CHtml::Link(CHtml::encode($model->companyActivite->name),$this->createUrl('handbook/companyActivites',array('link'=>$model->companyActivite->link)))."<br>"; 
							}
							?>
						</td>
						<td>
							<?php if($model->doctorSpecialtyDiseases): ?>
								<?php foreach($model->doctorSpecialtyDiseases as $dsd): ?>
									<?= CHtml::Link(CHtml::encode($dsd->diseaseGroup->name),$this->createUrl('handbook/disease',array('link'=>$dsd->diseaseGroup->link)))."<br>" ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</td>					
					</tr>
				</tbody>			
			</table>
		<?php endif; ?>
	
	
<!--	<ul>
		<li><a href="#">Врачи, лечащие заболевание</a></li>
		<li><a href="#">Лекарства, применяемые для лечения заболевания</a></li>
		<li><a href="#">Какие органы поражает</a></li>
	</ul>-->
</div>

