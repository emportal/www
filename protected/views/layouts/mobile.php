<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/jquery/jquery.mobile-1.2.0.min.css" />
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jquery/jquery-1.8.3.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jquery/jquery.mobile-1.2.0.min.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU"></script>
    <script type="text/javascript" charset="utf-8">

    // Wait for Cordova to load
    //
    document.addEventListener("deviceready", onDeviceReady, false);

    // Cordova is ready
    //
    function onDeviceReady() {
    	checkConnection();
    	document.addEventListener("online", onOnline, false);
    	document.addEventListener("offline", onOffline, false);
    }

    // onSuccess Geolocation
    //
    
    var myMap;
    
    function onSuccess(position) {
       /* var element = document.getElementById('geolocation');
        element.innerHTML = 'Latitude: '           + position.coords.latitude              + '<br />' +
                            'Longitude: '          + position.coords.longitude             + '<br />' +
                            'Altitude: '           + position.coords.altitude              + '<br />' +
                            'Accuracy: '           + position.coords.accuracy              + '<br />' +
                            'Altitude Accuracy: '  + position.coords.altitudeAccuracy      + '<br />' +
                            'Heading: '            + position.coords.heading               + '<br />' +
                            'Speed: '              + position.coords.speed                 + '<br />' +
                            'Timestamp: '          +                                   position.timestamp          + '<br />';
                            
          */
  /*         myMap = new ymaps.Map("ymaps-map-id_13589806809772402736", {
              center: [59.95325, 30.21],
              zoom: 5
          });
        var
        // Первый способ задания метки
        myPlacemark = new ymaps.Placemark([position.coords.latitude, position.coords.longitude],{
            balloonContent: 'Вы тут!'
        }),
        // Второй способ
        myGeoObject = new ymaps.GeoObject({
            // Геометрия.
            geometry: {
                // Тип геометрии - точка
                type: "Point",
                // Координаты точки.
                coordinates: [59.95325, 30.8]
            }
        });

    // Добавляем метки на карту
    myMap.geoObjects
        .add(myPlacemark)
        .add(myGeoObject);
    
    	//showAlert(); */
    }

    // onError Callback receives a PositionError object
    //
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
    }
    function alertDismissed() {
        // do something
    }

    function showAlert() {
        navigator.notification.alert(
            'Вышла новая версия приложения',  // message
            alertDismissed,         // callback
            'Информационное сообщение',            // title
            'Хорошо'                  // buttonName
        );
    }
    // Beep three times
    //
    function playBeep() {
        navigator.notification.beep(1);
    }

    // Vibrate for 2 seconds
    //
    function vibrate() {
        navigator.notification.vibrate(2000);
    }
    
    function onOnline() {
    	showAlert();
    }
    
    function onOffline() {
    	noConnection();
    }

    function checkConnection() {
        var networkState = navigator.connection.type;

        if(networkState == 'none') {
        	noConnection();
    	}
	    else {
	    	startApp();
	    }
    }
    function noConnection() {
    	navigator.notification.alert(
                'Отсутствует подключение',  // message
                alertDismissed,         // callback
                'Критическая ошибка',            // title
                'Хорошо'                  // buttonName
            );
    	playBeep();
    }
    function startApp() {
    	navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }

    function exitFromApp()
    {
		navigator.app.exitApp();
    }
    function getMetro() {
    	
    	$.ajax({
    		type: "GET",
    		url: "http://dev.emportal.ru:10080/search/metro",
    		dataType: "json"
    		}).done(function( json ) {
    			myMap = new ymaps.Map("ymaps-map-id_13589806809772402736", {
		              center: [59.95325, 30.21],
		              zoom: 10
	          	});
    			// Создаем группу геообъектов и задаем опции.
    			var myGroup = new ymaps.GeoObjectArray({}, {
    			    preset: "twirl",
    			    strokeWidth: 4,
    			    geodesic: true
    			});
    			for (b in json) {
    				myGroup.add(new ymaps.Placemark([json[b].latitude, json[b].longitude],{
    		            balloonContent: json[b].name
    		        }));
    			}
    			//myGroup.add(new ymaps.Polyline([[13.38, 52.51], [30.30, 50.27]]));

    			// Добавляем группу на карту.
    			myMap.geoObjects.add(myGroup);
    			// Устанавливаем карте центр и масштаб так, чтобы охватить группу целиком.
    			//myMap.setBounds(myGroup.getBounds());
    			
    			/* myMap = new ymaps.Map("ymaps-map-id_13589806809772402736", {
    	              center: [59.95325, 30.21],
    	              zoom: 5
    	          });
   				var myCollection = new ymaps.GeoObjectArray({}, {
   			       preset: 'twirl#redIcon', //все метки красные
   			    });
    			
    			for (b in json) {
    				myCollection.add(new ymaps.Placemark([json[b].latitude, json[b].longitude],{
    		            balloonContent: json[b].name
    		        }));
    			}
    			
    			myMap.geoObjects.add(myCollection); */
    		});
    	//http://dev.emportal.ru:10080/search/metro
    }
    </script>
</head>

<body>
<div data-role="page"> 
	<div data-role="header"><h1>ЕМП</h1></div> 
	<div data-role="content">
	<div class="map">
		<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (начало) -->
		<div id="ymaps-map-id_13589806809772402736" style="width: 100%; height: 350px;"></div>

		
	</div>

		<ul data-role="listview" data-inset="true">
	    	<li><a href="#category-items?category=submit">Записаться</a></li>
	    	<li><a href="#category-items?category=cat">Каталоги</a></li>
	    	<li><a href="#category-items?category=info">Информация</a></li>
	    	<li><a href="#" onclick="getMetro(); return false;">Метро</a></li>
	    	<li><a href="#" onclick="showAlert(); return false;">Show Alert</a></li>
		    <li><a href="#" onclick="playBeep(); return false;">Play Beep</a></li>
		    <li><a href="#" onclick="vibrate(); return false;">Vibrate</a></li>
			    	
    	</ul>
	</div> 
	<div data-role="footer">
		<h4>Page Footer</h4>
	</div><!-- /footer -->
</div>

<script type="text/javascript">

</script>
</body>
</html>