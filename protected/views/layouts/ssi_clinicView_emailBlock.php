<?php if (empty(Yii::app()->request->cookies['clinic' . $link])): ?>
<!-- 		<div class="report-clinic">
			Не нашли ничего подходящего?
			<br>
			<span data-id="<?= $link ?>" class="blueLink" style="font-size: 10px; cursor: pointer;">
				Пожаловаться на отсутствие клиники
			</span>
		</div> -->
<?php endif; ?>
<?php if (empty(Yii::app()->request->cookies['clinic' . $link])): ?>
		<div id="clinicSave" data-save="1" data-link="<?=$link;?>"></div>
<?php endif; ?>
<!-- лидогенерация неактивной клиники -->
<div id="emailModal" class="md-modal popup">
	<div class="md-close">X</div>
	<div class="md-content">
		<div class="md-form-container">
			<div id="#emailSubscriptionForm">
				<div>
					 К сожалению, запись в эту клинику закрыта! Пожалуйста, оставьте свой e-mail, чтобы мы сообщили вам, когда запись на прием в эту клинику возобновится. 
				</div>
				<div class="vmargin20">
				<?php
					$emailBlock = new EmailBlock;
					$emailBlock->options = [
						'blockId' => 'trackClinicEmailBlock',
						'emailAttributes' => [
							'data-type' => '4',
							'data-companyUrl' => $companyLinkUrl,
							'data-doctorUrl' => $linkUrl
						]
					];
					$emailBlock->run();
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
<script>
	var emailModal = {}; //accepted globally
	$(function() {
		var options = {
			'id': 'emailModal',
			//'templateId': 'htmlTemplate',
		};
		emailModal = new ModalWindowConstructor(options);
		if($('#clinicSave').attr('data-save') == 1) {
			emailModal.show();
			var cName = 'clinic'+$('#clinicSave').attr('data-link');
			document.cookie = cName + "=1; path=/";
		}
	});
</script>
<!-- //лидогенерация неактивной клиники -->
								