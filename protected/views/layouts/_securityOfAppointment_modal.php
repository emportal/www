<style>
#securityOfAppointment p {
	font-size: 13px;
}
.md-title h3 {
	color: rgb(51,51,51);
}
</style>
<!-- Модальное окно: securityOfAppointment  -->
<div id="securityOfAppointment" class="md-modal md-effect-1" style="width: 900px; max-width: 900px;">
	<div class="md-close">X</div>
    <div class="md-content" style="min-height: 450px;">
    	<div class="md-title"><h2 class="h3">Почему запись на прием через ЕМП безопасна?</h2></div>
        <div class="md-form-container">
        	<div style="">
  				<img src="/images/drhouse/house_01_m.jpg" style="max-width: 240px; float: left; margin: -25px 40px 0 20px; padding-bottom: 35px;" />
  				<div style="margin-top: 40px; margin-left: 40px; text-align: left;">
					<p>	+ Использование сервиса абсолютно бесплатно для всех пациентов.</p>
					<p>	+ Цены на услуги клиник при записи на прием через наш сайт идентичны стандартным прайсам. Ни клиника, ни наш портал не делают никаких наценок на лечение.</p>
					<p>	+ Напротив, ЕМП регулярно проводит скидочные акции совместно с различными медицинскими центрами Петербурга, что помогает нашим пользователям сэкономить деньги.</p>
					
					<p>	+ С любой из клиник, доступной для записи на портале, мы имеем официальные соглашения о сотрудничестве.</p>
					<p>	+ Мы работаем только с проверенными компаниями.</p>
					<p>	+ Каждая запись на прием к врачу тщательно контролируется нашими специалистами на всех этапах.</p>
					
					<br><br>
					<p>	Узнать больше <a href="/about_us.html" target="_blank">про проект</a>.</p>
					<p> Узнать больше <a href="/team.html" target="_blank">про команду</a>.</p>
				</div>
			</div>
		</div>
		<div class="md_submit" style="padding-top: 35px;">
			<input id="md_submit" type="submit" value="Хорошо" class="md_submit_btn btn-green">
		</div>
    </div>
    <div class="md-after-content" id="md_inProcess" style="display: none;">
    	<div class="md-title">Хорошо</div>
    </div>
</div>
<div class="md-overlay"></div>
<script>
function ModalWindow(WindowId) {
	this.id = WindowId;
}

ModalWindow.prototype.show = function (args) {
	$( "#"+this.id ).removeClass( "md-show" );
	$( "#"+this.id ).addClass( "md-show" );

	$("#"+this.id+" .md-close").unbind( "click" );
	$("#"+this.id+" .md-close").bind( "click", function(self) { return function(event){
		self.close(self.id);
	}; }(this));

	$("#"+this.id+" #md_submit").unbind( "click" );
	$("#"+this.id+" #md_submit").bind( "click", function(self) { return function(event){
		self.submit(self.id);
	}; }(this));

	$("#"+this.id+" .md-content").show();
	$("#"+this.id+" .md-after-content").hide();
};

ModalWindow.prototype.close = function () {
	$("#"+this.id).removeClass( "md-show" );
};

ModalWindow.prototype.submit = function () {
	this.close();
};
var securityOfAppointment = new ModalWindow("securityOfAppointment");

$('.md-overlay').bind( "click", function() { securityOfAppointment.close(); });
</script>