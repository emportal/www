<?php
	$path = Yii::getPathOfAlias('shared');
	$pathTojRating = $path . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'jRating' . DIRECTORY_SEPARATOR;
	$urlScript = Yii::app()->assetManager->publish($pathTojRating . 'jRating.jquery.js');
	Yii::app()->clientScript->registerScriptFile($urlScript, CClientScript::POS_END);
	$urlcss = Yii::app()->assetManager->publish($pathTojRating . 'jRating.jquery.css');
	Yii::app()->clientScript->registerCssFile($urlcss);
	
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/shared/css/review_box.css');
	Yii::app()->clientScript->registerCoreScript('jquery.maskedinput');
?>
	<div class="showPostReview" style="display: none;">
		<div class="reviewForm">
			<div class="reviewContent">
			<h5 class="reviewContent_header"><div class="icon_comments_mini"></div> Добавить отзыв</h5>
  
			<!-- <p class="note">Поля, помеченные <span class="required">*</span> обязательны для заполнения.</p> -->
			<input name="Review[reviewId]" id="Review_reviewId" type="hidden" value="">
			<input name="Review[companyId]" id="Review_companyId" type="hidden" value="<?= $data['companyId'] ?>">
			<input name="Review[addressId]" id="Review_addressId" type="hidden" value="<?= $data['addressId'] ?>">
			<input name="Review[appointmentToDoctorsId]" id="Review_appointmentToDoctorsId" type="hidden" value="<?= $appointment->id ?>">
			<input name="Review[doctorId]" id="Review_doctorId" type="hidden" value="<?= $data['doctorId'] ?>">
			<table> <?php /* см. стили в shared/css/review_box */ ?>
				<tbody>
				<tr>
					<td colspan="2">
						<label for="Review_userName" class="required">Ваше имя <span class="required">*</span></label>
						
						<br>
						
						<input name="Review[userName]" class="custom-text" id="Review_userName" placeholder="Ваше имя" type="text" value="<?=CHtml::encode(Yii::app()->user->fullname)?>">
						<span style="display: block; vertical-align: bottom; padding: 10px 0;">
							<input type="checkbox" name="option1" onclick="var userInput = $( this ).closest( '.reviewForm' ).find('#Review_userName'); if(this.checked) { userInput.val('Аноним'); userInput.prop('disabled', true); } else { userInput.val('<?=CHtml::encode(Yii::app()->user->fullname)?>'); userInput.prop('disabled', false); }">
							<label>Анонимно</label>
						</span>
					</td>
				</tr>
				<tr id="ratingDoctor_block">
					<td>
						<label for="Review_ratingDoctor" class="required">Рейтинг врача <span class="required">*</span></label>
					</td>
					<td>
						<div class="jRatingView" data-average="5" data-id="1" id="Review_ratingDoctor" style="display:inline-block">
							<input name="Review[ratingDoctor]" type="hidden">
						</div>
						<div class="errorMessage" id="ratingDoctor_Message" style="display:none; color:red;">Поставьте оценку!</div>
					</td>
				</tr>
				<tr>
					<td>
						<label for="Review_ratingClinic">Рейтинг клиники</label>
					</td>
					<td>
						<div class="jRatingView" data-average="5" data-id="1" id="Review_ratingClinic" style="display:inline-block">
							<input name="Review[ratingClinic]" type="hidden">
						</div>
						<div class="errorMessage" id="ratingClinic_Message" style="display:none; color:red;">Поставьте оценку!</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="Review_reviewText">Ваш отзыв</label>
						<br>
						<textarea style="width: 100%; height: 100px; padding: 10px;" placeholder="Текст отзыва" class="custom-text" name="Review[reviewText]" id="Review_reviewText"></textarea>
					</td>
				</tr>
				</tbody>
			</table>
			<div class="row buttons">
				<input style="width: 100%;" type="submit" name="yt0" value="Отправить" class="btn-green btn_doctor_sign vmargin10" onclick="postReview(this);">
			</div>
			</div>
			<div class="toConfirmForm" style="display: none;">
				<h5>Подтвердите отзыв</h5>
				<p>Для публикации отзыва, его необходимо подтвердить!</p>
				<div id="phonetoConfirmContainer" style="margin: 10px;">
					<form action="#" onsubmit="getTheСode(this); return false;" style="display: inline-block;">
						<input placeholder="Ваш телефон" class="custom-text" style="" id="phonetoConfirmInput" name="phoneInput" type="text">
					</form>
					<input type="submit" value="Получить код" class="btn-green vmargin10" onclick="getTheСode(this);">
					<div class="errorMessage" id="toConfirmForm_phone_errorMessage" style="display:none; color:red;">Необходимо заполнить поле Телефон</div>
					<span class="phone-help" style="display: block;">Вам будет прислан код подтверждения</span>
					
				</div>
				<div id="сonfirmCodeContainer" style="margin: 10px; display: none;">
					<form action="#" onsubmit="confirmReview(this); return false;" style="display: inline-block;">
						<input placeholder="Код подтверждения" class="custom-text" style="" id="сonfirmCodeInput" name="phoneInput" type="text">
					</form>
					<input type="submit" value="Подтвердить отзыв" class="btn-green vmargin10" onclick="confirmReview(this);">
					<div class="errorMessage" id="сonfirmCode_errorMessage" style="display:none; color:red;">Необходимо ввести код подтверждения</div>
					<span class="phone-help" style="display: block;">Код подтвердит Ваш отзыв</span>
				</div>
			</div>
			<div class="errorMessage" id="reviewForm_Message" style="display:none; color:red;"></div>
		</div>
	</div>
		<script type="text/javascript">
			function postReview(obj) {
				
				var form = $( obj ).closest( ".reviewForm" );
				var review = {};
				var noerror = true;
				review.companyId = form.find("#Review_companyId").val().trim();
				review.addressId = form.find("#Review_addressId").val().trim();
				review.appointmentToDoctorsId = form.find("#Review_appointmentToDoctorsId").val().trim();
				review.doctorId = form.find("#Review_doctorId").val().trim();
				review.userName = form.find("#Review_userName").val().trim();
				review.ratingDoctor = parseFloat(form.find("#Review_ratingDoctor input").val().trim());
				review.ratingClinic = parseFloat(form.find("#Review_ratingClinic input").val().trim());
				review.reviewText = form.find("#Review_reviewText").val();
				if(!review.userName) {
					form.find("#Review_userName").css("border","solid red 1px");
					form.find("#Review_userName").closest('tr').css('color', 'red');
					noerror = false;
				}
				else {
					form.find("#Review_userName").css("border","");
					form.find("#Review_userName").closest('tr').css('color', '');
				}
				if(form.find('#Review_doctorId').val().trim() != "") {
					if(isNaN(review.ratingDoctor)) {
						form.find("#ratingDoctor_Message").css("display", "inline-block");
						form.find("#ratingDoctor_Message").closest('tr').css('color', 'red');
						noerror = false;
					}
					else {
						form.find("#ratingDoctor_Message").hide();
						form.find("#ratingDoctor_Message").closest('tr').css('color', '');
					}
				} else {
					if(isNaN(review.ratingClinic)) {
						form.find("#ratingClinic_Message").css("display", "inline-block");
						form.find("#ratingClinic_Message").closest('tr').css('color', 'red');
						noerror = false;
					}
					else {
						form.find("#ratingClinic_Message").hide();
						form.find("#ratingClinic_Message").closest('tr').css('color', '');
					}
				}
				form.find("#reviewForm_Message").hide();
				if(noerror) {
					var set = {
						url: "/?r=ajax/postReview",
						data: { review : review },
						method:'POST',
						dataType:'json',
						success: function(data, textStatus, XHR) {
							form.find("#reviewForm_Message").css("color", "green");
							form.find("#reviewForm_Message").show();
							form.find("#reviewForm_Message").text(data.text);
							if(data.status == "OK") {
								form.find(".reviewContent").hide();
								form.find(".toConfirmForm").hide();
								if(data.autologin) {
									location.reload();
								}
								isSuccess(form);
							} else if(data.status == "TOCONFIRM") {
								form.find("#Review_reviewId").val(data["reviewId"]);
								form.find(".reviewContent").hide();
								form.find(".toConfirmForm").show();
							} else {
								this.error(XHR, data.text, data.status)
							}
							//console.log(data);
						},
						error: function(XHR, textStatus, errorThrown) {
							form.find("#reviewForm_Message").css("color", "red");
							form.find("#reviewForm_Message").show();
							form.find("#reviewForm_Message").text(textStatus);
						},
						complete: function(data) {
							$( obj ).prop('disabled', false);
				        }
						
					};
					$( obj ).prop('disabled', true);
					$.ajax(set);
				}
			}
			
			function isSuccess(form) {
				setTimeout( function() {
					form.hide();
					form.find(".toConfirmForm").hide();
					form.find(".reviewContent").show();
					parent.$.fancybox.close();
				} , 1000);
			}

			function getTheСode(obj) {
				var noerror = true;
				var form = $( obj ).closest( ".reviewForm" );
				var phonetoConfirm = form.find("#phonetoConfirmInput").val();
				var reviewId = form.find("#Review_reviewId").val();
				validatePhone(phonetoConfirm, function() {
					form.find("#toConfirmForm_phone_errorMessage").hide();
					form.find("#phonetoConfirmInput").css("border", "");
				}, function() {
					noerror = false;
					form.find("#toConfirmForm_phone_errorMessage").show();
					form.find("#phonetoConfirmInput").css("border","solid red 1px");
				});
				form.find("#reviewForm_Message").hide();
				if(noerror) {
					var set = {
						url: "/?r=ajax/confirmReview",
						data: { phone: phonetoConfirm , reviewId: reviewId },
						method:'POST',
						dataType:'json',
						success: function(data, textStatus, XHR) {
							form.find("#reviewForm_Message").css("color", "green");
							form.find("#reviewForm_Message").show();
							form.find("#reviewForm_Message").text(data.text);
							if(data.status == "OK") {
								form.find("#phonetoConfirmContainer").hide();
								form.find("#сonfirmCodeContainer").show();
							} else {
								this.error(XHR, data.text, data.status)
							}
						},
						error: function(XHR, textStatus, errorThrown) {
							form.find("#reviewForm_Message").css("color", "red");
							form.find("#reviewForm_Message").show();
							form.find("#reviewForm_Message").text(textStatus);
						},
						complete: function(data) {
							$( obj ).prop('disabled', false);
				        }
						
					};
					$( obj ).prop('disabled', true);
					$.ajax(set);
				}
			}

			function confirmReview(obj) {
				if(true) {
					var form = $( obj ).closest( ".reviewForm" );
					var confirmCode = form.find("#сonfirmCodeInput").val();
					var reviewId = form.find("#Review_reviewId").val().trim();
					form.find("#сonfirmCode_errorMessage").hide();
					form.find("#reviewForm_Message").hide();
					var set = {
						url: "/?r=ajax/confirmReview",
						data: { confirmCode: confirmCode , reviewId: reviewId },
						method:'POST',
						dataType:'json',
						success: function(data, textStatus, XHR) {
							form.find("#reviewForm_Message").css("color", "green");
							form.find("#reviewForm_Message").show();
							form.find("#reviewForm_Message").text(data.text);
							if(data.status == "OK") {
								form.find(".reviewContent").hide();
								form.find(".toConfirmForm").hide();
								form.find("#phonetoConfirmContainer").show();
								form.find("#сonfirmCodeContainer").hide();
								if(data.autologin) {
									location.reload();
								}
								isSuccess(form);
							} else {
								this.error(XHR, data.text, data.status)
							}
						},
						error: function(XHR, textStatus, errorThrown) {
							form.find("#reviewForm_Message").css("color", "red");
							form.find("#reviewForm_Message").show();
							form.find("#reviewForm_Message").text(textStatus);
						},
						complete: function(data) {
							$( obj ).prop('disabled', false);
				        }
						
					};
					$( obj ).prop('disabled', true);
					$.ajax(set);
				}
			}
			
			function validatePhone(phone, onsuccess, onerror) {
			    var regex = /^\+[7][0-9]{6,20}$/;
			    if (regex.test(phone)) {
			    	onsuccess();
			    } else {
			    	onerror();
			    }
			}
			
			function initRatings(form) {
				
				form.find(".jRatingView").jRating({
					step:true,
					showRateInfo:false,
					length : 5, // nb of stars
					decimalLength : 1,
					rateMax : 5,
					nbRates : 9999999999,
					sendRequest:false,
					canRateAgain:true,
					onSuccess : function(){
						jSuccess('Success : your rate has been saved :)',{
							HorizontalPosition:'center',
							VerticalPosition:'top'
						});
					},
					onError : function(){
						jError('Error : please retry');
					},
					onClick : function(element,rate) {
						if(rate <= 0 || isNaN(rate)) rate = '';
						else rate /= 5;
						$(element).find("input").val(rate);
					},
				});
			       
				form.find('#Review_ratingDoctor input').val('');
				form.find('#Review_ratingClinic input').val('');
			    form.find('#Review_ratingDoctor.rating-container .star').click(function() {
			        $('#Review_ratingDoctor.rating-container .star').prop('src', '/images/Five-pointed_star_1.png');
			        $(this).prevAll('.star').addBack().prop('src', '/images/Five-pointed_star_2.png');
			        $('#Review_ratingDoctor input').val($(this).attr("name"));
			    });
			    form.find('#Review_ratingClinic.rating-container .star').click(function() {
			        $('#Review_ratingClinic.rating-container .star').prop('src', '/images/Five-pointed_star_1.png');
			        $(this).prevAll('.star').addBack().prop('src', '/images/Five-pointed_star_2.png');
			        $('#Review_ratingClinic input').val($(this).attr("name"));
			    });
				
				if(form.find('#Review_doctorId').val().trim() == "") {
					form.find('#ratingDoctor_block').hide();
				}
				
			    try {
					form.find("#phonetoConfirmInput").mask("+79999999999", {placeholder: "_"});
			    }
			    catch(e) {}
			};
			
			//function jRatingInit() {
			//	console.log('ini!');
			//}
		</script>