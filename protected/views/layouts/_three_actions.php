<div class="SECTION ACTIONS" style="display: none; overflow: hidden !important;">
	<div class="main_column">
		<a class="btn-close" id="instruction_close" href="#instruction_close"></a>
		<div class="header_section">
			<h2>Запись в 3 действия</h2>
		</div>
		<div class="action_block">
			<h3 class="action_header">1. Найдите врача</h3>
			<img src="/images/newLayouts/action_step_1.png" style="float: right; min-width: 360px; min-height: 280px;" usemap="#map_action_step_1">
			<map id="map_action_step_1" name="map_action_step_1">
	    		<area shape="poly" coords="61,149,154,149,154,180,61,180" href="/doctor" alt="Записаться на приём">
			</map>
		</div>
		<div class="action_block">
			<h3 class="action_header">2. Выберите удобное время, введите ваше имя и телефон</h3>
			<img src="/images/newLayouts/action_step_2.png" style="min-width: 280px; min-height: 280px;" usemap="#map_action_step_2">
			<map id="map_action_step_2" name="map_action_step_2">
	    		<area shape="poly" coords="72,206,211,206,211,232,72,232" href="/doctor" alt="Записаться на приём">
			</map>
		</div>
		<div class="action_block">
			<h3 class="action_header">3. Ожидайте звонка из клиники</h3>
			<img src="/images/newLayouts/action_step_3.png" style="min-width: 160px; min-height: 280px;">
		</div>
	</div>
</div>

<?php 
Yii::app()->clientScript->registerScript("three_actions_controller", "
$( document ).ready(function() {
	var self = this;
	$('.btn-instruction').bind('click', function() {
		if(!self.scrollNow) {
			self.scrollNow = true;
			$('.SECTION.ACTIONS').slideDown(500);
			$('html, body').animate({ scrollTop: 0 }, 500, function() {
				self.scrollNow = false;
			});
		}
		return false;
	});
	$('#instruction_close').bind('click', function() {
		$('.SECTION.ACTIONS').slideUp(500);
		return false;
	});
});", CClientScript::POS_END);
?>