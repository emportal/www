<?php
if(!MyTools::desktop_version()) {
	$urlToMobileCss = Yii::app()->assetManager->publish( (Yii::getPathOfAlias('shared').DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'mobile.css') );
	Yii::app()->clientScript->registerCssFile($urlToMobileCss."?v=32");
	Yii::app()->clientScript->registerMetaTag("width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no", "viewport");
}
?>
<?php if(Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']): ?>
	<?php
	$emiasScript = Yii::app()->assetManager->publish( (Yii::getPathOfAlias('shared').DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'emias.js') );
	Yii::app()->clientScript->registerScriptFile($emiasScript.'?v=2', CClientScript::POS_HEAD);
	?>
	<script>
	var emias = new Emias;
	$(function(){
	    $(document).ready(function() {
			emias.omsNumber = $.cookie('OMSForm_omsNumber');
			emias.omsSeries = $.cookie('OMSForm_omsSeries');
			emias.birthDate = $.cookie('OMSForm_birthday');
			$("#Search_doctorSpecialtyId").on('change', function() {
					try {
						emias.unselectReferral(false);
					} catch(e){}
				});
			emias.checkReferrals();
	    });
	});
	</script>
<?php endif; ?>
<!--# block name="section_white_head" -->
<?= $this->renderPartial("//layouts/ssi_white_head", []); ?>
<!--# endblock -->
<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_white_head" stub="section_white_head" -->