<?php
/* Получаем данные для счетчиков в шапке сайта */
$cityId = (new Search())->cityId;
$cCriteria = new CDbCriteria();
$cCriteria->with = [
			'company' => [
				'with' => [
					'companyType',
					'category',
				],
			],
			'userMedicals' => [
				'together' => true,
			],
			
		];

$cCriteria->group = 't.id';
$cCriteria->compare('companyType.isMedical', 1);
$cCriteria->compare('t.cityId', $cityId);
//$cCriteria->compare('company.denormCitySubdomain', Yii::app()->session['selectedRegion'] ? Yii::app()->session['selectedRegion'] : 'spb'); 
$cCriteria->compare('t.isActive', 1);
$cCriteria->compare('company.removalFlag', 0); 
$cCriteria->compare('userMedicals.agreementNew', '1', false);
//$cCriteria->scopes = 'clinic';
$cCriteria->compare("t.samozapis", (Yii::app()->params["samozapis"]) ? 1 : 0);
$clinicCount = Address::model()->count($cCriteria);

$dCriteria = new CDbCriteria();
$dCriteria->with = [
	'placeOfWorks' => [
		'together' => true,
		'select' => false,
		'with' => [
			'address' => [
				'select' => "id,link,cityId",
				'together' => true,		
				'with' => [
					'company' => [
						'together' => true,
						'select' => false,
						'with' => [
							'companyContracts' => [
								'joinType' => 'INNER JOIN',
								'select' => false,
								'together' => true
							]
						]
					],
					'userMedicals' => [
						'joinType' => 'INNER JOIN',
						'on' => 'userMedicals.agreementNew = 1',
						'together' => true
					]
				]
			]
		],
	],	
];

$dCriteria->compare('address.cityId', $cityId);
$cCriteria->compare('company.removalFlag', 0); 
$dCriteria->compare("address.samozapis", (Yii::app()->params["samozapis"]) ? 1 : 0);
$doctorCount = Doctor::model()->count($dCriteria);


$newDateTime = new DateTime();
$newDateTime->setTimezone(new DateTimeZone('Europe/Moscow'));
$AppointmentsCountType = Config::model()->findByAttributes(["name" => "Отображать желаемое количество записей на приём"]);
$AppointmentsCountTypeValue = "0";
if($AppointmentsCountType) {
	$AppointmentsCountTypeValue = $AppointmentsCountType->value;
}
switch ($AppointmentsCountTypeValue) {
	case "1":
		$appointmentCount = ceil(((time()-strtotime($newDateTime->format('Y-m-d'))) -  3600*7)/60/4);
	break;
	default:
		$appointmentCount = AppointmentToDoctors::model()->count('createdDate >= CURDATE() AND statusId<>\''.AppointmentToDoctors::DISABLED.'\'');
	break;
}
if($appointmentCount < 0) $appointmentCount = 0;
?>

	<div class="SECTION ICONS_BOTTOM">
		<div class="main_column">
			<a href="/doctor" class="stats_block icon-link">
				<div class="stats_icon_doctor"></div>
				<div class="stats_value"><?= $doctorCount ?></div>
				<div class="stats_description"><span><?= MyTools::createCountableForm($doctorCount, "Врач", "Врача", "Врачей") ?></span><br> в базе</div>
			</a>
			<a href="/klinika" class="stats_block icon-link">
				<div class="stats_icon_hosp"></div>
				<div class="stats_value"><?= $clinicCount ?></div>
				<div class="stats_description"><span><?= MyTools::createCountableForm($clinicCount, "Клиника", "Клиники", "Клиник") ?></span><br> в базе</div>
			</a>
			<div class="stats_block">
				<div class="stats_icon_check"></div>
				<div class="stats_value"><?= $appointmentCount ?></div>
				<div class="stats_description">
					<span><?= MyTools::createCountableForm($appointmentCount, "Запись", "Записи", "Записей") ?> на приём<br> сегодня<span class="none-in-responsive">*</span></span>
					<div class="stats_description_annotation">*по всему порталу</div>
				</div>
			</div>
		</div>
	</div>