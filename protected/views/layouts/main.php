<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name='yandex-verification' content='5b3c94a54b4278bf' />
	<meta name='yandex-verification' content='5181a6638b6f7dd6' />
	<meta name="apple-itunes-app" content="app-id=887827260" />
	<title><?= CHtml::encode($this->pageTitle) ?></title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/shared/js/jquery.pie.min.js"></script>
	<![endif]-->
	<!-- Google Analytics counter -->
	<script> 
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga'); 

		ga('create', 'UA-50143164-1', 'auto'); 
		ga(function(tracker) { 
			var clientId = tracker.get('clientId');
			document.cookie = "_ga_cid=" + clientId + "; path=/"; 
			ga('set', 'dimension1', clientId); 
		}); 
		ga('send', 'pageview'); 
	</script>
	<!-- /Google Analytics counter -->
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter18794458 = new Ya.Metrika({id:18794458,
						webvisor:true,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						trackHash:true});
			} catch(e) { }
		});
	
		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
	
		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
</head>
<body>
	<div class="<?=ucfirst(Yii::app()->controller->id)."Controller"?> <?="action".ucfirst(Yii::app()->controller->action->id)?>">
	<?php echo $content; ?>
	</div>
	
	<!-- Yandex.Metrika counter -->
	<noscript>
		<div>
			<img src="//mc.yandex.ru/watch/18794458" style="position:absolute; left:-9999px; height: 1; width: 1;" alt="" />
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->
    <?php /*
	<noscript>
		<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=368420073309799&ev=PixelInitialized"/>
	</noscript> 
	<script>
	(function() {
		var _fbq = window._fbq || (window._fbq = []);
		if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[ 0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
		}
		_fbq.push(['addPixelId', '368420073309799']);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', 'PixelInitialized', {}]);
	</script>
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1477929245843724');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1477929245843724&ev=PageView&noscript=1"
	/></noscript>
	<!-- /Facebook Pixel Code -->
	
	<!-- вконтакте ретаргетинг -->
	<?php if (isset(Yii::app()->session['selectedRegion'])) {
		$vkrtrgScriptText = "(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=tPKC2YTJzEDoqYIAMyw38d6sdxteggV6Zw1KoPJdJjWYXn3h/v*VYumxuXfqHHQv5xaVxvv231HFjEMEy*FpIdaHj5tAj9rt97xt8Kngq7BqHztTXp1N3/8RUdylAhZO/5xfwaCd/cg8ubohBNfizq5q0QIAPy4N1HN1gX21tQ0-';";
		switch (Yii::app()->session['selectedRegion']) {
			case "spb":
				if (Yii::app()->params['samozapis']) {
					$vkrtrgScriptText = "(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=PtUnjKj2Ctqz90J3FvhYV53w8nJ3MU0gb7ImL5T0NUpxADq/p1NowCSioS8gXoIUU77kDYLNoTB*PAMiJFx25KONg7pE9onvqm10K/Afg*VAviZ2Cu*TagHBkvNCJ74gFtPfrFqKw1IsBEGkHqIG2XayU1h4Y6LGQVE1Jladn4Y-';";
				}
				break;
			case "moskva":
				$vkrtrgScriptText = "(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=JMjBBnWwD13AM4VzzqrfWlI32XTJ97kxFej7X41q9YKXVrCpoA*jkD2TdIAlTH8Xqz7DPRFq0d10XMakBo3ZzuOvNo*3rey4T72pckG/Cc1qqWJN42fSkam*za0ejz9RzzCUpXh/1IhfbRUQrQo1lv0FWU213hl8jR4T7sHzm3I-';";
				break;
			case "nizhny-novgorod":
				$vkrtrgScriptText = "(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=ZLHIwH3nnrx8ucLNUj/JwH8ih94osvx7DY9McogIG2ZFhHJX2zkfGEC3V0Z4IyFJZW/UsDcWP5pTZuDVyBveSTbTmaoXlYS4EdD5Ml44/0qWFw1q7ZCZvwfzY5VbRjewkeKJQzTX2r3pmSmX94*9YJ8cWDLH82q3a8tg4P1Ym/w-';";
				break;
			case "novosibirsk":
				$vkrtrgScriptText = "(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=Rn0bSmfUBwOzkOpGwtNjUoXErnjAPE54t141Io/9oYrKraRtzosnZlQhF6siBhZE/RuYH*C82rFt3eQoqnCYfxJ/pveWN8U/r6qpqLrM4JHXOpznP7WkR0PNnRCn21l75qQPZnVnxQs*2pLwvbF9QaTU*R3U7KI1KtVEi*zLrOA-';";
				break;
		}
		Yii::app()->clientScript->registerScript('vkrtrg', $vkrtrgScriptText, CClientScript::POS_READY);
	} ?>
	<!-- /вконтакте ретаргетинг -->
    */?>
</body>
</html>
