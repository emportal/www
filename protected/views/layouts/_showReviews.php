<?php if (Yii::app()->request->isAjaxRequest) : ?>
	<?php if (!empty($reviews)): ?>
	<?php foreach($reviews as $key=>$review): if(empty($review->reviewText)) continue; ?>
		<div class="review_block_item">
			<p class="review_text"><?= htmlspecialchars($review->reviewText) ?></p>
			<div class="author_block">
				<div class="review_author_avatar">
					<img class="author_avatar_micro" src="<?= $review->userAvatarSrc ?>">
					<img class="review_author_proof" src="/images/newLayouts/icon-proof-micro.png">
				</div>
				<div class="author_sign">
					<div class="author_name"><?= htmlspecialchars($review->userName) ?></div>
					<div class="date-time"><?= RuDate::post($review->createDate) ?></div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<?php endif; ?>
<?php else: ?>
	<div class="information" id="reviews">
		<a class="roll-more opened"><span class="information_header"><span class="icon_comments_micro"></span>Отзывы<span class="color2">(<span class="review_count"><?=count($reviews);?></span>)</span></span></a>
		<div class="more">
			<button class="btn-blue" onclick="javascript: showPostReview()">Добавить отзыв</button>
			<span>
				<span class="icon_safe_micro"></span><span class="review_safe_label">Все отзывы проверяются</span>
			</span>
			<?php if (!empty($reviews)): ?>
				<?php foreach($reviews as $key=>$review): if(empty($review->reviewText)) continue; ?>
					<div class="review_block_item" itemscope itemtype="http://schema.org/Review">
						<p class="review_text" itemprop="reviewBody"><?= htmlspecialchars($review->reviewText) ?></p>
						<div class="author_block">
							<div class="review_author_avatar">
								<img class="author_avatar_micro" src="<?= $review->userAvatarSrc ?>">
								<img class="review_author_proof" src="/images/newLayouts/icon-proof-micro.png">
							</div>
							<div class="author_sign" itemprop="author" itemscope itemtype="http://schema.org/Person">
								<div class="author_name" itemprop="name"><?= htmlspecialchars($review->userName) ?></div>
								<div class="date-time"><?= RuDate::post($review->createDate) ?></div>
							</div>
							<meta itemprop="datePublished" content="<?= $review->createDate ?>" />
							<span style="display: none;"><a href="<?= Yii::app()->request->requestUri ?>" itemprop="url"></a></span>
						</div>
						<span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Organization" style="display: none;">
							<meta itemprop="name" content="<?= $review->address->company->name;?>" />
							<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<meta content="<?= CHtml::encode($review->address->street) ?>, <?= CHtml::encode($review->address->houseNumber) ?>" itemprop="streetAddress">
	    						<meta content="<?=$review->address->city->name;?>" itemprop="addressLocality">								
							</span>
						</span>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>
		</div>
		<!-- <div class="icon_loading"></div> -->
	</div>
	<script type="text/javascript">
		function loadReview(obj) {
			$.ajax({
				url: "/?r=ajax/showReviews",
				data: {
					'attributes' : 
						{
							<?= isset($data['companyId']) ? "companyId: ".Yii::app()->db->quoteValue($data['companyId'])."," : "" ?>
							<?= isset($data['addressId']) ? "addressId: ".Yii::app()->db->quoteValue($data['addressId'])."," : "" ?>
							<?= isset($data['doctorId']) ? "doctorId: ".Yii::app()->db->quoteValue($data['doctorId'])."," : "" ?>
						}
				},
				method:'GET',
				success: function(data, textStatus, XHR) {
					if(data.length > 1) {
						$(".information#reviews .more").append(data);
					}
				},
				error: function(XHR, textStatus, errorThrown) {
					console.log(textStatus);
				},
				complete: function(data) {
					$(".information#reviews .icon_loading").hide();
					var review_count = $(".information#reviews .review_block_item").length;
					if(review_count > 0) {
						$(".review_count").text(review_count);
						$(".review_comments").text("проверенных отзывов");
					}
		        }
			});
		}
		var reviewForm = null;
		function showPostReview() {
			if(isMobile()) {
				nextPageOpenContent(".showPostReview");
			} else {
				var form = reviewForm.clone();
		    	$.fancybox({
		    		id: "PostReview_fancyView",
		    		content: form,
		    	});
				initRatings(form);
			}
		}

		$(window).load(function(){
			//loadReview();
			if(window.location.hash.trim() == "#openReviewForm") { showPostReview(); }
			reviewForm = $( ".reviewForm" ).clone();
			initRatings($( ".reviewForm" ));

			/* review cut function's */
			var countReviews = $('div.more .review_block_item').size();
			if (countReviews > 5) {
				$(".information#reviews .more").append("<div class='cut_reviews'></div>");
				$(".information#reviews .more").append("<span class='show_all_reviews'>Показать все отзывы</span>");
				$('div.cut_reviews').slideUp();
			}
			$('div.more .review_block_item').each(function(index, value) {
				if (index >= 5) {
					$('.cut_reviews').append($(this));
				}
			});
			/* review cut function's end */
		});

		$('.information#reviews .more').on('click', 'span.show_all_reviews', function() {
			if ($('div.cut_reviews').hasClass('full_review_opened')) {
				$('div.cut_reviews').slideUp('slow');
				$('span.show_all_reviews').html('Показать все отзывы');
				$('div.cut_reviews').removeClass('full_review_opened');
			}
			else {
				$('div.cut_reviews').slideDown('slow');
				$('span.show_all_reviews').html('Скрыть отзывы');
				$('div.cut_reviews').addClass('full_review_opened');
			}
		});
		
	</script>
<?php endif; ?>
