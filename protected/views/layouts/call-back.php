<div id="callback" class="md-modal md-effect-1">

    <div class="md-close">X<span>закрыть форму</span></div>
    <div class="md-content">
        <div class="md-title color1">ОБРАТНЫЙ ЗВОНОК</div>
        <div class="md-form-container">
            Мы перезвоним на ваш номер: <input type="text" class="phone-field" /> в ближайшие 15 минут <a href="#" class="green-button order-call-back">Заказать обратный звонок</a>
        </div>
        <div class="md-form-container-resp">
            Мы перезвоним на ваш номер в ближайшие 15 минут
        </div>
    </div>
</div>
<div class="bg-block"></div>
<script>

    var popUp = {};

    popUp.callBackShow = function(sel){
        $('.bg-block').show();
        $(sel).fadeIn();
    };

    popUp.callBackHide = function(sel){
        $('.bg-block').hide();
        $(sel).fadeOut();
    };

    $('#callback .md-close, .bg-block').click(function(){
        popUp.callBackHide('#callback');
    });

    $('.order-call-back').click(function(){
        var phone = $('.phone-field');
        if(phone.val() != ''){
            $.ajax({
                type: 'POST',
                url: 'ajax/callback',
                data: 'phone=' + phone.val(),
                success: function(data){

                    $('.md-form-container').hide();
                    $('.md-form-container-resp').show();
                    setTimeout(function(){
                        $('#callback').fadeOut();
                        $('.bg-block').fadeOut();
                        setTimeout(function(){
                            $('.md-form-container').show();
                            $('.md-form-container-resp').hide();
                        }, 1000);
                        phone.val('');
                    }, 3000);
                }
            });

        }
    });

    $(document).ready(function() {
        $(".phone-field").mask("+7(999)999-99-99");
    });
</script>

<?php
    Yii::app()->clientScript->registerScriptFile("/shared/js/jquery.maskedinput.min.js", CClientScript::POS_END);
?>