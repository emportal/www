<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<?php $this->renderPartial('//layouts/_header'); ?>

	<div class="SECTION crumbs_section">
		<div class="main_column">
			<?php $this->widget('Breadcrumbs', array(
				'links' => $this->breadcrumbs,
				'htmlOptions' => [
					'class' => 'crumbs'
				]
			)); ?>
		</div>
	</div>
<div class="SECTION">
	<section class="main_column">
		<div class="container">
			<table class="content">
				<tr>
					<td class="left-block">
						<?php
						$this->widget('zii.widgets.CMenu', array(
							'items'			 => $this->leftMenu,
							'encodeLabel'	 => false,
							'htmlOptions'	 => array('style' => 'margin: 0; padding: 0;'),
						));
						?>
					</td>
					<td class="right-block" width="70%">
						<?php echo $content; ?>
					</td>
				</tr>
			</table>			
			<div class="clearfix">
			</div><!-- /clearfix -->
		</div><!-- /container-->
	</section>
</div>
<?php $this->renderPartial('//layouts/_seo'); ?>
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>