<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title><?= CHtml::encode($this->pageTitle) ?></title>
	<link rel="icon" type="image/png" href="favicon.png" />
</head>
<body>
	<div class="<?=ucfirst(Yii::app()->controller->id)."Controller"?> <?="action".ucfirst(Yii::app()->controller->action->id)?> mainVkApp">

		<style>
			.SEARCH_BOX_HEAD {
				height: 0;
			}
			input#s2id_autogen4.select2-input.select2-default {
				color: rgb(51,51,51) !important;  
			}
			@media (max-width: 1000px) {}
				.mobilecrumbs {
					background-image: inherit;
				    background-color: #ecf5f9;
				}
			}
		</style>
		<?php 			
			if(Yii::app()->controller->action->id == 'doctor') {
					Yii::app()->controller->breadcrumbs = ['выберите врача'];
			}
			elseif(Yii::app()->controller->action->id == 'diagnostics') {
					Yii::app()->controller->breadcrumbs = ['выберите услугу'];
			}

				Yii::app()->controller->breadcrumbs = [array_pop(Yii::app()->controller->breadcrumbs)];
				$this->widget('Breadcrumbs', array(
						'homeLink'=> '',
						'links' => Yii::app()->controller->breadcrumbs,
						'htmlOptions' => [ 'class' => 'mobilecrumbs', 'style' => 'display:none;' ],
					)
				); 
		?>
		<?php if($this->searchbox_type): ?>
			<div class="SEARCH_BOX_HEAD1">
				<?php
					 $widgetParams = [
						'type' => $this->searchbox_type, 
						'tab' => $this->searchbox_tab, 
						'tabNumber' => isset($_GET['diagnostics']) ? SearchBox::TAB_SERVICE : ((Yii::app()->params['samozapis']) ? SearchBox::TAB_OMSCLINIC : SearchBox::TAB_DOCTOR),
						'searchModel' => $this->searchModel,
						'controller' => $this,
					];
					$this->widget('SearchBox', $widgetParams);
		 		?>
	 		</div>
	 	<? endif; ?>
		<style type="text/css">
						.SEARCH_BOX_HEAD1 {
							display: none;
						}
						.SEARCH_RESULT {
							min-height: 650px !important;
						}
						@media (max-width: 1000px) {
							.SEARCH_RESULT {
								min-height: 660px !important;
							}
						}						
		</style>


		<div class="contents" style="min-height: 642px;">
			<section class="content main_content">
				<?php echo $content; ?>
			</section>
			<section class="content next_content" style="display: none;"></section>
		</div>
	</div>

	<script src="https://vk.com/js/api/xd_connection.js?2"  type="text/javascript"></script>
	<script type="text/javascript"> 
	  VK.init(function() {}, function() {}, '5.60'); 
	</script>

	<?php
	if(!MyTools::desktop_version()) {
		$urlToMobileCss = Yii::app()->assetManager->publish( (Yii::getPathOfAlias('shared').DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'mobile.css') );
		Yii::app()->clientScript->registerCssFile($urlToMobileCss."?v=33");
		Yii::app()->clientScript->registerMetaTag("width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no", "viewport");
	}
	Yii::app()->clientScript->registerScriptFile('/shared/js/_drop_table.js', CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile('/shared/js/jquery.maskedinput.min.js', CClientScript::POS_END);

	Yii::app()->clientScript->registerScript('resizeWin', '
		$(document).ready(function() {
			setInterval(function() {
				if($(".contents").height() > 700) {
					VK.callMethod("resizeWindow", 800, $(".mainVkApp").height());
				}
				else {
					VK.callMethod("resizeWindow", 800, 700);
				}
			},500);
		});
	');
	?>
</body>
</html>
