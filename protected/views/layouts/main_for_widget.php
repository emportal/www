<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name='yandex-verification' content='41a62a5805897618' />
	<meta name="google-site-verification" content="RyUXjF-df4mkMJ2R0Y5lutt6OEDpEPzqfZDEpAVXAoY" />
	<meta name="apple-itunes-app" content="app-id=887827260">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<link rel="canonical" href="<?=Yii::app()->createAbsoluteUrl(Yii::app()->urlManager->parseUrl(Yii::app()->request),$_GET,'http');?>"/>
</head>
<body style="background: none;">
	<noindex>
	<?php echo $content; ?>	
	<div style="display: none; opacity: 0;" class="b-top" id="scroller">
		<span class="b-top-but" style="display: none; opacity: 0;">наверх</span>
	</div>
	</noindex>
</body>
</html>