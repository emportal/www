<!-- Модальное окно: exitbox -->
<div id="emailbox" class="md-modal md-effect-1">
	<div class="md-close">X</div>
    <div class="md-content">
    	<div class="md-title color1">Узнавайте первыми о скидках на лечение и последних достижениях медицины! Подпишитесь на нашу рассылку! </div>
        <div class="md-form-container">
			<?php 
			$emailBlock = new EmailBlock;
			$emailBlock->options = [
				'blockId' => 'empSubscribeEmailBlock',
				'emailAttributes' => [
					'data-type' => '1',
					'data-companyUrl' => $model->company->linkUrl,
					'data-doctorUrl' => $model->linkUrl
				]
			];
			$emailBlock->run();
			?>
        </div>
        <!-- <p>Спасибо за помощь! Будьте здоровы и счастливы!</p> -->
    </div>
    <div class="md-after-content">
    	<div class="md-title">Спасибо за помощь! Будьте здоровы и счастливы!</div>
    </div>
</div>
<div id="exitbox" class="md-modal md-effect-1">
	<div class="md-close">X</div>
    <div class="md-content">
    	<div class="md-title color1">Хотите получить скидку на лечение?</div>
        <div class="md-form-container">
			<img src="/images/promo_compliment.jpg">
			<p style="font-size:18px; text-align:center;">
			<a href="/register?promoCode=COMPLIMENT">Зарегистрируйтесь</a>
			на нашем сайте прямо сейчас,
			<br>
			<a href="/register?promoCode=COMPLIMENT">укажите</a>
			промо-код COMPLIMENT при регистрации
			<br>
			и получите 500 рублей на Ваш первый визит к врачу!
			</p>
        </div>
        <!-- <p>Спасибо за помощь! Будьте здоровы и счастливы!</p> -->
    </div>
    <div class="md-after-content">
    	<div class="md-title">Спасибо за помощь! Будьте здоровы и счастливы!</div>
    </div>
</div>
<div class="md-overlay"></div>

<script>
function ExitBox(exitBoxId) {
	this.id = exitBoxId;
	this.tabsCounter(+1);
	window.onunload = function(self) { 
		return function (e) {
			self.tabsCounter(-1);
		};
	}(this);

	if(this.tabsCounter() > 1) {
		this.timeout = 1;
	}

	var handler =  function(self) {
		return function() {
			window.onmouseout = function(e) {
				e = e ? e : window.event;
			    var from = e.relatedTarget || e.toElement;
			    //var target = e.target || e.srcElement;
			    if (!from || from.nodeName == "HTML") {
			        // 
			    	self.show();
			    }
			};
		};
	}(this);
	setTimeout( handler , this.timeout);
}

ExitBox.prototype.tabsCounter = function (val) {
	var tabscounter = parseInt(this.Cookie.get("tabscounter"));
	if(isNaN(tabscounter)) tabscounter = 0;
	if(val) {
		val = parseInt(val);
		tabscounter += val;
		if(tabscounter < 0) tabscounter = 0;
		this.Cookie.remove("tabscounter");
		this.Cookie.set("tabscounter", tabscounter, {domain: '.emportal.ru',});
	}
	return tabscounter;
};

ExitBox.prototype.stats = function (val) {
	val = parseInt(val);
	if(!isNaN(val)) {
		this.Cookie.remove("exitboxstats");
		this.Cookie.set("exitboxstats", val, {
		    expires: 2592000,
		    domain: '.emportal.ru',
		});
	}
	var exitboxstats = this.Cookie.get("exitboxstats");
	return exitboxstats;
};

ExitBox.prototype.show = function (always) {
	always = typeof always !== 'undefined' ? always : false;
	if(!always) {
		var cookieEnabled = (navigator.cookieEnabled) ? true : false   
		if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) { 
			document.cookie = "testcookie";
			cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
		}
		if (!cookieEnabled || ( this.stats() == 1 || this.tabsCounter() > 1 )) return false;
	}
	this.stats(1);

	$("#"+this.id+" #md_textarea").val("");
	$("#"+this.id+" #md_user_email").val("");
	
	$( "#"+this.id ).removeClass( "md-show" );
	$( "#"+this.id ).addClass( "md-show" );

	$("#"+this.id+" .md-close").unbind( "click" );
	$("#"+this.id+" .md-close").bind( "click", function(self) { return function(event){
		self.close(self.id);
	}; }(this));

	$("#"+this.id+" #md_submit").unbind( "click" );
	$("#"+this.id+" #md_submit").bind( "click", function(self) { return function(event){
		self.submit(self.id);
	}; }(this));

	$("#"+this.id+" .md-content").show();
	$("#"+this.id+" .md-after-content").hide();
};

ExitBox.prototype.close = function () {
	$("#"+this.id).removeClass( "md-show" );
};

ExitBox.prototype.submit = function () {
	if($("#"+this.id+" #md_textarea").val() === "") return;
	$("#"+this.id+" .md-content").hide();
	$("#"+this.id+" .md-after-content").show();
	var handler = function(self){ return function() {
			self.close(self.id);
		}; }(this);
	setTimeout( handler , 2000);
	var md_subject = "Exit-Box";
	var md_textarea = $("#"+this.id+" #md_textarea").val();
	var md_email = $("#"+this.id+" #md_user_email").val();
	if(md_email == "") md_email = "Анонимный отправитель";
	var set = {
		url: "/index.php?r=site/exitBoxSubmit",
		data: { exitMessage : { subject: md_subject, email: md_email, body: md_textarea, page_url: document.URL } },
		method:'POST',
		dataType:'json',
		success: function(data, textStatus, XHR) {
			if(data.status == "OK") {
				
			}
		},
		error: function(XHR, textStatus, errorThrown) {
			
		}
	};
	$.ajax(set);
};

ExitBox.prototype.Cookie = { //возвращает cookie с именем name, если есть, если нет, то undefined
		'get' : function (name) {
				var matches = document.cookie.match(new RegExp(
					"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
				));
				return matches ? decodeURIComponent(matches[1]) : undefined;
			},
		'set' : function (name, value, options) {
				options = options || {};

				var expires = options.expires;

				if (typeof expires == "number" && expires) {
					var d = new Date();
					d.setTime(d.getTime() + expires*1000);
					expires = options.expires = d;
				}
				if (expires && expires.toUTCString) { 
					options.expires = expires.toUTCString();
				}

				value = encodeURIComponent(value);

				var updatedCookie = name + "=" + value;

				for(var propName in options) {
					updatedCookie += "; " + propName;
					var propValue = options[propName];    
					if (propValue !== true) { 
						updatedCookie += "=" + propValue;
					}
				}

				document.cookie = updatedCookie;
			}, /* Аргументы:
			name - название cookie
			value - значение cookie (строка)
			options - Объект с дополнительными свойствами для установки cookie:
			expires - Время истечения cookie. Интерпретируется по-разному, в зависимости от типа:
					Число — количество секунд до истечения. Например, expires: 3600 — кука на час.
					Объект типа Date — дата истечения.
					Если expires в прошлом, то cookie будет удалено.
					Если expires отсутствует или 0, то cookie будет установлено как сессионное и исчезнет при закрытии браузера.
			path - Путь для cookie.
			domain - Домен для cookie.
			secure - Если true, то пересылать cookie только по защищенному соединению. */
		'remove' : function (name) {
				this.set(name, "", { expires: -1 });
			},
	};

ExitBox.prototype.timeout = 180000;

<?php if(Yii::app()->user->isGuest && !Yii::app()->params['isBlogDomen']): ?> 
// Если устройство не сенсорное то запускаем
try {  
	document.createEvent("TouchEvent");
} catch (e) {
	if(window.location.hash.trim() == "#openEmailbox") {
		var exitbox = new ExitBox('emailbox');
		exitbox.show(true);
	} else {
		var exitbox = new ExitBox('exitbox');
	}
} 
<?php endif; ?>
</script>