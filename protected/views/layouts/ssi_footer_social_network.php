<div class="footer_social_network">
			<div class="footer_block_header">Мы в соцсетях</div>
			<div class="footer_social_icons">
				<a href="http://vk.com/emportal" rel="nofollow" target="blank">
					<img src="/images/newLayouts/icon_social_vk.png" alt="ВКонтакте" class="social-icon">
				</a>
				<a href="https://plus.google.com/u/0/communities/115414954349017363451" rel="nofollow" target="blank">
					<img src="/images/newLayouts/icon_social_google.png" alt="Google+" class="social-icon">
				</a>
				<a href="https://twitter.com/emportalru" rel="nofollow" target="blank">
					<img src="/images/newLayouts/icon_social_twit.png" alt="twiter" class="social-icon">
				</a>
				<a href="https://www.facebook.com/emportalru?ref=hl" rel="nofollow" target="blank">
					<img src="/images/newLayouts/icon_social_facebook.png" alt="facebook" class="social-icon">
				</a>
				<a href="http://www.odnoklassniki.ru/group/54687358844954" rel="nofollow" target="blank">
					<img src="/images/newLayouts/icon_social_ok.png" alt="OK" class="social-icon">
				</a>
			</div>
			<a href="javascript: void(0)" onclick="javascript: window.location.href = 'tel:'+$(this).find('span').first().text().split(' ').join('').split('(').join('').split(')').join('').split('-').join('')" class="footer_item">
				<span class="ya-phone"><?= $phones['empRegistryPhone'] ?></span>
			</a>
			<?php if(MyTools::desktop_version()): ?>
			<a class="link-reset menu-item none-in-responsive" rel="nofollow" href="javascript:void(0)" onclick="$.cookie('desktop_version',null,{expires:1,path:'/',domain:'.emportal.ru'});window.location.reload();">
	            Перейти на мобильную версию сайта
	        </a>
	        <?php else: ?>
			<a class="link-reset menu-item show-in-responsive" style="display: none;" rel="nofollow" href="javascript:void(0)" onclick="$.cookie('desktop_version',1,{expires:1,path:'/',domain:'.emportal.ru'});window.location.reload();">
	            Перейти на полную версию сайта
	        </a>
	        <?php endif; ?>
</div>