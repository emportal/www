<?php $this->beginContent('//layouts/main'); ?>
<div class="wrapper">
	<?php $this->renderPartial('//layouts/_header_producer'); ?>
	<section class="middle">
		<div class="container">
			<div class="clearfix">
				<?php echo $content; ?>
			</div><!-- /clearfix -->
			<?php $this->widget('NavigationBox'); ?>
		</div><!-- /container-->
	</section><!-- /middle-->
</div><!-- /wrapper -->
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>