<?php $this->beginContent('//layouts/main'); ?>
<?php $this->renderPartial('//layouts/_header'); ?>
<div class="SECTION">
	<section class="main_column">
		<div class="container">
			<div class="clearfix">
				<?php echo $content; ?>
			</div>
		</div>
	</section>
</div>
<?php $this->renderPartial('//layouts/_seo'); ?>
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>