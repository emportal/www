<!-- Модальное окно: ContactManager -->
<div id="mdContactManager" class="md-modal md-effect-1">
	<div class="md-close">X</div>
    <div class="md-content">
    	<div class="md-title"><h3>Форма для связи с менеджером emportal.ru</h3></div>
    	<div class="md-header">
    		<img class="crm_manager_img" src="/images/test.jpg" style="float: left;">
    		<p style="font-size: 12px;">Ваш менеджер: <?=Yii::app()->user->getModel()->company->managerRelations->user->name?>
    			<br> <?=Yii::app()->user->getModel()->company->managerRelations->user->telefon?>
    			<br><a href="mailto:<?=Yii::app()->user->getModel()->company->managerRelations->user->email?>"><?=Yii::app()->user->getModel()->company->managerRelations->user->email?></a>
    			<br>
    		</p>
    	</div>
        <div class="md-form-container">
			<input id="md_sender" type="text" hidden="hidden" value="<?= Yii::app()->user->model->id ?>">
			<div style="text-align: left">
				<label>Тема сообщения: </label>
				<select id="md_subject">
					<optgroup label="Тема сообщения">
						<option value="Задать вопрос">Задать вопрос</option>
						<option value="Запрос смены тарифа">Запрос смены тарифа</option>
						<option value="Оповестить о смене юридических реквизитов">Оповестить о смене юридических реквизитов</option>
					</optgroup>
				</select>
			</div>
        	<textarea id="md_msgbody" class="userText" rows="15" placeholder="Текст сообщения"></textarea>
        </div>
		<div class="md_submit"><input id="md_submit" type="submit" value="Отправить" class="md_submit_btn"></div>
    </div>
    <div class="md-after-content" id="md_inProcess">
    	<div class="md-title">Отправляется</div>
    </div>
    <div class="md-after-content" id="md_success">
    	<div class="md-title">Ваше сообщение отправлено</div>
    </div>
    <div class="md-after-content" id="md_error">
    	<div class="md-title">Ошибка</div>
    </div>
</div>
<div class="md-overlay"></div>
<script>
function ModalWindow(WindowId) {
	this.id = WindowId;
}

ModalWindow.prototype.show = function (args) {
	$("#"+this.id+" #md_msgbody").val("");
	if(args) {
		if(typeof args["msgbody"] != 'undefined') {
			$("#"+this.id+" #md_msgbody").val(args["msgbody"]);
		}
		if(typeof args["subject"] != 'undefined') {
			$("#"+this.id+" #md_subject")[0].options[args["subject"]].selected=true;
		}
		if(typeof args["mdheader"] != 'undefined') {
			$("#"+this.id+" .md-header").html(args["mdheader"]);
		}
	}
	
	$( "#"+this.id ).removeClass( "md-show" );
	$( "#"+this.id ).addClass( "md-show" );

	$("#"+this.id+" .md-close").unbind( "click" );
	$("#"+this.id+" .md-close").bind( "click", function(self) { return function(event){
		self.close(self.id);
	}; }(this));

	$("#"+this.id+" #md_submit").unbind( "click" );
	$("#"+this.id+" #md_submit").bind( "click", function(self) { return function(event){
		self.submit(self.id);
	}; }(this));

	$("#"+this.id+" .md-content").show();
	$("#"+this.id+" .md-after-content").hide();
};

ModalWindow.prototype.close = function () {
	$("#"+this.id).removeClass( "md-show" );
};

ModalWindow.prototype.submit = function () {
	if($("#"+this.id+" #md_msgbody").val() === "") return;
	var mdcontent = $("#"+this.id+" .md-content");
	var mdinProcess = $("#"+this.id+" #md_inProcess");
	var mdsuccess = $("#"+this.id+" #md_success");
	var mderror = $("#"+this.id+" #md_error");
	
	var OKhandler = function(self){ return function() {
			mdinProcess.hide();
			mdsuccess.show();
			mderror.hide();
			setTimeout( function() {
				mdsuccess.hide();
				self.close();
			} , 2000);
		}; }(this);
	var ERRORhandler = function(self){ return function() {
			mdinProcess.hide();
			mdsuccess.hide();
			mderror.show();
			setTimeout( function() {
				mderror.hide();
				mdcontent.show();
			} , 2000);
		}; }(this);
		
	var md_subject = $("#"+this.id+" #md_subject").val();
	var md_sender = $("#"+this.id+" #md_sender").val();
	var md_msgbody = $("#"+this.id+" #md_msgbody").val();

	mdcontent.hide();
	mdinProcess.show();
	
	var set = {
		url: "/adminClinic/ajax/contactManagerSubmit",
		data: { message : { subject: md_subject, sender: md_sender, msgbody: md_msgbody } },
		method:'POST',
		dataType:'json',
		success: function(data, textStatus, XHR) {
			if(data.status == "OK") {
				OKhandler();
			} else {
				this.error(XHR, textStatus, null)
			}
		},
		error: function(XHR, textStatus, errorThrown) {
			ERRORhandler();
		}
	};
	$.ajax(set);
};
var mdContactManager = new ModalWindow("mdContactManager");
/* 
 * Инструкция:
 * Показать модальное окно связи с менеджеро. 
 * 		mdContactManager.show({msgbody:null, subject:"0"})
 * Метод может принимать параметры.
 *		msgbody - Текст сообщения
 *		subject - Номер темы письма 
 */
</script>