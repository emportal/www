<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main_for_widget'); ?>
<div class="wrapper" style="width: 700px; overflow: hidden;">
	<section class="middle">
		<div class="container card">
			<?php $this->widget('Breadcrumbs', array(
				'links' => $this->breadcrumbs,
				'htmlOptions' => [
					'class' => 'crumbs'
				]
			)); ?>
			<a href="https://emportal.ru" target="_blank" onclick="ga('send', 'event', 'go', 'website_partner');  return true;">
				<img src="<?=$this->assetsImageUrl . '/logo.png'?>" style="margin: 10px 20px 10px 0; max-height: 40px; float: left;">
			</a>
			<div class="content" style="width: 700px;">
				<?php echo $content; ?>
				<div class="clearfix"></div><!-- /clearfix -->
			</div>		
			<div class="clearfix"></div><!-- /clearfix -->
		</div><!-- /container-->
	</section><!-- /middle-->
</div><!-- /wrapper -->
<!-- 2GIS GA Counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50714926-1', '2gis.com');
  ga('send', 'pageview');

</script>
<?php $this->endContent(); ?>