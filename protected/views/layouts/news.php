<?php $this->beginContent('//layouts/main'); ?>
	<?php $this->widget('CurrentBannersBlock',array('type'=>'top')); ?>
	
	<?php $this->renderPartial('//layouts/_header'); ?>
	
	<div class="SECTION crumbs_section">
		<div class="main_column">
			<?php
			$this->widget('Breadcrumbs', array(
				'homeLink'=>CHtml::link('emportal.ru', "http://emportal.ru"),
				'links' => $this->breadcrumbs,
				'htmlOptions' => [
					'class' => 'crumbs'
				]
			)); ?>
		</div>
	</div>
<div class="SECTION">
	<section class="main_column">
		<div class="content">
			<?php echo $content; ?>
		</div>
	</section>
</div>
<?php $this->renderPartial('//layouts/_seo'); ?>
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>