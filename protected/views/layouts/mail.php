<style>	
	div, p, span, strong, b, em, i, a, li, td {
            -webkit-text-size-adjust: none;
    }
	
	p {
		margin:0px 0px 0px 0px!important;
	}
	
	
	.mail_content font {
		font-size:14px;padding-bottom:8px;padding-top:10px;display:block;
	}
	
	.head {
		color: #545454;font-size: 21px;line-height: 21px;
	}
</style>

<table width="98%" cellpadding="0" align="center" cellspacing="0" border="0" style="border-collapse: collapse;">
	<tr>
		<td>
			<table width="700" cellpadding="0" align="center" cellspacing="0" border="0" style="border-collapse: collapse;">
				<tr>
					<td width="280" style="padding-left:25px;">
						<div style="line-height:0;">
							<a href="https://emportal.ru">
								<img  src="https://www.emportal.ru/images/mail_logo.png?v=2" alt="Единый Медицинский Портал"/>
							</a>
						</div>						
					</td>
					<td>
						<font style="color: #00b3cf; margin:0px;font-size:15px">Телефон:</font><br/>
						<font style="color: #004557; margin:0px;font-size:15px">8 (812) 313-21-29</font><br/>
						<font style="color: #004557; margin:0px;font-size:15px">8 (800) 333-46-45</font>
					</td>
					<td>
						<div style="line-height:0;">
							<img src="https://www.emportal.ru/images/mail_doctor.png"/>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
							<tr>
								<td colspan="3">
									<div style="line-height:0;">
										<img src="https://www.emportal.ru/images/mail_top_border.png"/>
									</div>
								</td>	
							</tr>
							<tr>
								<td width="1" style="background-color:#e8e8e8"></td>
								<td class="mail_content" style="padding-left:25px;padding-right:25px;">										
									<?php echo $content; ?>
								</td>
								<!--<td valign="top" width="210" style="padding-right:25px;padding-left:25px;width:210px">
															
								</td>-->
								<td width="1" style="background-color:#e8e8e8"></td>
							</tr>
							<tr>
								<td colspan="3">
									<div style="line-height:0;">
										<img src="https://www.emportal.ru/images/mail_bottom_border.png"/>
									</div>
								</td>								
							</tr>
						</table>
					</td>
				</tr>					
				<tr>
					<td colspan="3" style="padding-left:5px;padding-top:10px">
						<a href="http://vk.com/emportal"><img style="padding-right:5px;" src="https://emportal.ru/shared/images/vk.png"/></a>
						<a href="http://instagram.com/emportal"><img src="https://emportal.ru/shared/images/instagram.png"/></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>

