<?php
$isBlogDomen = Yii::app()->params['isBlogDomen'];
$absoluteLink = $isBlogDomen ? "http://emportal.ru" : "";
$phones = UserRegion::getRegionPhones();
?>
<div class="SECTION FOOTER">
	<div class="main_column">
		<!--# block name="footer_social_network" -->
		<?= $this->renderPartial("//layouts/ssi_footer_social_network", ['phones'=>$phones]); ?>
		<!--# endblock -->
		<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_footer_social_network&params[phones][empRegistryPhoneForLinkHref]=<?=urlencode($phones['empRegistryPhoneForLinkHref'])?>&params[phones][empRegistryPhone]=<?=urlencode($phones['empRegistryPhone'])?>" stub="footer_social_network" -->
		<div class="footer_content">
			<div class="footer_block">
				<div class="footer_block_header">Пациентам</div>
				<a href="<?=$absoluteLink?>/doctor" class="footer_item">Все врачи</a>
				<a href="<?=$absoluteLink?>/klinika" class="footer_item">Все клиники</a>
                <a href="<?=$absoluteLink?>/diagnostics" class="footer_item">Все услуги</a>
				<a target="_blank" href="https://emportal.ru/handbook/disease" class="footer_item">Все болезни</a>
			</div>
			<div class="footer_block">
				<div class="footer_block_header">О нас</div>
				<a href="<?=$absoluteLink?>/team.html" class="footer_item">Команда</a>
				<a href="<?=$absoluteLink?>/pressa.html" class="footer_item">Пресса</a>
				<a href="<?=$absoluteLink?>/about_us.html" class="footer_item">О проекте</a>
                <a href="<?=$absoluteLink?>/vozmozhnosti_dlya_klinik/api" class="footer_item">API</a>
			</div>
			<div class="footer_block">
				<div class="footer_block_header">Важная информация</div>
				<a href="<?=$absoluteLink?>/vozmozhnosti_dlya_klinik/forma_zapisi_online" class="footer_item">Возможности для клиник</a>
				<a href="<?=$absoluteLink?>/site/contact" class="footer_item">Стать партнёром</a>
				<a href="<?=$absoluteLink?>/privacy_policy.html" class="footer_item">Политика конфиденциальности</a>
				<a href="<?=$absoluteLink?>/terms_of_use.html" class="footer_item">Публичная оферта</a>
			</div>
			<div class="footer_copyright">
				© 2012-2016 все права защищены. Все права на любые материалы, опубликованные на сайте, защищены в соответствии с российским и международным
				законодательством об авторском праве. При использовании текстовых материалов в интернете и при использовании текстов в печатных и электронных
				СМИ ссылка на данный ресурс обязательна.
<?php
$controller = Yii::app()->getController();
$default_controller = Yii::app()->defaultController;
$isHome = (($controller->id === "site") && ($controller->action->id === "index")) ? true : false;
if($isHome && is_array(Yii::app()->params['regions'])):
?>
	<br>Записаться к врачам онлайн в 
	<?php
	$regions = [];
	foreach (Yii::app()->params['regions'] as $region) {
		$regions[] = @$region['subdomain'];
	}
	$criteria = new CDbCriteria;
	$criteria->addInCondition('subdomain',$regions);
	$cities = City::model()->findAll($criteria);
	foreach ($cities as $index=>$citiy):
	?><?= ($index>0 ? ', ' : '') ?><a href="<?="https://".($citiy->subdomain!='spb' ? ($citiy->subdomain.".") : "")."emportal.ru"?>"><?=$citiy->getLocative()?></a><?php endforeach; ?>
<?php endif; ?>
			</div>
		</div>
	</div>
</div><!-- /footer -->


<!-- exit-box -->
<?php
	//$this->renderPartial('//layouts/_exit-box',[ ]);
	$this->renderPartial('//layouts/call-back',[ ]);
	//$str = 'xdebug_print_function_stack';
	//echo '<br><br>' . $str . ': ' . $str();
?>

<?php if (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) : ?>
	<?php $this->widget('OMSpopup', array( 'id' => 'OMSpopupView' )); ?>
	<?php /* Yii::app()->clientScript->registerScript('showOMSpopupView', 'if(!$.cookie("OMSpopup_isShowing")) showOMSpopupView();', CClientScript::POS_READY); */ ?>
<?php endif; ?>

<?php if (!Yii::app()->params['samozapis'] && Yii::app()->controller->action->id == 'index') : ?>
	<div class="mango-callback" data-settings='{"type":"", "id": "MTAwMDM1Mjk=","autoDial" : "0", "lang" : "ru-ru", "host":"lk.mango-office.ru/", "errorMessage": "В данный момент наблюдаются технические проблемы и совершение звонка невозможно"}'> 
	</div> 
	<script>!function(t){function e(){i=document.querySelectorAll(".button-widget-open");for(var e=0;e<i.length;e++)"true"!=i[e].getAttribute("init")&&(options=JSON.parse(i[e].closest('.'+t).getAttribute("data-settings")),i[e].setAttribute("onclick","alert('"+options.errorMessage+"(0000)'); return false;"))}function o(t,e,o,n,i,r){var s=document.createElement(t);for(var a in e)s.setAttribute(a,e[a]);s.readyState?s.onreadystatechange=o:(s.onload=n,s.onerror=i),r(s)}function n(){for(var t=0;t<i.length;t++){var e=i[t];if("true"!=e.getAttribute("init")){options=JSON.parse(e.getAttribute("data-settings"));var o=new MangoWidget({host:window.location.protocol+'//'+options.host,id:options.id,elem:e,message:options.errorMessage});o.initWidget(),e.setAttribute("init","true"),i[t].setAttribute("onclick","")}}}host=window.location.protocol+"//lk.mango-office.ru/";var i=document.getElementsByClassName(t);o("link",{rel:"stylesheet",type:"text/css",href:host+"widget/widget-button.css"},function(){},function(){},e,function(t){document.documentElement.insertBefore(t,document.documentElement.firstChild)}),o("script",{type:"text/javascript",src:host+"js/widget/m.c.w-min.js"},function(){("complete"==this.readyState||"loaded"==this.readyState)&&n()},n,e,function(t){document.documentElement.appendChild(t)})}("mango-callback");</script>
	<link rel="stylesheet" type="text/css" href="/shared/css/callback_mango.css">
	<script type="text/javascript">
		
			setTimeout(function() {
				if(!localStorage.getItem("callbackFormShow")) {
					if($('.mng-overlay').css('display') == 'none') {
						$('.button-widget-open').click();
					}
				}
				localStorage.setItem("callbackFormShow",1);
			}, 30000);

	</script>
<?php endif; ?>
