<?php $this->beginContent('//layouts/main'); ?>
	<?php $this->widget('CurrentBannersBlock',array('type'=>'top')); ?>
		
	<?php /* $this->renderPartial('//layouts/_three_actions'); */ ?>
	<?php $this->renderPartial('//layouts/_header'); ?>
	<div class="contents">
		<section class="content main_content">
			<?= $content ?>
		</section>
		<section class="content next_content" style="display: none;"></section>
	</div>
	<?php $this->renderPartial('//layouts/_seo'); ?>
	<?php $this->renderPartial('//layouts/_footer'); ?>
	<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>