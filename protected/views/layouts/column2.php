<?php $this->beginContent('//layouts/main'); ?>
<?php $this->renderPartial('//layouts/_header'); ?>
	<div class="contents">
		<div class="SECTION crumbs_section">
			<div class="main_column">
				<?php $this->widget('Breadcrumbs', array('links' => $this->breadcrumbs, 'htmlOptions' => [ 'class' => 'crumbs' ] )); ?>
			</div>
		</div>
		<section class="content main_content">
			<div class="main_column">
				<?php echo $content; ?>
			</div>
		</section>
		<section class="content next_content" style="display: none;"></section>
	</div>
<?php $this->renderPartial('//layouts/_seo'); ?>
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>