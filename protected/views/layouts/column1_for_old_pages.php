<?php Yii::app()->clientScript->registerPackage('eldar_css'); ?>
<?php $this->beginContent('//layouts/main'); ?>
	<?php $this->widget('CurrentBannersBlock',array('type'=>'top')); ?>
		
	<?php $this->renderPartial('//layouts/_header'); ?>
	<style>
		h1, h2 {
			display: block;
			margin: 20px 0;
		}
	</style>
<div class="SECTION crumbs_section">
	<div class="main_column">
		<?php $this->widget('Breadcrumbs', array(
			'links' => $this->breadcrumbs,
			'htmlOptions' => [
				'class' => 'crumbs'
			]
		)); ?>
	</div>
</div>
<div class="SECTION">
	<div class="wrapper">
		<section class="content">
			<?= $content ?>
		</section>
	</div>
</div>
<?php $this->renderPartial('//layouts/_seo'); ?>
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>