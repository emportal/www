<?php /* @var $this Controller */ ?>
<header class="header">
	<div class="section-top">
		<div class="right-corner-top"></div>
		<div class="section-bottom min">
			<div class="right-corner-bot"></div>
			<div class="section">
				<div class="logo">
					<div class="logo-image">
						<?= CHtml::link(CHtml::image($this->assetsImageUrl.'/logo.png?v=2', "Единый медицинский портал"), '/')?>
					</div><!-- /logo-image -->
				</div><!-- /logo -->
				<?php
				//$this->widget('UserLogin', array('visible' => Yii::app()->user->isGuest));
				?>
				<?php
				//$this->widget('UserMenu', array('visible' => !Yii::app()->user->isGuest));
				?>
			</div><!-- /section -->
		</div><!-- /section-bottom -->
	</div><!-- /section-top -->
</header><!-- /header-->
