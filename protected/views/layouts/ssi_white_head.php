<div class="SECTION WHITE_HEAD">
	<div class="main_column">
		<noindex>
			<?php $this->widget('UserCabinetMenu', array('visible' => !Yii::app()->user->isGuest)); ?>
		<div class="for-user">
			<?php if(Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']): ?>
			<div style="float: left;margin: 7px 7px 0px 7px;">
				<a style="font-weight: bold;" href="javascript:void(0)" onclick="emias.showReferrals(); return false;" class="normal-link">
					<span class="referralsCount"><div class="icon_loading" style="min-width: 100px;height: 20px;"></div></span>
				</a>
				<a style="font-weight: bold;" href="javascript:void(0)" onclick="showOMSpopupView(); return false;" class="normal-link">
					Изменить ОМС данные.
				</a>
			</div>
			<?php endif; ?>
			<?php $this->widget('UserLogin', array('visible' => Yii::app()->user->isGuest)); ?>
		</div>
		</noindex>
	</div>
</div>

<style>
@media (max-width: 1000px) {
	.nav--sm {
	    position: fixed;
	    top: 0;
	    bottom: 0;
	    left: 0;
	    right: 0;
	    z-index: 2001;
	    visibility: hidden;
	}
	.nav-mask--sm {
	    position: absolute;
	    cursor: pointer;
	    height: 100%;
	    width: 100%;
	    background-color: #000;
	    opacity: 0;
	    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	    -webkit-transition: opacity 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	    -moz-transition: opacity 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	    -o-transition: opacity 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	    transition: opacity 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	}
	.nav-content--sm {
	    position: absolute;
	    left: -285px;
	    height: 100%;
	    border: 0;
	    background: white;
	    -webkit-transition: left 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	    -moz-transition: left 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	    -o-transition: left 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	    transition: left 0.2s cubic-bezier(0.4, 0, 0.2, 1);
	}
	.nav-menu-wrapper {
	    height: 100%;
	    overflow-x: hidden;
	    overflow-y: auto;
	    -webkit-overflow-scrolling: touch;
	}
	.va-container-h {
	    width: 100%;
	}
	.va-container-v {
	    height: 100%;
	}
	.va-container {
	    display: table;
	    position: relative;
	    width: 285px;
	}
	.nav-content--sm .nav-menu {
	    font-size: 20px;
	    border-color: #ffffff;
	    border-color: rgba(255,255,255,0.2);
	}
	.nav-content--sm .nav-menu .menu-group {
	    padding: 0;
	    margin-bottom: 34px;
	    line-height: 34px;
	    text-transform: uppercase;
	    font-size: 16px;
	    font-weight: bold;
	}
	.nav-content--sm .nav-menu .menu-group li {
	    list-style: none !important;
	    border-bottom: 1px solid rgb(220,220,220);
	    padding: 0 20px 0 15px;
	    background: url(/images/icons/arrow-right.png) no-repeat right transparent;
	}
	.nav-content--sm .nav-menu .menu-group li:hover {
	    background: #eceeee;
	}
	.nav-content--sm .nav-menu .menu-group li a {
	    display: block;
	}
	.nav-content-close {
		padding: 10px 20px 0px 15px;
		width: 20px;
	}
	.nav-opened.nav--sm {
		visibility: visible;
	}
	.nav-opened .nav-mask--sm {
		opacity: 0.5;
	}
	.nav-opened .nav-content--sm {
		left: 0;
	}
	.icon-reorder {
	    cursor: pointer;
	    width: 40px;
	    height: 40px;
	    padding: 20px 20px 20px 20px;
	    margin: 0 0 0 2%;
	    position: absolute;
	    background: transparent url(/images/icons/nav-menu.png) no-repeat left;
	    background-size: 50%;
	    z-index: 1000;
	}
}

@media (max-width: 480px) {
	.icon-reorder {
	    cursor: pointer;
	    width: 12px;
	    height: 12px;
	    padding: 10px 20px 20px 20px;
	    margin: 0 0 0 8px;
	    position: absolute;
	    background: transparent url(/images/icons/nav-menu.png) no-repeat left;
	    background-size: 50%;
	    z-index: 1000;
	}
}
</style>
<i class="icon icon-reorder icon-rausch show-in-responsive" style="display:none;"></i>
<nav class="nav--sm show-in-responsive" style="display:none;" role="navigation">
	<div class="nav-mask--sm"></div>
	<div class="nav-content--sm panel text-white">
	  <div class="nav-header items-logged-in"></div>
	  <div class="nav-menu-wrapper">
	    <div class="va-container va-container-v va-container-h">
	      <img src="/images/popup_close.png" class="nav-content-close">
	      <div class="va-middle nav-menu panel-body">
			  <?php if(!Yii::app()->user->isGuest): ?>
				  <div class="col-cabinet-info">
					  <div class="row-user-name"><?=Yii::app()->user->model->attributes['name'] ?></div>
					  <div class="row-button">
						  <a class="link-reset menu-item" rel="nofollow" href="/logout?returnUrl=<?= urlencode(Yii::app()->request->url) ?>">
							  Выход
						  </a>
					  </div>
					  <div style="clear: both"></div>
				  </div>
			  <?php endif; ?>
	        <ul class="menu-group list-unstyled row-space-3">
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="javascript: void(0)">
	              Город: 
	              <select id="citySelector" style="
					    position: absolute;
					    width: 100%;
					    left: 0;
					    height: 35px;
					    background: transparent;
					    border: 0;
					    padding: 0 0px 0px 75px;
					    font-weight: bold;
					    text-transform: uppercase;
					">
		          	<?php
		          	foreach (Yii::app()->params['regions'] as $region):
		          	?>
			          	<?php
		          		$selected = ($region['subdomain'] == City::getSelectedCity()->subdomain);
			          	$isSamozapis = Yii::app()->params['samozapis'];
						if ($isSamozapis)
						{
							if($region['samozapis'] && strval($region['samozapisSubdomain']) !== "") {
								?>
						        	<option value="<?= CHtml::encode($region['samozapisSubdomain']) ?>" <?= $selected ? "selected" : "" ?>>
						        		<?= CHtml::encode($region['name']) ?>
					        		</option>
								<?php
							}
						} else {
						?>
			          	<option value="<?= CHtml::encode($region['subdomain']) ?>" <?= $selected ? "selected" : "" ?>>
			          		<?= CHtml::encode($region['name']) ?>
		          		</option>
		          		<?php } ?>
		          	<?php endforeach; ?>
	              </select>
	            </a>
	          </li>
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="http://<?=(City::getSelectedCity()->subdomain !== 'spb' ? (City::getSelectedCity()->subdomain.".") : "" ).Yii::app()->params["host"]?>">
	              Главная
	            </a>
	          </li>
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="/doctor">
	              Врачи
	            </a>
	          </li>
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="http://<?=(City::getSelectedCity()->subdomain !== 'spb' ? (City::getSelectedCity()->subdomain.".") : "" ).Yii::app()->params["host"]?>/diagnostics">
	              Услуги
	            </a>
	          </li>
	          <?php if (Yii::app()->params['regions'][Yii::app()->session['selectedRegion']]['samozapis']): ?>
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="http://<?=urlencode(Yii::app()->params['regions'][Yii::app()->session['selectedRegion']]['samozapisSubdomain'])?>.<?=Yii::app()->params["host"]?>">
	              Запись по ОМС
	            </a>
	          </li>
	          <?php endif; ?>
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="http://blog.emportal.ru">
	              Блог
	            </a>
	          </li>
	          <?php if(Yii::app()->user->isGuest): ?>
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="/login">
	              Войти / Зарегистрироваться
	            </a>
	          </li>
	          <?php else: ?>
	          <li>
	          	<a class="link-reset menu-item" rel="nofollow"  href="/user">
					Личный кабинет
				</a>
				<ul>
					<li><a href="/user/profile">Профиль</a></li>
					<li><a href="/user/registry">Регистратура</a></li>
					<li><a href="/user/comments">Мои отзывы</a></li>
					<li><a href="/user/bonuses">Мои бонусы: <?=Yii::app()->user->model->attributes['balance']; ?> баллов</a></li>
				</ul>
	          </li>
	          <?php endif; ?>
	          <li>
	            <a class="link-reset menu-item" rel="nofollow" href="javascript:void(0)" onclick="$.cookie('desktop_version',1,{expires:1,path:'/',domain:'.emportal.ru'});window.location.reload();">
	              Полная версия
	            </a>
	          </li>
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>
</nav>
<script>
$(function(){
	$('.nav-mask--sm, .nav-content-close').bind('mouseup touchend', function(event){
		$('.nav--sm').removeClass('nav-opened');
		return false;
	}).bind('mousedown touchstart', function(event){ return false; });
	$('.icon-reorder').bind('mouseup touchend', function(event){
		$('.nav--sm').addClass('nav-opened');
		return false;
	}).bind('mousedown touchstart', function(event){ return false; });

	$('.clouse-tag-menu').bind('mouseup touchend', function(event){
		$('.tag-menu').removeClass('nav-opened');
		return false;
	}).bind('mousedown touchstart', function(event){ return false; });
	$('.ico-open-tag-menu').bind('mouseup touchend', function(event){
		$('.tag-menu').addClass('nav-opened');
		return false;
	}).bind('mousedown touchstart', function(event){ return false; });

});
</script>