<?php /* @var $this Controller */

//white stripe
$this->renderPartial('//layouts/_white_head');
$widgetParams = [
	'type' => $this->searchbox_type, 
	'tab' => $this->searchbox_tab, 
	'tabNumber' => isset($_GET['diagnostics']) ? SearchBox::TAB_SERVICE : ((Yii::app()->params['samozapis']) ? SearchBox::TAB_OMSCLINIC : SearchBox::TAB_DOCTOR),
	'searchModel' => $this->searchModel,
	'controller' => $this,
];
$this->widget('SearchBox', $widgetParams);
?>