
<div class="assests" style="display:none;">
	<div id="pop_up_spasibo">
		<div class="pop_up_spasibo_content" style="display:none;">
			<img src="/images/doctor.png" style="
			    position: absolute;
			    margin: -96px 0px 0 490px;
			">
			<div style="width: 100%;height: 100%;padding: 15px 15px;">
				<div style="display: block;">
					<div style="font-size: 18px;font-weight: bold;line-height: 1.2em;color: #005061;">
						Спасибо, <span class="appointment_user_firstName"></span>!
						<br>Вы успешно записаны на приём к врачу.
					</div>
					<div style="font-size: 14px; color: #005061;padding: 10px 0px 20px 0px;">
						Не забудьте распечатать Ваш талон. Он потребуется вам при посещении клиники.
					</div>
				</div>
				<div style="display: table-row;">
					<div style="display: table-cell;width: 200px;height: 256px;vertical-align: top;">
						<div id="appointment_preview" style=" ">
						</div>
					</div>
					<div style="display: table-cell;padding: 0px 0px 0px 20px;">
						<div style="font-size: 30px;font-weight: bold;line-height: 1em;color: #04b0cf;">
						А ещё мы начислим
						<br>
						<span style="font-size: 49px;line-height: 1.2em;">500 бонусов</span>
						<br>вам на счёт!</div>
						<div style="font-size: 16px;font-weight: bold;line-height: 1.3em;color: #005061;padding: 30px 0 30px 0;">Вы сможете оплачивать
							<br>бонусами услуги
							<br>частных клиник по курсу
							<br>1 бонус=1 рублю!
						</div>
					</div>
					<div style="display: table-cell;">
					</div>
				</div>
				<div style="display: table-row;">
					<div style="display: table-cell;text-align: center;vertical-align: middle;">
						<button class="btn-green" style="width: 100%;height: 55px;font-size: 18px;padding: 0;" onclick="PrintElem($('#appointment_result .appointment_print_form').get(0)); return false;">
							Распечатать талон
						</button>
					</div>
					<div style="display: table-cell;padding: 0px 0px 0px 20px;vertical-align: middle;">
						<div style="font-size: 16px;font-weight: bold;line-height: 1.3em;color: #005061;float: left;">Регистрируйтесь прямо
							<br>сейчас  получайте
							<br>бонусы на счет!
						</div>
						<a href="/register?promoCode=SAMOZAPIS" target="blank">
						<button class="btn-blue" style="text-transform: uppercase;height: 55px;font-size: 22px;padding: 19px;margin: 2px 0px 0px 10px;float: right;">
						Зарегистрироваться
						</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		var pop_up_spasibo_content = "";
		function show_pop_up_spasibo() {
			if(pop_up_spasibo_content == "") {
				pop_up_spasibo_content = $('.assests #pop_up_spasibo').html();
				$('.assests #pop_up_spasibo').remove();
			}
			$.fancybox({
				autoDimensions: false,
				width:750,
				height:430,
				content:pop_up_spasibo_content,
				onComplete: function() {
					$("#fancybox-content").css('overflow','visible');
					$("#fancybox-content").children().css('overflow','visible');
					$("#appointment_preview").html( $("#appointment_result .appointment_print_form").first().html() );
					$(".pop_up_spasibo_content").show();
				}
			});
		}
	</script>
</div>