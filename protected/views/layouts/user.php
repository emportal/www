<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<?php $this->renderPartial('//layouts/_header'); ?>
	<div class="SECTION crumbs_section">
		<div class="main_column">
			<?php $this->widget('Breadcrumbs', array(
				'links' => $this->breadcrumbs,
				'htmlOptions' => [
					'class' => 'crumbs inline-block'
				]
			)); ?>
			<?php if ($this->model->registerPromoCodeUsed) : ?>
			<!-- 
			<div class="float_right" style="background-color: #fafafa; padding: 6px; border-bottom: 1px dashed lightgray;">
				Баланс: <?= $this->model->balance ?> руб.
			</div>
			-->
			<?php endif; ?>
		</div>
	</div>
	<div class="SECTION">
		<div class="main_column">

				
					<?php /*<td class="left-block">
						<?php
						$this->widget('zii.widgets.CMenu', array(
							'items'			 => $this->leftMenu,
							'encodeLabel'	 => false,
							'htmlOptions'	 => array('style' => 'margin: 0; padding: 0;'),
						));
						?>
					</td> */ ?>

						<section class="content">
							<?php echo $content; ?>
						</section>

		</div>
	</div>
<?php //$this->renderPartial('//layouts/_seo'); ?>
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_counters'); ?>
<?php $this->endContent(); ?>