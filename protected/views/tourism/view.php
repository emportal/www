<?php
/* @var $model Address */
$phones = [];
foreach($model->phones as $ph) {
	$phones[]=$ph->name;
}
$this->pageTitle = "Туристическая фирма {$model->company->seoNameTourism}: запись онлайн, адрес, телефон – ".Yii::app()->name;
Yii::app()->clientScript->registerMetaTag("Туристическая фирма {$model->company->seoNameTourism} ({$model->name}," 
	.(!empty($model->nearestMetroStation->name) ? "м. ".$model->nearestMetroStation->name : '').","
	. " ".implode(", ",$phones).").".Yii::app()->name." запись в клиники онлайн", 'description');




$this->breadcrumbs = array(	
	'Тур. фирмы и тур. операторы' => '/search/tourism',
	$model->company->name,
);
?>


<div class="search-result-title">
	<h1 class="h2style">Туристическая фирма "<?= $model->company->seoNameTourism ?>"</h1>
	<small><?= $model->company->companyType->name ?></small>
</div><!-- /search-result-title -->
<table class="service-table">
	<tr>
		<td>
			<h5>
				<?= $model->name ?> (<?= Html::link('все адреса', array($this->createUrl('addresses'), 'link' => $model->link)) ?>)
				
				<!-- (<a href="#">смотреть на карте</a>) -->
				<?php if($model->nearestMetroStation->name):?>
					<br />
					<small>м. <?php echo $model->nearestMetroStation->name;?></small>
				<?php endif;?>
			</h5>
			
			<?php if (count($model->phones)): ?>
			<p class="w50">
				<span class="size-14">Тел.:</span>
				<?php foreach ($model->phones as $phone): ?>
						<!-- <br>(<?= $phone->cityCode ?>) <?= $phone->number ?>-->
					<?= $phone->name ?>; 
				<? endforeach; ?>
				<br />
			</p>
			<? endif; ?>
			<!-- 
			<p class="w50">
				<span class="size-14">Часы работы:</span><br>
				ПН-ПТ с 10 до 18<br>
				СБ-ВС выходной
			</p>
			 -->
			<?php if (count($model->refs)): ?>
			<p class="w50">
				
				<span class="size-14">Сайт:</span> 
					<?php foreach ($model->refs as $ref) : ?>
						<a href="<?= $ref->refValue ?>" target="_blank" rel="nofollow"><?= $ref->name ?></a>; 
					<? endforeach; ?>
					<br />
				<? endif; ?>
				<?php if (count($model->emails)): ?>
					<span class="size-14">Email:</span> 
					<?php foreach ($model->emails as $email) : ?>
						<a href="mailto:<?= $email->name ?>"><?= $email->name ?></a>; 
					<? endforeach; ?>
					<br />
			</p>
				<? endif; ?>

		</td>
	</tr>
	<tr>
		<td><strong>Год основания:</strong> <?= $model->company->dateOpening ?></td>
	</tr>

	<?php if($model->description):?>
	<tr>
		<td><?= $model->description ?></td>
	</tr>
	<?php endif;?>
	<!-- /search-result-info -->
</table>
