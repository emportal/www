<? /* @var $model Doctors */ ?>
<td class="left-block">
	<img src="/shared/images/pic-9.png" width="240" height="103" alt="1">
	<?php $link = $model->currentPlaceOfWork->address->link; ?>

	<?= Html::link("Записаться на прием", array('/clinic/visit', 'link' => $link), array('class' => 'btn-green')); ?><br><br>
	<?= Html::link("Описание", array('/clinic/view', 'link' => $link), array('class' => 'btn w100')); ?>
	<?= Html::link("Специалисты", array('/clinic/experts', 'link' => $link), array('class' => 'btn w100')); ?>
	<?= Html::link("Стоимость услуг", array('/clinic/price', 'link' => $link), array('class' => 'btn w100')); ?>
	<?= Html::link("Отзывы", array('/clinic/comments', 'link' => $link), array('class' => 'btn w100')); ?>
	<?= Html::link("Фотогалерея", array('/clinic/foto', 'link' => $link), array('class' => 'btn w100')); ?>
	<?= Html::link("Акции", array('/clinic/action', 'link' => $link), array('class' => 'btn w100')); ?>
</td><!-- /search-result-signup -->