
<div id="appointment_prev_step" class="appointment">
	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'form',
			'action' => CHtml::normalizeUrl([
				'/ajax/validateOMSData',
				'addressLinkUrl' => $addressLinkUrl,
				'companyLinkUrl' => $companyLinkUrl,
				'linkUrl' => $linkUrl
			]),
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'inputContainer' => 'td',
				'validateOnChange' => false,
				'beforeValidate' => 'js:function(form) {
					
					$( "#appointment_wait_while_loading" ).removeClass( "appointment_hidden" );
					$( "#appointment_prev_step" ).addClass( "appointment_hidden" );
					scrollToId("#appointment_block");
					return true;
				}',
				'afterValidate' => 'js: function(form,data,hasError) {
					
					
					$( "#appointment_wait_while_loading" ).addClass( "appointment_hidden" );
					$( "#appointment_prev_step" ).removeClass( "appointment_hidden" );
					scrollToId("#appointment_block");
				
					if(hasError === false) {
						/*countersManager.reachGoal({
							"forCounters" : ["yandex", "google"],
							"goalName" : "appointment_to_doctor",
						});*/
						console.log("нет ошибок");
						prepareAppointmentForm();
						return false;
					} else {
						console.log("есть ошибки");
						console.log(data);
					}
					return true;
				}'
			),
			'htmlOptions' => array('class' => 'zakaz relative', 'enctype' => 'multipart/form-data'),
		));
		/* @var $form CActiveForm */
	?>
	<h3 class="appointment_header">Запись по ОМС</h3>
	<table class="search-result-table" style="margin-top: 10px;">
		<tr class="search-result-box">
			<td class="search-result-info">
				<span class="appointment_item_name">Фамилия</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'surName', ['class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'surName') ?>
					</div>
				</div>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-info">
				<span class="appointment_item_name">Имя</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'firstName', ['class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'firstName') ?>
					</div>
				</div>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-info">
				<span class="appointment_item_name">Отчество</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'fatherName', ['class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'fatherName') ?>
					</div>
				</div>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-info">
				<span class="appointment_item_name">Дата рождения</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'birthday', ['id' => 'OMSForm_birthday', 'class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'birthday') ?>
					</div>
				</div>
			</td>
		</tr>
		<?php if(City::model()->getSelectedCityId() === City::MOSKVA): ?>
		<tr class="search-result-box">
			<td class="search-result-info">
				<span class="appointment_item_name">Серия полиса ОМС</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'omsSeries', ['id' => 'OMSForm_omsSeries', 'class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'omsSeries') ?>
					</div>
				</div>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-info">
				<span class="appointment_item_name">Номер полиса ОМС</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'omsNumber', ['id' => 'OMSForm_omsNumber', 'class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'omsNumber') ?>
					</div>
				</div>
			</td>
		</tr>
		<?php endif; ?>
		<tr class="search-result-box" style="display: none;">
			<td class="search-result-info">
				<span class="appointment_item_name">Id пациента</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'patientId', ['class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'patientId') ?>
					</div>
				</div>
			</td>
		</tr>
		<tr class="search-result-box" style="display: none;">
			<td class="search-result-info">
				<span class="appointment_item_name">extAddressId</span>
				<div class="relative ap_name">
					<?= $form->textField($omsForm, 'extAddressId', ['class' => 'custom-text']); ?>
					<div>
						<?= $form->error($omsForm, 'extAddressId') ?>
					</div>
				</div>
			</td>
		</tr>
		<tr class="search-result-box" id="OMSForm_exception" style="display: none;"></tr>
		<tr class="search-result-box">
			<td class="search-result-info">
				<div class="relative ap_name">
					<?php echo CHtml::submitButton('Далее', ['class' => 'btn-green btn_doctor_sign vmargin16']); ?>
				</div>
			</td>
		</tr>
	</table>
</div>

	<script>
		
		var patientOMSData = {};
		
		function prepareAppointmentForm() {
			$("#appointment_prev_step .errorMessage").hide();
			//подставляем полученные значения
			var fio = $("#appointment_prev_step #OMSForm_surName").val() + ' ' +  $("#appointment_prev_step #OMSForm_firstName").val() + ' ' + $("#appointment_prev_step #OMSForm_fatherName").val();
			$("#AppointmentToDoctors_name").val( fio );
			$("#phoneInput").val('');//+79213038563
			$(".ap_phone").hide();
			
			//прячем соотв. инпуты
			//$( "#AppointmentToDoctors_name" ).addClass( "appointment_hidden" );
			//$( "#phoneInput" ).addClass( "appointment_hidden" );
			//$( ".ap_phone" ).first().addClass( "appointment_hidden_field" );
			$( "#appointment_item_name_fio_title" ).parent().addClass( "appointment_hidden_field" );
			$( "#AppointmentToDoctors_name" ).parent().addClass( "not-active" );
			
			//получаем Id пациента по его фио и д.р.
			var patientData = {},
				idLpu = $("#appointment_prev_step #OMSForm_extAddressId").val(); //< ?=$omsForm-> extAddressId?>
			
			patientData['Surname'] = $("#appointment_prev_step #OMSForm_surName").val();
			patientData['Name'] = $("#appointment_prev_step #OMSForm_firstName").val();
			patientData['SecondName'] = $("#appointment_prev_step #OMSForm_fatherName").val();
			$("#appointment_prev_step #OMSForm_birthday").val($("#appointment_prev_step #OMSForm_birthday").val().split('-').join('.'));
			var splitbirthday = $("#appointment_prev_step #OMSForm_birthday").val().split(".");
			if(splitbirthday[0].length < 3) {
				splitbirthday.reverse();
			}
			patientData['Birthday'] = splitbirthday.join("-");
			if($("#appointment_prev_step #OMSForm_omsNumber").length > 0) {
				patientData['Polis_S'] = $("#appointment_prev_step #OMSForm_omsSeries").val();
				patientData['Polis_N'] = $("#appointment_prev_step #OMSForm_omsNumber").val();
			}
			for(key in patientData) patientOMSData[key] = patientData[key];
			patientOMSData['idLpu'] = idLpu;
			
			//http://dev1.emportal.ru/ajax/checkPatientOMS?idLpu=1&patientData%5BSurname%5D=%D0%9A%D0%BE%D1%80%D0%BE%D0%BB%D1%8C&patientData%5BName%5D=%D0%90%D0%BD%D0%B4%D1%80%D0%B5%D0%B9&patientData%5BSecondName%5D=%D0%9C%D0%B0%D0%BA%D1%81%D0%B8%D0%BC%D0%BE%D0%B2%D0%B8%D1%87&patientData%5BBirthday%5D=1972-11-29
			
			$.ajax({
				url: "/ajax/checkPatientOMS",
				async: false,
				data: ({
					idLpu: idLpu,
					patientData: patientData,
				}),
				//dataType: 'json',
				type: "GET",
				success: function(jsondata) {
					data = JSON.parse(jsondata);
					console.log(data);
					if (data.result) {
						var idPat = data.message;
						if(typeof(idPat) === "object") {
							idPat = JSON.stringify(idPat);
						}
						patientOMSData['idPat'] = idPat;
						$("#AppointmentToDoctors_extPatId").val(patientOMSData['idPat']);
						$("#patient_Surname").val(patientData['Surname']);
						$("#patient_Name").val(patientData['Name']);
						$("#patient_SecondName").val(patientData['SecondName']);
						$("#patient_Birthday").val(patientData['Birthday']);
						var $input = $('#datetimepicker');
						$input.val('<?=date('Y-m-d'); ?>');
						updateAvailableTime('', $input);
						//прячем омс блок записи
						$( "#appointment_prev_step" ).addClass( "appointment_hidden" );
						//показываем основной блок записи
						$( "#appointment_main" ).removeClass( "appointment_hidden" );
						scrollToId("#appointment_block");
						$(".appointment_user_firstName").text(patientData['Name']);
					} else {
						this.error(null,data["message"],null);
					}
				},
				error: function(XHR, textStatus, errorThrown) {
					$("#appointment_prev_step #OMSForm_exception").text(textStatus);
					$("#appointment_prev_step #OMSForm_exception").show();
				},
				complete: function() {
					//прячем индикатор обработки данных
					$( "#appointment_wait_while_loading" ).addClass( "appointment_hidden" );
				}
			});
		}		
		
		
		$(document).ready(function() {		
			
			//спрячем обычный блок записи
			$( "#appointment_wait_while_loading" ).addClass( "appointment_hidden" );
			$( "#appointment_main" ).addClass( "appointment_hidden" );
			
			$("#appointment_prev_step #OMSForm_birthday").datetimepicker({
	            changeMonth: true,
	            changeYear: true,
				lang: 'ru',
				timepicker: false,
				format: 'd.m.Y',
				closeOnDateSelect: true,
				minDate: "1900/01/01",
				maxDate: '0',
				defaultDate:'1989/12/31',
				firstDay: 1,
			});
			$("#appointment_prev_step #OMSForm_birthday").mask("99.99.9999", {placeholder: "_"});
			$("#appointment_prev_step #OMSForm_birthday").attr("placeholder", "31.12.1989");

			$('#appointment_prev_step input').each(function() {
				var input = $(this);
				if(input.val().trim()=="" && input.is("[id]")) {
					input.val($.cookie(input.attr('id')));
				}
			});
			$("#appointment_prev_step #OMSForm_birthday").val($("#appointment_prev_step #OMSForm_birthday").val().split('-').join('.'));
			var splitbirthday = $("#appointment_prev_step #OMSForm_birthday").val().split(".");
			if(splitbirthday[0].length > 3) {
				$("#appointment_prev_step #OMSForm_birthday").val(splitbirthday.reverse().join("."));
			}

			<?php if(City::model()->getSelectedCityId() === City::MOSKVA): ?>
			if($("#appointment_prev_step #OMSForm_surName").val().trim() !== ""
				&& $("#appointment_prev_step #OMSForm_firstName").val().trim() !== ""
					&& $("#appointment_prev_step #OMSForm_birthday").val().trim() !== ""
						&& $("#appointment_prev_step #OMSForm_omsNumber").val().trim() !== "") {
				prepareAppointmentForm();
			}
			<?php endif; ?>
		});
		
		//для тестовых нужд - тестируем пациента
		$(document).keypress(function( event ) {
			switch(event.keyCode) {
			case 43:
				this.keystate = (!(this.keystate > 0)) ? 1 : ((this.keystate%2==0) ? (this.keystate+1) : 0);
				break;
			case 45:
				this.keystate = (this.keystate%2==1) ? this.keystate+1 : 0;
				break;
			}
			if(this.keystate >= 5) {
				this.keystate = 0;
				$("#appointment_prev_step #OMSForm_surName").val('Король');
				$("#appointment_prev_step #OMSForm_firstName").val('Андрей');
				$("#appointment_prev_step #OMSForm_fatherName").val('Максимович');
				$("#appointment_prev_step #OMSForm_birthday").val('29.11.1972');
			}
		});
		
	</script>

<?php $this->endWidget(); ?>
