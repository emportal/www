<?php
/* @var $model AppointmentToDoctors */
/*
$this->breadcrumbs = array(
	'Запись на прием',
);*/


Yii::app()->clientScript->registerScript('search', "	
	$(document).ready(function() {
	 if(scrollY < 395) {
		window.scrollTo(0,395);
	 }
	});
");

?>

<div class="search-result">
	<div class="crumbs"><a href="/">Главная</a> > <span>Запись на прием</span></div>
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method'				 => 'post',
		'id'					 => 'doctor-visit',
		//'action'				 => CHtml::normalizeUrl(array('/default/updateLogo')),
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'inputContainer' => 'td',
			'validateOnChange' => false,
			'afterValidate' => 'js: function(form,data,hasError) { 
				if(hasError === false) {
					yaCounter18794458.reachGoal("priem_success");
					ga("send", "pageview", "/priem_success");
				}
				if(data["AppointmentToDoctors_phone"] !== undefined && data["AppointmentToDoctors_phone"][0] === "Телефон не активирован") {
					yaCounter18794458.reachGoal("priem_error_without_phone");
					$(".getCodeButton").click();
				}
				return true;
			 }'
		),
		'htmlOptions'			 => array('class'		 => 'zakaz relative', 'enctype'	 => 'multipart/form-data'),
	));
	/* @var $form CActiveForm */
	?>
	<table class="search-result-table">
		<?php if (Yii::app()->user->hasFlash('askAuth')): ?>
			<tr class="search-result-box">
				<td colspan="2">
					<div class="flash-notice askAuth">
						<?= Yii::app()->user->getFlash('askAuth'); ?>
					</div>
				</td>
			</tr>
		<?php endif; ?>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Клиника</td>
			<td class="search-result-info">
				<?= $form->hiddenField($model, 'address[link]') ?>
				<?php if(!empty($model->address)): ?>
				<?= CHtml::link(CHtml::encode($model->address->getName()), array('clinic/view', 'linkUrl' => $model->address->linkUrl, 'companyLink' => $model->company->linkUrl)); ?>
				<?php /* ЗДЕСЬ НУЖНО ДОБАВИТЬ ВЫБОР адреса врача через таблицу placeOfWorks*/ ?>
				<?php endif; ?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Врач</td>
			<td class="search-result-info">
				<?= $model->doctor->name; ?>
				<?= CHtml::hiddenField(CHtml::activeName($model, 'doctorId'),$model->doctor->link);?>
<!--				<input type="text" class="custom-text" name="doctor" value="Иванов Иван Иванович">
				<label><a href="#">убрать</a></label>-->
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Календарь <span class="required">*</span></td>
			<td class="search-result-info">
				<div id="calendar">
					<?php
						$this->renderPartial('/clinic/_calendarBlock',array(
							'model' => $model,
							'time' => $time,						
						));
					?>
				</div>				
				<input type="hidden" id="timeblock_date" value="<?=date("Y-m-d",mktime(0, 0, 0, date('m'), date('j')+1, date('Y')))?>"/>
				<?= $form->hiddenField($model, 'plannedTime'); ?>
				<?= $form->error($model, 'plannedTime') ?>
			</td>
		</tr>		
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Ваше имя <span class="required">*</span></td>
			<td class="search-result-info">
				<?= $form->textField($model, 'name', array('class' => 'custom-text')); ?><br/>
				<?= $form->error($model, 'name') ?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Телефон <span class="required">*</span></td>
			<td class="search-result-info">
				<?php if(Yii::app()->user->isGuest):?>					
					<?= $form->hiddenField($model, 'phone',	array(
								'value'=>Yii::app()->session['appointment_phone_set']?Yii::app()->session['appointment_phone_set']:''
						));
					?>
					<?php if(Yii::app()->session['appointment_phone_set']):?>
						<span id="phoneSpan" class="phone" style=""><?=Yii::app()->session['appointment_phone_set']?></span>
						<span onclick="removePhoneForAppointmentGuest();" class="btn-red" style="">отменить выбранный номер</span>			
					<?php else:?>
						<span id="phoneSpan" class="phone" style="display:none"></span>
						<input class="custom-text" style="" id="phoneInput" name="phoneInput"  type="text"/>
						<script>
						$(function() {
							$("#phoneInput").mask("+79999999999",{placeholder:"_"});
							var isGuest = 1;
						});
						</script>
						<span onclick="getCodeForAppointment();" class="getCodeButton btn-green" style="">получить код</span>			
						<span onclick="acceptCodeForAppointment();" class="acceptButton btn-green" style="display:none">подтвердить</span>
					<?php endif;?>					
					
				<?php else: ?>
					<?php if($pv): ?>
						<?= CHtml::hiddenField('default',$model->phone);?>
						<?= CHtml::activeHiddenField($model, 'phone',array(
							'value'=>$pv->phone
						)); ?>						
						<span id="phoneSpan" class="phone"><?=$pv->phone?></span>
						<span onclick="removePhoneForAppointment();" class="removeButton btn-red">Вернуть стандартный телефон</span>
					<?php else:?>
						<?= $form->hiddenField($model, 'phone') ?>
						<span id="phoneSpan" class="phone"><?=$model->phone?></span>
						<span onclick="changePhoneForAppointment()" class="btn-green changePhoneAppointment">Поменять телефон на одну запись</span>
						<span onclick="getCodeForAppointment();" class="getCodeButton btn-green" style="display:none">получить код</span>			
						<span onclick="acceptCodeForAppointment();" class="acceptButton btn-green" style="display:none">подтвердить</span>
					<?php endif;?>						
				<?php endif; ?><br/>
				<?= $form->error($model, 'phone') ?>
				<div id="phone_tooltip">
					<div class="triangle_left"></div>
					<div class="app_tooltip flash-notice">
						Ваш номер нигде не выводится и используется <b>только</b> для записи на прием.<br/>Вся процедура бесплатна.<br><br>Пример для России: <b>+79217234455</b>
					</div>
				</div>
			</td>
		</tr>
		<!--<tr class="search-result-box">
			<td class="search-result-signup size-14">Email <span class="required">*</span></td>
			<td class="search-result-info">
				<?= $form->textField($model, 'email', array('class' => 'custom-text')); ?><br/>
				<?= $form->error($model, 'email') ?>
			</td>
		</tr>-->
		<tr class="search-result-box">
			<td class="search-result-signup size-14"></td>
			<td class="search-result-info">
				<button class="btn-green" type="submit">Записаться на прием</button>
			</td>
		</tr>
	</table>
	<?php $this->endWidget(); ?>
</div>
