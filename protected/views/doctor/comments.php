<?php
/* @var $model Doctor */
/* @var $comment Comment */

$this->breadcrumbs = array(
	'Клиники'									=> $this->createUrl('search/clinic'),
	$model->currentPlaceOfWork->company->name	 => array("/clinic/view", 'companyLink' => $model->currentPlaceOfWork->company->linkUrl, 'linkUrl' => $model->currentPlaceOfWork->address->linkUrl),
	'Специалисты'								=> array("/clinic/experts/", 'companyLink' => $model->currentPlaceOfWork->company->linkUrl, 'linkUrl' => $model->currentPlaceOfWork->address->linkUrl),
	$model->name					=> array('/doctor/view', 'linkUrl' => $model->linkUrl),
	'Отзывы'
);
?>


<div class="search-result-title"><h2><?= $model->name ?></h2></div>
<?php if (empty($model->comments)) : ?>
	<h3 style="text-align: center; margin: 20px 0px">нет отзывов</h3>
<? else: ?>
	<table class="reviews-list">
		<tr>
			<td>
				<br>
				<?php foreach ($model->comments as $comment): ?>
					<div class="reviews-box">
						<i class="reviews-box-arrow"></i>
						<blockquote>
							<?= CHtml::encode($comment->review) ?>
						</blockquote>
						<p class="reviews-amount"><a href="#">Отзыв #<?= $comment->date ?>   <?= CHtml::encode($clinic->street) ?><?= CHtml::encode($clinic->houseNumber) ?></a></p>
					</div>
					<?php if($comment->answer):?>
					<div class="answer">
						<div class="reviews-box">
							<blockquote>
								<?= CHtml::encode($comment->answer) ?>
							</blockquote>
						</div>
					</div>
					<?php endif;?>
				<? endforeach; ?>
				<br>
			</td>
		</tr>
	</table>
<? endif; ?>
