<?php
/* @var $model Doctor */
$keywords = "";
if (!empty($model->specialtyOfDoctors) && is_array($model->specialtyOfDoctors)) {
	foreach ($model->specialtyOfDoctors as $specialty) :
		if ($specialty->doctorSpecialty):
			$keywords[] = $specialty->doctorSpecialty->name;
		endif;
	endforeach;
}

Yii::app()->clientScript->registerScript('search', "
	
	$(document).on('focus','.ui-autocomplete-input', function(){
		$(this).autocomplete('search');
	});

	$('a.clear-row').click(function(){
		$(this).closest('td').find('input').val('');		
		if( $(this).attr('type')=='doctor' ) {
			updateWorkingHourByDoctor();
		}		
		return false;
	});	
");

//$this->pageTitle = ($keywords ? implode(", ", $keywords) . " – " : '') . "{$model->name} – {$company->companyType->name} {$company->name} – Запись на приём – Единый медицинский портал;";
$this->pageTitle = $model->name . ' - врач ' . ($keywords ? implode(', ', $keywords) . ' ' : '') . ' - отзывы - Единый Медицинский Портал';

$pageMetaDescription;
$pageMetaDescription .= $model->name . ' - ' . ($keywords ? implode(', ', $keywords) . ' ' : '');
$pageMetaDescription .= 'клиники ' . $model->company->name . '. Отзывы о сотруднике клиники: ' . $model->name;

$pageMetaDescription = preg_replace("/&#?[a-z0-9]+;/i","", htmlspecialchars($pageMetaDescription)); //remove html special chars

Yii::app()->clientScript->registerMetaTag($pageMetaDescription, 'description');

//Yii::app()->clientScript->registerMetaTag("запись онлайн, самозапись" . ($keywords ? ', ' . mb_strtolower(implode(", ", $keywords), "utf-8") : ''), 'keywords');

$bcLinks = [
	'Клиники' => '/klinika',
	$appointment->company->name => '/klinika/' . $appointment->company->linkUrl,
	$appointment->address->street . ', ' . $appointment->address->houseNumber => $appointment->address->getPagesUrls(true, true),
	$model->name
];

$isSamozapis = Yii::app()->params['samozapis'];

if($isSamozapis) {
	$bcLinks = [
		'Самозапись' => '/klinika',
		$appointment->address->street . ', ' . $appointment->address->houseNumber => $appointment->address->getPagesUrls(true, true),
		$model->name
	];
}

$this->breadcrumbs = $bcLinks;

Yii::app()->clientScript->registerCss('dialog', '
	.ui-dialog-titlebar {
		display: none;
	}
	#show-similar-doctors {
		display:none;
	}
');
//if ($appointment->address->userMedicals->agreementNew != 1 || $doctorDeleted || !$appointment->address->isActive):
$reviewsCount = count($reviews);
$isVkApp = Yii::app()->request->getParam('vk');
$isVkForm = Yii::app()->request->getParam('vkForm');
?>

<?php if ($doctorDeleted): ?>
<div id="doctor_description_section" style="margin: auto;">
	<div class="deleted-doctor">
		<h1><?= CHtml::encode($model->name) ?></h1>
		Этот врач на данный момент не доступен для записи через Единый Медицинский Портал.
		<br>
			<?php /* if ($model->specialties): ?>
				Другие врачи по специальности 
				<?php
				foreach ($model->specialties as $key => $sp) {
					echo mb_strtolower($sp->shortSpecialtyOfDoctor->name . (count($model->specialties) > 1 && count($model->specialties) - 1 != $key ? ', ' : ''), 'utf-8');
				}
				?>
			<?php endif; */?>
		<div class="clearfix"></div>
		<div class="similar-doctors">		
			<?php foreach ($similar ? $similar : [] as $name => $spec): ?>		
				<div class="specialty-type">
					<span class="specialty-name">Другие врачи по специальности <?= CHtml::encode($name) ?></span>				
					<div class="row">
						<?php foreach ($spec as $key => $doc): ?>
							<?php
							$groupedData = [
								$doc->id => $doc
							];
							$this->renderPartial('//search/_doctor_card', [
								'data' => $doc,
								'groupedData' => $groupedData,
							]);
							?>
						<?php endforeach; ?>
						<div class="clearfix"></div>
					</div>				
				</div>
			<?php endforeach; ?>	
			<div class="clearfix"></div>
		</div>
		<div style="margin-bottom:10px">
			<div class="size-14">
				Посмотреть еще рядом по специальности: 
				<?php foreach ($specArr ? $specArr : [] as $name => $spec): ?>
					<?= CHtml::link($spec, ['search/doctor', 'metro' => 'metro-' . $appointment->address->nearestMetroStation->linkUrl, 'specialty' => 'specialty-' . $name]); ?>
				<?php endforeach; ?>
			</div>
			<?php if (empty(Yii::app()->request->cookies['doctor' . $model->link])): ?>
				<div class="report-doctor">
					Не нашли ничего подходящего?
					<span data-id="<?= $model->link ?>" class="btn btn-blue" style="">
						Пожаловаться на отсутствие врача
					</span>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php else: ?>
<div class="doctor" itemscope itemtype="http://schema.org/Person">
	<div class="SECTION">
		<div class="main_column">
			<div class="doctor_section_content">
				<div class="doctor_section_column_sign">
                    <div class="doctor_card_main_left">
                        <?php if ($model->photo): ?>
                            <?= CHtml::image($model->photoUrl, 'Врач ' . CHtml::encode($model->name), array('title' => CHtml::encode($model->name))) ?>
                        <?php else: ?>
                            <?= CHtml::image($this->assetsImageUrl . '/staff-'. ($model->sex->order==0 ? 'm' : 'f') .'.jpg', 'Врач') ?>
                        <?php endif; ?>
                        <?php if (Yii::app()->params['regions'][$addr->city->subdomain]['loyaltyProgram']
							&& $model->getIsLoyaltyProgramParticipant() && !$isVkApp) : ?>
                        <?= Yii::app()->params['empLoyaltyPageLink'] ?>
                            <div class="empLoyaltyProgram vmargin16"></div>
                        </a>
                        <?php endif; ?>
                    </div>
				</div>
				<div class="doctor_section_column_description">
					<h1 itemprop="name" class="doctor_section_name"><?= CHtml::encode($model->name) ?></h1>
					<div class="doctor_rank-address_block">
						<div>
						<?php if(count($model->scientificDegrees)): ?>
							<div class="card_row">
								<span class="item_value"><?= (count($model->scientificDegrees)) ? CHtml::encode($model->scientificDegrees[0]->name) : "" ?></span>
								<span class="item_name">
									<?= (!empty($model->scientificTitle) && !empty($model->scientificTitle->name)) ? CHtml::encode($model->scientificTitle->name) : "" ?>
								</span>	
							</div>
						<?php endif; ?>
						<?php if(!empty($model->specialtyOfDoctors)): ?>
							<?php foreach ($model->specialtyOfDoctors as $specialty) : ?>
							<div class="card_row">
								<span class="item_value">
									<?php
									if (!empty($specialty->doctorSpecialty->name)) {
										if($isVkApp) {
											echo '<span itemprop="jobTitle">'.CHtml::encode($specialty->doctorSpecialty->name).'</span>';
										}
										else {
											echo CHtml::link(
												'<span itemprop="jobTitle">'.CHtml::encode($specialty->doctorSpecialty->name).'</span>', 
												'/doctor/specialty-' . $specialty->doctorSpecialty->linkUrl
											);
										}
									}
									(!empty($specialty->doctorSpecialty->name)) ? CHtml::encode($specialty->doctorSpecialty->name) : ""; 
									?>
								</span>
								<span class="item_name">
									<?= (!empty($specialty->doctorCategory->name)) ? CHtml::encode($specialty->doctorCategory->name) : "" ?>
								</span>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
						<?php if ($model->getExperience()): ?>
							<div class="card_row">
								<span class="item_name" title="Стаж">Стаж:</span><span class="item_value"><?= $model->getExperience() ?></span>
							</div>
						<?php endif; ?>
						</div>
					</div>
					<?php if(!$appointment->address->getDisableRatingParameter()): ?>
					<div class="doctor_rate_block" itemscope itemtype="http://schema.org/Rating">
						<input type="range" min="0" max="5" value="<?= $model->roundedRating ?>" step="0.1" id="backing-<?= $model->link ?>" style="display: none;">
						<meta itemprop="ratingValue" content="<?= $model->roundedRating ?>">
						<meta itemprop="bestRating" content="5">
						<span id="rate-<?= $model->link ?>" data-rateit-value="<?= $model->roundedRating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" class="rateit" data-rateit-backingfld="#backing-<?= $model->link ?>"></span>
						<button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
						<span class="rating-text"><?= $model->boldedRoundedRating; ?></span>
						<script>
						$(function() {
							jQuery('#rate-<?= $model->link ?>').rateit({max: 5, step: 0.1, starwidth: 23, starheight: 20, backingfld: '#backing-<?= $model->link ?>'});
						});
						</script>
					</div>
					<?php endif; ?>
					
					<?php if (!$isSamozapis): ?>
						<div class="price-box">
							<span class="price-box-inner">
								<?php if ($model->inspectPrice || $model->InspectFree): ?>
									Стоимость приёма <?= (!$model->InspectFree ? $model->inspectPrice . ' Р.' : 'бесплатно') ?>
								<?php else: ?>
									Стоимость первичного приёма не указана
								<?php endif; ?>
							</span>
						</div>
					<?php endif; ?>
					
					<?php if (!Yii::app()->user->isGuest): ?>
						<?=
						CHtml::link("В избранное", array('/doctor/favorite', 'link' => $model->link), array(
								'class' => 'btn-green btnFav ' . ( $model->isFavorite ? 'added' : ''),
								'title' => $model->isFavorite ? 'Удалить из избранного' : 'Добавить в избранное',
								'style' => 'display: none;',
								'ajax' => array(
								'url' => array('/doctor/favorite', 'link' => $model->link),
								'data' => '',
								'dataType' => 'json',
								'type' => 'POST',
								'success' => 'function(data){
									if ( data[0] )
										$("a.btnFav").addClass("added").attr("title", data[1]).find("div").html(data[1]);
									else
										$("a.btnFav").removeClass("added").attr("title", data[1]).find("div").html(data[1]);
								}',
							)
						))
						?>
					<?php endif; ?>	
				</div>
			</div>
		</div>
	</div>

		
	<div class="SECTION menu_group">
		<div class="main_column">
			<div class="menu_group_content">
				<div class="menu_group_cell<?= (empty($services))? ' vhidden': '' ?>"><div class="icon_service_mini"></div><a href="#doctor_description_section" onclick="javascript: scrollToId('.information#service');return false;">Услуги</a></div>
				<div class="menu_group_cell"><div class="icon_education_mini"></div><a href="#doctor_description_section" onclick="javascript: scrollToId('.information#education');return false;">Образование</a></div>
				<div class="menu_group_cell"><div class="icon_hosp_mini"></div><a href="#doctor_description_section" onclick="javascript: scrollToId('.information#placeofwork');return false;">Место работы</a></div>
				<?php if(!$appointment->address->getDisableReviewsParameter()): ?>
				<div class="menu_group_cell"><div class="icon_comments_mini"></div><span class="menu_group_count review_count"><?=$reviewsCount ? $reviewsCount : '' ;?></span><a class="review_comments" href="#doctor_description_section" onclick="javascript: scrollToId('.information#reviews');return false;"><?=$reviewsCount ? 'проверенных отзывов' : 'Оставьте отзыв' ;?></a></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
		
	<div class="SECTION doctor_description" id="doctor_description_section">
	
		<div class="main_column">
			<div class="doctor-middle-block box">
				<?php if ($appointment->address->userMedicals->agreementNew == 1 AND !$doctorDeleted AND $appointment->address->isActive) : ?>
					<?php if(!$isVkForm): ?>
						<button class="btn-green information show-in-responsive" title="Записаться на приём" onclick="nextPageOpenContent('.main_content #appointment_block')" style="display:none; padding: 10px 20px 10px 10px; width: 100%; text-align: left; ">
							Записаться на приём
						</button>
					<?php else: ?>
						<div style="color: rgb(100,157,22); margin: 13px; font-weight: bold; text-align: center;font-size: 19px;">
							Ваша заявка на прием отправленна!
						</div>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($isVkApp): ?>
					<button class="btn-blue information show-in-responsive" title="Записаться на приём" onclick="document.location.href = '/doctor?vk=1&address=<?=Yii::app()->request->getParam('address');?>';" style="display:none; padding: 10px 20px 10px 10px; width: 100%; text-align: left; ">
						Вернуться к списку врачей
					</button>
				<?php endif; ?>
				<?php if ($model->description): ?>
				<div class="information" id="description">
					<a class="roll-more opened"><span class="information_header"><span class="icon_doctor_micro"></span>О враче</span></a>
					<div class="more">
						<p itemprop="description"><?= CHtml::encode($model->description) ?></p>
					</div>
				</div>
				<?php endif; ?>
				
				<?php if (!empty($services)): ?>
				<div class="information" id="service">
					<a class="roll-more opened"><span class="information_header"><span class="icon_service_micro"></span>Услуги</span></a>
						<ul class="services more">
							<?php foreach ($services as $key=>$serv): ?>
								<li>— <?= CHtml::encode($serv->service->name) ?></li>
							<?php endforeach; ?>
						</ul>
					<div class="more">
						<p>Информация скоро появится</p>
					</div>
				</div>
                <?php endif; ?>
				
				<div class="information" id="education">
					<a class="roll-more opened"><span class="information_header"><span class="icon_education_micro"></span>Образование</span></a>
					<div class="more">
						<?php if (!empty($model->doctorEducations) && is_array($model->doctorEducations)): ?>
						<?php foreach ($model->doctorEducations as $key => $education): ?>
						<div>
							<div class="item_head">
								<span class="item_name">
									<?= $education->typeOfEducation->name ? $education->typeOfEducation->name . ($education->yearOfStady > 0 ? ", " : "") : "" ?>
									<?= ($education->yearOfStady > 0) ? "год выпуска: ".$education->yearOfStady : "" ?>
								</span>
							</div>
							<div><?= $education->medicalSchool->fullName ?></div>
						</div>
						<?php endforeach; ?>
						<?php else: ?>
						<p>Информация скоро появится</p>
						<?php endif; ?>
					</div>
				</div>
				<?php $hasManyPlacesOfWorks = (count($model->placeOfWorks)>1); ?> 
				<div class="information" id="placeofwork"  itemprop="worksFor" itemscope itemtype="http://schema.org/Organization">
					<a class="roll-more <?= $isSamozapis ? 'opened' : '' ?>">
						<span class="information_header">
							<span class="icon_hosp_micro"></span>
							<?php if($hasManyPlacesOfWorks): ?>Места работы (<?=count($model->placeOfWorks)?>)
							<?php else: ?>Место работы<?php endif; ?>
						</span>
					</a>
					<div class="more" style="<?= $isSamozapis ? '' : 'display: none;' ?>">
						<div class="card_row">
							<span class="item_name" title="Стаж">Место работы:</span>
							<?php if($isVkApp): ?>
								<span itemprop="name"><?= CHtml::encode($model->placeOfWorks[0]->address->company->name) ?></span>
							<?php else: ?>
								<a href="<?= '/klinika/' . $model->placeOfWorks[0]->company->linkUrl ?>">
									<span itemprop="name"><?= CHtml::encode($model->placeOfWorks[0]->address->company->name) ?></span>									
								</a>
							<?php endif; ?>
						</div>	
					<?php foreach ($model->placeOfWorks as $placeOfWork): ?>
					<div style="display: table; width: 100%;">
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="card_row" style="<?=($hasManyPlacesOfWorks)?"display: table-cell;":""?>">
							<span class="item_name" title="Адрес">Адрес:</span>
							<?php if($isVkApp): ?>
								<span><?= CHtml::encode($placeOfWork->address->street) ?>, <?= CHtml::encode($placeOfWork->address->houseNumber) ?></span>
							<?php else: ?>
								<a href="<?= $placeOfWork->address->getPagesUrls(true, true) ?>">
									<span><?= CHtml::encode($placeOfWork->address->street) ?>, <?= CHtml::encode($placeOfWork->address->houseNumber) ?></span>
								</a>
							<?php endif; ?>
							<meta content="<?= CHtml::encode($placeOfWork->address->street) ?>, <?= CHtml::encode($placeOfWork->address->houseNumber) ?>" itemprop="streetAddress">
    						<meta content="<?=$placeOfWork->address->city->name;?>" itemprop="addressLocality">
						</div>
						<?php if ($placeOfWork->address->nearestMetroStation->name): ?>	
						<div class="card_row" style="<?=($hasManyPlacesOfWorks)?"display: table-cell; text-align: right;":""?>">
							<span class="item_name" title="Метро">Метро:</span>
							<span class="item_value"><?= CHtml::encode($placeOfWork->address->nearestMetroStation->name) ?></span>
						</div>
						<?php endif; ?>
					</div>
					<?php endforeach; ?>
					</div>
				</div>
				<?php if(!$appointment->address->getDisableReviewsParameter()): ?>
				<!-- отзывы -->
				<?php
					$this->renderPartial('//layouts/_showReviews',[
							'data' => array('doctorId' => $model->id ),
							'reviews' => $reviews,
					]);
				?>
				<?php 
					$this->renderPartial('//layouts/_postReview_Form',[
							'data' => array('companyId' => $model->currentPlaceOfWork->company->id,
											'addressId' => $model->currentPlaceOfWork->address->id,
											'doctorId' => $model->id, ),
					]);
				?>
				<?php endif; ?>
				<?php if(!$isVkApp): ?>				
					<div class="share_buttons" style="min-width: inherit;" onclick="countersManager.reachGoal({'forCounters' : ['yandex'], 'goalName' : 'share_doctor_card'});">				
						<script type="text/javascript" src="//yandex.st/share/share.js"	charset="utf-8"></script>
						<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter" ></div> 		
						<div class="text">Поделиться </div>
					</div>
				<?php endif; ?>
				
			</div>
			
			<div class="klinika-right-block">
				<?php if ($appointment->address->userMedicals->agreementNew == 1 AND !$doctorDeleted AND $appointment->address->isActive) : ?>
				<div id="appointment_block" class="doctor-right-block box">
					<div id="appointment_wait_while_loading" class="appointment appointment_hidden" style="padding: 270px 0px; text-align: center;">
						<h3 class="appointment_header">Обработка данных...</h3>
						<div class="icon_loading" style="margin-top: 30px;"></div>
					</div>
					
					<?php
					$extDoctor = ExtDoctor::checkDoctor($model->id, $appointment->address->id);
					if(is_object($extDoctor))
					{
						$omsForm = new OMSForm();
						$extAddress = Address::model()->getExtAddress($appointment->address->id);
						$omsForm->extAddressId = $extAddress->extId;
						$this->renderPartial('oms_form', [
							'omsForm' => $omsForm,
							'addressLinkUrl' => $appointment->address->linkUrl,
							'companyLinkUrl' => $appointment->company->linkUrl,
							'linkUrl' => $model->linkUrl,
							'doctorId' => $model->id,
						]);
					}
                    
                    $prioritizeEmpRegistryPhone = ($appointment->address->samozapis == 0) && (true == @Yii::app()->params["regions"][$appointment->address->city->subdomain]['prioritizeEmpRegistryPhone']);
                    if ($prioritizeEmpRegistryPhone) : 
                    $phones = UserRegion::getRegionPhones();
					?>
					<div class="appointment appointment-phone-prioritized t-center-align">
                        <h3 class="appointment_header">Запиcь на приём</h3>
						<!--# block name="doctorView_RegistryPhone_1" -->
						<?= $this->renderPartial("//layouts/ssi_doctorView_RegistryPhone_1", ['phones'=>$phones]); ?>
						<!--# endblock -->
						<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_doctorView_RegistryPhone_1&params[phones][empRegistryPhoneForLinkHref]=<?=urlencode($phones['empRegistryPhoneForLinkHref'])?>&params[phones][empRegistryPhone]=<?=urlencode($phones['empRegistryPhone'])?>" stub="doctorView_RegistryPhone_1" -->
                        <div>или</div>
                        <div class="btn btn-empty no-select cursor-pointer">записаться на прием онлайн</div>
                        <div class="clearfix no-margin"></div>
                    </div>
                    <?php endif; ?>
					<div id="appointment_main" class="appointment <?= (is_object($extDoctor) || $prioritizeEmpRegistryPhone) ? "appointment_hidden" : "" ?>">
					
					<!--# block name="doctorView_appointmentForm" -->
					<?= $this->renderPartial("//layouts/ssi_doctorView_appointmentForm", ['appointmentType'=>$appointment->scenario, 'doctorId'=>$model->id,'addressId'=>$addr->id, 'isVkApp' => $isVkApp]); ?>
					<!--# endblock -->
					<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_doctorView_appointmentForm&params[appointmentType]=<?=$appointment->scenario?>&params[doctorId]=<?=$model->id?>&params[addressId]=<?=$addr->id?>&params[isVkApp]=<?=$isVkApp?>" stub="doctorView_appointmentForm" -->
										
					<div class="btn_securityOfAppointment">
						<a href="#securityOfAppointment" onclick="securityOfAppointment.show(); return false;">
							Почему запись на приём через ЕМП безопасна?
						</a>
					</div>
					<div style="display:none;" class="share_buttons">				
						<script type="text/javascript" src="//yandex.st/share/share.js"	charset="utf-8"></script>							
						<div class="text" style="margin-top:2px">Поделиться</div>
						<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter" ></div> 		
					</div>
					</div>
					<div id="appointment_result" class="appointment" style="display: none;">
						<div class="search-result-table">
							<div class="appointment_print_form">
							<h2 id="appointment_result_header" class="appointment_header">Талон на приём к врачу</h2>
							<div class="card_row">
								<span class="item_name" title="Название ЛПУ">Название ЛПУ:</span>
								<span id="appointment_result_companyName" class="item_value"></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Адрес">Адрес:</span>
								<span id="appointment_result_companyAddress" class="item_value"></span>
							</div>
							<div class="card_row" id="appointment_result_doctorRoomBlock" style="display: none;">
								<span class="item_name" title="Кабинет">Кабинет:</span>
								<span id="appointment_result_doctorRoom" class="item_value"></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Имя пациента">Имя пациента:</span>
								<span id="appointment_result_patientName" class="item_value"></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Дата рождения">Дата рождения:</span>
								<span id="appointment_result_patientBirthday" class="item_value"></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Имя врача">Имя врача:</span>
								<span id="appointment_result_doctorName" class="item_value"></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Специальность">Специальность:</span>
								<span id="appointment_result_doctorSpesiality" class="item_value"></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Время приёма">Время приёма:</span>
								<span id="appointment_result_plannedTime" class="item_value"></span>
							</div>
							<div class="card_row">
								<span class="item_name" title="Идентификатор талона">ID талона:</span>
								<span id="appointment_result_IdAppointment" class="item_value"></span>
							</div>
							<?php if (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']): ?>
							<div>
								<span style="margin-top: 10px;">Если вы не можете посетить прием в указанное время, отмените запись на прием на портале http://emportal.ru или по телефону 8-495-539-3000</span>
							</div>
							<?php endif; ?>
							</div>
							<a href="#print" class="btn-blue" onclick="PrintElem($('#appointment_result .appointment_print_form').get(0)); return false;">Распечатать</a>
						</div>
					</div>
				</div>
				<?php else: ?>
				<div class="doctor_description">
					<div class="doctor-right-block box" style="width: 340px; position: absolute;">
						<div class="appointment" style="padding: 20px; width: auto;">
							
							<div style="display: block; float: left; width: auto; height: 40px; margin-right: 10px; padding: 4px;">
								<img src="<?php echo Yii::app()->request->baseUrl.'/images/icons/not_available.png'?>">
							</div>
							
							<h3 class="color2">Запись <br>недоступна!</h3>
							<p style="font-size: 15px; line-height: 20px; padding: 12px 0;">
								Запись на приём в клинику <nbr><?= CHtml::encode($model->currentPlaceOfWork->company->name) ?><nbr> и, в частности, 
								к врачу <nbr><?= CHtml::encode($model->name) ?><nbr> через Единый Медицинский Портал более недоступна.
							</p>
							
							<a href="#" onclick="scrollToId('#top_doctor_containter'); return false;" style="text-decoration: none;">
								<div class="doctor-unavailable-block">
									Посмотреть похожих врачей
								</div>
							</a>
							<div class="clearfix"></div>
							<div style="margin-bottom:10px">
							
								<!--# block name="clinicView_emailBlock" -->
								<?= $this->renderPartial("//layouts/ssi_clinicView_emailBlock", ['link'=>$model->currentPlaceOfWork->address->link, 'companyLinkUrl'=>$model->currentPlaceOfWork->address->company->linkUrl, 'linkUrl'=>$model->currentPlaceOfWork->address->linkUrl]); ?>
								<!--# endblock -->
								<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_clinicView_emailBlock&params[link]=<?=urlencode($model->currentPlaceOfWork->address->link)?>&params[companyLinkUrl]=<?=urlencode($model->currentPlaceOfWork->address->company->linkUrl)?>&params[linkUrl]=<?=urlencode($model->currentPlaceOfWork->address->linkUrl)?>" stub="clinicView_emailBlock" -->
							
							</div>							
							<div style="margin-top: 12px;">
								Вы представитель клиники?
								<br>
								<a href="/site/contact" class="blueLink" style="font-size: 10px;">Станьте партнером</a>								
							</div>							
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php $this->widget('NewsReferenceBlock',['newsReferences'=>$model->newsReference()]); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	window.show_on_unload = 0;
    $(window).bind('beforeunload', function() {
			if (window.show_on_unload == 1) {
				//window.setTimeout(function() {
					$( "#appointment_main" ).addClass( "appointment_hidden" );
					$( "#appointment_wait_while_loading" ).removeClass( "appointment_hidden" );
					scrollToId('#appointment_block');
				//}, 1);
			}
        } 
    );
</script>
<?php if (!$isVkApp) : ?>
<div class="SECTION TOP_DOCS" id="topDocsOnDoctorView" style="">
	<div class="main_column">
		<div class="top_doctor_containter" id="top_doctor_containter">
			<div class="header_section">
				<h2 class="color1">Похожие врачи</h2>
			</div>
			
			<a href="#topdoctor_drop_left" class="icon_topdoctor_drop_left"></a>
			<a href="#topdoctor_drop_right" class="icon_topdoctor_drop_right"></a>
			
			<div class="top_docs_wrapper">
				<div class="screen_loading" style="display: none;"></div>
				<div class="top_docs_content"></div>
			</div>
		</div>
		<script type="text/javascript">
		$( ".TOP_DOCS .main_column .icon_topdoctor_drop_left" ).css('visibility', 'hidden');
		$( document ).ready(function() {
            try {
				$(".appointment-phone-prioritized .btn-empty").on('click', function() {
                    $(".appointment-phone-prioritized").hide();
                    $("#appointment_main").removeClass('appointment_hidden');
                });
			} catch(e) {console.log(e)}
			try {
				load_top_docs_containter('topDocsOnDoctorView','doctor','/ajax/similarDoctor?addressLinkUrl=<?= $appointment->address->linkUrl ?>&doctorLinkUrl=<?= $model->linkUrl ?>');
			} catch(e) {console.log(e)}
		});
		</script>	
	</div>
</div>
<?php endif; ?>
<?php endif; ?>

<?php Yii::import('application.views.layouts._securityOfAppointment_modal', true); ?>

<script>
$( document ).ready(function() {
	try {
		$.ajax({
	        url: '<?= $this->createUrl('clinic/map', ['linkUrl' => $appointment->address->linkUrl, 'companyLink' => $appointment->company->linkUrl]); ?>',
	        type: "GET",
	        success: function(response) {
	        	try {
	            	$(".map_block").html(response);
	        	} catch(e) {}
	        },
	        error: function(xhr) {
	        	$(".map_block").first().html("<h3 style='text-align:right;'>Карта не загружена</h3>");
	        }
	    });
	} catch(e) {}
});
</script>