<? /* @var $data Action */ ?>
<?php /*?>
	<dt>
		<?php if($data->unlimited):?>
		Старая цена <?php echo $data->priceOld?>,
		Цена по акции <?php echo $data->priceNew?>
		<?php else:?>
			СРОК АКЦИИ:
			<?= Yii::app()->dateFormatter->formatDateTime(strtotime($data->start), 'medium', null) ?>
			-
			<?= Yii::app()->dateFormatter->formatDateTime(strtotime($data->end), 'medium', null) ?>
		<?php endif;?>
	</dt>
	<dd>
		<?//php if ( $data )?>
<!--		<img width="104" height="74" alt="1" src="images/pic-1.png">-->
		<div class="news-info">
			<h3>
				<?= Html::link($data->name, array('actions/view', 'link'=>$data->link))?>
			</h3>
			<p><?//= $data->preview ?></p>
		</div><!-- /news-info -->
	</dd>
	<?php */?>
	
<div class="stock_item_wrap">
			<a href="<?= $widget->controller->createUrl('/actions/view', array('link'=>$data->link))?>" class="stock-item">
				<div class="stock-item-inner">
					<i class="<?php if($data->type==="0") {
						echo 'ico-free';
					} else {
						echo 'ico-discount';
					}?>"></i>
					<div class="stock-item-title">
						<h3><?= CHtml::encode($data->name) ?></h3>
						<strong>
						<?php //if($data->unlimited):?>
							<?php if($data->priceOld && $data->priceNew):?>
							Старая цена <?= CHtml::encode($data->priceOld) ?>,
							Цена по акции <?= CHtml::encode($data->priceNew) ?>
							<?php elseif($data->type==="0"): ?>
							  Бесплатно
							<?php elseif($data->type==="2"): ?>
							  Подарок - <?= CHtml::encode($data->present) ?>
							<?php else:?>
							  Скидка - <?= CHtml::encode($data->discount) ?>%
							<?php endif;?>
						<?php /*else:?>
							СРОК АКЦИИ:
							<?= Yii::app()->dateFormatter->formatDateTime(strtotime($data->start), 'short', null) ?>
							-
							<?= Yii::app()->dateFormatter->formatDateTime(strtotime($data->end), 'short', null) ?>
						<?php endif;*/?>							 
						</strong><br/>
						<div class="action_links">												
							<div class="agent" data-href="<?=Yii::app()->createUrl('clinic/view',array('link'=>$data->address->link))?>"><?=CHtml::encode($data->company->name)?></div>							
							<div class="companyActivity" data-href="<?= Yii::app()->createUrl('handbook/CompanyActivites',array('link'=>$data->companyActivite->link))?>"><?= CHtml::encode($data->companyActivite->name) ?></div>																			
						</div>						
					</div><!-- /stock-item-title -->
					<p></p>
				</div><!-- /stock-item-inner -->
			</a>
</div>
