<?php
/* @var $this ActionsController */
/* @var $model Action */

$this->breadcrumbs = array(
	'Акции' => array('actions/index'),
	$model->name,
);
?>
<h2>
	<?= CHtml::encode($model->name) ?>
</h2>
<table cellspacing="0" cellpadding="0" border="0" class="view_action">
	<tbody>
		<tr>
			<td>
				Компания
			</td>
			<td>
				<a href="<?=$this->createUrl('clinic/view',array('link'=>$model->address->link))?>"><?=CHtml::encode($model->company->name)?></a>
			</td>
		</tr>
		<?php if($model->companyActivitesId): ?>
		<tr>
			<td>
				Профиль деятельности
			</td>
			<td>
				<a href="<?= Yii::app()->createUrl('handbook/CompanyActivites',array('link'=>$model->companyActivite->link))?>"><?=CHtml::encode($model->companyActivite->name)?></a>		
			</td>
		</tr>
		<?php endif;?>
		<?php if($model->serviceId): ?>
			<tr>
				<td>
					Услуга
				</td>
				<td class="first_letter_upper">
					<?=CHtml::encode($model->service->name)?>
				</td>
			</tr>
		<?php endif;?>
		<?php if(!$model->unlimited):?>
			<tr>
				<td>
					СРОК АКЦИИ:
				</td>
				<td>
					<?= Yii::app()->dateFormatter->formatDateTime(strtotime($model->start), 'short', null) ?>
						-
					<?= Yii::app()->dateFormatter->formatDateTime(strtotime($model->end), 'short', null) ?>
				</td>
			</tr>
		<?php endif; ?>
			<?php if($model->discount): ?>
			<tr>
				<td>
					Размер скидки:
				</td>
				<td>
					<?=CHtml::encode($model->discount)?>%
				</td>
			</tr>
		<?php endif;?>
		<?php if($model->priceOld):?>
			<tr>
				<td>
					Старая цена:
				</td>
				<td>
					<strike><?=CHtml::encode($model->priceOld)?> руб.</strike>
				</td>
			</tr>
		<?php endif;?>
		<?php if($model->priceNew):?>
			<tr>
				<td>
					Цена по акции:
				</td>
				<td>
					<?=CHtml::encode($model->priceNew)?> руб.
				</td>
			</tr>
		<?php endif;?>
		
		<?php /* ?><tr>
			<td>			
				<span>
					<strong>Старая цена: <strike><?php echo $model->priceOld?></strike></strong>,
						Цена по акции <?php echo $model->priceNew?>
					<?php if($model->unlimited):?>
						
						<?php else:?>
							СРОК АКЦИИ:
							<?= Yii::app()->dateFormatter->formatDateTime(strtotime($model->start), 'short', null) ?>
							-
							<?= Yii::app()->dateFormatter->formatDateTime(strtotime($model->end), 'short', null) ?>
						<?php endif;?>
					</<span>
				<!--<div><?= $model->description ?></div>-->
			</td>			
		</tr><?php */?>
		
		
		
		<?php if($model->description): ?>
			<tr>
				<td>
					Описание акции:
				</td>
				<td>
					<?=CHtml::encode($model->description)?>
				</td>
			</tr>
		<?php endif;?>
	</tbody>
</table>