<?php
/* @var $this NewsController */

$this->breadcrumbs = array(
	'Акции',
);
Yii::app()->clientScript->registerScript('actions', '
	 $(".agent").click(function(e) {
		var href = $(this).attr("data-href");
		location.href=href;
		e.stopPropagation();
		return false;
	 });
	 $(".companyActivity").click(function(e) {
		var href = $(this).attr("data-href");
		location.href=href;
		e.stopPropagation();
		return false;
	 });
		 ');

?>
<h2>Акции</h2>
<?php
$this->widget('zii.widgets.CListView', array(
	'dataProvider'	 => $dataProvider,
	'itemView'		 => '_action',
	'template'		 => "{items}\n{pager}",
	'itemsTagName'	=> 'div',
	'pagerCssClass'	 => 'pages',	
	'htmlOptions' => array(
		'class' => 'new-list'
	),
	'pager' => array(
		'cssFile' => false,
		'header' => 'Страницы: ',		
		'maxButtonCount' => 5,		
	),	
));
?>
