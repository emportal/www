<?php
foreach ($model->address->phones as $phone) {
	$html_phones.= ' '.$phone->countryCode.' '.$phone->cityCode.' '.$phone->number;
}
?>

<p>Зарегистрирована новая заявка на <?= $model->getVisitTypeText(true) ?>!</p>
<p>Создана запись на <?=strtotime($model->plannedTime) != false ? date("d.m.Y H:i",strtotime($model->plannedTime)) : '"время не указано"'?>
	в клинику "<?= CHtml::encode($model->company->name) ?>" по адресу "<?= CHtml::encode($model->address->name) ?>"<?=$html_phones?> 
	<?=$model->doctor->shortname ? ' к специалисту '.CHtml::encode($model->doctor->shortname) : '' ?></p>
<p>Имя: <?= CHtml::encode($model->name) ?></p>
<p>Телефон: <?= CHtml::encode($model->phone) ?></p>
<p>Услуга: <?= CHtml::encode($model->addressService->service->name) ?></p>
<p>Cтатус заявки: <?= CHtml::encode($model->statusText) ?></p>
<p>Дата создания заявки: <?=date("d.m.Y H:i",strtotime($model->createdDate))?></p>