<?php /*Уважаем<?=mb_strtolower($model->user->sex->name,"UTF-8")=="мужской"?'ый':'ая'?>, */?>
Уведомление об отмене записи на <?= $model->getVisitTypeText(true) ?>.
<p>
	Запись на <?= CHtml::encode($model->getVisitTypeText(true)) ?> <?=date("d.m.Y H:i",strtotime($model->plannedTime))?>
	<?php if (!$model->serviceId): ?>
	к специалисту <?= CHtml::encode($model->doctor->shortname) ?>
	<?php endif; ?>
	<b>отменена</b>
</p>
<p>Адрес клиники: <?= CHtml::encode($model->address->name) ?></p>
<?php if($model->comment): ?>
	<p>Причина отклонения: <?= CHtml::encode($model->comment) ?></p>
<?php endif; ?>
<p>Cтатус заявки: <?= CHtml::encode($model->statusText) ?></p>
<p><a href="<?=Yii::app()->createAbsoluteUrl('user/registry');?>">Просмотр текущих заявок</a></p>





