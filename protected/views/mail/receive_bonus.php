<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Добрый день, <?= CHtml::encode($model->name) ?>!
</font>
<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Теперь оплачивать поход к врачу можно за баллы!
</font>

<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Поделитесь с друзьями своим промо-кодом, и мы подарим каждому из них по 500 баллов на первый приём. Вы также получите по 500 баллов за каждого из друзей, записавшихся на прием через emportal.ru и фактически сходивших на прием. Максимальная сумма, которую можно накопить таким образом - 15000 баллов. Вы можете оплачивать часть стоимости медицинского обслуживания в клиниках по курсу 1 балл = 1 рубль.
</font>
<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	<a href="https://emportal.ru/user/bonuses">Получить бонусы</a>
</font>
<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Команда Единого медицинского портала.
</font>
<font style="font-size:10px;padding-bottom:8px;padding-top:10px;display:block;">
	<a href="https://emportal.ru/site/mailUnsubscribe?code=<?=$cancelCode;?>">Здесь можно отписаться от рассылки</a>
</font>