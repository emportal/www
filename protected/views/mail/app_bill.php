<p>
	Во вложении находится счёт на оплату и отчет по записям за указанный период, которые вы также всегда можете скачать в своем личном кабинете на emportal.ru
</p>
<p>
	Данное письмо сформировано автоматически, отвечать на него не требуется.
</p>
<?php if(isset($clinic->company->managerRelations->user)): ?>
<p>
	По возникшим вопросам вы можете связаться с вашим менеджером <?=$clinic->company->managerRelations->user->name?>, <?=$clinic->company->managerRelations->user->email?>
</p>
<?php endif; ?>