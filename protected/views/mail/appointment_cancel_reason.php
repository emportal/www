<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Здравствуйте, <?= CHtml::encode($model->name) ?>! Спасибо, что воспользовались нашим сайтом! 
</font>
<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Мы видим, что у вас не получилось попасть на приём. Пожалуйста, отметьте, по какой причине у вас не получилось попасть  к врачу: 
	<form style="margin-left: 5px;margin-top: 5px;" action="https://emportal.ru/site/mailFeedback" method="post">
		<input type="hidden" name="link" value="<?=$model->link?>">
		<input type="radio" name="reason" value="1"> изменились мои планы<br>
		<input type="radio" name="reason" value="2"> в карточке клиники или врача была указана неверная информация<br>
		<input style="margin-left: 25px;" type="text" name="reasonText2" placeholder="что именно неверно?"><br>
		<input type="radio" name="reason" value="3"> записался в другом месте<br>
		<input style="margin-left: 25px;" type="text" name="reasonText3" placeholder="где именно?"><br>
		<input type="radio" name="reason" value="4"> просто опробовал сервис<br>
		<input type="submit" style="margin-left: 25px;margin-top: 5px;" value="Ответить">
	</form>

</font>
<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Спасибо за отклик! Мы постоянно совершенствуем свой сервис. Вот код – <?= CHtml::encode($model->user->myPromocode) ?>, если ваши знакомые захотят записаться к врачу, то получат скидку в размере 500 рублей! Отошлите его друзьям. Запишитесь к врачу, а мы оплатим часть суммы!
</font>
<font style="font-size:14px;padding-bottom:8px;padding-top:10px;display:block;">
	Команда Единого медицинского портала.
</font>
<font style="font-size:10px;padding-bottom:8px;padding-top:10px;display:block;">
	<a href="https://emportal.ru/site/mailUnsubscribe?code=<?=$cancelCode;?>">Здесь можно отписаться от рассылки</a>
</font>