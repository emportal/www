<p>
	Уважаемый Клиенты!
</p>
<p>
	Мы успешно обновили наши банковские реквизиты. Благодаря этому мы сможем быстрее видеть поступающие от Вас платежи и работа системы будет еще удобнее.
</p>
<p>
	Просим всех тех клиентов, кто имеет задолженности, скачать новые счета для оплаты в личных кабинетах и производить оплату уже на новый счет.
</p>
<p>
	Спасибо за Ваше понимание!
</p>
<p>
    С уважением,
    <br>Руководитель Единого Медицинского портала
    <br>Митрофанов Владимир"
</p>