<?php /*Уважаем<?=mb_strtolower($model->user->sex->name,"UTF-8")=="мужской"?'ый':'ая'?>, */?>
<p><?= CHtml::encode($model->name) ?>,</p>
<p>Запись на прием <?=date("d.m.Y H:i",strtotime($model->plannedTime))?> к специалисту 
	<?= CHtml::encode($model->doctor->shortname) ?> отменена!</p>
<p>Адрес клиники: <?= CHtml::encode($model->address->name) ?></p>
<?php if($model->comment): ?>
	<p>Причина отклонения: <?= CHtml::encode($model->comment) ?></p>
<?php endif; ?>
<p>Cтатус вашей заявки: <?= CHtml::encode($model->statusText) ?></p>
<p><a href="<?=Yii::app()->createAbsoluteUrl('user/registry');?>">Просмотр текущих заявок</a></p>





