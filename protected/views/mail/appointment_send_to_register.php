<?php /*Уважаем<?=mb_strtolower($model->user->sex->name,"UTF-8")=="мужской"?'ый':'ая'?>, */?>
<p><?= CHtml::encode($model->name) ?>,</p>
<p>Вы создали запись на <?=strtotime($model->plannedTime) != false ? date("d.m.Y H:i",strtotime($model->plannedTime)) : '"время не указано"'?>
		<?= $model->doctor->shortname ? ' к специалисту '.CHtml::encode($model->doctor->shortname) : '' ?></p>
<p>Адрес клиники: <?= CHtml::encode($model->address->name) ?></p>
<p>Cтатус вашей заявки: <?= CHtml::encode($model->statusText) ?></p>
<?php if(!Yii::app()->user->isGuest):?>
	<p><a href="<?=Yii::app()->createAbsoluteUrl('user/registry');?>">Просмотр текущих заявок</a></p>
<?php endif;?>

<p>Как только специалисты клиники обработают вашу заявку, вы получите подтверждение в виде смс или звонка представителя клиники. Для обеспечения высокого качества обслуживания операторы Единого Медицинского Портала будут контролировать процесс обработки вашей заявки сотрудниками клиники.</p>




