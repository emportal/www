<p>Зарегистрирована новая заявка на <?= $model->getVisitTypeText(true) ?></p>
<p>Создана запись на <?=strtotime($model->plannedTime) != false ? date("d.m.Y H:i",strtotime($model->plannedTime)) : '"время не указано"'?>
		<?= $model->doctor->shortname ? ' к специалисту '.CHtml::encode($model->doctor->shortname) : '' ?></p>
<p>Адрес клиники: <?= CHtml::encode($model->address->name) ?></p>
<p>Cтатус заявки: <?= CHtml::encode($model->statusText) ?></p>
<p>
	<a href="<?=Yii::app()->createAbsoluteUrl('adminClinic/default/login',array('h'=>$model->id, 'key'=>$model->autoLoginKey));?>">
		Просмотреть заявку
	</a>
</p>
<p>Дата создания заявки: <?=date("d.m.Y H:i",strtotime($model->createdDate))?></p>
<p><small>Ссылка в данном письме позволяет автоматически авторизоваться в личном кабинете клиники только 1 раз.</small></p>