<p>Приветствуем вас на сайте Единый Медицинский Портал!</p>

<p>
    Ваши регистрационные данные:
    <br>Имя: <?= CHtml::encode($name) ?>
    <br>Логин: <?= CHtml::encode($login) ?>
</p>

<?php if($code): ?>
<p>Для того, чтобы продолжить регистрацию, вам необходимо перейти по ссылке<br>
<?php $link = Yii::app()->createAbsoluteUrl('/site/activate', array('key'=>$code))?>
<?= CHtml::link($link, $link); ?></p>
<?php endif; ?>
<p>Если данное письмо пришло к вам по ошибке, просто удалите его.</p>