<?php
/* @var $model Address */

$this->breadcrumbs = array(
	'Страховые компании' => '/search/insurance',
	$model->company->name	 => array($this->createUrl('view'), 'link' => $model->link),
	'Все адреса'
);
?>

<div class="search-result-title">
	<h2>
		<?= CHtml::encode($model->company->name) ?>
		<small>
			<?= CHtml::encode($model->company->companyType->name) ?>
		</small>
	</h2>
</div>
<?php if (empty($model->company->addresses)) : ?>
	<h3 style="text-align: center; margin: 20px 0px">нет адресов</h3>
<? else: ?>
	<table class="reviews-list">
		<tr>
			<td>
				<div class="search-result-title">
				
				<?php foreach ($model->company->addresses as $address): ?>
				<h2>
					<?= Html::link(CHtml::encode($address->name), array($this->createUrl('view'), 'link' => $address->link)) ?>
				</h2>
				<br />
				<? endforeach; ?>
				</div>
			</td>
		</tr>
	</table>
<? endif; ?>

<script>
	$(document).ready(function() {
		$('.title-row').next('table').css('display', 'none');
		$('.title-row').toggle(
				function() {
					$('.title-row').next('table').hide('slow');
					$(this).next('table').show('slow');
				},
				function() {
					$(this).next('table').hide('slow');
				}
		);
	});
</script>
