<style>
	#.info-table tr td:nth-child(2) {background-color: #00B0CF;}
	.info-table tr td {padding: 4px; border: 1px solid #eee; background-color: #fafafa;}
	.info-table {width: 500px;}
</style>
<div style="clear: left;">
<div style="padding: 15px 0 0 0;">
	<h1>Вы успешно записались на прием к врачу!</h1>
	<table class="info-table">
		<tr>
			<td>Время</td>
			<td><?=$storedAppointment['plannedTime']?></td>
		</tr>
		<tr>
			<td>Врач</td>
			<td><?=$storedAppointment['doctorName']?></td>
		</tr>
		<tr>
			<td>Клиника</td>
			<td><?=$storedAppointment['companyName']?></td>
		</tr>
		<tr>
			<td>Ваше имя</td>
			<td><?=$storedAppointment['name']?></td>
		</tr>
		<tr>
			<td>Ваш телефон</td>
			<td><?=$storedAppointment['phone']?></td>
		</tr>
	</table>
</div>
<?php if (!empty($storedAppointment['hiddenField']) || !Yii::app()->session['widgetReferrer']) : ?>

	<div style="color: #fff;"><?=$storedAppointment['hiddenField']?></div>
	<!-- для 2GIS GA Counter'а - срабатывание об успешной заявке -->
	<script type="text/javascript">
		function sendMetric() {
			try{
			ga('send', 'event', 'order', 'sent');
			}
			catch(e) {
			console.log(e);
			}
		}
		
		if(window.addEventListener){
			window.addEventListener('load', function(){
				sendMetric();
			}, false);
		} else {
			window.attachEvent('onload', function(){
				sendMetric();
			});
		}  
	</script>
	
<?php endif; ?>
</div>