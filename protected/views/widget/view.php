<?php
/* @var $model Address */
$this->pageTitle = "{$model->company->seoName} - {$model->company->companyType->name} - Запись на прием - " . Yii::app()->name;
Yii::app()->clientScript->registerMetaTag("Запись в клинику {$model->company->seoName} - {$model->company->companyType->name},"
		. " {$model->name}, " . (!empty($model->nearestMetroStation->name) ? "м. " . $model->nearestMetroStation->name : '') . ". " . Yii::app()->name . " запись в клиники онлайн", 'description');
?>
<?php if ($model->userMedicals->agreementNew != 1 || !$model->isActive): ?>
	<p style="font-size: 15px; line-height: 20px;">Дорогой пациент, запись на прием в клинику <?= $model->company->name; ?> через Единый Медицинский Портал более недоступна. Рекомендуем записаться на прием в другие ближайшие к вам клиники</p>	
	<div class="clearfix"></div>
	<div class="similar-doctors">		
		<div class="specialty-type">
			<div class="row">
				<?php $count = 0; ?>
			<?php foreach($similar as $key=>$clinic): $count++; ?>
				<?php 
					$this->renderPartial('//search/_clinics_unique',[
						'data' => $clinic,
						'address' => $clinic->addresses[0],
						'addresses' => null,
						'big' => 0,					
					]);
				?>
				<?= $count % 2 == 0 && $count !== count($similar) ? '<div class="clearfix"></div></div><div class="row">' : ''; ?>
			<?php endforeach; ?>
				<div class="clearfix"></div>
			</div>
		</div>		
		<div class="clearfix"></div>
	</div>
	<div style="margin-bottom:10px">
		<div class="size-14">			
			<a style="display:inline-block;margin-top:10px" href="<?= $this->createUrl('search/clinic',['metro' => 'metro-'.$metroLink]); ?>">Посмотреть еще рядом</a>
			
		</div>
	<?php if (empty(Yii::app()->request->cookies['clinic' . $model->link])): ?>
			<div class="report-clinic">
				Не нашли ничего подходящего?
				<span data-id="<?= $model->link ?>" class="btn btn-blue" style="">
					Пожаловаться на отсутствие клиники
				</span>
			</div>
	<?php endif; ?>
	</div>

	<?php /* $this->endWidget(); */ ?>

<?php else: ?>
<div style="margin-top: 10px;">
	<!-- <h1 id="info" style="display: inline-block; border: none;"><?= $model->company->name . ', ' . $model->street . ', ' . $model->houseNumber ?></h1>  -->
	
	<div style="float: right;">
		<div class="short-search-box" style="border: none; margin-right: 20px;">
		
			<div class="left-filter">				
				<?php
					$form = $this->beginWidget('CActiveForm', array(
						'method' => 'get',
						'id' => 'search-doctor',
						'action' => '/doctor',
						'enableAjaxValidation' => false,
						'htmlOptions' => array('enctype' => 'multipart/form-data'),
					));
					echo CHtml::hiddenField('isWidget', 1);
					echo CHtml::hiddenField('shortSearch', 1);
					echo CHtml::hiddenField(CHtml::activeName($searchModel, 'addressLink'), $model->link);
					$this->widget('ESelect2', [
						'name' => CHtml::activeName($searchModel, 'doctorSpecialtyId') . '_',
						'data' => $doctorSpecialties,
						'value' => $searchModel->doctorSpecialtyId,
						'options' => [
							'placeholder' => 'Специальность врача',
							'allowClear' => true,
						],
						'htmlOptions' => [
							'style' => 'margin-right: 20px; margin-top: 4px; width: 200px;',
						]
					]);
	
					/*$this->widget('ESelect2', [
						'name' => CHtml::activeName($searchModel, 'sexId') . '_',
						'data' => CHtml::listData(Sex::model()->findAll(), 'id', 'name'),
						'options' => [
							'placeholder' => 'Пол специалиста',
							'allowClear' => true,
							'minimumResultsForSearch' => -1,
						],
						'htmlOptions' => [
							'class' => 'search-doctor-refresh'
						]
					]);*/
					?>
					<?php
					$metro = Yii::app()->request->getParam('Search')['metroStationId'] ? Yii::app()->request->getParam('Search')['metroStationId'] : '';
					Yii::app()->clientScript->registerScript('select22', '
					$(document).ready(function() {		
						$("#Search_sexId1").select2("val", ["' . Yii::app()->request->getParam('Search')['sexId'] . '"]); 
						$("#Search_price1").select2("val", ["' . Yii::app()->request->getParam('Search')['price'] . '"]); 					
							
					});	
					');
					$this->endWidget();
					?>		
				</div>
			<div class="clearfix"></div>
		</div>
	</div>
	
	
	<div class="short-doctor-search appointment">		
		
		<div class="search-result" style="margin-left: 0;">
			<?php
			$this->widget('zii.widgets.CListView', array(
				'dataProvider' => $dataProvider,
				'itemView' => '//search/_doctors_for_widget',
				'itemsTagName' => 'table',
				'template' => '<div class="search-result-head" style="padding-left: 0; padding-top: 15px;">
							<span class="search-found">Найдено врачей: <a title="' . $dataProvider->totalItemCount . ' врачей">' . $dataProvider->totalItemCount . '</a></span>'
				. '{pager}</div>'
				. "\n{items}\n{pager}",
				'emptyText' => $emptyText,
				'afterAjaxUpdate' => 'function(id,date) { window.scrollTo(0, $("#doctors").offset().top); }',
				'viewData' => array(
					'show' => $show,
					'doctors' => $doctors
				),
				'pager' => array(
					'firstPageLabel' => '',
					'prevPageLabel' => '< ',
					'nextPageLabel' => ' >',
					'lastPageLabel' => '',
					'maxButtonCount' => 5,
					'header' => '',
					'cssFile' => false
				),
				'htmlOptions' => array(
					"class" => "search_with_pager"
				)
			));
			?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php endif; ?>