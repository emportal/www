
<div style="clear: left;">
<a href="/widget/<?=$company->linkUrl?>/<?=$addressLinkUrl?>" style="display: block; padding: 10px 0;">назад к списку</a>
<h1 style="border: none; display: inline-block; margin: 0 auto;"> <?=$model->name?> </h1>
<br>
<div class="doctor-left-block box" style="margin: 20px 0 0 0; border: 0px solid; width: 300px;">
	<div class="appointment">
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'doctor-visit',
			'action' => CHtml::normalizeUrl([
				'/doctor/view',
				'addressLinkUrl' => $appointment->address->linkUrl,
				'companyLinkUrl' => $appointment->company->linkUrl,
				'linkUrl' => $model->linkUrl
			]),
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'inputContainer' => 'td',
				'validateOnChange' => false,
				'beforeValidate' => 'js:function(form) {
				if($("#code").val()) {
					acceptCodeForAppointment();
				}
				
				return true;
			}',
				'afterValidate' => 'js: function(form,data,hasError) { 
				if(hasError === false) {
					if (typeof yaCounter18794458 != \'undefined\') yaCounter18794458.reachGoal("priem_success");
					if (typeof ga != \'undefined\') ga("send", "pageview", "/priem_success");
				} 

				if(data["AppointmentToDoctors_phone"] !== undefined && data["AppointmentToDoctors_phone"][0] === "Телефон не активирован") {
					/*yaCounter18794458.reachGoal("priem_error_without_phone");*/
					if($(".acceptButton:visible").length == 0 ) {
						$(".getCodeButton").click();
					}
				}	
			
				
				if(form.find("#AppointmentToDoctors_phone").val() == "") {
					return false;
				}
												
				return true;
			 }'
			),
			'htmlOptions' => array('class' => 'zakaz relative', 'enctype' => 'multipart/form-data'),
		));
		/* @var $form CActiveForm */
		?>
		<div style="text-align:center;">Запиcь на прием</div>
		<?= CHtml::hiddenField('isWidget', 1); ?>
		<?= $form->hiddenField($appointment, 'address[link]') ?>
		<?= CHtml::hiddenField(CHtml::activeName($appointment, 'doctorId'), $appointment->doctor->link); ?>
		<table class="search-result-table">

	<!--<tr class="search-result-box">
	<td class="search-result-signup size-14">Клиника</td>
	<td class="search-result-info">
			<?= $form->hiddenField($appointment, 'address[link]') ?>
			<?= CHtml::link($appointment->address->getName(), array('clinic/view', 'linkUrl' => $appointment->address->linkUrl, 'companyLink' => $appointment->company->linkUrl)); ?>
			<?php /* ЗДЕСЬ НУЖНО ДОБАВИТЬ ВЫБОР адреса врача через таблицу placeOfWorks */ ?>
	</td>
	</tr>
	<tr class="search-result-box">
	<td class="search-result-signup size-14">Врач</td>
	<td class="search-result-info">
			<?php $appointment->doctor->name; ?>
			<?= CHtml::hiddenField(CHtml::activeName($appointment, 'doctorId'), $appointment->doctor->link); ?>
	</td>
	</tr>-->
	<!--<tr class="search-result-box">
	<td class="search-result-signup size-14">
	Услуга <span class="required">*</span>
			<?= $form->hiddenField($appointment, 'address[link]') ?>
			<?= CHtml::hiddenField(CHtml::activeName($appointment, 'doctorId'), $appointment->doctor->link); ?>
	</td>
	<td class="search-result-info">		

			<?php if ($showPicker): ?>					
				<?= $form->hiddenField($appointment, 'serviceId') ?>
				<?php
				$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
					'model' => $appointment,
					'name' => 'serviceName',
					//'attribute'		 => 'city.name',
					'value' => $appointment->service->name,
					'source' => 'js:function (request, response) {	
		
		 $.ajax({
			url: "/ajax/serviceByClinic",
			dataType: "json",
				data: {
					clinicLink: $("#' . CHtml::activeId($appointment, 'address[link]') . '").val(),
					term: request.term
				},
				success: response
			});
	}',
					// additional javascript options for the autocomplete plugin
					'options' => array(
						'minLength' => '0',
						'select' => 'js:function(event,ui) {  
				if ( ui.item ) {
					$( "#' . CHtml::activeId($appointment, 'serviceId') . '" ).val( ui.item.id );
					getPriceByDoctorAndService();
					return true;
				} else {
					$(this).val( "" );
					$( "#' . CHtml::activeId($appointment, 'serviceId') . '" ).val( "" );
					getPriceByDoctorAndService();
					return false;
				}
	
			}
		'
					),
					'htmlOptions' => array(
						'class' => 'custom-text',
					),
				));
				?>				
																				<label><a class="clear-row" href="#">убрать</a></label><br/>
				<?= $form->error($appointment, 'serviceId') ?>
			<?php else: ?>
				<?= $form->hiddenField($appointment, 'serviceId', array('value' => 'first')) ?>
																				<span>Первоначальное обращение</span>
			<?php endif; ?>

	</td>
	</tr>-->
			<tr class="search-result-box">
				<td class="search-result-info">
					<div class="relative ap_cal">
						<div id="calendar">
							<?php
							$this->renderPartial('/clinic/_newCalendarBlock', array(
								'model' => $appointment,
								'time' => $time,
								'short' => true
							));
							?>
						</div>				
						<input type="hidden" id="timeblock_date" value="<?= date("Y-m-d", mktime(0, 0, 0, date('m'), date('j'), date('Y'))) ?>"/>
						<?= $form->hiddenField($appointment, 'plannedTime'); ?>
						<?= $form->error($appointment, 'plannedTime') ?>
					</div>
				</td>
			</tr>		
			<tr class="search-result-box">

				<td class="search-result-info">
					<div class="relative ap_name">
						<?= $form->textField($appointment, 'name', array('class' => 'custom-text', 'placeholder' => 'Ваше имя')); ?><br/>
						<?= $form->error($appointment, 'name') ?>
					</div>
				</td>
			</tr>
			<tr class="search-result-box">					
				<td class="search-result-info">
					<div class="relative ap_phone">
						<?php if (Yii::app()->user->isGuest): ?>					
							<?=
							$form->hiddenField($appointment, 'phone', array(
								'value' => Yii::app()->session['appointment_phone_set'] ? Yii::app()->session['appointment_phone_set'] : ''
							));
							?>
							<?php if (Yii::app()->session['appointment_phone_set']): ?>
								<span id="phoneSpan" class="phone" style=""><?= Yii::app()->session['appointment_phone_set'] ?></span>
								<span onclick="removePhoneForAppointmentGuest();" class="btn-red" style="">отменить номер</span>			
							<?php else: ?>
								<span id="phoneSpan" class="phone" style="display:none"></span>
								<input placeholder="Ваш телефон" class="custom-text" style="" id="phoneInput" name="phoneInput"  type="text"/>
								<span class="phone-help">На телефон придет код подтверждения</span>
								<script>
								$(function() {
									$("#phoneInput").mask("+79999999999", {placeholder: "_"});
									var isGuest = 1;
								});
								</script>
								<span onclick="getCodeForAppointment();" class="getCodeButton btn-green" style="display:none">получить код</span>			
								<span onclick="acceptCodeForAppointment();" class="acceptButton btn-green" style="display:none">подтвердить</span>
							<?php endif; ?>					

						<?php else: ?>
							<?php if ($pv): ?>
								<?= CHtml::hiddenField('default', $appointment->phone); ?>
								<?=
								CHtml::activeHiddenField($appointment, 'phone', array(
									'value' => $pv->phone
								));
								?>						
								<span id="phoneSpan" class="phone"><?= $pv->phone ?></span>
								<span onclick="removePhoneForAppointment();" class="removeButton btn-red">Вернуть стандартный телефон</span>
							<?php else: ?>
								<?= $form->hiddenField($appointment, 'phone') ?>
								<span id="phoneSpan" class="phone"><?= $appointment->phone ?></span>
								<span onclick="changePhoneForAppointment()" class="btn-green changePhoneAppointment">Поменять телефон на одну запись</span>
								<span onclick="getCodeForAppointment();" class="getCodeButton btn-green" style="display:none">получить код</span>			
								<span onclick="acceptCodeForAppointment();" class="acceptButton btn-green" style="display:none">подтвердить</span>
							<?php endif; ?>						
						<?php endif; ?>
						<?= $form->error($appointment, 'phone') ?>
						<div style="display:none" id="phone_tooltip">
							<!--<div class="triangle_left"></div>-->
							<div class="app_tooltip flash-notice">
								Ваш номер нигде не выводится и используется <b>только</b> для записи на прием.<br/>Вся процедура бесплатна.<br><br>Пример для России: <b>+79217234455</b>
							</div>
						</div>
					</div>
				</td>
			</tr>
			<!--<tr class="search-result-box">
				<td class="search-result-signup size-14">Email <span class="required">*</span></td>
				<td class="search-result-info">
			<?= $form->textField($appointment, 'email', array('class' => 'custom-text')); ?><br/>
			<?= $form->error($appointment, 'email') ?>
				</td>
			</tr>-->
			<tr class="search-result-box">

				<td class="search-result-info">
					<button class="btn-green" type="submit">Записаться на прием</button>
				</td>
			</tr>
		</table>
		<?php $this->endWidget(); ?>

		<div class="address-block" style="margin-top: 20px;">

			<div class="company-info-name">								
				<a onclick="return false;" href="<?= $this->createUrl('clinic/view', ['companyLink' => $appointment->company->linkUrl, 'linkUrl' => $appointment->address->linkUrl]) ?>" class="company-icon">
					<span></span><?= $appointment->company->name ?>
				</a>
			</div>	
			<div class="street">
				<a onclick="return false;" href="<?= $this->createUrl('clinic/map', ['linkUrl' => $appointment->address->linkUrl, 'companyLink' => $appointment->company->linkUrl]); ?>" class="address">
					<span></span><?= $appointment->address->street ?>, <?= $appointment->address->houseNumber ?>
				</a>
			</div>
			<?php if ($appointment->address->nearestMetroStation->name): ?>								
				<div class="metro" style="display: block; clear: both;">
					<span></span><?= $appointment->address->nearestMetroStation->name ?>
				</div>
			<?php endif; ?>

			<div class="clearfix"></div>
		</div>
	</div>
</div>
</div>