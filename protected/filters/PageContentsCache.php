<?php 

class PageContentsCache extends CFilter
{
	public $cacheIndex;
	public $cacheLifeTime = 10800; //3 часа
	
    protected function preFilter($filterChain)
    {
		if ($this->skipFilter())
			return true;
		
		//$this->cacheIndex = Yii::app()->getBaseUrl(true) . Yii::app()->request->url . (MyTools::desktop_version() ? "#desktop_version" : "");
		//$pageCache = Yii::app()->cache->get($this->cacheIndex);
		$pageCache = false;
		if ($pageCache)
		{
			echo $pageCache;
			Yii::app()->end();
		}
		ob_start();
        return true;
    }
 
    protected function postFilter($filterChain)
    {
		if ($this->skipFilter())
			return true;
		
		$content = ob_get_clean();
		
		//if (Yii::app()->user->isGuest) Yii::app()->cache->set($this->cacheIndex, $content, $this->cacheLifeTime);
		
		echo $content;
    }
    
    private function skipFilter()
    {
        return (!Yii::app()->user->isGuest
        || Yii::app()->request->getParam('AppointmentToDoctors')
        || (Yii::app()->session['selectedRegion'] === 'moskva' AND Yii::app()->params['samozapis'])) ? 
        true : false;
    }
}
