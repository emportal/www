<?php 

class CheckBlockedAppointment extends CFilter
{
    protected function preFilter($filterChain)
    {
		$user = Yii::app()->user->model;
		if(!Yii::app()->user->isGuest && $user->isBlockedAppointment != User::APPOINT_ALLOW) {
			if($user->isBlockedAppointment == User::APPOINT_DISALLOW) {
				$user->isBlockedAppointment=User::APPOINT_BLOCKED;
				$user->blockedTime = time();
				$user->save();
			}
			
			if(isset($_REQUEST['JSON'])) {
				echo CJSON::encode(['success'=>false,'blockedappointment'=>true]);
				Yii::app()->end();
			}
			
			Yii::app()->session['returnUrl']= Yii::app()->controller->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request),$_GET);
			
			Yii::app()->controller->redirect(array('site/blockedappointment'));
		}
       
        return true; 
    }
 
    protected function postFilter($filterChain)
    {
        // код, выполняемый после выполнения действия
    }
}
