<?php 

class CheckExpirePhone extends CFilter
{
    protected function preFilter($filterChain)
    {
		if(!Yii::app()->user->isGuest) {
			Yii::app()->session['returnUrl']= Yii::app()->controller->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request),$_GET);
			//3 месяца
			if( !((time() - strtotime (Yii::app()->user->model->phoneActivationDate)) < Yii::app()->params['phoneActivationLifetime']) ) {	
				if(isset($_REQUEST['JSON'])) {
					echo CJSON::encode(['success'=>false,'activatephone'=>true,'phone'=>Yii::app()->user->model->telefon]);
					Yii::app()->end();
				}
				Yii::app()->controller->redirect(array('site/activatephone'));
			} 
		}
       
        return true; 
    }
 
    protected function postFilter($filterChain)
    {
        // код, выполняемый после выполнения действия
    }
}
