<?php 

class HttpSimpleAuth extends CFilter
{
	public static $userAPI;
    protected function preFilter($filterChain)
    {
		$username = null;
		$password = null;
		if (isset($_SERVER['PHP_AUTH_USER'])) { // mod_php
			$username = $_SERVER['PHP_AUTH_USER'];
			$password = $_SERVER['PHP_AUTH_PW'];
		} elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) { // most other servers
			if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'basic')===0)
			list($username,$password) = explode(':',base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
		}
		
		if (is_null($username) || !$this->checkIfValidUser($username, $password)) {
			header('WWW-Authenticate: Basic realm="EMP"');
			header('HTTP/1.0 401 Unauthorized');
			$errors['errors'] = [
				[
					'code' => '1',
					'message' => 'HTTP/1.0 401 Unauthorized',
				]
			];
			if (Yii::app()->request->getParam('json')) {
				header("Content-type: application/json; charset=utf-8");
				echo CJSON::encode(["result"=>$errors]);
			} else {
				header("Content-type: text/xml; charset=utf-8");
				$xml = Array2XML::createXML("result", ["error"=>$errors]);
				echo $xml->saveXML() . "\n";
			}
			return false;
		} else {
			return $this->checkIfValidUser($username, $password);
		}
		
    }
	
	protected function checkIfValidUser($username, $password)
	{
		$searchAttributes = [
			'accessKey' => $password,
			'isActive' => '1',
		];
		self::$userAPI = UserAPI::model()->findByAttributes($searchAttributes);
		Yii::app()->controller->userAPI = self::$userAPI;
		return (self::$userAPI) ? true : false;
	}
 
    protected function postFilter($filterChain)
    {
		//...
    }
}

?>