<?php 

class isOfferAccepted extends CFilter
{
    protected function preFilter($filterChain)
    {
		$user = Yii::app()->user->model;
		
		if(!Yii::app()->user->isGuest && $user->agreementNew == 0 && Yii::app()->controller->action->id != 'logout' && !Yii::app()->session['adminClinic'])
			Yii::app()->controller->redirect("/".Yii::app()->controller->module->id."/default/offer");
		
        return true; 
    }
 
    protected function postFilter($filterChain)
    {
        // код, выполняемый после выполнения действия
    }
}
