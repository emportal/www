<?php 

class accessControlForMobile extends CFilter
{
    protected function preFilter($filterChain)
    {
		/**
		 * only for profile now
		 */
		if(Yii::app()->user->isGuest && isset($_REQUEST['JSON'])) {
			echo CJSON::encode(array('success'=>false,'msg' => 'Необходимо авторизоваться','logonRequired'=>true));
			Yii::app()->end();
		}
       
        return true; 
    }
 
    protected function postFilter($filterChain)
    {
        // код, выполняемый после выполнения действия
    }
}
