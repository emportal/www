<?php 

class PageLogger extends CFilter
{
	public $attributes;
	
    protected function preFilter($filterChain)
    {
    	$doctorFilterDeleted = Doctor::$filterDeleted;
    	$companyShowRemoved = Company::$showRemoved;
    	$addressShowInactive = Address::$showInactive;
    	Doctor::$filterDeleted = false;
    	Company::$showRemoved = true;
    	Address::$showInactive = true;
    	try {
	    	$controllerName = get_class($filterChain->controller);
	    	$actionId = ($filterChain->action->id);
			if($actionId === "view") {
		    	$linkUrl = Yii::app()->request->getParam('linkUrl');
		    	$link = Yii::app()->request->getParam('link');
		    	if($controllerName === "ClinicController") {
		    		UpdateCounters::updateAddressCounter((!empty($linkUrl)?$linkUrl:$link));
		    	}
		    	elseif ($controllerName === "DoctorController") {
		    		UpdateCounters::updateDoctorCounter((!empty($linkUrl)?$linkUrl:$link));
		    	}
			}
    	} catch (Exception $e) { echo "Ошибка: ".$e->getMessage().PHP_EOL; }
		Doctor::$filterDeleted = $doctorFilterDeleted;
		Company::$showRemoved = $companyShowRemoved;
		Address::$showInactive = $addressShowInactive;
        return true;
    }
 
    protected function postFilter($filterChain)
    {
        return true;
    }
}
