<?php

class AjaxController extends Controller {

	public $layout = 'empty';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'cities' => array(
				'class' => 'AutoCompleteAction',
				'model' => 'City',
				'attributeValue' => 'name',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionPostReview() {
		$response = [];
		try {
		if(isset($_POST["review"])) {
			$review = new Review();
			$review->addressId = $_POST["review"]["addressId"];
			$review->companyId = $_POST["review"]["companyId"];
			if(!empty($_POST["review"]["appointmentToDoctorsId"])) $review->appointmentToDoctorsId = $_POST["review"]["appointmentToDoctorsId"];
			if(!empty($_POST["review"]["doctorId"])) $review->doctorId = $_POST["review"]["doctorId"];
			$ratingClinic = floatval($_POST["review"]["ratingClinic"]);
			$ratingDoctor = floatval($_POST["review"]["ratingDoctor"]);
			if($ratingClinic > 0 && $ratingClinic <= 1) {
				$review->ratingClinic = $ratingClinic;
			}
			if($ratingDoctor > 0 && $ratingDoctor <= 1) {
				$review->ratingDoctor = $ratingDoctor;
			}
			$review->reviewText = trim($_POST["review"]["reviewText"]);
			$review->userName = $_POST["review"]["userName"];
			if (Yii::app()->user->isGuest) {
				$review->statusId = 0;
			} else {
				$review->userId = Yii::app()->user->model->id;
				$review->userPhone = Yii::app()->user->model->telefon;
				if(Yii::app()->user->model->phoneActivationStatus) {
					$review->statusId = 1;
				} else {
					$review->statusId = 0;
				}
			}
			
			if($review->save()) {
				$response["reviewId"] = $review->id;
				if($review->statusId) {
					$response["status"] = "OK";
					$response["text"] = "Отзыв отправлен!";
					$amoCRM = new AmoCRM();
					$amoCRM->setRew($review);
				} else {
					$response["status"] = "TOCONFIRM";
					//$response["text"] = "Подтвердите отзыв";
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "" . var_export($review->getErrors(), true);
			}
		}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "" . $e->getMessage();
		}
		echo json_encode($response);
	}
	
	public function actionConfirmReview() {
		if(isset($_POST["reviewId"])) {
			$review = Review::model()->findByPk($_POST["reviewId"]);
			if($review) {
				if(isset($_POST["phone"])) {
					$smsgate = new SmsGate();
					$code = $smsgate->generateCode();
	
					$review->confirmCode = $code;
					$review->userPhone = $_POST["phone"];
					//$response["confirmCode"] = $review->confirmCode;
					
					if($review->update()) {
						$smsgate->setMessage('Код подтверждения: _code_.', array(
							'_code_' => $review->confirmCode
						)); // = SmsGate::VERIFICATION;				
						$smsgate->phone = $_POST["phone"];
						if($smsgate->send()) {
							$response["status"] = "OK";
							$response["text"] = "Код отправлен";
						} else {
							$response["status"] = "ERROR";
							$response["text"] = "Ошибка отправки СМС";
						}
					} else {
							$response["status"] = "ERROR";
							$response["text"] = "Ошибка базы данных!";
						}
				} else {
					if(isset($_POST["confirmCode"])) {
						if($review->confirmCode == $_POST["confirmCode"]) {
							$review->statusId = 1;
							if($review->update()) {

								$amoCRM = new AmoCRM();
								$amoCRM->setRew($review);

								$response["status"] = "OK";
								$response["text"] = "Отзыв подтверждён!";
								
								$userModel = User::model()->findByAttributes(array('telefon' => $review->userPhone));
								if($userModel) {
									if($review->userId !== $userModel->id) {
										$review->userId = $userModel->id;
										$review->update();
									}
									if(self::login($userModel->telefon, $userModel->password)) {
										$response["autologin"] = true;
									}
								}
							} else {
								$response["status"] = "ERROR";
								$response["text"] = "Ошибка базы данных!";
							}
						} else {
							$response["status"] = "ERROR";
							$response["text"] = "Код не верный";
						}
					} else {
							$response["status"] = "ERROR";
							$response["text"] = "Запрос не верный";
						}
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "";
			}
		} else {
				$response["status"] = "ERROR";
				$response["text"] = "";
			}
		echo json_encode($response);
	}
	
	public function actionShowReviews() {
		if (Yii::app()->request->isAjaxRequest) {
			$attributes = Yii::app()->request->getParam('attributes');
			if(!is_array($attributes)) $attributes = [];
			$reviews = Review::model()->findAllByAttributes(array_merge($attributes, ['statusId' => 2]), "reviewText != '' AND reviewText IS NOT NULL");
			$this->renderPartial('//layouts/_showReviews', array(
				'reviews' => $reviews,
			));
		} else {
			$this->render('//layouts/_showReviews', array(
				'reviews' => $reviews,
			));
		}
	}
	
//	public function actionCities() {
//
//		$cities = City::model()->findAll();
//
//		$data = array();
//		foreach ($cities as $city)
//			$data[$city->id] = $city->name;
//
//		echo CJSON::encode($data);
//	}
	
	public function actionReportDoctor() {
		$dLink = Yii::app()->request->getParam('dLink');
		
		$doctor = Doctor::model()->findByLink($dLink);
		
		$mail = new YiiMailer();
		$mail->setView('reportDoctor');
		$mail->setData(array(
			'model' => $doctor
		));
		$mail->render();
		$mail->From = 'robot@emportal.ru';
		$mail->FromName = Yii::app()->name;
		$mail->Subject = "Пользователь не смог записаться на прием к врачу " . $doctor->name . ' в клинику '.$doctor->currentPlaceOfWork->company->name;
		
		if(Yii::app()->params['devMode'])
			$mail->AddAddress(Yii::app()->params['devEmail']);
		else
			$mail->AddAddress(Yii::app()->params['reportEmail']);
		
		if ($mail->Send()) {

		} else {

		}
		
		echo CJSON::encode(['success' => true]);
		Yii::app()->end();
	}
	
	public function actionReportClinic() {
		$cLink = Yii::app()->request->getParam('cLink');
		
		Doctor::$filterDeleted = false;
		Company::$showRemoved = true;
		Address::$showInactive = true;
		$clinic = Address::model()->findByLink($cLink);
		
		$mail = new YiiMailer();
		$mail->setView('reportClinic');
		$mail->setData(array(
			'model' => $clinic
		));
		$mail->render();
		$mail->From = 'robot@emportal.ru';
		$mail->FromName = Yii::app()->name;
		$mail->Subject = "Пользователь пожаловался на неактивность клиники " . $clinic->company->name;

		if(Yii::app()->params['devMode'])
			$mail->AddAddress(Yii::app()->params['devEmail']);
		else {
			$mail->AddAddress(Yii::app()->params['reportEmail']);
			$mailList = $clinic->getMailListForReminderToPay();
			if(is_array($mailList)) foreach ($mailList as $value) {
				$mail->AddAddress($value);
			}
		}
		
		if ($mail->Send()) {

		} else {

		}
		
		echo CJSON::encode(['success' => true]);
		Yii::app()->end();
	}
	
	public function actionCitiesByRegion($idRegions) {

		$cities = City::model()->findAllByAttributes(array());
	}

	public function actionServiceActivite() {

		//CompanyActivite::model()->findAll();

		$array = array();
		$criteria = new CDbCriteria();
		$criteria->order = 't.name';
		$criteria->with = [
			'addressServices' => [
				'together' => true,
				'select' => false,
				'joinType' => 'INNER JOIN'
			]
		];
		foreach (CompanyActivite::model()->findAll($criteria) as $group) {
			$array[$group->link] = $group->name;
		}

		$this->render('serviceActivite', array('groups' => $array));
	}

	public function actionServiceSectionByServiceActivite($link) {

		$array = array();

		$creteria = new CDbCriteria([
			'order' => '`t`.`name`',
			'with' => [
				'services' => [
					'select' => FALSE,
					'with' => [
						'addressServices' => [
							'together' => TRUE,
							'joinType' => 'INNER JOIN',
							'select' => FALSE,
						],
						'companyActivite' => [
							'select' => FALSE,
						]
					]
				],
			],
		]);

		$creteria->compare('companyActivite.link', $link);

		foreach (ServiceSection::model()->findAll($creteria) as $group) {
			$array[$group->link] = $group->name;
		}

		$this->render('serviceSectionByServiceActivite', array('groups' => $array));
	}

	public function actionServiceByServiceActivite($link) {
		if (!$link) {
			//Yii::app()->end();
		}
		$array = array();

		$creteria = new CDbCriteria(array(
			'order' => '`t`.`name`',
		));

		$creteria->compare('`t`.`companyActiviteId`', $link);
		if (Yii::app()->request->getParam('term')) {
			$creteria->compare('t.name', Yii::app()->request->getParam('term'), true);
		}

		foreach (Service::model()->findAll($creteria) as $group) {
			$array[] = array(
				'id' => $group->id,
				'value' => $group->name
			);
		}


		if (Yii::app()->request->getParam('json')) {
			header('Content-Type: application/json');
			echo CJSON::encode($array);
		}
	}

	public function actionServiceByServiceSection($link) {

		$array = array();

		$criteria = new CDbCriteria(array(
			'order' => '`t`.`name`',
			'with' => array(
				'addressServices' => [
					'joinType' => 'INNER JOIN',
					'select' => FALSE,
					'together' => TRUE
				],
				'serviceSection' => [
					'select' => FALSE,
				],
			)
		));

		$criteria->compare('serviceSection.link', $link);

		foreach (Service::model()->findAll($criteria) as $group) {
			$array[$group->link] = $group->name;
		}

		$this->render('serviceByServiceSection', array('groups' => $array));
	}

	public function actionServiceByClinic() {
		$array = array();

		$criteria = new CDbCriteria(array(
			'order' => '`service`.`name`',
			'with' => array(
				'address' => array(
					'select' => FALSE,
				),
				'service'
			)
		));

		$criteria->compare('address.link', Yii::app()->request->getParam('clinicLink'));
		$criteria->compare('service.name', Yii::app()->request->getParam('term'), true);

		foreach (AddressServices::model()->findAll($criteria) as $group) {
			$array[] = array(
				'id' => $group->service->link,
				'value' => $group->service->name
			);
		}
		if (!$array) {
			$srv = Service::model()->findByLink("first");
			$array[] = [
				'id' => $srv->link,
				'value' => $srv->name
			];
		}
		echo CJSON::encode($array);
	}

	public function actionDoctorByClinicAndService() {
		$array = array();

		$creteria = new CDbCriteria(array(
			'order' => '`t`.`name`',
			'with' => array(
				'doctorServices',
				'placeOfWorks' => array(
					'condition' => 'placeOfWorks.current = 1 OR placeOfWorks.current = 2',
					'select' => FALSE,
					'with' => array(
						'address'
					),
				),
				'specialties',
			)
		));

		$service = Service::model()->with('companyActivite.doctorSpecialties')->findByLink(Yii::app()->request->getParam('serviceLink'));
		/* @var $service Service */

		$ids = array();
		if ($service && $service->link != 'first')
			foreach ($service->companyActivite->doctorSpecialties as $specialty) {
				$ids[] = $specialty->id;
			}

		$creteria->compare('address.link', Yii::app()->request->getParam('clinicLink'));

		if ($service->link != 'first') {
			$creteria->addInCondition('specialties.id', $ids);
		}

		$creteria->compare('t.name', Yii::app()->request->getParam('term'), true);

		$array = array();
		foreach (Doctor::model()->findAll($creteria) as $group) {
			$array[] = array(
				'id' => $group->link,
				'value' => $group->name,
			);
		}

		echo CJSON::encode($array);
	}

	public function actionDoctorSpecialties($term) {

		$criteria = new CDbCriteria();
		
		$criteria->select = 't.id, t.name';
		$criteria->compare('t.name', $term, true);
        $criteria->compare('t.isArchive', '0', false);
		$criteria->order = 't.name';
		
		$SScriteria = new CDbCriteria();
		$SScriteria->group = 't.id';
		$SScriteria->compare('samozapis',0);
		$SSmodel = ShortSpecialtyOfDoctor::model()->findAll($SScriteria);
		$ids = [];
		foreach ($SSmodel as $value) {
			$ids[] = $value->id;
		}
		$SScriteria = new CDbCriteria();
		$SScriteria->select = 't.specialtyId';
		$SScriteria->addNotInCondition('t.specialtyId', $ids);
		$SSmodel = ExtSpeciality::model()->findAll($SScriteria);
		$ids = [];
		foreach ($SSmodel as $value) {
			$ids[] = $value->specialtyId;
		}
		$criteria->addNotInCondition('t.id', $ids);
			
		$array = array();
		foreach (DoctorSpecialty::model()->findAll($criteria) as $group) {
			$array[] = array(
				'id' => $group->id,
				'value' => $group->name
			);
		}

		echo CJSON::encode($array);
	}

	public function actionMedicalSchools($term) {

		$criteria = new CDbCriteria();

		$criteria->select = 'id, name';
		$criteria->compare('name', $term, true);
		$criteria->order = 'name';


		$array = array();
		foreach (MedicalSchool::model()->findAll($criteria) as $group) {
			$array[] = array(
				'id' => $group->id,
				'value' => $group->name
			);
		}

		echo CJSON::encode($array);
	}

	public function actionJson($link) {
		Yii::import('ext.yii-json-dataprovider.JSonActiveDataProvider');

		//header('Content-Type: application/json');

		$criteria = new CDbCriteria();
		$criteria->compare('`t`.`link`', $link);
		$criteria->with = array(
		);
		//same syntax like CActiveDataProvider
		$dataProvider = new JSonActiveDataProvider('Address', array('attributes' => array('id', 'name'),
			'relations' => array(
				'doctors' => array(
					'attributes' => array('link', 'name')
				)
			),
			'criteria' => $criteria,
		));
		print_r($dataProvider->getJsonData());
		//$myModels = new Doctor();
		//$array = ModelToArrayConverter::instance($myModels->findAll(), array('id', 'name', 'specialties'))->convert();
		//print_r($array);
		Yii::app()->end();
	}

	public function actionGetDatesForCalendar() {
		//$this->layout="empty";	
		$date = Yii::app()->getRequest()->getParam('date');
		$type = Yii::app()->getRequest()->getParam('type');
		$link = Yii::app()->getRequest()->getParam('link');
		$doctor = Yii::app()->getRequest()->getParam('doctor');
		$bck = Yii::app()->getRequest()->getParam('backend');
		$backend = !empty($bck);

		$timeBlock = WorkingHour::getDatesForCal($link, $date, $type, $doctor, $backend);

		Yii::app()->clientScript->reset();
		$arr['html'] = trim($this->render('//clinic/_calendarBlock', array(
					'time' => $timeBlock,
					'backend' => $backend,
						), true));
		echo CJSON::encode($arr);
	}

	public function actionGetWorkingHoursByDate($link = null, $date = null) {
		
		header("Access-Control-Allow-Origin: *");
		$json = Yii::app()->request->getParam("JSON");
		$doctor = Yii::app()->request->getParam("doctor");
		$type = Yii::app()->request->getParam("type");
		$new = Yii::app()->request->getParam('new');
		$idPat = (Yii::app()->request->getParam('idPat')) ? Yii::app()->request->getParam('idPat') : '';

		if ($link && $date) {
			$checkDate = preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $date);

			if ($checkDate) {
				switch ($type) {
					case 'forward':
						$date = date('Y-m-d', strtotime($date . '+1 days'));
						break;
					case 'back':
						$date = date('Y-m-d', strtotime($date . '-1 days'));
						break;
					default:
						break;
				}

				$time = WorkingHour::getDate($link, $date, $doctor, false, $idPat);

				if ($json) {
					echo CJSON::encode($time);
				} else {
					Yii::app()->clientScript->reset();
					$arr['html'] = trim($this->render('//clinic/' . ($new ? '_newCalendarBlock' : '_calendarBlock'), array(
								'time' => array($time),
								'short' => true
									), true));
					echo CJSON::encode($arr);
				}
				Yii::app()->end();
			} else {
				$error['error']['date'] = 'Введите date в формате YYYY-MM-DD';
			}
		} else {
			if (!$link) {
				$error['error']['link'] = 'Введите link';
			}
			if (!$date) {
				$error['error']['date'] = 'Введите date в формате YYYY-MM-DD';
			}
		}

		echo CJSON::encode($error);
		Yii::app()->end();
	}

	public function actionGetDateByAppointmentClinic($date = null) {
		
		$type = Yii::app()->request->getParam("type");

		if ($date) {
			$checkDate = preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $date);

			if ($checkDate) {
				switch ($type) {
					case 'forward':
						$date = date('Y-m-d', strtotime($date . '+1 days'));
						break;
					case 'back':
						$date = date('Y-m-d', strtotime($date . '-1 days'));
						break;
					default:
						break;
				}

				$weekArr = array(0=>"Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб");
				$dateArr = explode('.', date('Y.m.d', strtotime($date)));
				$arr['dateHtml'] = $weekArr[date('w', mktime(0, 0, 0, $dateArr[1], $dateArr[0], $dateArr[2]))].'. '.$dateArr[2].' '.MyTools::getMonth((int)$dateArr[1]).' '.$dateArr[0];
				$arr['date'] = $date;

				echo CJSON::encode($arr);
				Yii::app()->end();
			}
		}

	}

	/**
	 * Если совпадение точное - возвращаем 1, иначе 0
	 * @param string $name Наименование услуги
	 */
	public function actionGetServiceByName($name) {
		$model = Service::model()->find('name = :name', array(":name" => $name));
		if ($model)
			echo 1;
		else
			echo 0;
	}

	public function actionGetPriceByServiceAndDoctor($serviceId, $doctorId) {
		$service = $serviceId;
		$doctor = $doctorId;

		$doctor = Doctor::model()->with(array(
					'doctorServices' => array(
						'together' => true,
						'with' => array(
							'cassifierService' => array(
								'together' => true
							)
						)
			)))->find('cassifierService.link=:sId AND t.link=:dId', array(
			':sId' => $service,
			':dId' => $doctor
		));
		if ($doctor->id) {
			echo $doctor->doctorServices[0]->price;
		} else {
			$doctorsClinic = Doctor::model()->find('link=:dId', array(
				':dId' => $doctorId
			));
			$addressId = $doctorsClinic->currentPlaceOfWork->addressId;
			$serviceId = Service::model()->find('link=:sId', array(
				":sId" => $service
			));
			$servicePrice = AddressServices::model()->find('serviceId=:sId AND addressId=:aId', array(
				':aId' => $addressId,
				':sId' => $serviceId->id
			));
			echo $servicePrice->price;
		}

		Yii::app()->end();
	}

	public function actionGetFormData() {
		$cityId = Yii::app()->db->createCommand()
				->select('id')
				->from(City::model()->tableName())
				->where("name = 'Санкт-Петербург'")
				->queryScalar();

		/* $cityId=1; */
		$metroStation = Yii::app()->db->createCommand()
				->select('id,name')
				->from(MetroStation::model()->tableName())
				->order('name ASC')
				->where("cityId = :cityId", array('cityId' => $cityId))
				->queryAll();

		$doctorSpecialty = Yii::app()->db->createCommand()
				->select('id,name')
				->from(ShortSpecialtyOfDoctor::model()->tableName())
				->order('name ASC')->queryAll();

		$data = array(
			array(
				'id' => 'text',
				'name' => 'Поисковый запрос',
				'type' => 'text',
				'data' => null,
			),
			array(
				'id' => 'metroStationId',
				'name' => 'Станция метро',
				'type' => 'list',
				'data' => $metroStation,
			),
			array(
				'id' => 'doctorSpecialtyId',
				'name' => 'Специальность врача',
				'type' => 'list',
				'data' => $doctorSpecialty,
			),
			array(
				'id' => 'price',
				'name' => 'Стоимость',
				'type' => 'range',
				'data' => array('min' => 500, 'max' => '10000', 'step' => '500'),
			),
			array(
				'id' => 'datetime',
				'name' => 'Желательное время',
				'type' => 'datetime',
				'data' => null,
			),
			array(
				'id' => 'onHouse',
				'name' => 'Выезд врача на дом',
				'type' => 'switch',
				'data' => array(0 => 'нет', 1 => 'да'),
			),
		);


		echo json_encode($data);
	}

	public function actionGenerateCodeForPhone($phone) {
		$phone = "+" . MyRegExp::phoneRegExp($phone);
		$model = User::model();
		$model->telefon = $phone;
		if (Yii::app()->user->isGuest) {
			if (User::model()->find("telefon=:phone", array('phone' => $phone))) {
				echo "error_phoneIsBusy";
				return;
			}
		}
		if ($model->validate()) {
			$pv = PhoneVerification::model()->find("phone=:phone AND status IS NULL OR status=" . PhoneVerification::READY_TO_REGISTER, array(
				':phone' => $phone,
			));

			if (empty($pv)) {
				$smsgate = new SmsGate();
				$code = $smsgate->generateCode();
				$smsgate->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
					'_code_' => $smsgate->code
				)); // = SmsGate::VERIFICATION;				
				$smsgate->phone = $phone;
				$smsgate->send();

				//$smsgate->test();

				$pvModel = new PhoneVerification();
				$pvModel->code = $code;
				$pvModel->phone = $phone;
				if ($pvModel->save()) {
					echo "true";
					Yii::app()->end();
				}
			} elseif (time() - $pv->timestamp < 3600 * 2) {
				echo "already_sent";
				Yii::app()->end();
			} else {
				$pv->delete();

				$smsgate = new SmsGate();
				$code = $smsgate->generateCode();
				$smsgate->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
					'_code_' => $smsgate->code
				)); //SmsGate::VERIFICATION;
				$smsgate->phone = $phone;
				$smsgate->send();
				//$smsgate->test();

				$pvModel = new PhoneVerification();
				$pvModel->code = $code;
				$pvModel->phone = $phone;
				if ($pvModel->save()) {
					echo "true";
					Yii::app()->end();
				}
			}
		}
		echo "false";
	}

	public function actionActivatePhone($phone, $code) {
		$phone = "+" . MyRegExp::phoneRegExp($phone);
		$model = User::model();
		$model->telefon = $phone;
		$code = $code * 1;

		if ($model->validate()) {
			$pvModel = PhoneVerification::model()->find("phone = :phone AND code=:code", array(
				':phone' => $phone,
				':code' => $code
			));
			if ($pvModel) {
				$pvModel->status = PhoneVerification::READY_TO_REGISTER;
				$pvModel->save();
				echo "valid";
				Yii::app()->end();
			} else {
				echo "invalid";
			}
		}
	}

	public function actionChangePhone() {
		$phone = Yii::app()->request->getParam('phone');
		$code = Yii::app()->request->getParam('code');

		//Если кода нет - просто выполняем отсылку кода пользователю
		if (strlen($phone) < 10) {
			if (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(['success' => false, 'comment' => 'Слишком короткий номер']);
				Yii::app()->end();
			}
			Yii::app()->end();
		}
		$user = Yii::app()->user->model;

		$phone = "+" . MyRegExp::phoneRegExp($phone);

		$existUser = User::model()->find("telefon = :phone", [
			'phone' => $phone
		]);
		if ($existUser) {
			if ($existUser->id == $user->id) {
				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(['success' => false, 'comment' => 'Этот номер уже является вашим']);
					Yii::app()->end();
				}
			} else {
				if (isset($_REQUEST['JSON'])) {

					echo CJSON::encode(['success' => false, 'comment' => 'Данный номер занят']);
					Yii::app()->end();
				}
			}
		} else {

			if (!$code) {
				$pv = PhoneVerification::model()->find("phone=:phone AND status=:status", array(
					'phone' => $phone,
					'status' => PhoneVerification::CHANGEPHONE
				));
				if (empty($pv)) {
					$smsgate = new SmsGate();
					$code = $smsgate->generateCode();
					$smsgate->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
						'_code_' => $smsgate->code
					)); //SmsGate::VERIFICATION;				
					$smsgate->phone = $phone;
					$smsgate->send();


					$pvModel = new PhoneVerification();
					$pvModel->code = $code;
					$pvModel->phone = $phone;
					$pvModel->userId = $user->id;
					$pvModel->status = PhoneVerification::CHANGEPHONE;
					if ($pvModel->save()) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'sendCode' => true, 'comment' => 'Введите код присланный на указанный вами номер']);
							Yii::app()->end();
						}
						echo "true";
						Yii::app()->end();
					}
				} elseif (time() - $pv->timestamp < Yii::app()->params['SendCodeDelay']) {
					if (isset($_REQUEST['JSON'])) {
						echo CJSON::encode(['success' => true, 'sendCode' => true, 'comment' => 'Введите код присланный ранее на указанный вами номер']);
						Yii::app()->end();
					}
					echo "already_sent";
					Yii::app()->end();
				} else {
					$pv->delete();

					$smsgate = new SmsGate();
					$code = $smsgate->generateCode();
					$smsgate->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
						'_code_' => $smsgate->code
					)); //SmsGate::VERIFICATION;
					$smsgate->phone = $phone;
					$smsgate->send();
					//$smsgate->test();

					$pvModel = new PhoneVerification();
					$pvModel->code = $code;
					$pvModel->phone = $phone;
					$pvModel->userId = $user->id;
					$pvModel->status = PhoneVerification::CHANGEPHONE;
					if ($pvModel->save()) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'sendCode' => true, 'comment' => 'Введите код присланный на указанный вами номер']);
							Yii::app()->end();
						}
						echo "true";
						Yii::app()->end();
					}
				}
				//Если code прислан - пытаемся поменять телефон
			} else {
				$pv = PhoneVerification::model()->find("phone=:phone AND userId=:userId AND status=:status AND code=:code", array(
					'phone' => $phone,
					'status' => PhoneVerification::CHANGEPHONE,
					'code' => $code,
					'userId' => $user->id
				));
				if ($pv) {
					$user->telefon = $phone;
					$user->phoneActivationDate = new CDbExpression("NOW()");
					if ($user->save()) {
						$pv->delete();
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'comment' => 'Номер успешно изменен']);
							Yii::app()->end();
						}
					}
					echo "ok";
				}
			}
		}
	}

	public function actionRemovePhone($phone) {
		$pv = PhoneVerification::model()->find("phone=:phone AND userId=:userId", array(
			'phone' => $phone,
			'userId' => Yii::app()->user->model->id
		));
		if ($pv) {
			if ($pv->delete()) {
				echo "ok";
			}
		}
	}

	public function actionChangePhoneForAppointment($phone, $code = "") {
		//Если кода нет - просто выполняем отсылку кода пользователю
		if (mb_strlen(MyRegExp::phoneRegExp($phone), 'UTF-8') > 9) {
			$phone = "+" . MyRegExp::phoneRegExp($phone);
			$user = Yii::app()->user->model;
			if (!$code) {
				if (Yii::app()->user->isGuest) {
					/* if (User::model()->find("telefon=:phone", array('phone' => $phone))) {
					  echo "error_phoneIsBusy";
					  return;
					  } */
				}
				$pv = PhoneVerification::model()->find("phone=:phone AND (status=:status OR status=:status2) ", array(
					'phone' => $phone,
					'status' => PhoneVerification::READY_TO_APPOINTMENT,
					'status2' => PhoneVerification::APPOINTMENT
				));
				if (empty($pv)) {
					
					if (!isset($_REQUEST['JSON'])) {
						//если новый юзер-телефон, то не шлем смс, а сразу аппрувим
						if (empty($user->telefon) && !User::userWithThisPhoneExists($phone)) {
						
							$pv = new PhoneVerification();
							$pv->code = 'auto';
							$pv->phone = $phone;
							$pv->userId = $user->id;
							$pv->status = PhoneVerification::READY_TO_APPOINTMENT;
							//$pv->status = PhoneVerification::APPOINTMENT; //!! reusable
							$pv->save();
							Yii::app()->session['appointment_phone_set'] = $pv->phone;
							if (!isset(Yii::app()->session['auto_verification_tries'])) {
								Yii::app()->session['auto_verification_tries'] = 10; //больше 10-и раз не даст записываться гостю на разные номера
							}
							echo "auto";
							Yii::app()->end();
						}
					}
					
					$smsgate = new SmsGate();
					$code = $smsgate->generateCode();
					$smsgate->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
						'_code_' => $smsgate->code
					)); //SmsGate::VERIFICATION;
					$smsgate->phone = $phone;
					$smsgate->send();


					$pvModel = new PhoneVerification();
					$pvModel->code = $code;
					$pvModel->phone = $phone;
					$pvModel->userId = $user->id;
					$pvModel->status = PhoneVerification::READY_TO_APPOINTMENT;
					if ($pvModel->save()) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['SENT' => true, 'msg' => 'Код подтверждения отправлен']);
						} else {
							echo "true;" . Yii::app()->params['SendCodeDelay'];
						}
						Yii::app()->end();
					}
				} elseif (time() - $pv->timestamp < Yii::app()->params['SendCodeDelay']) {
					if ($pv->status == 5) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['ALREADY_SENT_READY' => true, 'msg' => 'Код уже высылался']);
						} else {
							echo "already_sent_ready;" . (Yii::app()->params['SendCodeDelay'] - (time() - $pv->timestamp));
						}
					} else {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['ALREADY_SENT' => true, 'msg' => 'Код уже высылался']);
						} else {
							echo "already_sent;" . (Yii::app()->params['SendCodeDelay'] - (time() - $pv->timestamp));
						}
					}
					Yii::app()->end();
				} else {
					$pv->delete();

					$smsgate = new SmsGate();
					$code = $smsgate->generateCode();
					$smsgate->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
						'_code_' => $smsgate->code
					)); //SmsGate::VERIFICATION;
					$smsgate->phone = $phone;
					$smsgate->send();
					//$smsgate->test();

					$pvModel = new PhoneVerification();
					$pvModel->code = $code;
					$pvModel->phone = $phone;
					$pvModel->userId = $user->id;
					$pvModel->status = PhoneVerification::READY_TO_APPOINTMENT;
					if ($pvModel->save()) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['SENT' => true, 'msg' => 'Код подтверждения отправлен']);
						} else {
							echo "true";
						}
						Yii::app()->end();
					}
				}
				//Если code прислан - пытаемся поменять телефон
			} else {
				$criteria = new CDbCriteria();
				$criteria->compare("phone", $phone);
				$criteria->compare("status", PhoneVerification::READY_TO_APPOINTMENT);
				Yii::app()->session['auto_verification_tries'] = 10; //rm !! закомментить это потом!
				if ($code == 'iddqd' AND isset(Yii::app()->session['auto_verification_tries']) AND Yii::app()->session['auto_verification_tries'] != 'none') {
					
					Yii::app()->session['auto_verification_tries'] -= 1;
					if (Yii::app()->session['auto_verification_tries'] == 0) Yii::app()->session['auto_verification_tries'] = 'none';
				} else {
					$criteria->compare("code", $code);
				}
				
				if (!Yii::app()->user->isGuest) {
					$criteria->compare("userId", $user->id);
				}

				$pv = PhoneVerification::model()->find($criteria);

				if ($pv) {
					$pv->status = PhoneVerification::APPOINTMENT; //!! reusable
					$pv->save();
					/* Здесь записываем телефон в сессию */
					Yii::app()->session['appointment_phone_set'] = $pv->phone;
					#$user->telefon = $phone;
					#$user->phoneActivationDate = new CDbExpression("NOW()");
					#if($user->save()) {
					#	$pv->delete();
					#}
					if (isset($_REQUEST['JSON'])) {
						echo CJSON::encode(['CODE_ACCEPTED' => true, 'msg' => 'Код подтвержден']);
					} else {
						echo "ok";
					}
				} else {
					if (isset($_REQUEST['JSON'])) {
						echo CJSON::encode(['NOT_FOUND' => true, 'msg' => 'Неверный код']);
					} else {
						echo "not_found";
					}
				}
			}
			/* else {

			  $criteria = new CDbCriteria();
			  $criteria->compare("phone", $phone);
			  $criteria->compare("status", PhoneVerification::APPOINTMENT);
			  $criteria->compare("code", $code);
			  if(!Yii::app()->user->isGuest) {
			  $criteria->compare("userId", $user->id);
			  }
			  $pv=PhoneVerification::model()->find($criteria);
			  if($pv) {
			  echo "ok";
			  }
			  } */
		}
	}

	/* sends sms code when reactivating phone */

	public function actionReactivatePhone($type, $phone = null) {

		if (!Yii::app()->user->isGuest) {
			if (User::model()->find("telefon=:phone AND id<>:userId", array('phone' => $phone, 'userId' => Yii::app()->user->model->id))) {
				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(['success' => false, 'comment' => 'Данный номер занят']);
					Yii::app()->end();
				}
				echo "busy";
				return;
			}

			switch ($type) {
				case "yes":
					$status = PhoneVerification::PHONE_EXTEND_LIFETIME;
					$status2 = PhoneVerification::CHANGE_PHONE_EXTEND_LIFETIME;
					break;
				case "no":
					$status = PhoneVerification::CHANGE_PHONE_EXTEND_LIFETIME;
					$status2 = PhoneVerification::PHONE_EXTEND_LIFETIME;
					break;
				default:
					return;
					break;
			}



			$user = Yii::app()->user->model;


			$model = PhoneVerification::model()->find("(status=:status OR status=:status2) AND userId=:userId", array(
				':status' => $status,
				':status2' => $status2,
				':userId' => $user->id
			));

			if (!$phone) {
				$phone = $user->telefon;

				if ($model->phone != $phone) {
					if ($model) {
						$model->delete();
						unset($model);
					}
				}
			} else {
				$phone = "+" . MyRegExp::phoneRegExp($phone);
				if ($user->telefon == $phone && $user->telefon != $model->phone) {
					if ($model) {
						$model->delete();
						unset($model);
					}

					$status = PhoneVerification::PHONE_EXTEND_LIFETIME;
					$status2 = PhoneVerification::CHANGE_PHONE_EXTEND_LIFETIME;
				}
			}


			/* Если код не высылался */
			if (!count($model)) {
				$sms = new SmsGate();

				$sms->phone = $user->telefon;
				$code = $sms->generateCode();
				$sms->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
					'_code_' => $sms->code
				));

				if ($sendsms = $sms->send()) {
					PhoneVerification::model()->deleteAll('userId = :userId AND (status=:status OR status=:status2)', array(
						'userId' => $user->id,
						'status' => $status,
						'status2' => $status2
					));
					$pv = new PhoneVerification();
					$pv->code = $code;
					$pv->status = $status;
					$pv->userId = $user->id;
					$pv->phone = $phone;

					if ($pv->save()) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации отправлено']);
							Yii::app()->end();
						}
						echo "true";
						Yii::app()->end();
					}
				}
				//если код высылался только что
			} elseif (time() - $model->timestamp < Yii::app()->params['SendCodeDelay']) {
				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации уже отправлено']);
					Yii::app()->end();
				}
				if ($model->phone != $phone) {
					echo 'delay';
				} else {
					echo "already_sent";
				}
				Yii::app()->end();
				//если код высылался но давно
			} else {

				$model->delete();
				$sms = new SmsGate();

				$sms->phone = $phone;
				$code = $sms->generateCode();
				$sms->setMessage('Для продолжения записи на прием и авторизации введите код подтверждения: _code_.', array(
					'_code_' => $sms->code
				));

				if ($sendsms = $sms->send()) {
					PhoneVerification::model()->deleteAll('userId = :userId AND (status=:status OR status=:status2)', array(
						'userId' => $user->id,
						'status' => $status,
						'status2' => $status2
					));
					$pv = new PhoneVerification();
					$pv->code = $code;
					$pv->status = $status;
					$pv->phone = $phone;
					$pv->userId = $user->id;

					if ($pv->save()) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации отправлено']);
							Yii::app()->end();
						}
						echo "true_new";
						Yii::app()->end();
					}
				}
			}
		}
	}

	public function actionGetCompanyActivitesByCompanyType() {
		$companyTypeId = Yii::app()->request->getParam('companyTypeId');
		if ($companyTypeId) {
			$criteria = new CDbCriteria();
			$criteria->compare('t.companyTypeId', $companyTypeId);
			$criteria->compare('company.companyTypeId', $companyTypeId);
			$criteria->order = '`companyActivite`.`name`';
			$criteria->with = [
				'companyActivite' => [
					'together' => true,
					'with' => [
						'addressServices' => [
							'select' => false,
							'together' => true,
							'joinType' => 'INNER JOIN',
							'with' => [
								'address' => [
									'select' => false,
									'together' => true,
									'with' => [
										'company' => [
											'select' => false,
											'together' => true,
										]
									]
								]
							]
						]
					]
				]
			];
			$activites = CompanyTypeActivitesId::model()->findAll($criteria);
			$result = array();
			foreach ($activites as $act) {
				$result[] = array('id' => $act->companyActivite->id, 'name' => $act->companyActivite->name);
			}
		} else {
			$criteria = new CDbCriteria();
			$criteria->order = 'name';
			$criteria->with = [
				'addressServices' => [
					'together' => true,
					'joinType' => 'INNER JOIN'
				]
			];
			$result = CompanyActivite::model()->findAll($criteria);
		}

		/* echo "<pre>";
		  var_dump($result);
		  echo "</pre>"; */
		echo CJSON::encode($result);
	}

	public function actionGetPricesByActivity() {
		$addressLink = Yii::app()->request->getParam('link');
		$actLink = Yii::app()->request->getParam('activity');
		if (!$addressLink || !$actLink) {
			Yii::app()->end();
		}

		$activity = CompanyActivite::model()->findByAttributes(array('link' => $actLink));
		$criteria = new CDbCriteria();

		$criteria->with = array(
			'service',
			'address'
		);
		$criteria->compare('service.companyActiviteId', $activity->id);
		$criteria->compare('address.link', $addressLink);

		$prices = AddressServices::model()->findAll($criteria);
		$model = Address::model()->findByLink($addressLink);
		$this->renderPartial('getPricesByActivity', [
			'activity' => $activity,
			'model' => $model,
			'prices' => $prices,
		]);
	}

	public function actionUploadBanner() {

		$name = Yii::app()->request->getParam('filename');
		$username = Yii::app()->request->getParam('username');
		$password = Yii::app()->request->getParam('password');
		/* $file = Yii::app()->request->getParam('file'); */
		if (!empty($_REQUEST)) {
			MyTools::setMessage(print_r($_REQUEST, true) . print_r($_POST, true) . print_r($_GET, true) . print_r($_FILES, true));
		}

		/* Ищем название в базе */
		$model = CurrentBanners::model()->findByAttributes([
			'name' => $name
		]);
		if ($model) {
			$model->scenario = 'uploadBanner';
			$model->username = $username;
			$model->password = $password;

			$model->file = CUploadedFile::getInstanceByName('file');

			if ($model->validate()) {

				if ($model->file instanceof CUploadedFile) {
					if ($model->file->saveAs($model->filePath)) {
						echo "uploaded";
					}
				} else {
					MyTools::setMessage("file not uploaded");
					echo "file not uploaded";
				}
			} else {
				MyTools::setMessage(print_r($model->getErrors(), true));
				print_r($model->getErrors());
			}
		}
		/*
		  echo '<form method="POST" enctype="multipart/form-data">'
		  . '<input type="text" name="username" placeholder="username"/><br/>'
		  . '<input type="text" name="password" placeholder="password"/><br/>'
		  . '<input type="text" name="filename" placeholder="filename"/><br/>'
		  . '<input type="file" name="file" placeholder="file"/><br/>'
		  . '<input type="submit"/><br/>'
		  . '</form>'; */
	}

	/**
	 * Метод используется для выдачи докторов в контекстную рекламу
	 * @param array $doctorIds
	 */
	public function actionGetDoctors(array $doctorIds = NULL) {
		/*
		 * TODO:
		 * 1. Если у врача статус 0 , то при отдаче такого врача ставим activated,
		 *  как вариант сделать интерфейс где будут включаться только выбранные врачи
		 * + modified для нужных 
		 * переключаться с 4 на 2
		 */

		if ($doctorIds === NULL) {
			$criteria = new CDbCriteria();
			$criteria->together = true;
			$criteria->select = 'link,name,contextStatus';
			$criteria->limit = 3;
			$criteria->order = 't.rating DESC';
			$criteria->group = 't.name';
			$criteria->with = [
				'specialties' => [
					'together' => true
				]
			];
			$criteria->compare('t.contextStatus', '<>' . Doctor::CONTEXT_NONE);
			$doctors = Doctor::model()->findAll($criteria);
			$response = [];
			foreach ($doctors as $d) {
				$response[] = [
					'name' => $d->name,
					'link' => $d->link,
					'url' => 'http://emportal.ru/doctor/' . $d->link . '.html',
					'specialty' => $d->specialties[0]->name,
					'contextStatus' => $d->contextStatus,
					'contextUpdateTime' => $d->contextUpdateTime
				];
				if ($d->contextStatus == Doctor::CONTEXT_CREATE) {
					$d->contextStatus = Doctor::CONTEXT_DISABLED;
					$d->save();
				}
			}

			echo CJSON::encode(['response' => $response]);
		} else {
			$criteria = new CDbCriteria();
			$criteria->together = true;
			$criteria->select = 'link,name,contextStatus';
			$criteria->order = 't.rating DESC';
			$criteria->addInCondition('t.link', $doctorIds);
			$doctors = Doctor::model()->findAll($criteria);
			$response = [];
			foreach ($doctors as $d) {
				$response[] = [
					'name' => $d->name,
					'link' => $d->link,
					'url' => 'http://emportal.ru/doctor/' . $d->link . '.html',
					'specialty' => $d->specialties[0]->name,
					'contextStatus' => $d->contextStatus,
					'contextUpdateTime' => $d->contextUpdateTime
				];
				if ($d->contextStatus == Doctor::CONTEXT_CREATE) {
					$d->contextStatus = Doctor::CONTEXT_DISABLED;
					$d->save();
				}
			}

			echo CJSON::encode(['response' => $response]);
		}
	}

	public function actionGetDoctorPhoto() {
		if (isset($_REQUEST['JSON'])) {
			$link = Yii::app()->request->getParam('link');
			$height = Yii::app()->request->getParam('height');
			$height = $height ? $height : 100;
			$width = Yii::app()->request->getParam('width');
			$width = $width ? $width : 100;

			$criteria = new CDbCriteria();
			$criteria->compare('t.link', $link);
			$doctors = Doctor::model()->findAll($criteria);

			if (empty($doctors)) {
				echo CJSON::encode(['error' => 'Доктор не найден']);
				Yii::app()->end();
			}

			foreach ($doctors as $model) {
				#error_reporting(E_ALL);ini_set('display_errors',1);
				if (is_file($model->photoPath)) {
					$photoType = 'photo';
					#if(!$model->mobilePhoto) {					
					Yii::app()->image->load($model->photoPath)->resize($width, $height)->save($model->uploadPath . '/mobile.png');
					#}
					$photo = $model->mobilePhotoUrl;
				} elseif (mb_strtolower($model->sex->name, 'utf8') == 'женский') {/* женский */
					$photoType = 'female';
					$photo = $this->assetsImageUrl . '/mobile-staff-f.jpg';
					#if(!is_file(Yii::getPathOfAlias('shared.images').'/mobile-staff-f.jpg')) {					
					$photoUrl = Yii::getPathOfAlias('shared.images') . '/staff-f.jpg';
					Yii::app()->image->load($photoUrl)->resize($width, $height)->save(Yii::getPathOfAlias('shared.images') . '/mobile-staff-f.jpg');
					#}
				} else {/* мужской */
					$photoType = 'male';
					$photo = $this->assetsImageUrl . '/mobile-staff-m.jpg';
					#if(!is_file(Yii::getPathOfAlias('shared.images').'/mobile-staff-m.jpg')) {
					$photoUrl = Yii::getPathOfAlias('shared.images') . '/staff-m.jpg';
					Yii::app()->image->load($photoUrl)->resize($width, $height)->save(Yii::getPathOfAlias('shared.images') . '/mobile-staff-m.jpg');
					#}			
				}
				$data[$model->link] = ['mobilePhotoUrl' => $photo, 'photoType' => $photoType];
			}
			echo CJSON::encode($data);
		}
	}

	public function actionGetCurrentSearchUrl() {
		$type = Yii::app()->request->getParam('type');
		$data = Yii::app()->request->getParam('data',[]);
		$shortSearch = Yii::app()->request->getParam('shortSearch');
		$isVkApp = Yii::app()->request->getParam('vk');
		echo MyTools::GetSearchUrl($type,$data,$shortSearch,$isVkApp);
	}
	
	public function actionAddPoll() {
		$pollId = Yii::app()->request->getParam('pollId');
		$pollOptionId = Yii::app()->request->getParam('pollOptionId');
		
		$poll = Poll::model()->findByPk($pollId);
		$pollOption = PollOption::model()->findByPk($pollOptionId);
			
		if($poll && $pollOption) {
			$pa = new PollAnswer();
			$pa->pollId = $poll->id;
			$pa->pollOptionId = $pollOption->id;
			if($pa->save()) {
				echo CJSON::encode(['success' => true]);
			} else {
				
			}
		}
	}
	
	public function actionGetSearchParameters() {
		
		$speciality = Yii::app()->request->getParam('speciality');	
		$service = Yii::app()->request->getParam('service');	

		$model = new stdClass();
		$model->{"Parameters"} = json_encode(["Metro" => [], "District" => [], "Companies" => []]);
		
		if($speciality) {
			$criteria = new CDbCriteria;			
			$criteria->compare("linkUrl", $speciality);
			$criteria->compare("cityId", City::model()->selectedCityId);
			$criteria->compare("samozapis",(Yii::app()->params["samozapis"]) ? 1 : 0);
			$model = ShortSpecialtyOfDoctor::model()->find($criteria);
		}
		elseif($service) {
			$serviceModel = Service::model()->find("t.linkUrl = :linkUrl", array(":linkUrl" => $service));
			if(empty($serviceModel)) {
				$serviceModel = Service::model()->find("t.id = :id", array(":id" => $service));
			}
			if(!empty($serviceModel)) {
				$model->Parameters = json_encode(Search::GetSearchParametersOfService($serviceModel->linkUrl, City::model()->selectedCityId, Yii::app()->params["samozapis"]));
			}
		} else {
			$criteria = new CDbCriteria;
			$criteria->compare("cityId", City::model()->selectedCityId);
			$criteria->order = 'name ASC';
			
			$metroStations = MetroStation::model()->findAll($criteria);
			$Metro = [];
			foreach($metroStations as $value) {
				$Metro[] = 'metro-'.$value->linkUrl;
			}
			
			$cityDistricts = CityDistrict::model()->findAll($criteria);
			$District = [];
			foreach($cityDistricts as $value) {
				$District[] = 'raion-'.$value->linkUrl;
			}

			$criteria = new CDbCriteria;
			$criteria->compare("cityId", City::model()->selectedCityId);
			$criteria->compare("samozapis",(Yii::app()->params["samozapis"]) ? 1 : 0);
			$criteria->order = 'companyName ASC';
			
			$acrtiveCompanies = ShortCompaniesOfDoctor::model()->findAll($criteria);
			$Companies = [];
			foreach($acrtiveCompanies as $value) {
				$Companies[] = $value->companyLinkUrl;
			}
			
			$model->Parameters = json_encode(["Metro" => $Metro, "District" => $District, "Companies" => $Companies]);
		}
		
		echo $model->Parameters;
		Yii::app()->end();
	}
	
	
	public function actionValidateOMSData($link = null, $linkUrl = null, $companyLinkUrl = null, $addressLinkUrl = null) {
		
		$model = new OMSForm;
		$model->attributes = Yii::app()->request->getParam(get_class($model));
		//$model->attributes = $_POST['ajax'];
		
		if(isset($_POST['ajax'])) {
			if ($_POST['ajax']=='form') {
			
				//$model->performAjaxValidation();
				$arrData = CJSON::decode(CActiveForm::validate($model));
				echo CJSON::encode($arrData);
			}
			Yii::app()->end();
		}
	}
	
	
	public function actionCheckPatientOMS() {
			
		$idLpu = Yii::app()->request->getParam('idLpu');
		$patientData = Yii::app()->request->getParam('patientData');
		//if (!$idLpu) $idLpu = 1;
		//if (!$patientData) $patientData = ['Birthday' => '1972-11-29', 'Surname' => 'Король', 'Name' => 'Андрей', 'SecondName' => 'Максимович'];
		
		$patientData['Birthday'] .= 'T00:00:00';
		
		if(City::model()->getSelectedCityId() === City::MOSKVA) {
			$loader = new EmiasLoader();
		} else {
			$loader = new NetrikaLoader();
		}
		$checkResult = $loader->checkPatient($idLpu, $patientData);
		$patientId = (isset($checkResult->Success) && $checkResult->Success) ? $checkResult->IdPat : false;
		
		$output = [];
		if (!$patientId) {
			$output['result'] = false;
			$error = false;
			if(isset($checkResult->ErrorList)) {
				if(is_array($checkResult->ErrorList) && isset($checkResult->ErrorList[0])) {
					$error = $checkResult->ErrorList[0];
				}
				elseif (is_object($checkResult->ErrorList) && isset($checkResult->ErrorList->Error)) {
					$error = $checkResult->ErrorList->Error;
				}
			}
			if($error && isset($error->IdError) && isset($error->ErrorDescription)) {
				$output['message'] = MyTools::createFromTemplate("Ошибка %/Error/IdError%: %Error/ErrorDescription%. Для успешной записи следует явиться в закрепленное за вами медицинское учреждение и зарегистрироваться в очном порядке.",
						["%/Error/IdError%" => $error->IdError, "%Error/ErrorDescription%" => $error->ErrorDescription]);
			} else {
				$output['message'] = 'Запись в данную клинику невозможна. Рекомендуется проверить корректность данных пациента.';
			}
		} else {
			$output['result'] = true;
			$output['message'] = $patientId;
			
			$omsForm = new OMSForm;
			$patientData = Yii::app()->request->getParam('patientData');
			$omsForm->surName = trim($patientData['Surname']);
			$omsForm->firstName = trim($patientData['Name']);
			$omsForm->fatherName = trim($patientData['SecondName']);
			$omsForm->birthday = trim($patientData['Birthday']);
			$omsForm->omsSeries = trim($patientData['Polis_S']);
			$omsForm->omsNumber = trim($patientData['Polis_N']);
			$omsForm->saveOMSForm();
		}
		
		echo CJSON::encode($output);
		return true; //$output['result'];
	}

	public function actionGetAddressToGeocoder($format='json') {
		$criteria = new CDbCriteria;
		$criteria->group = 't.id';
		$criteria->compare('t.isActive', 1);
		//$criteria->compare('t.samozapis', 1);
		$criteria->compare('company.removalFlag', 0);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->with = [
				'company' => [
						'together' => true,
						'select' => false,
				],
				'userMedicals' => [
						'together' => true,
						'select' => false,
				],
		];
		$allAddresses = Address::model()->findAll($criteria);
		
		$response = [];
		foreach ($allAddresses as $address) {
			$response[]["linkUrl"] = $address->linkUrl;
		}
		if($format == 'php') {
			return $response;
		} elseif($format == 'json' || true) {
			echo CJSON::encode($response);
			return true;
		}
	}

	public function actionGetMetro($metroName, $cityName=NULL) {
		$response = MyTools::getMetro($metroName, $cityName);
		echo CJSON::encode($response);
		return true;
	}
	public function actionDefineGeocodesOfAddress($linkUrl=NULL,$format='json') {
		$GOOGLE_API_KEY = "AIzaSyC8CDQaHpEU3gNEGEbslV5dFYqN7Si_AtA";
		Address::$showInactive = true;
		$address = Address::model()->find([
				"condition" => "linkUrl = :linkUrl",
				"params" => [ ":linkUrl" => $linkUrl ]
		]);
		$response = [];
		if(!$address) {
			$response["status"] = "ERROR";
			$response["text"] = "Адрес не найден в базе данных!";
		} else {
			$strAddress = $address->city->name . ", " . $address->street;
			if(!empty($address->houseNumber)) $strAddress .= ", ".$address->houseNumber;
			$strAddress = trim($strAddress);
			
			if(empty($address->longitude) || empty($address->latitude)) {
				$APIlink = "https://maps.googleapis.com/maps/api/place/textsearch/json?key=".$GOOGLE_API_KEY."&location=60,30&radius=100000&query=".urlencode($strAddress);
				$response["APIlink"] = $APIlink;
				$json = file_get_contents($APIlink);
				if(!$json) {
					$response["status"] = "ERROR";
					$response["text"] = "geocoder: Нет ответа";
				} else {
					$APIResponse = json_decode($json);
					if(!$APIResponse) {
						$response["status"] = "ERROR";
						$response["text"] = "geocoder: Ответ не может быть преобразован";
					} else {
						if(!isset($APIResponse->status)) {
							$response["status"] = "ERROR";
							$response["text"] = "geocoder: Неизвестная ошибка!";
						} else {
							switch ($APIResponse->status) {
								case "ZERO_RESULTS":
									$response["status"] = "ERROR";
									$response["text"] = "geocoder: Геокодирование выполнено успешно, но не возвратило никаких результатов!";
								break;
								case "OVER_QUERY_LIMIT":
									$response["status"] = "ERROR";
									$response["text"] = "geocoder: Превышена квота!";
								break;
								case "REQUEST_DENIED":
									$response["status"] = "ERROR";
									$response["text"] = "geocoder: Запрос отклонен!";
								break;
								case "INVALID_REQUEST":
									$response["status"] = "ERROR";
									$response["text"] = "geocoder: Запрос отсутствует!";
								break;
								default:
									$response["status"] = "ERROR";
									$response["text"] = "geocoder: Неизвестная ошибка!";
								break;
								case "OK":
									if(isset($APIResponse->results[0]) && isset($APIResponse->results[0]->geometry) && isset($APIResponse->results[0]->geometry->location)) {
										$latitude = floatval($APIResponse->results[0]->geometry->location->lat);
										$longitude = floatval($APIResponse->results[0]->geometry->location->lng);
										$response["latitude"] = $latitude;
										$response["longitude"] = $longitude;
										$address->latitude = $latitude;
										$address->longitude = $longitude;
										if($address->update()) {
											$response["status"] = "OK";
											$response["text"] = "Координаты сохранены!";
										} else {
											$response["status"] = "ERROR";
											$response["text"] = "Не удалось сохранить координаты!";
										}
									} else {
										$response["status"] = "ERROR";
										$response["text"] = "geocoder: координаты не найдены!";
									}
								break;
							}
						}
					}
				}
			}
			if(!empty($address->longitude) && !empty($address->latitude)) {
				if(empty(trim($address->cityDistrictId))) {
					$district = MyTools::getDistrictFromYandex($address->longitude, $address->latitude, $address->cityId);
					if($district["status"] == "OK")  {
						$address->cityDistrictId = $district['result']['districtId'];
						if($address->update()) {
							$response["status"] = "OK";
							$response["text"] = "Координаты сохранены!";
						} else {
							$response["status"] = "ERROR";
							$response["text"] = "Не удалось сохранить координаты!";
						}
					}
				}
				$text = $strAddress . " :: latitude:".$address->latitude.", longitude:".$address->longitude;
				if(count($address->nearestMetroStations) < 1) {
					$nearestMetro = MyTools::getNearestMetroStationFromYandex($address->longitude, $address->latitude, $address->cityId);
					if($nearestMetro["status"] == "OK") {
						NearestMetroStation::model()->deleteAll('addressId = :addressId', [ 'addressId' => $address->id, ]);
						$address->saveMetroList($nearestMetro["list"]);
					}
					if(isset($nearestMetro["text"])) {
						$text .= ": ".$nearestMetro["text"];
					}
				}
				$response["status"] = "OK";
				$response["text"] = $text;
			}
		}
		if($format == 'php') {
			return $response;
		} elseif($format == 'json' || true) {
			echo CJSON::encode($response);
			return true;
		}
	}
	
	public function actionGetDoctor($format='json') {
		try {
			$attributes = Yii::app()->request->getParam('doctor');
			if(is_array($attributes) && count($attributes)>0) {
				$criteria = new CDbCriteria();
				$criteria->compare("t.deleted", 0);
				$criteria->compare("company.removalFlag", 0);
				$criteria->compare("address.isActive", 1);
				$criteria->compare('userMedicals.agreementNew', 1);
				$criteria->with = [
					'placeOfWorks' => [
						'together' => true,
						'with' => [
							'company' => [
								'together' => true,
							],
							'address' => [
								'together' => true,
								'with' => [
									'userMedicals' => [
										'together' => true,
									],
								],
							],
						],
					],
					'doctorServices' => [
						'joinType' => 'LEFT JOIN',
						'together' => true,
						'with' => [
							'cassifierService' => [
								'joinType' => 'LEFT JOIN',
								'together' => true,
							],
						],
					],
					'doctorEducations' => [
						'joinType' => 'LEFT JOIN',
						'together' => true,
						'with' => [
							'medicalSchool' => [
								'joinType' => 'LEFT JOIN',
								'together' => true,
							],
							'typeOfEducation' => [
								'joinType' => 'LEFT JOIN',
								'together' => true,
							],
						],
					],
					'scientificDegrees' => [
						'joinType' => 'LEFT JOIN',
						'together' => true,
					],
					'specialties' => [
						'joinType' => 'LEFT JOIN',
						'together' => true,
					],
					'reviews' => [
						'joinType' => 'LEFT JOIN',
						'together' => true,
					],
				];
				foreach ($attributes as $key=>$value) {
					if(strrpos($key, ".") === false) {
						$key = 't.'.$key;
					} 
					$criteria->compare($key,$value);
				}
				if($response["status"] !== "ERROR") {
					$modelDoctors = Doctor::model()->findAll($criteria);
					if(count($modelDoctors) > 0) {
						$response["status"] = "OK";
						$response["text"] = "Успех!";
						foreach ($modelDoctors as $index=>$modelDoctor) {
							$doctor[$index] = $modelDoctor->attributes;
							foreach ($modelDoctor->doctorEducations as $key=>$value) {
								$doctor[$index]["doctorEducations"][$key] = [
									"id" => $value->id,
									"name" => $value->medicalSchool->name,
									"type" => $value->typeOfEducation->name,
									"yearOfStady" => $value->yearOfStady,
								];
							}
							foreach ($modelDoctor->scientificDegrees as $key=>$value) {
								$doctor[$index]["scientificDegrees"][$key] = $value->attributes;
							}
							foreach ($modelDoctor->doctorServices as $key=>$value) {
								$doctor[$index]["doctorServices"][$key] = $value->attributes;
								$doctor[$index]["doctorServices"][$key]["name"] = $value->cassifierService->name;
							}
							foreach ($modelDoctor->placeOfWorks as $key=>$value) {
								$doctor[$index]["placeOfWorks"][$key] = [
									"company" => $value->company->attributes,
									"address" => $value->address->attributes,
									"current" => $value->current,
								];
							}
							foreach ($modelDoctor->specialties as $key=>$value) {
								$doctor[$index]["specialties"][$key] = $value->attributes;
							}
							foreach ($modelDoctor->reviews as $key=>$value) {
								if(intval($value->statusId) == 2) {
									$doctor[$index]["reviews"][$key] = $value->attributes;
								}
							}
							$response["doctor"] = $doctor;
						}
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Врачей с заданными параметрами не найдено!";
					}
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Запрос не верный!";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Неизвестная ошибка!";
		}
		switch ($format) {
			case 'xml':
				$xml = Array2XML::createXML('root', $response);
				header("Content-type: text/xml; charset=utf-8");
				echo $xml->saveXML() . "\n";
			break;
			case 'php':
				print_r($response);
			break;
			default:
				$json = CJSON::encode($response);
				header("Content-type: text/json; charset=utf-8");
				echo $json;
			break;
		}
		//print_r($response);
		return true;
	}
	
	public function actionAppForm($action = null)
	{
		header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET");
		header('Content-Type: application/json');
		
		$response = ['result' => 'failed', 'action' => $action];
		
		if ($action == 'getAddressList')
		{
			$companyLinkUrl = Yii::app()->request->getParam('companyLinkUrl');
			$company = Company::model()->findByLink($companyLinkUrl);			
			//$addresses = $company->addresses;
			$criteria = new CDbCriteria;
			$criteria->with = [
					'userMedicals' => [
							'together' => true,
					],
			];
			$criteria->compare('ownerId', $company->id);
			$criteria->compare('isActive', 1);
			$criteria->compare('agreementNew', 1);
			$addresses = Address::model()->findAll($criteria);
			$addressList = [];
			foreach($addresses as $address)
			{
				$addressList[] = [
					'url' => $address['linkUrl'],
					'name' => $address['street'] . ', ' . $address['houseNumber']
				];
			}
			$response = ['result' => 'ok', 'addressList' => $addressList];
		}
		
		if ($action == 'getDoctorList')
		{
			$companyLinkUrl = Yii::app()->request->getParam('companyLinkUrl');
			$addressLinkUrl = Yii::app()->request->getParam('addressLinkUrl');
			
			$criteria = new CDbCriteria;
			$criteria->with = [
				'placeOfWorks' => [
                    'select' => false,
                    'together' => true,
					'with' => [
						'company',
						'address'
					]
				],
			];
			$criteria->compare('company.linkUrl', $companyLinkUrl, false);
			$criteria->compare('address.linkUrl', $addressLinkUrl, false);
			$criteria->limit = 200;
			$doctors = Doctor::model()->findAll($criteria);
			$doctorList = [];
			foreach($doctors as $doctor)
			{
				$doctorListItem = [];
				$doctorListItem['id'] = $doctor->id;
				$doctorListItem['name'] = $doctor->name;
				$doctorListItem['photoUrl'] = $doctor->photo;
				$doctorListItem['doctorUrl'] = $doctor->linkUrl;
				$doctorListItem['experience'] = $doctor->getExperience();
				$doctorListItem['speciality'] = $doctor->specialties[0]->name;
				//$doctorListItem['specialities'] = $doctor->specialties[0]->name;
				
				$doctorSpecialities = $doctor->specialties;
				foreach($doctorSpecialities as $doctorSpeciality)
				{
					$value = ['name' => $doctorSpeciality->name, 'url' => $doctorSpeciality->linkUrl];
					$doctorListItem['specialities'][] = $value;
				}
				
				$doctorListItem['link'] = $doctor->link;
				if ($price = $doctor->inspectPrice) $doctorListItem['price'] = $doctor->inspectPrice;
				$doctorList[] = $doctorListItem;
			}
			$response = ['result' => 'ok', 'doctorList' => $doctorList];
		}
		
		if ($action == 'getSchedule')
		{
			//getWorkingHoursByDate?date=2015-04-10&type=forward&link=000077019&new=1&doctor=000011504&idPat=&JSON=1
			$address = Address::model()->findByAttributes(['linkUrl' => Yii::app()->request->getParam('linkUrl')]);
			//$_GET['link'] = $address->link;
			//$redirectParams = array_merge(['ajax/getWorkingHoursByDate'], $_GET);
			//$redirectParams['date']
			$link = $address->link;
			$date = Yii::app()->request->getParam('date');
			$date = gmdate("Y-m-d", strtotime($date));
			//$this->redirect($redirectParams);
		
			$json = Yii::app()->request->getParam("JSON");
			$doctor = Yii::app()->request->getParam("doctor");
			$type = Yii::app()->request->getParam("type");
			$new = Yii::app()->request->getParam('new');
			$idPat = (Yii::app()->request->getParam('idPat')) ? Yii::app()->request->getParam('idPat') : '';
	
			if ($link && $date) {
				$checkDate = preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $date);
	
				if ($checkDate) {
					switch ($type) {
						case 'forward':
							$date = date('Y-m-d', strtotime($date . '+1 days'));
							break;
						case 'back':
							$date = date('Y-m-d', strtotime($date . '-1 days'));
							break;
						default:
							break;
					}
	
					$time = WorkingHour::getDate($link, $date, $doctor, false, $idPat);
	
					if ($json) {
						echo CJSON::encode($time);
					} else {
						Yii::app()->clientScript->reset();
						$arr['html'] = trim($this->render('//clinic/' . ($new ? '_newCalendarBlock' : '_calendarBlock'), array(
									'time' => array($time),
									'short' => true
										), true));
						echo CJSON::encode($arr);
					}
					Yii::app()->end();
				} else {
					$error['error']['date'] = 'Введите date в формате YYYY-MM-DD';
				}
			} else {
				if (!$link) {
					$error['error']['link'] = 'Введите link';
				}
				if (!$date) {
					$error['error']['date'] = 'Введите date в формате YYYY-MM-DD';
				}
			}
	
			echo CJSON::encode($error);
			Yii::app()->end();
			
		}
		
		if ($action == 'addAppointment')
		{
			//назначаем атрибуты
			$attributes = [
				'name' => Yii::app()->request->getParam('name'),
				'phone' => Yii::app()->request->getParam('phone'),
				//'companyId' => Yii::app()->request->getParam('company'),
				//'addressId' => Yii::app()->request->getParam('address'),
				//'doctorId' => Yii::app()->request->getParam('doctor'),
				'plannedTime' => Yii::app()->request->getParam('date'),
				//'' => Yii::app()->request->getParam('time'),
				'sourcePageUrl' => urldecode(Yii::app()->request->getParam('source')),
			];
			
			//найдем соотв. клинику, адрес и врача
			$attributes['companyId'] = Company::model()->findByAttributes(['linkUrl' => Yii::app()->request->getParam('company')])->id;
			$attributes['addressId'] = Address::model()->findByAttributes(['linkUrl' => Yii::app()->request->getParam('address')])->id;
			$attributes['doctorId'] = Doctor::model()->findByAttributes(['linkUrl' => Yii::app()->request->getParam('doctor')])->id;
			$plannedTimeUnix = strtotime(Yii::app()->request->getParam('date') . ' ' . Yii::app()->request->getParam('time'));
			$timeDiff = 0;
			$attributes['plannedTime'] = date('Y-m-d H:i:s', ($plannedTimeUnix));
			$attributes['createdDate'] = date('Y-m-d H:i:s', (time()));
			$attributes['clientIdGA'] = Yii::app()->request->cookies['_ga_cid']->value;
			
			if (Yii::app()->request->getParam('appType') == AppointmentToDoctors::APP_TYPE_PHONE_EMPORTAL)
			{
				//добавление записи оператором
				$fromCallCenter = true;
				$attributes['appType'] = AppointmentToDoctors::APP_TYPE_PHONE_EMPORTAL;
				$attributes['statusId'] = AppointmentToDoctors::ACCEPTED_BY_REGISTER;
				$attributes['managerStatusId'] = '1';
			} else {
				$fromCallCenter = false;
				$attributes['appType'] = AppointmentToDoctors::APP_TYPE_FROM_APP_FORM;
				$attributes['statusId'] = AppointmentToDoctors::SEND_TO_REGISTER;
			}
			
			
			$model = new AppointmentToDoctors();
			$model->scenario = 'fromAppForm';
			$model->attributes = $attributes;
			
			$searchAttributes = [
				'companyId' => $attributes['companyId'], 
				'addressId' => $attributes['addressId'], 
				'doctorId' => $attributes['doctorId'], 
				'plannedTime' => $attributes['plannedTime']
			];
			$records = AppointmentToDoctors::model()->findByAttributes($searchAttributes);
			if (!$records && $model->validate() && $model->save())
			{
				$model->sendMailToClinic('new_record');
				if ($fromCallCenter)
				{
					//$model->sendSmsToClinic('new_record');
					$model->sendSmsToGuest(AppointmentToDoctors::ACCEPTED_BY_REGISTER); //т.к. все добавлено через колл-центр
					$attributes['fromCallCenter'] = $fromCallCenter;
				}
				$result = true;
			} else {
				$result = false;
			}
			
			$response = ['result' => $result, 'appointmentInfo' => $attributes, 'modelFields' => CJSON::encode($model->attributes)];
		}
		
		echo CJSON::encode($response);
	}
	
	public function actionSubscribe()
	{
		$attributes = [
			'type' => Yii::app()->request->getParam('type'),
			'email' => Yii::app()->request->getParam('email'),
			'name' => Yii::app()->request->getParam('name'),
		];
		
		$company = Company::model()->findByLink( Yii::app()->request->getParam('companyUrl') );
		if (!$company)
		{
			if ($attributes['type'] == 2) //если это подписка на новости компании, то будет ошибка
			{
				$output = [
					'success' => 'false',
					'message' => 'Компания с указанным идентификатором не найдена'
				];
				echo CJSON::encode($output);
				Yii::app()->end();
			}
		} else {
			$attributes['companyId'] = $company->id;
		}
		
		$doctorUrl = Yii::app()->request->getParam('doctorUrl');
		if ($doctorUrl)
		{
			$doctor = Doctor::model()->findByLink( Yii::app()->request->getParam('doctorUrl') );
			if ($doctor->id)
			{
				$attributes['doctorId'] = $doctor->id;
			}
		}
		
		if ($attributes['type'] == 3)
		{
			$user = User::model()->findByPk(Yii::app()->user->id);
			//если посетитель подписывается впервые (сразу после первой записи на прием через ЕМП)
			if (count($user) && !$user->email)
			{
				$userWithExistingEmail = User::model()->findByAttributes([ 'email' => $attributes['email'] ]);
				if (count($userWithExistingEmail) > 0)
				{
					$output = [
						'success' => 'false',
						'message' => 'Пользователь с таким e-mail уже существует! Пожалуйста, укажите другой e-mail адрес.'
					];
					echo CJSON::encode($output);
					Yii::app()->end();
				}
				$user->scenario = 'emailchange';
				$user->email = $attributes['email'];
				$user->update();
				$user->lastAppointment->sendMailToUser( AppointmentToDoctors::SEND_TO_REGISTER );
			}
		}
		
		$model = new EmailSubscription;
		$model->attributes = $attributes;

		$searchAttributes = [
				'type' => $model->type,
				'email' => $model->email,
		];
		if (EmailSubscription::model()->findByAttributes($searchAttributes) || $model->save())
		{
			$output = [
				'success' => 'true',
				'message' => 'E-mail подписка сохранена'
			];
		} else {
			$output = [
				'success' => 'false',
				'message' => 'При сохранении e-mail подписки произошла ошибка',
			];
		}
		
		echo CJSON::encode($output);
	}
	
	public function actionUnsubscribe()
	{
		$params = [
			'id' => Yii::app()->request->getParam('subId'),
			'email' => Yii::app()->user->model->email
		];
		
		$output = [
			'success' => false,
			'params' => $params,
			'msg' => null
		];
		
		$emailSub = EmailSubscription::model()->findByAttributes($params);
		if (count($emailSub))
		{
			if ($emailSub->delete())
			{
				$output['success'] = true;
			} else {
				$output['msg'][] = 'cannot delete emailSub';
			}
		} else {
			$output['msg'][] = 'cannot find emailSub';
		}
		echo CJSON::encode($output);
	}
	
	public function actionGetAppForm($id = null)
	{
		header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET");
		//header('content-type: text/html; charset=utf-8');
		
		$options = [
			'companyLinkUrl' => (string)Yii::app()->request->getParam('id'),
			'addressLinkUrl' => (string)Yii::app()->request->getParam('a'),
			'appType' => (string)Yii::app()->request->getParam('at'),
		];
		$appForm = new AppForm();
		$appForm->options = $options;
		
		$this->render('getAppForm', ['appForm' => $appForm]);
	}
	
	public function actionPurchaseService() {
		
		$result = [];
		$user = (Yii::app()->user->isGuest) ? null : Yii::app()->user->model;
		if (!$user)
		{
			//$result['success'] = false;
			//$result['msg'] = 'Зарегистрируйтесь или авторизуйтесь на emportal.ru, чтобы получить возможность приобретать услуги';
			//echo CJSON::encode($result);
			//Yii::app()->end();
		}
		$patientName = Yii::app()->request->getParam('name');
		$patientPhone = Yii::app()->request->getParam('phone');
		if (!$patientName)
		{
			$result['success'] = false;
			$result['msg'] = 'Пожалуйста, укажите свое имя';
			echo CJSON::encode($result);
			Yii::app()->end();
		}
		if (!$patientPhone)
		{
			$result['success'] = false;
			$result['msg'] = 'У вас не указан телефон! Пожалуйста, заполните телефон, чтобы иметь возможность записываться на услуги';
			echo CJSON::encode($result);
			Yii::app()->end();
		}
		$userId = $user ? $user->id : null;
		
		$addressServiceId = Yii::app()->request->getParam('id');
		$addressService = AddressServices::model()->findByPk( $addressServiceId );
		if (!count($addressService))
		{
			$result['success'] = false;
			$result['msg'] = 'В указанном адресе клиники такой услуги не найдено';
			echo CJSON::encode($result);
			Yii::app()->end();
		}
		$purchasedServiceAttributes = [
			'userId' => $userId,
			'serviceId' => $addressService->serviceId,
			'addressServiceId' => $addressService->id,
			'price' => $addressService->price,
		];
		$purchasedService = new PurchasedService();
		$purchasedService->attributes = $purchasedServiceAttributes;
		
		$newAppointmentAttributes = [
			'companyId' => $addressService->address->company->id,
			'userId' => $userId,
			'serviceId' => $addressService->id,
			'addressId' => $addressService->address->id,
			'name' => $patientName,
			'phone' => $patientPhone,
			//'email' => '',
			//'appType' => '',
			'visitType' => 'service',
			'createdDate' => date('Y-m-d H:i:s'),
			'plannedTime' => date('Y-m-d H:i:s', $addressService->address->closestReceptionTime),
			'statusId' => AppointmentToDoctors::SEND_TO_REGISTER,
		];
		$newAppointment = new AppointmentToDoctors('visit_to_service');
		$newAppointment->attributes = $newAppointmentAttributes;
		$newAppointment->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;

		$isVkApp = Yii::app()->request->getParam('vk');
		if($isVkApp) {
			$vkGroupId = Yii::app()->session['vkGroupId'];
			$newAppointment->sourcePageUrl = 'Группа: '.$vkGroupId;
		}
		
		if ($purchasedService->save() && $newAppointment->save())
		{
			//рассылка уведомлений
			$newAppointment->sendMailToClinic('new_record');
			$newAppointment->notifyOperator();
			if (Yii::app()->user->isGuest) {
				$newAppointment->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
			} else {
				$newAppointment->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
			}
			$newAppointment->sendSmsToClinic('new_record');
			
			$result['success'] = true;
			$result['msg'] = 'Запись на услугу успешно сохранена, с вами свяжутся в ближайшее время';
			$result['guest'] = Yii::app()->user->isGuest ? '1': '0';
			echo CJSON::encode($result);
			Yii::app()->end();
		} else {
			$result['success'] = false;
			$result['msg'] = 'При сохранении услуги произошла ошибка';
			echo CJSON::encode($result);
			Yii::app()->end();
		}
	}
	
	public function actionTest() {
		$params = [
			'default_timezone_get' => date_default_timezone_get(),
			'current_time' => date('Y-m-d H:i:s'),
		];
		print_r($params);
	}

	public function actionSimilarDoctor() {
		$address = Address::model()->find("linkUrl=:addressLinkUrl",[":addressLinkUrl"=>$_GET['addressLinkUrl']]);
		$doctor = Doctor::model()->find("linkUrl=:doctorLinkUrl",[":doctorLinkUrl"=>$_GET['doctorLinkUrl']]);
		
		$similar = [];
		$countSpec = [];
		if(is_array($doctor->specialties)) {
			foreach ($doctor->specialties as $sp) {
				if (!in_array($sp->shortSpecialtyId, $countSpec) && $sp->shortSpecialtyId) {
					$countSpec[$sp->shortSpecialtyOfDoctor->name] = $sp->shortSpecialtyId;
				}
			}
			if (count($countSpec)) {
				if (count($countSpec) == 1) {
					$countPerSpec = 4;
				} else {
					$countPerSpec = 8/count($countSpec);
				}
			}
		}
		
		if(count($countSpec)) {
			$doctorIds = [];
			foreach ($countSpec as $key => $spec) {
				$criteria = new CDbCriteria();
				$criteria->with = [
						'placeOfWorks' => [
								'together' => true,
								'with' => [
										'address' => [
												'together' => true,
												'with' => [
														'company' => [
																'together' => true,
																'with' => [
																		'companyContracts' => [
																				'joinType' => 'INNER JOIN'
																		]
																]
														],
														'userMedicals' => [
																'together' => true,
														],
														'metroStations' => [
																'together' => true,
														],
												]
										]
								]
						],
						'specialties' => [
								'together' => true,
						],
						'scientificTitle' => [
								'select' => 'id,name',
								'together' => true
						],
						'scientificDegrees' => [
								'select' => 'id,name',
								'together' => true
						],
						'sex' => [
								'select' => 'id,name,order',
								'together' => true
						],
				];
				$criteria->compare('specialties.shortSpecialtyId', $spec);
				$criteria->compare('t.id', '<>' . $doctor->id);
				$criteria->compare('userMedicals.agreementNew', 1);
				$criteria->compare('address.isActive',1);
				$criteria->addNotInCondition('t.id', $doctorIds);
				$criteria->compare('address.cityId', City::model()->selectedCityId);
				$criteria->limit = $countPerSpec;
				$criteria->group = 't.id';
				$criteria->order = 't.rating DESC';
				$criteria1 = clone $criteria;
				$criteria1->compare('address.samozapis', 0);
				$criteria2 = clone $criteria1;
				if(is_array($address->metroStations)) {
					$nearestMetroStations = [];
					foreach ($address->metroStations as $metroValue) {
						$nearestMetroStations[] = $metroValue->id;
					}
					$criteria2->addInCondition('metroStations.id', $nearestMetroStations);
				}
				$criteria3 = clone $criteria1;
				$criteria3->compare('address.cityDistrictId', $address->cityDistrictId);
		
				$data = Doctor::model()->findAll($criteria2);
				if (empty($data)) {
					$data = Doctor::model()->findAll($criteria3);
					if (empty($data)) {
						$data = Doctor::model()->findAll($criteria1);
					}
				}
				foreach($data as $d) {
					$doctorIds[] = $d->id;
				}
				$similar[$key] = $data;
			}
		} else {
			$criteria = new CDbCriteria();
			$criteria->with = [
					'placeOfWorks' => [
							'together' => true,
							'with' => [
									'company' => [
											'together' => true,
											'with' => [
													'companyContracts' => [
															'joinType' => 'INNER JOIN'
													]
											]
									],
									'address' => [
											'together' => true,
											'with' => [
													'userMedicals' => [
															'together' => true,
													],
													'metroStations' => [
															'together' => true,
													]
											]
									]
							]
					],
					'specialties' => [
							'together' => true,
					]
			];
			$criteria->compare('t.id', '<>' . $doctor->id);
			$criteria->compare('userMedicals.agreementNew', 1);
			if($address->isActive) {
				$criteria->compare('address.id', $address->id);
			}
			$criteria->compare('address.isActive',1);
			
			$criteria->limit = 4;
			$criteria->group = 't.id';
			$criteria->order = 't.rating DESC';
			
			$data = Doctor::model()->findAll($criteria);
			$similar['Похожие врачи клиники'] = $data;
		}
		
		echo "<div class='header_section' style='display:none;'>";
		if(isset($similar['Похожие врачи клиники'])) {
			echo "<h2 class='color1'>Не удалось записаться? <br> Посмотрите похожих врачей клиники</h2>";
		} elseif ($isSamozapis) {
			echo "<h2 class='color1'>Не удалось записаться? <br> Посмотрите врачей в частных клиниках</h2>";
		} else {
			echo "<h2 class='color1'>Похожие врачи</h2>";
		}
		echo "</div>";
		foreach ($similar ? $similar : [] as $name => $spec) {
			foreach ($spec as $key => $doc) {
				$this->renderPartial('//search/_doctor_card', [
					'data' => $doc,
					'groupedData' => [ $doc->id => $doc ],
				]);
			}
		}
	}
	
	public function actionSimilarClinic() {
		$address = Address::model()->find("linkUrl=:addressLinkUrl",[":addressLinkUrl"=>$_GET['addressLinkUrl']]);
		
		$caId = $address->companyActivites[0]->companyActivitesId;
		$metroId = $address->nearestMetroStation->id;
		$metroLink = $address->nearestMetroStation->linkUrl;
			
		$criteria = new CDbCriteria();
		$criteria->limit = 4;
		$criteria->group = 't.id';
		$criteria->compare('addresses.cityId',City::model()->selectedCityId);
		$criteria->compare('addresses.samozapis',0);
		$criteria->compare('userMedicals.agreementNew',1);
		$criteria->with = [
				'addresses' => [
						'together' => true,
						'with' => [
								'metroStations' => [
										'together' => true,
								],
								'companyActivites' => [
										'together' => true,
								],
								'userMedicals' => [
										'together' => true,
								]
						]
				]
		];
			
		$company = Company::model()->findAll($criteria);
		$cIds = [];
		foreach($company as $c) {
			if(!in_array($c->id, $cIds,true)) {
				$cIds[] = $c->id;
			}
		}
		$criteria = new CDbCriteria();
		$criteria->index = 'id';
		$criteria->order = 't.rating DESC';
		$criteria->addInCondition('t.id', $cIds);
		$criteria->compare('addresses.cityId',City::model()->selectedCityId);
		$criteria->compare('addresses.samozapis',0);
		$criteria->compare('userMedicals.agreementNew',1);
		$criteria->with = [
				'addresses' => [
						'scopes' => 'active',
						'together' => true,
						'with' => [
								'metroStations' => [
										'together' => true,
								],
								'companyActivites' => [
										'together' => true,
								],
								'userMedicals' => [
										'together' => true,
								]
						]
				]
		];
			
		$similar = Company::model()->findAll($criteria);
		
		foreach($similar as $key=>$clinic) {
			$this->renderPartial('//search/_clinic_card',[
					'data' => $clinic,
					'address' => $clinic->addresses[0],
					'addresses' => null,
					'big' => 0,
			]);
		}
	}
	
	public function actionRenderPartial()
    {
		$name = Yii::app()->request->getParam('name');
		$params = Yii::app()->request->getParam('params');
		if(!is_array($params)) {
			$params = [];
		}
		$this->renderPartial($name, $params);
	}
    
    public function actionGetSearchForm()
    {
		header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET");
        Yii::import('application.components.searchForm.*');
        Yii::app()->clientScript->reset();
        
        $params = [
            'settings' => [
                'refererId' => (int) Yii::app()->request->getParam('refererId'),
            ]
        ];
        if(Yii::app()->request->getParam('default_specialty')) {
        	$params['settings']['default_specialty'] = Yii::app()->request->getParam('default_specialty');
        }
        if(Yii::app()->request->getParam('title')) {
        	$params['settings']['title'] = Yii::app()->request->getParam('title');
        }
        if ($width = (int) Yii::app()->request->getParam('width') AND $width >= 180 AND $width <= 540) {
            $params['styles'] = [
                'width' => $width,
                'height' => $width,
            ];
        }
        
        $searchForm = new SearchForm($params);
        $searchForm->run();
    }

	public function actionCallBack(){
		$phone = Yii::app()->request->getParam('phone');
		$model = new CallBack();
		$model->phone = $phone;
		$model->date = date("Y-m-d H:i:s");
		//$model->setAttributes(['phone' => $phone]);
		$model->save();

		$model->sendMailToUser($phone);
	}

	public function actionVkAppClinic() {
		$region = Yii::app()->request->getParam('region');
		if($region) {
			$attributes = ['subdomain' => $region];
			$cityId = City::model()->findByAttributes($attributes)->id;

			$companyListCriteria = new CDbCriteria;
			$companyListCriteria->select = "t.companyLinkUrl, t.companyName";
			$companyListCriteria->order = "t.companyName";
			$companyListCriteria->compare("samozapis",0);
			$companyListCriteria->compare("cityId",$cityId);
			$companyList = CHtml::listData(ShortCompaniesOfDoctor::model()->findAll($companyListCriteria), 'companyLinkUrl', 'companyName');

			$this->renderPartial('//search/_vkAdminClinic', ['region' => $region, 'companyList' => $companyList]);
		}
		else {
			echo "Выберите регион!";
		}
	}

	public function actionVkAppAddress() {
		$companyLinkUrl = Yii::app()->request->getParam('company');

		if($companyLinkUrl) {
			$vkGroupId = Yii::app()->session['vkGroupId'];
			$vkAppModel = VkApp::model()->findAll('groupVk = :groupVk', ['groupVk' => $vkGroupId]);

			$criteria = new CDbCriteria;
			$criteria->with = ['company'];
			$criteria->compare('company.linkUrl',$companyLinkUrl);
			$criteria->compare('isActive',1);
			$addressList = Address::model()->findAll($criteria);

			$this->renderPartial('//search/_vkAdminAddress', ['companyLinkUrl' => $companyLinkUrl, 'addressList' => $addressList, 'vkAppModel' => $vkAppModel]);
		}
		else {
			echo "Выберите клинику!";
		}
	}

	public function actionVkAppSave() {
		$companyLinkUrl = Yii::app()->request->getParam('company');
		$saveAll = Yii::app()->request->getParam('saveAll');
		$addresses = Yii::app()->request->getParam('addresses');
		$addresses = json_decode($addresses);
		$vkGroupId = Yii::app()->session['vkGroupId'];

		$company = Company::model()->findByAttributes(['linkUrl' => $companyLinkUrl]);

		if($company) {
			Yii::app()->db->createCommand()->delete(VkApp::model()->tableName(), 'groupVk=:groupVk', ["groupVk"=>$vkGroupId]);
		
			if($saveAll) {
				$vkAppModel = new VkApp();
				$vkAppModel->groupVk = $vkGroupId;
				$vkAppModel->companyId = $company->id;
				$vkAppModel->save();
			}
			else {
				foreach ($addresses as $value) {
					$vkAppModel = new VkApp();
					$vkAppModel->groupVk = $vkGroupId;
					$vkAppModel->companyId = $company->id;
					$vkAppModel->addressId = $value;
					$vkAppModel->save();
				}
			}
		}

	}

	public function actionVkUserId() {
		echo Yii::app()->session['vkViewerId'];
	}
}


