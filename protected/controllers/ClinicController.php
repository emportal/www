<?php

class ClinicController extends LeftMenuController {
	#public $layout = '//layouts/card_layout';

	public $searchbox_type = SearchBox::S_TYPE_PATIENT;
	public $searchbox_tab = Search::S_CLINIC;
	public $menu = array(
		"Описание" => 'view',
			/* "Специалисты" => 'experts',
			  "Стоимость услуг" => 'price',
			  "Отзывы" => 'comments',
			  "Фотогалерея" => 'gallery',
			  "Акции" => 'actions', */
	);
	public $_model;
	public $arrGMU = array(
		'e909b718-2b35-11e2-b014-e840f2aca94f',
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f',
		'c06a39cb-f532-11e2-856f-e840f2aca94f',
		'93b817de-ee70-11e2-856f-e840f2aca94f',
		'c06a39ca-f532-11e2-856f-e840f2aca94f',
		'22075224-e934-11e2-856f-e840f2aca94f'
	);
	public $CompanyTypesNotOnline = array(
		'c2148457-29d6-11e2-b014-e840f2aca94f', // Выездная медицинская помощь
		'd16b98ad-349a-11e2-b014-e840f2aca94f', // Санаторий
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f', // Диспансер
		'71d418b8-6072-11e2-b019-e840f2aca94f', // Дом престарелых, интернат
		'a950904d-86fc-11e2-856f-e840f2aca94f'  // Тур. фирмы и тур. операторы с оздоровительными турами
	);

	public function filters() {
		return array(
			'accessControl',
			array('application.filters.PageLogger + view'), //rf: работает медленно, надо ускорить
			array('application.filters.PageContentsCache + view'/*,'cacheLifeTime'=>60*60*/),
			array('application.filters.CheckExpirePhone + visit'),
			array('application.filters.CheckBlockedAppointment + visit'),
		);
	}

	public function accessRules() {
		return array(
			array('deny',
				'actions' => array('comment', 'favorite'),
				'users' => array('?'),
			),
				/* array('deny',
				  'actions'	 => array('visit'),
				  'expression' => '
				  if(Yii::app()->user->model) {
				  return true;
				  } else {
				  return true;
				  }
				  '
				  ) */
		);
	}

	/* public function actionIndex() {

	  #$this->redirect('/');
	  } */

	public function actionView($link = null, $companyLink = null, $linkUrl = null) {
		Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', CClientScript::POS_END);
		$this->layout = '//layouts/card_layout';
		Address::$showInactive = true;

		#$add = Address::model()->
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$id = $addr->id;
			$link = $addr->link;
			$mdl = $this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				if (!$mdl->linkUrl OR !$mdl->company->linkUrl) {
					throw new CHttpException(404, 'The requested page does not exist.');
				} else {
					$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
				}
			}
		}
		if(Yii::app()->request->getParam('onlyIds')) {
			$ids = [];
			if(is_object($mdl)) $ids[] = $mdl->company->id;
			echo CJSON::encode($ids);
			exit;
		}
		if(Yii::app()->request->getParam('cardsOnly')) {
			$this->redirect(['/klinika', 'cardsOnly'=>1, 'Search[clinicLinkUrl]' => $mdl->company->linkUrl]);
		}


		// if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'clinic-visit') {
				
		// 	$result = [];
		// 	$user = (Yii::app()->user->isGuest) ? null : Yii::app()->user->model;

		// 	$patientName = Yii::app()->request->getParam('AppointmentToDoctors')['name'];
		// 	$patientPhone = Yii::app()->request->getParam('phoneInput');
		// 	if(!$patientName || !$patientPhone) {
		// 		if (!$patientName) {
		// 			$result['success'] = false;
		// 			$result['AppointmentToDoctors_name'][0] = 'Фамилия и Имя пациента обязательны';
		// 		}
		// 		if (!$patientPhone) {
		// 			$result['success'] = false;
		// 			$result['AppointmentToDoctors_phone'][0] = 'Введите телефон!';
		// 		}
		// 		echo CJSON::encode($result);
		// 		Yii::app()->end();
		// 	}
		// 	$userId = $user ? $user->id : null;
			
		// 	$addressServiceId = Yii::app()->request->getParam('serviceId');
		// 	$addressService = AddressServices::model()->findByPk( $addressServiceId );
		// 	if (!count($addressService))
		// 	{
		// 		$result['success'] = false;
		// 		$result['msg'] = 'В указанном адресе клиники такой услуги не найдено';
		// 		echo CJSON::encode($result);
		// 		Yii::app()->end();
		// 	}
		// 	$purchasedServiceAttributes = [
		// 		'userId' => $userId,
		// 		'serviceId' => $addressService->serviceId,
		// 		'addressServiceId' => $addressService->id,
		// 		'price' => $addressService->price,
		// 	];
		// 	$purchasedService = new PurchasedService();
		// 	$purchasedService->attributes = $purchasedServiceAttributes;
			
		// 	$newAppointmentAttributes = [
		// 		'companyId' => $addressService->address->company->id,
		// 		'userId' => $userId,
		// 		'serviceId' => $addressService->id,
		// 		'addressId' => $addressService->address->id,
		// 		'name' => $patientName,
		// 		'phone' => $patientPhone,
		// 		//'email' => '',
		// 		//'appType' => '',
		// 		'visitType' => 'service',
		// 		'createdDate' => date('Y-m-d H:i:s'),
		// 		'plannedTime' => Yii::app()->request->getParam('date'),
		// 		'statusId' => AppointmentToDoctors::SEND_TO_REGISTER,
		// 	];
		// 	$newAppointment = new AppointmentToDoctors('visit_to_service');
		// 	$newAppointment->attributes = $newAppointmentAttributes;
		// 	$newAppointment->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;
			
		// 	if ($purchasedService->save() && $newAppointment->save())
		// 	{
		// 		//рассылка уведомлений
		// 		if (Yii::app()->user->isGuest) {
		// 			$newAppointment->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
		// 		} else {
		// 			$newAppointment->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
		// 		}
		// 		$newAppointment->sendSmsToClinic('new_record');
		// 		$newAppointment->sendMailToClinic('new_record');
		// 		$newAppointment->notifyOperator();
				
		// 		Yii::app()->user->setFlash('visited', 1);
		// 		if (!Yii::app()->user->isGuest) {
		// 			/* if ($notifyOperator) { */
		// 			if ($isWidget == 1) {
		// 				$this->redirect(array('/widget/success'));
		// 			} else {
		// 				Yii::app()->session['reachedGoal_appointment_as_old_user'] = 1;
		// 				$this->redirect([
		// 					'/user/registry',
		// 					'filter' => 'all',
		// 				]);
		// 			}
		// 		} else {

		// 			/* create if account doesn't exists and autologin after */
		// 			if (!$userModel) {
		// 				$userModel = new User();
		// 				$userModel->name = $newAppointment->name;
		// 				$userModel->telefon = $newAppointment->phone;
						
		// 				$guidString = '';
		// 				if (function_exists('com_create_guid')){
							
		// 					$guidString = com_create_guid();
		// 				} else {
							
		// 					mt_srand((double)microtime()*10000);//для php 4.2.0 и выше
		// 					$charid = strtoupper(md5(uniqid(rand(), true)));
		// 					$hyphen = chr(45);// "-"
		// 					$guidString = chr(123)// "{"
		// 						.substr($charid, 0, 8).$hyphen
		// 						.substr($charid, 8, 4).$hyphen
		// 						.substr($charid,12, 4).$hyphen
		// 						.substr($charid,16, 4).$hyphen
		// 						.substr($charid,20,12)
		// 						.chr(125);// "}"
		// 				}
						
		// 				$newGenPassword = (string)substr(md5($guidString),0,6);							
		// 				$userModel->password = $newGenPassword; //генерим пароль
						
		// 				$userModel->phoneActivationDate = new CDbExpression('NOW()');
		// 				$userModel->dateRegister = new CDbExpression('NOW()');
		// 				$userModel->phoneActivationStatus = 1;
		// 				$userModel->status = 1;
		// 				$userModel->sexId = 'ae11c45d855a991340c5f59edcb404da';
						
		// 				//сохраняем нового юзера и высылаем на телефон смс с паролем для доступа к ЛК
		// 				if ($userModel->save(false)) {
						
		// 					$sms = new SmsGate();
		// 					$sms->phone = $userModel->telefon;
		// 					$sms->setMessage('Для просмотра или изменения своей записи на прием авторизуйтесь на emportal.ru! Логин - '. $userModel->telefon .', пароль - '.$newGenPassword, array(
		// 						'_code_' => $sms->code
		// 					));
		// 					$sms->send();
		// 				}

		// 				$newAppointment->userId = $userModel->id;
		// 				$newAppointment->save(false);
		// 			}
					
		// 			self::login($userModel->telefon, $userModel->password);
					
		// 			if ($isWidget == 1) {
		// 				$this->redirect(array('/widget/success'));
		// 			} else {
		// 				Yii::app()->session['reachedGoal_appointment_as_old_user'] = 1;
		// 				Yii::app()->session['reachedGoal_user_register'] = 1;
		// 				$this->redirect([
		// 					'/user/registry',
		// 				]);
		// 			}
		// 		}
		// 	} else {
		// 		$result['success'] = false;
		// 		$result['msg'] = 'При сохранении услуги произошла ошибка';
		// 		echo CJSON::encode($result);
		// 		Yii::app()->end();
		// 	}
		// }

		$appointmentType = 'visit_to_service';
		$appointment = new AppointmentToDoctors($appointmentType);
		$appointment->company = $addr->company;
		$appointment->address = $addr;
		$appointment->name = Yii::app()->user->model->name;
		$appointment->phone = Yii::app()->user->model->telefon;
		$appointment->email = Yii::app()->user->model->email;
		$appointment->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;
		$addressServiceId = Yii::app()->request->getParam('serviceId');
		$addressService = AddressServices::model()->findByPk( $addressServiceId );
		$appointment->serviceId = $addressService->id;

		if (Yii::app()->request->getParam('removeGuestPhone')) {
			if (Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll("phone=:phone AND (status=:stat1 or status=:stat2)", array(
					"phone" => Yii::app()->session['appointment_phone_set'],
					"stat1" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT,
				));
				Yii::app()->session['appointment_phone_set'] = "";
			}
			$this->redirect(array("/clinic/view",
				'linkUrl' => $linkUrl
			));
		}


		if (Yii::app()->request->getParam(get_class($appointment))) {
			$appointment->attributes = Yii::app()->request->getParam(get_class($appointment));
			$patientPhone = Yii::app()->request->getParam('phoneInput');
			$appointment->phone = $patientPhone;

			if(Yii::app()->request->getParam('date') == date('Y-m-d')) {
				$plannedTime = date('Y-m-d H:i:s', $addr->closestReceptionTime);
			} else {
				$plannedTime = Yii::app()->request->getParam('date').' 10:00:00';
			}
			$appointment->plannedTime = $plannedTime;
			$appointment->createdDate = date('Y-m-d H:i:s');
			$appointment->userId = Yii::app()->user->id;
			$appointment->statusId = AppointmentToDoctors::SEND_TO_REGISTER;
			$appointment->visitType = 'service';

			$lnk = Yii::app()->request->getParam(get_class($appointment))['address']['link'];
			if (!empty($lnk)) {
				$addr = Address::model()->findByLink($lnk);
				if ($addr->company->id == $appointment->company->id) {
					$appointment->address = $addr;
				}
			}

			if(!empty($appointment->phone)) {
				/* Если гость ввел номер который указан у пользователя, привязываем запись к аккаунту */
				if (Yii::app()->user->isGuest) {
					$userModel = User::model()->findByAttributes([
						'telefon' => $appointment->phone
					]);
					if ($userModel) {
						$appointment->userId = $userModel->id;
					}
				}
				/* Если не гость ввел номер, при записи, сохраняем в аккаунт */
				else {
					if (empty(Yii::app()->user->model->telefon) && !User::userWithThisPhoneExists($appointment->phone)) {
						$userModel = Yii::app()->user->model;
						$userModel->telefon = $appointment->phone;
						$userModel->save();
					}
				} /*  */
			}

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'doctor-visit') {
				$result = CJSON::decode(CActiveForm::validate($appointment));
				if(empty($appointment->extAppointmentId)) {
					$request_appointment_phone = strval(trim($_REQUEST[get_class($appointment)]['phone']));
					$request_phoneInput = strval(trim($_REQUEST['phoneInput']));
					if ($request_appointment_phone !== $request_phoneInput) {
						$result['AppointmentToDoctors_phone'][0] = 'Телефон не активирован';
						$result['success'] = false;
					}
				}
				$patientName = Yii::app()->request->getParam('AppointmentToDoctors')['name'];
				$patientPhone = Yii::app()->request->getParam('phoneInput');
				if(!$patientName || !$patientPhone) {
					if (!$patientName) {
						$result['success'] = false;
						$result['AppointmentToDoctors_name'][0] = 'Фамилия и Имя пациента обязательны';
					}
					if (!$patientPhone) {
						$result['success'] = false;
						$result['AppointmentToDoctors_phone'][0] = 'Введите телефон!';
					}
				}
				echo CJSON::encode($result);
				Yii::app()->end();
			}
				
			
			if ($appointment->save()) {
				
				$storedAppointment = [
					'name' => $appointment->name,
					'phone' => $appointment->phone,
					'plannedTime' => $appointment->plannedTime,
					'companyName' => $appointment->company->name,
					'addressName' => $appointment->address->name,
					'doctorName' => $appointment->doctor->name,
					'hiddenField' => $appointment->plannedTime,
				];
				if ($isWidget == 1) {
					
					Yii::app()->session['storedAppointment'] = $storedAppointment;
				}
				
				if (Yii::app()->user->isGuest) {
					Yii::app()->session['appointment_phone_set'] = "";
				}

				PhoneVerification::model()->deleteAll('phone = :pId AND (status = :stat OR status = :stat2)', array(
					"pId" => $appointment->phone,
					"stat" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT
				));
				#}
				//Чистим сессию
				if (Yii::app()->user->isGuest) {
					Yii::app()->session['appointment_phone_set'] = "";
				}
				
				//рассылка уведомлений
				$appointment->notifyOperator();
				
				if (Yii::app()->user->isGuest) {
					$appointment->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
				} else {
					$appointment->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				}
				$appointment->sendSmsToClinic('new_record');
				$appointment->sendMailToClinic('new_record');

				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(array('registrySuccess' => 'Запись успешно произведена'));
					Yii::app()->end();
				} else {
				
					Yii::app()->user->setFlash('visitedService', 1);
					if (!Yii::app()->user->isGuest) {
						/* if ($notifyOperator) { */
						if ($isWidget == 1) {
							$this->redirect(array('/widget/success'));
						} else {
							Yii::app()->session['reachedGoal_appointment_as_old_user'] = 1;
							$this->redirect([
								'/user/registry',
								'filter' => 'all',
								//'_ym_debug' => '1'
							]);
						}
						/* } else {
						  $this->redirect(array('user/registry', 'link' => $doctor->link));
						  } */
					} else {

						/* create if account doesn't exists and autologin after */
						if (!$userModel) {
							$userModel = new User();
							$userModel->name = $appointment->name;
							$userModel->telefon = $appointment->phone;
							
							$guidString = '';
							if (function_exists('com_create_guid')){
								
								$guidString = com_create_guid();
							} else {
								
								mt_srand((double)microtime()*10000);//для php 4.2.0 и выше
								$charid = strtoupper(md5(uniqid(rand(), true)));
								$hyphen = chr(45);// "-"
								$guidString = chr(123)// "{"
									.substr($charid, 0, 8).$hyphen
									.substr($charid, 8, 4).$hyphen
									.substr($charid,12, 4).$hyphen
									.substr($charid,16, 4).$hyphen
									.substr($charid,20,12)
									.chr(125);// "}"
							}
							
							$newGenPassword = (string)substr(md5($guidString),0,6);							
							$userModel->password = $newGenPassword; //генерим пароль
							
							$userModel->phoneActivationDate = new CDbExpression('NOW()');
							$userModel->dateRegister = new CDbExpression('NOW()');
							$userModel->phoneActivationStatus = 1;
							$userModel->status = 1;
							$userModel->sexId = 'ae11c45d855a991340c5f59edcb404da';
							
							//сохраняем нового юзера и высылаем на телефон смс с паролем для доступа к ЛК
							if ($userModel->save(false)) {
							
								$sms = new SmsGate();
								$sms->phone = $userModel->telefon;
								$sms->setMessage('Для просмотра или изменения своей записи на прием авторизуйтесь на emportal.ru! Логин - '. $userModel->telefon .', пароль - '.$newGenPassword, array(
									'_code_' => $sms->code
								));
								$sms->send();
							}

							$appointment->userId = $userModel->id;
							$appointment->save(false);
						}
						
						self::login($userModel->telefon, $userModel->password);

						//Yii::app()->user->setFlash('visited', 1); //rm
						
						if ($isWidget == 1) {
							$this->redirect(array('/widget/success'));
						} else {
							Yii::app()->session['reachedGoal_appointment_as_old_user'] = 1;
							Yii::app()->session['reachedGoal_user_register'] = 1;
							$this->redirect([
								'/user/registry',
								//'_ym_debug' => '1'
							]);
						}
					}
				}
			} else {
				if (isset($_REQUEST['JSON'])) {
					$errors = $appointment->errors;
					if ($_REQUEST[get_class($appointment)]['phone'] != "" && Yii::app()->user->isGuest) {
						$errors['phone'][] = 'ACTIVATE_PHONE';
					}

					echo CJSON::encode(array('registryError' => $errors));
					Yii::app()->end();
				}
			}
		}


		// Переадресация на поддомен нужного региона
		//if($mdl->samozapis==1 && !Yii::app()->params["samozapis"]) {
		//	$this->layout = null;
		//	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
		//	$this->render('//site/error404', ['code' => 404]);
		//	Yii::app()->end();
		//}
		//$url = Yii::app()->createUrl('clinic/view',['linkUrl' => $linkUrl, 'companyLink' => $companyLink]);
		//$redirectUrl = City::getRedirectUrl($mdl->city->subdomain, $mdl->samozapis==1, $url);
		//if($redirectUrl !== false) $this->redirect($redirectUrl, true, '301');

		$url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		$url = explode('?', $url)[0]; //allow get parameters
		$possibleUrls = $mdl->pagesUrls;
		if (!in_array($url, $possibleUrls)) {
			if(count(array_intersect(["dev1","dev2","dev3","dev4","dev5","dev6","dev7","dev8","dev9","spb","local"], explode('.', $_SERVER['HTTP_HOST']))) < 1) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		$model = $mdl;

		$services = [];
		$prices = [];
		$minPrice = null;
		foreach ($model->addressServices as $as) {

			if($as->service->link == 'zapnp') {
				$service = $as;
				continue;
			}

			if (!$as->isActual){
				continue;
			}
			$companyActiviteName = ($as->companyActivite->name) ? $as->companyActivite->name : 'Прочие услуги';
			
			if($minPrice === null || $minPrice > $as->price) {
				$minPrice = $as->price;
			} 
			if(empty($prices[$companyActiviteName])) {
				$prices[$companyActiviteName] = $as->price;
			} else {
				if($as->price < $prices[$companyActiviteName]) {
					$prices[$companyActiviteName] = $as->price;
				}
			}
			$services[$companyActiviteName][] = [
				'id' => $as->id,
				'name' => $as->service->name,
				'price' => $as->price,
				'description' => $as->description,
				'oldPrice' => $as->oldPrice,
				'free' => $as->free,
				'as' => $as,
			];
		}

		$specialtyCriteria = new CDbCriteria();
		$specialtyCriteria->with = [
			'doctorSpecialties' => [
				'select' => false,
				'with' => [
					'select' => false,
					'specialtyOfDoctors' => [
						'select' => false,
						'with' => [
							'select' => false,
							'doctor' => [
								'select' => false,
								'with' => [
									'select' => false,
									'placeOfWorks'
								]
							]
						]
					]
				]
			]
		];
		$specialtyCriteria->order = 't.name ASC';
		$specialtyCriteria->compare('placeOfWorks.addressId', $model->id);
		$doctorSpecialties = CHtml::listData(ShortSpecialtyOfDoctor::model()->findAll($specialtyCriteria), 'id', 'name');


		$doctorCriteria = new CDbCriteria;
		$doctorCriteria->select = "name,linkUrl,link,sexId,rating,experience";
		$doctorCriteria->group = '`t`.`id`';
		$doctorCriteria->order = "t.rating DESC, t.name ASC";
		$doctorCriteria->with = array(
				'appointmentCount' => array(
						'together' => true
				),
				'placeOfWorks' => array(
						'together' => true,
						'select' => 'id,doctorId,addressId',
						'with' => array(
								'address' => [
										'select' => "id,street,houseNumber,link,ownerId,cityId,linkUrl,name,latitude,longitude,samozapis",
										'together' => true,
										#'scopes' => 'active',
										'with' => [
												'userMedicals' => [
														'together' => true,
														'select' => false,
												],
												'company' => [
														'together' => true,
														'select' => 'id,name,ratingSite,rating,ratingUser,link,linkUrl',
														'with' => [
																'companyContracts' => [
																		'joinType' => 'INNER JOIN',
																		'select' => false,
																		'together' => true
																]
														]
												],
												'metroStations' => [
														'together' => false,
														'select' => 'id,name'
												]
										]
								]
						),
				),
				'scientificTitle' => [
						'select' => 'id,name',
						'together' => true
				],
				'scientificDegrees' => [
						'select' => 'id,name',
						'together' => true
				],
				'sex' => [
						'select' => 'id,name,order',
						'together' => true
				],
		);
		
		$doctorCriteria->compare('address.link', $model->link);


		$dataProvider = new CActiveDataProvider('Doctor', array(
			'criteria' => $doctorCriteria,
			'pagination' => array(
				'pageSize' => isset($_REQUEST['photo']) ? 400 : 10,
				'pageVar' => 'page',
			),
		));
		
		$metroLink = [];
		foreach ($model->nearestMetroStations as $nearestMetro) {
			$nearestMetroLinkUrl = "metro-" . $nearestMetro->metroStation->linkUrl;
			if(!in_array($nearestMetroLinkUrl, $metroLink)) {
				$metroLink[] = $nearestMetroLinkUrl;
			}
		}
		if(!empty($model->cityDistrict)) {
			$metroLink[] = "raion-" . $model->cityDistrict->linkUrl;
		}

		$searchModel = new Search();

		$reviews = Review::model()->findAllByAttributes(['addressId' => $model->id, 'statusId' => 2], "reviewText != '' AND reviewText IS NOT NULL");
		
		$this->render('view', array(
			'model' => $model,
			'searchModel' => $searchModel,
			'doctorSpecialties' => $doctorSpecialties,
			'dataProvider' => $dataProvider,
			'doctors' => [],
			'prices' => $prices,
			'metroLink' => $metroLink,
			'minPrice' => $minPrice,
			'services' => $services,
			'similar' => $similar,
			'show' => 1,
			'reviews' => $reviews,
			'service' => $service,
				//'email' => $email
		));
	}

	public function actionGallery($link = null, $companyLink = null, $linkUrl = null) {
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$model = $this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}
		//$this->pageTitle = $model->company->name;

		$this->render('gallery', array(
			'model' => $model,
		));
	}

	public function actionActions($link = null, $companyLink = null, $linkUrl = null) {
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$model = $this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}



		$actions = Action::model()->published()->active()->findAll('addressId=:id', array(
			'id' => $model->id
		));

		//$this->pageTitle = $model->company->name;
		$this->render('actions', array(
			'model' => $model,
			'actions' => $actions,
		));
	}

	public function actionExperts($link = null, $companyLink = null, $linkUrl = null) {
		
		if (!isset($_REQUEST['JSON']))
			throw new CHttpException(404, 'The requested page does not exist.');
		
		if ($link || ($companyLink && $linkUrl)) {
			if ($companyLink && $linkUrl) {
				$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
					':cLink' => $companyLink,
					':aLink' => $linkUrl
				]);

				$link = $addr->link;

				$model = $this->loadModel($link);
				#if($model->userMedicals->agreementNew != 1) {				
				$this->redirect(['view','linkUrl' => $linkUrl,'companyLink' => $companyLink]);
				#}
			} else {
				$mdl = $this->loadModel($link);
				if (!isset($_REQUEST['JSON'])) {
					$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
				}
			}



			$model = $model->with(array(
						'doctors',
						'doctors.specialties',
						'doctors.specialtyOfDoctors',
						'doctors.specialtyOfDoctors.doctorSpecialty',
						'doctors.specialtyOfDoctors.doctorCategory'
							)
					)->findByAttributes(array('link' => $link));
			/* @var $model Company */

			$doctors = $model->doctors;

			$specialties = array();

			foreach ($doctors as &$doctor) {
				foreach ($doctor->specialties as &$specialty) {
					$specialty->s_doctors[] = $doctor;
					$specialties[$specialty->name] = $specialty;
				}
			}
			ksort($specialties);
			if (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(array('doctors' => $doctors, 'specialties' => $specialties));
				Yii::app()->end();
			}


			$this->render('experts', array(
				'model' => $model,
				'doctors' => $doctors,
				'specialties' => $specialties,
			));
		} else {

			#$companyLink = Yii::app()->request->getParam('companyLink');
			$addressLink = trim(Yii::app()->request->getParam('addressLink'), ',');
			$aLinks = [];
			if (mb_strpos($addressLink, ',') !== FALSE) {
				$aLinks = explode(',', $addressLink);
			} else {
				$aLinks[0] = $addressLink;
			}
			$doctorSpecialtyId = Yii::app()->request->getParam('doctorSpecialtyId');
			$criteria = new CDbCriteria();
			$criteria->with = [
				'scientificDegrees' => [
					'select' => 'id,name',
				],
				'scientificTitle' => [
					'select' => 'id,name',
				],
				/* 'currentPlaceOfWork' => [
				  'select' => false,
				  'scopes' => 'current'
				  ],
				  'currentPlaceOfWork.address' => [
				  'select' => false
				  ],
				  'currentPlaceOfWork.address.company' => [
				  'select' => false
				  ], */
				'placeOfWorks' => [
				],
				'placeOfWorks.address' => [
					'alias' => 'placeOfWorkAddress',
				],
				'specialties',
				'specialtyOfDoctors',
				'specialtyOfDoctors.doctorSpecialty' => [
				],
				'specialtyOfDoctors.doctorCategory'
			];

			$criteria->addInCondition('placeOfWorkAddress.link', $aLinks);
			$criteria->compare('specialties.shortSpecialtyId', $doctorSpecialtyId);
			$doctors = Doctor::model()->findAll($criteria);

			$specialtyOfDoctor = $placeOfWorks = $specialties = $items = [];

			foreach ($doctors as &$doctor) {
				$doctor->experience = date('Y') - $doctor->experience;
				foreach ($doctor->placeOfWorks as $pw) {
					$placeOfWorks[$doctor->link][] = ['link' => $pw->address->link, 'name' => $pw->address->name];
				}

				foreach ($doctor->specialties as &$specialty) {
					$specialty->s_doctors[] = $doctor;
					$specialties[$specialty->getPrimaryKey()] = $specialty;
					$items[$specialty->name][] = $doctor;
				}

				foreach ($doctor->specialtyOfDoctors as $sp) {
					$specialtyOfDoctor[$doctor->link][] = ['specialtyName' => $sp->doctorSpecialty->name, 'categoryName' => $sp->doctorCategory->name];
				}
				$doctorDegrees[$doctor->link] = [
					'scientificTitle' => $doctor->scientificTitle->name,
					'scientificDegree' => $doctor->scientificDegrees[0]->name
				];
			}

			if (isset($_REQUEST['JSON'])) {
				echo CJSON::encode([
					'doctors' => $doctors,
					'specialties' => $specialties,
					'placeOfWorks' => $placeOfWorks,
					'specialtyOfDoctor' => $specialtyOfDoctor,
					'doctorDegrees' => $doctorDegrees,
					'items' => $items
				]);
				Yii::app()->end();
			}
		}
	}

	public function actionVisit($link = null, $companyLink = null, $linkUrl = null) {
		
		//redirect to main address
		$this->redirect(array('site/index'));
		return false;
		
		Yii::app()->clientScript->registerCoreScript('jquery.maskedinput');
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$clinic = $this->loadModel($link);
			if($clinic->userMedicals->agreementNew != 1 && !isset($_REQUEST['JSON'])) {				
				$this->redirect(['view','linkUrl' => $linkUrl,'companyLink' => $companyLink]);
			}
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
			$clinic = $mdl;
		}


		if (Yii::app()->request->getParam('goto_register')) {
			Yii::app()->session['returnUrl'] = $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('link' => $link));
			$this->redirect('/register');
		}
		if (Yii::app()->request->getParam('removeGuestPhone')) {
			if (Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll("phone=:phone AND (status=:stat1 or status=:stat2)", array(
					"phone" => Yii::app()->session['appointment_phone_set'],
					"stat1" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT,
				));
				Yii::app()->session['appointment_phone_set'] = "";
			}
			$this->redirect(array("/clinic/visit",
				'link' => $link
			));
		}


		//$debug=true;
		$showPicker = true;

		if (Yii::app()->user->isGuest) {
			//Если пользователь гость - выводим сообщение о возможности зарегистрироваться
			Yii::app()->user->setFlash('askAuth', 'Уважаемый гость, вы можете зарегистрироваться на нашем портале. После регистрации Вы получите доступ в личный кабинет, в котором Вы сможете: <ul>'
					. '<li>Отследить историю посещения клиник и врачей</li>'
					. '<li>Оставить отзыв о медицинском учреждении</li>'
					. '<li>Подписаться на рассылку о новых услугах и акциях клиник, Единого медицинского портала и др.</li>'
					. '</ul>'
					. '<div style="text-align:center"><span onclick="location.href=\'' . $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('goto_register' => 1, 'link' => $link)) . '\'" class="btn-green">Зарегистрироваться</span> <span id="closeAuthBlock" class="btn-red">Скрыть блок</span></div>');
		} else {
			//Если пользователь зарегистрирован - проверяем есть ли у него подтвержденный телефон для записи на 1 раз
			$pv = PhoneVerification::model()->find("status=:status AND userId = :userId", array(
				':status' => PhoneVerification::APPOINTMENT,
				':userId' => Yii::app()->user->model->id,
			));
		}

		$model = new AppointmentToDoctors('visit');


		$LKK = UserMedical::model()->findByAttributes([
			'companyId' => $clinic->company->id
		]);


		//Если у компании нет ЛКК - только услуга первоначальный выбор
		if (empty($LKK)) {

			$showPicker = false;

			#$model->scenario = 'visit_no_contract';
			//Если контракта нету. Уведомить штатного оператора о заявке
			/* $notifyOperator = true; */


			/* Для отладки */
			if (!empty($debug)) {
				$showPicker = true;
			}
		}

		$model->company = $clinic->company;
		$model->address = $clinic;
		$model->service = Service::model()->findByLink(Yii::app()->request->getParam('service'));
		$model->name = Yii::app()->user->model->name;
		$model->phone = Yii::app()->user->model->telefon;
		$model->email = Yii::app()->user->model->email;
		$model->visitType = AppointmentToDoctors::VISIT_TYPE_CLINIC;
		$model->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;

		if (Yii::app()->request->getParam(get_class($model))) {
			$model->attributes = Yii::app()->request->getParam(get_class($model)); //$_POST[get_class($model)];
			$lnk = Yii::app()->request->getParam(get_class($model))['address']['link'];
			if (!empty($lnk)) {
				$addr = $this->loadModel($lnk);
				if ($addr->company->id == $model->company->id) {
					$model->address = $addr;
				}
			}

			$model->phone = "+" . MyRegExp::phoneRegExp($model->phone);
			$model->createdDate = date('Y-m-d H:i:s');
			$model->service = Service::model()->findByLink($model->serviceId);
			$model->doctor = Doctor::model()->findByLink($model->doctorId);
			$model->userId = Yii::app()->user->id;
			$model->statusId = AppointmentToDoctors::SEND_TO_REGISTER;

			/* Если гость ввел номер который указан у пользователя, привязываем запись к аккаунту */
			/* Если не гость ввел номер при реге, сохраняем в аккаунт */
			if (Yii::app()->user->isGuest) {
				$userModel = User::model()->findByAttributes([
					'telefon' => $model->phone
				]);
				if ($userModel) {
					$model->userId = $userModel->id;
				}
			} else {
				if (empty(Yii::app()->user->model->telefon)) {
					$userModel = Yii::app()->user->model;
					$userModel->telefon = $model->phone;
					$userModel->save();
				}
			}

			if (!Yii::app()->user->isGuest && $model->phone != Yii::app()->user->model->telefon) {
				$phoneModel = PhoneVerification::model()->user()->readyOneTimePhone()->find();

				if ($phoneModel === NULL) {


					$phone = $model->phone;
					$smsgate = new SmsGate();
					$code = $smsgate->generateCode();
					$smsgate->setMessage('Код подтверждения: _code_.', array(
						'_code_' => $smsgate->code
					)); //SmsGate::VERIFICATION;				
					$smsgate->phone = $phone;
					$smsgate->send();

					$pvModel = new PhoneVerification();
					$pvModel->code = $code;
					$pvModel->phone = $phone;
					$pvModel->userId = $model->userId;
					$pvModel->status = PhoneVerification::READY_TO_APPOINTMENT;
					$pvModel->save();

					echo CJSON::encode(['success' => false, 'blockedappointment' => true]);
					Yii::app()->end();
				} else {
					
				}
			}

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'doctor-visit') {
				$arrData = CJSON::decode(CActiveForm::validate($model));
				if ($_POST[get_class($model)]['phone'] == "" && $_POST['phoneInput'] != "") {
					$arrData['AppointmentToDoctors_phone'][0] = 'Телефон не активирован';
					unset($arrData['AppointmentToDoctors_phone'][1]);
				}
				echo CJSON::encode($arrData);
				Yii::app()->end();
			}

			#var_dump($model->attributes);
			if ($model->save()) {

				/* Чистим таблицу PhoneVerification для временного номера */
				#if(!Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll('phone = :pId AND (status = :stat OR status = :stat2)', array(
					"pId" => $model->phone,
					"stat" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT
				));
				#}
				//Чистим сессию
				if (Yii::app()->user->isGuest) {
					Yii::app()->session['appointment_phone_set'] = "";
				}

				/*
				  #if (!empty($notifyOperator)) {

				  #} */

				#if($model->phone != '+79213434502') {
				/* Уведомить штатного оператора о том, что произведена заявка в клинику  */
				$model->notifyOperator();

				/* Оповещаем пользователя по смс и почте */
				$model->sendMailToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				if (Yii::app()->user->isGuest) {
					$model->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
				} else {
					$model->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				}
				/* Оповещаем клинику по смс и почте */
				$model->sendMailToClinic('new_record');
				$model->sendSmsToClinic('new_record');
				#}
				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(array('registrySuccess' => 'Запись успешно произведена'));
					Yii::app()->end();
				} else {
					Yii::app()->user->setFlash('registrySuccess', 'Запись успешно произведена');
					if (!Yii::app()->user->isGuest) {
						if ($notifyOperator) {
							$this->redirect(array('/user/registry', 'filter' => 'all'));
						} else {
							$this->redirect(array('/user/registry'));
						}
					} else {
						$pollCriteria = new CDbCriteria();
						$pollCriteria->scopes = 'afterVisit';
						#$pollCriteria->addInCondition('t.id', []);
						$polls = Poll::model()->afterVisit()->findAll($pollCriteria);
						$text = "							
						<p>Ваша заявка на прием успешно отправлена в медицинское учреждение!</p>
						
						Информация по записи:
						<ul style=\"margin-top:0px;\">
							<li>Учреждение: " . $model->company->name . "</li>
							<li>Время приема: " . $model->registryPlannedTime . "</li>						
							<li>ФИО специалиста: " . $model->doctor->name . "</li>
						</ul>					
						
						<p>Как только специалисты клиники обработают вашу заявку, 
						вы получите подтверждение в виде смс или звонка представителя клиники. 
						Для обеспечения высокого качества обслуживания операторы 
						Единого Медицинского Портала будут контролировать процесс обработки вашей заявки сотрудниками клиники.</p>
						
						<p>А пока просим вас поделиться своими впечатлениями.</p>
						<div class=\"pollAppointment\">";
						foreach($polls as $poll) {							
							$text.="<div class=\"one-poll\">							
										<div class=\"one-poll-text\">".$poll->question."</div>
										".CHtml::radioButtonList($poll->id, '', CHtml::listData($poll->pollOptions,'id','pollOption'))."
									</div>";
						}
						
						$text.=	
						"</div>
						<a href=\"" . $this->createUrl('site/register') . "\">Регистрируйтесь</a> на нашем сайте и получайте еще больше возможностей: 						
						<ul style=\"margin-top:0px;\">
							<li>Просмотр истории посещения клиник;</li>
							<li>Уведомления о скидках и акциях;</li>
							<li>Публикация отзывов, а также оценка клиник и врачей.</li>	
						</ul>
						
						<p>Будем признательны, если после приема вы оставите отзыв о медицинском учреждении и специалисте на нашем сайте.</p>
						<a onclick=\"sendPoll(); return true;\" class=\"btn btn-green\" href=\"/\">Далее</a>
						";
						Yii::app()->user->setFlash('confirmedVisit', $text);
						$this->redirect(array('/site/confirmedVisit'));
					}
				}
			} else {
				if (isset($_REQUEST['JSON'])) {
					$errors = $model->errors;
					if ($_REQUEST[get_class($model)]['phone'] != "" && Yii::app()->user->isGuest) {
						$errors['phone'][] = 'ACTIVATE_PHONE';
					}

					echo CJSON::encode(array('registryError' => $errors));
					Yii::app()->end();
				}

				#var_dump($model->getErrors());
			}
			$model->doctorId = $model->doctor->link;
			$model->serviceId = $model->service->link;
		}
		$timeBlock = WorkingHour::getDatesForCal($link, null, null, $model->doctor->link);

		#var_dump($timeBlock);

		$this->render('visit', array(
			'link' => $link,
			'model' => $model,
			'pv' => $pv,
			'time' => $timeBlock,
			'showPicker' => $showPicker
		));
	}

	public function actionPrice($link = null, $companyLink = null, $linkUrl = null) {
		if (Yii::app()->request->getParam('JSON')) {
			$this->layout = null;
			Yii::app()->clientScript->reset();
		}
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}

		$activity = Yii::app()->request->getParam('activity', false);
		$activity = CompanyActivite::model()->findByAttributes(array('link' => $activity));

		$prices = array();

		if ($activity) {
			$criteria = new CDbCriteria();

			$criteria->with = array(
				'service',
				'address'
			);
			$criteria->compare('service.companyActiviteId', $activity->id);
			$criteria->compare('address.link', $link);
			$prices = AddressServices::model()->findAll($criteria);
		}
		$addressCriteria = new CDbCriteria();
		$addressCriteria->compare("t.link", $link);
		$addressCriteria->order = "ca.name ASC";
		$addressCriteria->with = [
			/* cущности разные, просто написаны одинаково  */
			'companyActivites' => [
				'together' => true,
				'with' => [
					'companyActivites' => [
						'alias' => 'ca'
					]
				]
			]
		];
		$model = Address::model()->find($addressCriteria);
		#$model = $this->loadModel($link);

		$noDataId = Yii::app()->db->createCommand()
				->select("id")
				->from(CompanyActivite::model()->tableName())
				->where("name = 'Нет данных'")
				->queryScalar();

		$countServices = array();
		$data = Yii::app()->db->createCommand()
						->select("*,count(*) as count")
						->from(AddressServices::model()->tableName())
						->where("companyActivitesId <> '$noDataId' AND  addressId ='$model->id'")
						->group('companyActivitesId')->queryAll();

		foreach ($data as $row) {
			if ($row['companyActivitesId'])
				$countServices[$row['companyActivitesId']] = $row['count'];
		}
		//Yii::app()->db()->createCommand()->g	
		/* foreach($model->addressServices as $service) {
		  $countServices[$service->companyActivitesId]=count(AddressServices::model()->findAll(''));
		  }
		  var_dump($countServices); */
		/*
		 * foreach($model->companyActivites as $act) {			
		  $criteria = new CDbCriteria();

		  $criteria->with	 = array(
		  'service',
		  'address'
		  );
		  $criteria->compare('service.companyActiviteId', $activity->id);
		  $criteria->compare('address.link',$link);
		  //search/clinic?ajax=yw0&JSON=1&Company_page=1
		  $addrs=AddressServices::model()->findAll($criteria);

		  $countServices[$act->companyActivites->id] = count($addrs);
		  }
		 */
		$this->render('price', array(
			'model' => $model,
			'activity' => $activity,
			'prices' => $prices,
			'countServices' => $countServices
		));
	}

	public function actionComments($link = null, $companyLink = null, $linkUrl = null) {
	
		//redirect to main address
		$this->redirect(array('site/index'));
		return false;
		
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$clinic = $this->loadModel($link);		
			if($clinic->userMedicals->agreementNew != 1) {				
				$this->redirect(['view','linkUrl' => $linkUrl,'companyLink' => $companyLink]);
			}
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}


		$this->render('comments', array(
			'model' => $clinic,
		));
	}

	public function actionComments_experts($link = null, $companyLink = null, $linkUrl = null) {
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			if($model->userMedicals->agreementNew != 1) {				
				$this->redirect(['view','linkUrl' => $linkUrl,'companyLink' => $companyLink]);
			}
		} else {
			$mdl = $this->loadModel($link);
			if($mdl && $mdl->company) {
				if (!isset($_REQUEST['JSON'])) {
					$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
				}
			} else {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}

		$criteria = new CDbCriteria();
		$criteria->with = array(
			'doctor' => array(
				'together' => true,
				'with' => array(
					'currentPlaceOfWork' => array(
						'together' => true
					)
				),
			)
		);
		$criteria->compare('currentPlaceOfWork.addressId', $model->id);
		$criteria->compare('t.status', 1);

		$comments = RaitingDoctorUsers::model()->findAll($criteria);

		$this->render('comments_experts', array(
			'model' => $model,
			'comments' => $comments
		));
	}

	public function actionComment($link = null, $companyLink = null, $linkUrl = null) {
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}

		$comment = new Comment;

		if (isset($_POST['Comment'])) {
			$comment->attributes = $_POST['Comment'];

			$comment->userId = Yii::app()->user->model->id;
			$comment->addressId = $model->id;

			if ($comment->save())
				$this->redirect('/user/comments');
		}

		$this->render('comment', array(
			'model' => $model,
			'comment' => $comment,
		));
	}

	public function actionFavorite($link = null, $companyLink = null, $linkUrl = null) {

		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}



		$favorite = Favorite::model()->findByAttributes(array(
			'usersId' => Yii::app()->user->model->id,
			'ownerId' => $model->id,
		));

		if ($favorite) {
			$favorite->delete();
			echo CJSON::encode(array(false, 'В избранное'));
		} else {
			$favorite = new Favorite();

			$favorite->usersId = Yii::app()->user->model->id;
			$favorite->ownerId = $model->id;
			#$favorite->id = md5($favorite->usersId . $favorite->ownerId);

			$favorite->save();
			echo CJSON::encode(array(true, 'Удалить'));
		}
	}

	public function actionMap($link = null, $companyLink = null, $linkUrl = null) {
		Yii::app()->clientScript->reset();
		Yii::app()->clientScript->registerCoreScript('yandex');
		$this->layout = "//layouts/empty";

		Address::$showInactive = true;
		Company::$showRemoved = true;
		if ($companyLink && $linkUrl) {
			$model = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
		} else {
			$model = $this->loadModel($link);
		}
		$phone = Phone::model()->findAll('addressId = :id', array('id' => $model->id));
		$phone_arr = array();
		foreach ($phone as $ph) {
			$phone_arr[] = $ph->name;
		}

		$phone_name = "<nobr>";
		if (count($phone_arr)) {
			if (count($phone_arr) != 1) {
				$phone_name.=implode('</nobr>, <nobr>', $phone_arr);
			} else {
				$phone_name.=$phone_arr[0];
			}
		}
		$phone_name.="</nobr>";



		$json = array();
		$json[] = array(
			'id' => $model->id,
			'name' => $model->company->name,
			'address' => $model->name,
			'lat' => $model->latitude,
			'long' => $model->longitude,
			'url' => $model->company->linkUrl.'/'.$model->linkUrl,
			'phone' => $phone_name
		);
		$json = CJSON::encode($json);

		$coord = CJSON::encode(array(
					'lat' => $model->latitude,
					'long' => $model->longitude,
		));

		if (Yii::app()->request->isAjaxRequest) {
			$this->render("map", array(
				'data' => $json,
				'coord' => $coord
			));
		} else {
			//$url = preg_replace("/(\/map$|\/map\/.*)/", "", MyTools::url_origin($_SERVER));
			//$this->redirect($url);
			$this->redirect(['clinic/view', 'linkUrl' => $model->linkUrl, 'companyLink' => $model->company->linkUrl]);
		}
	}

	public function actionAddresses($companyLink = null, $link = null) {
		Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', CClientScript::POS_END);
		Yii::app()->clientScript->registerCoreScript('yandex');
		
		$search = new Search();
		
		if ($companyLink) {
			$company = Company::model()->find('t.linkUrl = :lnk', [
				':lnk' => $companyLink,
				#':cityId' => $search->cityId
			]);
			
			if (!$company) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}

			$tempAddressShowInactive = Address::$showInactive;
			Address::$showInactive = true;
			
			$criteria = new CDbCriteria();	
			$criteria->with = [
				'company' => [
					'together' => true,
				]
			];
			$criteria->compare('company.linkUrl', $companyLink);
			$criteria->compare('t.cityId',$search->cityId);
			$addresses = Address::model()->findAll($criteria);
			
			if($addresses[0]->link) {
				$model = $this->loadModel($company->addresses[0]->link);
			} else {
				$model = new stdClass();
				$model->{"company"} = $company;
				$this->_model = $model;
				$this->layout = '//layouts/card_layout';
				$showCap = true;
			}
			UpdateCounters::updateClinicCounter($companyLink);
			$addr = $model->company->address;
			if($addr->cityId !== $search->cityId) {
				$addr = $addresses[0];
				//foreach ($addresses as $addressItem) {
				//	if($addressItem->cityId === $search->cityId) {
				//		$addr = $addressItem;
				//		break;
				//	}
				//}
			}
			Address::$showInactive = $tempAddressShowInactive;
			if(empty($addr->linkUrl) OR empty($model->company->linkUrl)) {
				throw new CHttpException(404, 'The requested page does not exist.');
			} else {
				$redirectAddress = MyTools::getKlinikaRedirectAddress(['companyLink' => $model->company->linkUrl, 'addressLinkUrl' => $addr->linkUrl]);
				//$redirectAddress = "";
				//if($addr->cityId !== $search->cityId) {
				//	$redirectUrl = City::getRedirectUrl($addr->city->subdomain, $addr->samozapis, "");
				//	$redirectAddress = $redirectUrl . "/" . "klinika" . "/" . $model->company->linkUrl . "/" . $addr->linkUrl;
				//} else {
				//	$redirectAddress = MyTools::getKlinikaRedirectAddress(['companyLink' => $model->company->linkUrl, 'addressLinkUrl' => $addr->linkUrl]);
				//}
				$this->redirect($redirectAddress, TRUE, 301);
			}
		} else {
			$model = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'companyLink' => $model->company->linkUrl]);
			}
			#$company = $model->company;
		}

		
		if(empty($showCap)) {
			$criteria = new CDbCriteria();
			$criteria->compare("cityId", $search->cityId);
			$criteria->compare("ownerId", $company->id);
			$criteria->order = "name ASC";
			
			$addresses = Address::model()->findAll($criteria);
			
			$arr = [];
			$json = [];
			foreach ($addresses as $adr) {
				$json[] = array(
					'id' => $adr->id,
					'name' => $adr->company->name,
					'address' => $adr->name,
					'lat' => $adr->latitude,
					'long' => $adr->longitude,
					'url' => $adr->company->linkUrl.'/'.$adr->linkUrl,
						//'phone' => $phone_arr[$adr->id]
				);

				$arr[] = $adr->id;
			}
			$data = CJSON::encode($json); 
			$coord = CJSON::encode(array(
						'lat' => '59.938',
						'long' => '30.313',
			));

			$this->render('addresses', array(
				'company' => $company,
				'addresses' => $addresses,
				'data' => $data,
				'coord' => $coord
			));
		} else {
			$criteria = new CDbCriteria();
			$criteria->with = [				
				'addresses' => [
					'together' => true,
					'with' => [
						'userMedicals' => [
							'together' => true,
						],
						'metroStations' => [
							'together' => true,
						]
					]
				],
			];
			$criteria->group = 't.id';
			$criteria->order = 't.rating DESC';
			$criteria->limit = 4;
			$criteria->compare('userMedicals.agreementNew',1);
			$capData = Company::model()->findAll($criteria);
			$this->render('addresses', array(
				'company' => $company,
				'capData' => $capData,
				'showCap' => true,				
			));
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 * @return Address
	 */
	public function loadModel($link) {
		$this->_model = Address::model()->findByLink($link);		
		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */

	public function getLeftMenu() {
		$menuItems = array(
			array(
				'label' => $this->_model->company->logo ?
						CHtml::image($this->_model->company->logoUrl, $this->_model->company->name, array('class' => 'company-logo')) :
						CHtml::image($this->assetsImageUrl . '/icon_emp_med.jpg', '', array('class' => 'company-logo no-logo default'))
			)
		);

		//Не гос медицинская клиника
		//Есть контракт
		//Тип контракта с кнопкой
		/**/
		if ($this->_model->userMedicals->agreementNew == 1 && $this->_model->isActive && !in_array($this->_model->company->companyType->id, $this->arrGMU) && !in_array($this->_model->company->companyType->id, $this->CompanyTypesNotOnline)) {
			$menuItems[] = array(
				'label' => 'Записаться на прием',
				'url' => array($this->createUrl('view'), '#' => 'info', 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
				'itemOptions' => array(
				),
				'linkOptions' => array(
					'class' => 'btn-green',
					'style' => 'text-align: center; width: 145px;',
				)
			);
		} else {
			$menuItems[] = array(
					'label' => 'ЗАПИСЬ НЕДОСТУПНА',
					'url' => array($this->createUrl('view'), '#' => 'info', 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
					'itemOptions' => array(
					),
					'linkOptions' => array(
							'class' => 'btn btn-red',
							'style' => 'text-align: center; width: 145px;',
							'onclick' => 'return false;'
					)
			);
			$menuItems[] = array('template' => '<div style="margin-top: 10px; margin-left: 37px; margin-right: 32px; text-align: left;">К сожалению, запись на прием в эту клинику через Единый Медицинский Портал отсутствует.
			</div>');
			if (!empty(Yii::app()->request->cookies['clinic' . $this->_model->company->link])) {
				$menuItems[] = array('template' => '<div class="report-clinic" style="font: inherit; margin-top: 10px; margin-left: 37px; text-align: left;">
					Вы пациент?
					<br>
						<span data-id="'.$this->_model->company->link.'">
							<a href="#" onclick="return false;">Пожаловаться на отсутствие клиники</a>
						</span>				
				</div>');
			}
			$menuItems[] = array('template' => '<div style="margin-top: 10px; margin-left: 37px; margin-right: 32px; text-align: left;">Вы представитель клиники?
			<p><a href="/site/contact">Станьте партнером</a></p></div>');
		}
		
		$menuItems[] = array('template' => '<br>');

		foreach ($this->menu as $key => $value) {
			$menuItems[] = array(
				'label' => $key,
				'url' => array($this->createUrl($value), 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}

		if (!empty($this->_model->doctors)) {
			$menuItems[] = array(
				'label' => 'Специалисты',
				'url' => array($this->createUrl('view'), '#' => 'doctors', 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}
		$criteria = new CDbCriteria;
		$criteria->with = [
			'addressServices' => [
				'with' => [
					'companyActivite' => [
						'joinType' => 'INNER JOIN'
					]
				]
			],
		];
		$criteria->compare('t.id', $this->_model->id);
		$prices = Address::model()->findAll($criteria);

		if (!empty($prices)) {
			$menuItems[] = array(
				'label' => 'Стоимость услуг',
				'url' => array($this->createUrl('view'), '#' => 'info', 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}
		/* if (!empty($this->_model->comments)) { */
		$menuItems[] = array(
			'label' => 'Отзывы',
			'url' => array($this->createUrl('comments'), 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
			'itemOptions' => array(
				'class' => 'btn w100'
			),
		);
		/* } */



		$photos = $this->_model->galleryBehavior->getGalleryPhotos('gallery');
		if (!empty($photos) && count($photos)) {
			$menuItems[] = array(
				'label' => 'Фотогалерея',
				'url' => array($this->createUrl('view'), '#' => 'gallery', 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}
		$actions = Action::model()->published()->active()->findAll('addressId=:id', array(
			'id' => $this->_model->id
		));
		if (!empty($actions)) {
			$menuItems[] = array(
				'label' => 'Акции',
				'url' => array($this->createUrl('actions'), 'companyLink' => $this->_model->company->linkUrl, 'linkUrl' => $this->_model->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}
		return $menuItems;
	}
	
	public function createSeoTitle($clinic, $companyWithManyAddresses = null) {
		
		if ($clinic->seoTitle != null)
			return $clinic->seoTitle;
		
		$pageSeoTitle;
		$pageSeoTitle .= 'Клиника ' . str_replace('Клиника', '', $clinic->company->seoName);
		//$pageSeoTitle .= $clinic->company->companyType->name;
		if ($companyWithManyAddresses) {
			$pageSeoTitle .= (!empty($clinic->nearestMetroStation->name) ? " у м. " . $clinic->nearestMetroStation->name : '');
			$pageSeoTitle .= ' по адресу ' .  $clinic->shortName;
		}
		$pageSeoTitle .= ' — запись онлайн, отзывы, врачи';
		//$pageSeoTitle .= ' - Единый Медицинский Портал';
		return $pageSeoTitle;
	}
	
	public function createSeoDescription($clinic, $companyWithManyAddresses = null) {
		$pageSeoDescription;
		$pageSeoDescription .= $clinic->company->seoName . ' - ';
		$pageSeoDescription .= $clinic->company->companyType->name;
		if ($companyWithManyAddresses) {
			$pageSeoDescription .= (!empty($clinic->nearestMetroStation->name) ? " у м. " . $clinic->nearestMetroStation->name : '');
			$pageSeoDescription .= ' по адресу ' .  $clinic->shortName;
		}
		$pageSeoDescription .= '. ';
		$pageSeoDescription .= 'Отзывы пациентов о клинике. Записаться на прием.'; // . $clinic->company->seoName;
		return $pageSeoDescription;
	}
	
}
