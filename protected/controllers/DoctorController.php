﻿<?php

class DoctorController extends LeftMenuController {
	#public $layout = '//layouts/card_layout';

	public $searchbox_type = SearchBox::S_TYPE_PATIENT;
	public $searchbox_tab = Search::S_DOCTOR;
	public $menu = array(
		"Описание" => 'clinic/view',
			/* "Специалисты"		 => 'clinic/experts',
			  "Стоимость услуг"	 => 'clinic/price',
			  "Отзывы"			 => 'clinic/comments', */
			//"Фотогалерея"		 => 'clinic/foto',
			//"Акции"				 => 'clinic/action',
	);

	/**
	 *
	 * @var Doctor
	 */
	private $_model;

	public function filters() {
		return array(
			'accessControl',
			array('application.filters.PageLogger + view'),
			array('application.filters.PageContentsCache + view', 'cacheLifeTime' => '604800'),//кешируем на неделю
			array('application.filters.CheckExpirePhone + visit'),
			array('application.filters.CheckBlockedAppointment + visit'),
		);
	}

	public function accessRules() {
		return array(
			array('deny',
				'actions' => array('comment'),
				'users' => array('?'),
			),
		);
	}

	public function actionReviewForm()
	{
		$this->layout = NULL;
		$model=new Review;
	
		// uncomment the following code to enable ajax-based validation
		/*
		 if(isset($_POST['ajax']) && $_POST['ajax']==='review-_reviewForm-form')
		 {
		 echo CActiveForm::validate($model);
		 Yii::app()->end();
		 }
		 */
	
		if(isset($_POST['Review']))
		{
			$model->attributes=$_POST['Review'];
			if($model->validate())
			{
				// form inputs are valid, do something here
				return;
			}
		}
		$this->render('reviewForm',array('model'=>$model));
	}
	/*
	  public function actionIndex() {
	  $this->render('index');
	  } */
	
	public function actionView($link = null, $linkUrl = null, $companyLinkUrl = null, $addressLinkUrl = null) {
	
		if ($addressLinkUrl == 'shevchenko-229')
			throw new CHttpException(404, 'Not found');

		$this->layout = '//layouts/card_layout';
		Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', CClientScript::POS_END);
		Yii::app()->clientScript->registerCoreScript('jquery.maskedinput');
		Doctor::$filterDeleted = false;
		$showPicker = true;
		
		//смотрим, пришла заявка из виджета 2gis или нет
		$isWidget = Yii::app()->request->getParam('isWidget');

		$isVkApp = Yii::app()->request->getParam('vk');
		$isVkForm = Yii::app()->request->getParam('vkForm');
		if($isVkApp) {
			$vkGroupId = Yii::app()->session['vkGroupId'];
			$this->layout = '/layouts/main_vk';
		}

		//Не гос медицинская клиника
		//Есть контракт
		//Тип контракта с кнопкой
		#$add = Address::model()->
		if ($linkUrl) {
			$model = $this->loadModel($linkUrl, $companyLinkUrl, $addressLinkUrl);			
			if (empty($companyLinkUrl) || empty($addressLinkUrl)) {
				$this->redirect(['doctor/view', 'linkUrl' => $model->linkUrl, 'companyLinkUrl' => $model->currentPlaceOfWork->company->linkUrl, 'addressLinkUrl' => $model->currentPlaceOfWork->address->linkUrl]);
			}
		} else {
			$model = $this->loadModel($link);
			
			$doctorIsAvailable = $model->IsAvaialble;
			if (!isset($_REQUEST['JSON'])) {
				
				//Если врач не доступен (например: клиника удалена/адрес не активен/врач удален и пр.)
				//выбрасываем 404
				if (!$doctorIsAvailable) {
					throw new CHttpException(404, 'The requested page does not exist.');
				}
				$this->redirect([
					'doctor/view',
					'companyLinkUrl' => $model->currentPlaceOfWork->company->linkUrl,
					'addressLinkUrl' => $model->currentPlaceOfWork->address->linkUrl,
					'linkUrl' => $model->linkUrl
				]);
			}
			
			//if (!isset($_REQUEST['JSON'])) {
			//	$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $model->linkUrl, 'companyLinkUrl' => $model->currentPlaceOfWork->company->linkUrl, 'addressLinkUrl' => $model->currentPlaceOfWork->address->linkUrl]);
			//}
		}
		if(Yii::app()->request->getParam('onlyIds')) {
			$ids = [];
			if(is_object($model)) $ids[] = $model->id;
			echo CJSON::encode($ids);
			exit;
		}

		// Переадресация на поддомен нужного региона
		//$url = Yii::app()->createUrl('doctor/view',['linkUrl' => $linkUrl, 'companyLinkUrl' => $companyLinkUrl, 'addressLinkUrl' => $addressLinkUrl]);
		//$redirectUrl = City::getRedirectUrl($model->currentPlaceOfWork->address->city->subdomain, $model->currentPlaceOfWork->address->samozapis==1, $url);
		//if($redirectUrl !== false) $this->redirect($redirectUrl, true, '301');
		
		$url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		$url = explode('?', $url)[0]; //allow get parameters
		$possibleUrls = $model->pagesUrls;
		
		if (!Yii::app()->params['devMode'] && !in_array($url, $possibleUrls)) {
			if(count(array_intersect(["dev1","dev2","dev3","dev4","dev5","dev6","dev7","dev8","dev9","spb","local"], explode('.', $_SERVER['HTTP_HOST']))) < 1) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		
		if ($this->_model->deleted) {
			$doctorDeleted = 1;
		}
		$company = $this->_model->currentPlaceOfWork->address->company;

		$companyActivites = [];
		foreach ($model->specialtyOfDoctors as $ds) {
			$companyActivites[] = $ds->doctorSpecialty->companyActivite->id;
		}

		$services = []; /*AddressServices::model()->findAll([
			'condition' => "addressId = '{$model->currentPlaceOfWork->address->id}' AND companyActivitesId IN ('" . implode("', '", $companyActivites) . "')",
			'with' => [
				'service' => [
					'together' => true
				]
			],
			'order' => 'service.name ASC',
		]);*/


		$data = Yii::app()->db->createCommand()
						->select("*,count(*) as count")
						->from(AddressServices::model()->tableName())
						->where("companyActivitesId <> '$noDataId' AND addressId ='{$model->currentPlaceOfWork->address->id}' AND companyActivitesId IN ('" . implode("', '", $companyActivites) . "')")
						->group('companyActivitesId')->queryAll();

		foreach ($data as $row) {
			if ($row['companyActivitesId'])
				$countServices[$row['companyActivitesId']] = $row['count'];
		}

		if (Yii::app()->user->isGuest) {
			//Если пользователь гость - выводим сообщение о возможности зарегистрироваться
			//Yii::app()->user->setFlash('askAuth', 'Уважаемый гость, вы можете зарегистрироваться на нашем портале. После регистрации Вы получите доступ в личный кабинет, в котором Вы сможете: <ul>'
			//		. '<li>Отследить историю посещения клиник и врачей</li>'
			//		. '<li>Оставить отзыв о медицинском учреждении</li>'
			//		. '<li>Подписаться на рассылку о новых услугах и акциях клиник, Единого медицинского портала и др.</li>'
			//		. '</ul>'
			//		. '<div style="text-align:center"><span onclick="location.href=\'' . $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('goto_register' => 1, 'link' => $link)) . '\'" class="btn-green">Зарегистрироваться</span> <span id="closeAuthBlock" class="btn-red">Скрыть блок</span></div>');
		} else {
			//Если пользователь зарегистрирован - проверяем есть ли у него подтвержденный телефон для записи на 1 раз
			$pv = PhoneVerification::model()->find("status=:status AND userId = :userId", array(
				':status' => PhoneVerification::APPOINTMENT,
				':userId' => Yii::app()->user->model->id,
			));
		}
		
		$addr = Address::model()->findByAttributes(array(
			'linkUrl' => $addressLinkUrl,
			'ownerId' => Company::model()->findByAttributes(['linkUrl'=>$companyLinkUrl])->id
		));
		
		
		if ($addr->linkUrl != $model->currentPlaceOfWork->address->linkUrl) {
			Yii::app()->clientScript->registerLinkTag('canonical', null, Yii::app()->createAbsoluteUrl('doctor/view', [
						'companyLinkUrl' => $model->currentPlaceOfWork->address->company->linkUrl,
						'addressLinkUrl' => $model->currentPlaceOfWork->address->linkUrl,
						'linkUrl' => $model->linkUrl
			]));
		}
		$appointmentType = 'visit_to_doctor';
		$extDoctor = ExtDoctor::checkDoctor($model->id, $addr->id);
		if($extDoctor) {
			$loader = $extDoctor->sysType->getLoader();
			if(is_object($loader)) {
				$appointmentType = 'visit_to_' . preg_replace('/(loader)/', '', mb_strtolower(get_class($loader)));
			}
		}
		$appointment = new AppointmentToDoctors($appointmentType);
		$appointment->doctor = $model;
		$appointment->company = $addr->company;
		$appointment->address = $addr;
		$appointment->name = Yii::app()->user->model->name;
		$appointment->phone = Yii::app()->user->model->telefon;
		$appointment->email = Yii::app()->user->model->email;
		if($isVkApp) {
			$appointment->sourcePageUrl = 'Группа: '.$vkGroupId;
		}
		$appointment->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;


		#$time = WorkingHour::getDatesForCal($appointment->address->link, null, null, $appointment->doctor->link);
		if (Yii::app()->request->getParam('removeGuestPhone')) {
			if (Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll("phone=:phone AND (status=:stat1 or status=:stat2)", array(
					"phone" => Yii::app()->session['appointment_phone_set'],
					"stat1" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT,
				));
				Yii::app()->session['appointment_phone_set'] = "";
			}
			$this->redirect(array("/doctor/view",
				'linkUrl' => $linkUrl
			));
		}

		if (Yii::app()->request->getParam(get_class($appointment))) {
			$appointment->attributes = Yii::app()->request->getParam(get_class($appointment));
			$appointment->createdDate = date('Y-m-d H:i:s');
			$appointment->userId = Yii::app()->user->id;
			$appointment->statusId = AppointmentToDoctors::SEND_TO_REGISTER;
			$appointment->visitType = AppointmentToDoctors::VISIT_TYPE_DOCTOR;

			$lnk = Yii::app()->request->getParam(get_class($appointment))['address']['link'];
			if (!empty($lnk)) {
				$addr = Address::model()->findByLink($lnk);
				if ($addr->company->id == $appointment->company->id) {
					$appointment->address = $addr;
				}
			}

			if(!empty($appointment->phone)) {
				/* Если гость ввел номер который указан у пользователя, привязываем запись к аккаунту */
				if (Yii::app()->user->isGuest) {
					$userModel = User::model()->findByAttributes([
						'telefon' => $appointment->phone
					]);
					if ($userModel) {
						$appointment->userId = $userModel->id;
					}
				}
				/* Если не гость ввел номер, при записи, сохраняем в аккаунт */
				else {
					if (empty(Yii::app()->user->model->telefon) && !User::userWithThisPhoneExists($appointment->phone)) {
						$userModel = Yii::app()->user->model;
						$userModel->telefon = $appointment->phone;
						$userModel->save();
					}
				} /*  */
			}

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'doctor-visit') {
				$arrData = CJSON::decode(CActiveForm::validate($appointment));
				if(empty($appointment->extAppointmentId)) {
					$request_appointment_phone = strval(trim($_REQUEST[get_class($appointment)]['phone']));
					$request_phoneInput = strval(trim($_REQUEST['phoneInput']));
					if ($request_appointment_phone !== $request_phoneInput) {
						$arrData['AppointmentToDoctors_phone'][0] = 'Телефон не активирован';
						unset($arrData['AppointmentToDoctors_phone'][1]);
					}
					//если при первой валидации не назначен noVerification, проверяем и выдаем ответ 
					if ($_REQUEST['noVerification'] == "") {
						//if (User::userWithThisPhoneExists($_REQUEST[get_class($appointment)]['phone'])) {
						//	$arrData['AppointmentToDoctors_noVerification'] = 0;
						//}
					}
				}
				if(count($arrData) === 0 && is_object($loader)) {
					if(empty($appointment->extAppointmentId)) {
						$arrData["external_visit"] = [
							"success" => false,
							"text" => "Время приёма не выбрано",
						];
					} else {
						$referralId = @Yii::app()->request->cookies['OMSForm_referralId'];
						$extAppResponse = $loader->setAppointment($appointment->extAppointmentId, $extDoctor->extAddressId, $appointment->extPatId, $referralId);
						if($extAppResponse->SetAppointmentResult->Success) {
							if(isset($extAppResponse->SetAppointmentResult->AppointmentId)) {
								$appointment->extAppointmentId = $extAppResponse->SetAppointmentResult->AppointmentId;
							}
							if(isset($extAppResponse->SetAppointmentResult->IdHistory)&&!empty($extAppResponse->SetAppointmentResult->IdHistory)){
								$notificationAttr['IdHistory'] = $extAppResponse->SetAppointmentResult->IdHistory;
							}
							$notificationAttr = [
								'idLpu' => $extDoctor->extAddressId,
								'appointment' => new SoapVar(
													'<ns2:appointment xsi:type="ns1:Appointment" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.
														'<ns1:IdAppointment>'.$appointment->extAppointmentId.'</ns1:IdAppointment>'.
														'<ns1:VisitEnd>'.$_REQUEST['appointment']['VisitEnd'].'</ns1:VisitEnd>'.
														'<ns1:VisitStart>'.$_REQUEST['appointment']['VisitStart'].'</ns1:VisitStart>'.
													'</ns2:appointment>', XSD_ANYXML),
								'doctor' => [
									'IdDoc' => (string)$extDoctor->extId,
									'Name' => (string)$extDoctor->extFio,
								],
								'spesiality' => [
									'IdSpesiality' => (string)$extDoctor->extSpecialityId,
									'NameSpesiality' => (string)$extDoctor->extSpeciality->extName,
								],
								'patient' => [
									'Birthday' => $_REQUEST['patient']['Birthday']."T00:00:00.000", //datetime 1989-12-11T00:00:00.000
									'IdPat' => (string)$appointment->extPatId,
									'Name' => (string)$_REQUEST['patient']['Name'],
									'Surname' => (string)$_REQUEST['patient']['Surname'],
									'SecondName' => (string)$_REQUEST['patient']['SecondName'],
								],
							];
							$arrData['notificationAttr'] = $notificationAttr;
							$arrData['notificationResponse'] = $loader->SendNotificationAboutAppointment($notificationAttr);
							$arrData["external_visit"] = [
								"success" => true,
								"text" => "Вы записались на приём!",
								"plannedTime" => RuDate::post($appointment->plannedTime, true, false),
								"IdAppointment" => $appointment->extAppointmentId,
								"patient" => [
									'name' => $appointment->name,
									'birthday' => $_REQUEST['patient']['Birthday']
								],
								"doctor" => [
									'name' => $extDoctor->extFio,
									'spesiality' => $extDoctor->extSpeciality->extName,
								],
								"company" => [
									'name' => $appointment->company->name,
									'address' => 
										(!empty($appointment->address->postIndex) ? $appointment->address->postIndex.", " : "") .
										(!empty($appointment->address->street) ? $appointment->address->street : "") .
										(!empty($appointment->address->houseNumber) ? ", ".$appointment->address->houseNumber : ""),
								],
							];
							if (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) {
								$searchModel = new Search(Search::S_DOCTOR);
								$searchModel->initOMSForm();
								$attr = [
										'omsNumber' => $searchModel->OMSForm_omsNumber,
										'omsSeries' => $searchModel->OMSForm_omsSeries,
										'birthDate' => date('Y-m-d\TH:i:s',strtotime($searchModel->OMSForm_birthday)),
								];
								$emiasAppointmets = $loader->getAppointmentReceptionsByPatient($attr);
								if(isset($emiasAppointmets->return) && !is_array($emiasAppointmets->return)) {
									$emiasAppointmets->return = [$emiasAppointmets->return];
								}
								$doctorRoom;
								foreach ($emiasAppointmets->return as $value) {
									if($value->id == $appointment->extAppointmentId) {
										$doctorRoom = $value->appointmentReceptionToDoctor->room;
										break;
									}
								}
								$arrData["external_visit"]["doctor"]["room"] = $doctorRoom;
							}
							$appointment->sysTypeId = $extDoctor->sysTypeId;
							try { $appointment->save(); } catch (Exception $e) { }
						} else {
							$arrData["external_visit"] = [
								"success" => false,
								"text" => $extAppResponse->SetAppointmentResult->ErrorList->Error->ErrorDescription,
							];
							LogSamozapis::addLog($loader,$extAppResponse->SetAppointmentResult,$model->id);
						}
					}
				}
				echo CJSON::encode($arrData);
				Yii::app()->end();
			}
				
			
			if ($appointment->save()) {
				
				$storedAppointment = [
					'name' => $appointment->name,
					'phone' => $appointment->phone,
					'plannedTime' => $appointment->plannedTime,
					'companyName' => $appointment->company->name,
					'addressName' => $appointment->address->name,
					'doctorName' => $appointment->doctor->name,
					'hiddenField' => $appointment->plannedTime,
				];
				if ($isWidget == 1) {
					
					Yii::app()->session['storedAppointment'] = $storedAppointment;
					//$appointment->name = '2GIS '.$appointment->name;
				}
				
				/* Чистим таблицу PhoneVerification для временного номера */
				#if(!Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll('phone = :pId AND (status = :stat OR status = :stat2)', array(
					"pId" => $appointment->phone,
					"stat" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT
				));
				#}
				//Чистим сессию
				if (Yii::app()->user->isGuest) {
					Yii::app()->session['appointment_phone_set'] = "";
				}


				/* if ($notifyOperator) {

				  } */


				#if($appointment->phone != '+79213434502') {
				/* Уведомить штатного оператора о том, что произведена заявка в клинику */
				$appointment->notifyOperator();

				/* Оповещаем пользователя по смс и почте */
				$appointment->sendMailToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				if (Yii::app()->user->isGuest) {
					$appointment->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
				} else {
					$appointment->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				}
				/* Оповещаем клинику по смс и почте */
				$appointment->sendMailToClinic('new_record');
				$appointment->sendSmsToClinic('new_record');
				#}

				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(array('registrySuccess' => 'Запись успешно произведена'));
					Yii::app()->end();
				} else {
					
					if(!$isVkApp) {
	 					Yii::app()->user->setFlash('visited', 1);
						if (!Yii::app()->user->isGuest) {
							/* if ($notifyOperator) { */
							if ($isWidget == 1) {
								$this->redirect(array('/widget/success'));
							} else {
								Yii::app()->session['reachedGoal_appointment_as_old_user'] = 1;
								$this->redirect([
									'/user/registry',
									'filter' => 'all',
									//'_ym_debug' => '1'
								]);
							}
							/* } else {
							  $this->redirect(array('user/registry', 'link' => $doctor->link));
							  } */
						} else {

							/* create if account doesn't exists and autologin after */
							if (!$userModel) {
								$userModel = new User();
								$userModel->name = $appointment->name;
								$userModel->telefon = $appointment->phone;
								
								$guidString = '';
								if (function_exists('com_create_guid')){
									
									$guidString = com_create_guid();
								} else {
									
									mt_srand((double)microtime()*10000);//для php 4.2.0 и выше
									$charid = strtoupper(md5(uniqid(rand(), true)));
									$hyphen = chr(45);// "-"
									$guidString = chr(123)// "{"
										.substr($charid, 0, 8).$hyphen
										.substr($charid, 8, 4).$hyphen
										.substr($charid,12, 4).$hyphen
										.substr($charid,16, 4).$hyphen
										.substr($charid,20,12)
										.chr(125);// "}"
								}
								
								$newGenPassword = (string)substr(md5($guidString),0,6);							
								$userModel->password = $newGenPassword; //генерим пароль
								
								$userModel->phoneActivationDate = new CDbExpression('NOW()');
								$userModel->dateRegister = new CDbExpression('NOW()');
								$userModel->phoneActivationStatus = 1;
								$userModel->status = 1;
								$userModel->sexId = 'ae11c45d855a991340c5f59edcb404da';
								
								//сохраняем нового юзера и высылаем на телефон смс с паролем для доступа к ЛК
								if ($userModel->save(false)) {
								
									$sms = new SmsGate();
									$sms->phone = $userModel->telefon;
									$sms->setMessage('Для просмотра или изменения своей записи на прием авторизуйтесь на emportal.ru! Логин - '. $userModel->telefon .', пароль - '.$newGenPassword, array(
										'_code_' => $sms->code
									));
									$sms->send();
								}

								$appointment->userId = $userModel->id;
								$appointment->save(false);
							}
							
							self::login($userModel->telefon, $userModel->password);

							//Yii::app()->user->setFlash('visited', 1); //rm
							
							if ($isWidget == 1) {
								$this->redirect(array('/widget/success'));
							} else {
								Yii::app()->session['reachedGoal_appointment_as_old_user'] = 1;
								Yii::app()->session['reachedGoal_user_register'] = 1;
								$this->redirect([
									'/user/profile',
									//'_ym_debug' => '1'
								]);
							}
						}
					}
				}
			} else {
				if (isset($_REQUEST['JSON'])) {
					$errors = $appointment->errors;
					if ($_REQUEST[get_class($appointment)]['phone'] != "" && Yii::app()->user->isGuest) {
						$errors['phone'][] = 'ACTIVATE_PHONE';
					}

					echo CJSON::encode(array('registryError' => $errors));
					Yii::app()->end();
				}
			}
		}

		//$time = [
		//	WorkingHour::getDate($appointment->address->link, date('Y-m-d'), $appointment->doctor->link)
		//];
		$timeStub = array(
			[
				'date' => date('d.m.Y'),
				'time' => [
					[
						'text' => '14:00',
						'status' => 'allow',
					]
				],
				'msg' => "Рабочий"
			]
		);
		$time = $timeStub;
		
		$specArr = $searchArr = [];
		foreach ($model->specialties as $sp) {
			if (!in_array($sp->shortSpecialtyOfDoctor->id, $specArr)) {
				$specArr[$sp->shortSpecialtyOfDoctor->linkUrl] = $sp->shortSpecialtyOfDoctor->name;
			}
		}

		$reviews = Review::model()->findAllByAttributes(['doctorId' => $model->id, 'statusId' => 2], "reviewText != '' AND reviewText IS NOT NULL");

		$this->render('view', array(
			'model' => $model,
			'company' => $company,
			'addr' => $addr,
			'countServices' => $countServices,
			'pv' => $pv,
			'services' => $services,
			'doctorDeleted' => $doctorDeleted,
			'showPicker' => $showPicker,
			'appointment' => $appointment,
			'specArr' => $specArr,
			'time' => $time,
			'similar' => $similar,
			'reviews' => $reviews,
		));
	}

	public function actionVisit($link = null, $linkUrl = null) {
		/* ищем модель */
		if ($linkUrl) {
			$doctor = $this->loadModel($linkUrl);
			$doctorIsAvailable = $doctor->IsAvaialble;
			
			if (!$doctorIsAvailable) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
			
			$this->redirect([
				'doctor/view',
				'companyLinkUrl' => $doctor->currentPlaceOfWork->company->linkUrl,
				'addressLinkUrl' => $doctor->currentPlaceOfWork->address->linkUrl,
				'linkUrl' => $doctor->linkUrl
			]);
		} else {
			$mdl = $this->loadModel($link);
			$doctorIsAvailable = $mdl->IsAvaialble;
			if (!isset($_REQUEST['JSON'])) {
				
				//Если врач не доступен (например: клиника удалена/адрес не активен/врач удален и пр.)
				//выбрасываем 404
				if (!$doctorIsAvailable) {
					throw new CHttpException(404, 'The requested page does not exist.');
				}
				//$this->redirect([
				//	'doctor/view',
				//	'companyLinkUrl' => $mdl->currentPlaceOfWork->company->linkUrl,
				//	'addressLinkUrl' => $mdl->currentPlaceOfWork->address->linkUrl,
				//	'linkUrl' => $mdl->linkUrl
				//]);
			}
			$doctor = $mdl;
		}
		if (!isset($_REQUEST['JSON'])) {
			$this->redirect([
				'doctor/view',
				'companyLinkUrl' => $doctor->currentPlaceOfWork->company->linkUrl,
				'addressLinkUrl' => $doctor->currentPlaceOfWork->address->linkUrl,
				'linkUrl' => $doctor->linkUrl
			]);
		}
		$this->pageTitle = "Единый медицинский портал - Запись на прием к врачу - " . $doctor->name;
		/* подключаем masked input */
		Yii::app()->clientScript->registerCoreScript('jquery.maskedinput');




		/* если человека нажал кнопку goto_register, то сохраняем урл и кидаем на регистрацию */
		if (Yii::app()->request->getParam('goto_register')) {
			Yii::app()->session['returnUrl'] = $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('link' => $link));
			$this->redirect('/register');
		}

		/* Если гость нажал кнопку удалить телефон на 1 запись и обновляем страницу */
		if (Yii::app()->request->getParam('removeGuestPhone')) {
			if (Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll("phone=:phone AND (status=:stat1 or status=:stat2)", array(
					"phone" => Yii::app()->session['appointment_phone_set'],
					"stat1" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT,
				));
				Yii::app()->session['appointment_phone_set'] = "";
			}
			$this->redirect(array("/doctor/visit",
				'link' => $link
			));
		}



		//Если пользователь гость - выводим сообщение о возможности зарегистрироваться
		//Если пользователь зарегистрирован - проверяем есть ли у него подтвержденный телефон (измененный) для записи на 1 раз
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash('askAuth', 'Уважаемый гость, вы можете зарегистрироваться на нашем портале. После регистрации Вы получите доступ в личный кабинет, в котором Вы сможете: <ul>'
					. '<li>Отследить историю посещения клиник и врачей</li>'
					. '<li>Оставить отзыв о медицинском учреждении</li>'
					. '<li>Подписаться на рассылку о новых услугах и акциях клиник, Единого медицинского портала и др.</li>'
					. '</ul>'
					. '<div style="text-align:center"><span onclick="location.href=\'' . $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('goto_register' => 1, 'link' => $link)) . '\'" class="btn-green">Зарегистрироваться</span> <span id="closeAuthBlock" class="btn-red">Скрыть блок</span></div>');
		} else {
			$pv = PhoneVerification::model()->find("status=:status AND userId = :userId", array(
				':status' => PhoneVerification::APPOINTMENT,
			':userId' => Yii::app()->user->model->id,
			));
 		}

		$model = new AppointmentToDoctors('visit_to_doctor');


		/*
		 * showPicker True - старый вариант, сейчас всегда включено, вынесено в самый верх как настройка 
		 * Показываем выбор времени для записи на прием, если есть личный кабинет, сейчас если нет личного кабинета пользователь не должен иметь возможности зайти сюда 
		 * Атавизм
		 */
		$showPicker = true;
		$LKK = UserMedical::model()->findByAttributes([
			'addressId' => $doctor->currentPlaceOfWork->addressId
		]);

		if (empty($LKK)) {
			$showPicker = false;

			#$model->scenario = 'visit_no_contract';

			/* $notifyOperator = true; */

			if (!empty($debug)) {
				$showPicker = true;
			}
		}

		$model->doctor = $doctor;
		$model->company = $doctor->currentPlaceOfWork->company;
		$model->address = $doctor->currentPlaceOfWork->address;
		$model->name = Yii::app()->user->model->name;
		$model->phone = Yii::app()->user->model->telefon;
		$model->email = Yii::app()->user->model->email;
		$model->visitType = AppointmentToDoctors::VISIT_TYPE_DOCTOR;
		$model->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;

		if (Yii::app()->request->getParam(get_class($model))) {
			$model->attributes = Yii::app()->request->getParam(get_class($model));
			$model->createdDate = date('Y-m-d H:i:s');
			$model->userId = Yii::app()->user->id;
			$model->statusId = AppointmentToDoctors::SEND_TO_REGISTER;

			$lnk = Yii::app()->request->getParam(get_class($model))['address']['link'];
			if (!empty($lnk)) {
				$addr = Address::model()->findByLink($lnk);
				if ($addr->company->id == $model->company->id) {
					$model->address = $addr;
				}
			}

			/* Если гость ввел номер который указан у пользователя, привязываем запись к аккаунту */
			/* Если не гость ввел номер при реге, сохраняем в аккаунт */
			if (Yii::app()->user->isGuest) {
				$userModel = User::model()->findByAttributes([
					'telefon' => $model->phone
				]);
				if ($userModel) {
					$model->userId = $userModel->id;
				}
			} else {
				if (empty(Yii::app()->user->model->telefon)) {
					$userModel = Yii::app()->user->model;
					$userModel->telefon = $model->phone;
					$userModel->save();
				}
			}

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'doctor-visit') {
				$arrData = CJSON::decode(CActiveForm::validate($model));
				if ($_REQUEST[get_class($model)]['phone'] == "" && $_REQUEST['phoneInput'] != "") {
					$arrData['AppointmentToDoctors_phone'][0] = 'Телефон не активирован';
					unset($arrData['AppointmentToDoctors_phone'][1]);
				}
				echo CJSON::encode($arrData);
				Yii::app()->end();
			}

			if ($model->save()) {
				/* Чистим таблицу PhoneVerification для временного номера */
				#if(!Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll('phone = :pId AND (status = :stat OR status = :stat2)', array(
					"pId" => $model->phone,
					"stat" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT
				));
				#}
				//Чистим сессию
				if (Yii::app()->user->isGuest) {
					Yii::app()->session['appointment_phone_set'] = "";
				}


				/* if ($notifyOperator) {

				  } */


				#if($model->phone != '+79213434502') {
				/* Уведомить штатного оператора о том, что произведена заявка в клинику */
				$model->notifyOperator();

				/* Оповещаем пользователя по смс и почте */
				if (Yii::app()->user->isGuest) {
					$model->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
				} else {
					$model->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				}
				/* Оповещаем клинику по смс и почте */
				$model->sendMailToClinic('new_record');
				$model->sendSmsToClinic('new_record');
				#}

				if (isset($_REQUEST['JSON'])) {
					
					if (!$userModel) {
						$userModel = new User();
						$userModel->name = $model->name;
						$userModel->telefon = $model->phone;
						
						$guidString = '';
						if (function_exists('com_create_guid')){
							$guidString = com_create_guid();
						} else {
							mt_srand((double)microtime()*10000);//для php 4.2.0 и выше
							$charid = strtoupper(md5(uniqid(rand(), true)));
							$hyphen = chr(45);// "-"
							$guidString = chr(123)// "{"
								.substr($charid, 0, 8).$hyphen
								.substr($charid, 8, 4).$hyphen
								.substr($charid,12, 4).$hyphen
								.substr($charid,16, 4).$hyphen
								.substr($charid,20,12)
								.chr(125);// "}"
						}
						
						$newGenPassword = (string)substr(md5($guidString),0,6);							
						$userModel->password = $newGenPassword; //генерим пароль
						
						$userModel->phoneActivationDate = new CDbExpression('NOW()');
						$userModel->dateRegister = new CDbExpression('NOW()');
						$userModel->phoneActivationStatus = 1;
						$userModel->status = 1;
						$userModel->sexId = 'ae11c45d855a991340c5f59edcb404da';
						
						//сохраняем нового юзера и высылаем на телефон смс с паролем для доступа к ЛК
						if ($userModel->save(false)) {
						
							$sms = new SmsGate();
							$sms->phone = $userModel->telefon;
							$sms->setMessage('Для просмотра или изменения своей записи на прием авторизуйтесь на emportal.ru! Логин - '. $userModel->telefon .', пароль - '.$newGenPassword, array(
								'_code_' => $sms->code
							));
							//$sms->send();
						}

						$model->userId = $userModel->id;
						$model->save(false);
					}
					
					self::login($userModel->telefon, $userModel->password);
					
					echo CJSON::encode(array('registrySuccess' => 'Запись успешно произведена'));
					Yii::app()->end();
				} else { //этот кусок не используется мобильным приложением
					
					if (!Yii::app()->user->isGuest) {
						
						$this->redirect(array('/user/profile', 'filter' => 'all'));
					} else {
						
						/* create if account doesn't exists and autologin after */
						if (!$userModel) {
							$userModel = new User();
							$userModel->name = $model->name;
							$userModel->telefon = $model->phone;
							$userModel->password = '';
							$userModel->phoneActivationDate = new CDbExpression('NOW()');
							$userModel->dateRegister = new CDbExpression('NOW()');
							$userModel->phoneActivationStatus = 1;
							$userModel->status = 1;
							$userModel->sexId = 'ae11c45d855a991340c5f59edcb404da';
							$userModel->save(false);

							$model->userId = $userModel->id;
							$model->save(false);
						}

						self::login($userModel->telefon, $userModel->password);

						Yii::app()->user->setFlash('visited', 1);
						
						//$this->redirect(array('/user/profile'));
						$this->redirect(array('/user/profile', 'filter' => 'all'));
						
						##dont work here after redirect
						//полагаю код ниже можно удалить

						$pollCriteria = new CDbCriteria();
						$pollCriteria->scopes = 'afterVisit';
						#$pollCriteria->addInCondition('t.id', []);
						$polls = Poll::model()->afterVisit()->findAll($pollCriteria);
						$text = "							
						<p>Ваша заявка на прием успешно отправлена в медицинское учреждение!</p>
						
						Информация по записи:
						<ul style=\"margin-top:0px;\">
							<li>Учреждение: " . $model->company->name . "</li>
							<li>Время приема: " . $model->registryPlannedTime . "</li>						
							<li>ФИО специалиста: " . $model->doctor->name . "</li>
						</ul>					
						
						<p>Как только специалисты клиники обработают вашу заявку, 
						вы получите подтверждение в виде смс или звонка представителя клиники. 
						Для обеспечения высокого качества обслуживания операторы 
						Единого Медицинского Портала будут контролировать процесс обработки вашей заявки сотрудниками клиники.</p>
						
						<p>А пока просим вас поделиться своими впечатлениями.</p>
						<div class=\"pollAppointment\">";
						foreach ($polls as $poll) {
							$text.="<div class=\"one-poll\">							
										<div class=\"one-poll-text\">" . $poll->question . "</div>
										" . CHtml::radioButtonList($poll->id, '', CHtml::listData($poll->pollOptions, 'id', 'pollOption')) . "
									</div>";
						}

						$text.=
								"</div>
						<a href=\"" . $this->createUrl('site/register') . "\">Регистрируйтесь</a> на нашем сайте и получайте еще больше возможностей: 						
						<ul style=\"margin-top:0px;\">
							<li>Просмотр истории посещения клиник;</li>
							<li>Уведомления о скидках и акциях;</li>
							<li>Публикация отзывов, а также оценка клиник и врачей.</li>	
						</ul>
						
						<p>Будем признательны, если после приема вы оставите отзыв о медицинском учреждении и специалисте на нашем сайте.</p>
						<a onclick=\"sendPoll(); return true;\" class=\"btn btn-green\" href=\"/\">Далее</a>
						";

						Yii::app()->user->setFlash('confirmedVisit', $text);
						$this->redirect(array('/site/confirmedVisit'));
					}
				}
			} else {
				if (isset($_REQUEST['JSON'])) {
					$errors = $model->errors;
					if ($_REQUEST[get_class($model)]['phone'] != "" && Yii::app()->user->isGuest) {
						$errors['phone'][] = 'ACTIVATE_PHONE';
					}

					echo CJSON::encode(array('registryError' => $errors));
					Yii::app()->end();
				}
			}
		}

		$timeBlock = WorkingHour::getDatesForCal($model->address->link, null, null, $model->doctor->link);

		$this->render('visit', array(
			'model' => $model,
			'pv' => $pv,
			'time' => $timeBlock
		));
	}

	public function actionPrice($link = null, $linkUrl = null) {
		$activity = Yii::app()->request->getParam('activity', false);


		if ($linkUrl) {
			$doctor = $this->loadModel($linkUrl);
			$this->redirect(['doctor/view', 'linkUrl' => $doctor->linkUrl, 'companyLinkUrl' => $doctor->currentPlaceOfWork->company->linkUrl, 'addressLinkUrl' => $doctor->currentPlaceOfWork->address->linkUrl]);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'activity' => $activity, 'linkUrl' => $mdl->linkUrl]);
			}
		}

		$activity = CompanyActivite::model()->findByAttributes(array('link' => $activity));
		$prices = array();

		if ($activity) {
			$criteria = new CDbCriteria();

			$criteria->with = array(
				'service',
				'address'
			);
			$criteria->compare('service.companyActiviteId', $activity->id);
			$criteria->compare('address.link', $doctor->currentPlaceOfWork->address->link);
			$prices = AddressServices::model()->findAll($criteria);
		}
		$addressCriteria = new CDbCriteria();
		$addressCriteria->compare("t.link", $doctor->currentPlaceOfWork->address->link);
		$addressCriteria->order = "ca.name ASC";
		$addressCriteria->with = [
			'companyActivites' => [
				'together' => true,
				'with' => [
					'companyActivites' => [
						'alias' => 'ca'
					]
				]
			]
		];
		$model = Address::model()->find($addressCriteria);
		#$model = $this->loadModel($link);

		$noDataId = Yii::app()->db->createCommand()
				->select("id")
				->from(CompanyActivite::model()->tableName())
				->where("name = 'Нет данных'")
				->queryScalar();

		$countServices = array();
		$companyActivites = [];
		foreach ($doctor->specialtyOfDoctors as $ds) {
			$companyActivites[] = $ds->doctorSpecialty->companyActivite->id;
		}

		$data = Yii::app()->db->createCommand()
						->select("*,count(*) as count")
						->from(AddressServices::model()->tableName())
						->where("companyActivitesId <> '$noDataId' AND addressId ='{$doctor->currentPlaceOfWork->address->id}' AND companyActivitesId IN ('" . implode("', '", $companyActivites) . "')")
						->group('companyActivitesId')->queryAll();

		foreach ($data as $row) {
			if ($row['companyActivitesId'])
				$countServices[$row['companyActivitesId']] = $row['count'];
		}

		$this->render('price', array(
			'model' => $model,
			'activity' => $activity,
			'prices' => $prices,
			'countServices' => $countServices,
			'doctor' => $doctor
		));
	}

	public function actionComments($link = null, $linkUrl = null) {
		if ($linkUrl) {
			$model = $this->loadModel($linkUrl);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl]);
			}
		}
		$this->render('comments', array(
			'model' => $model,
		));
	}

	public function actionComment($link = null, $linkUrl = null) {

		if ($linkUrl) {
			$model = $this->loadModel($linkUrl);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl]);
			}
		}

		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = array(
			'currentPlaceOfWork' => array(
				'together' => true
			)
		);
		$criteria->compare('doctorsId', $model->id);
		$criteria->compare('t.status', 1);

		$comments = RaitingDoctorUsers::model()->findAll($criteria);

		$this->render('comments_experts', array(
			'model' => $model,
			'comments' => $comments
		));
	}

	public function actionFavorite($link = null, $linkUrl = null) {
		if ($linkUrl) {
			$model = $this->loadModel($linkUrl);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl]);
			}
		}

		$favorite = Favorite::model()->findByAttributes(array(
			'usersId' => Yii::app()->user->model->id,
			'ownerId' => $model->id,
		));

		if ($favorite) {
			$favorite->delete();
			echo CJSON::encode(array(false, 'В избранное'));
		} else {
			$favorite = new Favorite();

			$favorite->usersId = Yii::app()->user->model->id;
			$favorite->ownerId = $model->id;
			$favorite->id = md5($favorite->usersId . $favorite->ownerId);

			$favorite->save();
			echo CJSON::encode(array(true, 'Удалить'));
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 * @return Doctor
	 */
	public function loadModel($link, $companyLinkUrl = null, $addressLinkUrl = null) {
		Address::$showInactive = true;
		$this->_model = Doctor::model()->find([
			'order' => 'deleted ASC',
			'condition' => 'linkUrl = :LU OR link = :LU',
			'params' => [
				':LU' => $link
			]
		]);

		if ($this->_model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		if (!empty($companyLinkUrl) && !empty($addressLinkUrl) && !$this->_model->deleted) {

			$match = false;
			foreach ($this->_model->placeOfWorks as $pw) {
				if ($pw->address->linkUrl === $addressLinkUrl) {
					$currPW = $pw;
					$match = true;
				}
			}

			if ($currPW->company->linkUrl !== $companyLinkUrl || !$match) {
				/* validates if companyLinkUrl corresponds to doctorLink */
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}

		return $this->_model;
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */

	public function getLeftMenu() {

		$link = $this->_model->currentPlaceOfWork->address->link;
		$company = $this->_model->currentPlaceOfWork->address->company;

		$menuItems = array(
			array(
				'label' => $company->logo ?
						CHtml::image($company->logoUrl, $company->name, array('class' => 'company-logo')) :
						CHtml::image($this->assetsImageUrl . '/icon_emp_med.jpg', '', array('class' => 'company-logo no-logo default'))
			),
		);

		//Не гос медицинская клиника
		//Есть контракт
		//Тип контракта с кнопкой
		if (!in_array($this->_model->currentPlaceOfWork->company->companyType->id, Address::$arrGMU) && !in_array($this->_model->currentPlaceOfWork->company->companyType->id, Company::$CompanyTypesNotOnline)) {
			$menuItems[] = array(
				'label' => 'Записаться на прием',
				'url' => array('/clinic/visit', 'linkUrl' => $this->_model->currentPlaceOfWork->address->linkUrl, 'companyLink' => $this->_model->currentPlaceOfWork->company->linkUrl),
				'itemOptions' => array(
				),
				'linkOptions' => array(
					'class' => 'btn-green',
				)
			);
		}
		$menuItems[] = array('template' => '<br>');
		$menuItems[] = array('template' => '<br>');
		foreach ($this->menu as $key => $value) {
			$menuItems[] = array(
				'label' => $key,
				'url' => array($this->createUrl($value), 'linkUrl' => $this->_model->currentPlaceOfWork->address->linkUrl, 'companyLink' => $this->_model->currentPlaceOfWork->company->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}

		if (!empty($this->_model->currentPlaceOfWork->address->doctors)) {
			$menuItems[] = array(
				'label' => 'Специалисты',
				'url' => array($this->createUrl('/clinic/experts'), 'linkUrl' => $this->_model->currentPlaceOfWork->address->linkUrl, 'companyLink' => $this->_model->currentPlaceOfWork->company->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}
		$criteria = new CDbCriteria;
		$criteria->with = [
			'addressServices' => [
				'with' => [
					'companyActivite' => [
						'joinType' => 'INNER JOIN'
					]
				]
			],
		];
		$criteria->compare('t.id', $this->_model->currentPlaceOfWork->address->id);
		$prices = Address::model()->findAll($criteria);

		if (!empty($prices)) {
			$menuItems[] = array(
				'label' => 'Стоимость услуг',
				'url' => array($this->createUrl('/clinic/price'), 'linkUrl' => $this->_model->currentPlaceOfWork->address->linkUrl, 'companyLink' => $this->_model->currentPlaceOfWork->company->linkUrl),
				'itemOptions' => array(
					'class' => 'btn w100'
				),
			);
		}
		/* if (!empty($this->_model->currentPlaceOfWork->address->comments)) { */
		$menuItems[] = array(
			'label' => 'Отзывы',
			'url' => array($this->createUrl('/clinic/comments'), 'linkUrl' => $this->_model->currentPlaceOfWork->address->linkUrl, 'companyLink' => $this->_model->currentPlaceOfWork->company->linkUrl),
			'itemOptions' => array(
				'class' => 'btn w100'
			),
		);
		/* } */
		#return [];
		return $menuItems;
	}

}
