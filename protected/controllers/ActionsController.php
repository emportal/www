<?php

class ActionsController extends Controller {

	public $searchbox_type	 = SearchBox::S_TYPE_PATIENT;
	public $searchbox_tab	 = Search::S_ACTION;
	public $infoBox_type	 = false;

	public $layout = 'news';

	public function actionIndex() {		
		$this->pageTitle = Yii::app()->name.' - Акции';
		
		
		$searchModel = new Search(Search::S_ACTION);		
		if(Yii::app()->request->getParam('Search')) {
			$searchModel->attributes = Yii::app()->request->getParam('Search');
		}
		
		
		$criteria = new CDbCriteria;
		$criteria->scopes=array('published','active');
		$criteria->group = '`t`.`id`';
		$criteria->with=array(
			'address'=>array(
				'select'=>false,
				'with'=>array(
					'together'	 => true,
					'cityDistrict'	 => array('select'=>false,'together' => true),											
					'metroStations'	 => array('select'=>false,'together' => true)
				)
			),
			'service' => [
				'together' => true,
				'with' => [
					'companyActivite' => [
						'together' => true
					]
				]
			]
		);
				
		
		
		if($searchModel->validate()) {					
			
			$criteria->compare('metroStations.id', $searchModel->metroStationId);
			$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
			$criteria->compare('t.companyActivitesId', $searchModel->companyActiviteId);
			
			
			if($searchModel->for_free)
				$criteria->compare('t.type',0);
			
			$criteria->compare('t.serviceId',$searchModel->serviceId);		
			
			
			if($searchModel->start) {
				$searchModel->start = date('Y-m-d', CDateTimeParser::parse($searchModel->start, 'yyyy-mm-dd'));					
				$criteria->addCondition("t.start >= '".$searchModel->start."' OR t.unlimited=1");
				$criteria->order = 't.unlimited ASC';				
			}
				
			
			//$criteria->compare('create_date',$this->create_date);
				
			
		} /*else {				
			$dataProvider = new CActiveDataProvider(Action::model(),array(
				'criteria'=>$criteria,
				'pagination'=>array(
					'pageSize'=> 10,
				),
			));
		}*/
		$dataProvider = new CActiveDataProvider(Action::model(),array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=> 10,
			),
		));			
		

		$this->render('index', array('dataProvider'=>$dataProvider));
	}

	public function actionView($link) {
		$model = $this->loadModel($link);
		$this->pageTitle = Yii::app()->name.' - '.$model->name;
		
		
		$this->render('view', array('model'=>$model));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 * @return News
	 */
	public function loadModel($link) {
		$model = Action::model()->findByLink($link);
		if ( $model === null )
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

}