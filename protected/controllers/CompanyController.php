<?php

class CompanyController extends Controller {
	
	public $layout='//layouts/catalog';

	public function actionIndex() {
		$this->render('index');
	}

	public function actionView($link) {

		$this->render('view', array(
			'model' => $this->loadModel($link),
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($link) {
		$model = Companies::model()->findByAttributes(array('Link'=>$link));
		if ( $model === null )
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}