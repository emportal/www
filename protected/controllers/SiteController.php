<?php
//require "Array2XML.php";

class SiteController extends Controller {

	public $layout = '//layouts/column1';
	public $searchbox_type = SearchBox::S_TYPE_PATIENT;
	public $searchbox_tab = Search::S_CLINIC;
	public $infoBox_type = false;
    public $seotype;

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
				'layout' => '//layouts/column1_for_old_pages'
			),
		);
	}
	
	public function filters() {
		return [
			['application.filters.PageContentsCache + index', 'cacheLifeTime' => '3600'],
			['application.filters.PageContentsCache + page', 'cacheLifeTime' => '172800'] //2 суток
		];
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {

		$this->layout = NULL;

		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
			if ($error['code'] == 404 || $error['code'] == 400) {
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
				$this->render('error404', $error);
			} else {
				if(!empty($error['code'])) header($_SERVER["SERVER_PROTOCOL"]." ".$error['code']." ".(!empty($error['message'])) ? $error['message'] : "");
				$this->render('error', $error);
			}
		}
	}
	
	public function actionCheckGeoIp() {
		$url = City::model()->checkGeoIp();
		$this->renderPartial('//layouts/_checkGeoIp', ['url'=>$url]);
	}

	public function actionIndex() {
		$criteria = new CDbCriteria();
		$criteria->order = 'count DESC';
		$criteria->compare("samozapis", (Yii::app()->params["samozapis"]) ? 1 : 0);
		$criteria->compare('t.cityId', City::model()->selectedCityId, false);
		
		$shortSpecialties = ShortSpecialtyOfDoctor::model()->findAll($criteria);
		/* if (empty(Yii::app()->request->cookies['testAB'])) {
		  Yii::app()->request->cookies['testAB'] = new CHttpCookie('testAB', mt_rand(1, 2), [
		  'expire' => time() + 60 * 60 * 24 * 30
		  ]);
		  }
		  if (Yii::app()->request->cookies['testAB']->value == 1) { */
		$this->searchbox_type = SearchBox::S_TYPE_PATIENT_NEW;
		/* } else {
		  $this->searchbox_type = SearchBox::S_TYPE_PATIENT_CLINIC;
		  } */

		$this->layout = '//layouts/column1';

		//Find popular doctor in the city
		$criteria = new CDbCriteria;
			//$criteria->select = "name,linkUrl,link,sexId,rating,experience,countRecords";
			//$criteria->group = '`t`.`id`';
			$criteria->order = "t.rating DESC, t.name ASC";
			$criteria->with = array(
				'appointmentCount' => array(
					'together' => true
				),
				'placeOfWorks' => array(
					'together' => true,
					'select' => 'id,doctorId,addressId',
					'with' => array(
						'address' => [
							'select' => "id,street,houseNumber,link,ownerId,cityId,linkUrl,name,latitude,longitude,samozapis",
							'together' => true,
							#'scopes' => 'active',
							'with' => [
								'userMedicals' => [
									'together' => true,
									'select' => false,
								],
							]
						]
					),
				),
			);

		// $criteria->scopes = 'active';
		$criteria->compare('userMedicals.agreementNew',1);//???????????????????????????????????
		$criteria->limit=1;
		$criteria->compare('address.cityId', City::model()->selectedCityId);
		$criteria->AddCondition('`placeOfWorks`.`id` IS NOT NULL');


		$doctors =Doctor::model()->findAll($criteria);

		//ob_start();
		$this->render('index', [
			'shortSpecialties' => $shortSpecialties,
			'doctors'=>$doctors,
			
		]);
		//$content = ob_get_clean();
		//Yii::app()->cache->set('pageCache', $content, 60);
		//echo $content;
	}
	
	//Перезаписать таблицу ShortSpecialtyOfDoctor
	public function actionUpdateShortSpecialtyOfDoctor($samozapis=FALSE) {
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$args = array('yiic', 'UpdateShortSpecialtyOfDoctor');
		if($samozapis) $args = array_merge($args, ["samozapis"]);
		ob_start();
		$runner->run($args);
		echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
	}
	//Загрузка клиник нетрики
	public function actionUpdateFromNetricaCommand($action=NULL) {
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$args = array('yiic', 'UpdateFromNetrica', 'action'=>$action);
		ob_start();
		$runner->run($args);
		echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
	}
	public function actionUpdateRating() {
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$args = array('yiic', 'UpdateRating');
		ob_start();
		$runner->run($args);
		echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
	}
	
	public function actionRemoveMarkedCompanies() {
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$args = array('yiic', 'RemoveMarkedCompanies');
		ob_start();
		$runner->run($args);
		echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
	}

	public function actionExitBoxSubmit() {
		//echo json_encode(["status" => "OK"]); exit;
		$response = [];
		
		try {
			$postArray = Yii::app()->request->getPost("exitMessage");
			if(!is_array($postArray)) $this->redirect('/');
			$modelExitMessage = new stdClass();
			foreach($postArray as $key => $value)
			{
				$modelExitMessage->{$key} = $value;
			}
			
			$mail = new YiiMailer();
			$mail->setView('feedback');
			$mail->setData(array(
				'model' => $modelExitMessage
			));
			$mail->render();
			$mail->From = 'robot@emportal.ru';
			$mail->FromName = Yii::app()->name;
			$mail->Subject = "Отзыв от пользователя #" . date('Y-M-d H:i:s');

			if(Yii::app()->params['devMode'])
				$mail->AddAddress(Yii::app()->params['devEmail']);
			else {
				$mail->AddAddress("alexey.m@emportal.ru");
				$mail->AddAddress("roman.z@emportal.ru");
				$mail->AddAddress("alexander.d@emportal.ru");
			}
	
			if ($mail->Send()) {
				$response["status"] = "OK";
			} else {
				$response["status"] = "ERROR";
			}
		} catch(Exception $e)
		{
			$response["status"] = "ERROR";
		}
		echo json_encode($response);
	}

//Показываем гостю confirmedVisit сообщение
	public function actionConfirmedVisit() {
		$this->searchbox_type = SearchBox::S_TYPE_PATIENT;
		if (Yii::app()->user->hasFlash('confirmedVisit')) {
			$this->render('confirmedVisit');
		} else {
			$this->redirect('/');
		}
	}

	public function actionFeedbackApp() {

		$model = new FeedbackForm;
#$body = '';
#$from = 'robot@emportal.ru';
		if (isset($_POST['FeedbackForm'])) {
			$model->attributes = $_POST['FeedbackForm'];

			/* 	$body .= '<br /><b>Название компании</b>: ' . $model->subject;
			  $body .= '<br /><b>Город</b>: ' . $model->city;
			  $body .= '<br /><b>Адрес</b>: ' . $model->address;
			  $body .= '<br /><b>Телефон</b>: ' . $model->phone;
			  $body .= '<br /><b>контактное лицо</b>: ' . $model->name;
			  $body .= '<br /><b>комментарий</b>: ' . $model->body; */

			if ($model->validate()) {
				/* $headers    = array
				  (
				  'MIME-Version: 1.0',
				  'Content-Type: text/html; charset="UTF-8";',
				  'Content-Transfer-Encoding: 7bit',
				  'Date: ' . date('r', $_SERVER['REQUEST_TIME']),
				  'Message-ID: <' . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>',
				  'From: ' . $from,
				  'Reply-To: ' . $from,
				  'Return-Path: ' . $from,
				  'X-Mailer: PHP v' . phpversion(),
				  'X-Originating-IP: ' . $_SERVER['SERVER_ADDR'],
				  );

				  mail(Yii::app()->params['adminEmail'], "Заявка от партнера #".date('Y-M-d H:i:s'), $body, implode("\n", $headers));
				 */
				$mail = new YiiMailer();
				$mail->setView('feedback');
				$mail->setData(array(
					'model' => $model
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "Отзыв от пользователя #" . date('Y-M-d H:i:s');

				if(Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['devEmail']);
				else
					$mail->AddAddress(Yii::app()->params['remarkAppstoreEmail']);

				if ($mail->Send()) {
					
				} else {
					
				}

				Yii::app()->user->setFlash('contact', 'Заявка успешно отправлена.');
				$this->refresh();
			}
		}
		$this->render('feedback', array('model' => $model));
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		
		$this->layout = NULL;
		$model = new ContactForm;
		#$body = '';
		#$from = 'robot@emportal.ru';
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];

			if ($model->validate()) {
				$mail = new YiiMailer();
				$mail->setView('contact');
				$mail->setData(array(
					'model' => $model
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "Заявка от партнера #" . date('Y-M-d H:i:s');

				if(Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['devEmail']);
				else
					$mail->AddAddress(Yii::app()->params['adminEmail']);
					$mail->AddAddress(Yii::app()->params['itEmail']);
				if ($mail->Send()) {
					
				} else {
					
				}

				Yii::app()->user->setFlash('contact', 'Заявка успешно отправлена. Наши менеджеры свяжутся с вами в ближайшее время');
				$this->refresh();
			}
		}
		$renderParams = [
			'model' => $model,
			'replaceWord' => 'бесплатно',
			'replacePhrase' => 'Ноль затрат для начала работы',
			'replaceImageSrc' => '/images/landing_icon_okey.png',
			'replaceText' => 'Мы привлекаем <span class="brush">новых</span><br/><span class="brush">пациентов</span> без каких-<br/>либо затрат с Вашей<br/>стороны!<br>Вы оплачиваете только<br/><span class="brush">результат</span> — дошедших до приема пациентов!',
		];
		if (City::isSelfRegisterCity()) {
			$renderParams['newCompany'] = new NewCompany();
			$renderParams['replaceWord'] = 'удобно';
			$renderParams['replacePhrase'] = 'Всего 990 рублей в месяц';
			$renderParams['replaceImageSrc'] = '/images/landing_icon_piggy_bank.png';
			$renderParams['replaceText'] = 'Мы привлекаем <span class="brush">новых</span><br/><span class="brush">пациентов</span> эффективнее любых других рекламных каналов! Использование сервиса <span class="brush" style="margin-right: 30px;">снижает</span> Ваши издержки и повышает лояльность пациентов.';
		}
		$this->render('contact', $renderParams);
	}
	public function actionVozmozhnosti_dlya_klinik($page) {
		$this->layout = NULL;
		$page = preg_replace('/(\.html)($|\?|#)/', '', $page);
		try {
			$this->render("vozmozhnosti_dlya_klinik/{$page}", array('model' => $model));
		} catch (Exception $e) {
			try {
				$this->render("vozmozhnosti_dlya_klinik/{$page}/index", array('model' => $model));
			} catch (Exception $e) {
				try {
					$page = preg_replace('/(\.html)($|\?|#)/', '', $page);
					$this->render("vozmozhnosti_dlya_klinik/{$page}", array('model' => $model));
				} catch (Exception $e) {
					header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
					$this->render('error404', $error);
				}
			}
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$JSON = ($_REQUEST['JSON'] || $_REQUEST['json'] || $_REQUEST['state'] == 'json');
		if(!empty(Yii::app()->request->urlReferrer)) {
			Yii::app()->user->returnUrl = Yii::app()->request->urlReferrer;
		}
		if (!Yii::app()->user->isGuest) {
			if ($JSON) {
				echo CJSON::encode(array(
						'authSuccess' => true,
						'session' => Yii::app()->getSession()->getSessionId(),
				));
				Yii::app()->end();
			}

			if (!Yii::app()->user->model->phoneActivationStatus) {
				$this->redirect(array('site/ActivatePhone'));
			}
			$this->redirect(array('/user'));
		}

		$model = new LoginForm;
		$type = Yii::app()->request->getParam('type');
		$service = Yii::app()->request->getQuery('service');
	    if (isset($service)) {
			$model->_authIdentity = Yii::app()->eauth->getIdentity($service);
			$model->_authIdentity->redirectUrl = Yii::app()->user->returnUrl.($JSON ? "?JSON=1" : "");
			$model->_authIdentity->cancelUrl = $this->createAbsoluteUrl('site/login'); 
	    	switch ($service) {
				case 'facebook':
	    			$type = SocialUserIdentity::TYPE_FB;
					break;
				case 'odnoklassniki':
					$model->_authIdentity->redirectUrl = Yii::app()->request->requestUri; 
	    			$type = SocialUserIdentity::TYPE_OK;
					break;
			}
		}
		/* Если $type установлен - значит логинимся через СоцСеть */
		if (!empty($type)) {
			if ($model->authenticateSocial($type)) {
				if ($JSON) {
					echo CJSON::encode(array(
							'authSuccess' => true,
							'session' => Yii::app()->getSession()->getSessionId(),
					));
					Yii::app()->end();
				}
				if(isset($model->_authIdentity)) {
					if(!empty($model->_authIdentity) && method_exists($model->_authIdentity, "redirect")) {
						$model->_authIdentity->redirect();
					}
				} else {
					$this->redirect(Yii::app()->user->returnUrl);
				}
			} else {
				if ($JSON) {
					echo CJSON::encode(array(
							'authSuccess' => false,
					));
					Yii::app()->end();
				}
				if(isset($model->_authIdentity)) {
					if(method_exists($model->_authIdentity, "cancel"))
						$model->_authIdentity->cancel();
				}
			}
		} else {
			// if it is ajax validation request
			if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			// collect user input data
			if (isset($_POST['LoginForm']))
			{
				$model->attributes = $_POST['LoginForm'];				
				$model->convertPhoneToStandardForm();
				
				// validate user input and redirect to the previous page if valid #$model->validate();
				if ($model->validate() && $model->login()) {

					if ($JSON) {
						echo CJSON::encode(array(
								'authSuccess' => true,
								'session' => Yii::app()->getSession()->getSessionId(),
						));
						Yii::app()->end();
					}
					if (!Yii::app()->user->model->phoneActivationStatus) { //rf ??
						$this->redirect(array('site/ActivatePhone'));
					} else {
					//Чистим таблицу phoneVerification
						Yii::app()->db->createCommand()->delete(PhoneVerification::model()->tableName(), "phone = :phone", array(
							":phone" => Yii::app()->user->model->telefon
						));
					}
					
					if (Yii::app()->user->model->hasRight('ACCESS_NEW_ADMIN'))
					{
                        Yii::app()->session['adminClinic'] = 1;
                        
                        if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA'))
                            $this->redirect(['newAdmin/clinicList/view']);
                        
						$this->redirect(['newAdmin/companyContact/list']);
					}
					
					if (Yii::app()->session['returnUrl']) {
						$ret = Yii::app()->session['returnUrl'];
						Yii::app()->session['returnUrl'] = "";
						$this->redirect($ret);
					} else {
						$this->redirect(Yii::app()->user->returnUrl);
					}
				} else if ($model->errorCode === UserIdentity::ERROR_NOT_ACTIVATED) {
					Yii::app()->user->setFlash('activate', 'Уважаемый посетитель,
					данная учетная запись еще не активирована или не существует, для активации
					пройдите по ссылке в письме высланному при
					регистрации или <a href="' . $this->createUrl('/activate') . '">повторите активацию</a>.');
				}

				if ($JSON) {
					if ($model->errorCode === UserIdentity::ERROR_NOT_ACTIVATED) {
						echo CJSON::encode(array('authError' => 'Уважаемый посетитель, данная учетная запись еще не активирована'));
					} else {
						echo CJSON::encode(array('authError' => 'Ошибка авторизации'));
					}
					Yii::app()->end();
				}
			}
		}


// display the login form
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout($returnUrl = null) {
		Yii::app()->user->logout();
		if (isset($_REQUEST['JSON'])) 
		{
			echo CJSON::encode(['success' => true]);
			Yii::app()->end();
		}
		if ($returnUrl)
		{
			$url = $returnUrl;
		} else {
			$url = Yii::app()->homeUrl;
		}
		//$url = Yii::app()->request->urlReferrer;
        Yii::app()->session['isAdmin'] = 0;
        Yii::app()->session['adminClinic'] = 0;
		$this->redirect($url);
	}

	public function actionRegister() {

		if (!Yii::app()->user->isGuest) {

			if (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(array('registerError' => 'Вы уже авторизованы'));
				Yii::app()->end();
			}
			$this->redirect(array('/user'));
		}

		$model = new User('register');
		$model->subscriber = 1;
		$model->confirmation = 1;
		if(isset($_REQUEST['promoCode'])) {
			$model->promoCode = $_REQUEST['promoCode'];
		}

		if (isset($_REQUEST[get_class($model)])) {

			$model->attributes = $_REQUEST[get_class($model)];

			if (empty($model->name)) {
				$model->name = $model->surName . " " . $model->firstName . " " . $model->fatherName;
			}
			/* в мобильном приложении сначала регистрация, потом подтверждение, поэтому пароли надо перегенить постоянно */
			$model->password ? $password = $model->password : $model->password = $password = User::generate_password(6, true);
			if (!empty($model->telefon)) {
				$model->telefon = '+' . MyRegExp::phoneRegExp($model->telefon);
			}


			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'reg-form') {

				if (isset($_REQUEST['JSON'])) {
					if (!$model->validate()) {
						echo CActiveForm::validate($model);
						Yii::app()->end();
					}
				} else {

// Получаем error по модели и мерджим с нашими кастомными ошибками 
// Грязный хак, потому что CActiveForm парсит POST данные пропуская наши добавленные ошибки через 
// model->addError

					CActiveForm::validate($model);
					$errors = array();
					foreach ($model->getErrors() as $attribute => $error)
						$errors[CHtml::activeId($model, $attribute)] = $error;

					$phone_error = array();
					/* if (!$model->getError('telefon')) {

					  $pvModel = PhoneVerification::model()->find('phone=:phone', array(
					  'phone' => $model->telefon
					  ));

					  //Если мобильный телефон не найден, значит код еще не высылался
					  if (!$pvModel) {
					  $phone_error['User_telefon'][] = 'Нажмите кнопку "получить код" и введите код подтверждения присланный в смс';

					  //Если мобильный телефон найден и времени с создания кода менее 2 часов
					  } elseif (time() - $pvModel->timestamp < 3600 * 2) {
					  //Если код еще не введен, выдать ошибку, иначе все валидно и разрешаем зарегистрироваться
					  if ($pvModel->status == 0) {
					  $phone_error['User_telefon'][] = 'Нажмите кнопку "получить код" и введите код подтверждения присланный в смс ранее';
					  }

					  //Если мобильный телефон найден и времени с создания кода более 2 часов,защита от багов
					  } else {
					  $pvModel->delete();
					  $phone_error['User_telefon'][] = 'Нажмите кнопку "получить код" и введите код подтверждения присланный в смс';
					  }
					  if ($pvModel && Yii::app()->request->getParam('phone_code')) {
					  $phone_error['User_telefon'][0] = 'Нажмите кнопку "Активировать"';
					  }
					  } */
					echo $errors = CJSON::encode(array_merge($errors, $phone_error));

					Yii::app()->end();
				}
			}

			if ($model->validate()) {

				/* phone activation */
//user/comments
				$model->birthday = date("Y-m-d", strtotime($model->birthday));



				if (isset($_REQUEST['JSON'])) {
					$model->phoneActivationStatus = 0;
					$model->status = 0;
					$model->phoneActivationDate = 0; //date("Y-m-d H:i:s",time())

					if ($model->save()) {

						//Отсылаем пароль по смс
						$sms = new SmsGate();
						$sms->phone = $model->telefon;
						$code = $sms->generateCode();

						$sms->setMessage('код активации: _code_', array(
							'_code_' => $code
						));

						$sms->send();

						//Сохраняем модель
						$pvModel = new PhoneVerification();
						$pvModel->code = $code;
						$pvModel->phone = $model->telefon;
						$pvModel->status = PhoneVerification::READY_TO_REGISTER;
						$pvModel->save();
					}
				} else {
					$pvModel = PhoneVerification::model()->find('phone=:phone', array(
						'phone' => $model->telefon
					));

					$model->phoneActivationStatus = 1;
					$model->status = 1;
					$model->phoneActivationDate = new CDbExpression("NOW()");


					if ($model->save()) {

						$login = new LoginForm;
						$login->username = $model->email;
						$login->password = $password;
						$login->login();
						
						Yii::app()->session['reachedGoal_user_register'] = 1;

//Через сайт пользователи вводят свой пароль
//
						//Отсылаем пароль по смс
						/* $sms = new SmsGate();
						  $sms->phone = $model->telefon;
						  $sms->user_pass = $password;

						  $sms->setMessage('Пароль для доступа в личный кабинет: _pass_', array(
						  '_pass_' => $password
						  ));

						  //$sms->message	= SmsGate::PASSWORD;
						  $sms->send();
						 */
						PhoneVerification::model()->deleteAll('phone = :phone', array(
							'phone' => $model->telefon
						));
					} else {
						
					}
				}

//$code			= $sms->generateCode();	
//Если смс отправлена, то сохраняем текущую запись в таблице PhoneVerification
				/* if($sendsms=$sms->send()) {
				  PhoneVerification::model()->deleteAll('phone = :phone',array(
				  'phone'=>$model->telefon
				  ));
				  $pv = new PhoneVerification();
				  $pv->code=$code;
				  $pv->phone=$model->telefon;
				  $pv->save();
				  } */
				/* phone activation */


				if ($model->email) {
					if (!isset($_REQUEST['JSON'])) {

						/* $emailCode = new EmailCode();
						  $emailCode->userId = $model->id;
						  $emailCode->value = $emailCode->generateCode();
						  $emailCode->date = time();
						  $emailCode->save(); */

						$mail = new YiiMailer();
						$mail->setView('register_success');
						$mail->setData(array(
							'name' => $model->name,
							'login' => $model->email,
							'password' => $password,
								//'code'		 => $emailCode->value,
								)
						);

						$mail->render();
						$mail->From = 'robot@emportal.ru';
						$mail->FromName = Yii::app()->name;
						$mail->Subject = 'Регистрация на ресурсе ' . Yii::app()->name;
						$mail->AddAddress($model->email);

						if ($mail->Send()) {
							
						}
					}

					if (!isset($_REQUEST['JSON'])) {
						Yii::app()->user->setFlash('registerSuccess', 'Уважаемый посетитель, на <u>мобильный телефон</u> и указанный вами адрес электронной почты (<strong>' . $model->email . '</strong>) отправлен пароль для доступа в личный кабинет.');
						$this->redirect('/login');
					} else {
						$data = array(
							'registerSuccess' => 'true',
							'registerSuccessText' => 'регистрация выполнена успешно. Для активации аккаунта введите код высланный в смс.'
						);
						echo CJSON::encode($data);
						Yii::app()->end();
					}
				} else {
					if (!isset($_REQUEST['JSON'])) {
						
					} else {
						$data = array(
							'registerSuccess' => 'true',
							'registerSuccessText' => 'регистрация выполнена успешно. Для активации аккаунта введите код высланный в смс.'
						);
						echo CJSON::encode($data);
						Yii::app()->end();
					}
				}
			} else {
				if (!isset($_REQUEST[get_class($model)]['confirmation']))
					$model->addError('confirmation', "Вы не подтвердили согласие на обработку данных");

				if (isset($_REQUEST['JSON'])) {
					$data = array('registerError' => $model->errors);
					echo CJSON::encode($data);
					Yii::app()->end();
				}
				Yii::log("Register errors: \n" . implode("\n", $model->errors), CLogger::LEVEL_ERROR, 'site.register');
			}
		}

		$this->layout = '//layouts/column1_for_old_pages';
		$this->render('register', array('model' => $model));
	}

	public function actionActivate($key = FALSE) {
		$key = Yii::app()->request->getParam('key');

		if (!Yii::app()->user->isGuest) {
			$this->redirect(array('/user'));
		}

		$model = new User('reactivate_code');
		$is_code = false;
		$action = Yii::app()->request->getParam('action');

		if (isset($_REQUEST['User']['email'])) {


			$model->attributes = $_REQUEST['User'];

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'activate-form') {

				if (isset($_REQUEST['JSON'])) {
					if (!$model->validate()) {
						echo CActiveForm::validate($model);
						Yii::app()->end();
					}
				} else {
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}


			if ($model->validate()) {

				$user = User::model()->findByAttributes(array('email' => $model->email, 'status' => 0));

				if ($user === null) {
					$model->addError('email', "Данный адрес электронной почты не зарегистрирован в системе или не требует активации.");
					$this->render('activate', array('model' => $model));
					Yii::app()->end();
				}

				$emailCode = new EmailCode();
				$emailCode->userId = $user->id;
				$emailCode->value = $emailCode->generateCode();
				$emailCode->date = time();

				if ($emailCode->save()) {
					$mail = new YiiMailer();
					$mail->setView('reactivate');
					$mail->setData(array(
						'code' => $emailCode->value)
					);

					$mail->render();
					$mail->From = 'robot@emportal.ru';
					$mail->FromName = Yii::app()->name;
					$mail->Subject = 'Активация учетной записи' . Yii::app()->name;
					$mail->AddAddress($user->email);

					if ($mail->Send()) {
						
					}

					Yii::app()->user->setFlash('recovery', 'Уважаемый посетитель, на указанный вами адрес электронной почты отправлено письмо для активации.');
				}
			}
		} else if ($key) {

			/* Активация аккаунта по коду присланному на телефон */

			$pv = PhoneVerification::model()->findByAttributes(array(
//нужен телефон
				'code' => (int) $key
			));

			if (count($pv)) {
				$is_code = true;
				$user = User::model()->findByAttributes(array(
					'telefon' => $pv->phone, //нужен телефон				
				));
				$user->phoneActivationStatus = 1;
				$user->phoneActivationDate = new CDbExpression('NOW()');
				$user->status = 1;
				$user->save();
				$phone = $user->telefon;
				$user->password = $password = User::generate_password(6, true);

				$sms = new SmsGate();
				$sms->phone = $phone;
				$sms->user_pass = $password;

				$sms->setMessage('Пароль для доступа в личный кабинет: _pass_', array(
					'_pass_' => $password
				));
//$sms->message	= SmsGate::PASSWORD;				
				$sms->send();

				$user->save();

				/* $usrlogin = new LoginForm;
				  $usrlogin->username = $user->email;
				  $usrlogin->password = $user->password;
				  $usrlogin->login(true); */
				$pv->delete();

				/* send mail */
				$mail = new YiiMailer();
				$mail->setView('register_success');
				$mail->setData(array(
					'name' => $user->name,
					'login' => $user->email,
					'password' => $password,
						//'code'		 => $emailCode->value,
						)
				);

				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = 'регистрация на ресурсе ' . Yii::app()->name;
				$mail->AddAddress($model->email);

				if ($mail->Send()) {
					
				}


				if (isset($_REQUEST['JSON'])) {
					$data = array('activateSuccess' => 'true', 'message' => 'Аккаунт успешно активирован. Пароль выслан в смс');
					echo CJSON::encode($data);
					Yii::app()->end();
				}
				$this->redirect(array('/user'));
//Yii::app()->user->setFlash('activated', 'Ваш мобильный телефон подтвержден');	
			} else {
				$pv = new PhoneVerification();
				$pv->attributes = $attr;
				if (isset($_REQUEST['JSON'])) {
					$data = array('activateError' => 'Неправильный код.');
					echo CJSON::encode($data);
					Yii::app()->end();
				}
				$model->addError('email', 'Неправильный код');
			}
			/* //////// Активация аккаунта по коду присланному на телефон */

			EmailCode::model()->deleteAll(' `date` < :date AND `type` = 1', array(':date' => strtotime('yesterday 00:00')));

			if ($emailCode = EmailCode::model()->findByAttributes(array('value' => $key))) {

				$user = $emailCode->user;
				$user->scenario = 'reactivate_code';
				$user->status = 1;

				if ($user->save()) {

					Yii::app()->user->setFlash('recovery', 'Ваша учетная запись успешно активирована.');

					$emailCode->delete();

					if (isset($_REQUEST['JSON'])) {

						$data = array('activateSuccess' => 'true');
						echo CJSON::encode($data);
						Yii::app()->end();
					} else {
						$model = new LoginForm;

						$model->username = $user->email;
						$model->password = $user->password;

						$model->login(true);
					}
					if (!Yii::app()->user->model->phoneActivationStatus) {
						$this->redirect(array('site/activatephone'));
					}
				} else {
					
					Yii::app()->user->setFlash('recovery', 'По техническим причинам активация не была завершенна, обратитесь к <a href="mailto:support@emportal.ru">администрации ресурса</a>.');
				}
			}
			else {
				if (isset($_REQUEST['JSON'])) {

					$data = array('activateError' => 'Указанный код недействителен.');
					echo CJSON::encode($data);
					Yii::app()->end();
				}
				Yii::app()->user->setFlash('recovery', 'Указанный код недействителен.');
			}
		}


		$this->render('activate', array('model' => $model));
	}

	public function actionForm() {
		$this->render('form');
	}

	public function actionRecoveryByPhone() {
		if (Yii::app()->user->isGuest) {

			if (Yii::app()->session['recoveryByPhone']) {
				$remain = Yii::app()->params['SendCodeDelay'] - (time() - Yii::app()->session['recoveryByPhone']['time']);

				$remain = (int) ($remain / 60) . ":" . str_replace(" ", "0", sprintf("%2d", $remain % 60));
				if (time() - Yii::app()->session['recoveryByPhone']['time'] > Yii::app()->params['SendCodeDelay']) {
					unset(Yii::app()->session['recoveryByPhone']);
				}
			}

			$key = Yii::app()->request->getParam('key');
			$errors = [];
			$model = new User('reactivateByPhone');
			$userData = Yii::app()->request->getParam('User');

			if (!$userData && Yii::app()->session['recoveryByPhone']['phone']) {
				$userData['telefon'] = Yii::app()->session['recoveryByPhone']['phone'];
			}
			if ($userData['telefon']) {
				$userData['telefon'] = "+" . trim($userData['telefon'], "+");
			}

			if (!empty($userData['telefon'])) {

				if (!$key && !Yii::app()->session['recoveryByPhone']) {

					$model->attributes = $userData;
					if (isset($_POST['ajax']) && $_POST['ajax'] === 'recovery-form') {
						echo CActiveForm::validate($model);
						Yii::app()->end();
					}
					$user = User::model()->findByAttributes([
						'telefon' => $model->telefon
					]);

					if ($user) {

						$pv = PhoneVerification::model()->find("status = :stat AND phone = :ph", [
							'stat' => PhoneVerification::RECOVERY_BY_PHONE,
							'ph' => $user->telefon
						]);
						if (!$pv) {
							$sms = new SmsGate();
							$pv = new PhoneVerification();
							$code = $sms->generateCode();

							$pv->status = PhoneVerification::RECOVERY_BY_PHONE;
							$pv->code = $code;
							$pv->phone = $model->telefon;
							$pv->userId = $user->id;
							$pv->save();


							$sms->phone = $user->telefon;
							$sms->setMessage('код подтверждения: _code_.', ['_code_' => $sms->code]);
							if ($sms->send()) {

								Yii::app()->session['recoveryByPhone'] = [
									'isSent' => true,
									'time' => time(),
									'phone' => $user->telefon
								];
								$remain = Yii::app()->params['SendCodeDelay'];
								$remain = (int) ($remain / 60) . ":" . str_replace(" ", "0", sprintf("%2d", $remain % 60));
								if (isset($_REQUEST['JSON'])) {
									echo CJSON::encode(['success' => true, 'comment' => 'код подтверждения отправлен.']);
									Yii::app()->end();
								}
							} else {
								if (isset($_REQUEST['JSON'])) {
									echo CJSON::encode(array('success' => false, 'comment' => 'Не удалось отправить код подтверждения.'));
									Yii::app()->end();
								}
							}
						} elseif (time() - $pv->timestamp < Yii::app()->params['SendCodeDelay']) { #
							if (isset($_REQUEST['JSON'])) {
								echo CJSON::encode(array('success' => 'true', 'comment' => 'код уже высылался. Повторная отправка возможна через 10 минут.'));
								Yii::app()->end();
							}
							$model->addError('telefon', 'Введите код присланный ранее.');
						} else {
							$pv->delete();

							$sms = new SmsGate();
							$pv = new PhoneVerification();
							$code = $sms->generateCode();

							$pv->status = PhoneVerification::RECOVERY_BY_PHONE;
							$pv->code = $code;
							$pv->phone = $model->telefon;
							$pv->userId = $user->id;
							$pv->save();


							$sms->phone = $user->telefon;
							$sms->setMessage('код подтверждения: _code_.', ['_code_' => $sms->code]);
							if ($sms->send()) {

								Yii::app()->session['recoveryByPhone'] = [
									'isSent' => true,
									'time' => time(),
									'phone' => $user->telefon
								];
								$remain = Yii::app()->params['SendCodeDelay'];
								$remain = (int) ($remain / 60) . ":" . str_replace(" ", "0", sprintf("%2d", $remain % 60));
								if (isset($_REQUEST['JSON'])) {
									echo CJSON::encode(['success' => true, 'comment' => 'код подтверждения отправлен.']);
									Yii::app()->end();
								}
							} else {
								if (isset($_REQUEST['JSON'])) {
									echo CJSON::encode(array('success' => false, 'comment' => 'Не удалось отправить код подтверждения.'));
									Yii::app()->end();
								}
							}
						}
//$result = $sms->send();
#$sms->test();
						/* Если в течение 10 минут код не введен то он удаляется */
						/* Устанавливаем время жизни кода и */

						Yii::app()->user->setFlash('recoveryByPhone', '<div class="flash-success">На указанный вами номер выслан код активации</div>');
					} else {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(array('success' => false, 'comment' => 'Данный телефон не зарегистрирован в системе.'));
							Yii::app()->end();
						}
						$model->addError('telefon', 'Данный телефон не зарегистрирован в системе');
					}

//Если код прислан
				} elseif ($key) {
					if (isset($_POST['ajax']) && $_POST['ajax'] === 'recovery-form') {
						echo CActiveForm::validate($model);
						Yii::app()->end();
					}
					$pv = PhoneVerification::model()->find("status = :stat AND code = :code AND phone = :ph", [
						'stat' => PhoneVerification::RECOVERY_BY_PHONE,
						'code' => $key,
						'ph' => $userData['telefon']
					]);
					if ($pv) {
						$user = User::model()->findByAttributes([
							'id' => $pv->userId
						]);
						$password = User::generate_password(6, true);
						$user->password = $password;
						$user->save();
						$sms = new SmsGate();
						$sms->phone = $user->telefon;
						$sms->setMessage('Новый пароль: _pw_', ['_pw_' => $password]);
						$sms->send();
						$pv->delete();
						Yii::app()->user->setFlash('fromRecovery', '<div class="flash-success">Введите пароль, присланный в смс.</div>');
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'comment' => 'Новый пароль выслан по смс.']);
							Yii::app()->end();
						}
						unset(Yii::app()->session['recoveryByPhone']);
						$this->redirect('/site/login');
					} else {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => false, 'comment' => 'Неверный код.']);
							Yii::app()->end();
						}
						$errors[] = 'Неверный код';
					}
				}
			} else {

				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(['success' => false, 'comment' => 'Укажите мобильный телефон']);
					Yii::app()->end();
				}
				/* Если пришел телефон - генерим код смены пароля и высылаем в смс */
				if (isset($_POST['ajax']) && $_POST['ajax'] === 'recovery-form') {
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}

			$this->render('recoverybyphone', ['model' => $model, 'errors' => $errors, 'remain' => $remain]);
		} else {
			$this->redirect('/');
		}
#$action = Yii::app()->request->getParam('action');
	}

	public function actionRecovery($key = FALSE) {

		$model = new User('reactivate');

		$action = Yii::app()->request->getParam('action');

		if ($action === 'Отправить' && isset($_REQUEST['User']['email'])) {

			$model->attributes = $_REQUEST['User'];

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'recovery-form') {

				if (isset($_REQUEST['JSON'])) {
					if (!$model->validate()) {
						echo CActiveForm::validate($model);
						Yii::app()->end();
					}
				} else {
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}


			if ($model->validate()) {
				$user = User::model()->findByAttributes(array('email' => $model->email, 'status' => 1));

				if ($user === null) {
					$model->addError('email', "Данный адрес электронной почты не зарегистрирован в системе.");
					$this->render('recovery', array('model' => $model));
					Yii::app()->end();
				}


				$user = User::model()->findByAttributes(array('email' => $_REQUEST['User']['email']));

				$emailCode = new EmailCode();
				$emailCode->userId = $user->id;
				$emailCode->value = $emailCode->generateCode();
				$emailCode->date = time();
				$emailCode->type = 1;

				if ($emailCode->save()) {
					$mail = new YiiMailer();
					$mail->setView('recovery_password');
					$mail->setData(array(
						'name' => $user->name,
						'login' => $user->email,
						'code' => $emailCode->value)
					);

					$mail->render();
					$mail->From = 'robot@emportal.ru';
					$mail->FromName = Yii::app()->name;
					$mail->Subject = 'Восстановление пароля ' . Yii::app()->name;
					
					$mail->AddAddress($user->email);

					if ($mail->Send()) {
						
					}

					Yii::app()->user->setFlash('recovery', 'Уважаемый посетитель, на указанный вами адрес электронной почты отправлено письмо для продолжения процедуры восстановление пароля.');
				}
			} else {
				$model->addError('email', "Данный адрес электронной почты не зарегистрирован в системе.");
				$this->render('recovery', array('model' => $model));
				Yii::app()->end();
			}
		} else if ($key) {
			EmailCode::model()->deleteAll(' `date` < :date AND `type` = 1', array(':date' => strtotime('yesterday 00:00')));

			if ($emailCode = EmailCode::model()->findByAttributes(array('value' => $key))) {

				$user = $emailCode->user;

				$password = $user->password = User::generate_password(10);
				$user->scenario = "reactivate";
				if ($user->save()) {

					$mail = new YiiMailer();
					$mail->setView('newpassword');
					$mail->setData(array(
						'name' => $user->name,
						'login' => $user->email,
						'password' => $password)
					);

					$mail->render();
					$mail->From = 'robot@emportal.ru';
					$mail->FromName = Yii::app()->name;
					$mail->Subject = 'Смена пароля ' . Yii::app()->name;
					
					$mail->AddAddress($user->email);

					if ($mail->Send()) {
						
					}

					Yii::app()->user->setFlash('recovery', 'Пароль успешно изменен и выслан на ваш адрес электронной почты.');

					$emailCode->delete();
				} else {
					Yii::app()->user->setFlash('recovery', 'По техническим причинам пароль не был сменен, обратитесь к <a href="mailto:support@emportal.ru">администрации ресурса</a>.');
				}
			} else
				Yii::app()->user->setFlash('recovery', 'Данный код для сменны пароля, устарел или не является действительным, просьба <a href="' . $this->createUrl('/recovery') . '">попробывать еще</a>.');
		}

		$this->render('recovery', array('model' => $model));
	}

	/* Активация телефона заново */

	public function actionActivatePhone() {
		Yii::app()->clientScript->registerCoreScript('jquery.maskedinput');

		$user = Yii::app()->user->model;

//Если гость
		if (Yii::app()->user->isGuest) {
			if (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(['success' => true, 'comment' => 'Вы не авторизованы']);
				Yii::app()->end();
			}
			$this->redirect('/');
		} elseif ($user->phoneActivationStatus && (time() - strtotime($user->phoneActivationDate)) < Yii::app()->params['phoneActivationLifetime']) {
			if (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(['success' => true, 'comment' => 'Телефон активирован']);
				Yii::app()->end();
			}
//Если телефон активирован, то редирект обратно
			if (Yii::app()->session['returnUrl']) {
				$this->redirect(Yii::app()->session['returnUrl']);
			} else {
				$this->redirect('/user');
			}
		} else {

			$attr = Yii::app()->request->getParam('PhoneVerification');

			if ($attr) {
				$model = new PhoneVerification();
				$model->attributes = $attr;

				if ($model->phone) {
					$model->phone = "+" . trim($model->phone, "+");
					$model->type = "no";
					if ($model->phone == $user->telefon) {
						$model->type = "yes";
					}
					$phone = $model->phone;
				} else {
					if (isset($_REQUEST['JSON'])) {
						echo CJSON::encode(['success' => false, 'comment' => 'Необходимо заполнить телефон']);
						Yii::app()->end();
					}
					$model->addError('phone', 'Необходимо заполнить телефон');
				}



				if (!$model->code) {
					/* Если пришел только номер */


					if (User::model()->find("telefon=:phone AND id<>:userId", array('phone' => $phone, 'userId' => Yii::app()->user->model->id))) {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => false, 'comment' => 'Данный номер занят']);
							Yii::app()->end();
						}
						$model->addError('phone', 'Данный номер занят');
					}

					if (!$model->hasErrors()) {
						$showCodeRow = true;

						switch ($model->type) {
							case "yes":
								$status = PhoneVerification::PHONE_EXTEND_LIFETIME;
								$status2 = PhoneVerification::CHANGE_PHONE_EXTEND_LIFETIME;
								break;
							case "no":
								$status = PhoneVerification::CHANGE_PHONE_EXTEND_LIFETIME;
								$status2 = PhoneVerification::PHONE_EXTEND_LIFETIME;
								break;
							default:
								return;
								break;
						}


						$modelNew = PhoneVerification::model()->find("(status=:status OR status=:status2) AND userId=:userId", array(
							':status' => $status,
							':status2' => $status2,
							':userId' => $user->id
						));


						if (!count($modelNew)) {
							$sms = new SmsGate();

							$sms->phone = $phone;
							$code = $sms->generateCode();
							$sms->setMessage('код подтверждения: _code_.', array(
								'_code_' => $sms->code
							));

							if ($sendsms = $sms->send()) {
								PhoneVerification::model()->deleteAll('userId = :userId AND (status=:status OR status=:status2)', array(
									'userId' => $user->id,
									'status' => $status,
									'status2' => $status2
								));
								$pv = new PhoneVerification();
								$pv->code = $code;
								$pv->status = $status;
								$pv->userId = $user->id;
								$pv->phone = $phone;

								if ($pv->save()) {
									if (isset($_REQUEST['JSON'])) {
										echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации отправлено']);
										Yii::app()->end();
									}
								}
							}
//если код высылался только что
						} elseif (time() - $modelNew->timestamp < Yii::app()->params['SendCodeDelay']) {
							if (isset($_REQUEST['JSON'])) {
								echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации уже отправлено']);
								Yii::app()->end();
							}
							$model->addError('phone', 'Смс с кодом активации уже отправлено');

//если код высылался но давно
						} else {

							$modelNew->delete();
							$sms = new SmsGate();

							$sms->phone = $phone;
							$code = $sms->generateCode();
							$sms->setMessage('код подтверждения: _code_.', array(
								'_code_' => $sms->code
							));

							if ($sendsms = $sms->send()) {
								PhoneVerification::model()->deleteAll('userId = :userId AND (status=:status OR status=:status2)', array(
									'userId' => $user->id,
									'status' => $status,
									'status2' => $status2
								));
								$pv = new PhoneVerification();
								$pv->code = $code;
								$pv->status = $status;
								$pv->phone = $phone;
								$pv->userId = $user->id;

								if ($pv->save()) {
									if (isset($_REQUEST['JSON'])) {
										echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации отправлено']);
										Yii::app()->end();
									}
								}
							}
						}
					}
				} else {
					/* Если номер и код */
#$model->type = $attr['type'];
					switch ($model->type) {
						case 'yes':
							$status = PhoneVerification::PHONE_EXTEND_LIFETIME;
							break;
						case 'no':
							$status = PhoneVerification::CHANGE_PHONE_EXTEND_LIFETIME;
							break;
						default:
							$status = 11111; // cant find that
							break;
					}

					$pv = PhoneVerification::model()->findByAttributes(array(
						'phone' => $model->phone,
						'code' => $model->code,
						'status' => $status
					));

					if ($pv) {
						$user = Yii::app()->user->model;
						$user->scenario = "activatePhone";
						$user->phoneActivationDate = new CDbExpression("NOW()");
						$user->phoneActivationStatus = 1;
						$user->isBlockedAppointment = 0;
						$user->blockedTime = 0;
						if ($status == PhoneVerification::CHANGE_PHONE_EXTEND_LIFETIME) {
							$user->telefon = $pv->phone;
						}
						if ($user->save()) {
							$pv->delete();
							if (isset($_REQUEST['JSON'])) {
								echo CJSON::encode(['success' => true, 'comment' => 'Телефон активирован']);
								Yii::app()->end();
							}
							$this->refresh();
						}
					} else {
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'comment' => 'Введены неправильные данные']);
							Yii::app()->end();
						}

						Yii::app()->user->setFlash("error", "Введены неправильные данные");
					}
				}
			}

			if (!$model) {
				$model = new PhoneVerification();
			}
			if ($model->phone) {
				$showCodeRow = true;
			}
			$model->phone = $model->phone ? $model->phone : $user->telefon;

			$this->render('activatephone', array(
				'model' => $model,
				'showCodeRow' => $showCodeRow
			));
		}
	}

	public function actionGallery() {
		$this->layout = 'news';
		
		$criteria = new CDbCriteria();
		$criteria->addCondition('name not like "Системный"');
		
		$albums = Album::model()->findAll($criteria);
		$name = Yii::app()->request->getParam('name');

		if (!empty($name)) {
			$album = Album::model()->findByAttributes([
				'name' => $name
			]);
			if ($album) {
				$photos = $album->galleryBehavior->getGalleryPhotos('gallery', 1000);
				$dataProvider = new CArrayDataProvider($photos, [
					'pagination' => [
						'pageSize' => 30
					]
				]);
			} else {
				unset($name);
			}

#$dataProvider->setTotalItemCount(count($photos));
		}

		$this->render('gallery', [
			'albums' => $albums, // list of albums
			'album' => $album, // picked album
#'photos' => $photos, // photos from album
			'dataProvider' => $dataProvider, //dataprovider for CGridView
			'name' => $name // name of picked album
		]);
	}

	/* разблокировка телефона */

	public function actionBlockedAppointment() {
		if (isset($_REQUEST['JSON'])) {
			$model = PhoneVerification::model()->user()->oneTimePhone()->find();

			if ($model) {
				$attr = Yii::app()->request->getParam('PhoneVerification');
				if (!empty($attr) && !empty($attr['code'])) {
					if ($attr['code'] == $model->code) {
						$model->status = PhoneVerification::APPOINTMENT;
						$model->save();
						echo CJSON::encode(['success' => true, 'comment' => 'Номер подтвержден']);
					} else {
						echo CJSON::encode(['success' => false, 'comment' => 'Неверный код']);
					}
				} else {
					echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации отправлено']);
				}
				Yii::app()->end();
			}
		}
		if (Yii::app()->user->isGuest || Yii::app()->user->model->isBlockedAppointment == 0) {
//Если телефон активирован
			if (Yii::app()->session['returnUrl']) {
				$this->redirect(Yii::app()->session['returnUrl']);
			} else {
				$this->redirect('/user');
			}
			if (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(['success' => false, 'auth' => false, 'comment' => 'Вы не авторизованы']);
				Yii::app()->end();
			}
		} else {

			$user = Yii::app()->user->model;
			$status = PhoneVerification::BLOCKED_APPOINTMENT;

//Ищем созданную ранее запись с кодом
			$pv = PhoneVerification::model()->findByAttributes(array(
				'phone' => $user->telefon,
				'status' => $status
			));

//Если нету значит создаем и отправляем сообщение			
			if (!$pv) {

				$sms = new SmsGate();
				$code = $sms->generateCode();
				$sms->phone = $user->telefon;
				$sms->setMessage('код подтверждения: _code_.', array(
					'_code_' => $sms->code
				));


				$pv = new PhoneVerification();
				$pv->userId = $user->id;
				$pv->status = PhoneVerification::BLOCKED_APPOINTMENT;
				$pv->phone = $user->telefon;
				$pv->code = $code;

				if ($pv->save()) {
					$sms->send();
					$sent = 1;

					if (isset($_REQUEST['JSON'])) {
						echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации отправлено']);
						Yii::app()->end();
					}
				}

// Если запись создана и еще не устарела
			} elseif (time() - $pv->timestamp < Yii::app()->params['SendCodeDelay']) {
				$sent = 2;
			} else {
				$pv->delete();

				$sms = new SmsGate();
				$code = $sms->generateCode();
				$sms->phone = $user->telefon;
				$sms->setMessage('код подтверждения: _code_.', array(
					'_code_' => $sms->code
				));


				$pv = new PhoneVerification();
				$pv->userId = $user->id;
				$pv->status = PhoneVerification::BLOCKED_APPOINTMENT;
				$pv->phone = $user->telefon;
				$pv->code = $code;

				if ($pv->save()) {
					$sms->send();
					$sent = 1;
					if (isset($_REQUEST['JSON'])) {
						echo CJSON::encode(['success' => true, 'comment' => 'Смс с кодом активации отправлено']);
						Yii::app()->end();
					}
				}
			}

			$attr = Yii::app()->request->getParam('PhoneVerification');

			if ($attr) {

				$model = new PhoneVerification();
				$model->attributes = $attr;

				$pv = PhoneVerification::model()->findByAttributes(array(
					'code' => $model->code,
					'phone' => $user->telefon,
					'status' => $status
				));

				if ($pv) {
					$user->phoneActivationDate = new CDbExpression("NOW()");
					$user->isBlockedAppointment = User::APPOINT_ALLOW;
					$user->blockedTime = 0;
					if ($user->save()) {
						$pv->delete();
						if (isset($_REQUEST['JSON'])) {
							echo CJSON::encode(['success' => true, 'comment' => 'Аккаунт разблокирован']);
							Yii::app()->end();
						}
						$this->refresh();
					}
				} else {
					$model->addError('code', 'Неправильный код');
				}
			}

			if (!$model) {
				$model = new PhoneVerification();
			}

			$this->render('blockedappointment', array(
				'model' => $model,
				'sent' => $sent
			));
		}
	}

//	public function actionPage($page) {
//
//
//		if ( $view = $this->getViewFile("pages/$page") )
//				$this->render('$view');
//
//	}

	public function actionTest()
    {
        //$company = Company::model()->findByPk('5452d0c3-38a2-11e2-b014-e840f2aca94f');
        //$address = $company->addresses[0];
        //$address->sendClaimsViaEmail([]);
        //var_dump($company->addresses[0]);FileMaker::createClaim($this, true, "pdf");
    	$statusesAppType = AppointmentToDoctors::getStatusesAppType();
    	print_r($statusesAppType);
        exit;
    	$address = Address::model()->findByPk("f4e2020c-179e-11e2-a2a8-e840f2aca94f");
        FileMaker::createClaim($address, false, "pdf");
        exit;
		if (@Yii::app()->user->model->hasRight('ACCESS_NEW_ADMIN')) {
			if($_REQUEST['promocode']) {
				var_dump(PromoCode::getAvailablePromoCodes());
			}
		} else {
			throw new CHttpException(403,'You are not authorized to per-form this action.');
		}
	}
	
	public function actionRss() {
		/*
		 * Yii::import('ext.feed.*');

		  $feed = new EFeed();

		  $feed->title= 'Единый Медицинский Портал';
		  $feed->description = 'Последние новости Единого Медицинского Портала';

		  #$feed->setImage('Testing RSS 2.0 EFeed class','http://www.ramirezcobos.com/rss','http://www.yiiframework.com/forum/uploads/profile/photo-7106.jpg');

		  $feed->addChannelTag('language', 'ru-Ru');
		  $feed->addChannelTag('pubDate', date(DATE_RSS, time()));
		  $feed->addChannelTag('link', 'http://dev1.emportal.ru/rss' );

		  // * self reference
		  ##$feed->addChannelTag('atom:link','http://dev1.emportal.ru/rss');


		  $item = $feed->createNewItem();
		  $item->title = "111";
		  $item->link = "http://www.yahoo.com";
		  $item->date = time();
		  $item->description = '111This is test of adding CDATA Encoded description <b>EFeed Extension</b>';
		  // this is just a test!!
		  #$item->setEncloser('http://www.tester.com', '1283629', 'audio/mpeg');

		  #$item->addTag('author', 'thisisnot@myemail.com (Antonio Ramirez)');
		  $item->addTag('guid', 'http://dev1.emportal.ru/',array('isPermaLink'=>'true'));

		  $feed->addItem($item);

		  $feed->generateFeed();
		  Yii::app()->end();

		 */
	}

	public function actionTestMail() {
		/*
		  $data = AppointmentToDoctors::model()->findAll('isSentThanksMail = 0 AND plannedTime < NOW() - INTERVAL 1 WEEK AND statusId = :stat',array(
		  #":stat"		=> AppointmentToDoctors::SEND_TO_REGISTER,
		  ":stat"	=> AppointmentToDoctors::VISITED
		  ));
		  foreach($data as $row) {
		  if($row->statusId == AppointmentToDoctors::SEND_TO_REGISTER) {
		  $row->statusId = AppointmentToDoctors::VISITED;
		  }

		  $row->isSentThanksMail = 1;
		  $row->isNotVisitedHash = md5(join("_",$row->attributes));

		  #echo 1;

		  $mail = new YiiMailer();
		  $mail->setView('appointment_motivate_review');
		  $mail->setData(array(
		  'model'=>$row
		  )
		  );
		  $mail->render();
		  $mail->From = 'robot@emportal.ru';
		  $mail->FromName = Yii::app()->name;
		  $mail->Subject = 'Ваше мнение о посещении клиники "'.$row->company->name.'"';
		  $mail->AddAddress($row->email);
		  if($mail->Send()) {

		  }

		  $row->save(false);

		  }

		 */
	}

	public function actionTestRegister() {
		/*
		 * update usermedicals with contracts - fix bug in 1C
		  $medicals = UserMedical::model()->findAll();
		  foreach($medicals as $med) {
		  echo $med->company->companyContracts->id."<br>";
		  if(!empty($med->company->companyContracts)) {
		  $med->agreement = 1;
		  $med->createDate = new CDbExpression("NOW()");
		  $med
		  }
		  } */


		$this->renderPartial("testRegister");
		/* $ch = curl_init();
		  curl_setopt($ch, CURLOPT_URL, "https://emportal.ru/register");
		  curl_setopt($ch, CURLOPT_POST, 1);
		  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
		  'JSON' => 1,
		  'User' => [
		  'email' => 'klka1@live.ru',
		  'surName' => 'Thigh',
		  'firstName' => 'Ffh',
		  'fatherName' => 'Ffh',
		  'subscriber' => 0,
		  'telefon' => "+79213434502",
		  'confirmation' => 1
		  ],

		  #'key' => 122222
		  )));

		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  $server_output = curl_exec($ch);
		  curl_close($ch);
		  var_dump($server_output);
		  var_dump(json_decode($server_output)); */
	}

	public function actionDeleteForMichael() {
		$phone = "+79217434713";
		$mail = "michael.chernikov@gmail.com";
		$val = User::model()->findByAttributes(array('telefon' => $phone));
		if (!$val) {
			$val = User::model()->findByAttributes(array('email' => $mail));
		}
		if ($val) {
			$val->syncThis = false;
			$val->email = "REMOVELATER";
			$val->telefon = "+79999999999";
			$val->save();
			echo "deleted";
		} else {
			echo "not found";
		}
	}

	/* Test activation after 3 months */

	public function actionTestPhone() {
		$phone = "+79217434713";
		$mail = "michael.chernikov@gmail.com";
		$val = User::model()->findByAttributes(array('telefon' => $phone));
		if (!$val) {
			$val = User::model()->findByAttributes(array('email' => $mail));
		}
		if ($val) {
			$val->syncThis = false;
			$val->phoneActivationDate = "1970-01-01 00:00:00";
			$val->save();
			echo "date changed";
		} else {
			echo "not found";
		}
	}
	
	public function actionXmlCreate($type = '2gis') {
		
		set_time_limit(120);
		
		$excludedCompanies = Yii::app()->request->getParam('ec');
		$json = Yii::app()->request->getParam('json');
		$phonePartner = stripslashes(htmlspecialchars( Yii::app()->request->getParam('phonePartner') ));
		
		$listName = 'offers';
		$itemName = 'offer';
		
		$criteria = new CDbCriteria;
		
		if (!empty($excludedCompanies)) {
			foreach($excludedCompanies as $link)
				$criteria->addCondition('company.linkUrl != "' . stripslashes(htmlspecialchars($link)) . '"');
		}
		
		if ($type == 'piluli') {
			$cityId = '534bd8b9-e0d4-11e1-89b3-e840f2aca94f'; //только Москва
		} else {
			$cityId = '534bd8b8-e0d4-11e1-89b3-e840f2aca94f'; //только СПб
		}
		Address::$showInactive = true;
		Company::$showRemoved = true;
		if($region = Yii::app()->request->getParam('region')) {
			if ($region == 'all') {
				$cityId = '';
			} else {
				$regionParams = Yii::app()->params['regions'][$region];
				if(($cityModel = City::model()->findByAttributes(['subdomain' => $region])) && $regionParams) {
					$cityId = $cityModel->id;
				} else {
					$xml = Array2XML::createXML('errors', ['error' => ['@value' => "Регион `$region` не найден!",  '@attributes' => ['code' => '404']]]);
					header("Content-type: text/xml; charset=utf-8");
					echo $xml->saveXML() . "\n";
					exit;
				}
			}
		}
		if(empty($cityModel)) $cityModel = City::model()->findByPk($cityId);
		
		if(empty($type) || $type == 'zoon' || $type == '2gis' || $type == 'piluli' || $type == 'feed') {
			if(Yii::app()->request->getParam('id') != null) {
				$criteria->compare('t.link',Yii::app()->request->getParam('id'));
			} else {
				$criteria->compare('t.cityId', $cityId, false);
			}
			
			$criteria->with = [
					'userMedicals' => [
							'together' => true,
							'joinType' => 'INNER JOIN'
					],
					"company" => [
							'together' => true,
							'joinType' => 'INNER JOIN',
							'with' => [
									'companyContracts' => [
											'together' => true,
											'joinType' => 'INNER JOIN',
											'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
									],
							],
					],
			];
			$criteria->addCondition('t.isActive = 1');
			$criteria->addCondition('t.samozapis = 0');
			$criteria->addCondition('userMedicals.agreementNew = 1');
			$criteria->addCondition('userMedicals.createDate > 0');
			if ($type == 'zoon') {
				$criteria->addCondition('company.linkUrl != \'medicentr_1\''); //никогда не включаем
				$criteria->addCondition('company.linkUrl != \'mrt-ekspert_3\'');
                $criteria->addCondition('company.linkUrl != \'gorodskaya-bolynica-20_1\'');
                $criteria->addCondition('company.linkUrl != \'sanavita\'');
				if (date('j') > 15) {
					$criteria->addCondition('company.linkUrl != \'msch-122\''); //выключать после 15 числа 122 мсч
					$criteria->addCondition('company.linkUrl != \'apeks-med\'');
					$criteria->addCondition('company.linkUrl != \'firakom\'');
					$criteria->addCondition('company.linkUrl != \'vrach-\'');
				}
				$criteria->addCondition('userMedicals.feedZoon = 1');
			}
			if(class_exists("HttpSimpleAuth")) {
				if(is_object(HttpSimpleAuth::$userAPI)) {
					$userAPIaddresses = [];
					foreach (HttpSimpleAuth::$userAPI->addresses as $userAPIaddress) {
						$userAPIaddresses[] = $userAPIaddress->id;
					}
					if(count($userAPIaddresses)>0) {
						$criteria->addInCondition("t.id", $userAPIaddresses);
					}
				}
			}
			
			$searchCriteria = null;
			if (Yii::app()->request->getParam('loyaltyProgram') != null) {
				if(Yii::app()->params['regions'][$cityModel->subdomain]['loyaltyProgram']) {
					if($searchCriteria == null) { $searchCriteria = clone $criteria; }
					$searchCriteria->compare('userMedicals.agreementLoyaltyProgram', 1);
				}
			}
			if(Yii::app()->request->getParam('limit') != null) {
				if($searchCriteria == null) { $searchCriteria = clone $criteria; }
				$count = Address::model()->count($searchCriteria);
				$searchCriteria->limit = Yii::app()->request->getParam('limit');
				$searchCriteria->offset = Yii::app()->request->getParam('offset');
			}
			if($searchCriteria !== null) {
				$searchCriteria->group = "t.id";
				$address = Address::model()->findAll($searchCriteria);
				$addressId = [];
				foreach ($address as $addressModel) {
					$addressId[] = $addressModel->id;
				}
				$criteria->addInCondition("t.id",$addressId);
			}
			
			$address = Address::model()->findAll($criteria);
			if($region == 'spb') {
				$address = array_merge($address,[Address::model()->findByLink('000001080')]);
			}
			if(!is_numeric($count)) $count = count($address);
			
			$offers = [];
			
			if(is_array($address)) {
				$nearestMetro = function($metroStations) {
					foreach ($metroStations as $MetroStation) {
						$result['metroStation'][] = ['name' => $MetroStation->name];
					}
					return is_array($result) ? $result : NULL;
				};
				
				$doctorSpetialities = function($doctors) {
					$uniSpec = [];
					foreach ($doctors as $doctor) {
						if($doctor->deleted) continue;
						foreach ($doctor->specialties as $specialty) {
							if(!in_array($specialty->name, $uniSpec)) {
								$uniSpec[] = $specialty->name;
								$result['specialty'][] = ['name' => $specialty->name];
							}
						}
					}
					return is_array($result) ? $result : NULL;
				};
				
				foreach($address as $row) {
					if($row->isActive || $row->link == '000001080') {
						if ($type == '2gis' && $row->link != '000001080')
						{
							$offer['id'] = ['@value' => ''.$row->link];
							$offer['reward'] = ['@value' => 250,  '@attributes' => ['metric' => 'roubles']];
							$offer['order_url'] = ['@value' => 'http://emportal.ru/widget/'.$row->company->linkUrl."/".$row->linkUrl];
							$offer['country_code'] = ['@value' => "ru"];
							$offer['city'] = ['@value' => "Санкт-Петербург"];
							$address = '';
							if(isset($row->street))$address .= ''.$row->street;
							if(isset($row->houseNumber))$address .= ', '.$row->houseNumber;
							$supplier = ['name' => ''.$row->company->name, 'address' => $address];
							$coordinates = ['lon' => ''.$row->longitude, 'lat'=> ''.$row->latitude];
							$supplier['coordinates'] = $coordinates;
							$offer['supplier'] = $supplier;
						}
						
						if ($type == 'zoon')
						{
							$id = $row->company->linkUrl . '--' . $row->linkUrl;
							//$maxLength = max($maxLength , strlen($id));
							$offer['id'] = ['@value' => $row->link];
							$offer['latName'] = ['@value' => $id];
							$offer['url'] = ['@value' => $row->getLinkHref()];
							$offer['country_code'] = ['@value' => "ru"];
							$address = '';
							if(isset($row->street))$address .= ''.$row->street;
							if(isset($row->houseNumber))$address .= ', '.$row->houseNumber;
							$supplier = ['name' => ''.$row->company->name, 'address' => $address];
							$coordinates = ['lon' => ''.$row->longitude, 'lat'=> ''.$row->latitude];
							$supplier['coordinates'] = $coordinates;
							$offer['supplier'] = $supplier;
							
							if (array_key_exists($row->city->subdomain, Yii::app()->params['regions']))
								$regionParams = Yii::app()->params['regions'][$row->city->subdomain];
							if ($regionParams && $regionParams['zoonPrice']) {
								$offer['price'] = $regionParams['zoonPrice'];
								if($id == 'olmed--5-ya-sovetskaya-23') {
									$offer['price'] = 100;
								}
								if($id == 'centr-ghenskogo-zdorovyya--kutuzovskiy-pr-33-str-1') {
									$offer['price'] = 300;
								}
							}
						}
						
						if ($type == 'piluli' && $row->link != '000001080')
						{
							$offer = [
								'id' => $row->link,
								'company' => $row->company->linkUrl,
								'address' => $row->linkUrl,
								'companyName' => $row->company->name,
								'addressName' => $row->shortName,
								'coordinates' => ['lon' => ''.$row->longitude, 'lat'=> ''.$row->latitude],
							];
						}
						
						if ($type == 'feed' && $row->link != '000001080') {
							$offer = [
								'id' => $row->link,
								'company' => $row->company->linkUrl,
								'address' => $row->linkUrl,
								'companyName' => $row->company->name,
								'addressName' => $row->shortName,
								'coordinates' => ['lon' => ''.$row->longitude, 'lat'=> ''.$row->latitude],
								'region' => $row->city->subdomain,
								'cityDistrict' => ['name' => $row->cityDistrict->name],
								'metroStations' => $nearestMetro($row->metroStations),
								'street' => $row->street,
								'houseNumber' => $row->houseNumber,
								'specialties' => $doctorSpetialities($row->doctors),
							];
							
							if ($phonePartner && $regionParams && $partnerPhones = $regionParams['empRegistryPhoneForPartner']) {
								if (array_key_exists($phonePartner, $partnerPhones))
									$offer['empRegistryPhone'] = $partnerPhones[$phonePartner];
							}
							
							$offer['loyaltyProgram'] = intval(Yii::app()->params['regions'][$row->city->subdomain]['loyaltyProgram'] && $row->userMedicals->agreementLoyaltyProgram);
						}
						
						$offers[$itemName][] = $offer;
					}
				}
			}
			if(is_numeric($count)) $offers['@attributes']['count'] = $count;
			//if ($json) {
			//	$jsonData = CJSON::encode([$listName => $offers]);
			//} else {
			//	$xml = Array2XML::createXML($listName, $offers);
			//}
		} elseif($type == 'feedDoctors') {
			$criteria->compare("t.deleted", 0);
			$criteria->compare("company.removalFlag", 0);
			$criteria->compare("address.isActive", 1);
			$criteria->compare("address.samozapis", (isset($_GET['samozapis']) ? $_GET('samozapis') : 0));
			$criteria->compare('userMedicals.agreementNew', 1);
			$criteria->compare('address.cityId', $cityId, false);
			$criteria->with = [
				'placeOfWorks' => [
					'together' => true,
					'with' => [
						'company' => [
							'together' => true,
							'with' => [
									'companyContracts' => [
											'together' => true,
											'joinType' => 'INNER JOIN',
											'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
									],
							],
						],
						'address' => [
							'together' => true,
							'with' => [
								'userMedicals' => [
									'together' => true,
								],
							],
						],
					],
				],
				'specialtyOfDoctors' => [
					'together' => true,
					//'with' => [
					//	'doctorSpecialty' => [
					//		'together' => true,
					//	],
					//],
				],
			];

			if (Yii::app()->request->getParam('addressId')!==null AND Yii::app()->request->getParam('addressId')!=='') {
				$criteria->compare("address.link", Yii::app()->request->getParam('addressId'));
			}
			if(class_exists("HttpSimpleAuth")) {
				if(is_object(HttpSimpleAuth::$userAPI)) {
					$userAPIaddresses = [];
					foreach (HttpSimpleAuth::$userAPI->addresses as $userAPIaddress) {
						$userAPIaddresses[] = $userAPIaddress->id;
					}
					if(count($userAPIaddresses)>0) {
						$criteria->addInCondition("address.id", $userAPIaddresses);
					}
				}
			}
			
			if ((int)Yii::app()->request->getParam('id')) {
				$doctors = [
					Doctor::model()->findByLink( (int)Yii::app()->request->getParam('id') )
				];
			}
			else {
				$searchCriteria = null;
				if (Yii::app()->request->getParam('surName') != null) {
					if($searchCriteria == null) { $searchCriteria = clone $criteria; }
					$searchCriteria->compare('t.surName', Yii::app()->request->getParam('surName'), true);
				}
				if (Yii::app()->request->getParam('cityDistrictId') != null) {
					if($searchCriteria == null) { $searchCriteria = clone $criteria; }
					$searchCriteria->compare('address.cityDistrictId', Yii::app()->request->getParam('cityDistrictId'), false);
				}
				if (Yii::app()->request->getParam('specialtyId') != null) {
					if($searchCriteria == null) { $searchCriteria = clone $criteria; }
					$searchCriteria->compare('specialtyOfDoctors.doctorSpecialtyId', Yii::app()->request->getParam('specialtyId'), false);
				}
				if (is_numeric(Yii::app()->request->getParam('sex'))) {
					if($searchCriteria == null) { $searchCriteria = clone $criteria; }
					$sexId = Yii::app()->request->getParam('sex') ? '81160944c16a392048918e487f6c328a' : 'b4a45cdfcb1a97ee4ba9baf3c3013f69';
					$searchCriteria->compare('t.sexId', $sexId, false);
				}
				if (Yii::app()->request->getParam('metroStationId') != null) {
					if($searchCriteria == null) { $searchCriteria = clone $criteria; }
					$searchCriteria->with['placeOfWorks']['with']['address']['with']['nearestMetroStations'] = ['together'=>true];
					$searchCriteria->compare('nearestMetroStations.metroStationId', Yii::app()->request->getParam('metroStationId'), false);
				}
				if (Yii::app()->request->getParam('loyaltyProgram') != null) {
					if(Yii::app()->params['regions'][$cityModel->subdomain]['loyaltyProgram']) {
						if($searchCriteria == null) { $searchCriteria = clone $criteria; }
						$searchCriteria = Doctor::getIsLoyaltyProgramParticipantCriteria($searchCriteria);
					}
				}
				if(is_string(Yii::app()->request->getParam('sort'))) {
					switch (explode(" ", Yii::app()->request->getParam('sort'))[0]) {
						case 'price':
							$sortcriteria = new CDbCriteria;
							$sortcriteria->with['doctorServices'] = array(
									'select' => "price",
									'together' => true,
									'with' => [
										'cassifierService' => [
											'select' => 'id,link,name',
											'together' => true,
										],
									],
								);
							$sortcriteria->compare('cassifierService.link', 'first');
							$sortcriteria->order = "doctorServices.".trim(Yii::app()->db->quoteValue(Yii::app()->request->getParam('sort')),"'").", t.link";
							break;
						case 'rating':
							$sortcriteria = new CDbCriteria;
							$sortcriteria->order = "t.".trim(Yii::app()->db->quoteValue(Yii::app()->request->getParam('sort')),"'").", t.link";
							break;
						default:
							break;
					}
					if(!empty($sortcriteria)) $criteria->mergeWith($sortcriteria);
				}
				if(Yii::app()->request->getParam('limit') != null) {
					if($searchCriteria == null) { $searchCriteria = clone $criteria; }
					$searchCriteria->limit = Yii::app()->request->getParam('limit');
					$searchCriteria->offset = Yii::app()->request->getParam('offset');
					if(!empty($sortcriteria)) $searchCriteria->mergeWith($sortcriteria);
				}
				if($searchCriteria !== null) {
					$searchCriteria->group = "t.id";
					$doctors = Doctor::model()->findAll($searchCriteria);
					$doctorsId = [];
					foreach ($doctors as $doctorModel) {
						$doctorsId[] = $doctorModel->id;
					}
					$criteria->addInCondition("t.id",$doctorsId);

					$searchCriteria->limit = null;
					$searchCriteria->offset = null;
					$count = Doctor::model()->count($searchCriteria);
				}
				$doctors = Doctor::model()->findAll($criteria);
				if(!is_numeric($count)) $count = count($doctors);
			}
			
			$doctorAddresses = function($placeOfWorks) {
				foreach ($placeOfWorks as $place) {
					$result['address'][] = [
						'linkUrl' => $place->address->linkUrl,
						'id' => $place->address->link,
					];
				}
				return is_array($result) ? $result : NULL;
			};
			$doctorSpecialties = function($specialtiesOfDoctor) {
				foreach ($specialtiesOfDoctor as $specialtyOfDoctor) {
					$result['specialty'][] = [
						'name' => $specialtyOfDoctor->doctorSpecialty->name,
						'linkUrl' => $specialtyOfDoctor->doctorSpecialty->linkUrl,
						'category' => $specialtyOfDoctor->doctorCategory->name
					];
					unset($specialtyOfDoctor->doctorSpecialty);
					//if ($specialtyOfDoctor->doctorCategory->name)
					//	$chunk['category'] = $specialtyOfDoctor->doctorCategory->name;
					//$result['specialty'][] = $chunk;
				}
				return is_array($result) ? $result : NULL;
			};
			$doctorEducations = function($educations) {
				if(is_array($educations)){
					foreach ($educations as $education) {
						$result['education'][] = [
							'name' => $education->medicalSchool->fullName,
							'type' => ($education->typeOfEducation->name ? $education->typeOfEducation->name : NULL),
							'year' => (($education->yearOfStady > 0) ? $education->yearOfStady : NULL),
						];
					}
				}
				return is_array($result) ? $result : NULL;
			};
			
			if(is_array($doctors))
			foreach ($doctors as $row) {
				if (Yii::app()->request->getParam('relatedModels')) {
					switch(Yii::app()->request->getParam('relatedModels')) {
						case 'receptions':
							//проверка на обязательность параметров doctorId и date?
							
							//заполнение массива dates
							$dates = (Yii::app()->request->getParam('date')) ? [Yii::app()->request->getParam('date')] : [date('Y-m-d')];
							
							foreach($dates as $date) {
								
								//сначала получим все адреса где врач импортирован
								//и получим доступное расписание по каждому адресу
								$placeOfWorks = $row->placeOfWorks;
								$allTimeQuants = [];
								foreach($placeOfWorks as $placeOfWork) {
									$addressLink = $placeOfWork->address->link;
									$timeQuants = WorkingHour::getDate($addressLink, $date, $row->link, false, '');
									$timeQuants = WorkingHour::convertReceptionsToApiFormat($timeQuants, $date, $addressLink);
									$allTimeQuants = array_merge($allTimeQuants, $timeQuants);
								}
								foreach($allTimeQuants as $offer) {
									$offers[$itemName][] = $offer;
								}
								
								//$offers[$itemName][] = $offer;
							}
							break;
						case 'reviews':
							$reviews = $row->publishedReviews;
							foreach($row->publishedReviews as $publishedReview) {
								$offers[$itemName][] = [
									'id' => $publishedReview->id,
									'addressId' => $publishedReview->address->link,
									'createDate' => substr($publishedReview->createDate, 0, -3),
									'authorName' => $publishedReview->userName,
									'text' => $publishedReview->reviewText,
									'ratingDoctor' => $publishedReview->ratingDoctor,
									'ratingClinic' => $publishedReview->ratingClinic,
								];
							}
							break;
					}
				} else {
					$offer = [
						'id' => $row->link,
						'surName' => $row->surName,
						'firstName' => $row->firstName,
						'fatherName' => $row->fatherName,
						'linkUrl' => $row->linkUrl,
						'photoUrl' => MyTools::url_origin($_SERVER, false) . $row->getDoctorPhotoUrl(),
						'rating' => $row->roundedRating,
						'company' => ['linkUrl' => $row->placeOfWorks[0]->company->linkUrl],
						'addresses' => $doctorAddresses($row->placeOfWorks),
						'specialties' => $doctorSpecialties($row->specialtyOfDoctors),
						'description' => $row->description,
						'educations' => $doctorEducations($row->doctorEducations),
						'loyaltyProgram' => intval(Yii::app()->params['regions'][$cityModel->subdomain]['loyaltyProgram'] && $row->getIsLoyaltyProgramParticipant()),
					];
					
					//if(Yii::app()->request->getParam('fromApi')) {
						if (substr($row->birthday, 0, 1) != 0)
							$offer['birthDate'] = substr($row->birthday, 0, -9);
						if ($row->inspectPrice || $row->InspectFree)
							$offer['receptionPrice'] = !$row->InspectFree ? $row->inspectPrice : '0';
						if (!!$row->experience)
							$offer['practiceStartYear'] = $row->experience;
						if ($row->scientificTitle)
							$offer['scientificTitle'] = $row->scientificTitle->name;
						if ($row->scientificDegrees && $row->scientificDegrees[0])
							$offer['scientificDegree'] = $row->scientificDegrees[0]->name;
						if ($row->sexId != 'ae11c45d855a991340c5f59edcb404da') //если не "нет данных"
							$offer['sex'] = ($row->sexId == 'b4a45cdfcb1a97ee4ba9baf3c3013f69') ? '0' : '1';
					//}
					$offers[$itemName][] = $offer;
				}
			}
			if(is_numeric($count)) $offers['@attributes']['count'] = $count;
		} elseif($type == 'feedReceptions') {
			$criteria->compare("t.deleted", 0);
			$criteria->compare("company.removalFlag", 0);
			$criteria->compare("address.isActive", 1);
			$criteria->compare("address.samozapis", (isset($_GET['samozapis']) ? $_GET('samozapis') : 0));
			$criteria->compare('userMedicals.agreementNew', 1);
			$criteria->compare('address.cityId', $cityId, false);
			$criteria->with = [
				'placeOfWorks' => [
					'together' => true,
					'with' => [
						'company' => [
							'together' => true,
							'with' => [
									'companyContracts' => [
											'together' => true,
											'joinType' => 'INNER JOIN',
											'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
									],
							],
						],
						'address' => [
							'together' => true,
							'with' => [
								'userMedicals' => [
									'together' => true,
								],
							],
						],
					],
				],
			];
			//id, date
			$doctor = Doctor::model()->findByLink( Yii::app()->request->getParam('id') );
			$itemName = Yii::app()->request->getParam('date');
			
			$offer = [
				'date' => $row->link,
				'surName' => $row->surName,
				'firstName' => $row->firstName,
				'fatherName' => $row->fatherName,
			];
			$offers[$itemName][] = $offer;
		}
		if ($json) {
			header("Content-type: application/json; charset=utf-8");
			$jsonData = CJSON::encode([$listName => $offers]);
			echo $jsonData;
		} else {
			header("Content-type: text/xml; charset=utf-8");
			$xml = Array2XML::createXML($listName, $offers);
			echo $xml->saveXML() . "\n";
		}
		
	}

	public function actionYandexCompanies() {
		$criteria = new CDbCriteria;
		$criteria->with = [
				'userMedicals'=>['together' => true, 'select' => false, 'joinType' => 'INNER JOIN'],
				'company'=>['together' => true, 'select' => false, 'joinType' => 'INNER JOIN'],
				'_workingHours'=>['together' => true, 'select' => false, 'joinType' => 'LEFT JOIN'],
				'city'=>['together' => true, 'select' => false, 'joinType' => 'INNER JOIN'],
		];
		$criteria->compare('t.isActive', 1);
		$criteria->compare('t.samozapis', 0);
		$criteria->compare('company.removalFlag', 0);
		$criteria->compare('userMedicals.agreementNew', 1);
		$address = Address::model()->findAll($criteria);
		$companies = ['@attributes' => ['version' => "1.0"]];
		list($usec, $sec) = explode(" ", microtime());
		if(is_array($address))
		foreach($address as $index=>$row) {
			if(!$row->isActive) continue;
			//if($index > 2) break;
			//if($row->link !== "000000564" && $row->link !== "000009728") continue;
			//if($row->linkUrl !== "-sankt-peterburg-prospekt-yuriya-gagarina-65") continue;
			$company = [];
			$company['@attributes'] = ['id' => $row->link];
			$company['book-mode'] = ['@value' => 'dynamic'];
			$company['name'] = ['@value' => ''.$row->company->name, '@attributes' => ['lang'=>"ru"]];
			$company['post-index'] = ['@value' => ''.$row->postIndex];
			$company['address'] = ['@value' => ''.trim(trim($row->name,',')), '@attributes' => ['lang'=>"ru"]];
			$company['country'] = ['@value' => 'Россия', '@attributes' => ['lang'=>"ru"]];
			$company['admn-area'] = ['@value' => ''.$row->city->name, '@attributes' => ['lang'=>"ru"]];
			$company['locality-name'] = ['@value' => ''.$row->city->name, '@attributes' => ['lang'=>"ru"]];
			$company['street'] = ['@value' => ''.$row->street, '@attributes' => ['lang'=>"ru"]];
			$company['house'] = ['@value' => ''.$row->houseNumber];
			$coordinates = ['lon' => floatval($row->longitude), 'lat'=> floatval($row->latitude)];
			//$company['coordinates'] = $coordinates;
			foreach ($row->phones as $phone) {
				$item = [
					'number'=> ''.$phone->name,
					'info'=> ''.$phone->type->name,
					'ext'=> '',
					'type'=> ($phone->type->name == "Факс") ? 'fax' : 'phone',
				];
				$company['phone'] = $item;
			}
			if(!empty($row->emails[0]->name)) {
				$company['email'] = $row->emails[0]->name;
			}
			$company['url'] = ['@value' => 'https://emportal.ru/klinika/'.$row->company->linkUrl."/".$row->linkUrl];

			$daysOfWeekMask = [];
			$WorkingHours = "";
			if ($row->workingHours->isWorkingHoursAllTheSame) {
				if ($row->workingHours->isWorkingHoursAllTheSame == 'fullday') {
					$WorkingHours .= "пн-вс круглосуточно";
					$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, ["00:00-24:00" => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true, 5=>true, 6=>true]]);
				}
				elseif ($row->workingHours->isWorkingHoursAllTheSame == 'work') {
					$Hours = $row->workingHours->MondayStart . "-" . $row->workingHours->MondayFinish;
					$WorkingHours .= "пн-вс " . $Hours;
					$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, [$Hours => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true, 5=>true, 6=>true]]);
				}
				else 
					$WorkingHours .= "пн-вс выходной";
			} else
			{
				if ($row->workingHours->isWeekdaysTheSame) {
					if ($row->workingHours->isWeekdaysTheSame == 'fullday') {
						$WorkingHours .= "пн-пт круглосуточно";
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, ["00:00-24:00" => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true]]);
					}
					elseif ($row->workingHours->isWeekdaysTheSame == 'work') {
						$Hours = $row->workingHours->MondayStart . "-" . $row->workingHours->MondayFinish;
						$WorkingHours .= "пн-пт " . $Hours;
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, [$Hours => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true]]);
					}
					else
						$WorkingHours .= "пн-пт выходной";
				}
				if ($row->workingHours->isWeekendTheSame) {
					if($WorkingHours !== "") $WorkingHours .= ", ";
					if ($row->workingHours->isWeekendTheSame == 'fullday') {
						$WorkingHours .= "сб-вс круглосуточно";
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, ["00:00-24:00" => [5=>true, 6=>true]]);
					}
					elseif ($row->workingHours->isWeekendTheSame == 'work') {
						$Hours = $row->workingHours->SaturdayStart . "-" . $row->workingHours->SaturdayFinish;
						$WorkingHours .= "сб-вс " . $Hours;
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, [$Hours => [5=>true, 6=>true]]);
					}
					else
						$WorkingHours .= "сб-вс выходной";
				} 
				if(empty($WorkingHours)) {
					foreach (array('пн'=>'Monday', 'вт'=>'Tuesday', 'ср'=>'Wednesday', 'чт'=>'Thursday', 'пт'=>'Friday', 'сб'=>'Saturday', 'вс'=>'Sunday') as $key => $day) {
						if (!empty($row->workingHours->{$day . Start})) {
							if($WorkingHours !== "") { $WorkingHours .= ", "; }
							$WorkingHours .= $key . " ";
							if ($row->workingHours->{$day . Start} == '00:00' && $row->workingHours->{$day . Finish} == '00:00')
								$WorkingHours .= "выходной";
							else {
								if (($row->workingHours->{$day . Start} == '11111' || $row->workingHours->{$day . Finish} == '11111') || ($row->workingHours->{$day . Start} == '00:00' && $row->workingHours->{$day . Finish} == '23:59')) {
									$WorkingHours .= "круглосуточно";
									$Hours = "00:00-24:00";
								}
								else {
									$Hours = $row->workingHours->{$day . Start} . "-" . $row->workingHours->{$day . Finish};
									$WorkingHours .= $Hours;
								}
								switch ($day) {
									case 'пн':
										$daysOfWeekMask[$Hours][0] = true;
									break;
									case 'вт':
										$daysOfWeekMask[$Hours][1] = true;
									break;
									case 'ср':
										$daysOfWeekMask[$Hours][2] = true;
									break;
									case 'чт':
										$daysOfWeekMask[$Hours][3] = true;
									break;
									case 'пт':
										$daysOfWeekMask[$Hours][4] = true;
									break;
									case 'сб':
										$daysOfWeekMask[$Hours][5] = true;
									break;
									case 'вс':
										$daysOfWeekMask[$Hours][6] = true;
									break;
								}
							}
						}
					}
				}
			}
			$company['working-time'] = ['@value' => $WorkingHours, '@attributes' => ['lang'=>"ru"]];
			
			if(count($daysOfWeekMask) == 0) $daysOfWeekMask["08:00-22:00"] = [0 => true, 1 => true, 2 => true, 3 => true, 4 => true, 5 => true, 6 => true];
			$daysOfWeekMaskStr = [];
			foreach ($daysOfWeekMask as $key=>$mask) {
				foreach ($mask as $index=>$value) {
					$daysOfWeekMaskStr[$key]['mask'] .= ($index+1);
					$WHM = explode("-", $key);
					$WHMfrom = explode(":", $WHM[0]);
					$WHMto = explode(":", $WHM[1]);
					$WMfrom = intval($WHMfrom[0])*60 + intval($WHMfrom[1]);
					$WMto = intval($WHMto[0])*60 + intval($WHMto[1]);
					if(!isset($daysOfWeekMaskStr[$key]['timeFrame'])) {
						for ($M = $WMfrom; $M < $WMto; $M+=30) {
							$hfrom = intval($M/60);
							$mfrom = intval($M%60);
							if($hfrom < 10) $hfrom = "0".$hfrom;
							if($mfrom < 10) $mfrom = "0".$mfrom;
							$hto = intval(($M+30)/60);
							$mto = intval(($M+30)%60);
							if($hto == 24) $hto = 0;
							if($hto < 10) $hto = "0".$hto;
							if($mto < 10) $mto = "0".$mto;
							$from = $hfrom . ":" . $mfrom;
							$to = $hto . ":" . $mto;
							$daysOfWeekMaskStr[$key]['timeFrame'][] = ['@attributes' => ["from"=>$from, "to"=>$to]];
						}
					}
				}
			}
			
			$company['rubric-id'][] = ['@value' => '184106108'];
			if(isset($row->company->companyType->name)) {
				if($row->company->companyType->name == "Стоматология") {
					$company['rubric-id'][] = ['@value' => "184106132"];
				}
			}
			$datetime = round(microtime(true)*1000);
			$company['actualization-date'] = ['@value' => $datetime];
			
			if ($row->description || $row->company->description) {
				$company['description'] = ['@value' => ($row->description ? $row->description : $row->company->description)];
			}
			$logoUrl = $row->company->getLogoUrl();
			if (!empty($logoUrl)){
				$photo = ['@attributes' => ['url' => "https://emportal.ru". $logoUrl, 'alt' => $row->company->name, 'type' => 'logo']];
				$company['photos']['photo'][] = $photo;
			}
			$photos = $row->galleryBehavior->getGalleryPhotos('gallery');
			if (!empty($photos) && count($photos)) {
				foreach ($photos as $item) {
					if(!empty($item->getUrl())) {
						$photo = ['@attributes' => ['url' => "https://emportal.ru".$item->getUrl(), 'alt' => $item->name, 'type' => 'interior']];
						$company['photos']['photo'][] = $photo;
					}
				}
			}
			if(isset($company['photos'])) {
				$company['photos']['@attributes'] = ['gallery-url' => 'https://emportal.ru/klinika/'.$row->company->linkUrl."/".$row->linkUrl];
			}
			/* */
			$serviceArr = [];
			$resourceArr = [];
			foreach ($row->doctors as $key=>$doctor) {
				if($doctor->deleted) continue;
				$resource = ['@attributes' => ['id' => $doctor->link]];
				$resource['name'] = $doctor->name;
				$resource['priority'] = intval($doctor->rating);
				if(!empty(trim($doctor->description))) $resource['description'] = trim($doctor->description);
				else $resource['description'] = 'Врач';
				if(is_file(Yii::getPathOfAlias('uploads.doctor.'.$doctor->link) .DIRECTORY_SEPARATOR. "photo.jpg")) {
					$resource['photo'] = "https://emportal.ru/uploads/doctor/".$doctor->link."/photo.jpg";
				}
				if(!empty($doctor->specialtyOfDoctors)) {
					foreach ($doctor->specialtyOfDoctors as $specialty) {
						if(!empty($specialty->doctorSpecialty)) {
							if(!isset($serviceArr[$specialty->doctorSpecialty->link])) {
								if(!empty($specialty->doctorSpecialty->genitive)) {
									$serviceArr[$specialty->doctorSpecialty->link]['title'] = "Приём у ".$specialty->doctorSpecialty->genitive;
								} else {
									$serviceArr[$specialty->doctorSpecialty->link]['title'] = $specialty->doctorSpecialty->name;
								}
								if(!empty($specialty->doctorSpecialty->description)) $serviceArr[$specialty->doctorSpecialty->link]['description'] = $specialty->doctorSpecialty->description;
							}
							if($resource['description'] == 'Врач') $resource['description'] .= " ".$specialty->doctorSpecialty->name;
							$schedule = ['@attributes' => ['res-id' => $doctor->link]];
							
							$serviceArr[$specialty->doctorSpecialty->link]['price'] = NULL;
							if($serviceArr[$specialty->doctorSpecialty->link]['price'] == NULL) {
								$serviceFirst = DoctorServices::model()->findByAttributes(["doctorId" => $doctor->id, "cassifierServiceId" => "54869cc8-4a4e-4f88-a20f-cb5a5a7bb33f"]);
								$priceValue = NULL;
								if($serviceFirst) {
									if(is_numeric($serviceFirst->price)) $priceValue = $serviceFirst->price;
									if($serviceFirst->free) $priceValue = 0;
									if($priceValue > 0) $serviceArr[$specialty->doctorSpecialty->link]['price'] = ['@attributes' =>['currency' => 'RUB', 'value' => $priceValue]];
								}
							}
							
							foreach ($daysOfWeekMaskStr as $key=>$value) {
								$daysOfWeek = ['@attributes' => ['mask' => $daysOfWeekMaskStr[$key]['mask']]];
								$daysOfWeek['time-frame'] = $daysOfWeekMaskStr[$key]['timeFrame'];
								$schedule['days-of-week'][$daysOfWeekMaskStr[$key]['mask']] = $daysOfWeek;
							}
							$serviceArr[$specialty->doctorSpecialty->link]['schedules']['schedule'][] = $schedule;
						}
						if($experience = $doctor->getExperience()) {
							$resource['attributes']['attribute'][0] = ['@attributes' => ['name'=>'experience', 'value'=>$experience]];
						}
						if(!empty($specialty->doctorSpecialty->name)) $resource['attributes']['attribute'][] = ['@attributes' => ['name'=>'profession', 'value'=>$specialty->doctorSpecialty->name]];
					}
				} else {
					continue;
				}
				$reviewsModel = Review::model()->findAllByAttributes(['doctorId' => $doctor->id]);
				foreach ($reviewsModel as $review) {
					$resource["reviews"]["review"][] = [
							"locale" => "ru",
							"type" => "biz",
							"url" => "https://emportal.ru/klinika/".$row->company->linkUrl."/".$row->linkUrl."/".$doctor->linkUrl."#review".$review->id,
							"description" => $review->reviewText,
							"rating" => floatval($review->ratingDoctor)*5,
							"reviewer" => [
									"vcard" => [
											"fn" => $review->userName,
									],
							],
							"reviewsurl" => "https://emportal.ru/klinika/".$row->company->linkUrl."/".$row->linkUrl."/".$doctor->linkUrl."#reviews",
							"dtreviewed" => date("Y-m-dTH:i:s",strtotime($review->createDate)),
					];
				}
				$resourceArr[] = $resource;
			}
			foreach ($serviceArr as $link=>$value) {
				if($serviceArr[$link]['price'] == NULL) unset($serviceArr[$link]['price']);
			}
			foreach ($serviceArr as $id=>$service) {
				$service['@attributes'] = ['id' => $id];
				$company['services']['service'][] = $service;
			}
			if(count($resourceArr) > 0) {
				$company['resources']['resource'] = $resourceArr;
				$company['resources']['@attributes'] = ['resources-url' => "https://emportal.ru/klinika/".$row->company->linkUrl."/".$row->linkUrl];
			}
			if(count($resourceArr) <= 1) {
				if(count($serviceArr) > 0) {
					$company['book-mode']['@value'] = 'dynamic-service-only';
				} else {
					$company['book-mode']['@value'] = 'blind';
				}
			}
			/* */
			
			$companies['company'][] = $company;
		}
		
		//print_r($companies);
		$xml = Array2XML::createXML('companies', $companies);
		//$xml->save('address.xml');
		header("Content-type: text/xml; charset=utf-8");
		echo $xml->saveXML() . "\n";
	}
	
	public function actionGetAddresses() {		
		$output_html = '';
		$criteria = new CDbCriteria;
		$criteria->with = ['userMedicals'=>['together' => true, 'joinType' => 'INNER JOIN'], 'company'=>['together' => true, 'joinType' => 'INNER JOIN']];
		$criteria->addCondition('cityId  = \'534bd8b8-e0d4-11e1-89b3-e840f2aca94f\'');
		$criteria->addCondition('isActive = 1');
		$criteria->addCondition('userMedicals.agreementNew = 1');
		$criteria->addCondition('userMedicals.createDate > 0');
		$criteria->order = 'company.name ASC';
		$Addresses = Address::model()->findAll($criteria);
		foreach($Addresses as $data) {
			
			$output_html .= '<tr><td>'.$data->company->name.'</td><td>'.$data->name.'</td><td>https://emportal.ru/klinika/'.$data->company->linkUrl.'/'.$data->linkUrl.'?p=sk#psk</td></tr>';
		}
		$output_html = '<table class="s_table"><tr><td>Клиника</td><td>Адрес</td><td>Ссылка</td></tr>'.$output_html.'</table>';
		$this->render('blankpage', ['output_html' => $output_html]);
	}
	
	public function actionAmoCRM() {
		$loader = new AmoCRM();
		$method = Yii::app()->request->getParam('method');
		$params = Yii::app()->request->getParam('params');
		if(method_exists($loader, $method)) {
			$response = call_user_func_array(array($loader,$method), is_array($params) ? $params : []);
		} else {
			$response = $loader->callServer($method, $params);
		}

		print_r($response);

	}

	public function actionEmias() {
		$loader = new EmiasLoader();
		$type = Yii::app()->request->getParam('type');
		$method = Yii::app()->request->getParam('method');
		$params = Yii::app()->request->getParam('params');
		if(method_exists($loader, $method)) {
			$response = call_user_func_array(array($loader,$method), is_array($params) ? $params : []);
		} else {
			$response = $loader->callServer($method, $params);
		}
		switch ($type) {
			case 'json':
				echo json_encode($response);
				break;
			default:
				print_r($params);
				print_r($response);
				break;
		}
	}
	public function actionEmiasReferrals() {
		$loader = new EmiasLoader();
		$searchModel = new Search(Search::S_DOCTOR);
		$searchModel->initOMSForm();
		$attr = [
				'omsNumber' => $searchModel->OMSForm_omsNumber,
				'omsSeries' => $searchModel->OMSForm_omsSeries,
				'birthDate' => date('Y-m-d\TH:i:s',strtotime($searchModel->OMSForm_birthday)),
		];
		$data = $loader->getReferralsInfo($attr);
		if(isset($data->return)) {
			if(!is_array($data->return)) {
				$data->return = [$data->return];
			} 
		}
		if(Yii::app()->request->getParam('json')) {
			echo json_encode($data);
		} else {
			$this->renderPartial('_emiasReferrals', ['data' => $data]);
		}
	}
	public function actionEmiasReceptions() {
		$loader = new EmiasLoader();
		$searchModel = new Search(Search::S_DOCTOR);
		$searchModel->initOMSForm();
		$attr = [
				'omsNumber' => $searchModel->OMSForm_omsNumber,
				'omsSeries' => $searchModel->OMSForm_omsSeries,
				'birthDate' => date('Y-m-d\TH:i:s',strtotime($searchModel->OMSForm_birthday)),
		];
		$data = $loader->getAppointmentReceptionsByPatient($attr);
		if(isset($data->return) && !is_array($data->return)) {
			$data->return = [$data->return];
		}
		if(Yii::app()->request->getParam('json')) {
			echo json_encode($data);
		} else {
			$this->renderPartial('_emiasReceptions', ['data' => $data]);
		}
	}
	
	public function actionNetrika() { //$idPat = 824178;
		$loader = new NetrikaLoader();
		$method = Yii::app()->request->getParam('method');
		$params = Yii::app()->request->getParam('params');
		if(!isset($params['guid'])) $params['guid'] = $loader->guid;
		print_r($params);
		$response = $loader->callServer($method, $params);
		print_r($response);
	}

	public function actionPopulateDoctorList($minExtId=0,$maxExtId=NULL) {
		$loader = new NetrikaLoader();
		$loader->populateDoctorList($minExtId,$maxExtId);
	}
	// http://local.emportal.ru/?r=site/PopulateSpecialityList&minExtId=0&maxExtId=0
	public function actionPopulateSpecialityList($minExtId=0,$maxExtId=NULL) {
		$loader = new NetrikaLoader();
		$loader->populateSpecialityList($minExtId,$maxExtId);
	}
	
	public function actionFIOExplode($fio) {
		$result = MyTools::FIOExplode($fio);
		print_r($result);
	}
	
	public function actionSoapTest($action=NULL) {
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$args = array('yiic', 'UpdateFromEmias', 'action'=>$action);
		ob_start();
		$runner->run($args);
		echo ob_get_clean();

		exit;
	}

	public function actionRedirectToMain() {
		$this->redirect(['site/index']);
	}
	
	public function actionRedirectToKlinikaUrl() {
		$redirectAddress = MyTools::getKlinikaRedirectAddress();
		$this->redirect($redirectAddress, TRUE, 301);
	}
	
	public function actionUserWithThisPhoneExists($phone = null) { //rm
		
		$user = User::userWithThisPhoneExists($phone);
	}
	
	public function actionLoadNetrika() {
	
		$loader = new NetrikaLoader('demo');
		$loader->populateLPUList(); //1. получить ЛПУ, наполнить extAddress
		$loader->populateSpecialityList(); //2. получить специальности, наполнить extSpeciality
		$loader->populateDoctorList(); //3. получить врачей, наполнить extDoctors
		$loader->collateDoctors(); //4. сопоставить врачей из extDoctors c doctors
	}
	
	public function actionGetMetroAndDistrictList()
    {
		header("Access-Control-Allow-Origin: *");
		$metroList = MetroStation::getActualStations(City::model()->selectedCityId);
		$districtList = CityDistrict::getActualDistricts(City::model()->selectedCityId);
		$mixedArr = [
			'metroList' => $metroList,
			'districtList' => $districtList,
		];
		echo CJSON::encode($mixedArr);		
	}
    
    public function actionGetDoctorSpecialties()
    {
        header("Access-Control-Allow-Origin: *");
        $s = new Search();
        $data = $s->doctorSpecialties;
        echo CJSON::encode($data);
    }

	//generate sitemap
	public function actionGenerateSM() {
		
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$args = array('yiic', 'GenerateSitemap');
		ob_start();
		$runner->run($args);
		echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
	}
	
	public function actionTestPush() {
		
		$notificationMessage = [
			'ClinicId' => '000001315',
			'Title' => 'Клиника44 (тестовый заголовок)',
			'Message' => 'Пациент33 214 (тестовый текст)',
		];
		
		$ns = new PushNotificationSender();
		$result = $ns->sendNotification($notificationMessage);
		/*
		$pk = 'EMPORTAL-f88a-3017-1b24-f2e279736f3a';
		$obj = AppointmentToDoctors::model()->findByAttributes(['id' => $pk]);
		//var_dump($obj);
		
		if ($obj->address->userMedicals->tabletAppRegistrationId) {
			echo '\n uMname = ' . $obj->address->userMedicals->name . ' \n';
			
			$notificationMessage = [
				'ClinicId' => $obj->address->userMedicals->name,
				'Title' => 'Тестовый тест33',
				'Message' => $obj->company->name . ': у вас новая заявка на прием1',
			];
			
			$ns = new PushNotificationSender();
			$result = $ns->sendNotification($notificationMessage);
						

		}*/
		echo '<br><br> notificationMessage';
		var_dump($notificationMessage);
		echo '<br><br> result';
		var_dump($result);
		echo '<br><br> ns';
		var_dump($ns);
	}
	
	public function actionSetRegion($regionSubdomain = null) {
		
		header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET");
		
		echo City::model()->changeRegion($regionSubdomain);
	}
	
	//generate sitemap
	public function actionExecCommand($commandName, $ar = null) {
		set_time_limit(1500);
		$user = Yii::app()->user->model;
		if (!Yii::app()->user->isGuest AND !Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA'))
		{
			echo 'Недостаточно прав на совершение действия';
			Yii::app()->end();
		}
		//http://local.emportal.ru/site/execCommand?commandName=UpdateCompanysCitySubdomain&ar=moskva
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$args = array('yiic', $commandName); //UpdateShortSpecialtyOfDoctor
		if (!empty($ar))
            $args[] = $ar;
		
		ob_start();
		$runner->run($args);
		echo ob_get_clean(); //htmlentities(ob_get_clean(), null, Yii::app()->charset);
	}
	
	public function actionRemoveDuplicateOfSpecialities() {
		foreach (DoctorSpecialty::$specialitiesAndDuplicates as $key=>$duplicates) {
			$dsmodel = DoctorSpecialty::model()->findAllByAttributes(['name'=>$key]);
			if(isset($dsmodel[0])) {
				echo $dsmodel[0]->name;
				foreach ($duplicates as $name) {
					$duplicatemodel = DoctorSpecialty::model()->findAllByAttributes(['name'=>$name]);
					foreach ($duplicatemodel as $model) {
						if(SpecialtyOfDoctor::model()->updateAll(["doctorSpecialtyId"=>$dsmodel[0]->id], ["condition"=>"doctorSpecialtyId=".Yii::app()->db->quoteValue($model->id)])) {
							echo "::Обновил";
						} else {
							echo "::Не обновил";
						}
						DoctorSpecialtyDisease::model()->deleteAll(["condition"=>"doctorSpecialtyId=".Yii::app()->db->quoteValue($model->id)]);
						try {
							if($model->delete()) echo "::Удалил дубль!";
						} catch (Exception $e) {
							echo "::Не удалил дубль!";
						} 
					}
				}
				echo "<br>";
			}
		}
	}
	
	public function actionSitemap($subdomain=NULL) {
		//error_reporting(FALSE);
		$file = Yii::getPathOfAlias('uploads.regions.'.Yii::app()->session['selectedRegion'].((Yii::app()->params["samozapis"]) ? ".samozapis" : "")).DIRECTORY_SEPARATOR."sitemap.xml";
		if(file_exists($file)) {
		    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
		    // если этого не сделать файл будет читаться в память полностью!
		    if (ob_get_level()) {
		      ob_end_clean();
		    }
			header("Content-type: text/xml; charset=utf-8");
			readfile($file);
		} else {
			header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			echo "Файл не найден!";
		}
	}

	public function actionReviews($subdomain=NULL) {
		//error_reporting(FALSE);
		$file = Yii::getPathOfAlias('uploads.regions.'.Yii::app()->session['selectedRegion'].((Yii::app()->params["samozapis"]) ? ".samozapis" : "")).DIRECTORY_SEPARATOR."reviews.xml";
		if(file_exists($file)) {
		    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
		    // если этого не сделать файл будет читаться в память полностью!
		    if (ob_get_level()) {
		      ob_end_clean();
		    }
			header("Content-type: text/xml; charset=utf-8");
			readfile($file);
		} else {
			header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			echo "Файл не найден!";
		}
	}

	public function actionRobots() {
		header("Content-Type: text/plain; charset=utf-8");
		if(!MyTools::validateHOST(Yii::app()->params) || count(array_intersect(["dev1","dev2","dev3","dev4","dev5","dev6","dev7","dev8","dev9","spb"], explode('.', $_SERVER['HTTP_HOST']))) > 0) {
			print ("
				User-agent: *
				Disallow: /
			");
		} else {
			print (
"User-agent: Yandex
Disallow: /register
Disallow: /login
Disallow: /recovery
Disallow: /terms_of_use.html
Disallow: /site/contact
Disallow: /ajax/
Disallow: /*/map$"
.(count(explode('.', $_SERVER['HTTP_HOST'])) > 2 ? PHP_EOL.
"Disallow: /news$".PHP_EOL.
"Disallow: /news/*".PHP_EOL.
"Disallow: /handbook/disease$".PHP_EOL.
"Disallow: /handbook/disease/*".PHP_EOL.
"Disallow: /site/contact$".PHP_EOL.
"Disallow: /team.html".PHP_EOL.
"Disallow: /pressa.html".PHP_EOL.
"Disallow: /about_us.html".PHP_EOL.
"Disallow: /privacy_policy.html".PHP_EOL.
"Disallow: /terms_of_use.html" : "").PHP_EOL.
"Disallow: /*?yclid=*".PHP_EOL.
"Disallow: /doctor*Search*".PHP_EOL.
"Disallow: /doctor*page=*".PHP_EOL.
"Disallow: /diagnostics*Search*".PHP_EOL.
"Disallow: /diagnostics*page=*".PHP_EOL.
"Disallow: /clinic*Search*".PHP_EOL.
"Disallow: /clinic*page=*".PHP_EOL.
"Disallow: /clinic/map/00*".PHP_EOL.
"Disallow: /klinika*Search*".PHP_EOL.
"Disallow: /klinika*page=*".PHP_EOL.
"Disallow: /klinika/map/00*".PHP_EOL.
"Disallow: /admin*".PHP_EOL.
"Disallow: /handbook/disease?Search$".PHP_EOL.
"Disallow: /handbook/disease*page*".PHP_EOL.
"Disallow: /handbook/medicament*".PHP_EOL.
"Disallow: /widget/*".PHP_EOL.
"Disallow: /doctor/000*.html$".PHP_EOL.
"Disallow: *?%*".PHP_EOL.
"
Host: https://".$_SERVER['HTTP_HOST']."

User-agent: *
Disallow: /register
Disallow: /login
Disallow: /recovery
Disallow: /terms_of_use.html
Disallow: /site/contact
Disallow: /ajax/
Disallow: /*/map$
Disallow: /clinic/addresses/
Disallow: /clinic/comments_experts/
Disallow: /clinic/price/
Disallow: /klinika/addresses/
Disallow: /klinika/comments_experts/
Disallow: /klinika/price/
Disallow: /doctor/price/
Disallow: /doctor/visit/
Disallow: /handbook/doctorSpecialty/
Disallow: /insurance/
Disallow: /m/
Disallow: /mobile/
Disallow: /search/medicament/
Disallow: /tourism/
Disallow: /*visit*
Disallow: /*price*"
.(count(explode('.', $_SERVER['HTTP_HOST'])) > 2 ? PHP_EOL.
"Disallow: /news$".PHP_EOL.
"Disallow: /news/*".PHP_EOL.
"Disallow: /handbook/disease$".PHP_EOL.
"Disallow: /handbook/disease/*".PHP_EOL.
"Disallow: /site/contact$".PHP_EOL.
"Disallow: /team.html".PHP_EOL.
"Disallow: /pressa.html".PHP_EOL.
"Disallow: /about_us.html".PHP_EOL.
"Disallow: /privacy_policy.html".PHP_EOL.
"Disallow: /terms_of_use.html" : "").PHP_EOL.
"Disallow: /*?yclid=*".PHP_EOL.
"Disallow: /clinic*Search*".PHP_EOL.
"Disallow: /clinic*page=*".PHP_EOL.
"Disallow: /klinika*Search*".PHP_EOL.
"Disallow: /klinika*page=*".PHP_EOL.
"Disallow: /doctor*Search*".PHP_EOL.
"Disallow: /doctor*page=*".PHP_EOL.
"Disallow: /diagnostics*Search*".PHP_EOL.
"Disallow: /diagnostics*page=*".PHP_EOL.
"Disallow: /admin*".PHP_EOL.
"Disallow: /handbook/disease?Search$".PHP_EOL.
"Disallow: /handbook/disease*page*".PHP_EOL.
"Disallow: /handbook/medicament*".PHP_EOL.
"Disallow: /widget/*".PHP_EOL.
"Disallow: /doctor/000*.html$".PHP_EOL.
"Disallow: *?%*".PHP_EOL.
"
					
Sitemap: https://".$_SERVER['HTTP_HOST']."/sitemap.xml");
		}
	}
	public function actionImage() {
	    function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
	    	$cut = imagecreatetruecolor($src_w, $src_h); // creating a cut resource
	    	imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h); // copying relevant section from background to the cut resource
	    	imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h); // copying relevant section from watermark to the cut resource
	    	imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct); // insert cut resource to destination image
	    }
	    
	    if (!extension_loaded('gd')) { // Проверяем установку библиотеки GD
	    	header('Content-Type: text/html; charset=UTF-8',true);
	        echo 'GD не установлено. Обратитесь к администратору сайта!';
	        exit(0);
	    } else {
	    	//header('Content-Type: text/html; charset=UTF-8',true);
	        //echo 'GD установлено. Успех!';
	        //exit(1);
	    }
	    error_reporting(0); // это для того, чтобы скрипт не выводил возможные ошибки, нам нужно просто выдать картинку, или не выдать ее, если что-то пойдет не так

	    if(preg_match('/[.]{2}/', $_GET['src'])) {
	    	exit(0);
	    }
	    
	    $basepath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . "..";
	    $cache = 'cache' . DIRECTORY_SEPARATOR;
	    
	    $pathexplode = explode('/', substr($_GET['src'],0,($stripos_GETsrc = stripos($_GET['src'], "?")) ? $stripos_GETsrc : strlen($_GET['src'])));
	    if(COUNT($pathexplode) == 1) { array_unshift($pathexplode,"uploads","gallery"); }
	    $img = array_pop($pathexplode);
	    
	    $path = "";
	    foreach ($pathexplode as $value) { if(!empty($value)) $path = $path . DIRECTORY_SEPARATOR . $value; }
	    $path .= DIRECTORY_SEPARATOR;

	    $image_path = $basepath.$path.$img;
	    $image_cache_path = $basepath.$path.$cache.$img;
	    
	    if(!file_exists($image_path))
	    {
			header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			echo "Файл не найден!";
	    	exit(0); // если такой картинки нет, то тупо вырубаемся, незачем тратить время на некорректные запросы
	    }
	    
	    list($width, $height) = getimagesize($image_path); // получаем размеры изображения
	    
	    if(($width > 200) && file_exists($image_cache_path)) // здесь мы проверяем, существует ли картинка с уже наложенным водяным знаком в папке cache, и если да, то...
	    {
			if(mb_strripos($img,".png") > 0) {
	    		$image = imagecreatefrompng($image_cache_path);
	    		header('Content-Type: image/png');
	    		imagepng($image);
			} else {
		    	$image = imagecreatefromjpeg($image_cache_path); // получаем ее идентификатор
		    	header('Content-Type: image/jpeg'); // отправляем браузеру HTTP заголовок ответа, мол, щас будет картинка
		    	imagejpeg($image); // и выливаем эту картинку браузеру
			}
	    	exit;
	    }
	    else // если же нет такой картинки в кэше
	    {
	    	
	    	if ($width <= 200) {
	    		$watermark_path = $basepath . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . 'badge.png';
		    	$final_x = $width - 100;
		    	$final_y = $height - 30;
	    	} else
	    	{
				$watermark_path = $basepath . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . 'wotermark.png';
		    	$final_x = 10;
		    	$final_y = 10;
	    	}
	    	// Настраиваем основные переменные и размещение
	    	$opacity = 90;
	    	// Загружаем изображения
			if(mb_strripos($img,".png") > 0) {
	    		$image = imagecreatefrompng($image_path);
			} else {
	    		$image = imagecreatefromjpeg($image_path);
			}
	    	$watermark = imagecreatefrompng($watermark_path);
			//если изначальное фото врача слишком узкое, делаем его шире
			list($image_width, $image_height) = getimagesize($image_path);
			if ($image_width < 140)
			{
				$scale_coeff = 140/$image_width;
				$new_image_width = 140;
				$new_image_height = round($image_height * $scale_coeff);
				$new_image = imagecreatetruecolor($new_image_width, $new_image_height);
				//imagecopyresized($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $image_width, $image_height);
				//imagecopyresampled медленнее imagecopyresized, но качество изображений заметно выше
				//см. https://blogs.oracle.com/oswald/entry/scaling_images_in_php_done
				imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_image_width, $new_image_height, $image_width, $image_height);
				$image = $new_image;
		    	$final_x = $new_image_width - 100;
		    	$final_y = $new_image_height - 30;
			}
	    	// Введем и сохраним высоту и ширину наших изображений
	    	list($watermark_width, $watermark_height) = getimagesize($watermark_path);
	    	// Установим окончательную позицию водного знака в зависимости от отступов и размера
	    	// Скопируем наш водный знак на оригинальное изображение
	    	imagecopymerge_alpha($image, $watermark, $final_x, $final_y, 0, 0, $watermark_width, $watermark_height, $opacity);
	    	//Настраиваем элемент header, выводим изображение, освобождаем память
			if($width > 200) {
		    	if (!file_exists($basepath.$path.$cache))
		    	{
		    		mkdir($basepath.$path.$cache, 0777); // если папка cache куда-то запропастилась, создаем ее
		    	}
				if(mb_strripos($img,".png") > 0) {
		    		imagepng($image, $image_cache_path, 90);
				} else {
		    		imagejpeg($image, $image_cache_path, 90);
				}
			}
			if(mb_strripos($img,".png") > 0) {
				imagesavealpha($image, true); 
	    		header('Content-Type: image/png');
	    		imagepng($image);
			} else {
		    	header('Content-Type: image/jpeg');
		    	imagejpeg($image);
			}
	        imagedestroy($image);
	    	imagedestroy($watermark);
	    	/* просто выдаем изображение
	    	{
	    		$image = imagecreatefromjpeg($image_path);
	    		header('Content-Type: image/jpeg');
	    		imagejpeg($image);
	    	} */
	    }
	}
	
	public function actionTotalAppointments($year=false,$month=false) {
		$condition = ($year !== false ? "(" . ($month !== false ? ("MONTH(atd.createdDate)=".intval($month)." AND ") : "") . ($year !== false ? ("YEAR(atd.createdDate) = " . intval($year)) : "") . ") AND " : "")."atd.statusId != 7 AND atd.appType != '' AND atd.appType != '".AppointmentToDoctors::APP_TYPE_OWN."'";
		$command = Yii::app()->db->createCommand()
		->select("appType,COUNT(*)`count`")
		->from("appointmentToDoctors atd")
		->where($condition)
		->group("appType");
		$result = $command->queryAll();
		$total = 0.0;
		echo "<html><body><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>" . PHP_EOL;
		foreach ($result as $row) {
			echo AppointmentToDoctors::getStatusesAppType()[$row["appType"]] . " : " . $row["count"] . " <br> " . PHP_EOL;
			$total += floatval($row["count"]);
		}
		echo " <br> ИТОГО : " . $total . " <br> " . PHP_EOL;
		echo "</body></html>";
		
	}
	
	public function actionStartCommand($name) {
		/*
		*/
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$runner->run(['yiic', $name]);
	}
	
	public function actionKassa($type = null) {
		
		$requestParams = array_merge($_GET, $_POST);
		
		if (in_array($type, ['check', 'aviso']) && !LogKassa::validateControlSum($requestParams))
			exit('invalid cs data');
		
		unset($requestParams['type']);
		foreach($requestParams as $key => $value)
			$requestParams[$key] = filter_var($value, FILTER_SANITIZE_STRING);
		
		$logKassa = new LogKassa();
		$logKassa->attributes = [
			'type' => $type,
			'requestParamsStr' => CJSON::encode($requestParams),
		];
		$logKassa->save();
		
		
		
		//var_dump($logKassa->attributes);
		//var_dump($logKassa->getErrors());
		
		//echo 'Kassa type: ' . $type;
		//echo '<br>';
		//var_dump(getdate(time()));
		switch($type) {
			case 'check':
				$responseAtrs = [
					'performedDatetime' => date(DATE_ATOM, time()),
					'code' => '0',
					'invoiceId' => (isset($requestParams['invoiceId'])) ? $requestParams['invoiceId'] : '1234567',
					'shopId' => Yii::app()->params['api']['kassa']['shopId'],
				];
				$xml = Array2XML::createXML('checkOrderResponse', ['@attributes' => $responseAtrs]);
				header("Content-type: application/xml; charset=utf-8");
				echo $xml->saveXML() . "\n";
				exit;
				break;
			case 'aviso':
				$responseAtrs = [
					'performedDatetime' => date(DATE_ATOM, time()),
					'code' => '0',
					'invoiceId' => (isset($requestParams['invoiceId'])) ? $requestParams['invoiceId'] : '1234567',
					'shopId' => Yii::app()->params['api']['kassa']['shopId'],
				];
				$xml = Array2XML::createXML('paymentAvisoResponse', ['@attributes' => $responseAtrs]);
				header("Content-type: application/xml; charset=utf-8");
				echo $xml->saveXML() . "\n";
				//тут надо делать соотв. пометки к соотв. счету bill, мол, оплачен
				$bill = Bill::model()->findByPk($requestParams['billNumber']);
				$bill->scenario = 'onlinePayment';
				$bill->sumPayedOnline += $requestParams['orderSumAmount'];
				if ($bill->sumPayedOnline >= $bill->sum)
					$bill->paymentStatusId = 1;
				$bill->update();
				/*
				 * // Создать платёж
				*/
				try {
					$payment = new Payment();
					$payment->createDate = date("Y-m-d H:i:s", time());
					$payment->addressId = $bill->addressId;
					$payment->billId = $bill->id;
					$payment->sum = $requestParams['orderSumAmount'];
					$payment->purpose = "Оплата по счету №".$bill->id;
					$payment->logId = NULL;
					$payment->statusId = Payment::STATUS_APPROVED;
					$payment->sourceId = "kassa_yandex";
					$payment->save();
				} catch (Exception $e) { }
				exit;
				break;
			case 'success':
				$this->redirect(Yii::app()->getBaseUrl(true) . '/adminClinic/default/documents?paymentSuccess=true');
				break;
			case 'fail':
				$this->redirect(Yii::app()->getBaseUrl(true) . '/adminClinic/default/documents?paymentSuccess=false');
				break;
			default:
				//echo 'default';
				break;
		}
	}
	
	public function actionNewCompany()
	{
		$userModel = (Yii::app()->user->isGuest) ? new User('register') : Yii::app()->user->model;
		$companyModel = new Company('self_register');
		$addressModel = new Address('self_register');
		
		$companyModel->attributes = Yii::app()->request->getParam(get_class($companyModel));
		$addressModel->attributes = Yii::app()->request->getParam(get_class($addressModel));
		
		//если новый юзер, привязываем атрибуты
		if ($userModel->isNewRecord)
		{
			$preDefinedUserData = [
				'sexId' => Sex::getDefaultValue(),
				'status' => 1,
				'phoneActivationStatus' => 1,
				'phoneActivationDate' => date('Y-m-d H:i:s'),// '2014-09-25 14:15:01',
			];
			$userModelData = (array)Yii::app()->request->getParam(get_class($userModel));
			$userModel->attributes = array_merge($userModelData, $preDefinedUserData);
			$presavedPass = $userModelData['password'];
			
			//var_dump([
			//	'$userModel->attributes' => $userModel->attributes,
			//	'$userModel->validate()' => $userModel->validate(),
			//	'errors' => $userModel->getErrors(),
			//]);
		}
		
		if (!empty($_POST) && (!$userModel->isNewRecord OR $userModel->validate()) && $companyModel->validate() && $addressModel->validate())
		{
			ob_start();
			$companyModel->nameRUS = $companyModel->name;
			$companyModel->nameENG = 'isViaSelfRegister';
			
			if ($companyModel->save())
			{
				var_dump(['saved new company!']);
				
				if ($userModel->isNewRecord && $userModel->save())
					var_dump(['saved new user!']);
				
				$addressModel->ownerId = $companyModel->id;
				$addressModel->isActive = 0;
				$addressModel->name = $addressModel->street . ', ' . $addressModel->houseNumber;
				
				$companyModel->denormCitySubdomain = City::model()->findByPk($addressModel->cityId)->subdomain;
				$companyModel->addressId = $addressModel->id;
				$companyModel->update();
				
				if ($addressModel->save())
				{
					var_dump(['saved new address!']);
					
					//создаем договор для адреса
					$companyContractModel = new CompanyContract();
					$indexNum = (Yii::app()->db->createCommand('SELECT MAX(indexNum) FROM `' . CompanyContract::model()->tableName() . '`')->queryScalar() + 1);
					$contractNumber = 'КО-1/' . $indexNum;
					$companyContractModel->attributes = [
						'contractNumber' => $contractNumber,
						'indexNum' => $indexNum,
						'name' => $companyModel->name,
						'periodOfValidity' => '2019' . date('-m-d H:i:s'),
						'companyId' => $companyModel->id,
					];
					if ($companyContractModel->save())
						var_dump(['saved new contract!']);
					
					//создаем аккаунт пользователя клиники
					$userMedicalModel = new UserMedical();
					$userMedicalModel->attributes = [
						'addressId' => $addressModel->id,
						'companyId' => $companyModel->id,
						'companyContractId' => $companyContractModel->id,
						'status' => 0,
						'agreement' => 0,
						'name' => $addressModel->link,
						'createdDate' => new CDbExpression("NOW()"),
						'email' => $userModel->email,
						'phoneForAlert' => $userModel->telefon,
						//email ?
					];
					if ($userMedicalModel->save())
						var_dump(['saved new userMedical!']);
					
					//привязываем юзера как владельца ЛКК через добавление соотв. права
					$rightModel = new Right('self_register');
					$rightModel->attributes = [
						'userId' => $userModel->id,
						'zRightId' => RightType::IS_OWNER_OF_ADDRESS,
						'value' => $addressModel->id,
					];
					if ($rightModel->save())
						var_dump(['saved new rightModel!']);
					
					//надо также привязать к свежесозданной клинике тариф по умолчанию
					$salesContractModel = new SalesContract();
					$salesContractModel->attributes = [
						'addressId' => $addressModel->id,
						'salesContractTypeId' => SalesContractType::model()->findByAttributes(['isFixed'=>'1', 'price'=>'990'])->id,
					];
					if ($salesContractModel->save())
						var_dump(['saved new salesContractModel!']);
					
					//выбрать менеджера из тех, к кому можно привязать новую компанию
					$rights = Right::model()->findAllByAttributes([
						'zRightId' => RightType::CAN_RECEIVE_SELF_REGISTERED_COMPANIES
					]);
					$selectedManager = $rights[array_rand($rights)]->user;
					
					//и привязать этого менеджера к новой компании
					$managerRelationModel = new ManagerRelation();
					$managerRelationModel->attributes = [
						'userId' => $selectedManager->id,
						'companyId' => $companyModel->id,
					];
					if ($managerRelationModel->save())
						var_dump(['saved new managerRelationModel!']);
					
					//также надо завести соотв. служебную информацию об ЛПР для карточки клиники в модуле newAdmin
					$companySalesInfo = new CompanySalesInfo();
					$companySalesInfo->attributes = [
						'companyId' => $companyModel->id,
						'lprName' => $userModel->name,
						'lprPosition' => Yii::app()->request->getParam('lprPosition'),
						'lprPhone' => $userModel->telefon,
						'email' => $userModel->email,
						'website' => Yii::app()->request->getParam('website'),
					];
					var_dump($companySalesInfo->attributes);
					if ($companySalesInfo->save())
						var_dump(['saved new companySalesInfo!']);
					
					//в итоге логиним и редиректим юзера в свежесозданный ЛКК
					//rf: UserController/actionAdministrate - same functionality
					var_dump(['will redirect to lkk']);
					Yii::app()->setModules(array('adminClinic'));
					$adminClinicModule = Yii::app()->getModule('adminClinic');
					
					//ненадежно, надо логинить другим механизмом
					//$identity = new UserIdentity('allAdmin5', 'letmeinplease26', $userMedicalModel->id);
					//Yii::app()->user->loginUrl = "/adminClinic/default/login";
					//Yii::app()->user->identityCookie = ["domain" => ".emportal.ru"];
					//Yii::app()->user->setStateKeyPrefix('admin_clinic_user_');
					//Yii::app()->user->allowAutoLogin = true;
					//Yii::app()->user->login($identity, 60*60);
					//$this->redirect(array('/user/administrate'));
					
					if ($presavedPass)
					{
						$identity = new UserIdentity($userModel->email, $presavedPass, $userModel->id);
						Yii::app()->user->loginUrl = "/site/login";
						Yii::app()->user->login($identity, 60*60);
					}
					$this->redirect(array('/user/administrate'));
				}
			}
		}
		
		$this->render('newCompany', [
			'userModel' => $userModel,
			'companyModel' => $companyModel,
			'addressModel' => $addressModel,
		]);
	}
	
	public function actionEuromodelLoader($method=null)
	{
		$params = $_REQUEST["params"];
		$loader = new EuromodelLoader();
		switch ($method) {
			default:
				$result = call_user_func_array(array($loader,$method), is_array($params) ? $params : []);
				print_r(["result"=>$result]);
			break;
		}
	}
	
	public function actionResizeImages() {
		function GetListFiles($folder){
			$fp=opendir($folder);
			$all_files = array();
			while($cv_file=readdir($fp)) {
				if(is_file($folder.DIRECTORY_SEPARATOR.$cv_file)) {
					$all_files[] = ["folder" => $folder, "name" => $cv_file];
				}elseif($cv_file!="." && $cv_file!=".." && is_dir($folder.DIRECTORY_SEPARATOR.$cv_file)){
					//$all_files = array_merge($all_files, GetListFiles($folder.DIRECTORY_SEPARATOR.$cv_file));
				}
			}
			closedir($fp);
			return $all_files;
		}
		// Создать список файлов в катологе и, рекурсивно во всех подкаталогах.
		$path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "gallery";
		$all_files = GetListFiles($path);
		
		$index = 0;
		// Перебрать в цикле все файлы
		foreach ($all_files as $file) {
			// Загружаем изображение
			$image = Yii::app()->image->load($file["folder"] . DIRECTORY_SEPARATOR . $file["name"]);
			// Если название файла начинается с нижнего подчёркивания (Превью)
			if($file["name"][0] === "_") {
				// Если размер изображения больше максимального, то изменяем размер
				if($image->height > 100) {
					$image->resize(null, 100)->save($file["folder"] . DIRECTORY_SEPARATOR . $file["name"]);
					
					echo "Preview: " . $file["folder"] . DIRECTORY_SEPARATOR . $file["name"] . PHP_EOL;
					$index++;
				}
			} else {
				// Если размер изображения больше максимального, то изменяем размер
				if($image->width > 800 OR $image->height > 800) {
					$image->resize(800, 800)->save($file["folder"] . DIRECTORY_SEPARATOR . $file["name"]);
					
					echo "Original: " . $file["folder"] . DIRECTORY_SEPARATOR . $file["name"] . PHP_EOL;
					$index++;
				}
			}
		}
	}
	
	public function actionGrayscale() {
		if (!extension_loaded('gd')) {
			header('Content-Type: text/html; charset=UTF-8',true);
			echo 'GD не установлено. Обратитесь к администратору сайта!';
			exit(0);
		} else {
			//echo 'GD установлено';
		}
		error_reporting(0);
	
		if(preg_match('/[.]{2}/', $_GET['src'])) {
			exit(0);
		}
		
		if(in_array(substr(trim(mb_strtolower($_GET['src'])), 0, 5), ["http:","https","ftp:/","ftps:"])) {
			$image_path = $_GET['src'];
		} else {
			$basepath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . "..";
			$pathexplode = explode('/', substr($_GET['src'],0,($stripos_GETsrc = stripos($_GET['src'], "?")) ? $stripos_GETsrc : strlen($_GET['src'])));
			if(COUNT($pathexplode) == 1) { array_unshift($pathexplode,"uploads","gallery"); }
			$img = array_pop($pathexplode);
			$path = "";
			foreach ($pathexplode as $value) { if(!empty($value)) $path .= DIRECTORY_SEPARATOR . $value; }
			$path .= DIRECTORY_SEPARATOR;
			$image_path = $basepath.$path.$img;
			if(!file_exists($image_path))
			{
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
				echo "Файл не найден!";
				exit(0);
			}
		}
		
		if(substr(trim(mb_strtolower($image_path)), -4) === '.png') {
			$format = "png";
			$image = imagecreatefrompng($image_path);
		} else {
			$format = "jpeg";
			$image = imagecreatefromjpeg($image_path);
		}
		imagefilter($image, IMG_FILTER_GRAYSCALE);
		//imagefilter($image, IMG_FILTER_BRIGHTNESS, -10);
		//imagefilter($image, IMG_FILTER_CONTRAST, -10);

		header('Content-Type: image/'.$format);
		imagejpeg($image);
		imagedestroy($image);
	}
	
	public function actionGitPull() {
		$allow = false;
		$whiteList = [
				"127.0.0.1",
				"131.103.20.160/27",
				"165.254.145.0/26",
				"104.192.143.0/24",
				"92.255.15.246",
				"95.55.87.0/24",
				"178.71.251.93",
				"92.255.15.243"
		];
		foreach ($whiteList as $CIDR) {
			if(MyTools::ipCIDRCheck(MyTools::getClientIp(), $CIDR)) {
				$allow = true;
				break;
			}
		}
		if($allow) {
			$cmd = "sudo git pull 2>&1";
			$output = shell_exec($cmd);
			echo "<pre>".PHP_EOL.htmlentities($output).PHP_EOL."</pre>";
			MyTools::StartCommand("RemoveCache");
		} else {
			throw new CHttpException(403,'You are not authorized to per-form this action.');
		}
	}
	
	public function actionUpload1CFile() {
		$response = [
			'success' => false,
			'errors' => [ ],
		];
		
		if(Yii::app()->user->model->hasRight('CAN_UPLOAD_DOCUMENTS_FROM_1C')) {
			if(!empty($_FILES['document']['tmp_name'])){
	    		$tmp_path = $_FILES['document']['tmp_name'];
				$fileHandle = fopen($tmp_path, 'r');
				if($fileHandle) {
					$response['success'] = true;
					$response['result'] = Payment::upload1CFile($fileHandle, $addressId);
				}
	    	}
		} else {
			$response['errors'] = 'Не достаточно прав!';
		}
    	echo json_encode($response);
	}

	public function actionHelp() {
		$model = new Help();
		$result = 0;

		if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'help-form') {
			if (!$model->validate()) {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}

		if(isset($_POST[get_class($model)]))
		{
			$model->attributes = $_POST[get_class($model)];
			$model->comment = strip_tags($_POST[get_class($model)]['comment']);

			if($model->validate() && $model->save()) {
				$result = 1;
				$mail = new YiiMailer();
				$mail->setFrom('robot@emportal.ru', 'Emportal.ru');
				$mailList = ['adm@emportal.ru','ALEXANDER.D@EMPORTAL.RU','dima90-dima@mail.ru'];
				//$mailList = ['dima90-dima@mail.ru'];
				foreach($mailList as $mailAddress)
					$mail->AddAddress($mailAddress);
		        
		        $mail->setData([
					'model' => $model,
				]);
				$mail->setView('help_email');
				$mail->Subject = "Заявка на подбор врача с emportal.ru";
				
				$mail->render();
				$mail->Send();
			}
		}

		$this->layout = '//layouts/column1_for_old_pages';
		$this->render('help', ['model' => $model, 'result' => $result]);
	}

	public function actionMailFeedback() {
		$reason = Yii::app()->request->getParam('reason');
		if(!empty($reason)) {
			$feed = new UserMailFeedback();
			$text = '';
			if($reason == 1) {
				$text = 'изменились мои планы';
			}
			elseif($reason == 2) {
				$text = 'в карточке клиники или врача была указана неверная информация '.Yii::app()->request->getParam('reasonText2');
			}
			elseif($reason == 3) {
				$text = 'записался в другом месте '.Yii::app()->request->getParam('reasonText3');
			}
			elseif($reason == 4) {
				$text = 'просто опробовал сервис';
			}

			$feed->link = Yii::app()->request->getParam('link');
			$feed->text = $text;
			$feed->save();

		 	$this->layout = '';
		 	$text = 'Спасибо за ваш ответ!';
			$this->render('mailInfoPage', ['pageText' => $text]);
		}
		else {
			$this->redirect('/');
		}
	}

	public function actionMailUnsubscribe() {
		$cancelCode = Yii::app()->request->getParam('code');
		$searchAttributes = [
				'cancelCode' => $cancelCode,
		];
		$emailSubscription = EmailSubscription::model()->findByAttributes($searchAttributes);

		if($emailSubscription) {
			$emailSubscription->delete();

			$this->layout = '';
			$text = 'Вы отписаны от рассылки!';
			$this->render('mailInfoPage', ['pageText' => $text]);
		}
		else {
			$this->redirect('/');
		}
	}

	public function actionClinicList($city) {
		if(!$city) {
			echo "Не задан город";
			return false;
		}

		header("Content-type: text/csv; charset=utf-8");
		header('Content-Disposition: attachment; filename="clinicList_'.$city.'.csv"');

		$criteria = new CDbCriteria();
		$criteria->with = [
			'city', 'company', 'userMedicals'
		];
		$criteria->compare('city.subdomain', $city);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->compare('t.samozapis', 0);
		$criteria->compare('t.isActive', 1);
		$criteria->order = 'company.name ASC, city.name ASC, t.name ASC';

		$model = Address::model()->findAll($criteria);
		foreach ($model as $clinic) {
			if(!$clinic->company->link || !$clinic->link) {
				continue;
			}
			$name = iconv("UTF-8", "WINDOWS-1251", $clinic->company->name);
			$address = iconv("UTF-8", "WINDOWS-1251", $clinic->city->name.', '.$clinic->name);
			$name = str_replace('"', '\'', $name);
			echo $name.";".$address.";'".$clinic->link."';".$clinic->userMedicals->createDate."\n";
		}
	}

}
