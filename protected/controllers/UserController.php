<?php

class UserController extends LeftMenuController {

	public $layout = '//layouts/user';
	public $searchbox_type	 = SearchBox::S_TYPE_PATIENT;
	public $searchbox_tab	 = Search::S_DOCTOR;
	public $infoBox_type	 = false;
	public $menu			 = array(
		"Профиль"			 => 'profile',
		"Регистратура"		 => 'registry',
		//"Мои заказы"		 => 'orders',
		//"Услуги"			 => 'services',
		//"Избранное"			 => 'favorites',
		"Мои отзывы"	 => 'comments',
		//"Мои бонусы"	 => 'bonuses', //see beforeAction method
		/*"Удалить аккаунт"=>array(
            'label'=>'Удалить аккаунт', 
            'url'=>'#',
			'itemOptions' => array(
					'class' => 'btn w100'
			),
            'linkOptions'=>array("submit"=>array('delete'), 'confirm' => 'Вы действительно хотите удалить аккаунт?'),
        ),*/
			
	);
	private $userModel = null;

	/**
	 * Actions that are always allowed.
	 */
	public function allowedActions() {
		return 'error, login';
	}
	
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
		);
	}

	public function filters() {
		return array(
			array('application.filters.accessControlForMobile + profile'),
			'accessControl',			
			//array('application.filters.CheckExpirePhone'),   //Во всех экшенах проверяем не истек ли период валидности мобильного телефона   
            
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('bonuses'),
				'users' => array('*'),
			),
			array('deny',
				'actions'	 => array(),
				'users'		 => array('?'),
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {			
		$model = Yii::app()->user->model;	
		
		
		#var_dump($model->sex);
		/*if(!Yii::app()->user->model->phoneActivationStatus) {
					$this->redirect(array('site/ActivatePhone'));
		}*/

		//$this->render('index', array('user' => $model));
		
		$this->redirect(array('profile'));
	}

	public function actionProfile() {
		
		$model = $this->model;
		$model->scenario = 'updateProfile';
		if ( isset($_POST['ajax']) && $_POST['ajax'] === 'reg-form' ) {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if ( isset($_POST[get_class($model)]) ) {
			$model->attributes = $_POST[get_class($model)];			
			$model->birthday=date("Y-m-d",  strtotime($model->birthday));
					
			if ( $model->save() ) {
				if(isset($_REQUEST['JSON'])) {
					echo CJSON::encode(array('success'=>'Данные профиля успешно сохранены'));
					Yii::app()->end();
				} else {
					Yii::app()->user->setFlash('success','Данные успешно обновлены!');
					$this->redirect(array('user/profile'));
				}
			} elseif (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(array('error'=>$model->errors));
				Yii::app()->end();
			}			
		}

		if(isset($_REQUEST['JSON'])) {
		
			if (Yii::app()->user->isGuest) {

				echo CJSON::encode(array('authError'=>'Пользователь не авторизован'));
				Yii::app()->end();
			}
			
			$model->attributes = array('password'=>'password', 'comment'=>'comment');
			echo CJSON::encode($model->attributes);
			Yii::app()->end();
		}
		
		if($model->birthday == "1970-01-01 00:00:00" || $model->birthday == "1970-01-01" || $model->birthday == "") {		
			$model->birthday = "";
		} else {
			$model->birthday=explode(" ",$model->birthday)[0];
		}
		
		if($model->birthday)
			$model->birthday=date("d.m.Y",strtotime($model->birthday));
		
		//rf lastappointment
		$criteria = new CDbCriteria();
		$criteria->compare('userId', $model->id);
		$criteria->order = 'createdDate DESC';
		$criteria->limit = 1;		
		$lastAppointment = AppointmentToDoctors::model()->find($criteria);		
		
		$reachedGoal = Yii::app()->session['reachedGoal_appointment'];
		$this->render('profile', array('model' => $model, 'lastAppointment' => $lastAppointment, 'reachedGoal' => $reachedGoal));
		
		/*if(isset($_REQUEST['JSON'])) {
			
			if (!isset($_SERVER['PHP_AUTH_USER'])) {
				header('WWW-Authenticate: Basic realm="My Realm"');
				header('HTTP/1.0 401 Unauthorized');
				echo CJSON::encode(array('authError'=>'Ошибка авторизации'));
				Yii::app()->end();
			} else {
				$oLogin = new LoginForm;
				
				$oLogin->username = $_SERVER['PHP_AUTH_USER'];
				$oLogin->password = $_SERVER['PHP_AUTH_PW'];
				
				$oLogin->login();
			}
			
			$model->attributes = array('password'=>'password', 'comment'=>'comment');
			echo CJSON::encode($model->attributes);
			Yii::app()->end();
		}*/		
	}

	public function actionFavorites() {
		$model = Yii::app()->user->model;

		//$favoriteIds = array_map(create_function('$elem', 'return $elem->value;'), $model->favorites);

		$favoriteIds = $model->favoritesArray;

		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $favoriteIds);

		$doctors = Doctor::model()->findAll($criteria);

//		$criteria->condition = '';
//		$criteria->params	 = array();
//
//		$criteria->addInCondition('id', $favoriteIds);

		$clinics = Address::model()->findAll($criteria);

		$this->render('favorites', array(
			'model'		 => $model,
			'doctors'	 => $doctors,
			'clinics'	 => $clinics,
		));
	}

	public function actionRegistry() {
		
		$model = $this->model;

		$criteria = new CDbCriteria();
		$criteria->compare('userId', $model->id);
		//$criteria->addCondition('serviceId IS NULL');
		$criteria->order = 'createdDate DESC';
		//switch (Yii::app()->request->getParam('filter', 'topical')){
		//	case 'topical':
		//		$criteria->addCondition(" plannedTime > NOW() - INTERVAL 1 DAY");
		//		$criteria->addCondition(' statusId = '.AppointmentToDoctors::SEND_TO_REGISTER.
		//								' OR statusId = '.AppointmentToDoctors::SEEN.
		//								' OR statusId = '.AppointmentToDoctors::ACCEPTED_BY_REGISTER);		
		//		break;
		//}
		$dataProvider = new CActiveDataProvider('AppointmentToDoctors', array(
			'criteria' => $criteria,
		));
		$infoArr = [];
		foreach($dataProvider->getData() as $data) {
			$infoArr[] = [
				'createdDate' => Yii::app()->dateFormatter->formatDateTime($data->createdDate, "medium", null),
				'plannedTime' => $data->registryPlannedTime,
				'addressLink' => $data->address->link,
				'companyName' => $data->company->name,
				'doctor' => $data->doctor ?	['name' => $data->doctor->shortName,'url' => Yii::app()->createAbsoluteUrl('/doctor/view', ['link' => $data->doctor->link,'JSON'=>1])	] :	null,
				//'serviceName' => $data->service->name,
				'status' => $data->getStatusText(),
				'statusAction' => $data->StatusActionJSON
			];
		}

		if(isset($_REQUEST['JSON'])) {			
			echo CJSON::encode($infoArr);
			Yii::app()->end();
		}
		
		//rf lastappointment
		$criteria = new CDbCriteria();
		$criteria->compare('userId', $model->id);
		$criteria->order = 'createdDate DESC';
		$criteria->limit = 1;		
		$lastAppointment = AppointmentToDoctors::model()->find($criteria);
		
		$this->render('registry', array('model' => $model, 'dataProvider' => $dataProvider, 'lastAppointment' => $lastAppointment));
	}
	
	public function actionServices() {
		
        $this->redirect(['user/registry']);
		//$user = $this->model;
		//$criteria = new CDbCriteria();
		//$criteria->compare('userId', $user->id);
		//$criteria->order = 'createdDate DESC';
		//$dataProvider = new CActiveDataProvider('PurchasedService', array(
		//	'criteria' => $criteria,
		//));
		//$data = [
		//	'user' => $user,
		//	'dataProvider' => $dataProvider
		//];
		//$this->render('services', $data);
	}
	
	public function actionCancelVisit($link, $delete=0) {	
		$model = AppointmentToDoctors::model()->findByLink($link);
		if ( $model->userId != Yii::app()->user->id ) {
			if(isset($_REQUEST['JSON'])) {
				$response = ['success' => false,'msg' => 'Не достаточно прав для совершения этого действия'];
				echo CJSON::encode($response);
				Yii::app()->end();
			}
			$this->redirect(array('user/registry'));
		}
		if($delete) {
			//-Пока поставил руками, потом нужно будет закрепить статус отмены в модели.
			$model->setAttribute('statusId', AppointmentToDoctors::DECLINED_BY_PATIENT);

			if ($model->save()) {
				Yii::app()->user->setFlash('registrySuccess', 'Запись успешно отменена');
				if(isset($_REQUEST['JSON'])) {
					$response = ['success' => true,'msg' => 'Запись успешно отменена'];
					echo CJSON::encode($response);
					Yii::app()->end();
				}
				$this->redirect(array('/user/registry'));
			}
		}

		if(isset($_REQUEST['JSON'])) {
			$response = ['success' => false,'msg' => 'Ошибка при удалении записи на приём'];
			echo CJSON::encode($response);
			Yii::app()->end();
		}
		$this->render('cancelVisit', array('model' => $model));
	}

	public function actionComments() {
		$model = $this->model;

		//$comments = Comments::model()->findAllByAttributes(array('userId'=>$model->userId));

		//$clinics = Address::model()->with('reviews')->findAll('commentsAll.usersId = :userId', array('userId' => $model->id));

		$clinics = Review::model()->with('company')->findAll('`t`.`userId` = :userId and `t`.ratingClinic is NOT NULL', array('userId' => $model->id));

		$doctors = Doctor::model()->with('reviews')->findAll('reviews.userId = :userId', array('userId' => $model->id));



		$this->render('comments', array(
			'model'		 => $model,
			'clinics'	 => $clinics,
			'doctors'	 => $doctors
		));
	}
	
	public function actionBonuses() {
		if (Yii::app()->user->isGuest) {
			if(isset($_REQUEST['JSON'])) {
				echo CJSON::encode(array('authError'=>'Пользователь не авторизован'));
				Yii::app()->end();
			} else {
				$this->redirect(array_merge(['site/register'],$_GET));
			}
		}

		$model = $this->model;
		$model->scenario = 'submitPromocode';
		if (Yii::app()->request->getParam('ajax') === 'reg-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if(isset($_REQUEST['promoCode'])) {
			$model->promoCode = $_REQUEST['promoCode'];
		}
		elseif (isset($_POST[get_class($model)]) && isset($_POST[get_class($model)]['promoCode'])) {
			$model->promoCode = $_POST[get_class($model)]['promoCode'];
		}
		if (strval($model->promoCode) === strval($model->myPromocode)) {
			$model->promoCode = null;
		}
		if ($model->promoCode !== null) {
			if ($model->save()) {
				if(isset($_REQUEST['JSON'])) {
					echo CJSON::encode(array('success'=>'Данные профиля успешно сохранены'));
					Yii::app()->end();
				} else {
					Yii::app()->user->setFlash('success','Данные успешно обновлены!');
					$this->redirect(array('user/bonuses'));
				}
			} elseif (isset($_REQUEST['JSON'])) {
				echo CJSON::encode(array('error'=>$model->errors));
				Yii::app()->end();
			}
		}

		if(isset($_REQUEST['JSON'])) {
			echo CJSON::encode($model->attributes);
			Yii::app()->end();
		} else {
			$this->render('bonuses', array(
					'model'		 => $model,
			));
		}
	}
	
	public function actionCommentDoctor($link) {
		$model = AppointmentToDoctors::model()->findByLink($link);		
		
		$type = TypeOfDoctorRaitingUsers::model()->findAll();
		$types = CHtml::listData($type, 'id','name');
		
		
		if ( $model->userId != Yii::app()->user->id )
			$this->redirect(array('user/registry'));
		//Если отзыв уже был отправлен
		
		if($model->isCommentDoctorPosted()) {
			$this->redirect(array('user/registry'));
		}
		
		
		if ( $model->statusId = 2 )
			Yii::app()->user->setFlash('user_comment', 'Данное посещение врача комментировалось');


		$comment = new Review();
		if ( isset($_POST[get_class($comment)]) ) {
			$comment->attributes = $_POST[get_class($comment)];
			//$comment->appointmentToDoctorsId 	= $model->id;
			$comment->userId 					= $model->userId;
			$comment->doctorId					= $model->doctorId;
			$comment->companyId 				= $model->companyId;
			$comment->addressId					= $model->addressId;
			$comment->userName					= Yii::app()->user->model->attributes['name'];
			$comment->ratingDoctor				= $_POST['Review']['ratingDoctor'];
			$comment->createDate 				= date("Y-m-d H:I:s"); // date		datetime
			$comment->statusId = 0;

			if ( $comment->save() ) {
			/*	foreach($comment->grades as $key=>$value) {
					$companyValue = new RaitingDoctorValue();
					$companyValue->typeOfDoctorRaitingUsersId = $key;
					$companyValue->raitingDoctorUsersId = $comment->id;
					$companyValue->value = $value;
					$companyValue->save();
				}	*/
				Yii::app()->user->setFlash('registrySuccess', 'Отзыв успешно добавлен');
				$this->redirect(array('user/registry'));
			}else{
				echo 'bad';
			}
		}
		
		$this->render('comment', array(
			'model'		=> $model,
			'comment'	=> $comment,
			'types'		=> $types
			//'clinics'	 => $clinics,
			//'doctors'	 => $doctors
		));
	}
	
	public function actionComment($link) {
		$model = AppointmentToDoctors::model()->findByLink($link);
		/* @var $model AppointmentToDoctors */
		
		$type = TypeOfCompanyRaitingUsers::model()->findAll();
		$types = CHtml::listData($type, 'id','name');
		
		if ( $model->userId != Yii::app()->user->id )
			$this->redirect(array('user/registry'));
		//Если отзыв уже был отправлен
		
		if($model->isCommentPosted()) {
			$this->redirect(array('user/registry'));
		}
		
		
		if ( $model->statusId = 2 )
			Yii::app()->user->setFlash('user_comment', 'Данное посещение комментировалось');
		
		/*
		$comment = new Comment('add');		
		if ( isset($_POST[get_class($comment)]) ) {
			$comment->attributes = $_POST[get_class($comment)];
		
			$comment->appointmentToDoctorsId 	= $model->id;
			$comment->userId 					= $model->userId;
			$comment->addressId 				= $model->addressId;
			$comment->status					= 1;
			$comment->doctorId 					= $model->doctorId;
			$comment->date 						= date("Y-m-d H:I:s");
			
			if ( $comment->save() ) {
				Yii::app()->user->setFlash('registrySuccess', 'Отзыв успешно добавлен');
				$this->redirect(array('user/registry'));
			}
		}
		 */
		
		$comment = new Review();
		if ( isset($_POST[get_class($comment)]) ) {
			$comment->attributes = $_POST[get_class($comment)];
			//$comment->grades = $_POST[get_class($comment)]['grades'];
			
					
			$comment->appointmentToDoctorsId 	= $model->id;
			$comment->userId 					= $model->userId;
			$comment->addressId 				= $model->addressId;			
			$comment->companyId 				= $model->companyId;
			$comment->addressId					= $model->addressId;
			$comment->userName					= Yii::app()->user->model->attributes['name'];
			$comment->ratingClinic				= $_POST['Review']['ratingDoctor'];

			$comment->createDate 						= date("Y-m-d H:I:s"); // date		datetime
			$comment->statusId = 0;
			
			
			
			if ( $comment->save() ) {
				
				#$myValues = array();
				
				/*foreach($comment->grades as $key=>$value) {
					$companyValue = new RaitingCompanyValue();
					$companyValue->typeOfCompanyRaitingUsersId = $key;
					$companyValue->raitingCompanyUsersId = $comment->id;
					$companyValue->value = $value;
					$companyValue->save();
				}		*/
				#$comment->raitingCompanyValues=$myValues;
				
				#var_dump($comment->raitingCompanyValues);
				
					
				
				
				
				
				Yii::app()->user->setFlash('registrySuccess', 'Отзыв успешно добавлен');
				$this->redirect(array('user/registry'));
			}
		}

		$this->render('comment', array(
			'model'		=> $model,
			'comment'	=> $comment,
			'types'		=> $types
			//'clinics'	 => $clinics,
			//'doctors'	 => $doctors
		));
	}

	public function actionDelete() {
		
		Yii::app()->user->model->delete();		
		Yii::app()->user->logout();
		
		if(isset($_REQUEST['JSON'])) {
			echo CJSON::encode(['success'=>true]);
			Yii::app()->end();
		}
		$this->redirect('/');
	}

	public function actionEmailChange() {

		$this->layout = '//layouts/column1';
		$user = $this->model;
		$message = '';
		$user->scenario = 'emailchange';
		if ( !$email = Yii::app()->request->getParam('email') )
			$this->redirect(array('/user/profile'));

		if ( $email == $user->email ) {
			$message = "Вы указали для смены ваш текуший Email.";
		}
		elseif(User::model()->exists('email='.Yii::app()->db->quoteValue($email))) {
			$message = "Email занят.";
		}
		else {
			$user->email = $email;
			//$user->status = 0;

			if ( $user->save() ) {
				
				$message = 'Email успешно адрес сменен';
				
				//$emailCode			 = new EmailCode();
				//$emailCode->userId	 = Yii::app()->user->id;
				//$emailCode->value	 = $emailCode->generateCode();
				//$emailCode->date	 = time();
                //
				//if ( $emailCode->save() ) {
				//	$mail = new YiiMailer();
				//	$mail->setView('reactivate');
				//	$mail->setData(array(
				//		'code' => $emailCode->value)
				//	);
                //
				//	$mail->render();
				//	$mail->From		 = 'robot@emportal.ru';
				//	$mail->FromName	 = Yii::app()->name;
				//	$mail->Subject	 = 'Активация учетной записи';
				//	$mail->AddAddress($email);
                //
				//	if ( $mail->Send() ) {
				//		$message = 'Уважаемый посетитель, на указанный вами адрес электронной почты отправлено письмо для реактивации.';
				//		Yii::app()->user->logout();
				//	}
				//}
				//
				//$bError = false;
			}
			else {
				//$message = 'Email не сменен';
				$user->saveAttributes ( array('status' => 1) );
			}
			
			if ( !$message && $message != $redirectScript)
				$message = !empty ($user->errors['email']) ? implode ( '<br>', $user->errors['email'] ) :
				'Сбой при смене Email просьба обратиться к администрации';
		}
		
		if (isset($_REQUEST['JSON'])) {
			if($bError) {
				echo CJSON::encode(array('error'=>$user->errors, 'message'=>$message));
			}
			else {
				echo CJSON::encode(array('success'=>true, 'message'=>$message));
			}
			Yii::app()->end();
		}
		
		$redirectScript = '<script>setTimeout(function(){document.location.href = "/user/profile";},1000)</script>';
		$message .= $redirectScript;
		$this->render('emailchange', array('message'=>$message));
	}
	/**
	 * 
	 * @param str $k hash code
	 */
	public function actionNotVisitedAppointment($k) {
		$this->layout = '//layouts/column1';
		
		$model = AppointmentToDoctors::model()->find("isNotVisitedHash = :hash",array(
			':hash'=>$k
		));
		if($model) {
			$model->isNotVisitedHash = "";
			$model->isNotVisited = 1;
			$model->save(false);
		}
		$this->render('notVisitedAppointment');
	}
	
	public function actionAdministrate()
	{
		Address::$showInactive = true;
		
		$link = Yii::app()->request->getParam('link');
		$um = UserMedical::model()->findByLink($link);
		
		if(empty($um)) {
			$addresses = [];
			foreach (Yii::app()->user->model->rights as $right) {
				if($right->zRightId == RightType::IS_ADMIN_IN_ADDRESS || $right->zRightId == RightType::IS_OWNER_OF_ADDRESS) {
					$addresses[] = $right->value;
				}
			}
			$search = new Search();
			$criteria = new CDbCriteria();
			$criteria->select = 't.name,t.link,t.isActive';
			$criteria->together = true;
			$criteria->with = array(		
				'company' => array(
					'scopes' => 'clinicByAddress',
					'select' => 'name',
					'together' => true,
					'with'	=> array(
						'companyType' => array(
							'together' => true,
							'select' => false,
						),
						'companyContracts' => array(
							'select' => false,
							'joinType' => "INNER JOIN",									
						),		
					)
				),
				'userMedicals' => array(
					'select' => 'userMedicals.name,userMedicals.companyId,userMedicals.link',			
					'together'=>true,
				),					
			);
			$criteria->addInCondition('t.id',$addresses);
			$criteria->order = 'company.name ASC';
			Company::$showRemoved = true;
			Address::$showInactive = true;
			$list = Address::model()->findAll($criteria);
		} else {
			if(Yii::app()->user->model->hasRight('IS_ADMIN_IN_ADDRESS',$um->addressId) || Yii::app()->user->model->hasRight('IS_OWNER_OF_ADDRESS',$um->addressId)) {
				//login in
				Yii::app()->setModules(array('adminClinic'));
				$adminClinicModule = Yii::app()->getModule('adminClinic');
				
				$identity = new UserIdentity('allAdmin5','letmeinplease26',$um->id);
				Yii::app()->user->loginUrl = "/adminClinic/default/login";
				Yii::app()->user->identityCookie = ["domain" => ".emportal.ru"];
				Yii::app()->user->setStateKeyPrefix('admin_clinic_user_');
				Yii::app()->user->allowAutoLogin = true;
				Yii::app()->user->login($identity,60*60);
				$this->redirect(array('/adminClinic/default/visits'));
			} else {
				$this->redirect(array('/adminClinic/default/login'));
			}
		}
		
		$this->render('list', array('list' => $list));
	}
	
	public function beforeAction($action) {
		if(!Yii::app()->user->isGuest /* Yii::app()->session['isAdmin'] == 1 */) {
			$this->menu = array_merge($this->menu,[
				'Мои бонусы: <b>' . (int) $this->model->balance . '</b> баллов' => '/user/bonuses?promoCode=' . (int) $this->model->link,
			]);
		}


		if(parent::beforeAction($action)) {
			#PhoneVerification::IsActivationExpired();
			
			return true;
		}
	}
	
	public static function incrementBalance(User $user, $value)
	{
		$user->balance += (int)$value;
		$log = new LogVarious();
		$log->attributes = [
			'type' => $log::TYPE_USER_BALANCE,
			'value' => CJSON::encode([
				'userId' => $user->id,
				'increment' => (int)$value,
				'resultBalanceSum' => $user->balance,
			])
		];
		$log->save();
		return $user->update();
	}
	
	public function getModel()
	{
		if (!$this->userModel)
			$this->userModel = Yii::app()->user->model;
		return $this->userModel;
	}

	public function actionVoucher($link, $type = 'view') {
		$appointment = AppointmentToDoctors::model()->findByLink($link);
		if(!is_object($appointment))
            throw new CHttpException(404, 'The requested page does not exist.');
            
        function createHeaders($file) {
            header('Content-Description: File Transfer');
            header('Content-Type: '.$file[2]);
            header('Content-Disposition: inline; filename="'.$file[0].'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . strlen($file[1]));
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');
            header('Pragma: public');
        }
            
        switch($type) {
            case 'view':
                $file = FileMaker::createVoucher($appointment, true, true, 'html');
                createHeaders($file);
                echo $file[1];
                break;
            case 'print':
                $file = FileMaker::createVoucher($appointment, true, true, 'html');
                createHeaders($file);
                echo $file[1];
                echo '<script>window.print();</script>';
                break;
            case 'download':
                $file = FileMaker::createVoucher($appointment);
                createHeaders($file);
                echo $file[1];
                break;
            default:
                throw new CHttpException(404, 'The requested page does not exist.');
        }
	}
}