<?php

class HandbookController extends Controller {

	public $layout = '//layouts/column1_for_old_pages';

	public $searchbox_type = SearchBox::S_TYPE_HANDBOOK;
	public $seotype = 'disease';

	public function actionIndex() {
		$this->redirect("/");
		Yii::app()->end();
		$this->render('index');
	}

	public function actionDoctorSpecialty($link = null, $linkUrl = null) {
		
		header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
		$this->layout = null;
		$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
		Yii::app()->end();
		
		$oldModel = DoctorSpecialty::model()->findByAttributes(array('link' => $link));
		if ($oldModel)
			$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $oldModel->linkUrl], true, 301);

		$this->searchbox_tab = Search::S_DOCTORSPECIALTY;

		//делаем постоянный редирект на страничку поиска врача соотв. специальности
		$model = DoctorSpecialty::model()->findByLink($linkUrl);
		if ($model->linkUrl) {
			
			$this->redirect([Yii::app()->baseUrl . '/doctor/specialty-' . $model->linkUrl], true, 301);
		}
		
		$searchModel = new Search();

		$city = $searchModel->getCityName();
		/* clinics */
		$criteria = new CDbCriteria();
		$criteria->select = "t.rating,t.id,t.name,t.link";
		$criteria->limit = 5;
		$criteria->order = "t.rating DESC";
		$criteria->group = "t.name";		
		$criteria->index = "id";
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->compare('companyType.isMedical', 1);
		$criteria->compare('addressServices.companyActivitesId', $model->companyActiviteId);
		if ($searchModel->cityId) {
			$criteria->compare('addresses.cityId', $searchModel->cityId);
		}
		$criteria->with = array(
			'addresses' => array(
				'select' => 'id,name,link',
				'together' => true,
				'with' => array(
					'userMedicals' => [
						'together' => true,
						'select' => false,
					],
					'addressServices' => [
						'together' => true,
					],
					'doctors' => [
						'together' => true,
						'with' => [
							'specialtyOfDoctors' => [
								'together' => true,
							]
						]
					],
				),
			),
			'companyType' => array(
				'select' => 'id,name',
				'together' => true
			),
			'companyContracts' => [
				'together' => true,
				'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
			]
		);
		$clinics = Company::model()->clinic()->findAll($criteria);

		$clinicIds = array_keys($clinics);

		/* Get Addresses with Specialty for 5 clinics we got by previous query */
		$criteria = new CDbCriteria();
		$criteria->compare('addressServices.companyActivitesId', $model->companyActiviteId);
		$criteria->addInCondition("company.id", $clinicIds);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->order = "company.rating DESC";
		$criteria->with = array(
			'userMedicals' => [
				'together' => true,
				'select' => false,
			],
			'addressServices' => [
				'together' => true,
			],
			'doctors' => [
				'together' => true,
				'with' => [
					'specialtyOfDoctors' => [
						'together' => true,
					]
				]
			],
			'company' => [
				'with' => [
					/* 'companyType' => array(
					  'select' => 'id,name',
					  'together' => true
					  ), */
					'companyContracts' => [
						'together' => true,
						'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
					]
				]
			]
		);
		$addresses = Address::model()->findAll($criteria);
		$clinics = array();
		foreach ($addresses as $addr) {
			$clinics[$addr->ownerId][] = $addr;
		}

		$criteria = new CDbCriteria();
		$criteria->limit = 5;
		$criteria->group = 't.id';
		$criteria->order = "t.rating DESC";
		#$criteria->compare("t.link","000000076");
		$criteria->compare("addressServices.companyActivitesId", $model->companyActiviteId);
		$criteria->compare("specialties.id", $model->id);		
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->with = [
			'specialties' => [
				'together' => true,
			],
			'currentPlaceOfWork' => [
				'together' => true,
				'with' => [
					'address' => [
						'with' => [
							'userMedicals' => [
								'together' => true,
								'select' => false,
							],
							'addressServices' => [
								'together' => true
							],
						]
					],
					'company' => [
						'together' => true,
						'companyContract' => [
							'together' => true,
							'joinType' => 'INNER JOIN'
						]
					]
				]
			]
		];
		$doctors = Doctor::model()->findAll($criteria);
		
		/* foreach($doctors as $d) {
		  echo $d->name." - ". $d->currentPlaceOfWork->company->name."<br>";
		  } */
		#var_dump(count($clinics));
		$this->render('doctorSpecialty', array(
			'doctors' => $doctors,
			'city' => $city,
			'clinics' => $clinics,
			'model' => $model,
		));
	}

	public function actionDisease($link = null, $linkUrl = null)
	{
		$subdomain = explode('.', $_SERVER['HTTP_HOST'])[0];
		if (!in_array($subdomain, ['local', 'dev1', '', 'emportal']))
		{
			$fullHost = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
			$fullUrl = $fullHost . $_SERVER['REQUEST_URI'];
			$redirectUrl = str_replace($fullHost, 'https://emportal.ru', $fullUrl);
			$this->redirect($redirectUrl, true, '301');
		}
		
		$this->searchbox_type = SearchBox::S_TYPE_PATIENT;
		$this->searchbox_tab = Search::S_DOCTOR;
		$this->searchbox_tab = Search::S_DISEASE;
		$this->seotype = 'hideseo';
		
		if($link !== NULL) {
			$oldModel = DiseaseGroup::model()->findByAttributes(array('link' => $link));
			if ($oldModel) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $oldModel->linkUrl], true, 301);
			} else {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		
		$model = DiseaseGroup::model()->findByAttributes(array('linkUrl' => $linkUrl));
		if(!$model) { throw new CHttpException(404, 'The requested page does not exist.'); }

		//Специалисты, которые лечат эту болезнь	
		$criteria = new CDbCriteria();
		$criteria->compare('t.deleted', 0);
		$criteria->compare('company.removalFlag', 0);
		$criteria->compare('address.cityId', City::model()->selectedCityId);
		$criteria->compare('address.isactive', 1);
		$criteria->compare('address.samozapis', 0);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->compare('diseaseGroup.linkUrl', $linkUrl);
		$criteria->order = 't.rating desc';
		$criteria->group = 't.id';
		$criteria->limit = 2;
		$criteria->with = [
				'placeOfWorks' => [
						'with' => [
								'company' => [
										'together' => true,
								],
								'address' => [
										'with' => [
												'userMedicals' => [
														'together' => true,
												],
										],
										'together' => true,
								],
						],
						'together' => true,
				],
				'specialties' => [
						'with' => [
								'doctorSpecialtyDiseases' => [
										'with' => [
												'diseaseGroup' => [
														'together' => true,
												],
										],
										'together' => true,
								],
						],
						'together' => true,
				],
		];
		$doctors = Doctor::model()->findAll($criteria);
		
		$this->layout = '//layouts/handbook';
		$this->render('diseases', array(
			'city' => City::model()->findByPk(City::model()->selectedCityId)->name,
			'model' => $model,
			'doctors' => $doctors
		));
	}

	public function actionMedicament($link) {
		
		throw new CHttpException(404, 'The requested page does not exist.');
		
		$this->searchbox_tab = Search::S_MEDICAMENT;
		$model = Brand::model()->findByAttributes(array('link' => $link));

		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		$this->render('medicament', array(
			'model' => $model,
		));
	}

	public function actionAppointments($link) {
		$search = new Search();
		$city = $search->getCityName();

		$this->searchbox_tab = Search::S_DOCTORSPECIALTY;
		$criteria = new CDbCriteria();
		$criteria->compare('t.link', $link);
		$criteria->order = "diseaseGroup.name ASC";
		$criteria->with = [
			'doctorSpecialtyDiseases' => [
				'together' => true,
				'with' => [
					'diseaseGroup' => [
						'together' => true
					]
				]
			]
		];
		$model = DoctorSpecialty::model()->find($criteria);

		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		$this->render('appointments', array(
			'model' => $model,
			'city' => $city
		));
	}

	public function actionCompanyActivites($link = null, $linkUrl = null) {
		$oldModel = CompanyActivite::model()->findByAttributes(array('link' => $link));
		if ($oldModel)
			$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $oldModel->linkUrl], true, 301);

		$this->searchbox_tab = Search::S_COMPANYACTIVITY;

		$model = CompanyActivite::model()->findByAttributes(array('linkUrl' => $linkUrl));
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		
		//делаем постоянный редирект на страничку поиска клиники соотв. профиля деятельности
		if ($model->linkUrl) {
			
			$this->redirect([Yii::app()->baseUrl . '/klinika/profile-' . $model->linkUrl], true, 301);
		}

		$search = new Search();
		$city = $search->getCityName();
		$cityPrep = $search->getCityPrepositionalName();

		/* Получаем 5 случайных компаний, с которыми есть контракт */
		$criteria = new CDbCriteria();
		$criteria->select = "t.rating,t.id,t.name,t.link";
		$criteria->limit = 5;
		$criteria->order = "RAND()";
		$criteria->group = "t.name";		
		$criteria->index = "id";
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->compare('companyType.isMedical', 1);
		$criteria->compare('addressServices.companyActivitesId', $model->id);
		if ($search->cityId) {
			$criteria->compare('addresses.cityId', $search->cityId);
		}
		$criteria->with = array(
			'addresses' => array(
				'select' => 'id,name,link',
				'together' => true,
				'with' => array(
					'userMedicals' => [
						'together' => true,
						'select' => false,
					],
					'addressServices' => [
						'together' => true,
					],
					'doctors' => [
						'together' => true,
						'with' => [
							'specialtyOfDoctors' => [
								'together' => true,
							]
						]
					],
				),
			),
			'companyType' => array(
				'select' => 'id,name',
				'together' => true
			),
			'companyContracts' => [
				'together' => true,
				'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
			]
		);
		$clinicIds = array_keys(Company::model()->clinic()->findAll($criteria));


		/* Get Addresses with Specialty for 5 clinics we got by previous query */
		$criteria = new CDbCriteria();
		$criteria->compare('addressServices.companyActivitesId', $model->id);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->order = 'company.rating DESC';
		$criteria->addInCondition("company.id", $clinicIds);
		$criteria->with = array(
			'userMedicals' => [
				'together' => true,
				'select' => false,
			],
			'addressServices' => [
				'together' => true,
			],
			'doctors' => [
				'together' => true,
				'with' => [
					'specialtyOfDoctors' => [
						'together' => true,
					]
				]
			],
			'company' => [
				'with' => [
					/* 'companyType' => array(
					  'select' => 'id,name',
					  'together' => true
					  ), */
					'companyContracts' => [
						'together' => true,
						'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
					]
				]
			]
		);
		$addresses = Address::model()->findAll($criteria);
		$clinics = array();
		foreach ($addresses as $addr) {
			$clinics[$addr->ownerId][] = $addr;
		}


		$criteria = new CDbCriteria();
		$criteria->limit = 5;
		$criteria->order = "t.rating DESC";
		#$criteria->compare("t.link","000000076");
		$criteria->compare("addressServices.companyActivitesId", $model->id);
		$criteria->compare("specialties.companyActiviteId", $model->id);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->with = [
			'specialties' => [
				'together' => true,
			],
			'currentPlaceOfWork' => [
				'together' => true,
				'with' => [
					'address' => [
						'with' => [
							'userMedicals' => [
								'together' => true,
								'select' => false,
							],
							'addressServices' => [
								'together' => true
							],
						]
					],
					'company' => [
						'together' => true,
						'companyContract' => [
							'together' => true,
							'joinType' => 'INNER JOIN'
						]
					]
				]
			]
		];
		$doctors = Doctor::model()->findAll($criteria);

		$doctorSpecialties = $model->doctorSpecialties;
		$doctorSpecialtiesSearchArray = [ 'specialty' => 'specialty-' . $model->doctorSpecialties[0]->shortSpecialtyOfDoctor->linkUrl];

		$this->render('companyActivites', array(
			'city' => $city,
			'cityPrep' => $cityPrep,
			'model' => $model,
			'doctorSpecialties' => $doctorSpecialties,
			'doctors' => $doctors,
			'clinics' => $clinics,
			'doctorSpecialtiesSearchArray' => $doctorSpecialtiesSearchArray
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 * @return Companies
	 */
	public function loadModel($link) {
		$model = Company::model()->findByAttributes(array('link' => $link));
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}
