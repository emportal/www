<?php

class BeautyController extends LeftMenuController
{

	public $searchbox_type	 = SearchBox::S_TYPE_BEAUTY;
	public $searchbox_tab	 = Search::S_BEAUTY;

	public $menu = array();
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('deny',
				'actions'	 => array('comment', 'favorite', 'visit'),
				'users'		 => array('?'),
			),
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionView($link)
	{

		$this->layout = '//layouts/logo_column';
		$this->render('view', array(
			'model' => $this->loadModel($link),
		));
	}
	public function actionAddresses($link)
	{

		$model = $this->loadModel($link);

		//$addresses = $model->company->addresses;

		$this->render('addresses', array(
			'model' => $model,
		));
	}
	public function getLeftMenu()
	{
	
		$menuItems = array(
				array(
						'label' => $this->_model->company->logo ?
						CHtml::image($this->_model->company->logoUrl, $this->_model->company->name, array('class' => 'company-logo')) :
						CHtml::image($this->assetsImageUrl . '/icon_emp_beauty.jpg', '', array('class' => 'company-logo default'))
				),

		);
	
		foreach ($this->menu as $key => $value) {
			$menuItems[] = array(
					'label'			 => $key,
					'url'			 => array($this->createUrl($value),'link'=>$this->_model->link),
					'itemOptions'	 => array(
							'class' => 'btn w100'
					),
			);
		}
	
		return $menuItems;
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 * @return Address
	 */
	public function loadModel($link)
	{
		$this->_model = Address::model()->findByLink($link);
		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}

}