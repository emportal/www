<?php

class ApiController extends Controller {
	
	public $layout = 'empty';
	public $userAPI = null;
	private $requestMethod;
	
	public function filters() {
		return [
			['application.filters.HttpSimpleAuth - questions'],
		];
	}
	
	public function beforeAction($action)
	{
		$this->requestMethod = Yii::app()->request->getRequestType();
		return true;
	}
	
	public function actionDoctors()
	{
		switch($this->requestMethod) {
			case 'GET':
				$options = [
					//'json' => 1,
					'type' => 'feedDoctors',
					'region' => 'spb',
					'fromApi' => 'true',
					//'relatedModels' => 'receptions',
				];
				$_GET = array_replace_recursive($options, $_GET);
				Yii::app()->runController('site/XmlCreate');
				break;
			case 'POST':
				//create doctor
				//echo 'okee';
				break;
		}
	}
	
	public function actionAddresses()
	{
		switch($this->requestMethod) {
			case 'GET':
				$options = [
					//'json' => 1,
					'type' => 'feed',
				];
				$_GET = array_replace_recursive($options, $_GET);
				Yii::app()->runController('site/XmlCreate');
				break;
			case 'POST':
				echo '';
				break;
		}
	}
	
	public function actionCityDistricts()
	{
		switch($this->requestMethod) {
			case 'GET':
				$city = City::model()->findByAttributes(['subdomain' => Yii::app()->request->getParam('region', 'spb')]);
				$json = Yii::app()->request->getParam('json');
				$listName = 'offers';
				$itemName = 'offer';
				$response = [];
				$criteria = new CDbCriteria;
				$criteria->compare('t.cityId', $city->id, false);
				$criteria->order = 'name';
				$cityDistricts = CityDistrict::model()->findAll($criteria);
				foreach ($cityDistricts as $cityDistrict) {
					$response[$itemName][] = [
						'id' => $cityDistrict->id,
						'name' => $cityDistrict->name,
						'linkUrl' => $cityDistrict->linkUrl
					];
				}
				if ($json) {
					header("Content-type: application/json; charset=utf-8");
					$jsonData = CJSON::encode([$listName => $response[$itemName]]);
					echo $jsonData;
				} else {
					header("Content-type: text/xml; charset=utf-8");
					$xml = Array2XML::createXML($listName, $response);
					echo $xml->saveXML() . "\n";
				}
				break;
			case 'POST':
				echo '';
				break;
		}
	}
	public function actionMetroStations()
	{
		switch($this->requestMethod) {
			case 'GET':
				$city = City::model()->findByAttributes(['subdomain' => Yii::app()->request->getParam('region', 'spb')]);
				$json = Yii::app()->request->getParam('json');
				$listName = 'offers';
				$itemName = 'offer';
				$response = [];
				$criteria = new CDbCriteria;
				$criteria->condition = 'cityId=:cityId';
				$criteria->params = array(':cityId' => $city->id);
				$criteria->order = 'name';
				$metroStations = MetroStation::model()->findAll($criteria);
				foreach ($metroStations as $metroStation) {
					$response[$itemName][] = [
						'id' => $metroStation->id,
						'name' => $metroStation->name,
						'linkUrl' => $metroStation->linkUrl
					];
				}
				if ($json) {
					header("Content-type: application/json; charset=utf-8");
					$jsonData = CJSON::encode([$listName => $response[$itemName]]);
					echo $jsonData;
				} else {
					header("Content-type: text/xml; charset=utf-8");
					$xml = Array2XML::createXML($listName, $response);
					echo $xml->saveXML() . "\n";
				}
				break;
			case 'POST':
				echo '';
				break;
		}
	}
	public function actionMe()
	{
		$response = [];
		$listName = 'User';
		if(Yii::app()->user->isGuest) {
			$listName = 'errors';
			$response['errors'] = [
				[
					'code' => '1',
					'message' => 'HTTP/1.0 401 Unauthorized',
				]
			];
		} else {
			switch($this->requestMethod) {
				case 'GET':
					$attributes = Yii::app()->user->model->attributes;
					$response['id'] = $attributes['id'];
					$FIOExplode = MyTools::FIOExplode($attributes['name']);
					$response['surName'] = $FIOExplode['surName'];
					$response['firstName'] = $FIOExplode['firstName'];
					$response['fatherName'] = $FIOExplode['fatherName'];
					$response['email'] = $attributes['email'];
					$response['telefon'] = $attributes['telefon'];
					$response['birthday'] = $attributes['birthday'];
					$response['balance'] = $attributes['balance'];
					$response['myPromocode'] = Yii::app()->user->model->myPromocode;
					$response['sex'] = Sex::apiIdentifier(@$attributes['sexId']);
					break;
				case 'POST':
					$changed = false;
					if(isset($_POST['promoCode'])) {
						Yii::app()->user->model->scenario = 'submitPromocode';
						Yii::app()->user->model->promoCode = $_POST['promoCode'];
						$changed = true;
					}
					if($changed) {
						if(Yii::app()->user->model->save()) {
							$response['success'] = true;
							$response['message'] = 'Данные профиля успешно сохранены';
						} else {
							$response['success'] = false;
							$response['errors'] = Yii::app()->user->model->errors;
						}
					}
					break;
			}
		}
		if (Yii::app()->request->getParam('json')) {
			header("Content-type: application/json; charset=utf-8");
			$jsonData = CJSON::encode([$listName => $response]);
			echo $jsonData;
		} else {
			header("Content-type: text/xml; charset=utf-8");
			$xml = Array2XML::createXML($listName, $response);
			echo $xml->saveXML() . "\n";
		}
	}
	
	public function actionReceptions()
	{
		switch($this->requestMethod) {
			case 'GET':
				//echo 'will get receptions (MAYBE)';
				break;
			case 'POST':
				//$possibleReceptionsStub = [
				//	[
				//		'doctorId' => '000007185',
				//		'addressId' => '000040162',
				//		'datetime' => '2015-09-11 14:30',
				//	],
				//	[
				//		'doctorId' => '000007185',
				//		'addressId' => '000040162',
				//		'datetime' => '2015-09-11 15:30',
				//	],
				//];
				//if(isset($_POST['receptions']))
				$possibleReceptionsStub = $_POST['receptions'];
				foreach($possibleReceptionsStub as $v) {
					$v['datetime'] = date('Y-m-d H:i', strtotime($v['datetime']));
					$possibleReceptions[$v['doctorId']][] = $v;
				}
				
				$allPossibleTimeQuantsInADay = [];
				for($i = 0; $i <= 23; $i++) {
					$allPossibleTimeQuantsInADay[ sprintf("%02d", $i) . ':' . '00' ] = '';
					$allPossibleTimeQuantsInADay[ sprintf("%02d", $i) . ':' . '30' ] = '';
				}
				$addressLinksAssociations = [];
				$possbileAddressLinksAssociations = $this->userAPI->addressLinksAssociations;
				foreach($possibleReceptions as $doctorLink => $value) {
					//надо прописать ошибку, если не найден доктор
					$doctor = Doctor::model()->findByLink($doctorLink);
					$disabledTimeArr = [];
					foreach($value as $timeQuant) {
						$values = explode(' ', $timeQuant['datetime']);
						
						$addressLink = $timeQuant['addressId'];
						if(!isset($addressLinksAssociations[ $addressLink ])) {
							//надо прописать ошибку, если не найден адрес
							$addressLinksAssociations[ $addressLink ] = Address::model()->findByLink($addressLink)->id;
						}
						
						if(!isset($disabledTimeArr[ $timeQuant['addressId'] ]))
							$disabledTimeArr[ $timeQuant['addressId'] ] = [];
						if(!isset($disabledTimeArr[ $timeQuant['addressId'] ][ $values[0] ]))
							$disabledTimeArr[ $timeQuant['addressId'] ][ $values[0] ] = $allPossibleTimeQuantsInADay;
							
						if(isset($disabledTimeArr[ $timeQuant['addressId'] ][ $values[0] ][ $values[1] ]))
							unset($disabledTimeArr[ $timeQuant['addressId'] ][ $values[0] ][ $values[1] ]);
					}
					$response = ['data'];
					foreach($disabledTimeArr as $addressLink => $datetime) {
						//проверка на возможность работы юзера с данным адресом
						if (!isset($possbileAddressLinksAssociations[ $addressLink ])) {
							$response['data'][] = [
								'success' => false,
								'errors' => [[
                                    'code' => '2',
                                    'message' => 'Not enough privileges to edit in this address',
                                ]],
								'addressId' => $addressLink,
							];
							continue;
						}
						$addressId = $addressLinksAssociations[ $addressLink ];
						
						foreach($datetime as $date => $timeArr) {
							$sql = "DELETE FROM appointmentToDoctors WHERE addressId = '" . $addressId . "' AND doctorId = '" . $doctor->id . "' AND statusId = '" . AppointmentToDoctors::DISABLED . "' AND plannedTime LIKE '" . $date . "%'";
							Yii::app()->db->createCommand($sql)->execute();
							//var_dump($sql);
							
							$enabledTimeQuants = array_diff_key($allPossibleTimeQuantsInADay, $timeArr);
							
							foreach($timeArr as $time => $value) {
								$disabledAppointment = new AppointmentToDoctors('disableTime');
								$disabledAppointmentAttributes = [
									'doctorId' => $doctor->id,
									'plannedTime' => $date . ' ' . $time,
									'addressId' => $addressId,
									'statusId' => AppointmentToDoctors::DISABLED,
								];
								$disabledAppointment->attributes = $disabledAppointmentAttributes;
								$disabledAppointment->save();
							}
							
							foreach($enabledTimeQuants as $enabledTimeQuant => $value) {
								$response['data'][] = [
									'success' => true,
									'addressId' => $addressLink,
									'doctorId' => $doctorLink,
									'datetime' => $date . ' ' . $enabledTimeQuant,
								];
							}
						}
					}
					//var_dump($response);
					echo CJSON::encode($response['data']);
				}
				break;
		}
	}
    
	public function actionAppointments()
	{
		switch($this->requestMethod) {
			case 'GET':
				//echo 'will get appointments (MAYBE)';
				break;
			case 'POST':
                $response = [];
                foreach($_POST['appointments'] as $appointment) {
                    $model = new AppointmentToDoctors();
                    $model->scenario = 'fromAppForm';
                    
                    $address = Address::model()->findByLink($appointment['addressId']);
                    //if (!$address->id) {
                    //    $response[] = [
                    //        'success' => false,
                    //        'errors' => [[
                    //            'code' => '4',
                    //            'message' => 'Адрес с данным id не найден',
                    //        ]],
                    //    ];
                    //    continue;
                    //}
                    $plannedTime;
                    if ($appointment['datetime']) {
                        //тут надо бы проверять, чтобы время было валидно (не в прошлом, номерок не занят и т.д.)
                        $plannedTime = date('Y-m-d H:i:s', strtotime($appointment['datetime']));
                    } else {
                        //автоматом ставим ближайшее возможное время приема, если оно не было указано явно
                        $plannedTime = date('Y-m-d H:i:s', $address->closestReceptionTime);
                    }
                    
                    $model->attributes = [
                        'addressId' => $address->id,
                        'companyId' => $address->company->id,
                        'doctorId' => Doctor::model()->findByLink($appointment['doctorId'])->id,
                        'plannedTime' => $plannedTime,
                        'name' => htmlspecialchars(strip_tags($appointment['patient']['name'])),
                        'phone' => htmlspecialchars(strip_tags($appointment['patient']['phone'])),
                        'sourcePageUrl' => htmlspecialchars(strip_tags($appointment['sourcePageUrl'])) . ' userAPIId: ' . $this->userAPI->id,
                        'appType' => ($this->userAPI->appType) ? $this->userAPI->appType : AppointmentToDoctors::APP_TYPE_FROM_API,
                        'statusId' => AppointmentToDoctors::SEND_TO_REGISTER,
                    ];
                    //var_dump([
                    //    'validate' => $model->validate(),
                    //    'errors' => $model->getErrors(),
                    //    'attributes' => $model->attributes,
                    //]);
                    
                    if ($model->save()) {
                        //вероятно, нужно также отсылать уведомления клинике
                        $model->sendMailToClinic('new_record');
                        //$appointment->sendSmsToClinic('new_record');
                        $response[] = [
                            'success' => true,
                            'id' => $model->link,
                            //'attributes' => (Yii::app()->params['devMode']) ? $model->attributes : '',
                        ];
                    } else {
                        $response[] = [
                            'success' => false,
                            'errors' => [[
                                'code' => '3',
                                'message' => $model->getErrors(),
                            ]],
                        ];
                    }
                }
                echo CJSON::encode($response);
				break;
		}
	}
    
    public function actionQuestions()
    {
    	switch($this->requestMethod) {
    		case 'GET':
    			
    			break;
    		case 'POST':
				$response = [
					'success' => false,
					'errors' => [ ],
				];
    			switch(Yii::app()->request->getParam('id')) {
    				case 'send':
    					$patientText = Yii::app()->request->getParam('text');
    					$patientEmail = Yii::app()->request->getParam('email');
    					if(mb_strlen($patientText) <= 10) {
							$response['errors'][] = 'Текст вопроса должен быть более 10 символов!';
    					}
		    			if (mb_strlen($patientText) > 15000) {
							$response['errors'][] = 'Текст вопроса не должен быть более 15000 символов! Пожалуйста, опишите проблему более компактно.';
						}
						if (!filter_var($patientEmail, FILTER_VALIDATE_EMAIL)) {
							$response['errors'][] = 'Неверный e-mail!';
						}
						if(empty($response['errors'])) {
							$mail = new YiiMailer();
							$mail->setView('question');
							$mail->setData(array(
								'patientText' => $patientText,
								'patientEmail' => $patientEmail,
							));
							$mail->render();
							if(Yii::app()->params['devMode'])
								$mail->AddAddress(Yii::app()->params['devEmail']);
							else {
								$mail->AddAddress('adm@emportal.ru');
								$mail->AddAddress('alexander.d@emportal.ru');
							}
							$mail->From = 'robot@emportal.ru';
							$mail->FromName = Yii::app()->name;
							$mail->Subject = "Задать вопрос врачу (emportal.ru)";
							if(!$mail->Send()) {
								$response['errors'][] = $mail->ErrorInfo;
							}
						}
    					break;
    			}
    			if(empty($response['errors'])) {
    				$response['success'] = true;
    				unset($response['errors']);
    			}
                echo CJSON::encode($response);
    			break;
    	}
    }
	
	public function actionTest()
	{
		$accessKey = '1b34-2d7896-45b7-8d-7a8c317896-4'; //тестовый пользователь
		$url = "http://local.emportal.ru/api/v1/appointments";
		
		$ch = curl_init();
		$appointments = [
			[
				//'doctorId' => '000011191', //доктор хаус
				'addressId' => '000037622',
				'datetime' => date('Y-m-d') . ' 20:00',
                'patient' => [
                    'name' => 'Иван Иваныч',
                    'phone' => '+79516509087',
                ],
                'sourcePageUrl' => 'http://yandex.ru',
			],
            [
				//'doctorId' => '000011191', //доктор хаус
				//'addressId' => '000037622',
				'datetime' => date('Y-m-d') . ' 20:00',
                'patient' => [
                    'name' => 'Иван Иваныч',
                    'phone' => '+79516509087',
                ],
                'sourcePageUrl' => 'http://medsovet.ru',
			],
            [
				//'doctorId' => '000011191', //доктор хаус
				'addressId' => '000037622',
				//'datetime' => date('Y-m-d') . ' 20:00',
                'patient' => [
                    'name' => 'Иван Иваныч',
                    'phone' => '+79516509087',
                ],
                'sourcePageUrl' => 'http://medsovet.ru',
			],
            [
			],
            [
				//'doctorId' => '000011191', //доктор хаус
				//'addressId' => '000037622',
				'datetime' => date('Y-m-d') . ' 20:00',
                'patient' => [
                    'name' => 'Иван Иваныч',
                    'phone' => '+79516509087',
                ],
                'sourcePageUrl' => 'http://medsovet.ru',
			]
		];
		$postfields = [
			'appointments' => $appointments
		];
		
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_NOBODY, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
		curl_setopt($ch, CURLOPT_USERPWD, "user:" . $accessKey);
		
		$server_output = curl_exec($ch);
		curl_close($ch);
		
		echo($server_output);
		
		//var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
		//echo curl_error($ch);
	}
}


