<?php

class NewsController extends Controller {

	public $searchbox_type	 = SearchBox::S_TYPE_PATIENT;
	public $searchbox_tab	 = Search::S_PATIENT;
	public $infoBox_type	 = false;
	public $seotype = 'news';

	public $layout = 'news';

	public function actionIndex() {
		$criteria = new CDbCriteria();

		$this->pageTitle = Yii::app()->name.' - Новости';
		$filter = Yii::app()->request->getParam("filter");
		$tags = [];
		switch($filter) {
			case 'world':
				$name = 'В мире';
				$this->pageTitle = 'Медицинские новости со всего мира - Единый Медицинский Портал';
				$tagDescription = 'Новости в мире';
				Yii::app()->clientScript->registerMetaTag('Новости в мире', 'description');
				break;
			case 'emp':
				$name = 'ЕМП';
				$this->pageTitle = 'Новости ЕМП';
				$tagDescription = 'Новости Единого Медицинского Портала';
				Yii::app()->clientScript->registerMetaTag('Новости Единого Медицинского Портала', 'description');
				break;
			case 'clinic':
				$name = 'Клиники';
				$this->pageTitle = 'Медицинские новости клиник - Единый Медицинский Портал';
				$tagDescription = 'Новости клиник';
				Yii::app()->clientScript->registerMetaTag('Новости клиник', 'description');
				break;
			case 'article':
				$name = 'Статьи';
				$this->pageTitle = $name . ' - Единый Медицинский Портал';
				$tagDescription = 'Статейные материалы';
				Yii::app()->clientScript->registerMetaTag('Статейные материалы', 'description');
				break;
			default:
				$this->pageTitle = 'Новости медицины - Единый Медицинский Портал';
				$tagDescription = 'Новости медицины';
				if(!empty($filter)) {
					$tags = explode(' ', $filter);
					if(count($tags)>0) {
						$criteria->with['tags'] = [ 'together' => true, 'select' => false ];
						$criteria->addInCondition('tags.name',$tags);
					}
				}
		}

		$author = Yii::app()->request->getParam("author");
		if($author) {
			$author = User::model()->findByLink($author);
			$this->pageTitle = 'Поиск по автору - Единый Медицинский Портал';
			$tagDescription = 'Поиск по автору';
			$criteria->compare('t.authorId',$author->id);			
		}

		if(is_string($name)) {
			$criteria->with['type'] = [ 'together' => true ];
			$criteria->compare('type.name',$name);
		}
				
		if(isset($_REQUEST['JSON'])) {
			$dataProvider = new CActiveDataProvider(News::model()->published()->recently(),array(
					'criteria'=>$criteria,
					'pagination'=>array(
							'pageSize'=> 10,
					),
			));
			
			$data = $dataProvider->getData();

			$dataArr = array_map(
				create_function(
					'$value', 'return $value->toArray();'), $data);

			$data['items'] = $dataArr;
			$data['itemsCount'] = $dataProvider->getTotalItemCount();
			$data['itemsPagesCount'] = $dataProvider->getPagination()->getPageCount();
			$data['itemsPage'] = $dataProvider->getPagination()->getCurrentPage();

			echo CJSON::encode($data);
			Yii::app()->end();
		} else {
			if(empty($tags) && !$author) {
				$criteria->limit = 5;
				$firstFive = News::model()->published()->recently()->findAll($criteria);
				foreach ($firstFive as $obj) {
					$criteria->addCondition("t.id != '" . $obj->id . "'");
				}
			}
			
			$dataProvider = new CActiveDataProvider(News::model()->published()->recently(),array(
					'criteria'=>$criteria,
					'pagination'=>array(
							'pageSize'=> 6,
					),
			));
		}

		if (!$filter)
			Yii::app()->clientScript->registerLinkTag('canonical', null, 'https://emportal.ru/news');

		$postTags = NewsTags::getAllTags();
		
		$params = array(
			'dataProvider'	=>	$dataProvider,
			'filter'		=>	$filter,
			'firstFive'		=>	$firstFive,
			'tags'			=>	$tags,
			'postTags'		=>	$postTags,
			'author'        =>  $author,
		);
		if (Yii::app()->request->isAjaxRequest) {
			$this->renderPartial('_list', $params);
		} else {
			$this->render('index', $params);
		}
	}

	public function actionView($link) {

		$model = $this->loadModel($link);
		$this->seotype = 'news-page';

		$this->pageTitle = $model->name . ' - ' . Yii::app()->name;
		Yii::app()->clientScript->registerMetaTag($this->pageTitle, 'description');

		$mainImage = $model->getMainImage();
		if($mainImage) {
			Yii::app()->clientScript->registerMetaTag($mainImage, 'og:image', null, array('id'=>'meta_og_image', 'property' => 'og:image'), 'meta_og_image');
			Yii::app()->clientScript->registerMetaTag($mainImage, 'image', null, array('id'=>'meta_image', 'property' => 'image'), 'meta_image');
			Yii::app()->clientScript->registerLinkTag('image_src',null,$mainImage);
		}

		if(isset($_REQUEST['JSON'])) {
			$model->Text = str_replace('&nbsp;',' ',strip_tags($model->Text));
			echo CJSON::encode($model->attributes);
			UpdateCounters::updateNewsCounter($link);
			Yii::app()->end();
		}
		$popularNews = $model->getPopularNews(3, $model->attributes['id']);
		Yii::app()->clientScript->registerLinkTag('canonical', null, 'https://emportal.ru/news/' . $link . '.html');

		$criteria = new CDbCriteria();
		if(count($model->tags) > 0){
			foreach($model->tags as $val){
				$strQuery[] = "'".$val->name ."'";
			}

			$criteria->with = array(
				'type',
				'tags' => [
					'together' => true,
				]
			);
			$criteria->together = true;
			$criteria->condition = "tags.name in (".implode(',',$strQuery).") AND tags.newsId <> :newsId";
			$criteria->params = [
				':newsId' => $model->id
			];
			$criteria->group = 'tags.newsId';
			$criteria->order = 'COUNT(tags.newsId) DESC';
			$criteria->limit = 6;

			$dataProvider = new CActiveDataProvider(News::model()->published()->recently(),array(
					'criteria'=>$criteria,
					'pagination'=>array(
							'pageSize'=> 6,
					),
			));
		}
		if((is_object($dataProvider) && intval($dataProvider->totalItemCount)==0) || count($model->tags)==0) {
			$criteria = new CDbCriteria();
			$criteria->with = array(
				'type'
			);
			$criteria->together = true;
			$criteria->limit = 6;
		}
		$dataProvider = new CActiveDataProvider(News::model()->published()->recently(),array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=> 6,
			),
		));
		$postTags = NewsTags::getAllTags();
		$this->render('view', array(
			'model'=>$model,
			'dataProvider' 	=> $dataProvider,
			'postTags'		=> $postTags,
			'popularNews'	=> $popularNews,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 * @return News
	 */
	public function loadModel($link) {
		$model = News::model()->findByLink($link);
		if ( $model === null )
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	public function actionAuthors() {
		$authors = User::model()->findAll('isAuthor = 1 AND deleted = 0');
		$this->render('authors',['authors' => $authors]);
	}
}