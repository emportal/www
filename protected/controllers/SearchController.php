<?php

class SearchController extends Controller {

	public $layout = '//layouts/column1';
	public $seotype = 'default';
	private $addr_array = array();
	public $CompanyTypesNotOnline = array(
		'c2148457-29d6-11e2-b014-e840f2aca94f', // Выездная медицинская помощь
		'd16b98ad-349a-11e2-b014-e840f2aca94f', // Санаторий
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f', // Диспансер
		'71d418b8-6072-11e2-b019-e840f2aca94f', // Дом престарелых, интернат
		'a950904d-86fc-11e2-856f-e840f2aca94f'  // Тур. фирмы и тур. операторы с оздоровительными турами
	);
	public $arrGMU = array(
		'e909b718-2b35-11e2-b014-e840f2aca94f',
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f',
		'c06a39cb-f532-11e2-856f-e840f2aca94f',
		'93b817de-ee70-11e2-856f-e840f2aca94f',
		'c06a39ca-f532-11e2-856f-e840f2aca94f',
		'22075224-e934-11e2-856f-e840f2aca94f'
	);
	public $GEOinterval = 0.035;

	private function convertToJSON($dataProvider) {

		if (Yii::app()->request->getParam('JSON', false)) {

			$data = $dataProvider->getData();
			$dataArr = array_map(
					create_function(
							'$value', 'return $value->toArray();'), $data);

			foreach ($data as $key => $item) {
//clinic + service		

				if (!empty($this->addr_array[$item->id])) {
					foreach ($this->addr_array[$item->id] as $address) {

//Не гос медицинская клиника
//Есть контракт
//&& !empty($address->company->companyContracts)
//&& $address->company->companyContracts->contractTypeId=='d9c0f169-9bd2-11e2-856f-e840f2aca94f
//Тип контракта с кнопкой						
						if (!in_array($address->company->companyType->id, Address::$arrGMU)) {
							$dataArr[$key]['canVisit'][$address->id] = 1;
						} else {
							$dataArr[$key]['canVisit'][$address->id] = 0;
						}
						$dataArr[$key]['addressStr'][$address->id] = "{$address->street}, {$address->houseNumber}";
						$dataArr[$key]['metro'][$address->id] = !empty($address->nearestMetroStation->name) ? "м. {$address->nearestMetroStation->name}" : "";
						$dataArr[$key]['longitude'][$address->id] = "{$address->longitude}";
						$dataArr[$key]['latitude'][$address->id] = "{$address->latitude}";
						$dataArr[$key]['addressLink'][$address->id] = $address->link;
					}
				}
//doctor
				if (!empty($item->currentPlaceOfWork)) {
					$dataArr[$key]['addressStr'][$item->currentPlaceOfWork->address->id] = "{$item->currentPlaceOfWork->address->street}, {$item->currentPlaceOfWork->address->houseNumber}";
					$dataArr[$key]['metro'][$item->currentPlaceOfWork->address->id] = "м. {$item->currentPlaceOfWork->address->nearestMetroStation->name}";
					$dataArr[$key]['longitude'][$item->currentPlaceOfWork->address->id] = "{$item->currentPlaceOfWork->address->longitude}";
					$dataArr[$key]['latitude'][$item->currentPlaceOfWork->address->id] = "{$item->currentPlaceOfWork->address->latitude}";
					$dataArr[$key]['addressLink'][$item->currentPlaceOfWork->address->id] = "{$item->currentPlaceOfWork->address->link}";
				}
// clinic + service
				if (!empty($item->category)) {
					$dataArr[$key]['categoryName'] = $item->companyType->name;
				}
			}



			$data = array();
			$data['items'] = $dataArr;
			$data['itemsCount'] = $dataProvider->getTotalItemCount();
			$data['itemsPagesCount'] = $dataProvider->getPagination()->getPageCount();
			$data['itemsPage'] = $dataProvider->getPagination()->getCurrentPage();

			return CJSON::encode($data);
		}

		return false;
	}

	public function actionClinic($metro = null, $district = null, $profile = null) {
		
		Yii::app()->clientScript->registerScriptFile(
				'https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU');

		$this->searchbox_type = SearchBox::S_TYPE_PATIENT_CLINIC;
		$this->searchbox_tab = Search::S_CLINIC;
		$this->seotype = "clinic";

		$searchModel = new Search(Search::S_CLINIC);
		$searchModel->attributes = Yii::app()->request->getParam(get_class($searchModel));
		if (Yii::app()->params['samozapis']) {
			$searchModel->initOMSForm();
		}
		$this->searchModel = &$searchModel;

		if (!empty($metro)) {
			$searchModel->metroStationId[] = $metro;
		}
		if (!empty($district)) {
			$searchModel->metroStationId[] = $district;
		}
		if (!empty($profile)) {
			$searchModel->companyActiviteId = $profile;
		}

		if (
				($metro == null && $district == null && $profile == null) &&
				( (is_array($searchModel->metroStationId) && $searchModel->metroStationId && count($searchModel->metroStationId) == 1) || $searchModel->companyActiviteId )
		) {
			$needRedirect = true;
		}
		$cardsOnly = Yii::app()->request->getParam('cardsOnly');
		$topDocs = Yii::app()->request->getParam('topDocs');
		$page = (!empty(Yii::app()->request->getParam('page'))) ? (int) Yii::app()->request->getParam('page') - 1 : 0;

		if (is_string($searchModel->companyActiviteId) AND !empty($searchModel->companyActiviteId)) {
			$spModel = CompanyActivite::model()->findByAttributes(array('linkUrl'=>str_replace('profile-', '', $searchModel->companyActiviteId)));
			if(!$spModel) {
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
				$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
				Yii::app()->end();
			}
		}
		
		if ($needRedirect) {
			$redirectParams = [];

			if (!empty($searchModel->metroStationId) && count($searchModel->metroStationId) == 1) {
				if (strpos($searchModel->metroStationId[0], 'metro') !== FALSE) {
					$redirectParams['metro'] = $searchModel->metroStationId[0];
				} elseif (strpos($searchModel->metroStationId[0], 'raion') !== FALSE) {
					$redirectParams['district'] = $searchModel->metroStationId[0];
				} else {
					$metroLinkUrl = MetroStation::model()->findByPk($searchModel->metroStationId[0])->linkUrl;
					if ($metroLinkUrl) {
						$redirectParams['metro'] = 'metro-' . $metroLinkUrl;
					}
					$cdLinkUrl = CityDistrict::model()->findByPk($searchModel->metroStationId[0])->linkUrl;
					if ($cdLinkUrl) {
						$redirectParams['district'] = 'raion-' . $cdLinkUrl;
					}
				}
				$searchModel->metroStationId = null;
			}
			if (!empty($searchModel->companyActiviteId)) {
				if (strpos($searchModel->companyActiviteId, 'profile') !== FALSE) {
					$redirectParams['profile'] = $searchModel->companyActiviteId;
				} else {
					$ca = $spModel->linkUrl;
					if ($ca) {
						$redirectParams['profile'] = 'profile-' . $ca;
					}
				}
				$searchModel->companyActiviteId = null;
			}
			if (!empty($redirectParams)) {
				if (isset($_REQUEST['JSON'])) {
					$redirectParams['JSON'] = 1;
				}
				$redirectParams = array_merge(['search/clinic'], $redirectParams);

				foreach ($searchModel->attributes as $name => $value) {
					if (!empty($value)) {
						$redirectParams['Search[' . $name . ']'] = $value;
					}
				}
				
				if (count($redirectParams) == 2 AND isset($redirectParams['metro']))
					$redirectParams = ['klinika' . '/' . $redirectParams['metro']];
				if (count($redirectParams) == 2 AND isset($redirectParams['district']))
					$redirectParams = ['klinika' . '/' . $redirectParams['district']];
				
				
				$this->redirect($redirectParams);
			}
		}

		$CompanyTypesList = $searchModel->companyTypes;

		If (!$searchParam = Yii::app()->request->getParam('Search')) {
			$searchParam = Yii::app()->user->getState('LastSearchParam');
		}

		$searchModel->metroStationId = str_replace([
			'metro-',
			'raion-'
				], '', $searchModel->metroStationId);
		$searchModel->companyActiviteId = str_replace([
			'profile-',
				], '', $searchModel->companyActiviteId);

		#$searchModel->attributes = $searchParam;

		$companyIdsBySearchText = [];
		$emptyText = 'К сожалению, поиск не дал результатов. Измените параметры запроса и попробуйте снова.';
        $notificationText;
		/* if (!empty($_REQUEST['Search']['expandSearchArea']) || empty($searchModel->metroStationId)) {
		  $emptyText = 'К сожалению, поиск не дал результатов. Измените параметры запроса и попробуйте снова.';
		  } else {
		  $emptyText = 'К сожалению, поиск не дал результатов. Вы можете <a href="#expand" class="expandArea">расширить зону поиска</a> , либо изменить параметры запроса и попробовать снова.';
		  } */

		if ($searchModel->validate()) {

			/* if (!empty($searchModel->companyTypeId)) {
			  if (in_array($searchModel->companyTypeId, Address::$arrGMU)) {
			  $searchModel->isGMU = 1;
			  } else {
			  $searchModel->isGMU = 0;
			  }
			  } */
			if($searchModel->metroStationId) {
				$metroDistricts = [];
				if(is_array($searchModel->metroStationId)) {
					$metroDistricts = $searchModel->metroStationId;
				} else {
					$metroDistricts[] = $searchModel->metroStationId;
				}
			}
			if($searchModel->companyActiviteId) {
				$spModel = CompanyActivite::model()->findByLink($searchModel->companyActiviteId);
				if(!$spModel) {
					header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
					$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
					Yii::app()->end();
				}
			}

			$model = new Company('search');

			$model->unsetAttributes();  // clear any default values			
			$SearchText = mb_strtolower(MyTools::cleanSearchText($searchModel->text), 'utf-8');
			/* $SearchText = trim($searchModel->text);			
			  $SearchText = preg_replace("/[^\sA-Za-zА-Яа-я]/iu", "", $SearchText);
			  $SearchText = preg_replace("/[\s\s]+/iu", " ", $SearchText); */

			function getSearchText($SearchText) {
				$companyIdsBySearchText = array();
				if (( $_SERVER['HTTP_HOST'] === 'dev1.emportal.ru' ) && Yii::App()->search->GetSphinxKeyword($SearchText)) {

					/* company name */
					$searchCriteria = new stdClass();
					$searchCriteria->query = '@name (' . Yii::App()->search->GetSphinxKeyword($SearchText) . ')';
					$searchCriteria->from = "companySearch";
					$resArray = Yii::app()->search->searchRaw($searchCriteria); // array result					

					foreach ($resArray['matches'] as $row) {
						$companyIdsBySearchText[] = $row['attrs']['id'];
					}
					$companyIdsBySearchText = array_unique($companyIdsBySearchText);
					#$resIterator = Yii::App()->search->search($searchCriteria); // interator result
					#var_dump($resArray);
				} else {
					$regex = Search::regexpForQuery($SearchText);

					if (!empty($regex)) {
						$regexTranslite = Search::regexpForQuery(MyTools::translitAuto($SearchText));
						$regexCriteria = new CDbCriteria;
						$regexCriteria->select = "t.id,t.name";
						$regexCriteria->params = array(':search' => $regex, ':searchT' => $regexTranslite);
						$regexCriteria->index = 'id';
						#$regexCriteria->scopes = 'clinic';						

						$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :search",), 'OR');
						$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :searchT",), 'OR');

						$companiesBySearchText = Company::model()->findAll($regexCriteria);
						$companyIdsBySearchText = array_keys($companiesBySearchText);
					}
					if(count($companyIdsBySearchText) < 1) {
						$regex = Search::regexpForQuery($SearchText, 2);
						
						if (!empty($regex)) {
							$regexTranslite = Search::regexpForQuery(MyTools::translitAuto($SearchText));
							$regexCriteria = new CDbCriteria;
							$regexCriteria->select = "t.id,t.name";
							$regexCriteria->params = array(':search' => $regex, ':searchT' => $regexTranslite);
							$regexCriteria->index = 'id';
						
							$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :search"), 'OR');
							$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :searchT",), 'OR');
						
							$companiesBySearchText = Company::model()->findAll($regexCriteria);
							$companyIdsBySearchText = array_keys($companiesBySearchText);
						}
					}
				}
				return $companyIdsBySearchText;
			}

			if ($SearchText) {

				$companyIdsBySearchText = getSearchText($SearchText);
				if (empty($companyIdsBySearchText)) {
					$text = MyTools::switcher($SearchText, 2);

					$companyIdsBySearchText = getSearchText($text);
				}
				/* Если ничего не нашлось - пробуем на другой раскладке найти */
			}
			#var_dump($SearchText,$companyIdsBySearchText);






			$criteria = new CDbCriteria;

			$criteria->group = '`t`.`id`';

			if (isset($_REQUEST['JSON'])) {
				
			} else {
				$criteria->select = "t.rating,t.id,t.link,t.name,t.linkUrl,t.companyTypeId";
			}
			
			$criteria->with = array(
				'addresses' => array(
					'select' => 'addresses.id,name,link,linkUrl,latitude,longitude',
					#'scopes' => 'active',
					'together' => true,
					'with' => array(
						'userMedicals' => [
							'together' => true,
							'select' => false,
						]
					/* 'cityDistrict' => [
					  'select' => false,
					  'together' => true
					  ],
					  'metroStations' => array('together' => true), */
					/* 'addressServices' => [
					  'together' => true,
					  'select' => 'addressServices.id',
					  'with' => [
					  'companyActivite' => [
					  'select' => 'id,name',
					  'together' => true,
					  ]
					  ]
					  ] */
					),
				/* 'with' => array(
				  'company' => array(
				  'together' => true,
				  'with' =>array(
				  'companyType'=>array(
				  'together'=>true,
				  'alias'=>'cType'
				  )
				  )
				  )
				  //'cityDistrict'	 => array('together' => true),
				  //'addressActivites'	 => array('together' => true),
				  //'addressServices'	 => array('together' => true),
				  //'metroStations'	 => array('together' => true)
				  ), */
				),
				//'comments',
				'companyType' => array(
					'select' => 'id,name',
					'together' => true
				),
				'companyContracts' => [
					'together' => true,
					'joinType' => 'INNER JOIN',
					'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
				],
				'appointmentCount' => [
					'together' => true
				]
			);
			$criteria->order = '`t`.`rating` DESC, `t`.`name`';
			if($searchModel->offline) {
				Address::$showInactive = true;
				unset($criteria->with['addresses']['with']['userMedicals']);
				unset($criteria->with['companyContracts']);
			} else {
				$criteria->order = '`Contract` DESC, ' . $criteria->order;
				$criteria->compare('userMedicals.agreementNew',1);
			}
			
			if ($SearchText) {
				$criteria->addInCondition('t.id', $companyIdsBySearchText);
			}

			//var_dump($criteria->with);
			//$criteria->compare('categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87');
			//$criteria->compare('addresses.id','0021393d-8eca-11e2-856f-e840f2aca94f');
			#echo 'name REGEXP \''.Search::regexpForQuery($SearchText).'\''; die();
			if ($SearchText) {

				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'cityDistrict' => [
						'select' => false,
						'together' => true
					],
					'metroStations' => array(
						'select' => 'id,name',
						'together' => true
					),
				));

				/* $ids = array_keys(CompanyActivite::model()->findAll(array(
				  'index' => 'id',
				  'condition' => 'LOWER(name) REGEXP :search',
				  'params' => [
				  ':search' => Search::regexpForQuery($SearchText)
				  ]
				  )));
				 */
				if ($caIds) {
					$criteria->addInCondition('companyActivite.id', $caIds);
				}
				/*
				 * поиск по тексту метро и района
				  if (Search::regexpForQuery($SearchText)) {
				  $ids_distr = array_keys(CityDistrict::model()->findAll(array(
				  'index' => 'id',
				  'condition' => 'LOWER(name) REGEXP :search AND cityId = :cId',
				  'params' => [
				  ':search' => Search::regexpForQuery($SearchText),
				  ':cId' => Yii::app()->user->cityId
				  ]
				  )));


				  if ($ids_distr) {
				  $criteria->addInCondition('cityDistrict.id', $ids_distr, 'OR');
				  }

				  $ids_metro = array_keys(MetroStation::model()->findAll(array(
				  'index' => 'id',
				  'condition' => 'LOWER(name) REGEXP :search AND cityId = :cId',
				  'params' => [
				  ':search' => Search::regexpForQuery($SearchText),
				  ':cId' => Yii::app()->user->cityId
				  ]
				  )));

				  if ($ids_metro) {
				  $criteria->addInCondition('metroStations.id', $ids_metro, 'OR');
				  }
				  }
				 */

				/* $addrIds = array_keys(Address::model()->findAll(array(
				  'index' => 'id',
				  'condition' => 'LOWER(name) REGEXP :search',
				  'params' => [
				  ':search' => Search::regexpForQuery($SearchText)
				  ]
				  )));

				  if($addrIds) {
				  $criteria->addInCondition('addresses.id',$addrIds,'OR');
				  } */


				/*
				  $cTypeText = Search::regexpForQuery(preg_replace('|(клиника)|iu','',$SearchText));

				  $ids_cType = array_keys(CompanyType::model()->findAll(array(
				  'index' => 'id',
				  'condition' => 'LOWER(name) REGEXP :search',
				  'params' => [
				  ':search' => $cTypeText
				  ]
				  )));
				  var_dump($ids_cType);
				  if($ids_cType) {
				  $criteria->addInCondition('companyType.id',$ids_cType);
				  } */
				/* $ids = array_keys(CompanyActivite::model()->findAll(array(
				  'index' => 'id',
				  'condition' => 'LOWER(name) REGEXP :search',
				  'params' => [
				  ':search' => Search::regexpForQuery($SearchText)
				  ]
				  )));
				  if($ids) {
				  $criteria->addInCondition('companyActivite.id',$ids);
				  } */
			}

			$criteria->compare('companyType.isMedical', 1);


			$criteria->compare('addresses.cityId', $searchModel->cityId);


			if ($searchModel->price) {				
				$range = explode('-',$searchModel->price);
				
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], [
					'addressServices' => [
						'together' => true,
						'select' => 'addressServices.id',
						'with' => [
							'service' => [
								'select' => 'id,link,name',
								'together' => true,
							]
						]
					]
				]);				
				$criteria->compare('service.link', 'first');				
				$criteria->addBetweenCondition('addressServices.price', $range[0], $range[1]);
			}

			if ($searchModel->doctorSpecialtyId) {
				$shortIds = array_keys(ShortSpecialtyOfDoctor::model()->findAll(['select' => 'id', 'index' => 'id']));

				if (!in_array($searchModel->doctorSpecialtyId, $shortIds)) {

					$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
						'doctors' => [
							'together' => true,
							'with' => [
								'specialtyOfDoctors' => [
									'together' => true,
								]
							]
						],
					));
					$criteria->compare('`specialtyOfDoctors`.doctorSpecialtyId', $searchModel->doctorSpecialtyId);
				} else {
					$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
						'doctors' => [
							'together' => true,
							'with' => [
								'specialtyOfDoctors' => [
									'together' => true,
									'with' => [
										'doctorSpecialty' => [
											'select' => 'shortSpecialtyId'
										]
									]
								]
							]
						],
					));

					$criteria->compare('`doctorSpecialty`.shortSpecialtyId', $searchModel->doctorSpecialtyId);
				}
				#$criteria->compare('specialtyOfDoctors.doctorSpecialtyId', $searchModel->doctorSpecialtyId);
			}
			if ($searchModel->cityDistrictId) {
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'cityDistrict' => array('together' => true),
				));
				$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
			}
			if(!empty($searchModel->clinicLinkUrl)) $criteria->compare("`t`.`linkUrl`", $searchModel->clinicLinkUrl);

			if ($searchModel->longitude && $searchModel->latitude) {
				$criteria->addBetweenCondition('addresses.longitude', $searchModel->longitude - $this->GEOinterval, $searchModel->longitude + $this->GEOinterval);
				$criteria->addBetweenCondition('addresses.latitude', $searchModel->latitude - $this->GEOinterval, $searchModel->latitude + $this->GEOinterval);
			}
			if ($searchModel->companyTypeId)
				$criteria->compare('companyType.id', $searchModel->companyTypeId);


			if ($searchModel->companyActiviteId) {
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'addressServices' => array('together' => true),
				));
				$criteria->addCondition("addressServices.companyActivitesId = '" . $searchModel->companyActiviteId . "'"
						. " OR addressServices.companyActivitesId = '" . CompanyActivite::model()->findByLink($searchModel->companyActiviteId)->id . "'");
			}
			//$criteria->compare('service.companyActiviteId', $searchModel->companyActiviteId);

			if ($searchModel->IsOMS) {
				$criteria->compare('t.IsOMS', 1);
			}
			if ($searchModel->IsDMS) {
				$criteria->compare('t.IsDMS', 1);
			}
			if ($searchModel->loyaltyProgram) {
				$selectedCity = City::model()->findByPk($searchModel->cityId);
				if(is_object($selectedCity) && Yii::app()->params['regions'][$selectedCity->subdomain]['loyaltyProgram']) {
					$criteria->compare('userMedicals.agreementLoyaltyProgram', 1);
				}
			}
			/*
			  if ($searchModel->isGMU) {
			  $criteria->addInCondition('companyType.id', Address::$arrGMU);
			  } else {
			  $criteria->addNotInCondition('companyType.id', Address::$arrGMU);
			  } */
			if ($searchModel->onHouse) {
				$criteria->compare('doctorsOnHouse', 1);
			}

			if ($searchModel->IsHospital) {
				$criteria->with = array_merge($criteria->with, array(
					'attribute' => array(
						'together' => true,
					),
				));
				$vAttribute = ValueAttribute::model()->findByAttributes([
					'name' => 'Наличие стационара'
				]);
				$criteria->compare('attribute.valueAttributeId', $vAttribute->id);
				//var_dump($criteria->with);
			}

			$criteria->compare("t.removalFlag",0);
			$criteria->compare("addresses.samozapis",(Yii::app()->params["samozapis"]) ? 1 : 0);
			
			if (Yii::app()->request->getParam('limit')) {
				$paginationSettings = false;
				$criteria->limit = min([(int)Yii::app()->request->getParam('limit'), 100]);
			} else {
				$paginationSettings = [
					'pageVar' => isset($_REQUEST['JSON']) ? 'Company_page' : 'page',
				];
			}
			
			/* !!!! THIS CRITERIA MUST BE LAST */
			if ($searchModel->metroStationId) {
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'metroStations' => array('together' => true),
				));
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'cityDistrict' => array('together' => true),
				));

				$dumpCriteria = clone $criteria; // criteria without metro

				if ($searchModel->expandSearchArea == 1) {
					$metro = MetroStation::model()->findByPk($searchModel->metroStationId);
					$criteria->addBetweenCondition('metroStations.longitude', $metro->longitude - $this->GEOinterval, $metro->longitude + $this->GEOinterval);
					$criteria->addBetweenCondition('metroStations.latitude', $metro->latitude - $this->GEOinterval, $metro->latitude + $this->GEOinterval);
				} else {
					if (is_array($searchModel->metroStationId)) {
						$tempMetroStation = [];
						foreach ($searchModel->metroStationId as $metroStationId) { $tempMetroStation[] = Yii::app()->db->quoteValue($metroStationId); }
						$queryStringMetroStation = implode(",", $tempMetroStation);
						$criteria->addCondition("metroStations.id in (" . $queryStringMetroStation . ")"
								. " OR metroStations.linkUrl in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.id in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.linkUrl in (" . $queryStringMetroStation . ")");
					} else {
						$criteria->addCondition("metroStations.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR metroStations.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
					}
				}
			}
			
			if (!$topDocs && Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) {
				$loader = new EmiasLoader();
				$attr = [
						'omsNumber' => $searchModel->OMSForm_omsNumber,
						'omsSeries' => $searchModel->OMSForm_omsSeries,
						'referralId' => $searchModel->OMSForm_referralId,
						'birthDate' => date('Y-m-d\TH:i:s',strtotime($searchModel->OMSForm_birthday)),
				];
				$emiasItems = [];

				if($attr['omsNumber'] !== "" && $attr['birthDate'] !== "" && !$searchModel->OMSFormData_check()) {
					$emptyText_head = "При запросе данных произошла ошибка: <span style='color:red;'>".join("<br>", $searchModel->OMSForm_exceptions)."</span>";
					$emptyText_step0 = "Вы можете:";
					$emptyText_step1 = "1. <a href='javascript:void(0)' onclick='showOMSpopupView(); return false;' class=\"normal-link\">проверить</a> корректность используемых ОМС данных";
					$emptyText_step2 = "2. повторить запрос позже, если вы уверены в корректности ОМС данных";
					$notificationText = $emptyText_head . "<br>" . $emptyText_step0 . "<br>" . $emptyText_step1 . "<br>" . $emptyText_step2;
				}
				elseif($attr['omsNumber'] === "" || $attr['birthDate'] === "") {
					$emptyText_head = "Для просмотра всех доступных вам клиник необходимо:";
					$emptyText_step1 = " - <a href='javascript:void(0)' onclick='showOMSpopupView(); return false;' class=\"normal-link\">заполнить</a> ваши данные ОМС";
					if(trim(strval($attr['omsNumber'])) !== "" AND trim(strval($attr['birthDate'])) !== "")
						$emptyText_step1 = "<strike>" . $emptyText_step1 . "</strike>";
					$notificationText = $emptyText_head . "<br>" . $emptyText_step1;
                    
                    $emiasItems = ['none'];
                    $criteria->with = array_replace_recursive($criteria->with, [
                        'extCompany' => [
                            'select' => '(extCompany.companyId) AS extCompany',
                            //'together' => 'true',
                            'joinType' => 'INNER JOIN',
                        ]
                    ]);
				}
				elseif(empty($emiasItems)) {
					$emiasInfo = $loader->getLpusInfo($attr);
					$emiasResponse = $loader->populateLPUList($emiasInfo->return, false, false);
					$emiasItems = array_keys($emiasResponse);
                    $criteria->addInCondition('t.id', $emiasItems);
				}
			}
			
			/*
			  if (trim($searchModel->text)) {
			  $regex = Search::regexpForQuery($searchModel->text);
			  $regexCriteria = new CDbCriteria;
			  $regexCriteria->params = array(':search' => $regex);
			  $regexCriteria->addCondition(array(
			  "LOWER(`t`.`name`) REGEXP :search",
			  //"LOWER(`t`.`nameENG`) REGEXP :search",
			  //"LOWER(`t`.`nameRUS`) REGEXP :search",
			  ), 'OR');
			  //$model->currentPlaceOfWork->CompaniesName

			  $criteria->mergeWith($regexCriteria);
			  } */
			
			$dataProvider = new CActiveDataProvider($model->clinic(), array(
				'criteria' => $criteria,
				'pagination' => $paginationSettings,
				'sort' => array(),
			));
			$data = $dataProvider->getData();

			if(Yii::app()->request->getParam('onlyIds')) {
				$criteria->group = 't.id';
				$criteria->order = '';
				$data = $model->findAll($criteria);
				$ids = [];
				foreach ($data as $row) {
					$ids[] = $row->id;
				}
				echo CJSON::encode($ids);
				exit;
			}
			
			/*
			 * Показать на карте
			 */
			if(Yii::app()->request->getParam('showOnMap')) {
				$criteria->group = '';
				$criteria->order = "";
				$mapData = $model->findAll($criteria);
				foreach ($mapData as $key => $value) {
					foreach ($value->addresses as $i=>$adr) {
						$json[] = array(
								'id' => $adr->id."_".$i,
								'name' => str_replace("'", "&#39;", $value->name),
								'address' => str_replace("'", "&#39;", $adr->name),
								'lat' => $adr->latitude,
								'long' => $adr->longitude,
								'url' => $value->linkUrl . '/' . $adr->linkUrl,
								//'phone' => $phone_arr[$adr->id]
						);
					}
				}
				$coordObj = array(
						'lat' => '59.938',
						'long' => '30.313',
				);
				$cityModel = City::model()->findByAttributes([ 'id'=>City::model()->selectedCityId ]);
				if($cityModel) {
					if(!empty($cityModel->longitude) || !empty($cityModel->latitude)) {
						$coordObj['lat'] = $cityModel->latitude;
						$coordObj['long'] = $cityModel->longitude;
					}
				}
				$coord = CJSON::encode($coordObj);
				$json = CJSON::encode($json);
				$this->renderPartial('map', array(
						'data' => $json,
						'search' => 1,
						'coord' => $coord
				));
				exit;
			}

			if (empty($data) && isset($_REQUEST['JSON']) && !empty($searchModel->metroStationId) && $searchModel->expandSearchArea == 0) {
				$repeatDumpCriteria = true;
				$dumpCriteria->addBetweenCondition('metroStations.longitude', $metro->longitude - $this->GEOinterval, $metro->longitude + $this->GEOinterval);
				$dumpCriteria->addBetweenCondition('metroStations.latitude', $metro->latitude - $this->GEOinterval, $metro->latitude + $this->GEOinterval);

				$dataProvider = new CActiveDataProvider($model->clinic(), array(
					'criteria' => $dumpCriteria,
					'pagination' => $paginationSettings,
					'sort' => array(),
				));
			}
			
			$clinics = array();

			foreach ($dataProvider->data as &$row) {
				/* @var $row Clinic */
				$clinics[$row->id] = $row;

				if (!in_array($row->companyType->id, Address::$arrGMU) && !in_array($row->companyType->id, $this->CompanyTypesNotOnline)) {
					$companyContracts[$row->id] = 1;
				} else {
					$companyContracts[$row->id] = 0;
				}
				/* foreach($row->addresses as $addr) {						


				  } */
				$row->addresses = array();
			}

			if (count($clinics) && ($searchModel->metroStationId || $searchModel->cityDistrictId || 1)) {

				$criteria = new CDbCriteria();
				/* Model Address */
				$criteria->select = "t.id,t.name,t.ownerId,t.link,t.street,t.houseNumber,t.linkUrl,t.longitude,t.latitude,t.cityId,t.samozapis";
				$criteria->with = array(
					/* 'cityDistrict'	 => array(
					  'together' => true,
					  ), */
					'userMedicals' => [
						'together' => true,
						'select' => false,
					],
					'metroStations' => array(
						#'select'=>'id,name',
						'together' => true,
					),
				);

				if($searchModel->offline) {
					Address::$showInactive = true;
					unset($criteria->with['userMedicals']);
				} else {
					$criteria->scopes = 'active';
					$criteria->compare('userMedicals.agreementNew',1);
				}
				if ($ids_metro) {
					$criteria->addInCondition('metroStations.id', $ids_metro, 'OR');
				}
				if ($ids_distr) {
					$criteria->addInCondition('t.cityDistrictId', $ids_distr, 'OR');
				}

				/* if($addrIds) {					
				  $criteria->addInCondition('t.id',$addrIds,'OR');
				  } */

				if ($searchModel->cityDistrictId) {
					$criteria->with = array_merge($criteria->with, array(
						'cityDistrict' => array('together' => true),
					));
					$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
				}
				//$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
				if (Yii::app()->user->cityId) {
					$criteria->compare('`t`.`cityId`', Yii::app()->user->cityId);
				}

				if ($searchModel->longitude && $searchModel->latitude) {
					$criteria->addBetweenCondition('t.longitude', $searchModel->longitude - $this->GEOinterval, $searchModel->longitude + $this->GEOinterval);
					$criteria->addBetweenCondition('t.latitude', $searchModel->latitude - $this->GEOinterval, $searchModel->latitude + $this->GEOinterval);
				}
				$criteria->addInCondition('ownerId', array_keys($clinics));
				$criteria->order = "t.name DESC";

				/* !!!! THIS CRITERIA MUST BE LAST */
				if ($searchModel->metroStationId) {
					$criteria->with = array_merge($criteria->with, array(
						'cityDistrict' => array('together' => true),
					));

					$dumpCriteria = clone $criteria;
					if ($searchModel->expandSearchArea == 1) {
						$metro = MetroStation::model()->findByPk($searchModel->metroStationId);
						$criteria->addBetweenCondition('metroStations.longitude', $metro->longitude - $this->GEOinterval, $metro->longitude + $this->GEOinterval);
						$criteria->addBetweenCondition('metroStations.latitude', $metro->latitude - $this->GEOinterval, $metro->latitude + $this->GEOinterval);
					} else {
						if (is_array($searchModel->metroStationId)) {
							$tempMetroStation = [];
							foreach ($searchModel->metroStationId as $metroStationId) { $tempMetroStation[] = Yii::app()->db->quoteValue($metroStationId); }
							$queryStringMetroStation = implode(",", $tempMetroStation);
							$criteria->addCondition("metroStations.id in (" . $queryStringMetroStation . ")"
									. " OR metroStations.linkUrl in (" . $queryStringMetroStation . ")"
									. " OR cityDistrict.id in (" . $queryStringMetroStation . ")"
									. " OR cityDistrict.linkUrl in (" . $queryStringMetroStation . ")");
						} else {
							$criteria->addCondition("metroStations.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
									. " OR metroStations.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
									. " OR cityDistrict.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
									. " OR cityDistrict.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
						}
					}
				}


				if ($repeatDumpCriteria) {
					$dumpCriteria->addBetweenCondition('metroStations.longitude', $metro->longitude - $this->GEOinterval, $metro->longitude + $this->GEOinterval);
					$dumpCriteria->addBetweenCondition('metroStations.latitude', $metro->latitude - $this->GEOinterval, $metro->latitude + $this->GEOinterval);
					$addresses = Address::model()->findAll($dumpCriteria);
				} else {
					$addresses = Address::model()->findAll($criteria);
				}
				/* ///   !!!! THIS CRITERIA MUST BE LAST */

				$this->addr_array = array();

				foreach ($addresses as &$address) {

					//$clinics[$address->ownerId]->addresses[]=$address;
					//array_push($clinics[$address->ownerId]->addresses, $address);	
					$this->addr_array[$address->ownerId][] = $address;
				}

				/*
				  echo "<pre>";
				  var_dump($this->addr_array);
				  echo "</pre>";
				 */

				$arr = array();
				foreach ($clinics as $cl) {
					foreach ($cl->addresses as $addr) {
						//var_dump($addr);
						$arr[] = $addr->id;
					}
				}

				/*
				  $phone_criteria= new CDbCriteria();
				  $phone_criteria->addInCondition('addressId', $arr);
				  $phone = Phone::model()->findAll($phone_criteria);
				  foreach($phone as $ph) {
				  $phone_arr[$ph->addressId][]=$ph->name;
				  }
				 */
			}

			if ($json = $this->convertToJSON($dataProvider)) {
				echo $json;
			} else {
				if ($cardsOnly) {
					header("Access-Control-Allow-Origin: *");
					$this->renderPartial('clinicCardsOnly', array(
						'model' => $model,
						'dataProvider' => $dataProvider,
						'addr' => $this->addr_array,
						'emptyText' => $emptyText,
						'CompanyTypesList' => $CompanyTypesList,
						'companyContracts' => $companyContracts,
					));
				}
				else {
					$REQUEST_metro = is_array($_REQUEST["metro"]) ? $_REQUEST["metro"] : ((!empty($_REQUEST["metro"])) ? [$_REQUEST["metro"]] : []);
					$REQUEST_district = is_array($_REQUEST["district"]) ? $_REQUEST["district"] : ((!empty($_REQUEST["district"])) ? [$_REQUEST["district"]] : []);
					$getParamSearchMetroStationId = !empty(Yii::app()->request->getParam('Search')['metroStationId']) 
						? (is_array(Yii::app()->request->getParam('Search')['metroStationId']) 
								? Yii::app()->request->getParam('Search')['metroStationId'] 
								: [Yii::app()->request->getParam('Search')['metroStationId']])
						: [];
					$district_metro = array_merge(array_merge($REQUEST_metro,$REQUEST_district),$getParamSearchMetroStationId);
					foreach ($district_metro as $index=>$value) {
						if(isset($searchModel->metroStations[$value])) { continue; }
						elseif(isset($searchModel->cityDistricts[$value])) { continue; }
						else {
							header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
							$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
							Yii::app()->end();
						}
					}
					$metroList = [];
					$districtlist = [];
					$locative = "";
					$metroDistrictStr = "";
					if(!empty($district_metro)) {
						$searchDistrictString = "";
						foreach ($district_metro as $index=>$value) {
							if(isset($searchModel->metroStations[$value])) {
								if(!empty($locative)) $locative .= ", ";
								else $locative .= "у ";
								$locative .= "метро " . $searchModel->metroStations[$value];
								
								if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
								$metroDistrictStr .= "метро " . $searchModel->metroStations[$value];
								
								$metroList[] = $searchModel->metroStations[$value];
							}
						}
						foreach ($district_metro as $index=>$value) {
							$cityDistrictModel = CityDistrict::model()->find("link=:link OR linkUrl=:link OR id=:link", [':link'=>mb_substr($value, stripos($value,'-')+1)]);
							if($cityDistrictModel) {
								if(!empty($locative)) $locative .= ", ";
								$locative .= $searchModel->cityDistricts[$value] . " район";
								
								if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
								$metroDistrictStr .= $searchModel->cityDistricts[$value] . " район";

								$districtlist[] = $cityDistrictModel->getLocative();
							}
						}
					} else {
						$locative = City::getSelectedCity()->locative;
					}
					$doctorCountableForm = MyTools::createCountableForm($dataProvider->totalItemCount, "клиника", "клиники", "клиник");
					
					$templateRules = [
							"%totalItemCount%" => $dataProvider->totalItemCount,
							"%name%" => $spModel->name,
							"%locative%" => $locative,
							"%metroDistrict%" => $metroDistrictStr,
							"%metroList%" => $metroList,
							"%districtlist%" => $districtlist,
							"%doctorCountableForm%" => $doctorCountableForm,
							"%searchText%" => CHtml::encode($searchModel->text),
							"%companyType%" => CompanyType::model()->findByPk($searchModel->companyTypeId)->name,
							"%appName%" => Yii::app()->name,
							"%inCity%" => (empty($district_metro)) ? ' в ' . $locative : '',
							"\"\"" => " ",
							"  " => " ",
							" ," => ",",
					];
					
					if ($doctorCountableForm == 'клиника') {
						$templateRules['%medEnding%'] = 'ая';
						$templateRules['%beginPhraseEnding%'] = 'а';
					} else {
						$templateRules['%medEnding%'] = 'их';
						$templateRules['%beginPhraseEnding%'] = 'o';
					}
					$templateRules['%companyActivite%'] = ($spModel) ? (($spModel->genitive) ? $spModel->genitive : $spModel->name) : '';
					//$templateRules['%metroListStr%'] = (!empty($templateRules["%metroList%"]) ? 'у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и ' : '') : '');
					//$templateRules['%districtlistStr%'] = (!empty($templateRules["%districtlist%"]) ? (((count($templateRules["%districtlist%"]) > 1) ? 'в районах' : 'в районе') . ' %districtlist% ') : '');
					$templateRules['%beginPhrase%'] = (!empty($templateRules["%searchText%"]) ? 'По названию "'.$templateRules["%searchText%"].'" найден' . $templateRules['%beginPhraseEnding%'] : 'Найден' . $templateRules['%beginPhraseEnding%']);

					$pageTagH1 = trim("Клиники и центры %companyActivite% "
					. (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
					. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist% ' : ' '));

					$pageTagTitle = trim("Клиники и центры %companyActivite% %inCity% "
					. (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
					. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist% ' : ' ')
					. " — запись онлайн, отзывы");
					
					$pageTagDescription = trim($templateRules['%beginPhrase%']
					. " %totalItemCount% "	
					. "медицинск%medEnding% %doctorCountableForm%"
					. "%inCity%"
					. (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
					. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist% ' : ' ')
					. ($templateRules["%companyActivite%"] ? ", %companyActivite%" : ""));
					
					$pageTagH1 = preg_replace('!\s+!', ' ', MyTools::createFromTemplate(strip_tags($pageTagH1), $templateRules));
					$pageTagTitle = preg_replace('!\s+!', ' ', MyTools::createFromTemplate(strip_tags($pageTagTitle), $templateRules));
					$pageTagDescription = preg_replace('!\s+!', ' ', MyTools::createFromTemplate(strip_tags($pageTagDescription), $templateRules));
					
					$this->render('clinic', array(
						'model' => $model,
						'dataProvider' => $dataProvider,
						'addr' => $this->addr_array,
						'emptyText' => $emptyText,
						'CompanyTypesList' => $CompanyTypesList,
						'companyContracts' => $companyContracts,
						'spModel' => $spModel,
						//'titleText' => $titleText,
						'searchModel' => $searchModel,
						'district_metro' => $searchModel->getRequestedLocative()['district_metro'],
						'templateRules' => $templateRules,
						'pageTagTitle' => $pageTagTitle,
						'pageTagH1' => $pageTagH1,
						'pageTagDescription' => $pageTagDescription,
                        'notificationText' => $notificationText,
					));
				}
			}
		} else {
			$this->render('clinic', array(
				'searchModel' => $searchModel,
				'dataProvider' => null,
			));
		}
	}

	public function actionDiagnostics($metro = null, $district = null, $service = null) {
		
		Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', CClientScript::POS_END);
		Yii::app()->clientScript->registerCoreScript('jquery.maskedinput', CClientScript::POS_END);
		
		$this->searchbox_type = SearchBox::S_TYPE_SERVICE;
		$this->searchbox_tab = Search::S_SERVICE;

		if (!$searchParam = Yii::app()->request->getParam('Search')) {
			$searchParam = Yii::app()->user->getState('LastSearchParam');
		}

		$isVkApp = Yii::app()->request->getParam('vk');
		if($isVkApp) {
			$vkGroupId = Yii::app()->session['vkGroupId'];
			$vkViewerType = Yii::app()->session['vkViewerType'];

			$vkAddress = Yii::app()->request->getParam('address');
			if($vkAddress) {
				Yii::app()->session['vkAddress'] = $vkAddress;
			} else {
				$vkAddress = Yii::app()->session['vkAddress'];
			}
			
			$this->layout = '/layouts/main_vk';
			$vkAppModel = VkApp::model()->findAll('groupVk = :groupVk', ['groupVk' => $vkGroupId]);

			$vkClinic = Company::model()->findByPk($vkAppModel[0]->companyId);

			if($vkClinic) {
				$attributes = ['id' => $vkClinic->address->cityId];
				$citySubdomain = City::model()->findByAttributes($attributes)->subdomain;
				Yii::app()->session['selectedRegion'] = $citySubdomain;
			}
			else {
				Yii::app()->session['selectedRegion'] = 'spb';
			}
		}

		$searchModel = new Search(Search::S_SERVICE);
		$this->searchModel = &$searchModel;
		$searchModel->attributes = $searchParam;
		if (!empty($metro)) {
			$searchModel->metroStationId = $metro;
		}
		if (!empty($district)) {
			$searchModel->cityDistrictId = $district;
		}
		if (!empty($service)) {
			$searchModel->companyServiceId = $service;
		}
		
		if (($metro == null && $district == null && $service == null) && ((is_array($searchModel->metroStationId) && $searchModel->metroStationId && count($searchModel->metroStationId) == 1) || ($searchModel->companyServiceId && !is_array($searchModel->companyServiceId)))) {
			$needRedirect = true;
		}
		$cardsOnly = Yii::app()->request->getParam('cardsOnly');
		$topDocs = Yii::app()->request->getParam('topDocs');
		$page = (!empty(Yii::app()->request->getParam('page'))) ? (int) Yii::app()->request->getParam('page') - 1 : 0;
		
		if ($needRedirect) {
			$redirectParams = ['search/diagnostics'];
			if (!empty($searchModel->metroStationId) && count($searchModel->metroStationId) == 1) {
				if (strpos($searchModel->metroStationId[0], 'metro') !== FALSE) {
					$redirectParams['metro'] = $searchModel->metroStationId[0];
				} elseif (strpos($searchModel->metroStationId[0], 'raion') !== FALSE) {
					$redirectParams['district'] = $searchModel->metroStationId[0];
				} else {
					$metroLinkUrl = MetroStation::model()->findByPk($searchModel->metroStationId[0])->linkUrl;
					if ($metroLinkUrl) {
						$redirectParams['metro'] = 'metro-' . $metroLinkUrl;
					}
					$cdLinkUrl = CityDistrict::model()->findByPk($searchModel->metroStationId[0])->linkUrl;
					if ($cdLinkUrl) {
						$redirectParams['district'] = 'raion-' . $cdLinkUrl;
					}
				}
				$searchModel->metroStationId = null;
			}
			if (!empty($searchModel->companyServiceId)) {
				if (strpos($searchModel->companyServiceId, 'service-') !== FALSE) {
					$redirectParams['service'] = $searchModel->companyServiceId;
				} else {
					$dsLinkUrl = Service::model()->findByAttributes(["id"=>$searchModel->companyServiceId])->linkUrl;
					if ($dsLinkUrl) {
						$redirectParams['service'] = 'service-' . $dsLinkUrl;
					}
				}
				$searchModel->companyServiceId = null;
			}
			if (isset($_REQUEST['JSON'])) {
				$redirectParams['JSON'] = 1;
			}
		
			foreach ($searchModel->attributes as $name => $value) {
				if (!empty($value)) {
					$redirectParams['Search[' . $name . ']'] = $value;
				}
			}
			$shortSearch = Yii::app()->request->getParam('shortSearch');
				
			if($shortSearch) {
				$redirectParams['shortSearch'] = 1;
				$redirectParams['isWidget'] = Yii::app()->request->getParam('isWidget');
			}
		
			$this->redirect($redirectParams);
		}
		
		/* clean search values from aliases */
		$searchModel->metroStationId = str_replace([
				'metro-',
		], '', $searchModel->metroStationId);
		$searchModel->cityDistrictId = str_replace([
				'raion-'
		], '', $searchModel->cityDistrictId);
		$searchModel->companyServiceId = str_replace([
				'service-'
		], '', $searchModel->companyServiceId);

		if($searchModel->metroStationId && !is_array($searchModel->metroStationId)) $searchModel->metroStationId = ["metro-".$searchModel->metroStationId];
		if($searchModel->cityDistrictId && !is_array($searchModel->cityDistrictId)) $searchModel->cityDistrictId = ["raion-".$searchModel->cityDistrictId];
		if(!is_array($searchModel->metroStationId)) $searchModel->metroStationId = [];
		if(!is_array($searchModel->cityDistrictId)) $searchModel->cityDistrictId = [];
		$metro = [];
		$district = [];
		foreach (array_merge($searchModel->metroStationId, $searchModel->cityDistrictId)  as $value) {
			if(is_numeric(strpos($value, 'metro-'))) {
				$metro[] = str_replace('metro-', '', $value);
			}
			elseif(is_numeric(strpos($value, 'raion-'))) {
				$district[] = str_replace('raion-', '', $value);
			}
		}

		$emptyText = 'К сожалению, поиск не дал результатов. Измените параметры запроса и попробуйте снова.';

		if ($searchModel->validate()) {
			$criteria = new CDbCriteria;
			$criteria->group = '`t`.`id`';
			$criteria->with = [
				'service' => [
					'together' => true,
				],
				'address' => [
					'together' => true,
					'with' => [
						'company' => [
							'together' => true,
							'with' => [
								'companyContracts' => [
									'joinType' => 'INNER JOIN',
									'select' => false,
									'together' => true
								]
							]
						],
						'userMedicals' => [
							'together' => true,
						],
					],
				],
			];
			if($searchModel->companyServiceId) {
				$criteria->compare('service.linkUrl', $searchModel->companyServiceId);
				
				$spModel = Service::model()->findByLink($searchModel->companyServiceId);
				if(!$spModel) {
					header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
					$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
					Yii::app()->end();
				}
			}
			$criteria->compare('company.removalFlag', 0);
			$criteria->compare('address.cityId', $searchModel->cityId);
			$criteria->compare('address.isActive', 1);
			$criteria->compare('userMedicals.agreementNew', 1);

			//$criteria->compare('t.companyActivitesId', 'e909b71b-2b35-11e2-b014-e840f2aca94f'); // Только сервис диагностики
			$criteria->compare('t.isActual', 1); //не показываем услуги, которые давно не обновлялись
			if(!empty($searchModel->clinicLinkUrl)) $criteria->compare("`company`.`linkUrl`", $searchModel->clinicLinkUrl);

			if ($metro) {
				$criteria->with['address']['with'] = array_merge($criteria->with['address']['with'], array('metroStations' => array('together' => true)));
				$criteria->addInCondition("metroStations.linkUrl", $metro, "AND");
			}
			if ($district) {
				$criteria->with['address']['with'] = array_merge($criteria->with['address']['with'], array('cityDistrict' => array('together' => true)));
				$criteria->addInCondition("cityDistrict.linkUrl", $district, "AND");
			}

			if (is_numeric($searchModel->priceMin) || is_numeric($searchModel->priceMax)) {
				if ($searchModel->priceMin) {
					$criteria->addCondition("t.price >= " . intval($searchModel->priceMin) );
				}
				$criteria->addCondition("t.price IS NOT NULL OR t.free = 1");
				if ($searchModel->priceMax) {
					if ($searchModel->priceMin) {
						$criteria->addCondition("t.price <= " . intval($searchModel->priceMax) );
					} else {
						$criteria->addCondition("t.price <= " . intval($searchModel->priceMax) );
					}
				}
			}
			if ($searchModel->onlySale) {
				$criteria->addCondition("t.oldPrice IS NOT NULL");
			}
			if ($searchModel->loyaltyProgram) {
				$selectedCity = City::model()->findByPk($searchModel->cityId);
				if(is_object($selectedCity) && Yii::app()->params['regions'][$selectedCity->subdomain]['loyaltyProgram']) {
					$criteria->compare('userMedicals.agreementLoyaltyProgram', 1);
				}
			}
			if($searchModel->sort) {
				$explodeSort = explode(" ", $searchModel->sort);
				switch ($explodeSort[0]) {
					case 'price':
						$priceOrder = "t.".trim(Yii::app()->db->quoteValue($searchModel->sort),"'");
						$criteria->order = "(t.price>0 OR t.free=1) desc, $priceOrder";
						break;
					default:
						$sort_parametr = trim(Yii::app()->db->quoteValue($explodeSort[0]),"'");
						$criteria->order = "(t.{$sort_parametr} IS NOT NULL AND t.{$sort_parametr} != '') DESC, t.".trim(Yii::app()->db->quoteValue($searchModel->sort),"'");
						break;
				}
			}
			/*
			 * Показать на карте
			 */
			$model = new AddressServices('search');
			$showOnMap = Yii::app()->request->getParam('showOnMap');
			if($showOnMap) {
				
				
				$dataProvider = new CActiveDataProvider($model, array(
					'criteria' => $criteria,
					'sort' => array(),
					'pagination'=>array(
						'pageSize'=>2000,
						'currentPage'=>1,
					),
				));
				
				foreach ($dataProvider->getData() as $key => $value) {
					$adr = $value->address;
					$json[] = array(
						'id' => $adr->id,
						'name' => str_replace("'", "&#39;", $value->service->name),
						'address' => str_replace("'", "&#39;", $adr->name),
						'lat' => $adr->latitude,
						'long' => $adr->longitude,
						'url' => $adr->company->linkUrl . '/' . $adr->linkUrl
					);
				}
				$coordObj = array(
					'lat' => '59.938',
					'long' => '30.313',
				);
				$cityModel = City::model()->findByAttributes([ 'id'=>City::model()->selectedCityId ]);
				if($cityModel) {
					if(!empty($cityModel->longitude) || !empty($cityModel->latitude)) {
						$coordObj['lat'] = $cityModel->latitude;
						$coordObj['long'] = $cityModel->longitude;
					}
				}
				$coord = CJSON::encode($coordObj);
				$json = CJSON::encode($json);
				$this->renderPartial('map', array(
					'data' => $json,
					'search' => 1,
					'coord' => $coord
				));
				exit;
			}

			if($isVkApp)
			{
				$criteria->compare("address.id", $vkAddress);
				$criteria->compare("t.serviceId", "<>EMPORTAL-9728-1ecc-b4d1-4709be278faf");
			}
			
			
			$dataProvider = new CActiveDataProvider($model, array(
				'criteria' => $criteria,
				'sort' => array(),
				'pagination'=>array(
					'pageSize'=>10,
					'currentPage'=>$page,
				),
			));

			if ($json = $this->convertToJSON($dataProvider)) {
				echo $json;
			} else {
				if ($cardsOnly) {
					$this->renderPartial('diagnosticsCardsOnly', array(
							'model' => $model,
							'dataProvider' => $dataProvider,
							'addr' => $this->addr_array,
							'emptyText' => $emptyText,
					));
				}
				else {
					$REQUEST_metro = is_array($_REQUEST["metro"]) ? $_REQUEST["metro"] : ((!empty($_REQUEST["metro"])) ? [$_REQUEST["metro"]] : []);
					$REQUEST_district = is_array($_REQUEST["district"]) ? $_REQUEST["district"] : ((!empty($_REQUEST["district"])) ? [$_REQUEST["district"]] : []);
					$getParamSearchMetroStationId = !empty(Yii::app()->request->getParam('Search')['metroStationId']) 
						? (is_array(Yii::app()->request->getParam('Search')['metroStationId']) 
								? Yii::app()->request->getParam('Search')['metroStationId'] 
								: [Yii::app()->request->getParam('Search')['metroStationId']])
						: [];
					$district_metro = array_merge(array_merge($REQUEST_metro,$REQUEST_district),$getParamSearchMetroStationId);

					foreach ($district_metro as $index=>$value) {
						if(isset($searchModel->metroStations[$value])) { continue; }
						elseif(isset($searchModel->cityDistricts[$value])) { continue; }
						else {
							header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
							$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
							Yii::app()->end();
						}
					}
					$metroList = [];
					$districtlist = [];
					$locative = "";
					$metroDistrictStr = "";
					if(!empty($district_metro)) {
						$searchDistrictString = "";
						foreach ($district_metro as $index=>$value) {
							if(isset($searchModel->metroStations[$value])) {
								if(!empty($locative)) $locative .= ", ";
								else $locative .= "районе ";
								$locative .= "метро " . $searchModel->metroStations[$value];
								
								if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
								$metroDistrictStr .= "метро " . $searchModel->metroStations[$value];
								
								$metroList[] = $searchModel->metroStations[$value];
							}
						}
						foreach ($district_metro as $index=>$value) {
							if(isset($searchModel->cityDistricts[$value])) {
								if(!empty($locative)) $locative .= ", ";
								$locative .= $searchModel->cityDistricts[$value] . " район";
								
								if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
								$metroDistrictStr .= $searchModel->cityDistricts[$value] . " район";

								$districtlist[] = $searchModel->cityDistricts[$value];
							}
						}
					} else {
						$locative = City::model()->findByPk(City::model()->getSelectedCityId())->locative;
					}
					
					$templateRules = [
						"%totalItemCount%" => $dataProvider->totalItemCount,
						"%name%" => trim($spModel->name),
						"%titleName%" => ($spModel->name) ? $spModel->name : 'Диагностика и прочие медицинские услуги',
						"%locative%" => $locative,
						"%metroDistrict%" => $metroDistrictStr,
						"%metroList%" => $metroList,
						"%districtlist%" => $districtlist,
						"%doctorCountableForm%" => MyTools::createCountableForm($dataProvider->totalItemCount, "услуга", "услуги", "услуг"),
						"%searchText%" => CHtml::encode($searchModel->text),
						"%companyType%" => CompanyType::model()->findByPk($searchModel->companyTypeId)->name,
						"%appName%" => Yii::app()->name,
						"\"\"" => " ",
						"  " => " ",
						" ," => ",",
						//"%beginPhrase%" => "Найдено",
						"%inCity%" => (empty($district_metro)) ? ' в ' . $locative : '',
					];
					
					switch ($templateRules['%doctorCountableForm%']) {
						case 'услуг':
							$templateRules['%beginPhraseEnding%'] = 'о';
							break;
						case 'услуга':
							$templateRules['%beginPhraseEnding%'] = 'а';
							break;
						default: //услуги (диагностики)
							$templateRules['%beginPhraseEnding%'] = 'ы';
							break;
					}
					
					$templateRules['metroListStr'] = (!empty($templateRules["%metroList%"]) ? 'у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и ' : '') : '');
					$templateRules['districtlistStr'] = (!empty($templateRules["%districtlist%"]) ? (((count($templateRules["%districtlist%"]) > 1) ? 'в районах' : 'в районе') . ' %districtlist% ') : '');
					
					$templateRules['%beginPhrase%'] = (!empty($templateRules["%searchText%"]) ? 'По названию "'.$templateRules["%searchText%"].'" найден' . $templateRules['%beginPhraseEnding%'] : 'Найден' . $templateRules['%beginPhraseEnding%']);
					
					$pageTagTitle = implode(' ', [
						"%titleName%",
						"%inCity%",
						$templateRules["metroListStr"],
						$templateRules["districtlistStr"],
						"- %appName%"
					]);
					
					$pageTagH1 = implode(' ', [
						$templateRules['%beginPhrase%'],
						"<span class='search-found-count'>%totalItemCount%</span>",
						"%doctorCountableForm%",
						"\"%name%\"",
						"%inCity%",
						$templateRules["metroListStr"],
						$templateRules["districtlistStr"],
					]);
					
					$pageTagDescription = implode(' ', [
						"%titleName%",
						"%inCity%",
						$templateRules["metroListStr"],
						$templateRules["districtlistStr"] . ", цены на услуги в %locative%",
						//$templateRules["metroListStr"],
						//$templateRules["districtlistStr"],
						" с отзывами",
					]);
					
					$pageTagH1 = preg_replace('!\s+!', ' ', MyTools::createFromTemplate(strip_tags($pageTagH1), $templateRules));
					$pageTagTitle = preg_replace('!\s+!', ' ', MyTools::createFromTemplate(strip_tags($pageTagTitle), $templateRules));
					$pageTagDescription = preg_replace('!\s+!', ' ', MyTools::createFromTemplate(strip_tags($pageTagDescription), $templateRules));
					
					$this->render('diagnostics', array(
						'model' => $model,
						'dataProvider' => $dataProvider,
						'addr' => $this->addr_array,
						'companyList' => ShortCompaniesOfDoctor::getCompanyList(),
						'emptyText' => $emptyText,
						'spModel' => $spModel,
						'searchModel' => $searchModel,
						'district_metro' => $searchModel->getRequestedLocative()['district_metro'],
						'templateRules' => $templateRules,
						'pageTagTitle' => $pageTagTitle,
						'pageTagH1' => $pageTagH1,
						'pageTagDescription' => $pageTagDescription,
					));
				}
			}
		} else {
			$this->render('diagnostics', array(
				'searchModel' => $searchModel,
				'dataProvider' => null,
				'emptyText' => $emptyText,
			));
		}
	}

	public function actionAppVk() {

		Yii::app()->controller->breadcrumbs = ['выберите адрес'];

		$isVkApp = Yii::app()->request->getParam('vk');

		$vkGroupId = Yii::app()->request->getParam('group_id');
		if($vkGroupId) {
			Yii::app()->session['vkGroupId'] = $vkGroupId;
		} else {
			$vkGroupId = Yii::app()->session['vkGroupId'];
		}

		$vkViewerId = Yii::app()->request->getParam('viewer_id');
		if($vkViewerId) {
			Yii::app()->session['vkViewerId'] = $vkViewerId;
		} else {
			$vkViewerId = Yii::app()->session['vkViewerId'];
		}

		$vkViewerType = Yii::app()->request->getParam('viewer_type');
		if($vkViewerType && ($vkViewerType == 4 || $vkViewerType == 3)) {
			$sign = ""; 
			foreach ($_GET as $key => $param) { 
			    if ($key == 'hash' || $key == 'sign' || $key == 'vk') continue; 
			    $sign .=$param; 
			} 	
			$secret = '5fDQIinwMeR3wjJuH94x'; 
			$sig = $secret ? hash_hmac('sha256', $sign, $secret) : ""; 
			if($sig != Yii::app()->request->getParam('sign')) {
				$vkViewerType = 1;
			}
		}

		if($vkViewerType) {
			Yii::app()->session['vkViewerType'] = $vkViewerType;
		} else {
			$vkViewerType = Yii::app()->session['vkViewerType'];
		}
		
		$this->layout = '/layouts/main_vk';
		$vkAppModel = VkApp::model()->findAll('groupVk = :groupVk', ['groupVk' => $vkGroupId]);

		$vkClinic = Company::model()->findByPk($vkAppModel[0]->companyId);

		if($vkClinic) {
			$attributes = ['id' => $vkClinic->address->cityId];
			$citySubdomain = City::model()->findByAttributes($attributes)->subdomain;
			Yii::app()->session['selectedRegion'] = $citySubdomain;
		}
		else {
			Yii::app()->session['selectedRegion'] = 'spb';
		}

		if(!count($vkAppModel) || $vkClinic->removalFlag != 0) {
			$this->render('_vkAdmin', [
					'isVkApp'       => $isVkApp,
					'vkGroupId'     => $vkGroupId,
					'vkViewerType'  => $vkViewerType,
					'vkAppModel'    => $vkAppModel,
					'vkClinic'      => $vkClinic,							
				]);
			Yii::app()->end();
		} 


		$criteria = new CDbCriteria;
		$criteria->with = ['company'];
		$criteria->compare('company.id',$vkAppModel[0]->companyId);
		$criteria->compare('isActive',1);
		$addressList = Address::model()->findAll($criteria);

		$addressCap = [];
		foreach ($addressList as $value) {
			$services = AddressServices::model()->count("addressId = :addressId AND isActual = 1 AND serviceId <> 'EMPORTAL-9728-1ecc-b4d1-4709be278faf'",['addressId' => $value->id]);
			$addressCap[$value->id]['service'] = $services ? 1: 0;

			$doctors = PlaceOfWork::model()->count("addressId = :addressId",['addressId' => $value->id]);
			$addressCap[$value->id]['doctor'] = $doctors ? 1: 0;
		}

		//if(count($vkAppModel) > 1 || (!$vkAppModel[0]->addressId && count($addressList) > 1)) {
			$this->render('vkApp', [
					'isVkApp'       => $isVkApp,
					'vkGroupId'     => $vkGroupId,
					'vkViewerType'  => $vkViewerType,
					'vkAppModel'    => $vkAppModel,
					'vkClinic'      => $vkClinic,
					'addressList'   => $addressList,	
					'addressCap'    => $addressCap,						
				]);
		// }
		// else {
		// 	if(count($addressList)) {
		// 		$this->redirect('/doctor?vk=1&address='.$addressList[0]->id);
		// 	} 
		// 	else {
		// 		$this->redirect('/doctor?vk=1&address='.$vkAppModel[0]->addressId);
		// 	}
		// }
	}

	public function actionDoctor($metro = null, $district = null, $specialty = null) {
        
		Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU');
		$this->searchbox_type = SearchBox::S_TYPE_PATIENT;
		$this->searchbox_tab = Search::S_DOCTOR;
		$this->seotype = "doctor";
		$sexTypesList = CHtml::listData(Sex::model()->findAll(), 'id', 'name');

		$isVkApp = Yii::app()->request->getParam('vk');
		if($isVkApp) {
			$vkGroupId = Yii::app()->session['vkGroupId'];
			$vkViewerType = Yii::app()->session['vkViewerType'];

			$vkAddress = Yii::app()->request->getParam('address');
			if($vkAddress) {
				Yii::app()->session['vkAddress'] = $vkAddress;
			} else {
				$vkAddress = Yii::app()->session['vkAddress'];
			}
			
			$this->layout = '/layouts/main_vk';
			$vkAppModel = VkApp::model()->findAll('groupVk = :groupVk', ['groupVk' => $vkGroupId]);

			$vkClinic = Company::model()->findByPk($vkAppModel[0]->companyId);

			if($vkClinic) {
				$attributes = ['id' => $vkClinic->address->cityId];
				$citySubdomain = City::model()->findByAttributes($attributes)->subdomain;
				Yii::app()->session['selectedRegion'] = $citySubdomain;
			}
			else {
				Yii::app()->session['selectedRegion'] = 'spb';
			}

			$doctorSpecialty = Yii::app()->request->getParam('Search',[]);
			if($doctorSpecialty['doctorSpecialty']) {
				$specialty = $doctorSpecialty['doctorSpecialty'];
			}
		}

		$searchModel = new Search(Search::S_DOCTOR);
		$searchModel->attributes = Yii::app()->request->getParam(get_class($searchModel));
		if (Yii::app()->params['samozapis']) {
			$searchModel->initOMSForm();
		}
		$this->searchModel = &$searchModel;
		
		if (!empty($metro)) {
			$searchModel->metroStationId[] = $metro;
			$isMetro = true;
			$this->seotype = "noindex";
		}
		if (!empty($district)) {
			$searchModel->metroStationId[] = $district;
			$isMetro = false;
			$this->seotype = "noindex";
		}
		if (!empty($specialty)) {
			$searchModel->doctorSpecialtyId = $specialty;
			$this->seotype = "none";
		}
		
		if (
				($metro == null && $district == null && $specialty == null) &&
				(
					(is_array($searchModel->metroStationId) && $searchModel->metroStationId && count($searchModel->metroStationId) == 1) ||
					($searchModel->doctorSpecialtyId && !is_array($searchModel->doctorSpecialtyId))
				)
		) {
			$needRedirect = true;
		}
		$cardsOnly = Yii::app()->request->getParam('cardsOnly');
		$topDocs = Yii::app()->request->getParam('topDocs');
		$page = (!empty(Yii::app()->request->getParam('page'))) ? (int) Yii::app()->request->getParam('page') - 1 : 0;

		if ($needRedirect) {
			$redirectParams = [];
			
			if (!empty($searchModel->metroStationId) && count($searchModel->metroStationId) == 1) {
				if (strpos($searchModel->metroStationId[0], 'metro') !== FALSE) {
					$redirectParams['metro'] = $searchModel->metroStationId[0];
				} elseif (strpos($searchModel->metroStationId[0], 'raion') !== FALSE) {
					$redirectParams['district'] = $searchModel->metroStationId[0];
				} else {
					$metroLinkUrl = MetroStation::model()->findByPk($searchModel->metroStationId[0])->linkUrl;
					if ($metroLinkUrl) {
						$redirectParams['metro'] = 'metro-' . $metroLinkUrl;
					}
					$cdLinkUrl = CityDistrict::model()->findByPk($searchModel->metroStationId[0])->linkUrl;
					if ($cdLinkUrl) {
						$redirectParams['district'] = 'raion-' . $cdLinkUrl;
					}
				}
				$searchModel->metroStationId = null;
			}
			if (!empty($searchModel->doctorSpecialtyId)) {
				if (is_string($searchModel->doctorSpecialtyId) AND strpos($searchModel->doctorSpecialtyId, 'specialty') !== FALSE) {
					$redirectParams['specialty'] = $searchModel->doctorSpecialtyId;
				} else {
					$dsLinkUrl = ShortSpecialtyOfDoctor::model()->findByAttributes(["id"=>$searchModel->doctorSpecialtyId, "cityId"=>City::model()->selectedCityId, "samozapis"=>(Yii::app()->params["samozapis"]) ? 1 : 0])->linkUrl;
					if ($dsLinkUrl) {
						$redirectParams['specialty'] = 'specialty-' . $dsLinkUrl;
					}
				}
				$searchModel->doctorSpecialtyId = null;
			}
			
			if (!empty($redirectParams)) {
				if (isset($_REQUEST['JSON'])) {
					$redirectParams['JSON'] = 1;
				}
				$redirectParams = array_merge(['search/doctor'], $redirectParams);
			}

			foreach ($searchModel->attributes as $name => $value) {
				if (!empty($value)) {
					$redirectParams['Search[' . $name . ']'] = $value;
				}
			}
			
			if($tempParam = Yii::app()->request->getParam('shortSearch')) {
				$redirectParams['shortSearch'] = $tempParam;
			}
			if($tempParam = Yii::app()->request->getParam('isWidget')) {
				$redirectParams['isWidget'] = $tempParam;
			}
			if($tempParam = Yii::app()->request->getParam('page')) {
				$redirectParams['page'] = $tempParam;
			}
			
			if (count($redirectParams) == 2 AND isset($redirectParams['metro']))
				$redirectParams = ['doctor' . '/' . $redirectParams['metro']];
			if (count($redirectParams) == 2 AND isset($redirectParams['district']))
				$redirectParams = ['doctor' . '/' . $redirectParams['district']];
			
			$this->redirect($redirectParams);
		}
		/* clean search values from aliases */
		$searchModel->metroStationId = str_replace([
			'metro-',
			'raion-'
				], '', $searchModel->metroStationId);

		$searchModel->doctorSpecialtyId = str_replace([
			'specialty-'
				], '', $searchModel->doctorSpecialtyId);
		
		#$searchModel->price = Yii::app()->request->getParam('price');
		$SearchText = mb_strtolower(MyTools::cleanSearchText($searchModel->text), "utf8");
		
		$emptyText = 'К сожалению, поиск не дал результатов. Измените параметры запроса и попробуйте снова.';
        $notificationText;
		
		if ($searchModel->validate())
		{
			$spModel = DoctorSpecialty::model()->findByLink($searchModel->doctorSpecialtyId);
				
			$criteria = new CDbCriteria;
			$criteria->select = "name,linkUrl,link,sexId,rating,experience,countRecords";
			$criteria->group = '`t`.`id`';
			$criteria->order = "t.rating DESC, t.name ASC";
			$criteria->with = array(
				'appointmentCount' => array(
					'together' => true
				),
				'placeOfWorks' => array(
					'together' => true,
					'select' => 'id,doctorId,addressId',
					'with' => array(
						'address' => [
							'select' => "id,street,houseNumber,link,ownerId,cityId,linkUrl,name,latitude,longitude,samozapis",
							'together' => true,
							#'scopes' => 'active',
							'with' => [
								'userMedicals' => [
									'together' => true,
									'select' => false,
								],
								'company' => [
									'together' => true,
									'select' => 'id,name,ratingSite,rating,ratingUser,link,linkUrl',
									'with' => [
										'companyContracts' => [
											'joinType' => 'INNER JOIN',
											'select' => false,
											'together' => true
										]
									]
								],
								'metroStations' => [
									'together' => false,
									'select' => 'id,name,linkUrl'
								]
							]
						]
					),
				),
				'scientificTitle' => [
					'select' => 'id,name',
					'together' => true
				],
				'scientificDegrees' => [
					'select' => 'id,name',
					'together' => true
				],
				'sex' => [
					'select' => 'id,name,order',
					'together' => true
				],
			);
			
			if (!$topDocs && Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) {
				$loader = new EmiasLoader();
				$attr = [
						'omsNumber' => $searchModel->OMSForm_omsNumber,
						'omsSeries' => $searchModel->OMSForm_omsSeries,
						'referralId' => trim(strval($searchModel->OMSForm_referralId)),
						'birthDate' => date('Y-m-d\TH:i:s',strtotime($searchModel->OMSForm_birthday)),
				];
				if(is_object($spModel)) {
					$extSpeciality = ExtSpeciality::model()->findByAttributes(['sysTypeId'=>$loader->systype, 'specialtyId'=>$spModel->id]);
					if(is_object($extSpeciality)) $attr['specialityId'] = $extSpeciality->extId;
				}
				$emiasItems = [];
				if($attr['omsNumber'] !== "" && $attr['birthDate'] !== "" && !$searchModel->OMSFormData_check()) {
					$emptyText_head = "При запросе данных произошла ошибка: <span style='color:red;'>".join("<br>", $searchModel->OMSForm_exceptions) . "</span>";
					$emptyText_step0 = "Вы можете:";
					$emptyText_step1 = "1. <a href='javascript:void(0)' onclick='showOMSpopupView(); return false;' class=\"normal-link\">проверить</a> корректность используемых ОМС данных";
					$emptyText_step2 = "2. повторить запрос позже, если вы уверены в корректности ОМС данных";
					$notificationText = $emptyText_head . "<br>" . $emptyText_step0 . "<br>" . $emptyText_step1 . "<br>" . $emptyText_step2;
				}
				elseif($attr['omsNumber'] === "" || $attr['birthDate'] === "" || ($attr['referralId'] === "" && (!isset($attr['specialityId'])))) {
					$emptyText_head = "Для просмотра всех доступных вам врачей необходимо:";
					
					if($attr['omsNumber'] === "" || $attr['birthDate'] === "") {
						$emptyText_step1 = "
							<li>
									<a style='font-weight: bold;' href='javascript:void(0)' onclick='showOMSpopupView(); return false;' class=\"normal-link\">заполнить</a> ваши данные ОМС
							</li>";
					}
					elseif(($attr['referralId'] === "" && (!isset($attr['specialityId'])))) {
                    	$emptyText_step2 = "
                    		<li>
                    				<a style='font-weight: bold;' href='javascript:void(0)' onclick='openSpecialtySelect(); return false;' class=\"normal-link\">выбрать</a> специальность врача
                    		</li>
                    		<li>
                    				или <a style='font-weight: bold;' href='javascript:void(0)' onclick='emias.showReferrals(); return false;' class='normal-link'>выбрать</a> направление
                    		</li>";
					}
					
					$notificationText = $emptyText_head . "<ul style='padding: 5px 0px 5px 25px; margin: 0 0 0 0;'>" . @$emptyText_step1 . @$emptyText_step2 . "</ul>";
                    
                    $emiasItems = ['none'];
                    $criteria->with = array_replace_recursive($criteria->with, [
                        'extDoctor' => [
                            'select' => '(extDoctor.doctorId) AS extDoctor',
                            //'together' => 'true',
                            'joinType' => 'INNER JOIN',
                        ]
                    ]);
				}
				elseif(empty($emiasItems)) {
					$emiasInfo = $loader->getDoctorsInfo($attr);
					if(isset($emiasInfo->availableResource)) {
						$emiasResponse = $loader->populateDoctorList($emiasInfo->availableResource, $attr, false);
						$emiasItems = array_keys($emiasResponse);
					} else {
						$notificationText = "Ошибка: ";
						if(isset($emiasInfo->message)) {
							$notificationText .= $emiasInfo->message;
						}
						elseif(isset($emiasInfo->detail->exception)) {
							$notificationText .= strval($emiasInfo->detail->exception);
						} else {
							$notificationText .= "неизвестная";
						}
					}
                    $criteria->addInCondition('t.id', $emiasItems);
				}
			}
			
			/* metroStationId can be metro Id or cityDistrictID */
			if($searchModel->doctorSpecialtyId) {
				$dsModel = ShortSpecialtyOfDoctor::model()->findByAttributes(["linkUrl"=>$searchModel->doctorSpecialtyId, "cityId"=>City::model()->selectedCityId, "samozapis"=>(Yii::app()->params["samozapis"]) ? 1 : 0]);
				if(!$dsModel) {
					header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
					$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
					Yii::app()->end();
				}
			}

			$model = new Doctor('search');
			$model->unsetAttributes();  // clear any default values

			/* search only by surName here */
			
			function getSearchText($SearchText) {
				$surNameSearchIds = array();
				
				$regex = Search::regexpForQuery($SearchText);
				$regexTranslite = Search::regexpForQuery(MyTools::translitAuto($SearchText));
			
				if (!empty($regex)) {
					$regexCriteria = new CDbCriteria;
					$regexCriteria->select = "t.id,t.name";
					$regexCriteria->params = array(':search' => $regex, ':searchT' => $regexTranslite);
					$regexCriteria->index = 'id';
					#$regexCriteria->scopes = 'clinic';
			
					$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :search",), 'OR');
					$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :searchT",), 'OR');
			
					$companiesBySearchText = Doctor::model()->findAll($regexCriteria);
					$surNameSearchIds = array_keys($companiesBySearchText);
				}
				if(count($surNameSearchIds) < 1) {
					$regex = Search::regexpForQuery($SearchText, 2);
					if (!empty($regex)) {
						$regexTranslite = Search::regexpForQuery(MyTools::translitAuto($SearchText));
						$regexCriteria = new CDbCriteria;
						$regexCriteria->select = "t.id,t.name";
						$regexCriteria->params = array(':search' => $regex, ':searchT' => $regexTranslite);
						$regexCriteria->index = 'id';
						$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :search"), 'OR');
						$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :searchT",), 'OR');
			
						$companiesBySearchText = Doctor::model()->findAll($regexCriteria);
						$surNameSearchIds = array_keys($companiesBySearchText);
					}
				}
				return $surNameSearchIds;
			}
			
			if ($SearchText) {
				$surNameSearchIds = getSearchText($SearchText);
				if (empty($surNameSearchIds)) {
					$text = MyTools::switcher($SearchText, 2);
			
					$surNameSearchIds = getSearchText($text);
				}
			}
			if ($searchModel->offline) {
				Address::$showInactive = true;
				unset($criteria->with['placeOfWorks']['with']['address']['with']['userMedicals']);
				unset($criteria->with['placeOfWorks']['with']['address']['with']['company']['with']['companyContracts']);
			} else {
				$criteria->compare('userMedicals.agreementNew',1);
			}
			
			if ($searchModel->addressLink) {
                //если поиск идет по конкретному адресу, не берем во внимание город
				$criteria->compare('address.link', $searchModel->addressLink);
			} else {
                $criteria->compare('address.cityId', $searchModel->cityId);
            }
			
			if (!empty($SearchText)) {
				$criteria->addInCondition('t.id', $surNameSearchIds);
			}

			/* if (!empty($surNameSearchIds) && !empty($specialtySearchIds)) {
			  $criteria->addInCondition('t.id', array_intersect($surNameSearchIds, $specialtySearchIds));
			  } elseif (!empty($surNameSearchIds)) {
			  $criteria->addInCondition('t.id', $surNameSearchIds);
			  } elseif (!empty($specialtySearchIds)) {
			  $criteria->addInCondition('t.id', $specialtySearchIds);
			  } else {
			  if (trim($SearchText)) {
			  $criteria->addCondition("t.id = ''");
			  }
			  } */
			$criteria->AddCondition('`placeOfWorks`.`id` IS NOT NULL'); // Если не заполнено текущее место работы

            
			if ($searchModel->metroStationId) {
				$criteria->with['placeOfWorks']['with']['address']['with'] = array_merge($criteria->with['placeOfWorks']['with']['address']['with'], array(
					'metroStations' => array(
						'together' => true,
						'select' => false,
					),
				));
				$criteria->with['placeOfWorks']['with']['address']['with'] = array_merge($criteria->with['placeOfWorks']['with']['address']['with'], array(
					'cityDistrict' => array(
						'together' => true,
						'select' => false
					),
				));

				if ($searchModel->expandSearchArea == 1) {
					$metro = MetroStation::model()->find("id = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
					$criteria->addBetweenCondition('metroStations.longitude', $metro->longitude - $this->GEOinterval, $metro->longitude + $this->GEOinterval);
					$criteria->addBetweenCondition('metroStations.latitude', $metro->latitude - $this->GEOinterval, $metro->latitude + $this->GEOinterval);
				} else {
					if (is_array($searchModel->metroStationId)) {
						$tempMetroStation = [];
						foreach ($searchModel->metroStationId as $metroStationId) { $tempMetroStation[] = Yii::app()->db->quoteValue($metroStationId); }
						$queryStringMetroStation = implode(",", $tempMetroStation);
						$criteria->addCondition("metroStations.id in (" . $queryStringMetroStation . ")"
								. " OR metroStations.linkUrl in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.id in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.linkUrl in (" . $queryStringMetroStation . ")");
					} else {
						$criteria->addCondition("metroStations.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR metroStations.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
					}
				}
			}
			if ($searchModel->cityDistrictId) {
				if (!$criteria->with['placeOfWorks']['with']['address']) {
					$criteria->with['placeOfWorks']['with']['address'] = array(
						'select' => "id,link",
						'together' => true,
						'with' => array()
					);
				}
				$criteria->with['placeOfWorks']['with']['address']['with'] = array_merge($criteria->with['placeOfWorks']['with']['address']['with'], array(
					'cityDistrict' => array(
						'together' => true,
						'select' => false
					),
				));
				$criteria->compare('`cityDistrict`.`id`', $searchModel->cityDistrictId);
			}

			if ($searchModel->doctorSpecialtyId) {
				$criteria->with['specialtyOfDoctors'] = array(
					'together' => true,
					'select' => false,
					'with' => array(
						'doctorSpecialty' => array(
							'together' => true,
							'select' => 'name,shortSpecialtyId',
						),
						'doctorCategory' => array(
							'together' => true,
							'select' => 'name',
						)
					)
				);
				$shortLinks = ShortSpecialtyOfDoctor::model()->findAll(['select' => 'id,linkUrl', 'index' => 'linkUrl']);
				$short = ShortSpecialtyOfDoctor::model()->findAll(['select' => 'id,linkUrl', 'index' => 'id']);
				$shortIds = array_keys(array_merge($short, $shortLinks));

				if (!in_array($searchModel->doctorSpecialtyId, $shortIds)) {
					if (is_array($searchModel->doctorSpecialtyId)) {
						$criteria->addInCondition('`specialtyOfDoctors`.doctorSpecialtyId', $searchModel->doctorSpecialtyId);
					} else {
						$criteria->compare('`specialtyOfDoctors`.doctorSpecialtyId', $short[$searchModel->doctorSpecialtyId]->id);
					}
				} else {
					$criteria->addCondition("`doctorSpecialty`.shortSpecialtyId = '" . $short[$searchModel->doctorSpecialtyId]->id . "'"
							. " OR `doctorSpecialty`.shortSpecialtyId = '" . $shortLinks[$searchModel->doctorSpecialtyId]->id . "'");
				}
			}
			if ($searchModel->sexId)
				$criteria->compare('`t`.`sexId`', $searchModel->sexId);

			if ($searchModel->onHouse)
				$criteria->compare('onHouse', 1);

			if ($searchModel->loyaltyProgram) {
				$selectedCity = City::model()->findByPk($searchModel->cityId);
				if(is_object($selectedCity) && Yii::app()->params['regions'][$selectedCity->subdomain]['loyaltyProgram']) {
					$criteria = Doctor::getIsLoyaltyProgramParticipantCriteria($criteria);
				}
			}

			if (is_numeric($searchModel->priceMin) || is_numeric($searchModel->priceMax)) {
				$criteria->with['doctorServices'] = array(
					'select' => "price",
					'joinType' => 'LEFT JOIN',
					'together' => true,
					'with' => [
						'cassifierService' => [
							'select' => 'id,link,name',
							'joinType' => 'LEFT JOIN',
							'together' => true,
							'condition' => "cassifierService.link = 'first'",
						],
					],
				);
				if ($searchModel->priceMin) {
					$criteria->addCondition((intval($searchModel->priceMin)>0 ? "doctorServices.free != 1 AND " : "") . "doctorServices.price >= " . intval($searchModel->priceMin) );
				}
				if ($searchModel->priceMax) {
					if ($searchModel->priceMin) {
						$criteria->addCondition("doctorServices.price <= " . intval($searchModel->priceMax) );
					} else {
						$criteria->addCondition("doctorServices.free = 1 OR doctorServices.price <= " . intval($searchModel->priceMax) );
					}
				}
			}
			if($searchModel->sort) {
				$explodeSort = explode(" ", $searchModel->sort);
				switch ($explodeSort[0]) {
					case 'price':
						$criteria->with['doctorServices'] = array(
							'select' => "price",
							'joinType' => 'LEFT JOIN',
							'together' => true,
							'with' => [
								'cassifierService' => [
									'select' => 'id,link,name',
									'joinType' => 'LEFT JOIN',
									'together' => true,
									'condition' => "cassifierService.link = 'first'",
								],
							],
						);
						$priceOrder = "doctorServices.".trim(Yii::app()->db->quoteValue($searchModel->sort),"'");
						$criteria->order = "(doctorServices.price>0 OR doctorServices.free=1) desc, $priceOrder, t.link";
						break;
					case 'countRecords':
						$criteria->order = "t.countRecords DESC";
						break;
					case 'countCommentDesc' :
						$criteria->with['reviews'] = array(
							'joinType' => 'LEFT JOIN',
							'together' => true
						);
						$criteria->addCondition('reviews.statusId = 2');
						$criteria->order = "count(reviews.id) DESC";
						break;
					case 'countCommentAsc' :
						$criteria->with['reviews'] = array(
							'joinType' => 'LEFT JOIN',
							'together' => true
						);
						$criteria->addCondition('reviews.statusId = 2');
						$criteria->order = "count(reviews.id) ASC";
						break;
					case 'experience':
					case 'rating':
					default:
						$sort_parametr = trim(Yii::app()->db->quoteValue($explodeSort[0]),"'");
						$criteria->order = "(t.{$sort_parametr} IS NOT NULL AND t.{$sort_parametr} != '') DESC, t.".trim(Yii::app()->db->quoteValue($searchModel->sort),"'");
						break;
				}
			}

			if ($searchModel->clinicLinkUrl) {
				$criteria->addCondition("`company`.`linkUrl` = ".  Yii::app()->db->quoteValue($searchModel->clinicLinkUrl));
			}

			//Не гос медицинская клиника
			//Есть контракт
			//Тип контракта с кнопкой
			//	&& !empty($company->companyContracts)
			//	&& $company->companyContracts->contractTypeId=='d9c0f169-9bd2-11e2-856f-e840f2aca94f'
			$show = 0;
			$company = $model->placeOfWorks[0]->address->company;
			if (!in_array($company->companyType->id, Address::$arrGMU)) {
				$show = 1;
			}
			if (!$searchModel->addressLink) {
				$criteria->compare("address.samozapis", (Yii::app()->params["samozapis"]) ? 1 : 0);
			}
			
			if (Yii::app()->request->getParam('limit')) {
				$paginationSettings = false;
				$criteria->limit = min([(int)Yii::app()->request->getParam('limit'), 100]);
			} else {
				$paginationSettings = [
					'pageSize' => isset($_REQUEST['photo']) ? 400 : 10,
					'pageVar' => 'page',
				];
			}
			
			if($isVkApp)
			{
				$criteria->compare("address.id", $vkAddress);

				$specialtyCriteria = new CDbCriteria();
				$specialtyCriteria->with = [
					'doctorSpecialties' => [
						'select' => false,
						'with' => [
							'select' => false,
							'specialtyOfDoctors' => [
								'select' => false,
								'with' => [
									'select' => false,
									'doctor' => [
										'select' => false,
										'with' => [
											'select' => false,
											'placeOfWorks'
										]
									]
								]
							]
						]
					]
				];
				$specialtyCriteria->order = 't.name ASC';
				$specialtyCriteria->compare('placeOfWorks.addressId', $vkAddress);
				$doctorSpecialties = CHtml::listData(ShortSpecialtyOfDoctor::model()->findAll($specialtyCriteria), 'linkUrl', 'name');
			}
			/*
			 * Показать на карте
			 */
			$showOnMap = Yii::app()->request->getParam('showOnMap');
			if($showOnMap) {
				//$criteria->group = '`address`.`id`';
				$criteria->order = "";
				$mapData = $model->findAll($criteria);
				foreach ($mapData as $key => $value) {
					$adr = $value->placeOfWorks[0]->address;

					$names = explode(' ', $value->name);
					$surName = $names[0];
					$firstName = mb_substr($names[1], 0, 1);
					$fatherName = mb_substr($names[2], 0, 1);
					$name = $surName." ".$firstName.". ".$fatherName.".";

					$address = $adr->street.', '.$adr->houseNumber.' - "'.$adr->company->name.'"';
					
					$price = "";
 					if ($value->inspectPrice || $value->InspectFree) {
						$price = !$value->InspectFree ? $value->inspectPrice . 'р.' : 'бесплатно';
 					}

					$json[] = array(
						'id' => $adr->id,
						'name' => str_replace("'", "&#39;", $name),
						'address' => str_replace("'", "&#39;", $address),
						'price' => $price,
						'lat' => $adr->latitude,
						'long' => $adr->longitude,
						'url' => $adr->company->linkUrl . '/' . $adr->linkUrl . '/' . $value->linkUrl,
						//'phone' => $phone_arr[$adr->id]
					);
				}
				$coordObj = array(
					'lat' => '59.938',
					'long' => '30.313',
				);
				$cityModel = City::model()->findByAttributes([ 'id'=>City::model()->selectedCityId ]);
				if($cityModel) {
					if(!empty($cityModel->longitude) || !empty($cityModel->latitude)) {
						$coordObj['lat'] = $cityModel->latitude;
						$coordObj['long'] = $cityModel->longitude;
					}
				}
				$coord = CJSON::encode($coordObj);
				$json = CJSON::encode($json);
				$this->renderPartial('map', array(
					'data' => $json,
					'search' => 1,
					'coord' => $coord
				));
				exit;
			}
			$companyListCriteria = new CDbCriteria;
			$companyListCriteria->select = "t.companyLinkUrl, t.companyName"; //denormCitySubdomain
			$companyListCriteria->order = "t.companyName";
			if (!$searchModel->addressLink) {
				$companyListCriteria->compare("samozapis",(Yii::app()->params["samozapis"]) ? 1 : 0);
			}
			$companyListCriteria->compare("cityId",$selectedCityId);
			
			$shortCompanies = ShortCompaniesOfDoctor::model()->findAll($companyListCriteria);
			
			$companyList = CHtml::listData($shortCompanies, 'companyLinkUrl', 'companyName');
			
			$dataProvider = new CActiveDataProvider($model, array(
				'criteria' => $criteria,
				'pagination' => $paginationSettings,
			));
			
			if(Yii::app()->request->getParam('onlyIds')) {
				$criteria->group = 't.id';
				$criteria->order = '';
				$data = $model->findAll($criteria);
				$ids = [];
				foreach ($data as $row) {
					$ids[] = $row->id;
				}
				echo CJSON::encode($ids);
				exit;
			}
			if(Yii::app()->request->getParam('downloadcsv')) {
				$dataProvider->pagination = false;
				$doctorFIO = [];
				foreach ($dataProvider->getData() as $row) {
					$doctorFIO[] = [$row->name];
				}
				FileMaker::createCSV("activeDoctors", $doctorFIO, ["Фамилия Имя Отчество"]);
				exit;
			}
			
			foreach ($dataProvider->getData() as $row) {
				$doctorIds[] = $row->id;
			}
			
			$infoCriteria = new CDbCriteria();
			$infoCriteria->index = "id";
			$infoCriteria->select = 'id';
			$infoCriteria->addInCondition('t.id', $doctorIds);			
			$infoCriteria->with = array(
				/*'placeOfWorks' => array(
					'together' => true,
					'select' => 'addressId',
					'with' => array(
						'company' => array(
							'together' => true,
							'select' => 'name,linkUrl',
						),
						'address' => array(
							'together' => true,
							'select' => 'name,street,houseNumber,link,cityId,linkUrl',
							'joinType' => 'INNER JOIN',
							'with' => array(
								'metroStations' => array(
									'together' => true,
									'select' => 'name',
								),
								'cityDistrict' => array(
									'together' => true,
									'select' => 'name',
								),
							)
						),
					),
				),
				'scientificDegrees' => array(
					'together' => true
				),*/
				'specialtyOfDoctors' => array(
					'together' => true,
					'with' => array(
						'doctorSpecialty' => array(
							'together' => true,
							'select' => 'name, linkUrl',
						),
						'doctorCategory' => array(
							'together' => true,
							'select' => 'name',
						)
					)
				),
				'doctorServices' => array(
					'select' => 'price,free',
					'together' => true,
					'with' => array(
						'cassifierService' => array(
							'select' => 'link',
							'together' => true,
						)
					)
				),
			);
			$isWidget = Yii::app()->request->getParam('isWidget');
			
			$doctors = Doctor::model()->findAll($infoCriteria);
			
			if ($json = $this->convertToJSON($dataProvider)) {
				echo $json;
			} else {
				if(!empty($district_metro) AND empty($spModel)) { $this->seotype = 'hideseo'; }
				if ($cardsOnly) {
					if ((int)$cardsOnly === 2) {
						$this->renderPartial('doctorCardsOnlyV2', array(
								'model' => $model,
								'doctors' => $doctors,
								'dataProvider' => $dataProvider,
								'show' => $show,
								'sexTypesList' => $sexTypesList,
								'emptyText' => $emptyText,
                                'notificationText' => $notificationText,
								'isWidget' => $isWidget,
						));
					} else {
						header("Access-Control-Allow-Origin: *");
						$this->renderPartial('doctorCardsOnly', array(
								'model' => $model,
								'doctors' => $doctors,
								'dataProvider' => $dataProvider,
								'show' => $show,
								'sexTypesList' => $sexTypesList,
								'emptyText' => $emptyText,
                                'notificationText' => $notificationText,
								'isWidget' => $isWidget,
						));
					}
					exit;
				}
				else {
					$REQUEST_metro = is_array($_REQUEST["metro"]) ? $_REQUEST["metro"] : ((!empty($_REQUEST["metro"])) ? [$_REQUEST["metro"]] : []);
					$REQUEST_district = is_array($_REQUEST["district"]) ? $_REQUEST["district"] : ((!empty($_REQUEST["district"])) ? [$_REQUEST["district"]] : []);
					$getParamSearchMetroStationId = !empty(Yii::app()->request->getParam('Search')['metroStationId']) 
						? (is_array(Yii::app()->request->getParam('Search')['metroStationId']) 
								? Yii::app()->request->getParam('Search')['metroStationId'] 
								: [Yii::app()->request->getParam('Search')['metroStationId']])
						: [];
					$district_metro = array_merge(array_merge($REQUEST_metro,$REQUEST_district),$getParamSearchMetroStationId);
					foreach ($district_metro as $index=>$value) {
						if(isset($searchModel->metroStations[$value])) { continue; }
						elseif(isset($searchModel->cityDistricts[$value])) { continue; }
						else {
							header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
							$this->render('//site/error404',['code' => 404, 'message' => 'Страница не найдена']);
							Yii::app()->end();
						}
					}
					$metroList = [];
					$districtlist = [];
					$locative = "";
					$metroDistrictStr = "";
					if(!empty($district_metro)) {
						$searchDistrictString = "";
						foreach ($district_metro as $index=>$value) {

							if(isset($searchModel->metroStations[$value])) {
								if(!empty($locative)) $locative .= ", ";
								else $locative .= "у ";
								$locative .= "метро " . $searchModel->metroStations[$value];
								
								if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
								$metroDistrictStr .= "метро " . $searchModel->metroStations[$value];
								
								$metroList[] = $searchModel->metroStations[$value];
							}
						}
						foreach ($district_metro as $index=>$value) {
							$cityDistrictModel = CityDistrict::model()->find("link=:link OR linkUrl=:link OR id=:link", [':link'=>mb_substr($value, stripos($value,'-')+1)]);
							$arrLinkUrlDistrict[] =$cityDistrictModel->attributes->linkUrl;
							if($cityDistrictModel) {
								if(!empty($locative)) $locative .= ", ";
								$locative .= $searchModel->cityDistricts[$value] . " район";
								
								if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
								$metroDistrictStr .= $searchModel->cityDistricts[$value] . " район";

								$districtlist[] = $cityDistrictModel->getLocative();
							}
						}
						$dataSpecList = ShortSpecialtyOfDoctor::getGeoSpecialty(array_merge($district_metro, $arrLinkUrlDistrict));
					} else {
						$dataSpecList = ShortSpecialtyOfDoctor::getCitySpecialty(City::getSelectedCity()->id);
						$locative = City::getSelectedCity()->locative;
					}
					$doctorCountableForm = MyTools::createCountableForm($dataProvider->totalItemCount, "врач", "врача", "врачей");
					
					$lastTotalCountDigit = substr($dataProvider->totalItemCount, -1, 1);
					$twoLastTotalCountDigits = (int)substr($dataProvider->totalItemCount, -2, 2);
					switch($lastTotalCountDigit) {
						case 1:
							if (($twoLastTotalCountDigits >= 11) && ($twoLastTotalCountDigits <= 14)) {
								$specialtyName = ($spModel->genitivePlural) ? $spModel->genitivePlural : ($doctorCountableForm . ' специальности ' . $spModel->name);
							} else {
								$specialtyName = $spModel->name;
							}
							break;
						case 2:
						case 3:
						case 4:
							if (($twoLastTotalCountDigits >= 11) && ($twoLastTotalCountDigits <= 14)) {
								$specialtyName = ($spModel->genitivePlural) ? $spModel->genitivePlural : ($doctorCountableForm . ' специальности ' . $spModel->name);
							} else {
								$specialtyName = $spModel->genitive;
							}
							break;
						default:
							$specialtyName = ($spModel->genitivePlural) ? $spModel->genitivePlural : ($doctorCountableForm . ' специальности ' . $spModel->name);
							break;
					}


					$templateRules = [
							"%totalItemCount%" => $dataProvider->totalItemCount,
							"%name%" => (!empty($specialty)) ? $specialtyName : $doctorCountableForm,
							"%namePlural%" => (is_object($spModel)) ? $spModel->getPlural() : "",
							"%nameDative%" => (is_object($spModel)) ? $spModel->getDative() : "",
							"%locative%" => $locative,
							"%metroDistrict%" => $metroDistrictStr,
							"%metroList%" => $metroList,
							"%districtlist%" => $districtlist,
							"%doctorCountableForm%" => $doctorCountableForm,
							"%searchText%" => CHtml::encode($searchModel->text),
							"%companyType%" => CompanyType::model()->findByPk($searchModel->companyTypeId)->name,
							"%appName%" => Yii::app()->name,
							"\"\"" => " ",
							"  " => " ",
							" ," => ",",
							"%foundWordEnding%" => ($lastTotalCountDigit == 1) ? '' : 'о',
							"%inCity%" => (empty($district_metro)) ? 'в ' . $locative : '',
					];
					$nameDative = MyTools::mb_lcfirst($templateRules["%nameDative%"]);

					$title = (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
						. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist%' : '');

					$titleH1 = MyTools::mb_ucfirst($templateRules["%namePlural%"]) . $title;
					$tagH1 = trim((!empty($templateRules["%searchText%"]) ? 'По фамилии "'.$templateRules["%searchText%"].'" найден%foundWordEnding% ' : 'Найден%foundWordEnding% ')
								. "<span class='search-found-count'>%totalItemCount%</span> %name% %inCity%"
								. (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
								. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist% ' : ' '));

					if(!empty($specialty)) {
						$keywords[] = $spModel->name.' в '.City::getSelectedCity()->locative;
						$keywords[] = ((mb_stripos($spModel->name, "врач") === false) ? "врач" : "") . " " . $spModel->name;
						$keywords[] = 'записаться онлайн к '.$spModel->getDative();
					}
					if(!empty($specialty) && (!empty($templateRules["%districtlist%"]) || !empty($templateRules["%metroList%"])) ) {
						$titleText = (Yii::app()->params["samozapis"] ? ("Самозапись к ".$templateRules["%nameDative%"]) : MyTools::mb_ucfirst($templateRules["%namePlural%"]))
								. (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
								. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist%' : '')
								. (!empty($templateRules["%districtlist%"]) ? ' '.(City::getSelectedCity()->getShortGenitive()) : '')
								. (Yii::app()->params["samozapis"] ? "" : ": записаться на прием к врачу онлайн через Единый Медицинский Портал");
						$title = (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
						. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist%' : '');

						//$titleH1 = MyTools::mb_ucfirst($templateRules["%namePlural%"]) . $title;
						$tagH1 = "Найдено ".$dataProvider->totalItemCount." ".$doctorCountableForm." для записи онлайн";
						$tagDescription = (!empty($templateRules["%districtlist%"]) ? "С помощью Единого Медицинского Портала" : "На сайте Единого Медицинского Портала")
								. " вы можете записаться к " . ((mb_stripos($nameDative, "врач") === false)?"врачу-":"").$nameDative
								. (!empty($templateRules["%metroList%"]) ? ' у метро %metroList%' . ((!empty($templateRules["%districtlist%"])) ? ' и' : '') : '')
								. (!empty($templateRules["%districtlist%"]) ? ' в %districtlist%' : '')
								. (!empty($templateRules["%districtlist%"]) ? ' '.(City::getSelectedCity()->getShortGenitive()) : '')
								. " в 1 клик не выходя из дома. Только настоящие отзывы";
						foreach (explode(',', $metroDistrictStr) as $value) {
							$keywords[] = $spModel->name." ".trim($value)." ".City::getSelectedCity()->getShortGenitive();
						}
						$keywords[] = "записаться онлайн";
					}
					elseif(!empty($specialty) && empty($district_metro)) {
						if(Yii::app()->params["samozapis"]) {
							$titleText = "Самозапись к ".$templateRules["%nameDative%"]
								." в ".City::getSelectedCity()->getLocative();
						} else {
							$titleText = MyTools::mb_ucfirst($templateRules["%namePlural%"])
								." ".City::getSelectedCity()->genitive
								.": запись онлайн на прием к ".((mb_stripos($nameDative, "врач") === false)?"врачу-":"").$nameDative
								." через Единый Медицинский Портал";
						}
						$tagDescription = "На Едином Медицинском Портале вы можете записаться к ".((mb_stripos($nameDative, "врач") === false)?"врачу-":"").$nameDative." в ".City::getSelectedCity()->locative." в 1 клик не выходя из дома. Только настоящие отзывы";
						$title = " в ".City::getSelectedCity()->locative;
						//$titleH1 = MyTools::mb_ucfirst($templateRules["%namePlural%"]).$title;
						$tagH1 = "Найдено ".$dataProvider->totalItemCount." ".$doctorCountableForm." для записи онлайн";
					} else {
						$titleText = $tagH1;
					}

					$criteria = new CDbCriteria();
					$criteria->with = [
						'nearestMetroStations' => [
							'together' => true,
							'with' => [
								'together' => true,
								'address' => [
									'together' => true,
									'with' => [
										'together' => true,
										'placeOfWorks' => [
											'together' => true,
											'with' => [
												'together' => true,
												'doctor' => [
													'together' => true,
													'with' => [
														'together' => true,
														'specialtyOfDoctors' => [
															'together' => true
														],
													],
												],
											],
										],
										'userMedicals' => [
											'together' => true,
											'select' => false,
										],
									],
								],
							],
						],
					];
					$criteria->select = "t.name, t.linkUrl";
					$criteria->distinct = true;
					$criteria->params = [
							':doctorSpecialtyId' => $spModel->attributes['id'],
							':cityId' => $searchModel->cityId,
							':samozapis' => (Yii::app()->params["samozapis"] ? 1 : 0),
					];
					$criteria->addCondition([
						"specialtyOfDoctors.doctorSpecialtyId = :doctorSpecialtyId",
						"address.cityId = :cityId",
						"address.samozapis = :samozapis",
						"userMedicals.agreementNew = 1",
					]);

					if(isset($searchModel['metroStationId']) and count($searchModel['metroStationId']) > 0 and is_array($searchModel['metroStationId'])){
						$criteria->addCondition([
							"t.linkUrl NOT IN ("."'".implode("','", $searchModel['metroStationId'])."')",
						]);
					}
					$criteria->limit = 3;
					$doctorsMetro = MetroStation::model()->findAll($criteria);

					/* Новый механизм определения ближайших районов
					$criteria = new CDbCriteria();
					$criteria->with = [
						'select'=>false,
						'together' => true,
						'nearbyDistrict' => [
							'together' => true,
							'with' => [
								'together' => true,
								'cityDistrict' => [
									'together' => true,
									'with' => [
										'addresses' => [
											'select'=>false,
											'together' => true,
											'with' => [
												'together' => true,
												'placeOfWorks' => [
													'select'=>false,
													'together' => true,
													'with' => [
														'together' => true,
														'doctor' => [
															'select'=>false,
															'together' => true,
															'with' => [
																'together' => true,
																'specialtyOfDoctors' => [
																	'select'=>false,
																	'together' => true
																],
															],
														],
														'company' =>[
															'select'=>false,
															'together' => true,
															'with' => [
																'select'=>false,
																'together' => true,
																'userMedicals' => [
																	'select'=>false,
																	'together' => true
																],
															],
														],
													],
												],
											],
										],
									],
								],
							],
						],
					];
					$criteria->select = "`cityDistrict`.`name` as name";
					$criteria->distinct = true;
					$criteria->params = [':doctorSpecialtyId' => $spModel->attributes['id'], ':cityId' => '534bd8b8-e0d4-11e1-89b3-e840f2aca94f'];
					$criteria->addCondition([
						"specialtyOfDoctors.doctorSpecialtyId = :doctorSpecialtyId",
						"addresses.cityId = :cityId",
						"userMedicals.agreementNew = 1",
						"addresses.samozapis = :samozapis",
					]);
					if(isset($searchModel['metroStationId']) and count($searchModel['metroStationId']) > 0 and is_array($searchModel['metroStationId'])){
						$criteria->addCondition([
							"t.linkUrl IN ("."'".implode("','", $searchModel['metroStationId'])."')",
						]);
					}
					$criteria->limit = 4;
					$doctorsRegion = CityDistrict::model()->findAll($criteria)[0]->nearbyDistrict;
					*/

					$criteria = new CDbCriteria();
					$criteria->select = "t.name, t.linkUrl";
					$criteria->with = [
							'together' => true,
							'addresses' => [
									'together' => true,
									'with' => [
											'together' => true,
											'placeOfWorks' => [
													'together' => true,
													'with' => [
															'together' => true,
															'doctor' => [
																	'together' => true,
																	'with' => [
																			'together' => true,
																			'specialtyOfDoctors' => [
																					'together' => true
																			],
																	],
															],
													],
											],
											'userMedicals' => [
													'together' => true,
													'select' => false,
											],
									],
							],
					];
					
					$criteria->params = [
							':doctorSpecialtyId' => $spModel->attributes['id'],
							':cityId' => $searchModel->cityId,
							':samozapis' => (Yii::app()->params["samozapis"] ? 1 : 0),
					];
					$criteria->addCondition([
							"specialtyOfDoctors.doctorSpecialtyId = :doctorSpecialtyId",
							"addresses.cityId = :cityId",
							"userMedicals.agreementNew = 1",
							"addresses.samozapis = :samozapis",
					]);
					if(isset($searchModel['metroStationId']) and count($searchModel['metroStationId']) > 0 and is_array($searchModel['metroStationId'])){
						$criteria->addCondition([
								"t.linkUrl NOT IN ("."'".implode("','", $searchModel['metroStationId'])."')",
						]);
					}
					$criteria->limit = 4;
					$doctorsRegion = CityDistrict::model()->findAll($criteria);


					if(isset($spModel->attributes['id'])){
						$criteria = new CDbCriteria();
						$criteria->with = array(
							'doctorSpecialtyDiseases' => array(
								'together' => true,
							)
						);
						$criteria->params = array(':doctorSpecialtyId' => $spModel->attributes['id']);
						$criteria->addCondition(array(
							"doctorSpecialtyDiseases.doctorSpecialtyId = :doctorSpecialtyId",
						));

						$criteria->limit = 9;
						$disease = DiseaseGroup::model()->findAll($criteria);
					}
					$specInfo = (is_object($spModel)) ? $spModel->attributes['description'] : null;


					$dataProvider->totalItemCount;
					$tagH2 = '';
					if(Yii::app()->request->getParam('typePageResult') == 1){
						$tagH2 = 'В клинике '.$dataProvider->totalItemCount.' '.$doctorCountableForm;
					}



					if(count($getParamSearchMetroStationId) > 1 ){
						$specInfo = '';
					}


					$this->render('doctor', array(
							'model' => $model,
							'doctors' => $doctors,
							'dataProvider' => $dataProvider,
							'show' => $show,
							'sexTypesList' => $sexTypesList,
							'companyList' => ShortCompaniesOfDoctor::getCompanyList(),
							'emptyText' => $emptyText,
                            'notificationText' => $notificationText,
							'isWidget' => $isWidget,
							'spModel' => $spModel,
							'titleText' => $titleText,
							'tagDescription' => $tagDescription,
							'keywords' => $keywords,
							'tagH1' => $tagH1,
							'titleH1' => $titleH1,
							'district_metro' => $district_metro,
							'searchModel' => $searchModel,
							'templateRules' => $templateRules,
							'disease' => $disease,
							'dataSpecList' => $dataSpecList,
							'title_all_spec' => "Все специальности".$title,
							'specInfo' => $specInfo,
							'doctorsMetro'	=> $doctorsMetro,
							'doctorsRegion'	=> $doctorsRegion,
							'tagH2'			=> $tagH2,
							'isVkApp'       => $isVkApp,
							'vkGroupId'     => $vkGroupId,
							'vkViewerType'  => $vkViewerType,
							'vkAppModel'    => $vkAppModel,
							'vkClinic'      => $vkClinic,
							'doctorSpecialties' => $doctorSpecialties,	
					));
				}
			}
		} else {
			//var_dump($searchModel->errors);
			$this->render('doctor', array(
				'searchModel' => $searchModel,
				'dataProvider' => null,
			));
		}
	}

	public function actionDisease() {
		
		$subdomain = explode('.', $_SERVER['HTTP_HOST'])[0];
		if (!in_array($subdomain, ['local', 'dev1', '', 'emportal']))
		{
			$fullHost = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
			$fullUrl = $fullHost . $_SERVER['REQUEST_URI'];
			$redirectUrl = str_replace($fullHost, 'https://emportal.ru', $fullUrl);
			$this->redirect($redirectUrl, true, '301');
		}
		
		$this->searchbox_type = SearchBox::S_TYPE_PATIENT;
		$this->searchbox_tab = Search::S_DOCTOR;
		$this->seotype = "disease";
		$model = new DiseaseGroup('search');
		$search = new Search(Search::S_HANDBOOK);
		$this->searchModel = &$searchModel;
		$search->attributes = Yii::app()->request->getParam('Search', array());
		$pageSize = 90;

		$regexp = Search::regexpForQuery($search->text);
		$criteria = new CDbCriteria;
		$criteria->order = '`t`.`name` ASC';

		if ($regexp) {
			$criteria->params = array(':search' => $regexp);
			$criteria->addCondition(array(
				"LOWER(`t`.`name`) REGEXP :search",
					), 'OR');
		}
		
		
		
		if ($search->chars && $search->letters) {
			$pageSize = 300;
			$temp = array();
			foreach ($search->letters as $letter)
				$temp[] = " `t`.`name` LIKE '{$letter}%'";
			$criteria->addCondition($temp, 'OR');
		} else {
			//$this->redirect(array('search/disease', 'chars' => 'value1'))
		}

		$dataProvider = new CActiveDataProvider($model, array(
			'criteria' => $criteria,
			'sort' => array(),
			'pagination' => array(
				'pageSize' => $pageSize,
				'pageVar' => 'Disease_page'
			),
		));

		if ($json = $this->convertToJSON($dataProvider)) {
			echo $json;
		} else {
			//$this->searchbox_type = SearchBox::S_TYPE_PATIENT_NEW;
			$this->searchbox_type = SearchBox::S_TYPE_PATIENT;
			$this->searchbox_tab = Search::S_DOCTOR;
			$this->layout = '//layouts/column1_for_old_pages';
			
			Yii::app()->clientScript->registerMetaTag('Список некоторых болезней, известных человечеству', 'description');
			
			$this->render('disease', array(
				'model' => $model,
				'dataProvider' => $dataProvider,
				'selectedChar' => $search->letters[0],
			));
		}
	}

	public function actionMedicament() {
		
		throw new CHttpException(404, 'The requested page does not exist.');
		
		$this->searchbox_type = SearchBox::S_TYPE_HANDBOOK;
		$this->searchbox_tab = Search::S_MEDICAMENT;

		$model = new Brand('search');

		$search = new Search(Search::S_HANDBOOK);
		$this->searchModel = &$searchModel;

		$search->attributes = Yii::app()->request->getParam('Search', array());

		$regexp = Search::regexpForQuery($search->text);
		$criteria = new CDbCriteria;
		$criteria->order = '`t`.`name` ASC';

		if ($regexp) {
			$criteria->params = array(':search' => $regexp);
			$criteria->addCondition(array(
				"LOWER(`t`.`name`) REGEXP :search",
					), 'OR');
		}

		if ($search->chars && $search->letters) {
			$temp = array();
			foreach ($search->letters as $letter)
				$temp[] = " `t`.`name` LIKE '{$letter}%'";
			$criteria->addCondition($temp, 'OR');
		}

		$dataProvider = new CActiveDataProvider($model, array(
			'criteria' => $criteria,
			'sort' => array(),
			'pagination' => array(
				'pageSize' => 30,
			),
		));

		if ($json = $this->convertToJSON($dataProvider)) {
			echo $json;
		} else
			$this->render('medicament', array(
				'model' => $model,
				'dataProvider' => $dataProvider
			));
	}

	public function actionDoctorSpecialty() {

		$this->searchbox_type = SearchBox::S_TYPE_HANDBOOK;
		$this->searchbox_tab = Search::S_DOCTORSPECIALTY;

		$model = new DoctorSpecialty('search');

		$search = new Search(Search::S_HANDBOOK);
		$this->searchModel = &$searchModel;
	
		$searchParams = Yii::app()->request->getParam('Search', array());
		$search->attributes = $searchParams;

		$regexp = Search::regexpForQuery($search->text);
		$criteria = new CDbCriteria;
		$criteria->select = '
				`t`.`id`,
				`t`.`name`,
				`t`.`link`,
				`t`.`description`,
				`t`.`isArchive`,
				`t`.`companyActiviteId`,
				`t`.`dative`,
				`t`.`genitive`,
				`t`.`shortSpecialtyId`,
				`t`.`linkUrl`,
				`t`.`noindex`,
				`t`.`tagH1`,
				`t`.`tagDescription`,
				`t`.`tagKeywords`,
				`t`.`seoText`
				';
		$criteria->order = '`t`.`name` ASC';

		if ($regexp) {
			$criteria->params = array(':search' => $regexp);
			$criteria->addCondition(array(
				"LOWER(`t`.`name`) REGEXP :search",
					), 'OR');
		}

		if ($search->chars && $search->letters) {
			$temp = array();
			foreach ($search->letters as $letter)
				$temp[] = " `t`.`name` LIKE '{$letter}%'";
			$criteria->addCondition($temp, 'OR');
		}
		$criteria->compare("noindex", $searchParams["noindex"]);

		$dataProvider = new CActiveDataProvider($model, array(
			'criteria' => $criteria,
			'sort' => array(),
			'pagination' => array(
				'pageSize' => 30,
			),
		));

		if ($json = $this->convertToJSON($dataProvider)) {
			echo $json;
		} else
			$this->render('doctorSpecialty', array(
				'model' => $model,
				'dataProvider' => $dataProvider
			));
	}

	public function actionCompanyActivity() {
		$this->searchbox_type = SearchBox::S_TYPE_HANDBOOK;
		$this->searchbox_tab = Search::S_COMPANYACTIVITY;

		$model = new CompanyActivite('search');

		$search = new Search(Search::S_HANDBOOK);
		$this->searchModel = &$searchModel;
		$city = $search->getCityName();

		$search->attributes = Yii::app()->request->getParam('Search', array());

		$regexp = Search::regexpForQuery($search->text);
		$criteria = new CDbCriteria;
		$criteria->order = '`t`.`name` ASC';

		if ($regexp) {
			$criteria->params = array(':search' => $regexp);
			$criteria->addCondition(array(
				"LOWER(`t`.`name`) REGEXP :search",
					), 'OR');
		}

		if ($search->chars && $search->letters) {
			$temp = array();
			foreach ($search->letters as $letter)
				$temp[] = " `t`.`name` LIKE '{$letter}%'";
			$criteria->addCondition($temp, 'OR');
		}

		$dataProvider = new CActiveDataProvider($model, array(
			'criteria' => $criteria,
			'sort' => array(),
			'pagination' => array(
				'pageSize' => 30,
			),
		));

		if ($json = $this->convertToJSON($dataProvider)) {
			echo $json;
		} else
			throw new CHttpException(404, 'The requested page does not exist.');
			//$this->render('companyActivites', array(
			//	'city' => $city,
			//	'model' => $model,
			//	'dataProvider' => $dataProvider
			//));
	}

	public function actionProducer() {

		$this->searchbox_type = SearchBox::S_TYPE_PRODUCER;
		$this->searchbox_tab = Search::S_PRODUCER;

		$searchModel = new Search(Search::S_PRODUCER);
		$this->searchModel = &$searchModel;

		$searchModel->attributes = Yii::app()->request->getParam(get_class($searchModel));

		if ($searchModel->validate()) {

			$model = new Producers('search');

			$model->unsetAttributes();  // clear any default values

			$criteria = new CDbCriteria;

			$criteria->group = '';

			$criteria->with = array();
			$criteria->compare('metroStations.id', $searchModel->metroStationId);

			if (trim($searchModel->text)) {
				$regex = Search::regexpForQuery($searchModel->text);
				$regexCriteria = new CDbCriteria;
				$regexCriteria->params = array(':search' => $regex);
				$regexCriteria->addCondition(array(
					"LOWER(t.Producers) REGEXP :search",
						), 'OR');
					//$model->currentPlaceOfWork->CompaniesName

				$criteria->mergeWith($regexCriteria);
				echo $regex;
			}

			$dataProvider = new CActiveDataProvider($model, array(
				'criteria' => $criteria,
			));

			if ($json = $this->convertToJSON($dataProvider)) {
				echo $json;
			} else
				$this->render('producer', array(
					'model' => $model,
					'dataProvider' => $dataProvider
				));
		}
	}

	public function actionIndex() {
		$this->redirect('/search/clinic');
		#$this->render('index');
	}

	public function actionInsurance() {

		$this->searchbox_type = SearchBox::S_TYPE_INSURANCE;
		$this->searchbox_tab = Search::S_INSURANCE;

		$searchModel = new Search(Search::S_INSURANCE);
		$this->searchModel = &$searchModel;

		If (!$searchParam = Yii::app()->request->getParam('Search')) {
			$searchParam = Yii::app()->user->getState('LastSearchParam');
		}

		$searchModel->attributes = $searchParam;

		if ($searchModel->validate()) {

			$model = new Company('search');

			$model->unsetAttributes();  // clear any default values

			$criteria = new CDbCriteria;

			$criteria->group = '`t`.`id`';



			$criteria->with = array(
				'addresses' => array(
					'together' => true,
					'with' => array(
						'cityDistrict' => array('together' => true),
						'metroStations' => array('together' => true)
					),
				),
				//'comments',
				'companyType',
			);
//$criteria->compare('categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87');

			if ($searchModel->cityId) {
				$criteria->compare('addresses.cityId', $searchModel->cityId);
			}
			$criteria->compare('metroStations.id', $searchModel->metroStationId);
			$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
			$criteria->compare('companyType.id', $searchModel->companyTypeId);

			if ($searchModel->IsOMS) {
				$criteria->compare('IsOMS', 1);
			}
			if ($searchModel->IsDMS) {
				$criteria->compare('IsDMS', 1);
			}


			$criteria->order = '`t`.`name`';

			if (trim($searchModel->text)) {
				$regex = Search::regexpForQuery($searchModel->text);
				$regexCriteria = new CDbCriteria;
				$regexCriteria->params = array(':search' => $regex);
				$regexCriteria->addCondition(array(
					"LOWER(`t`.`name`) REGEXP :search",
						//"LOWER(`t`.`nameENG`) REGEXP :search",
//"LOWER(`t`.`nameRUS`) REGEXP :search",
						), 'OR');
//$model->currentPlaceOfWork->CompaniesName

				$criteria->mergeWith($regexCriteria);
			}
//var_dump($criteria->order);


			$dataProvider = new CActiveDataProvider($model->insurance(), array(
				'criteria' => $criteria,
				'sort' => array(),
			));

			$clinics = array();
			foreach ($dataProvider->data as &$row) {

				/* @var $row Clinic */
				$clinics[$row->id] = $row;
				$row->addresses = array();
			}

			if ($searchModel->metroStationId || $searchModel->cityDistrictId || 1) {

				$criteria = new CDbCriteria();

				$criteria->with = array(
					'cityDistrict' => array(
						'together' => true,
					),
					'metroStations' => array(
						'together' => true,
					),
				);
				$criteria->compare('metroStations.id', $searchModel->metroStationId);
				$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
				$criteria->compare('`t`.`cityId`', $searchModel->cityId);
				$criteria->addInCondition('ownerId', array_keys($clinics));

				$addresses = Address::model()->findAll($criteria);

				$this->addr_array = array();

				foreach ($addresses as &$address) {
					$this->addr_array[$address->ownerId][] = $address;
				}

				/*
				 * 				 foreach ($addresses as &$address) {
				  echo $address->name."<br>";
				  $temp = &$clinics[$address->ownerId]->addresses;
				  array_push($temp, $address);
				  }
				 */
			}

			if ($json = $this->convertToJSON($dataProvider)) {
				echo $json;
			} else
				$this->render('insurance', array(
					'model' => $model,
					'addr' => $this->addr_array,
					'dataProvider' => $dataProvider
				));
		}
		else {
			$this->render('insurance', array(
				'searchModel' => $searchModel,
				'dataProvider' => null,
			));
		}
	}

	public function actionTourism() {

		$this->searchbox_type = SearchBox::S_TYPE_TOURISM;
		$this->searchbox_tab = Search::S_TOURISM;

		$searchModel = new Search(Search::S_TOURISM);
		$this->searchModel = &$searchModel;

		If (!$searchParam = Yii::app()->request->getParam('Search')) {
			$searchParam = Yii::app()->user->getState('LastSearchParam');
		}

		$searchModel->attributes = $searchParam;

		if ($searchModel->validate()) {

			$model = new Company('search');

			$model->unsetAttributes();  // clear any default values

			$criteria = new CDbCriteria;

			$criteria->group = '`t`.`id`';



			$criteria->with = array(
				'address' => array(),
				'addresses' => array(
					'together' => true,
					'with' => array(
						'cityDistrict' => array('together' => true),
						'metroStations' => array('together' => true)
					),
				),
				//'comments',
				'companyType',
			);
//$criteria->compare('categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87');

			if ($searchModel->cityId) {
				$criteria->compare('addresses.cityId', $searchModel->cityId);
			}
			$criteria->compare('metroStations.id', $searchModel->metroStationId);
			$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
			$criteria->compare('companyType.id', $searchModel->companyTypeId);

			if ($searchModel->IsOMS) {
				$criteria->compare('IsOMS', 1);
			}
			if ($searchModel->IsDMS) {
				$criteria->compare('IsDMS', 1);
			}


			$criteria->order = '`t`.`name`';

			if (trim($searchModel->text)) {
				$regex = Search::regexpForQuery($searchModel->text);
				$regexCriteria = new CDbCriteria;
				$regexCriteria->params = array(':search' => $regex);
				$regexCriteria->addCondition(array(
					"LOWER(`t`.`name`) REGEXP :search",
						//"LOWER(`t`.`nameENG`) REGEXP :search",
//"LOWER(`t`.`nameRUS`) REGEXP :search",
						), 'OR');
//$model->currentPlaceOfWork->CompaniesName

				$criteria->mergeWith($regexCriteria);
			}
//var_dump($criteria->order);


			$dataProvider = new CActiveDataProvider($model->tourism(), array(
				'criteria' => $criteria,
				'sort' => array(),
			));


			$clinics = array();
			foreach ($dataProvider->data as &$row) {
				/* @var $row Clinic */
				$clinics[$row->id] = $row;
				$row->addresses = array();
			}

			if ($searchModel->metroStationId || $searchModel->cityDistrictId || 1) {

				$criteria = new CDbCriteria();

				$criteria->with = array(
					'cityDistrict' => array(
						'together' => true,
					),
					'metroStations' => array(
						'together' => true,
					),
				);
				$criteria->compare('metroStations.id', $searchModel->metroStationId);
				$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
				$criteria->compare('`t`.`cityId`', $searchModel->cityId);
				$criteria->addInCondition('ownerId', array_keys($clinics));

				$addresses = Address::model()->findAll($criteria);


				$this->addr_array = array();

				foreach ($addresses as &$address) {
					$this->addr_array[$address->ownerId][] = $address;
				}

				/* foreach ($addresses as &$address) {
				  $temp = &$clinics[$address->ownerId]->addresses;
				  array_push($temp, $address);
				  } */
			}

			if ($json = $this->convertToJSON($dataProvider)) {
				echo $json;
			} else
				$this->render('tourism', array(
					'model' => $model,
					'addr' => $this->addr_array,
					'dataProvider' => $dataProvider
				));
		}
		else {
			$this->render('tourism', array(
				'searchModel' => $searchModel,
				'dataProvider' => null,
			));
		}
	}

	public function actionBeauty() {

		$this->searchbox_type = SearchBox::S_TYPE_BEAUTY;
		$this->searchbox_tab = Search::S_BEAUTY;

		$searchModel = new Search(Search::S_BEAUTY);
		$this->searchModel = &$searchModel;

		If (!$searchParam = Yii::app()->request->getParam('Search')) {
			$searchParam = Yii::app()->user->getState('LastSearchParam');
		}

		$searchModel->attributes = $searchParam;

		if ($searchModel->validate()) {

			$model = new Company('search');

			$model->unsetAttributes();  // clear any default values

			$criteria = new CDbCriteria;

			$criteria->group = '`t`.`id`';



			$criteria->with = array(
				'address' => array(),
				'addresses' => array(
					'together' => true,
					'with' => array(
						'cityDistrict' => array('together' => true),
						'metroStations' => array('together' => true)
					),
				),
				//'comments',
				'companyType',
			);
//$criteria->compare('categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87');

			if ($searchModel->cityId)
				$criteria->compare('addresses.cityId', $searchModel->cityId);
			$criteria->compare('metroStations.id', $searchModel->metroStationId);
			$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
			$criteria->compare('companyType.id', $searchModel->companyTypeId);

			if ($searchModel->IsOMS) {
				$criteria->compare('IsOMS', 1);
			}
			if ($searchModel->IsDMS) {
				$criteria->compare('IsDMS', 1);
			}


			$criteria->order = '`t`.`name`';

			if (trim($searchModel->text)) {
				$regex = Search::regexpForQuery($searchModel->text);
				$regexCriteria = new CDbCriteria;
				$regexCriteria->params = array(':search' => $regex);
				$regexCriteria->addCondition(array(
					"LOWER(`t`.`name`) REGEXP :search",
						//"LOWER(`t`.`nameENG`) REGEXP :search",
//"LOWER(`t`.`nameRUS`) REGEXP :search",
						), 'OR');
//$model->currentPlaceOfWork->CompaniesName

				$criteria->mergeWith($regexCriteria);
			}
//var_dump($criteria->order);


			$dataProvider = new CActiveDataProvider($model->beauty(), array(
				'criteria' => $criteria,
				'sort' => array(),
			));


			$clinics = array();
			foreach ($dataProvider->data as &$row) {
				/* @var $row Clinic */
				$clinics[$row->id] = $row;
				$row->addresses = array();
			}

			if ($searchModel->metroStationId || $searchModel->cityDistrictId || 1) {

				$criteria = new CDbCriteria();

				$criteria->with = array(
					'cityDistrict' => array(
						'together' => true,
					),
					'metroStations' => array(
						'together' => true,
					),
				);
				$criteria->compare('metroStations.id', $searchModel->metroStationId);
				$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
				$criteria->compare('`t`.`cityId`', $searchModel->cityId);
				$criteria->addInCondition('ownerId', array_keys($clinics));

				$addresses = Address::model()->findAll($criteria);


				$this->addr_array = array();

				foreach ($addresses as &$address) {
					$this->addr_array[$address->ownerId][] = $address;
				}

				/* foreach ($addresses as &$address) {
				  $temp = &$clinics[$address->ownerId]->addresses;
				  array_push($temp, $address);
				  } */
			}
			if ($json = $this->convertToJSON($dataProvider)) {
				echo $json;
			} else
				$this->render('beauty', array(
					'model' => $model,
					'addr' => $this->addr_array,
					'dataProvider' => $dataProvider
				));
		}
		else {
			$this->render('beauty', array(
				'searchModel' => $searchModel,
				'dataProvider' => null,
			));
		}
	}

	public function actionMetro() {
		$output = array();
		$oMetroStation = new MetroStation();

		$criteria = new CDbCriteria;
		$criteria->condition = 'cityId=:cityId';
		$criteria->params = array(':cityId' => '534bd8b8-e0d4-11e1-89b3-e840f2aca94f');
//$criteria->limit = 1;
		$oMetroStation = MetroStation::model()->findAll($criteria);
		foreach ($oMetroStation as $MetroStation) {
			if ($MetroStation->MetroStationLongitude > 0 && $MetroStation->MetroStationLatitude > 0) {
				$output[] = array('name' => $MetroStation->MetroStationName, 'longitude' => substr($MetroStation->MetroStationLongitude, 0, 2) . '.' . substr($MetroStation->MetroStationLongitude, 2), 'latitude' => substr($MetroStation->MetroStationLatitude, 0, 2) . '.' . substr($MetroStation->MetroStationLatitude, 2));
			}
		}

//print_r($output);

		echo CJSON::encode($output);
		Yii::app()->end();
	}

// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */


	public function actionMap($metro = null, $district = null, $profile = null) {

		$this->searchbox_type = SearchBox::S_TYPE_PATIENT;
		$this->searchbox_tab = Search::S_CLINIC;

		$searchModel = new Search(Search::S_CLINIC);
		$this->searchModel = &$searchModel;

		if (!$searchParam = Yii::app()->request->getParam('Search')) {
			$searchParam = Yii::app()->user->getState('LastSearchParam');
		}
		
		if (!empty($metro)) {
			$searchModel->metroStationId = $metro;
		}
		if (!empty($district)) {
			$searchModel->metroStationId = $district;
		}
		if (!empty($profile)) {
			$searchModel->companyActiviteId = $profile;
		}
		
		$searchModel->metroStationId = str_replace([
			'metro-',
			'raion-'
				], '', $searchModel->metroStationId);
		$searchModel->companyActiviteId = str_replace([
			'profile-',
				], '', $searchModel->companyActiviteId);
		
		$searchModel->attributes = $searchParam;
		
		if ($searchModel->validate()) {
			$model = new Company('search');
			$model->unsetAttributes();  // clear any default values

			$criteria = new CDbCriteria;
			$criteria->group = '`t`.`id`';

			$criteria->with = array(
				'companyContracts' => [
					'together' => true,
					'joinType' => 'INNER JOIN',
					'select' => '(`companyContracts`.`id` IS NOT NULL) AS Contract',
				],
				'addresses' => array(
					'together' => true,
					'joinType' => 'INNER JOIN',
					'with' => array(
						'userMedicals' => [
							'together' => true,
							'joinType' => 'INNER JOIN',
						],
						//'cityDistrict'	=> array('together' => true),
						//'addressActivites'	=> array('together' => true),
						//'addressServices'	=> array('together' => true),						
						//'metroStations'	=> array('together' => true)
						),
					),
				//'comments',
				'companyType',
			);

			//var_dump($criteria->with);
			//$criteria->compare('categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87');
			//$criteria->compare('addresses.id','0021393d-8eca-11e2-856f-e840f2aca94f');
			$criteria->compare('userMedicals.agreementNew',1);
			$criteria->compare('addresses.isActive',1);
			$criteria->compare('t.removalFlag',0);
			if ($searchModel->cityId)
				$criteria->compare('addresses.cityId', $searchModel->cityId);

			if($searchModel->cityDistrictId OR $searchModel->metroStationId) {
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'cityDistrict' => array('together' => true),
				));
			}

			if ($searchModel->metroStationId) {
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'metroStations' => array('together' => true),
				));

				if ($searchModel->expandSearchArea == 1) {
					$metro = MetroStation::model()->find("id = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
					$criteria->addBetweenCondition('metroStations.longitude', $metro->longitude - $this->GEOinterval, $metro->longitude + $this->GEOinterval);
					$criteria->addBetweenCondition('metroStations.latitude', $metro->latitude - $this->GEOinterval, $metro->latitude + $this->GEOinterval);
				} else {					
					if (is_array($searchModel->metroStationId)) {
						$tempMetroStation = [];
						foreach ($searchModel->metroStationId as $metroStationId) { $tempMetroStation[] = Yii::app()->db->quoteValue($metroStationId); }
						$queryStringMetroStation = implode(",", $tempMetroStation);
						$criteria->addCondition("metroStations.id in (" . $queryStringMetroStation . ")"
								. " OR metroStations.linkUrl in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.id in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.linkUrl in (" . $queryStringMetroStation . ")");
					} else {
						$criteria->addCondition("metroStations.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR metroStations.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
					}					
				}
			}


			if ($searchModel->cityDistrictId) {
				$criteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
			}

			$criteria->compare('companyType.isMedical', 1);
			if ($searchModel->companyTypeId)
				$criteria->compare('companyType.id', $searchModel->companyTypeId);


			if ($searchModel->companyActiviteId) {
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'addressServices' => array('together' => true),
				));
				$criteria->addCondition("addressServices.companyActivitesId = '".$searchModel->companyActiviteId."' "
						. "OR addressServices.companyActivitesId = '" . CompanyActivite::model()->findByLink($searchModel->companyActiviteId)->id . "'");
				#$criteria->compare('addressServices.companyActivitesId', $searchModel->companyActiviteId);
			}

			if ($searchModel->IsOMS) {
				$criteria->compare('IsOMS', 1);
			}
			if ($searchModel->IsDMS) {
				$criteria->compare('IsDMS', 1);
			}


			if ($searchModel->isGMU) {
				$criteria->addInCondition('companyType.id', Address::$arrGMU);
			} else {
				$criteria->addNotInCondition('companyType.id', Address::$arrGMU);
			}

			if ($searchModel->IsHospital) {
				$criteria->with['addresses']['with'] = array_merge($criteria->with['addresses']['with'], array(
					'attributeAddress' => array(
						'together' => true,
					),
				));
				$vAttribute = ValueAttribute::model()->isHospital()->find();

				$criteria->compare('attributeAddress.valueAttributeId', $vAttribute->id);
			}
			$criteria->order = '`t`.`rating` DESC';

			if (trim($searchModel->text)) {
				$regex = Search::regexpForQuery($searchModel->text);
				$regexCriteria = new CDbCriteria;
				$regexCriteria->params = array(':search' => $regex);
				$regexCriteria->addCondition(array(
					"LOWER(`t`.`name`) REGEXP :search",
						//"LOWER(`t`.`nameENG`) REGEXP :search",
//"LOWER(`t`.`nameRUS`) REGEXP :search",
						), 'OR');
//$model->currentPlaceOfWork->CompaniesName

				$criteria->mergeWith($regexCriteria);
			}

			$showAll = (int) Yii::app()->request->getParam('showAll');
			if (!$showAll) {
				$criteria->limit = 10;
				$page = (int) Yii::app()->request->getParam('page');
				if ($page) {
					$criteria->offset = ($page - 1) * 10;
				}
			}

			if ($searchModel->longitude && $searchModel->latitude) {
				$criteria->addBetweenCondition('addresses.longitude', $searchModel->longitude - $this->GEOinterval, $searchModel->longitude + $this->GEOinterval);
				$criteria->addBetweenCondition('addresses.latitude', $searchModel->latitude - $this->GEOinterval, $searchModel->latitude + $this->GEOinterval);
			}

			$criteria->compare("addresses.samozapis", (Yii::app()->params["samozapis"]) ? 1 : 0);
			
			$data = $model->clinic()->findAll($criteria);
			

			$c = 0;
			$json = array();
			$arr = array();
			foreach ($data as $row) {
				$arr[] = $row->id;
			}

//foreach($data as $row) {					
			$addrCriteria = new CDbCriteria();
			$addrCriteria->group = 't.name';
			$addrCriteria->with = array(
				'company' => array('together' => true),
				'userMedicals' => [
					'together' => true,
					'joinType' => 'INNER JOIN',
				],
			);
			$addrCriteria->compare('userMedicals.agreementNew',1);
			$addrCriteria->compare('t.isActive',1);
			$addrCriteria->compare('company.removalFlag',0);

			if ($searchModel->metroStationId) {
				$addrCriteria->with['metroStations'] = array('together' => true);
				$addrCriteria->with['cityDistrict'] = array('together' => true);
				
				if ($searchModel->expandSearchArea == 1) {
					$metro = MetroStation::model()->find("id = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
					$addrCriteria->addBetweenCondition('metroStations.longitude', $metro->longitude - $this->GEOinterval, $metro->longitude + $this->GEOinterval);
					$addrCriteria->addBetweenCondition('metroStations.latitude', $metro->latitude - $this->GEOinterval, $metro->latitude + $this->GEOinterval);
				} else {
					if (is_array($searchModel->metroStationId)) {
						$tempMetroStation = [];
						foreach ($searchModel->metroStationId as $metroStationId) { $tempMetroStation[] = Yii::app()->db->quoteValue($metroStationId); }
						$queryStringMetroStation = implode(",", $tempMetroStation);
						$addrCriteria->addCondition("metroStations.id in (" . $queryStringMetroStation . ")"
								. " OR metroStations.linkUrl in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.id in (" . $queryStringMetroStation . ")"
								. " OR cityDistrict.linkUrl in (" . $queryStringMetroStation . ")");
					} else {
						$addrCriteria->addCondition("metroStations.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR metroStations.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.id = " . Yii::app()->db->quoteValue($searchModel->metroStationId)
								. " OR cityDistrict.linkUrl = " . Yii::app()->db->quoteValue($searchModel->metroStationId));
					}
				}
			}
			if ($searchModel->cityDistrictId) {
				$addrCriteria->with['cityDistrict'] = array('together' => true);
				$addrCriteria->compare('cityDistrict.id', $searchModel->cityDistrictId);
			}

			if ($searchModel->longitude && $searchModel->latitude) {
				$addrCriteria->addBetweenCondition('t.longitude', $searchModel->longitude - $this->GEOinterval, $searchModel->longitude + $this->GEOinterval);
				$addrCriteria->addBetweenCondition('t.latitude', $searchModel->latitude - $this->GEOinterval, $searchModel->latitude + $this->GEOinterval);
			}

			$addrCriteria->compare('t.cityId', $searchModel->cityId);
			$addrCriteria->addInCondition('company.id', $arr);
			$addrData = Address::model()->findAll($addrCriteria);

			if (Yii::app()->request->isAjaxRequest) {
				foreach ($addrData as $adr) {
					$json[] = array(
						'id' => $adr->id,
						'name' => str_replace("'", "&#39;", $adr->company->name),
						'address' => str_replace("'", "&#39;", $adr->name),
						'lat' => $adr->latitude,
						'long' => $adr->longitude,
						'url' => $adr->company->linkUrl . '/' . $adr->linkUrl,
					);
					$arr[] = $adr->id;
				}
			} else {
				$redirectParams = ['search/clinic'];
				foreach ($searchModel->attributes as $name => $value) {
					if (!empty($value)) {
						$redirectParams['Search[' . $name . ']'] = $value;
					}
				}
				$this->redirect($redirectParams);
			}


//	}
			/*
			  $phone_criteria= new CDbCriteria();
			  $phone_criteria->addInCondition('addressId', $arr);
			  $phone = Phone::model()->findAll($phone_criteria);
			  $phone_arr=array();
			  foreach($phone as $ph) {
			  $phone_arr[$ph->addressId]=$ph->name;
			  }
			 */
			/* foreach($data as $row) {					
			  $json[] = array(
			  'id' => $row->addresses[0]->id,
			  'name' => str_replace("'", "&#39;", $row->name),
			  'address' => str_replace("'", "&#39;", $row->addresses[0]->name),
			  'lat' => $row->addresses[0]->latitude,
			  'long' => $row->addresses[0]->longitude,
			  'url' => $row->addresses[0]->link,
			  'phone' => $phone_arr[$row->addresses[0]->id]
			  );
			  } */

//City - Saint-Petersburg

			$coordObj = array(
					'lat' => '59.938',
					'long' => '30.313',
			);
			$cityModel = City::model()->findByAttributes([ 'id'=>City::model()->selectedCityId ]);
			if($cityModel) {
				if(!empty($cityModel->longitude) || !empty($cityModel->latitude)) {
					$coordObj['lat'] = $cityModel->latitude;
					$coordObj['long'] = $cityModel->longitude;
				}
			}
			$coord = CJSON::encode($coordObj);


			$json = CJSON::encode($json);
		}

		$this->renderPartial('map', array(
			'data' => $json,
			'search' => 1,
			'coord' => $coord
		));



//echo $json;
	}

}
