<?php

class WidgetController extends LeftMenuController {
	#public $layout = '//layouts/card_layout';

	public $searchbox_type = SearchBox::S_TYPE_PATIENT;
	public $searchbox_tab = Search::S_CLINIC;
	public $menu = array(
		"Описание" => 'view',
			/* "Специалисты" => 'experts',
			  "Стоимость услуг" => 'price',
			  "Отзывы" => 'comments',
			  "Фотогалерея" => 'gallery',
			  "Акции" => 'actions', */
	);
	public $_model;
	public $arrGMU = array(
		'e909b718-2b35-11e2-b014-e840f2aca94f',
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f',
		'c06a39cb-f532-11e2-856f-e840f2aca94f',
		'93b817de-ee70-11e2-856f-e840f2aca94f',
		'c06a39ca-f532-11e2-856f-e840f2aca94f',
		'22075224-e934-11e2-856f-e840f2aca94f'
	);
	public $CompanyTypesNotOnline = array(
		'c2148457-29d6-11e2-b014-e840f2aca94f', // Выездная медицинская помощь
		'd16b98ad-349a-11e2-b014-e840f2aca94f', // Санаторий
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f', // Диспансер
		'71d418b8-6072-11e2-b019-e840f2aca94f', // Дом престарелых, интернат
		'a950904d-86fc-11e2-856f-e840f2aca94f'  // Тур. фирмы и тур. операторы с оздоровительными турами
	);

	public function filters() {
		return array(
			'accessControl',
			array('application.filters.CheckExpirePhone + visit'),
			array('application.filters.CheckBlockedAppointment + visit'),
		);
	}

	public function accessRules() {
		return array(
			array('deny',
				'actions' => array('comment', 'favorite'),
				'users' => array('?'),
			)
		);
	}
	
	
	public function actionClearSession() {
		
		echo 'REFERRER WAS = '.Yii::app()->session['widgetReferrer'];
		Yii::app()->session['widgetReferrer'] = '';
	}
	

	public function actionView($link = null, $companyLink = null, $linkUrl = null) {
		
		//Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', CClientScript::POS_END);
		$this->layout = '//layouts/widget_layout';
		Address::$showInactive = true;
		
		#$add = Address::model()->
		if ($companyLink && $linkUrl) {			
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$id = $addr->id;
			$link = $addr->link;
			$mdl = $this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}
		
		$criteria = new CDbCriteria();
		$criteria->compare('t.id', $id);
		$criteria->order = 'companyActivite.name ASC, service.name ASC';
		$criteria->select = 'street,houseNumber,name,linkUrl,isActive,latitude,longitude,link,id,description,ratingSite,ratingUser,ratingSystem';
		
		$criteria->with = array(			
			'metroStations' => array(
				'select' => 'id,name,linkUrl',
				'together' => true
			),
			'_workingHours' => array(
				'together' => true
			),
			'emails' => array(
				'select' => 'id,name,link',
				'together' => true
			),
			'addressServices' => [
				'select' => 'id,price,free',
				'together' => true,
				'with' => [
					'companyActivite' => [
						'select' => 'id,name',
						'together' => true,
					],
					'service' => [
						'select' => 'id,name',
						'together' => true,
					]
				]
			],
			'phones' => array(
				'select' => 'id,name',
				'together' => true,
			),
			'cityDistrict' => array(
				'select' => 'id,name',
				'together' => true
			),
			'refs' => array(
				'select' => 'id,ownerId,refValue',
				'together' => true
			),
			'company' => array(
				'select' => 'id,name,rating,description,companyTypeId,linkUrl',
				'together' => true
			)
		);
		$model = Address::model()->find($criteria);

		$services = [];
		$prices = [];
		$minPrice = null;
		foreach ($model->addressServices as $as) {			
			if ($as->companyActivite->name) {
				if((empty($minPrice) || $minPrice > $as->price) && $as->price != 0 && !$as->free) {
					$minPrice = $as->price;
				} 
				if(empty($prices[$as->companyActivite->name])) {
					$prices[$as->companyActivite->name] = $as->price;
				} else {
					if($as->price < $prices[$as->companyActivite->name]) {
						$prices[$as->companyActivite->name] = $as->price;
					}
				}
				$services[$as->companyActivite->name][] = [
					'name' => $as->service->name,
					'price' => $as->price,
					'free' => $as->free
				];
			}
		}

		$specialtyCriteria = new CDbCriteria();
		$specialtyCriteria->with = [
			'doctorSpecialties' => [
				'select' => false,
				'with' => [
					'select' => false,
					'specialtyOfDoctors' => [
						'select' => false,
						'with' => [
							'select' => false,
							'doctor' => [
								'select' => false,
								'with' => [
									'select' => false,
									'placeOfWorks'
								]
							]
						]
					]
				]
			]
		];
		$specialtyCriteria->order = 't.name ASC';
		$specialtyCriteria->compare('placeOfWorks.addressId', $model->id);
		$doctorSpecialties = CHtml::listData(ShortSpecialtyOfDoctor::model()->findAll($specialtyCriteria), 'id', 'name');

		$doctorCriteria = new CDbCriteria;
		$doctorCriteria->select = "name,linkUrl,link,sexId,rating,experience";
		$doctorCriteria->group = '`t`.`id`';
		$doctorCriteria->order = "t.rating DESC, t.name ASC";

		$doctorCriteria->with = array(
			'appointmentCount' => array(
				'together' => true
			),
			'placeOfWorks' => array(
				'together' => true,
				'select' => false,
				'with' => array(
					'address' => [
						'select' => "id,link,cityId",
						'together' => true,
						'scopes' => 'active',
						'with' => [
							'company' => [
								'together' => true,
								'select' => false,
								'with' => [
									'companyContracts' => [
										'joinType' => 'INNER JOIN',
										'select' => false,
										'together' => true
									]
								]
							]
						]
					]
				),
			),
		);

		$doctorCriteria->compare('address.link', $model->link);


		$dataProvider = new CActiveDataProvider('Doctor', array(
			'criteria' => $doctorCriteria,
			'pagination' => array(
				'pageSize' => isset($_REQUEST['photo']) ? 400 : 10,
				'pageVar' => 'page',
			),
		));
		foreach ($dataProvider->getData() as $row) {
			$doctorIds[] = $row->id;
		}

		$infoCriteria = new CDbCriteria();
		$infoCriteria->index = "id";
		$infoCriteria->select = 'id';
		$infoCriteria->addInCondition('t.id', $doctorIds);
		$infoCriteria->with = array(
			'placeOfWorks' => array(
				'together' => true,
				'select' => 'addressId',
				'with' => array(
					'company' => array(
						'together' => true,
						'select' => 'name,linkUrl',
					),
					'address' => array(
						'together' => true,
						'select' => 'name,street,houseNumber,link,cityId,linkUrl',
						'with' => array(
							'metroStations' => array(
								'together' => true,
								'select' => 'name',
							),
						)
					),
				),
			),
			'scientificDegrees' => array(
				'together' => true
			),
			'specialtyOfDoctors' => array(
				'together' => true,
				'with' => array(
					'doctorSpecialty' => array(
						'together' => true,
						'select' => 'name',
					),
					'doctorCategory' => array(
						'together' => true,
						'select' => 'name',
					)
				)
			),
			'doctorServices' => array(
				'select' => 'price',
				'together' => true,
				'with' => array(
					'cassifierService' => array(
						'select' => 'link',
						'together' => true,
					)
				)
			),
		);
		$infoCriteria->compare('address.link', $model->link);
		$doctors = Doctor::model()->findAll($infoCriteria);

		$searchModel = new Search();

		if($model->userMedicals->agreementNew != 1 || !$model->isActive) {				
			$caId = $model->companyActivites[0]->companyActivitesId;
			$metroId = $model->nearestMetroStation->id;								
			$metroLink = $model->nearestMetroStation->linkUrl;
			
			$criteria = new CDbCriteria();
			$criteria->limit = 4;
			$criteria->group = 't.id';
			$criteria->compare('addresses.cityId',$searchModel->cityId);
			#$criteria->compare('companyActivites.companyActivitesId',$caId);
			$criteria->compare('userMedicals.agreementNew',1);
			#$criteria->compare('metroStations.id',$metroId);
			$criteria->with = [
				'addresses' => [
					'together' => true,
					'with' => [
						'metroStations' => [
							'together' => true,
						],
						'companyActivites' => [
							'together' => true,
						],
						'userMedicals' => [
							'together' => true,
						]
					]
				]
			];			
			
			$company = Company::model()->findAll($criteria);
			$cIds = [];
			foreach($company as $c) {
				if(!in_array($c->id, $cIds,true)) {
					$cIds[] = $c->id;
				}
			}
			$criteria = new CDbCriteria();			
			$criteria->index = 'id';
			$criteria->order = 't.rating DESC';
			$criteria->addInCondition('t.id', $cIds);
			$criteria->compare('addresses.cityId',$searchModel->cityId);
			$criteria->compare('userMedicals.agreementNew',1);
			#$criteria->compare('companyActivites.companyActivitesId',$caId);
			#$criteria->compare('metroStations.id',$metroId);
			$criteria->with = [
				'addresses' => [
					'scopes' => 'active',
					'together' => true,
					'with' => [
						'metroStations' => [
							'together' => true,
						],
						'companyActivites' => [
							'together' => true,
						],
						'userMedicals' => [
							'together' => true,
						]
					]
				]
			];			
			
			$similar = Company::model()->findAll($criteria);				
			
		}
		
		//проверяем реферрера
		$http_referer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);		
		if ($http_referer == 'www.stom-firms.ru')  Yii::app()->session['widgetReferrer'] = 'stom-firms';
		
		$this->render('view', array(
			'model' => $model,
			'searchModel' => $searchModel,
			'doctorSpecialties' => $doctorSpecialties,
			'dataProvider' => $dataProvider,
			'doctors' => $doctors,
			'prices' => $prices,
			'metroLink' => $metroLink,
			'minPrice' => $minPrice,
			'services' => $services,
			'similar' => $similar,
			'show' => 1,
				//'email' => $email
		));
	}

	public function actionVisit($link = null, $companyLink = null, $linkUrl = null) {
		Yii::app()->clientScript->registerCoreScript('jquery.maskedinput');
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$clinic = $this->loadModel($link);
			if($clinic->userMedicals->agreementNew != 1 && !isset($_REQUEST['JSON'])) {				
				$this->redirect(['view','linkUrl' => $linkUrl,'companyLink' => $companyLink]);
			}
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
			$clinic = $mdl;
		}


		if (Yii::app()->request->getParam('goto_register')) {
			Yii::app()->session['returnUrl'] = $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('link' => $link));
			$this->redirect('/register');
		}
		if (Yii::app()->request->getParam('removeGuestPhone')) {
			if (Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll("phone=:phone AND (status=:stat1 or status=:stat2)", array(
					"phone" => Yii::app()->session['appointment_phone_set'],
					"stat1" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT,
				));
				Yii::app()->session['appointment_phone_set'] = "";
			}
			$this->redirect(array("/clinic/visit",
				'link' => $link
			));
		}


		//$debug=true;
		$showPicker = true;

		if (Yii::app()->user->isGuest) {
			//Если пользователь гость - выводим сообщение о возможности зарегистрироваться
			Yii::app()->user->setFlash('askAuth', 'Уважаемый гость, вы можете зарегистрироваться на нашем портале. После регистрации Вы получите доступ в личный кабинет, в котором Вы сможете: <ul>'
					. '<li>Отследить историю посещения клиник и врачей</li>'
					. '<li>Оставить отзыв о медицинском учреждении</li>'
					. '<li>Подписаться на рассылку о новых услугах и акциях клиник, Единого медицинского портала и др.</li>'
					. '</ul>'
					. '<div style="text-align:center"><span onclick="location.href=\'' . $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('goto_register' => 1, 'link' => $link)) . '\'" class="btn-green">Зарегистрироваться</span> <span id="closeAuthBlock" class="btn-red">Скрыть блок</span></div>');
		} else {
			//Если пользователь зарегистрирован - проверяем есть ли у него подтвержденный телефон для записи на 1 раз
			$pv = PhoneVerification::model()->find("status=:status AND userId = :userId", array(
				':status' => PhoneVerification::APPOINTMENT,
				':userId' => Yii::app()->user->model->id,
			));
		}

		$model = new AppointmentToDoctors('visit');


		$LKK = UserMedical::model()->findByAttributes([
			'companyId' => $clinic->company->id
		]);


		//Если у компании нет ЛКК - только услуга первоначальный выбор
		if (empty($LKK)) {

			$showPicker = false;

			#$model->scenario = 'visit_no_contract';
			//Если контракта нету. Уведомить штатного оператора о заявке
			/* $notifyOperator = true; */


			/* Для отладки */
			if (!empty($debug)) {
				$showPicker = true;
			}
		}

		$model->company = $clinic->company;
		$model->address = $clinic;
		$model->service = Service::model()->findByLink(Yii::app()->request->getParam('service'));
		$model->name = Yii::app()->user->model->name;
		$model->phone = Yii::app()->user->model->telefon;
		$model->email = Yii::app()->user->model->email;
		$model->visitType = AppointmentToDoctors::VISIT_TYPE_CLINIC;
		$model->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;

		if (Yii::app()->request->getParam(get_class($model))) {
			$model->attributes = Yii::app()->request->getParam(get_class($model)); //$_POST[get_class($model)];
			$lnk = Yii::app()->request->getParam(get_class($model))['address']['link'];
			if (!empty($lnk)) {
				$addr = $this->loadModel($lnk);
				if ($addr->company->id == $model->company->id) {
					$model->address = $addr;
				}
			}

			$model->phone = "+" . MyRegExp::phoneRegExp($model->phone);
			$model->createdDate = date('Y-m-d H:i:s');
			$model->service = Service::model()->findByLink($model->serviceId);
			$model->doctor = Doctor::model()->findByLink($model->doctorId);
			$model->userId = Yii::app()->user->id;
			$model->statusId = AppointmentToDoctors::SEND_TO_REGISTER;

			/* Если гость ввел номер который указан у пользователя, привязываем запись к аккаунту */
			/* Если не гость ввел номер при реге, сохраняем в аккаунт */
			if (Yii::app()->user->isGuest) {
				$userModel = User::model()->findByAttributes([
					'telefon' => $model->phone
				]);
				if ($userModel) {
					$model->userId = $userModel->id;
				}
			} else {
				if (empty(Yii::app()->user->model->telefon)) {
					$userModel = Yii::app()->user->model;
					$userModel->telefon = $model->phone;
					$userModel->save();
				}
			}

			if (!Yii::app()->user->isGuest && $model->phone != Yii::app()->user->model->telefon) {
				$phoneModel = PhoneVerification::model()->user()->readyOneTimePhone()->find();

				if ($phoneModel === NULL) {


					$phone = $model->phone;
					$smsgate = new SmsGate();
					$code = $smsgate->generateCode();
					$smsgate->setMessage('Код подтверждения: _code_.', array(
						'_code_' => $smsgate->code
					)); //SmsGate::VERIFICATION;				
					$smsgate->phone = $phone;
					$smsgate->send();

					$pvModel = new PhoneVerification();
					$pvModel->code = $code;
					$pvModel->phone = $phone;
					$pvModel->userId = $model->userId;
					$pvModel->status = PhoneVerification::READY_TO_APPOINTMENT;
					$pvModel->save();

					echo CJSON::encode(['success' => false, 'blockedappointment' => true]);
					Yii::app()->end();
				} else {
					
				}
			}

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'doctor-visit') {
				$arrData = CJSON::decode(CActiveForm::validate($model));
				if ($_POST[get_class($model)]['phone'] == "" && $_POST['phoneInput'] != "") {
					$arrData['AppointmentToDoctors_phone'][0] = 'Телефон не активирован';
					unset($arrData['AppointmentToDoctors_phone'][1]);
				}
				echo CJSON::encode($arrData);
				Yii::app()->end();
			}

			#var_dump($model->attributes);
			if ($model->save()) {

				/* Чистим таблицу PhoneVerification для временного номера */
				#if(!Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll('phone = :pId AND (status = :stat OR status = :stat2)', array(
					"pId" => $model->phone,
					"stat" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT
				));
				#}
				//Чистим сессию
				if (Yii::app()->user->isGuest) {
					Yii::app()->session['appointment_phone_set'] = "";
				}

				/*
				  #if (!empty($notifyOperator)) {

				  #} */

				#if($model->phone != '+79213434502') {
				/* Уведомить штатного оператора о том, что произведена заявка в клинику  */
				$model->notifyOperator();

				/* Оповещаем пользователя по смс и почте */
				$model->sendMailToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				if (Yii::app()->user->isGuest) {
					$model->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
				} else {
					$model->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				}
				/* Оповещаем клинику по смс и почте */
				$model->sendMailToClinic('new_record');
				$model->sendSmsToClinic('new_record');
				#}
				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(array('registrySuccess' => 'Запись успешно произведена'));
					Yii::app()->end();
				} else {
					Yii::app()->user->setFlash('registrySuccess', 'Запись успешно произведена');
					if (!Yii::app()->user->isGuest) {
						if ($notifyOperator) {
							$this->redirect(array('/user/registry', 'filter' => 'all'));
						} else {
							$this->redirect(array('/user/registry'));
						}
					} else {
						$pollCriteria = new CDbCriteria();
						$pollCriteria->scopes = 'afterVisit';
						#$pollCriteria->addInCondition('t.id', []);
						$polls = Poll::model()->afterVisit()->findAll($pollCriteria);
						$text = "							
						<p>Ваша заявка на прием успешно отправлена в медицинское учреждение!</p>
						
						Информация по записи:
						<ul style=\"margin-top:0px;\">
							<li>Учреждение: " . $model->company->name . "</li>
							<li>Время приема: " . $model->registryPlannedTime . "</li>						
							<li>ФИО специалиста: " . $model->doctor->name . "</li>
						</ul>					
						
						<p>Как только специалисты клиники обработают вашу заявку, 
						вы получите подтверждение в виде смс или звонка представителя клиники. 
						Для обеспечения высокого качества обслуживания операторы 
						Единого Медицинского Портала будут контролировать процесс обработки вашей заявки сотрудниками клиники.</p>
						
						<p>А пока просим вас поделиться своими впечатлениями.</p>
						<div class=\"pollAppointment\">";
						foreach($polls as $poll) {							
							$text.="<div class=\"one-poll\">							
										<div class=\"one-poll-text\">".$poll->question."</div>
										".CHtml::radioButtonList($poll->id, '', CHtml::listData($poll->pollOptions,'id','pollOption'))."
									</div>";
						}
						
						$text.=	
						"</div>
						<a href=\"" . $this->createUrl('site/register') . "\">Регистрируйтесь</a> на нашем сайте и получайте еще больше возможностей: 						
						<ul style=\"margin-top:0px;\">
							<li>Просмотр истории посещения клиник;</li>
							<li>Уведомления о скидках и акциях;</li>
							<li>Публикация отзывов, а также оценка клиник и врачей.</li>	
						</ul>
						
						<p>Будем признательны, если после приема вы оставите отзыв о медицинском учреждении и специалисте на нашем сайте.</p>
						<a onclick=\"sendPoll(); return true;\" class=\"btn btn-green\" href=\"/\">Далее</a>
						";
						Yii::app()->user->setFlash('confirmedVisit', $text);
						$this->redirect(array('/site/confirmedVisit'));
					}
				}
			} else {
				if (isset($_REQUEST['JSON'])) {
					$errors = $model->errors;
					if ($_REQUEST[get_class($model)]['phone'] != "" && Yii::app()->user->isGuest) {
						$errors['phone'][] = 'ACTIVATE_PHONE';
					}

					echo CJSON::encode(array('registryError' => $errors));
					Yii::app()->end();
				}

				#var_dump($model->getErrors());
			}
			$model->doctorId = $model->doctor->link;
			$model->serviceId = $model->service->link;
		}
		$timeBlock = WorkingHour::getDatesForCal($link, null, null, $model->doctor->link);

		#var_dump($timeBlock);

		$this->render('visit', array(
			'link' => $link,
			'model' => $model,
			'pv' => $pv,
			'time' => $timeBlock,
			'showPicker' => $showPicker
		));
	}

	public function actionPrice($link = null, $companyLink = null, $linkUrl = null) {
		if (Yii::app()->request->getParam('JSON')) {
			$this->layout = null;
			Yii::app()->clientScript->reset();
		}
		if ($companyLink && $linkUrl) {
			$addr = Address::model()->with('company')->find('company.linkUrl = :cLink and t.linkUrl = :aLink', [
				':cLink' => $companyLink,
				':aLink' => $linkUrl
			]);
			$link = $addr->link;
			$this->loadModel($link);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLink' => $mdl->company->linkUrl]);
			}
		}

		$activity = Yii::app()->request->getParam('activity', false);
		$activity = CompanyActivite::model()->findByAttributes(array('link' => $activity));

		$prices = array();

		if ($activity) {
			$criteria = new CDbCriteria();

			$criteria->with = array(
				'service',
				'address'
			);
			$criteria->compare('service.companyActiviteId', $activity->id);
			$criteria->compare('address.link', $link);
			$prices = AddressServices::model()->findAll($criteria);
		}
		$addressCriteria = new CDbCriteria();
		$addressCriteria->compare("t.link", $link);
		$addressCriteria->order = "ca.name ASC";
		$addressCriteria->with = [
			/* cущности разные, просто написаны одинаково  */
			'companyActivites' => [
				'together' => true,
				'with' => [
					'companyActivites' => [
						'alias' => 'ca'
					]
				]
			]
		];
		$model = Address::model()->find($addressCriteria);
		#$model = $this->loadModel($link);

		$noDataId = Yii::app()->db->createCommand()
				->select("id")
				->from(CompanyActivite::model()->tableName())
				->where("name = 'Нет данных'")
				->queryScalar();

		$countServices = array();
		$data = Yii::app()->db->createCommand()
						->select("*,count(*) as count")
						->from(AddressServices::model()->tableName())
						->where("companyActivitesId <> '$noDataId' AND  addressId ='$model->id'")
						->group('companyActivitesId')->queryAll();

		foreach ($data as $row) {
			if ($row['companyActivitesId'])
				$countServices[$row['companyActivitesId']] = $row['count'];
		}

		$this->render('price', array(
			'model' => $model,
			'activity' => $activity,
			'prices' => $prices,
			'countServices' => $countServices
		));
	}
	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 * @return Address
	 */
	public function loadModel($link) {
		$this->_model = Address::model()->findByLink($link);		
		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}
	
	public function loadModelDoc($link, $companyLinkUrl = null, $addressLinkUrl = null) {
		Address::$showInactive = true;
		$this->_model = Doctor::model()->find([
			'order' => 'deleted ASC',
			'condition' => 'linkUrl = :LU OR link = :LU',
			'params' => [
				':LU' => $link
			]
		]);

		if ($this->_model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		if (!empty($companyLinkUrl) && !empty($addressLinkUrl) && !$this->_model->deleted) {

			$match = false;
			foreach ($this->_model->placeOfWorks as $pw) {
				if ($pw->address->linkUrl === $addressLinkUrl) {
					$currPW = $pw;
					$match = true;
				}
			}

			if ($currPW->company->linkUrl !== $companyLinkUrl || !$match) {
				/* validates if companyLinkUrl corresponds to doctorLink */
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}

		return $this->_model;
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */

	public function getLeftMenu() {
		
		return '';
	}
	
	
	
public function actionViewdoc($link = null, $linkUrl = null, $companyLinkUrl = null, $addressLinkUrl = null) {
		
		$searchbox_type = SearchBox::S_TYPE_PATIENT;
		$searchbox_tab = Search::S_DOCTOR;
		$menu = [
			"Описание" => 'clinic/view',
		];
		
		$this->layout = '//layouts/widget_layout';
		//Yii::app()->clientScript->registerScriptFile('https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', CClientScript::POS_END);
		Yii::app()->clientScript->registerCoreScript('jquery.maskedinput');
		Doctor::$filterDeleted = false;
		$showPicker = true;

		//Не гос медицинская клиника
		//Есть контракт
		//Тип контракта с кнопкой
		#$add = Address::model()->
		if ($linkUrl) {

			$model = $this->loadModelDoc($linkUrl, $companyLinkUrl, $addressLinkUrl);
			if (empty($companyLinkUrl) || empty($addressLinkUrl)) {
				$this->redirect(['widget/viewdoc', 'linkUrl' => $model->linkUrl, 'companyLinkUrl' => $model->currentPlaceOfWork->company->linkUrl, 'addressLinkUrl' => $model->currentPlaceOfWork->address->linkUrl]);
			}
			UpdateCounters::updateDoctorCounter($linkUrl);
		} else {
			$mdl = $this->loadModel($link);
			if (!isset($_REQUEST['JSON'])) {
				$this->redirect([Yii::app()->urlManager->parseUrl(Yii::app()->request), 'linkUrl' => $mdl->linkUrl, 'companyLinkUrl' => $mdl->currentPlaceOfWork->company->linkUrl, 'addressLinkUrl' => $mdl->currentPlaceOfWork->address->linkUrl]);
			}
		}
		if ($this->_model->deleted) {
			$doctorDeleted = 1;
		}
		$company = $this->_model->currentPlaceOfWork->address->company;

		$companyActivites = [];
		foreach ($model->specialtyOfDoctors as $ds) {
			$companyActivites[] = $ds->doctorSpecialty->companyActivite->id;
		}

		$services = AddressServices::model()->findAll([
			'condition' => "addressId = '{$model->currentPlaceOfWork->address->id}' AND companyActivitesId IN ('" . implode("', '", $companyActivites) . "')",
			'with' => [
				'service' => [
					'together' => true
				]
			],
			'order' => 'service.name ASC',
		]);


		$data = Yii::app()->db->createCommand()
						->select("*,count(*) as count")
						->from(AddressServices::model()->tableName())
						->where("companyActivitesId <> '$noDataId' AND addressId ='{$model->currentPlaceOfWork->address->id}' AND companyActivitesId IN ('" . implode("', '", $companyActivites) . "')")
						->group('companyActivitesId')->queryAll();

		foreach ($data as $row) {
			if ($row['companyActivitesId'])
				$countServices[$row['companyActivitesId']] = $row['count'];
		}

		if (Yii::app()->user->isGuest) {
			//Если пользователь гость - выводим сообщение о возможности зарегистрироваться
			Yii::app()->user->setFlash('askAuth', 'Уважаемый гость, вы можете зарегистрироваться на нашем портале. После регистрации Вы получите доступ в личный кабинет, в котором Вы сможете: <ul>'
					. '<li>Отследить историю посещения клиник и врачей</li>'
					. '<li>Оставить отзыв о медицинском учреждении</li>'
					. '<li>Подписаться на рассылку о новых услугах и акциях клиник, Единого медицинского портала и др.</li>'
					. '</ul>'
					. '<div style="text-align:center"><span onclick="location.href=\'' . $this->createAbsoluteUrl(Yii::app()->urlManager->ParseUrl(Yii::app()->request), array('goto_register' => 1, 'link' => $link)) . '\'" class="btn-green">Зарегистрироваться</span> <span id="closeAuthBlock" class="btn-red">Скрыть блок</span></div>');
		} else {
			//Если пользователь зарегистрирован - проверяем есть ли у него подтвержденный телефон для записи на 1 раз
			$pv = PhoneVerification::model()->find("status=:status AND userId = :userId", array(
				':status' => PhoneVerification::APPOINTMENT,
				':userId' => Yii::app()->user->model->id,
			));
		}

		$addr = Address::model()->findByLink($addressLinkUrl);
		if ($addr->linkUrl !== $model->currentPlaceOfWork->address->linkUrl) {
			Yii::app()->clientScript->registerLinkTag('canonical', null, Yii::app()->createAbsoluteUrl('doctor/view', [
						'companyLinkUrl' => $model->currentPlaceOfWork->company->linkUrl,
						'addressLinkUrl' => $model->currentPlaceOfWork->address->linkUrl,
						'linkUrl' => $model->linkUrl
			]));
		}

		$appointment = new AppointmentToDoctors('visit_to_doctor');
		$appointment->doctor = $model;
		$appointment->company = $appointment->company = $addr->company;
		$appointment->address = $addr;
		$appointment->name = Yii::app()->user->model->name;
		$appointment->phone = Yii::app()->user->model->telefon;
		$appointment->email = Yii::app()->user->model->email;
		$appointment->clientIdGA = Yii::app()->request->cookies['_ga_cid']->value;
		$time = [
			WorkingHour::getDate($appointment->address->link, date('Y-m-d'), $appointment->doctor->link)
		];


		#$time = WorkingHour::getDatesForCal($appointment->address->link, null, null, $appointment->doctor->link);
		if (Yii::app()->request->getParam('removeGuestPhone')) {
			if (Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll("phone=:phone AND (status=:stat1 or status=:stat2)", array(
					"phone" => Yii::app()->session['appointment_phone_set'],
					"stat1" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT,
				));
				Yii::app()->session['appointment_phone_set'] = "";
			}
			$this->redirect(array("/widget/viewdoc",
				'linkUrl' => $linkUrl
			));
		}

		if (Yii::app()->request->getParam(get_class($appointment))) {
			
			$appointment->attributes = Yii::app()->request->getParam(get_class($appointment));
			$appointment->createdDate = date('Y-m-d H:i:s');
			$appointment->userId = Yii::app()->user->id;
			$appointment->statusId = AppointmentToDoctors::SEND_TO_REGISTER;

			$lnk = Yii::app()->request->getParam(get_class($appointment))['address']['link'];
			if (!empty($lnk)) {
				$addr = Address::model()->findByLink($lnk);
				if ($addr->company->id == $appointment->company->id) {
					$appointment->address = $addr;
				}
			}

			/* Если гость ввел номер который указан у пользователя, привязываем запись к аккаунту */
			/* Если не гость ввел номер при реге, сохраняем в аккаунт */
			if (Yii::app()->user->isGuest) {
				$userModel = User::model()->findByAttributes([
					'telefon' => $appointment->phone
				]);
				if ($userModel) {
					$appointment->userId = $userModel->id;
				}
			} else {
				if (empty(Yii::app()->user->model->telefon)) {
					$userModel = Yii::app()->user->model;
					$userModel->telefon = $appointment->phone;
					$userModel->save();
				}
			}

			if (isset($_REQUEST['ajax']) && $_REQUEST['ajax'] === 'doctor-visit') {
				$arrData = CJSON::decode(CActiveForm::validate($appointment));
				if ($_REQUEST[get_class($appointment)]['phone'] == "" && $_REQUEST['phoneInput'] != "") {
					$arrData['AppointmentToDoctors_phone'][0] = 'Телефон не активирован';
					unset($arrData['AppointmentToDoctors_phone'][1]);
				}
				echo CJSON::encode($arrData);
				Yii::app()->end();
			}

			if ($appointment->save()) {
				/* Чистим таблицу PhoneVerification для временного номера */
				#if(!Yii::app()->user->isGuest) {
				PhoneVerification::model()->deleteAll('phone = :pId AND (status = :stat OR status = :stat2)', array(
					"pId" => $appointment->phone,
					"stat" => PhoneVerification::APPOINTMENT,
					"stat2" => PhoneVerification::READY_TO_APPOINTMENT
				));
				#}
				//Чистим сессию
				if (Yii::app()->user->isGuest) {
					Yii::app()->session['appointment_phone_set'] = "";
				}


				/* if ($notifyOperator) {

				  } */


				#if($appointment->phone != '+79213434502') {
				/* Уведомить штатного оператора о том, что произведена заявка в клинику */
				$appointment->notifyOperator();

				/* Оповещаем пользователя по смс и почте */
				$appointment->sendMailToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				if (Yii::app()->user->isGuest) {
					$appointment->sendSmsToGuest(AppointmentToDoctors::SEND_TO_REGISTER);
				} else {
					$appointment->sendSmsToUser(AppointmentToDoctors::SEND_TO_REGISTER);
				}
				/* Оповещаем клинику по смс и почте */
				$appointment->sendMailToClinic('new_record');
				$appointment->sendSmsToClinic('new_record');
				#}

				if (isset($_REQUEST['JSON'])) {
					echo CJSON::encode(array('registrySuccess' => 'Запись успешно произведена'));
					Yii::app()->end();
				} else {
					if (!Yii::app()->user->isGuest) {
						/* if ($notifyOperator) { */
						$this->redirect(array('/user/registry', 'filter' => 'all'));
						/* } else {
						  $this->redirect(array('user/registry', 'link' => $doctor->link));
						  } */
					} else {

						/* create if account doesn't exists and autologin after */
						if (!$userModel) {
							$userModel = new User();
							$userModel->name = $appointment->name;
							$userModel->telefon = $appointment->phone;
							$userModel->password = '';
							$userModel->phoneActivationDate = new CDbExpression('NOW()');
							$userModel->dateRegister = new CDbExpression('NOW()');
							$userModel->phoneActivationStatus = 1;
							$userModel->status = 1;
							$userModel->sexId = 'ae11c45d855a991340c5f59edcb404da';
							$userModel->save(false);

							$appointment->userId = $userModel->id;
							$appointment->save(false);
						}

						$identity = new UserIdentity($userModel->telefon, $userModel->password);
						$identity->authenticate(true);
						Yii::app()->user->login($identity, 3600 * 24 * 30);

						Yii::app()->user->setFlash('visited', 1);
						$this->redirect(array('/user/profile'));
					}
				}
			} else {
				if (isset($_REQUEST['JSON'])) {
					$errors = $appointment->errors;
					if ($_REQUEST[get_class($appointment)]['phone'] != "" && Yii::app()->user->isGuest) {
						$errors['phone'][] = 'ACTIVATE_PHONE';
					}

					echo CJSON::encode(array('registryError' => $errors));
					Yii::app()->end();
				}
			}
		}




		/* Выполняем подключение определенных данных, если нужно вывести плашку отключенной/заблокированной */
		if ($doctorDeleted || $addr->userMedicals->agreementNew != 1 || !$addr->isActive) {
			$metroId = $appointment->address->nearestMetroStation->id;
			$similar = [];
			$countSpec = [];
			foreach ($model->specialties as $sp) {
				if (!in_array($sp->shortSpecialtyId, $countSpec) && $sp->shortSpecialtyId) {				
					$countSpec[$sp->shortSpecialtyOfDoctor->name] = $sp->shortSpecialtyId;
				}
			}
			
			if (count($countSpec)) {
				if (count($countSpec) == 1) {
					$countPerSpec = 4;
				} else {
					$countPerSpec = 8/count($countSpec);
				}
			}
			$doctorIds = [];			
			if(count($countSpec)) {				
				foreach ($countSpec as $key => $spec) {					
					$criteria = new CDbCriteria();
					$criteria->with = [
						'placeOfWorks' => [
							'together' => true,
							'with' => [								
								'address' => [
									'together' => true,
									'with' => [
										'company' => [
											'together' => true,
											'with' => [
												'companyContracts' => [
													'joinType' => 'INNER JOIN'
												]
											]
										],
										'userMedicals' => [
											'together' => true,
										],
										'metroStations' => [
											'together' => true,
										]
									]
								]
							]
						],
						'specialties' => [
							'together' => true,
						],
						'scientificTitle' => [
							'select' => 'id,name',
							'together' => true
						],
						'scientificDegrees' => [
							'select' => 'id,name',
							'together' => true
						],
						'sex' => [
							'select' => 'id,name,order',
							'together' => true
						],
					];
					$criteria->compare('specialties.shortSpecialtyId', $spec);
					$criteria->compare('t.id', '<>' . $this->_model->id);
					$criteria->compare('userMedicals.agreementNew', 1);
					$criteria->compare('address.isActive',1);
					$criteria->addNotInCondition('t.id', $doctorIds);
					$criteria->compare('address.cityId', (new Search())->cityId);
					$criteria->limit = $countPerSpec;					
					$criteria->group = 't.id';
					$criteria->order = 't.rating DESC';
					$criteria2 = clone $criteria;
					$criteria->compare('metroStations.id', $metroId);

					#echo $countPerSpec;
					$data = Doctor::model()->findAll($criteria);					
					if (empty($data)) {
						$data = Doctor::model()->findAll($criteria2);
					}					
					foreach($data as $d) {
						$doctorIds[] = $d->id;
					}					
					
					$similar[$key] = $data;
					#var_dump(count($similar[$key]));
				}
			} else {
				$criteria = new CDbCriteria();
					$criteria->with = [
						'placeOfWorks' => [
							'together' => true,
							'with' => [
								'company' => [
									'together' => true,
									'with' => [
										'companyContracts' => [
											'joinType' => 'INNER JOIN'
										]
									]
								],
								'address' => [
									'together' => true,
									'with' => [
										'userMedicals' => [
											'together' => true,
										],
										'metroStations' => [
											'together' => true,
										]
									]
								]
							]
						],
						'specialties' => [
							'together' => true,
						]
					];					
					$criteria->compare('t.id', '<>' . $this->_model->id);
					$criteria->compare('userMedicals.agreementNew', 1);
					if($addr->isActive) {
						$criteria->compare('address.id', $addr->id);
					} 
					$criteria->compare('address.isActive',1);
								
				
					$criteria->limit = 4;
					$criteria->group = 't.id';
					$criteria->order = 't.rating DESC';
					

					#echo $countPerSpec;
					$data = Doctor::model()->findAll($criteria);
					$similar['Похожие врачи клиники'] = $data;
			}
			



			$specArr = $searchArr = [];
			foreach ($model->specialties as $sp) {
				if (!in_array($sp->shortSpecialtyOfDoctor->id, $specArr)) {
					$specArr[$sp->shortSpecialtyOfDoctor->linkUrl] = $sp->shortSpecialtyOfDoctor->name;
				}
			}

			#$searchSimilarLink = $this->createUrl('/clinic',$searchArr);
		}

		$this->render('viewdoc', array(
			'model' => $model,
			'company' => $company,
			'countServices' => $countServices,
			'pv' => $pv,
			'services' => $services,
			'doctorDeleted' => $doctorDeleted,
			'showPicker' => $showPicker,
			'appointment' => $appointment,
			'specArr' => $specArr,
			'time' => $time,
			'similar' => $similar,
			'addressLinkUrl' => $addressLinkUrl,
		));
	}
	
	public function actionSuccess() {
		
		$this->layout = '//layouts/widget_layout';
		$tmp = $storedAppointment = Yii::app()->session['storedAppointment'];
		
		if (!empty($tmp['hiddenField'])) {
			$tmp['hiddenField'] = '';
			Yii::app()->session['widgetReferrer'] = '';
			Yii::app()->session['storedAppointment'] = $tmp;
			$this->render('success', ['storedAppointment' => $storedAppointment]);
		}
	}

}
