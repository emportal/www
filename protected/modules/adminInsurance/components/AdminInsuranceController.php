<?php

/**
 *
 */
class AdminInsuranceController extends Controller
{

	public function init() {		
		parent::init();
		
		Yii::app()->user->loginUrl = $this->createUrl("default/login");
		
	}

	public function filters() {
		return array(
			'accessControl',
			['application.filters.isOfferAccepted - offer'],
		);
	}

	public function accessRules() {
		return array(
			array('allow',
				'actions'	 => array('login', 'error'),
				'users'		 => array('?'),
			),
			array('deny',
				'actions'	 => array(),
				'users'		 => array('?'),
			),
		);
	}

	/**
	 * @return Insurance
	 */
	public function loadModel() {
		if(!empty(Yii::app()->user->model->insurance)) {
			return Yii::app()->user->model->insurance;
		}
		else {
			Yii::app()->user->logout();
			$this->redirect($this->createUrl("default/error"));
			return false;
			
		}
	}
}