<?php

class DefaultController extends AdminInsuranceController {
	/* public function filters() {
	  return array_merge(
	  ['application.filters.isOfferAccepted']	,
	  parent::filters()
	  );
	  } */

	public function actionIndex() {

		$model = $this->loadModel();
		$temp = $model->company;

		$company = AgrCompany::model()->findByLink($temp->link);

		/* if(str_replace(chr(13),"",$company->description) == str_replace(chr(13),"",$temp->description)
		  && $company->dateOpening == $temp->dateOpening) {
		  $check=true;
		  } */

		if (!$company) {
			$company = new AgrCompany();
			$company->attributes = $temp->attributes;
		}
		$company->syncThis = false;


		//$company->attributes = $temp->attributes;

		if ($attr = Yii::app()->request->getParam(get_class($model))) {
			$model->attributes = $attr;
			$model->save();
		}

		if ($attr = Yii::app()->request->getParam(get_class($company))) {

			if (is_array($attr)) {
				$company->attributes = $attr;

				//Если строки идентичны - не производим сохранения
				if (str_replace(chr(13), "", $attr['description']) == str_replace(chr(13), "", $temp->description) && $attr['dateOpening'] == $temp->dateOpening) {

					if (!$company->isNewRecord)
						$company->delete();
					$dontsave = true;

					Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
					$this->redirect('index');
				}

				$company->logo = CUploadedFile::getInstance($company, 'logo');


				if ($company->isNewRecord ? (!$dontsave ? $company->save() : true) : (!$dontsave ? $company->update() : true)) {
					$company->validate();

					if (!$company->getError('logo')) {
						if ($company->logo instanceof CUploadedFile) {

							$company->logo->saveAs($company->logoPath);
							Yii::app()->image->load($company->logoPath)->resize(200, 200)->save($company->logoPath);
						}

						if (Yii::app()->request->getParam('delete', false))
							unlink($company->logoPath);

						Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
						$this->redirect('index');
					} else {
						
					}
				}
			}
		}


		$this->render('index', array('model' => $model, 'company' => $company));
	}

	public function actionUpdateInfo() {

		$model = $this->loadModel();
		$company = $model->company;




		$this->redirect('index');
	}

	public function actionUpdateLicenses() {

		$model = $this->loadModel();
		$company = $model->company;

		$attr = Yii::app()->request->getParam(get_class($model));

		#if (is_array($attr)) {

		$model->setCompanyLicenses($attr['companyLicenses']);
		$model->save();

		#}
		#$model->setCompanyLicenses($attr['companyLicenses']);

		if (Yii::app()->request->getParam('end')) {
			$this->renderPartial('licenseForm', array(
				'model' => $model,
				'company' => $company
			));

			Yii::app()->end();
		}
		$this->redirect('index');
	}

	public function actionContacts() {
		/*
		  Таблицы с припиской agr_ - таблицы подтверждения
		  Все данные изначально пишутся в таблицы подтверждения
		 */

		$model = $this->loadModel();
		$attrs = Yii::app()->request->getParam(get_class($model));

		/* Исправляем неправильные часы работы выгруженные из 1С */
		$needSave = false;
		foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $day) {
			if ($model->workingHours->{$day . Finish} === '23:59' || ($model->workingHours->{$day . Finish} == '11111' && $model->workingHours->{$day . Start} != '11111') || ($model->workingHours->{$day . Finish} != '11111' && $model->workingHours->{$day . Start} == '11111')
			) {
				$model->workingHours->{$day . Finish} = '11111';
				$model->workingHours->{$day . Start} = '11111';
				$needSave = true;
			}
		}
		if ($needSave) {
			$model->workingHours->save();
		}


		/* */
		if (!$model->www) {
			$model->www = new Ref();
		}

		if (!$model->email) {
			$model->email = new Email();
		}

		if (!$model->agrphones) {
			$i = 0;

			// some magic, doesnt working if simply push object into array, only if set like
			// $model->agrphones = array();
			$phones = array();
			foreach ($model->phones as $ph) {
				$newPhone = new AgrPhone();
				$newPhone->attributes = $ph->attributes;
				$phones[$i] = $newPhone;
				$i++;
			}
			$model->agrphones = $phones;
		}

		if (!$model->agr_www) {
			/* Если существуют подтвержденные данные, подгружаем их в модель для подтверждения - она отображается на сайте всегда */
			$model->agr_www = new AgrRef();
			$model->agr_www->attributes = $model->www->attributes;
		} else {

			$agr_www_set = true; // статус,  существует ли модель подтверждения
		}

		if (!$model->agr_email) {
			/* Если существуют подтвержденные данные, подгружаем их в модель для подтверждения - она отображается на сайте всегда */
			$model->agr_email = new AgrEmail();
			$model->agr_email->attributes = $model->email->attributes;
		} else {
			$agr_email_set = true;  // статус,  существует ли модель подтверждения
		}
		#echo $model->agr_www->refValue. " ". $model->www->refValue."<br>";
		/* if(preg_match("|^(https?://)?(www.)?[-\.\w]+$|", "https://www.tesm..tt" )) {
		  echo 1;
		  } else {
		  echo 2;
		  } */

		if ($attrs) {

			$model->attributes = $attrs;


			if (!$model->agr_www) {
				$model->agr_www = new AgrRef();
			}
			$model->agr_www->syncThis = false;
			$model->agr_www->setAttributes($attrs['agr_www']);



			if (empty($attrs['agr_www']['refValue'])) {
				/*
				 * Условие выполняется если модель подтверждения уже существует ( были введены новые данные ) и ввели старые данные
				 * Другими словами, логика возврата к старым данным, удаление данных из таблицы подтверждения
				 */
				if ($attrs['agr_www']['refValue'] == $model->www->refValue && $agr_www_set) {
					$model->agr_www->delete();
				} else {
					/*
					 * Скрипт попадает в ветку ИНАЧЕ в двух случаях
					 * 1. Если прислана пустая строка и модели подтверждения не существует - не должны сохранять
					 * 2. Если прислана пустая строка, а в подтвержденной модели есть данные => тикет на удаление данных
					 */
					if ($model->agr_www->id) {
						$model->agr_www->refValue = "";
						$sWww = $model->agr_www->save(false);
					}
				}
			} else {
				if ($model->agr_www->refValue == $model->www->refValue && $agr_www_set) {
					/* Аналогично верхней части, если присланы уже подтвержденные данные, то модель подтверждения удаляется */
					$model->agr_www->delete();
				} else {
					/*
					 * Скрипт попадает в ветку, когда
					 * 1.  Модели подтверждения еще нет
					 * 2.  Модель подтверждения существует, но введены данные, например пользователь ввел test1.ru, а потом ввел test2.ru
					 */
					if ($model->agr_www->refValue != $model->www->refValue) {
						$model->agr_www->setAttributes($attrs['agr_www']);
						$model->agr_www->type = ReferenceType::model()->find("link = 000000001 AND id <> ''");
						$model->agr_www->ownerId = $model->id;
						$model->agr_www->name = $model->agr_www->type->name;
						$sWww = $model->agr_www->save();
					}
				}
			}

			if (!$model->agr_email) {
				$model->agr_email = new AgrEmail();
			}
			$model->agr_email->syncThis = false;
			$model->agr_email->setAttributes($attrs['agr_email']);
			/* if($model->agr_email->isNewRecord && empty($attrs['agr_email']['name'])) {
			  $sEmail = true;
			  }
			  else */


			if (empty($attrs['agr_email']['name'])) {
				if ($attrs['agr_email']['name'] == $model->email->name && $agr_email_set) {
					$model->agr_email->delete();
				} else {
					if ($model->agr_email->id) {
						$model->agr_email->name = "";
						$sEmail = $model->agr_email->save();
					}
				}
			} else {
				if ($attrs['agr_email']['name'] == $model->email->name && $agr_email_set) {
					$model->agr_email->delete();
				} else {
					if ($model->agr_email->name != $model->email->name) {
						$model->agr_email->setAttributes($attrs['agr_email']);
						$model->agr_email->type = EmailType::model()->findByAttributes(array('link' => '000000001'));
						$model->agr_email->ownerId = $model->company->id;
						$model->agr_email->addressId = $model->id;
						$model->agr_email->forCompany = 1;
						$model->agr_email->general = 1;
						$sEmail = $model->agr_email->save();
					}
				}
			}


			$model->workingHours = $attrs['workingHours'];

			if ($model->save() && $model->workingHours->validate()) {


				$model->setPhones($attrs['agrphones']);
				#var_dump($sWWW,$sEmail);
				#if($sWww && $sEmail) {
				$this->refresh();
				#}
			}
		}
		unset($attrs['agrphones']['%new%']);


		$this->render('contacts', array('model' => $model, 'WorkingHour' => new WorkingHour(), 'phones' => $attrs['agrphones']));
	}

	public function actionOffer() {
		$this->layout = "/layouts/column_offer";
		$agreement = Yii::app()->request->getParam('offerAccept');
		if ($agreement) {
			$model = Yii::app()->user->model;
			$model->agreement = 1;
			$model->createDate = new CDbExpression("NOW()");
			if ($model->save()) {
				$this->redirect("index");
			}
		}
		$this->render('offer');
	}

	public function actionLogin() {

		$this->layout = 'column';

		if (!Yii::app()->user->isGuest)
			$this->redirect('index', true);

		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
				$this->redirect(array('index'));
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}

	public function actionLogout() {
		Yii::app()->user->logout();

		$this->redirect($this->createUrl("default/login"));
	}

	public function actionError() {
		Yii::app()->user->setFlash('error', 'К данному аккаунту не привязан ни один адрес клиники, обратитесь к администрации портала.');
		$this->render('error');
	}

	public function actionLicenseImageUpload($link) {

		$model = CompanyLicense::model()->findByLink($link);
		/* @var $model CompanyLicense */

		if ($model->addressId != $this->loadModel()->id) {
			$this->redirect(array('index'));
		}

		if ($image = CUploadedFile::getInstanceByName('image')) {

			$model->image = $image;

			if ($model->validate()) {
				$image->saveAs($model->imagePath);

				$this->redirect(array('index'));
			}
		}
		if (count($_POST)) {
			$this->redirect(array('index'));
		} else {

			Yii::app()->clientScript->reset();
			$this->layout = '//layouts/empty';

			//if (Yii::app()->request->isAjaxRequest) {
			$this->render('imageUploadForm', array('model' => $model));
			//}
			//else {
			//	$this->redirect(array('index'));
			//}
		}
	}

	public function actionLicenseImageDelete($link) {

		$model = CompanyLicense::model()->findByLink($link);
		/* @var $model CompanyLicense */

		if ($model->addressId != $this->loadModel()->id) {
			$this->redirect(array('index'));
		}

		unlink($model->imagePath);

		$this->redirect(array('index'));
	}

	public function actionVisits() {
		$count = Yii::app()->db->createCommand()
				->select("COUNT(*)")
				->from(AppointmentToDoctors::model()->tableName())
				->where("plannedTime < NOW() AND statusId=:statId", array(
					'statId' => AppointmentToDoctors::ACCEPTED_BY_REGISTER
				))
				->queryScalar();
		if ($count) {
			AppointmentToDoctors::model()->updateAll(array(
				'statusId' => AppointmentToDoctors::VISITED
					), "plannedTime < NOW() AND statusId=:statId", array(
				'statId' => AppointmentToDoctors::ACCEPTED_BY_REGISTER
			));
		}
		#this->plannedTime > date("Y-m-d H:i:s")
		$model = $this->loadModel();
		$criteria = new CDbCriteria();
		$user = Yii::app()->user->model;
		$attr = Yii::app()->request->getParam(get_class($user));

		if ($attr) {
			$user->phoneForAlert = $attr['phoneForAlert'];
			$user->update();
		}
		$criteria->compare('address.id', $model->id);
		$criteria->order = 'createdDate DESC, plannedTime DESC';

		$dataProvider = new CActiveDataProvider('AppointmentToDoctors', array(
			'criteria' => $criteria,
			'sort' => array(
				'multiSort' => true,
				'sortVar' => 'sort',
				'attributes' => array(
					'doctor' => 'doctor.surName',
					'service' => 'service.name',
					'*'
				),
			)
		));

		$this->render('visits', array('model' => $model, 'dataProvider' => $dataProvider));
	}

	public function actionGallery($gallery_id = null) {
		Yii::app()->clientScript->reset();
		$this->layout = '//layouts/empty';

		$model = $this->loadModel();
		$this->render('gallery', array('model' => $model));
	}

}
