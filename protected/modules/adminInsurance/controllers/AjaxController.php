<?php

class AjaxController extends AdminInsuranceController {

	public $layout = 'empty';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'cities' => array(
				'class' => 'AutoCompleteAction',
				'model' => 'City',
				'attributeValue' => 'name',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionAddClient($companyId) {
		if ($companyId) {
			$insuranceId = Yii::app()->user->model->insurance->id;
			$cId = Company::model()->findByLink($companyId)->id;
			if ($insuranceId && $cId) {
				$model = InsuranceClinicClients::model()->findByAttributes([
					'insuranceId' => $insuranceId,
					'companyId' => $cId
				]);
				if ($model) {					
					echo CJSON::encode(['success' => false, 'message' => 'Клиент уже добавлен']);
				} else {					
					$model = new InsuranceClinicClients();
					$model->insuranceId = $insuranceId;
					$model->companyId = $cId;
					if ($model->save()) {
						echo CJSON::encode(['success' => true, 'message' => 'Клиент успешно добавлен']);
					} else {						
						echo CJSON::encode(['success' => false, 'message' => 'Неизвестная ошибка']);
					}
				}
			}
		}
		Yii::app()->end();
	}

	public function actionRemoveClient($companyId) {
		if ($companyId) {
			$insuranceId = Yii::app()->user->model->insurance->id;
			$cId = Company::model()->findByLink($companyId)->id;
			
			if ($insuranceId && $cId) {
				
				if(InsuranceClinicClients::model()->findByAttributes([
					'insuranceId' => $insuranceId,
					'companyId' => $cId
				])->delete()) {
					echo CJSON::encode(['success' => true, 'message' => 'Клиент успешно удален']);
				} else {
					echo CJSON::encode(['success' => false, 'message' => 'Клиент не найден']);
				}
			}
		}
		Yii::app()->end();
	}

}
