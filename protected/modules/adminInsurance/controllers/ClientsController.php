<?php

class ClientsController extends AdminInsuranceController
{

	public function actionIndex()
	{		
		$model = $this->loadModel();
		$pcompanies = $model->clients;
		$ids=[];
		foreach($pcompanies as $p) {
			$ids[]=$p->id;
		}
		
		$criteria = new CDbCriteria();
		$criteria->select = "id,link,name";
		$criteria->with = [
			'addresses'=>[
				'select'=>false,
				'together'=>true
			],
			'companyType'=>[
				'together'=>true
			]
		];
		$search = new Search();
		$criteria->order = "t.name ASC";
		$criteria->compare('companyType.isMedical', 1);
		$criteria->addNotInCondition('companyType.id', Address::$arrGMU);	
		#$criteria->addNotInCondition("t.id", $ids);
		$criteria->compare('addresses.cityId',$search->cityId);
	
		$companies = Company::model()->clinic()->findAll($criteria);
		
		
		$this->render('index', [
			'companies' => $companies,
			'pcompanies'=>$pcompanies,
			'ids'=>$ids
		]);
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='company-experts')
		{
			foreach($model->getErrors() as $attribute=>$errors) {
				$result[CHtml::activeId($model,$attribute)]=$errors;
			}
	
			echo function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);
			Yii::app()->end();
		}
	}	
}