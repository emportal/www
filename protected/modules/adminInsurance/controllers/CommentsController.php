<?php

class CommentsController extends AdminInsuranceController
{
	private $_model;
	public function actionIndex()
	{
		$clinic = $this->loadModel();
		$criteria = new CDbCriteria();
		
		$criteria->with = array();
		$criteria->compare('`addressId`', $clinic->id);
		$dataProvider=new CActiveDataProvider('RaitingCompanyUsers', array(
			'criteria' => $criteria,
			'pagination'=>array(
					'pageSize'=>30,
			),
			'sort'=>array(
				'defaultOrder'=>'status ASC,date DESC'
			)
			
		));


		$this->render('index',array('dataProvider'=>$dataProvider));
	}
	public function actionExperts()
	{
		$clinic = $this->loadModel();
		$criteria = new CDbCriteria();
	
		$criteria->with = array(
			'doctor'=>array(
				'together'=>true,
				'with'=>array(
					'currentPlaceOfWork'=>array(
						'together'=>true,
						'with'=>array(
							'address'=>array(
								'together'=>true
							)	
						)
					)					
				)
			)
		);
		$criteria->compare('`address`.`id`', $clinic->id);
		$dataProvider=new CActiveDataProvider('RaitingDoctorUsers', array(
				'criteria' => $criteria,
				'pagination'=>array(
						'pageSize'=>30,
				),
				'sort'=>array(
					'defaultOrder'=>'date DESC'
				)
	
		));
	
	
		$this->render('experts', array('dataProvider'=>$dataProvider));
	}
	
	public function actionView($id)
	{
		$model = new RaitingCompanyUsers('answer');
		
		
		if($attr = Yii::app()->request->getParam(get_class($model))) {
			
			if (is_array($attr)) {
				$comment = $model->findByPk($id);
				
				$comment->attributes = $attr;
				$comment->status = 0;
				if ($comment->save()) {
					Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
					$this->redirect('index');
				}
			}
		}
		
		$this->render('answer', array(
				'model' => $this->loadModelComment($id),
		));
	}
	
	public function actionViewExpert($id)
	{
		$model = new RaitingDoctorUsers('answer');
		
		
		if($attr = Yii::app()->request->getParam(get_class($model))) {
			
			if (is_array($attr)) {
				$comment = $model->findByPk($id);
				
				$comment->attributes = $attr;
				$comment->status = 0;
				if ($comment->save()) {
					Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
					$this->redirect('index');
				}
			}
		}
		
		$this->render('answer', array(
				'model' => $this->loadModelExpertComment($id),
		));
	}
	
	public function loadModelComment($id)
	{
		$this->_model = RaitingCompanyUsers::model()->findByPk($id);
		
		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}
	
	public function loadModelExpertComment($id)
	{		
		$this->_model = RaitingDoctorUsers::model()->findByPk($id);
		
		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}
}