<?php

class NewsController extends AdminInsuranceController {

	private $_model;

	public function filters() {
		return array(
			'postOnly + delete', // we only allow deletion via POST request
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules() {
		return array(
			array('allow', 
				'actions' => array('index', 'add', 'update', 'delete'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionIndex() {
		$clinic = $this->loadModel();
		$criteria = new CDbCriteria();

		$criteria->compare('`clinicId`', $clinic->id);

		$dataProvider = new CActiveDataProvider('News', array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
			'sort' => array(
				'defaultOrder' => 'publish DESC,datePublications DESC'
			)
		));


		$this->render('index', array('dataProvider' => $dataProvider));
	}

	public function actionAdd() {
		$model = new News();

		if ($attr = Yii::app()->request->getParam(get_class($model))) {
			$model->attributes = $attr;

			$typeId = TypeNews::model()->clinic()->find()->id;
			$model->typeNewsId = $typeId;

			$model->publish = 0;
			$model->Date = date("Y-m-d H:i:s", time());
			$model->clinicId = $this->loadModel()->id;
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
				$this->redirect('index');
			}
		}

		$this->render('update', array(
			'model' => $model
		));
	}

	public function actionUpdate($id) {
		$model = $this->loadModelNews($id);

		if ($attr = Yii::app()->request->getParam(get_class($model))) {

			if (is_array($attr)) {

				$temp = $model->attributes;
				$model->attributes = $attr;

				$typeId = TypeNews::model()->clinic()->find()->id;
				$model->typeNewsId = $typeId;

				/* Если нет изменений - не сохраняем модель */
				$tmp1 = array_diff($temp, $model->attributes);
				$tmp2 = array_diff($model->attributes, $temp);

				$model->publish = 0;
				$save = true;
				if (!$tmp1 && !$tmp2) {
					$save = false;
				}

				if ($save) {

					if ($model->save()) {
						Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
						$this->redirect('index');
					}
				} else {
					$this->redirect('index');
				}
			}
		}

		$this->render('update', array(
			'model' => $model
		));
	}

	public function actionDelete($id) {
		$this->loadModelNews($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function loadModelNews($id) {
		$this->_model = News::model()->findByPk($id);

		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}

}
