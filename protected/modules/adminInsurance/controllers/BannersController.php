<?php

class BannersController extends AdminInsuranceController {

	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->compare('companyId', $this->loadModel()->company->id);

		$dataProvider = new CActiveDataProvider('Banner', array(
			'criteria'	 => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
		));


		$this->render('index', array('dataProvider' => $dataProvider));
	}

	public function actionAdd() {

		$model = new Action();

		if ( $_POST[get_class($model)] ) {
			$model->attributes = $_POST[get_class($model)];

			$model->company = $this->loadModel()->company;

			if ( $model->save() )
				$this->redirect('actions/index');
		}


		$this->render('update', array('model' => $model));
	}

	public function actionUpdate($link) {

		$model = Action::model()->findByAttributes(array(
			'link'		 => $link,
			'companyId'	 => $this->loadModel()->company->id,
		));

		if ( $_POST[get_class($model)] ) {
			$model->attributes = $_POST[get_class($model)];

			if ( $model->save() )
				$this->redirect('actions/index');
		}


		$this->render('update', array('model' => $model));
	}

	public function actionDelete($link) {

		$model = Action::model()->findByAttributes(array(
			'link'		 => $link,
			'companyId'	 => $this->loadModel()->company->id,
		));

		$model->delete();

		$this->redirect('actions/index');
	}

}