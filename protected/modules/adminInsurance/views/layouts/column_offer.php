<?php /* @var $this AdminClinicController */ 
 
$countRegistry=Yii::app()->db->createCommand()->select("COUNT(*)")
	->from(AppointmentToDoctors::model()->tableName())
	->where("addressId=:aId AND statusId=".AppointmentToDoctors::SEND_TO_REGISTER,array(':aId'=>Yii::app()->user->model->addressId))
	->queryScalar();

?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="wrapper clinic">
	<?php $this->renderPartial('/layouts/_header_nulled'); ?>
	<section class="middle">
		<div class="container">
			<div class="search-result cabinet">				
				<?php /*?>
				<div class="search-result-head">
					<span class="f-left">
						<h4><?= $this->loadModel()->getName()?></h4>
					</span>
					<span class="f-right">
						<?= $this->loadModel()->company->companyContracts ?
							"Договор №{$this->loadModel()->company->companyContracts->contractNumber} до ". Yii::app()->dateFormatter->formatDateTime($this->loadModel()->company->companyContracts->periodOfValidity) :
							'Нет данных о контрактах'?>
					</span>
					<?php echo $this->clips['head_content']; ?>
				</div>
				<?*/?>
				<table class="search-result-table">
					<tbody>
						<tr class="search-result-box menu">							
                            <td class="search-result-info-offer zakaz">
								<?php echo $content; ?>
                            </td>
                        </tr>
                    </tbody>
				</table>
			</div>
			<div class="clearfix">

			</div><!-- /clearfix -->
		</div><!-- /container-->
	</section><!-- /middle-->
</div><!-- /wrapper -->
<footer class="footer">
	<p class="copyright">© 2012-2013 все права защищены. Все права на любые материалы, опубликованные на сайте, защищены в соответствии с российским и международным<br>законодательством об авторском праве. При использовании текстовых материалов в интернете и при использовании текстов в печатных и электронных<br>СМИ ссылка на данный ресурс обязательна.</p>
</footer><!-- /footer -->
<?php $this->endContent(); ?>