<?php if ( Yii::app()->user->hasFlash('success') ): ?>
<div class="flash-success">
	<span><?php echo Yii::app()->user->getFlash('success')?></span>
</div>
<?php endif;?>
<div class="flash-notice">
	<p style="margin-bottom:0px;">
		<strong>Для заполнения данного раздела Вам необходимо:</strong><br/>
		1. Скачать форму прайса.<br/>
		2. Выбрать в форме оказываемые Вашей клиникой услуги и указать их стоимость в столбце D.<br/>
		3. Загрузить заполненную форму и нажать кнопку "Добавить".<br/>
		<strong>Примечание:</strong><br/>Если Вы не нашли нужную Вам услугу в форме прайса, воспользуйтесь кнопкой <nobr>"Добавить новую услугу"</nobr>. Обращаем Ваше внимание на то, что новая услуга появится на сайте только после её модерации.
	</p>
</div>



<a class="btn" href="<?= $this->createUrl('getfile')?>">Скачать форму прайса (xls)</a>
<br>
<br>
<br>
<h4>Загрузить прайс</h4>
<form action="<?= $this->createUrl('services/upload')?>" method="post" enctype="multipart/form-data">
<input type="file" value="" name="file" autocomplete="off">
<button class="btn" title="Добавить" href="#">Добавить</button>
</form>
<div><br/>
	<h4>Предыдущие 3 загруженных прайса</h4>
	Сортировка по дате добавления:<br/>
	<?php if(file_exists(glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old1*.csv')[0])):
		$path=glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old1*.csv')[0];
		$tmp=explode("/",$path);
		$date=str_replace(".csv","",explode("_",$tmp[count($tmp)-1])[2]);
	?>
	<a href="<?=glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old1*.csv')[0];?>">Прайс</a> от <?=$date?><br/>
	<?php endif;?>
	
	<?php if(file_exists(glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old2*.csv')[0])):
		$path=glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old2*.csv')[0];
		$tmp=explode("/",$path);
		$date=str_replace(".csv","",explode("_",$tmp[count($tmp)-1])[2]);
	?>
	<a href="<?=glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old2*.csv')[0];?>">Прайс</a> от <?=$date?><br/>
	<?php endif;?>
	
	<?php if(file_exists(glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old3*.csv')[0])):
		$path=glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old3*.csv')[0];
		$tmp=explode("/",$path);
		$date=str_replace(".csv","",explode("_",$tmp[count($tmp)-1])[2]);
	?>
	<a href="<?=glob(Yii::app()->user->model->clinic->uploadPath . '/services/current_old3*.csv')[0];?>">Прайс</a> от <?=$date?><br/>
	<?php endif;?>
</div>
<hr />
<?php
echo CHtml::link('Добавить новую услугу', array('addNew'), array('class' => 'btn'));
?>
<?php

if($ServicesRequest):?>
<div id="ServicesRequest">
<h4>Услуги на модерации</h4>
	<table>
		<thead>
			<tr>
				<?php /*?><td>Профиль</td><?php*/?>
				<td style="width: 200px;">Раздел</td>
				<td>Услуга</td>
				<td>Статус</td>
				<td style="width: 80px;text-align: right">Стоимость</td>
				<td style="width: 80px;text-align: center">Причина отклонения</td>
			</tr>
		</thead>
	<?php foreach ($ServicesRequest as $row):?>
	<tr>
		<?php /*?><td><?php echo $row['ServiceSectionName']?></td><?php*/?>
		<td><?php echo $row['ServiceSectionName']?></td>
		<td><?php echo $row['ServiceName']?></td>
		<td><?php 
		switch($row['Status']) {
			case 1: echo '<span class="service_declined">Отклонена</span>';break;
			case 2: echo '<span class="service_accepted">Подтверждена</span>';break;
			default: echo '<span class="service_moderate">На модерации</span>';
		}
			?></td>
		<td style="text-align: right"><span name="AddressServices[<?php echo $row['id']?>]"><?php echo $row['Price']?></span></td>
		<td style="width: 80px;text-align: left;padding:2px;"><?=$row['Reject']?></td>
	</tr>
	<?php endforeach;?>
	</table>
</div>
<?php endif; ?>
<h4 class="acting">Действующие услуги</h4>
<table id="AddressServices">
	<thead>
		<tr>
			<td>Профиль</td>
			<td>Раздел</td>
			<td>Услуга</td>
			<td style="width: 80px;text-align: right">Стоимость</td>
		</tr>
	</thead>
<?php foreach ($rows as $row):?>
<tr>
	<td><?=$row['ServiceSectionName']?></td>
	<td><?=$row['CompanyActiviteName']?></td>
	<td>
		<?=($row['link']=='first')? ('<b>'.$row['ServiceName'].'</b>'):$row['ServiceName'];?> 
		<img title="Удалить услугу" class="delete_price" src="/shared/images/delete.png"/>
	</td>
	<td style="text-align: right"><span name="AddressServices[<?php echo $row['id']?>]"><?php echo $row['Price']?></span>
		<img title="Редактировать цену" class="update_price" src="/shared/images/update.png"/>		
	</td>
</tr>
<?php endforeach;?>
</table>
<script type="text/javascript">
<!--
$(function () {
    $('#AddressServices span').live('click', function () {
        var input = $('<input />', {'type': 'text', 'name': $(this).attr('name'), 'value': $(this).html()});
		$(this).parent().find("img").hide();
        $(this).parent().prepend(input);		
        $(this).remove();
        input.focus();
    });

    $('#AddressServices input').live('blur', function () {
        $(this).parent().prepend($('<span />').attr('name', $(this).attr('name')).html($(this).val()));
		$(this).parent().find("img").show();
        $.ajax({
        	type: "POST",
        	url: '/adminClinic/services/ajaxUpdate',
        	data: $(this).attr('name') + '=' + $(this).val(),
        });
        $(this).remove();
    });
});
//-->
</script>
<?php
/*$this->widget('ext.htmlTableUi.htmlTableUi',array(
		'ajaxUrl'=>$this->createUrl('services/handleHtmlTable'),
		'arProvider'=>'',
		'collapsed'=>false,
		'columns'=>array('Профиль', 'Раздел', 'Услуга', 'Стоимость'),
		'cssFile'=>'',
		'editable'=>true,
		'enableSort'=>true,
		'exportUrl'=>$this->createUrl('services/exportTable'),
		//'extra'=>'Additional Information',
		//'footer'=>'Total rows: '.count($rows).' By: José Rullán',
		'formTitle'=>'Изменение стоимости услуги',
		'rows'=>$rows,
		'sortColumn'=>1,
		'sortOrder'=>'desc',
		//'subtitle'=>'Rev 1.3.5',
		'title'=>'Услуги',
));
*/
?>