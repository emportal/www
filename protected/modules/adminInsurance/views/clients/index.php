<script>
	$(".all_clients .client").live('click',function() {
		$(this).hide();
		$(this).removeClass("searchReady");
		var cid = $(this).attr('data-cid');
		$.ajax({
                url: '/adminInsurance/ajax/addClient',
                data: ({
                    'companyId': cid
                }),
                dataType: 'JSON',
                type: "GET",
                success: function(response) {                    
                   console.log(response);
                },
        });
		$(".checked_clients").append('<div data-cid="'+cid+'" class="pclient">'+$(this).html()+'</div>');
	});
	$(".checked_clients .pclient").live('click',function() {	
		
		var cid = $(this).attr('data-cid');
		$.ajax({
                url: '/adminInsurance/ajax/removeClient',
                data: ({
                    'companyId': cid
                }),
                dataType: 'JSON',
                type: "GET",
                success: function(response) {                    
                   console.log(response);
                },
        });
		$(".client[data-cid="+cid+"]").show();
		$(".client[data-cid="+cid+"]").addClass("searchReady");
		$(this).remove();
	});
	var clients = [<?php foreach($companies as $c): echo "'".str_replace("'", "\'", $c->name)."',"; endforeach; ?>];
	
	$(document).ready(function() {
		
		document.getElementById("searchClients").oninput = function() {			
			var txt  = $("#searchClients").val().toLowerCase();
			
			if(txt) {		
				setTimeout(function() {					
					if(txt == $("#searchClients").val().toLowerCase()) {						
						$(".searchReady").hide();
						$(".searchReady").each(function() {					
							var rowText = $(this).find("span").html().toLowerCase();
							var ind = rowText.indexOf(txt);
							if(ind != -1) {
								$(this).show();
							} 
						});
					}
				},250);				
			} else {
				$(".searchReady").show();
			}
		}
	});
	
</script>
<h3>Выберите клиентов:</h3>

<div class="flash-notice" style="width:325px;">
	Список клиентов сохраняется автоматически
</div>
<input placeholder="Введите название компании или ее часть" type="text" id="searchClients" class="custom-text searchClients">
<div class="clients">
	<div class="all_clients">
		<?php foreach($companies as $c): ?>
		<div data-cid="<?=$c->link?>" <?= in_array($c->id, $ids) ? 'class="client" style="display:none"' : 'class="client searchReady" '?>>
			<?php if ( $c->logo ): ?>
				<?= CHtml::image($c->logoUrl, $c->name, array('class' => 'insuranceClientsLogo')) ?>
			<? else: ?>
				<?= CHtml::image($this->assetsImageUrl.'/clinic_no_logo.jpg', '', array('class' => ' insuranceClientsLogo')) ?>
			<? endif; ?>
			<span><?=$c->name?></span>
			</div>
		<?php endforeach;?>
	</div>
	<div class="checked_clients">
		<?php if($pcompanies): ?>
			<?php foreach($pcompanies as $c): ?>
				<div data-cid="<?=$c->link?>" class="pclient">
					<?php if ( $c->logo ): ?>
						<?= CHtml::image($c->logoUrl, $c->name, array('class' => 'insuranceClientsLogo')) ?>
					<? else: ?>
						<?= CHtml::image($this->assetsImageUrl.'/clinic_no_logo.jpg', '', array('class' => ' insuranceClientsLogo')) ?>
					<? endif; ?>
					<?=$c->name?>
				</div>
			<?php endforeach;?>
		<?php endif; ?>
	</div>	
	<div class="both"></div>
</div>
