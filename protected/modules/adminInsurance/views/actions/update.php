<? /* @var $model Action */ ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'action-edit-form',
	//'action'				 => $this->createUrl('user/profile'),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array(
		//'class'		 => 'zakaz',
		'class'=>'admin-form',
		'enctype'	 => 'multipart/form-data'),
		)
);
/* @var $form CActiveForm */
?>

<table class="search-result-table">
	<tr class="search-result-box">
		<td class="search-result-signup size-14">Наименование акции *</td>
		<td class="search-result-info">
			<?= $form->textField($model, 'name', array('class' => 'custom-text w450')); ?>
			<?= $form->error($model, 'name');?>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14">Акция активна</td>
		<td class="search-result-info">
			<?= $form->checkBox($model, 'publish', array('class' => '')); ?>
			<?= $form->error($model, 'publish');?>
		</td>
	</tr>

	<tr class="search-result-box">
		<td class="search-result-signup size-14">Профиль деятельности *</td>
		<td class="search-result-info">
			<?= $form->dropDownList($model, 'companyActivitesId', CHtml::listData($clinic->addressActivites, 'id', 'name'), array('empty'=>'(выбрать)', 'style'=>'width:292px;', 'onchange'=>'js:$( "#' . CHtml::activeId($model, 'serviceId') . ', #Action_serviceName" ).removeAttr("value");if(this.value) {jQuery("#DIVAction_companyActivitesId").show();}else {jQuery("#DIVAction_companyActivitesId").hide();}')); ?>
			<?= $form->hiddenField($model, 'serviceId') ?>
			<?= $form->error($model, 'companyActivitesId');?>
		</td>
		</tr>
		<tr class="search-result-box" id="DIVAction_companyActivitesId"<?php if(empty($model->companyActivitesId)):?> style="display:none"<?php endif;?>>
		
			<td class="search-result-signup size-14">Услуга</td>
			<td class="search-result-info">
			<?php
			$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
				'model'			 => $model,
				'name'			 => 'serviceName',
				'id'			 => 'Action_serviceName',
				'value'			 => $model->service->name,
				'source'		 => 'js:function (request, response) {
					 $.ajax({
						type: "GET",
						url: "/adminClinic/ajax/ServiceByServiceActivite",
						dataType: "json",
						data: {
							"json": true,
							"link": $( "#' . CHtml::activeId($model, 'companyActivitesId') . '" ).val(),
							term: request.term
						},
						success: response
						});
				}',
				// additional javascript options for the autocomplete plugin
				'options'		 => array(
					'minLength'	 => '0',
					'change'	 => 'js:function( event, ui ) {					
						if ( ui.item ) {
							$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( ui.item.id ).change();
							return true;
						}
						else {							
							$(this).val( "" );							
							$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( "" ).change();
							return false;
						}
						}',
					'select'	 => 'js:function( event, ui ) {					
						if ( ui.item ) {
							$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( ui.item.id ).change();
							return true;
						}
						else {							
							$(this).val( "" );							
							$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( "" ).change();
							return false;
						}
						}'
				),
				'htmlOptions'	 => array(
					'class' => 'custom-text w450',					
				),
			));
			?>
			</td>
			<?//= $form->dropDownList($model, 'serviceId', CHtml::listData($clinic->addressServicesName, 'id', 'name'), array('empty'=>'(выбрать)')); ?>

	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14">Период проведения</td>
		<td class="search-result-info">
			<?= $form->radioButtonList($model, 'unlimited', array('1'=>'Бессрочная','0'=>'Временная'), array('container'=>false, 'class'=>'action_limit_choise'))?>
			<p class="f-right" id="time" style="<?//= !$model->unlimited ? 'display:block' : 'display:none'?>">
				<label>c</label>
				<?php
				$form->widget('zii.widgets.jui.CJuiDatePicker', array(
					'model'			 => $model,
					'attribute'		 => 'start',
					'language'		 => 'ru',
					'htmlOptions'	 => array('class' => 'custom-text w80'),
					'options'		 => array(
						'changeMonth'	 => true,
						'changeYear'	 => true,
						'altFormat'		 => "dd.mm.yy",
						'dateFormat'	 => "yy-mm-dd",
						'yearRange'		 => '-0:+3',
					),
						)
				);
				?>
				<label>по</label>
				<?php
				$form->widget('zii.widgets.jui.CJuiDatePicker', array(
					'model'			 => $model,
					'attribute'		 => 'end',
					'language'		 => 'ru',				
					'htmlOptions'	 => array('class' => 'custom-text w80'),
					'options'		 => array(
						'changeMonth'	 => true,						
						'changeYear'	 => true,
						'altFormat'		 => "dd.mm.yy",
						'dateFormat'	 => "yy-mm-dd",
						'yearRange'		 => '-0:+3',
					),
						)
				);
				?>
			</p>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14">Тип акции</td>
		<td class="search-result-info">
			<?= $form->radioButtonList($model, 'type', array('0'=>'Бесплатно','1'=>'Скидка', '2'=>'Подарок'), array('container'=>false, 'class'=>'action_type_choise'))?>
			<?= $form->error($model, 'type');?>
		</td>
	</tr>
	<tr class="search-result-box present">
		<td class="search-result-signup size-14">Подарок</td>
		<td class="search-result-info">
			<?= $form->textField($model, 'present', array('class' => 'custom-text w450')); ?>
			<?= $form->error($model, 'present');?>
	</tr>
	<tr class="search-result-box price">
		<td class="search-result-signup size-14">Стоимость до</td>
		<td class="search-result-info">
			<?= $form->textField($model, 'priceOld', array('class' => 'custom-text')); ?>
			<?= $form->error($model, 'priceOld');?>
	</tr>
	<tr class="search-result-box price">
		<td class="search-result-signup size-14">Стоимость по скидке</td>
		<td class="search-result-info">
			<?= $form->textField($model, 'priceNew', array('class' => 'custom-text')); ?>
			<?= $form->error($model, 'priceNew');?>
		</td>
	</tr>
	<tr class="search-result-box percent">
		<td class="search-result-signup size-14">Размер скидки в %</td>
		<td class="search-result-info">
			<?= $form->textField($model, 'discount', array('class' => 'custom-text')); ?>
			<?= $form->error($model, 'discount');?>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-info" colspan="2">
			<?= $form->textArea($model, 'description') ?>
			<?= $form->error($model, 'description');?>
		</td>
	</tr>
<!--	<tr class="search-result-box">
		<td class="search-result-signup size-14">Изображение (300х120)</td>
		<td class="search-result-info">
			<?//= CHtml::fileField('image')?>
			<input type="text" value="" class="custom-text" name="image"> <a class="btn" title="Добавить" href="#">Добавить</a>
		</td>
	</tr>-->
	<tr class="search-result-box">
		<td class="search-result-signup size-14"></td>
		<td class="search-result-info">
			<a class="btn-green" title="Отменить" href="<?= $this->createUrl('actions/index')?>">Отменить</a>
			<button type="submit" title="Сохранить" class="btn-green">Сохранить</button>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>
<script type="text/javascript">

function checkActionForm(){
	if($('#AgrAction_type_0').is(':checked')) {
		$('.price').hide();
		$('.present').hide();
		$(".percent").hide();
	}
	else if($('#AgrAction_type_1').is(':checked')){
		$('.present').hide();
		$('.price').hide();
		$(".percent").show();
		if($("#AgrAction_serviceId").val()) {
			$('.price').show();
		}

	}
	else {
		$('.price').hide();
		$('.present').show();
		$(".percent").hide();
	}
}

$(function(){
	document.getElementById('Action_serviceName').oninput = function() {		
		if($("#AgrAction_serviceName").val()) {
			if($('#AgrAction_type_1').is(':checked')){
				if(!$("#AgrAction_type_1").val())
					$('.price').hide('fast');
				else 
					$('.price').show('fast');
			}			
		} else {
			$('.price').hide('fast');
		}		
	}
	
	
	$(".action_type_choise").live('change', checkActionForm);
	$(".ui-autocomplete").live('click',function() {
		if($("#AgrAction_serviceName").val()) {
			if($('#AgrAction_type_1').is(':checked')){
				if(!$("#AgrAction_type_1").val())
					$('.price').hide('fast');
				else 
					$('.price').show('fast');
			}			
		} 
	});
	$("#AgrAction_serviceId").change(function() {
		if($('#AgrAction_type_1').is(':checked')){
			if(!$(this).val())
				$('.price').hide();
			else 
				$('.price').show();
		}
	});
	
	checkActionForm();
	
});
</script>