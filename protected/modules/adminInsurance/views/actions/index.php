<div class="flash-error">Внимание! Внесенные изменения появятся на сайте после подтверждения администратором</div>
<?php
/*if ( $model->isMain )*/
	echo CHtml::link('Добавить', array('actions/add'), array('class' => 'btn'));

echo CHtml::tag('p', array(), Yii::app()->user->getFlash('actions_add'));

?>

<br/><br/>
<?php if($AGRdataProvider->getData()):?>
<h4>Неподтвержденные</h4> 
<table class="service-table">
	<tr>
		<th>Наименование акции</th>
		<th>Статус</th>
		<th>Тип</th>
		<th>Действие</th>		
	</tr>
<?php foreach($AGRdataProvider->getData() as $row):?>
	<tr>
		<td><?=$row->name?></td>
		<td><?=$row->publish? "Активно":"Не активно"?></td>
		<td><?=($row->type==1) ? "Скидка": (($row->type==2) ? "Подарок": "Бесплатно")?></td>
		<td><?=CHtml::Link("Удалить",array("actions/agrDelete","link"=>$row->link))?></td>
	</tr>	
<?php endforeach; ?>
</table>
<?php/*
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'	 => $AGRdataProvider,
	'summaryText'	 => '',
	'itemsCssClass'	 => 'service-table',
	'enableSorting'	 => false,
	'columns'		 => array(
		'name',
		array(
			'name'	 => 'Статус',
			'value'	 => '$data->publish ? "Активно":"Не активно"',
		),
		array(
			'name'	 => 'Тип',
			'value'	 => '(($data->type==1) ? "Скидка": (($data->type==2) ? "Подарок": "Бесплатно"))',
		),
		array(
			'header'	 => 'Действие',
			'class'		 => 'CButtonColumn',
			'template'	 => '{delete}',
			'buttons'	 => array(				
				'delete' => array(
					'label'		 => 'Удалить',
					'imageUrl'	 => false,
					'url' =>  'CHtml::normalizeUrl(array("actions/agrDelete","link"=>$data->link))',
				),
			),
		),
	),
));*/
?><?php endif; ?>
<h4>Подтвержденные</h4> 

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'	 => $dataProvider,
	'summaryText'	 => '',
	'itemsCssClass'	 => 'service-table',
	'enableSorting'	 => false,
	'columns'		 => array(
		'name',
		array(
			'name'	 => 'Статус',
			'value'	 => '$data->publish ? "Активно":"Не активно"',
		),
		array(
			'name'	 => 'Тип',
			'value'	 => '(($data->type==1) ? "Скидка": (($data->type==2) ? "Подарок": "Бесплатно"))',
		),
		array(
			'header'	 => 'Действие',
			'class'		 => 'CButtonColumn',
			'template'	 => '{update}&nbsp;/&nbsp;{delete}',
			'buttons'	 => array(
				'update' => array(
					'label'		 => ' 	Редактировать',
					'imageUrl'	 => false,
					'url' =>  'CHtml::normalizeUrl(array("actions/update","link"=>$data->link))',
				),
				'delete' => array(
					'label'		 => 'Удалить',
					'imageUrl'	 => false,
					'url' =>  'CHtml::normalizeUrl(array("actions/delete","link"=>$data->link))',
				),
				'change' => array(
					'label'		 => 'Статус',
					'imageUrl'	 => false,
					'url' =>  'CHtml::normalizeUrl(array("actions/statusChange","link"=>$data->link))',
				),
			),
		),
	),
));
?>
