<?php
/* @var $this Controller */
/* @var $model Clinic */
/* @var $company Company */
?>

<?php
Yii::app()->clientScript->
registerPackage('jquery.maskedinput')->registerScript('form',
'
	$.mask.definitions["~"]="[2]";	
	$.mask.definitions["X"]="[01]";	
	$.mask.definitions["D"]="[0123]";
	$("#Company_dateOpening").mask("~999");	
	$("#licenseKindOfActivity").mask("D9.X9.~999",{placeholder:"_"});
	$("input[name=delete]").live("change",function() {
		if (confirm("Вы уверены что хотите удалить логотип?")) 
		{
			jQuery("#company-info-logo").submit(); 
		} else {
			$(this).attr("checked",false);
		}
	});
', CClientScript::POS_READY);
Yii::app()->clientScript->registerScript('info', '
	var temp_data;
	$("#company-licenses a.add_cl").live("click", function(){
		$("#licenseNumber").removeClass("error");
		$("#licenseKindOfActivity").removeClass("error");
		var err=0;
		var licenseNum = $("#licenseNumber").val().trim();
		$(\'input[id^="Clinic_companyLicenses"]\').each(function() {
			if(licenseNum == $(this).val()) {
				err = 1;
			}			
		});
		if ( $("#licenseNumber").val()  ) { /* && $("#licenseKindOfActivity").val() */
			/*
			$("#licenseKindOfActivity").val().split(".").forEach(function(v,i) { 
				if(parseInt(v) <= 0) err=1;
				if(i==2 && parseInt(v) < 1900) err=1;
				if(i==0 && parseInt(v) > 31) err=1;
				if(i==1 && parseInt(v) > 12) err=1;
			});
			*/
			if(err) {
				$("#licenseKindOfActivity").addClass("error");
				$("#licenseNumber").addClass("error");
			} else {
			
				var body = $(this).closest("tbody");
				var count = body.find("tr").length-2;
				var numberId = "' . CHtml::activeId($model, "companyLicenses[][number]") . '";
				var numberName = "' . CHtml::activeName($model, "companyLicenses[][number]") . '".replace("[]","["+count+"]");
				var kindOfActivityId = "' . CHtml::activeId($model, "companyLicenses[][kindOfActivity]") . '";
				var kindOfActivityName = "' . CHtml::activeName($model, "companyLicenses[][kindOfActivity]") . '".replace("[]","["+count+"]");
				body.append("\
				<tr>\
					<td>\
						<input type=\'hidden\' value=\'" + $("#licenseNumber").val() + "\' id=\'" + numberId + "\' name=\'" + numberName + "\'>\
						" + $("#licenseNumber").val() + "\
					</td>\
					<td>\
						<input type=\'hidden\' value=\'" + $("#licenseKindOfActivity").val() + "\' id=\'" + kindOfActivityId + "\' name=\'" + kindOfActivityName + "\'>\
						" + $("#licenseKindOfActivity").val() + "\
					</td>\
					<td></td>\
					<td>\
						<a class=\'del_cl btn-red admin_button\'>Удалить</a>\
					</td>\
				</tr>"
				);
				ajaxUpdateLicenses();
			}
		} else {
			if(!$("#licenseNumber").val())
				$("#licenseNumber").addClass("error");
			/*if(!$("#licenseKindOfActivity").val())
				$("#licenseKindOfActivity").addClass("error");*/
		}
		
		return false;
	});

	$("#company-licenses a.del_cl").live("click", function() {
		$(this).closest("tr").remove();
		
		ajaxUpdateLicenses(false);	
		
		return false;
	});
	
	function ajaxUpdateLicenses(isReload) {
		if(isReload == undefined)
		 isReload = true;
		
		var url = $("#company-licenses").attr("action") + "?" + $("#company-licenses").serialize();
				$.ajax({
					url: url, 
					async: true,
					data: {
						end: "true"
					},
					type: "GET",
					success: function(response) {
						if(isReload) {						
							$("#licenseForm").html(response);
							$("#licenseKindOfActivity").mask("D9.X9.~999",{placeholder:"_"});
							$("a.imageupload-iframe").fancybox({
							//"width" : "95%",
							//"height" : "95%",
							"autoScale" : false,
							"transitionIn" : "none",
							"transitionOut" : "none",
							//"type" : "iframe"
							}); 
     
						}
					}				
				});
	}
	
	$(".licenseUpd span").live("click", function () {
		temp_data = $(this).html();
		var span = $(this);
        var input = $("<input />", {"type": "text", "class": "upd", "name": $(this).attr("name"), "value": $(this).html()});		
        span.parent().prepend(input);
		if(span.parent().hasClass("date")) {		
			span.parent().find("input").mask("D9.X9.~999",{placeholder:"_"});
		}
        span.remove();
		
        input.focus();
    });

    $(".licenseUpd input.upd").live("blur", function () {		
        var error=0,err=0;	
		console.log($(this).val());
		if($(this).parent().hasClass("date")) {
			if($(this).val() != "__.__.____") {
				$(this).val().split(".").forEach(function(v,i) { 				
						if(parseInt(v) > 0) {
							if(i==2 && parseInt(v) < 1900) { err=1; }
							if(i==0 && parseInt(v) > 31) { err=1; }
							if(i==1 && parseInt(v) > 12) { err=1; }
						}
				});
				if(!$(this).val().match("^[0-9]{2}\.[0-9]{2}\.[2]{1}[0-9]{3}$") || err == 1) {
					$(this).val(temp_data);			
					error=1;
				}
			} else {
				$(this).val("");
			}
		} else {
			if($(this).val().trim()=="") {
				$(this).val(temp_data);			
				error = 1;
			} 
		}
		
		$(this).parent().prepend($("<span />").attr("name", $(this).attr("name")).html($(this).val().trim()));
		$(this).parent().find("input").not(".upd").val($(this).parent().find("span").html());        
        $(this).remove();
		if(!error)
			ajaxUpdateLicenses(false);
    });
	', CClientScript::POS_READY);
?>
<div class="flash-error">Внимание! Внесенные изменения появятся на сайте после подтверждения администратором</div>
<?php if ( Yii::app()->user->hasFlash('success') ): ?>
<div class="flash-success">
	<span><?php echo Yii::app()->user->getFlash('success')?></span>
</div>
<?php endif;?>
<?php if ( $model->isMain) : ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-info-logo',
	'action'				 => $this->createUrl('default/index'),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data', 'class'=>'admin-form'),
		));
/* @var $form CActiveForm */
?>
<table class="search-result-table">
	<tbody>
		<tr class="search-result-box odd">
			<td class="search-result-info">
				
					
					<table>
						<tr>
							<td>
							<h4>Логотип</h4>

							<?php if ( $company->logo ): ?>
								<?= CHtml::image($company->logoUrl . '?' . time(), $company->name, array('class' => 'company-logo')) ?>
								<br />
								<input type="checkbox" name="delete" id="deleteImg"><label for="deleteImg">Удалить изображение</label>
							<? else: ?>
							<small class="label-comment">
							Не более 200px в ширину<br/>
							Форматы файла: jpg, png, gif
							</small>
								<?= CHtml::image(Yii::app()->baseUrl . 'images/1x1.png', '', array('class' => 'company-logo default')) ?>
							<? endif; ?>
							</td>
							<td style="padding-left: 50px;vertical-align:middle"><?= $form->filefield($company, 'logo', array('onchange'=>'jQuery("#company-info-logo").submit();')) ?></td>
						</tr>
					</table>
					

			</td>


		</tr>

	</tbody>
</table>
<?php $this->endWidget();?>
<? endif; ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-info',
	'action'				 => $this->createUrl('default/index'),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data', 'class'=>'admin-form'),
		));
/* @var $form CActiveForm */
?>
<table class="search-result-table">
	<tbody>
	<?php /*?>
		<tr class="search-result-box odd">
			<td class="search-result-info">
				<?php if ( $company->logo ): ?>
					<?= CHtml::image($company->logoUrl, $company->name, array('class' => 'company-logo')) ?>
				<? else: ?>
					<?= CHtml::image(Yii::app()->baseUrl . 'images/1x1.png', '', array('class' => 'company-logo default')) ?>
				<? endif; ?>
			</td>
			<td class="search-result-info">
				<?php if ( $model->isMain ) : ?>
					<br>
					<?php if ( $company->logo ): ?>
						<div class="file-over v-middle">
							<a class="btn" href="#">Изменить</a>
							<?= $form->fileField($company, 'logo') ?>
						</div>
						<input type="checkbox" style="display:none;" name="delete" id="deleteImg">
						<label for="deleteImg" class="btn v-middle">Удалить</label>
					<? else: ?>
						<div class="file-over">
							<a class="btn" href="#">Загрузить логотип</a>
							<?= $form->fileField($company, 'logo') ?>
						</div>
					<? endif; ?>
				<? endif; ?>
		</tr>
		<?*/?>
		<tr class="search-result-box">
			<td colspan="2" class="search-result-info">
			<?php if($model->isMain):?>
				<?php echo $form->textArea($company, 'description', array('class'=> 'custom-textarea')) ?>
				<?php echo $form->error($company, 'description');?>
			<?php else :?>
				<smal class="label-comment">Если не заполнено, отображается текст главного адреса</smal>
				<?php echo $form->textArea($model, 'description', array('class'=> 'custom-textarea')) ?>
				<?php echo $form->error($model, 'description');?>
			<?php endif;?>
				
			</td>
		</tr>

		<tr class="search-result-box odd">
			<td class="search-result-signup size-14">Год основания
			<br />
			<smal class="label-comment">Редактируется главным адресом</smal>
			</td>
			<td class="search-result-info">
				<?= $form->textField($company, 'dateOpening', array('class'		 => 'custom-text', 'readonly'	 => !$model->isMain)) ?>
				<?php echo $form->error($company, 'dateOpening');	?>
			</td>
		</tr>
		<tr class="search-result-box odd">
			<td class="search-result-signup size-14">Дополнительно
			<br />
			<smal class="label-comment">Редактируется главным адресом</smal>
			</td>
			<td class="search-result-info">
				<?php if($model->isMain):?>
					<?php echo $form->checkBox($company, 'IsOMS')?> Клиника работает с ОМС <br />
					<!--<?php echo $form->checkBox($company, 'IsDMS')?> Клиника работает с ДМС <br />-->
				<?php else :?>
					<?php echo $form->checkBox($company, 'IsOMS', array('disabled'=>'disabled'))?> Клиника работает с ОМС <br />
					<!--<?php echo $form->checkBox($company, 'IsDMS', array('disabled'=>'disabled'))?> Клиника работает с ДМС <br />-->
				<?php endif;?>
			</td>
		</tr>

		<tr class="search-result-box">
			
			<td class="search-result-info" colspan="2" style="text-align: center;">
				<?php //if ( $model->isMain ) : ?>
					<a class="btn-red" title="Отменить" href="">Отменить</a>
					<button class="btn-green" type="submit" title="Сохранить">Сохранить</button>
				<? //endif; ?>
			</td>
		</tr>
	</tbody>
</table>
<?php
$this->endWidget(); ?>
<div id="licenseForm">
	<?php
	$this->renderPartial('licenseForm', array(
		'model' => $model,
		'company' => $company
	));
	?>
</div>
