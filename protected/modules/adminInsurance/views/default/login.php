<?php
/* @var $this Controller */
$this->breadcrumbs = array(
	'Вход на сайт',
);

Yii::app()->clientScript->registerScript('login-page', '
	$("html, body").animate({
    scrollTop: $("div.search-result").offset().top
}, 100);
', CClientScript::POS_READY)
?>

<div class="cont">
	<?php if ( Yii::app()->user->hasFlash('error') ): ?>
	<div class="flash-error">
		<span><?php echo Yii::app()->user->getFlash('error')?></span>
	</div>
	<?php endif;?>
	<div class="middle-auth w500">

			<!--<p class="error">Пароль введен не верно! Попробуйте еще раз.</p>-->
		<?php
		/* @var $form CActiveForm */
		$form	 = $this->beginWidget('CActiveForm', array(
			'id'					 => 'login-form',
			'enableAjaxValidation'	 => true,
		));
		?>
		<?php if ( $error	 = $model->getError('password') ): ?>
			<p class="error"><?= $error ?></p>
		<? endif; ?>
		<fieldset>
			<div class="auth-field">
			<label>Логин</label>
				<?php
					echo $form->textField($model, 'username', array('class' => 'custom-text'));
				?>
				<!--<a title="зарегистрироваться" href="/register" name="">зарегистрироваться</a>-->
			</div><!-- /field -->
			<div class="auth-field">
			<label>Пароль</label>
				<?php
					echo $form->passwordField($model, 'password', array('class' => 'custom-text'));
				?>
				<div class="align-right">
					<?php
					echo $form->checkBox($model, 'rememberMe', array('class' => 'niceCheck'));
					?>
					<label for="remember">запомнить</label>
				</div><!-- /align-right -->
			</div><!-- /field -->
			<br />
			<input id="login-button" type="submit" class="btn" value="Войти">
		</fieldset>
		<?php $this->endWidget(); ?>
	</div>
</div>
