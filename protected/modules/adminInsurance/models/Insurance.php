<?php

/**
 * This is the model class for table "address".
 *
 * The followings are the available columns in table 'address':
 * @property-read boolean $isMain Свойство показывает главное ли это отделение
 * @property-read string $uplaodPath Корневая папка для загрузки файлов
 *
 * The followings are the available model relations:
 * @property PlaceOfWork[] $currentPlaceOfWorks Текушие места работы привязанные к данной клинике
 * @property Doctor[] $doctors Врачи привязанные к данной клинике
 * @property Ref $www
 * @property Email $email
 */
class Insurance extends Address {

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserMedical the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array_merge(parent::rules(), array(
		));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array_merge(parent::relations(), array(
			'currentPlaceOfWorks'	 => array(self::HAS_MANY, 'PlaceOfWork', 'addressId', 'scopes' => 'current'),
			#'doctors'				 => array(self::HAS_MANY, 'Doctor', 'doctorId', 'through' => 'currentPlaceOfWorks'),
			'insuranceClinic'		 => array(self::HAS_MANY,'InsuranceClinicClients','insuranceId'),
			'clients'				 => array(self::HAS_MANY,'Company','companyId','through'=>'insuranceClinic', 'order'=>'clients.name ASC'),
			'www'					 => array(self::HAS_ONE, 'Ref', 'ownerId'),
			'agr_www'				 => array(self::HAS_ONE, 'AgrRef', 'ownerId'),
			'email'					 => array(self::HAS_ONE, 'Email', 'addressId'),
			'agr_email'				 => array(self::HAS_ONE, 'AgrEmail', 'addressId'),			
//			'cityDistrict' => array(self::BELONGS_TO, 'CityDistrict', 'cityDistrictId'),
//			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
//			'country' => array(self::BELONGS_TO, 'Country', 'countryId'),
//			'region' => array(self::BELONGS_TO, 'Region', 'regionId'),
//			'addressCompanyUnits' => array(self::HAS_MANY, 'AddressCompanyUnit', 'addressId'),
//			'addressServices' => array(self::HAS_MANY, 'AddressServices', 'addressId'),
//			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'addressId'),
//			'companies' => array(self::HAS_MANY, 'Company', 'addressesId'),
//			'companyLicenses' => array(self::HAS_MANY, 'CompanyLicense', 'addressesId'),
//			'contactInformations' => array(self::HAS_MANY, 'ContactInformation', 'addressesId'),
//			'contactPersonCompanies' => array(self::HAS_MANY, 'ContactPersonCompany', 'addressId'),
//			'emails' => array(self::HAS_MANY, 'Email', 'addressId'),
//			'insuranceCompanyLicenses' => array(self::HAS_MANY, 'InsuranceCompanyLicense', 'addressId'),
//			'nearestMetroStations' => array(self::HAS_MANY, 'NearestMetroStation', 'addressId'),
//			'metroStations' => array(self::MANY_MANY, 'NearestMetroStation', 'nearestMetroStation(addressId,metroStationId)'),
//			'phones' => array(self::HAS_MANY, 'Phone', 'addressId'),
//			'placeOfWorks' => array(self::HAS_MANY, 'PlaceOfWork', 'addressId'),
//			'producers' => array(self::HAS_MANY, 'Producer', 'mainAddressId'),
//			'producerOffices' => array(self::HAS_MANY, 'ProducerOffice', 'addressId'),
//			'userDrugstores' => array(self::HAS_MANY, 'UserDrugstore', 'addressId'),
//			'userInsurances' => array(self::HAS_MANY, 'UserInsurance', 'addressId'),
//			'userMedicals' => array(self::HAS_MANY, 'UserMedical', 'addressId'),
//			'userPatronages' => array(self::HAS_MANY, 'UserPatronage', 'addressId'),
//			'userShops' => array(self::HAS_MANY, 'UserShops', 'addressId'),
//			'userSuppliers' => array(self::HAS_MANY, 'UserSuppliers', 'addressId'),
//			'userTourisms' => array(self::HAS_MANY, 'UserTourism', 'addressId'),
//			'workingHours' => array(self::HAS_MANY, 'WorkingHour', 'addressId'),
//			'workingHoursOfDoctors' => array(self::HAS_MANY, 'WorkingHoursOfDoctors', 'addressId'),
		));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), array(
		));
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('ownerId', $this->ownerId, true);
		$criteria->compare('countryId', $this->countryId, true);
		$criteria->compare('postIndex', $this->postIndex, true);
		$criteria->compare('regionId', $this->regionId, true);
		$criteria->compare('cityId', $this->cityId, true);
		$criteria->compare('cityDistrictId', $this->cityDistrictId, true);
		$criteria->compare('street', $this->street, true);
		$criteria->compare('houseNumber', $this->houseNumber, true);
		$criteria->compare('longitude', $this->longitude);
		$criteria->compare('latitude', $this->latitude);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('lunchBreak', $this->lunchBreak);
		$criteria->compare('breakStart', $this->breakStart, true);
		$criteria->compare('breakFinish', $this->breakFinish, true);
		$criteria->compare('forCompany', $this->forCompany);
		$criteria->compare('forProducers', $this->forProducers);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 *
	 * @return boolean
	 */
	public function getIsMain() {
		return $this->id === $this->company->addressId;
	}

	public function getUploadPath() {
		return Yii::getPathOfAlias('uploads.company') . '/' . $this->company->link . '/address/' . $this->link;
	}
	
	public function getUploadPathForUser() {	
		return  '/uploads/company/' . $this->company->link . '/address/' . $this->link;
	}

}