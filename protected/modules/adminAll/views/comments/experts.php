<?php if ( Yii::app()->user->hasFlash('success') ): ?>
<div class="flash-success">
	<span><?php echo Yii::app()->user->getFlash('success')?></span>
</div>
<?php endif;?>
<? /**/ ?>
<div class="flash-notice">
	Вы можете написать ответ на отзыв. Публикацию отзыва выполняет администратор портала.
</div>
<?= CHtml::link('Отзывы о клинике', array('comments/index'), array('class'=> 'btn clinic'))?>
&nbsp;
<?= CHtml::link('Отзывы о врачах', array('comments/experts'), array('class'=> 'btn doctor'))?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dataProvider,
		'summaryText' => '',
		'itemsCssClass'=>'service-table',
		'columns'=>array(

				array(
						'name'=>'Дата',
						'value'=>'Yii::app()->dateFormatter->formatDateTime($data->date, "medium", null);',
						'type'=>'html',

				),
				/*array(
						'name'=>'Текст',
						'value'=>'$data->content',
				),*/
				array(
						'class'=>'CLinkColumn',
						'urlExpression'=>'Yii::app()->controller->createUrl("viewExpert", array("id"=>$data->id))',
						'header'=>'Текст отзыва',
						'labelExpression'=>'$data->review',

				),
				array(
				 		'name'=>'Ответ клиники',
						'value'=>'$data->answer ? "да" : "нет"',
				),
				array(
				 		'name'=>'Статус публикации',
						'value'=>'$data->status ? "да" : "нет"',
				),
		),
));
?>