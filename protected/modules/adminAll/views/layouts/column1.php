<?php /* @var $this adminAllController */ 
 
$countRegistry=Yii::app()->db->createCommand()->select("COUNT(*)")
	->from(AppointmentToDoctors::model()->tableName())
	->where("addressId=:aId AND statusId=".AppointmentToDoctors::SEND_TO_REGISTER,array(':aId'=>Yii::app()->user->model->addressId))
	->queryScalar();

?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="wrapper clinic">
	<?php $this->renderPartial('/layouts/_header_nulled'); ?>
	<section class="middle">
		<div class="container">
			<div class="search-result cabinet">
				<div class="crumbs">
					<?php
					$this->widget('Breadcrumbs', array('links' => $this->breadcrumbs));
					?>
				</div>
				<?php /*?>
				<div class="search-result-head">
					<span class="f-left">
						<h4><?= $this->loadModel()->getName()?></h4>
					</span>
					<span class="f-right">
						<?= $this->loadModel()->company->companyContracts ?
							"Договор №{$this->loadModel()->company->companyContracts->contractNumber} до ". Yii::app()->dateFormatter->formatDateTime($this->loadModel()->company->companyContracts->periodOfValidity) :
							'Нет данных о контрактах'?>
					</span>
					<?php echo $this->clips['head_content']; ?>
				</div>
				<?*/?>
				<table class="search-result-table">
					<tbody>
						<tr class="search-result-box menu">
							<td class="search-result-signup">
								<?php	
								/*
								$this->widget('zii.widgets.CMenu', array(
									'items'			 => $this->module->moduleMenu,
									'itemCssClass'	 => 'btn w100',
									'lastItemCssClass'	 => 'iframe',
									'htmlOptions' => array('style'=>'margin: 0; padding: 0;'),
									)
								)
								 */
								?>
								<ul style="margin: 0; padding: 0;min-width:140px;">
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/default/index'?'active ':'';?>btn w100"><a href="/adminAll/default/index">Описание</a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/default/contacts'?'active ':'';?>btn w100"><a href="/adminAll/default/contacts">Контакты</a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/services/index'?'active ':'';?>btn w100"><a href="/adminAll/services/index">Услуги</a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/experts/index'?'active ':'';?>btn w100"><a href="/adminAll/experts/index">Специалисты</a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/default/visits'?'active ':'';?>btn w100"><a href="/adminAll/default/visits">Регистратура<?php if($countRegistry): ?><span class="badge badge-important"><?=$countRegistry?></span><?php endif; ?></a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/default/calendar'?'active ':'';?>btn w100"><a href="/adminAll/default/calendar">Календарь</a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/actions/index'?'active ':'';?>btn w100"><a href="/adminAll/actions/index">Акции</a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/comments/index'?'active ':'';?>btn w100"><a href="/adminAll/comments/index">Отзывы</a></li>
									<li class="<?=Yii::app()->urlManager->parseUrl(Yii::app()->request)=='adminAll/news/index'?'active ':'';?>btn w100"><a href="/adminAll/news/index">Новости</a></li>
									<li class="iframe btn w100"><a href="/adminAll/default/gallery">Галерея</a></li>
								</ul>
                            </td><!-- /search-result-signup -->
                            <td width="80%" class="search-result-info zakaz">
								<?php echo $content; ?>
                            </td>
                        </tr>
                    </tbody>
				</table>
			</div>
			<div class="clearfix">

			</div><!-- /clearfix -->
		</div><!-- /container-->
	</section><!-- /middle-->
</div><!-- /wrapper -->
<footer class="footer">
	<p class="copyright">© 2012-2013 все права защищены. Все права на любые материалы, опубликованные на сайте, защищены в соответствии с российским и международным<br>законодательством об авторском праве. При использовании текстовых материалов в интернете и при использовании текстов в печатных и электронных<br>СМИ ссылка на данный ресурс обязательна.</p>
</footer><!-- /footer -->
<?php $this->endContent(); ?>