<?php if (Yii::app()->user->hasFlash('success')): ?>
	<div class="flash-success">
		<span><?php echo Yii::app()->user->getFlash('success') ?></span>
	</div>
<?php endif; ?>
<? /**/ ?>
<div class="flash-notice">
	Вы можете добавить новость на сайт. Публикацию новости выполняет администратор портала.
</div>
<?= CHtml::link('Добавить новость', array('news/add'), array('class' => 'btn')) ?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	'summaryText' => '',
	'itemsCssClass' => 'service-table',
	'columns' => array(
		/* array(
		  'name'=>'Текст',
		  'value'=>'$data->content',
		  ), */
		array(
			'class' => 'CLinkColumn',
			'urlExpression' => 'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
			'header'=>'Название',
			'labelExpression' => '$data->name',
		),
		array(
			'name' => 'datePublications',
			'value' => '$data->datePublications == "0000-00-00 00:00:00" ? "Не указано" : Yii::app()->dateFormatter->formatDateTime($data->datePublications, "medium",null);',
			'type' => 'html',
		),
		array(
			'name'	=>	'publish',
			'value'	=>	'$data->publish ? "да":"нет"'
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}'
		)
	),
));
?>