<? /* @var $model Action */ ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'post',
	'id' => 'action-edit-form',
	//'action'				 => $this->createUrl('user/profile'),
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		//'class'		 => 'zakaz',
		'class' => 'admin-form',
		'enctype' => 'multipart/form-data'),
		)
);
/* @var $form CActiveForm */
?>

<?php Yii::import('ext.TDatePicker'); ?>

<table class="search-result-table">
	<!--<tr class="search-result-box">
		<td class="search-result-signup size-14"><?php echo $form->label($model, 'publish'); ?></td>
		<td class="search-result-info">
			<?php echo $form->checkbox($model, 'publish', array('style' => 'margin-top:5px;')); ?><br/>
			<?=$form->error($model,'publish');?>
		</td>
	</tr>-->
	
	<tr class="search-result-box">
		<td class="search-result-signup size-14 width100"><?php echo $form->label($model, 'name'); ?></td>
		<td class="search-result-info">
			<?php echo $form->textField($model, 'name', array('class' => 'custom-text width400')); ?><br/>
			<?=$form->error($model,'name');?>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14 width100"><?php echo $form->label($model, 'title'); ?></td>
		<td class="search-result-info">
			<?php echo $form->textField($model, 'title', array('class' => 'custom-text width400')); ?><br/>
			<?=$form->error($model,'title');?>
		</td>
	</tr>
	
	<tr class="search-result-box">
		<td class="search-result-signup size-14 width100"><?php echo $form->label($model, 'datePublications'); ?></td>
		<td class="search-result-info">
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'attribute' => 'datePublications',
				'options' => array(
					'dateFormat'	 => "yy-mm-dd",
				),				
				'htmlOptions' => array(
					'id' => 'News_Date_div',
					'class' => 'custom-text',
					'value'=>$model->datePublications == "0000-00-00 00:00:00" ? '' : $model->datePublications
				)
			));
			?><br/>
			<?=$form->error($model,'');?>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14 width100"><?php echo $form->label($model, 'preview'); ?></td>
		<td class="search-result-info">
			<?php echo $form->textArea($model, 'preview', array('class' => 'custom-text news_textarea')); ?><br/>
			<?=$form->error($model,'preview');?>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14 width100"><?php echo $form->label($model, 'Text'); ?></td>
		<td class="search-result-info">
			<?php echo $form->textArea($model, 'Text', array('class' => 'custom-text news_textarea')); ?><br/>
			<?=$form->error($model,'Text');?>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14 width100"></td>
		<td class="search-result-info">
			<?=CHtml::link("Отменить",array('index'),array(
				'class'=>'btn-red'
			));?>
			<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => $this->action->id == 'update' ? 'Сохранить' : 'Добавить', 'htmlOptions' => array('class' => 'btn-green'))); ?>
		</td>
	</tr>
</table>
<?php //echo $form->textFieldRow($model, 'Date', array('class'=>'span2'));  ?>



<?php $this->endWidget(); ?>
