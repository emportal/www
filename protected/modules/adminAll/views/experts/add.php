<?php /* @var $model Doctor */ ?>
<?php
Yii::app()->clientScript->
		registerPackage('jquery.maskedinput')->registerScript('doctor', '
				$.mask.definitions["~"]="[12]";
				$("#AgrDoctor_experience").mask("~999");
	$("#company-experts").on("click", "a.add_sod",function(){
		if ( $("#doctorSpecialtyName").val() && $("#doctorCategory").val() ) {
			var body = $(this).closest("tbody");
			var count = body.find("tr").length-2;
			var specialtyId = "' . CHtml::activeId($model, "specialtyOfDoctors[][doctorSpecialtyId]") . '";
			var specialtyName = "' . CHtml::activeName($model, "specialtyOfDoctors[][doctorSpecialtyId]") . '".replace("[]","["+count+"]");
			var categoryId = "' . CHtml::activeId($model, "specialtyOfDoctors[][doctorCategoryId]") . '";
			var categoryName = "' . CHtml::activeName($model, "specialtyOfDoctors[][doctorCategoryId]") . '".replace("[]","["+count+"]");
			body.append("\
			<tr>\
				<td>\
					<input type=\'hidden\' value=\'" + $("#doctorSpecialty").val() + "\' id=\'" + specialtyId + "\' name=\'" + specialtyName + "\'>\
					" + $("#doctorSpecialtyName").val() + "\
				</td>\
				<td>\
				<input type=\'hidden\' value=\'" + $("#doctorCategory").val() + "\' id=\'" + categoryId + "\' name=\'" + categoryName + "\'>\
					" + $("#cuselFrame-doctorCategory .cuselText").html() + "\
				</td>\
				<td>\
					<a class=\'del_sod\'>Удалить</a>\
				</td>\
			</tr>"
			);
					
			$("#doctorSpecialtyName").val("");
		}

		return false;
	});

	$("#company-experts").on("click", "a.del_sod",function(){
		$(this).closest("tr").remove();

		return false;
	});

	$("#company-experts").on("click", "a.add_de",function(){
		if ( $("#medicalSchoolName").val() && $("#typeOfEducation").val() ) {
			var body = $(this).closest("tbody");
			var count = body.find("tr").length-2;
			var medicalSchoolId = "' . CHtml::activeId($model, "doctorEducations[][medicalSchoolId]") . '";
			var medicalSchoolName = "' . CHtml::activeName($model, "doctorEducations[][medicalSchoolId]") . '".replace("[]","["+count+"]");
			var typeOfEducationId = "' . CHtml::activeId($model, "doctorEducations[][typeOfEducationId]") . '";
			var typeOfEducationName = "' . CHtml::activeName($model, "doctorEducations[][typeOfEducationId]") . '".replace("[]","["+count+"]");
			var yearId = "' . CHtml::activeId($model, "doctorEducations[][yearOfStady]") . '";
			var yearName = "' . CHtml::activeName($model, "doctorEducations[][yearOfStady]") . '".replace("[]","["+count+"]");
			body.append("\
			<tr>\
				<td>\
					<input type=\'hidden\' value=\'" + $("#medicalSchool").val() + "\' id=\'" + medicalSchoolId + "\' name=\'" + medicalSchoolName + "\'>\
					" + $("#medicalSchoolName").val() + "\
				</td>\
				<td>\
					<input type=\'hidden\' value=\'" + $("#typeOfEducation").val() + "\' id=\'" + typeOfEducationId + "\' name=\'" + typeOfEducationName + "\'>\
					" + $("#cuselFrame-typeOfEducation .cuselText").html() + "\
				</td>\
				<td>\
					<input type=\'hidden\' value=\'" + $("#year").val() + "\' id=\'" + yearId + "\' name=\'" + yearName + "\'>\
					" + $("#year").val() + "\
				</td>\
				<td>\
					<a class=\'del_de\'>Удалить</a>\
				</td>\
			</tr>"
			);
			
			
			$("#medicalSchoolName, #year").attr("value", "").removeClass("error");
					
		}
		else {
				/*if(!$("#year").val()) {
					$("#year").addClass("error");
				}*/
				if(!$("#medicalSchoolName").val()) {	
					$("#medicalSchoolName").addClass("error");
				}
		}

		return false;
	});

	$("#company-experts").on("click", "a.del_de",function(){
		$(this).closest("tr").remove();

		return false;
	});
	
	$("#company-experts").on("submit", "",function() {
		$("#doctorSpecialtyName").removeClass("error");
		$("#AgrDoctor_specialtyOfDoctors_em_").html("").hide();
		if($(".specialties tr").length<=2) {
			$("#doctorSpecialtyName").addClass("error");
			$("#AgrDoctor_specialtyOfDoctors_em_").html("Необходимо заполнить поле Специальность").show();
			return false;
		}
	});

	$.mask.definitions["~"]="[12]";
	$("#year").mask("~999");

	', CClientScript::POS_READY);
?>
<?php if ( Yii::app()->user->hasFlash('success') ): ?>
<div class="flash-success">
	<span><?php echo Yii::app()->user->getFlash('success')?></span>
</div>
<?php endif;?>

<?php if($model->scenario == 'update'):?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-experts-logo',
	'action'				 => $this->createUrl('experts/uploadPhoto', array('link'=>$model->link)),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data', 'class'=>'admin-form'),
		));
/* @var $form CActiveForm */
?>
<table class="search-result-table">
	<tbody>
		<tr class="search-result-box odd">
			<td class="search-result-info">
				
					<table>
						<tr>
							<td>
							<h4>Фотография</h4>

							<?php if ( $model->photoUrl ): ?>
								<?= CHtml::image($model->photoUrl . '?' . time(), $model->name, array('class' => 'doctor-photo')) ?>
								<br />
								<input type="checkbox" name="delete" id="deleteImg" onchange='jQuery("#company-experts-logo").submit();'><label for="deleteImg">Удалить изображение</label>
							<?php else: ?>
								<smal class="label-comment"><!--Не более 160px в ширину<br/>-->Форматы файла: jpg, png, gif </smal>
								<?= CHtml::image(Yii::app()->baseUrl . 'images/1x1.png', '', array('class' => 'doctor-photo default')) ?>							<?php endif; ?>
							</td>
							<td style="padding-left: 50px;vertical-align:middle"><?= $form->filefield($model, 'photo', array('onchange'=>'jQuery("#company-experts-logo").submit();', 'accept'=>"image/*")) ?>
							</td>
						</tr>
					</table>
					
			</td>
		</tr>

	</tbody>
</table>
<?php $this->endWidget();?>
<?php endif;?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-experts',
	//'action'				 => CHtml::normalizeUrl(array('/default/updateLogo')),
	//'enableAjaxValidation'=>true,
    //'enableClientValidation'=>true,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data', 'class'=>'admin-form'),
	'enableAjaxValidation'	 => true,
	'enableClientValidation' => false,
	'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => false,
	),
));
/* @var $form CActiveForm */
?>
<table class="search-result-table">
	<tbody>
	<?php /*?>
		<tr class="search-result-box">
			<td class="search-result-info">
				<?php if ( $model->photo ): ?>
					<?= CHtml::image($model->photoUrl, $model->name, array('class' => 'doctor-photo')) ?>
				<?php else: ?>
					<?= CHtml::image(Yii::app()->baseUrl . 'images/1x1.png', '', array('class' => 'doctor-photo default')) ?>
				<?php endif; ?>
			</td>
			<td class="search-result-info">
					<br>
					<?php if ( $model->photo ): ?>
						<div class="file-over v-middle">
							<a class="btn" href="#">Изменить</a>
							<?= $form->fileField($model, 'photo') ?>
						</div>
						<input type="checkbox" style="display:none;" name="delete" id="deleteImg">
						<label for="deleteImg" class="btn v-middle">Удалить</label>
					<?php else: ?>
						<div class="file-over">
							<a class="btn" href="#">Загрузить фото</a>
							<?= $form->fileField($model, 'photo') ?>
						</div>
					<?php endif; ?>
			</td>
		</tr>
		<?*/?>
		<tr class="search-result-box">
			<td colspan="2" class="search-result-signup"><h4>Личные данные</h4></td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Фамилия</td>
			<td class="search-result-info">
				<?= $form->textField($model, 'surName', array('class' => 'custom-text w280')) ?>
				<?php echo $form->error($model, 'surName');?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Имя</td>
			<td class="search-result-info">
				<?= $form->textField($model, 'firstName', array('class' => 'custom-text w280')) ?>
				<?php echo $form->error($model, 'firstName');?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Отчество</td>
			<td class="search-result-info">
				<?= $form->textField($model, 'fatherName', array('class' => 'custom-text w280')) ?>
				<?php echo $form->error($model, 'fatherName');?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Пол</td>
			<td class="search-result-info overflnone">
				<?=
				$form->dropDownList(
						$model, 'sexId', CHtml::listData(
								Sex::model()->findAll(), 'id', 'name'), array('class' => 'custom-select w280', 'empty'=>'(выбрать)'))
				?>
				<?php echo $form->error($model, 'sexId');?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Дата рождения</td>
			<td class="search-result-info">
				<?php
				
				if($model->birthday == "30.11.-0001" || $model->birthday == "0000-00-00 00:00:00") {
					$model->birthday = "";
				}
				$form->widget('zii.widgets.jui.CJuiDatePicker', array(
					'model'			 => $model,
					'attribute'		 => 'birthday',
					'language'		 => 'ru',
					'htmlOptions'	 => array('class' => 'custom-text w280'),
					'options'		 => array(
						'changeMonth'	 => true,
						'changeYear'	 => true,
						'altFormat'		 => "dd.mm.yy",
						'dateFormat'	 => "dd.mm.yy",
						'yearRange'		 => '-80:+0',
					),
						)
				);
				?>
				<?php echo $form->error($model, 'birthday');?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td colspan="2" class="search-result-signup"><br><h4>Опыт работы</h4></td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Стаж работы<br /><smal class="label-comment">Необходимо указать год начала карьеры</smal></td>
			<td class="search-result-info">
				<?= $form->textField($model, 'experience', array('class' => 'custom-text w280')) ?>
				<?php echo $form->error($model, 'experience');?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Описание врача</td>
			<td class="search-result-info">

				<div>

				<?= $form->textArea($model, 'description', array('class' => 'custom-text w280')) ?>
				<?php echo $form->error($model, 'description');?>

				</div>

			</td>
		</tr>
		<tr>
			<td colspan="2"><br>
			<?php echo $form->error($model, 'specialtyOfDoctors');?>
				<table class="specialties service-table">
					<tbody>
						<tr>
							<td>Специальность</td>
							<td>Квалификационная категория</td>
							<td>Действия</td>
						</tr>
						<tr>
							<td>
								<?= CHtml::hiddenField('doctorSpecialty') ?>
								<?php
								$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
									'model'			 => $model,
									'name'			 => 'doctorSpecialtyName',
									//'attribute'		 => 'city.name',
									'value'			 => '',
									'source'		 => 'js:function (request, response) {
											$.ajax({
											   url: "/ajax/doctorSpecialties",
											   dataType: "json",
												   data: {
													   term: request.term
												   },
												   success: response
											   });
									   }',
									// additional javascript options for the autocomplete plugin
									'options'		 => array(
										'minLength'	 => '0',
										'change'	 => 'js:function( event, ui ) {
							if ( ui.item ) {
								$( "#' . CHtml::getIdByName('doctorSpecialty') . '" ).val( ui.item.id );
								return true;
							}
							else {
								$(this).val( "" );
								$( "#' . CHtml::getIdByName('doctorSpecialty') . '" ).val( "" );
								return false;
							}
							}'
									),
									'htmlOptions'	 => array(
										'class' => 'custom-text',
									),
								));
								?>
							<td>
								<?= CHtml::dropDownList('doctorCategory', '', CHtml::listData(DoctorCategory::model()->findAll(), 'id', 'name'), array('class' => 'custom-select w240')) ?>
							</td>
							<td>

								<a class="btn-green add_sod" title="Добавить" href="#">Добавить</a>
							</td>
						</tr>
						<?php  foreach ($model->specialtyOfDoctors as $key => $specialty): ?>
							<tr>
								<td>
									<?= $form->hiddenField($model, "specialtyOfDoctors[{$key}][doctorSpecialtyId]") ?>
									<?= $form->hiddenField($model, "specialtyOfDoctors[{$key}][id]") ?>
									<?= $specialty->doctorSpecialty->name ?>
								</td>
								<td>
									<?= $form->hiddenField($model, "specialtyOfDoctors[{$key}][doctorCategoryId]") ?>
									<?= $specialty->doctorCategory->name ?>
								</td>
								<td>
									<!--									<a href="#">Редактировать</a>
																		/ -->
									<a class="del_sod" href="#">Удалить</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				
			</td>
		</tr>
		<tr class="search-result-box">
			<td colspan="2" class="search-result-signup"><h4>Образование</h4></td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Ученая степень</td>
			<td class="search-result-info overflnone">
				<?=
				$form->dropDownList(
						$model, 'scientificDegrees[0]', CHtml::listData(
								ScientificDegree::model()->findAll(), 'id', 'name'), array('class' => 'custom-select w240', 'empty'=>'(выбрать)'))
				?>
				<?php echo $form->error($model, 'scientificDegrees');?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Ученое звание</td>
			<td class="search-result-info overflnone">
				<?=
				$form->dropDownList(
						$model, 'scientificTitle', CHtml::listData(
								ScientificTitle::model()->findAll(), 'id', 'name'), array('class' => 'custom-select w240', 'empty'=>'(выбрать)'))
				?>
				<?php echo $form->error($model, 'scientificTitle');?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><br>
			<?php echo $form->error($model, 'doctorEducations');?>
				<table class="service-table">
					<tbody><tr>
							<td>Учебное заведение</td>
							<td>Вид</td>
							<td>Год вып.</td>
							<td>Действия</td>
						</tr>
						<tr>
							<td>
								<?= CHtml::hiddenField('medicalSchool') ?>
								<?php
								$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
									'model'			 => $model,
									'name'			 => 'medicalSchoolName',
									//'attribute'		 => 'city.name',
									'value'			 => '',
									'source'		 => 'js:function (request, response) {
											$.ajax({
											   url: "/ajax/medicalSchools",
											   dataType: "json",
												   data: {
													   term: request.term
												   },
												   success: response
											   });
									   }',
									// additional javascript options for the autocomplete plugin
									'options'		 => array(
										'minLength'	 => '0',
										'change'	 => 'js:function( event, ui ) {
							if ( ui.item ) {
								$( "#' . CHtml::getIdByName('medicalSchool') . '" ).val( ui.item.id );
								return true;
							}
							else {
								$(this).val( "" );
								$( "#' . CHtml::getIdByName('medicalSchool') . '" ).val( "" );
								return false;
							}
							}'
									),
									'htmlOptions'	 => array(
										'class' => 'custom-text',
									),
								));
								?>
							<td>
								<?= CHtml::dropDownList('typeOfEducation', '', CHtml::listData(TypeOfEducation::model()->findAll(), 'id', 'name'), array('class' => 'custom-select w240')) ?>
							</td>
							<td>
								<?= CHtml::textField('year', '', array(
									'class'		 => 'custom-text w60',
									'maxlength'	 => 4,
									)) ?>
							</td>
							<td>

								<a class="btn-green add_de" title="Добавить" href="#">Добавить</a>
							</td>
						</tr>

						<?php foreach ($model->doctorEducations as $key => $education): ?>

							<tr>
								<td>
									<?= $form->hiddenField($model, "doctorEducations[{$key}][medicalSchoolId]") ?>
									<?= $form->hiddenField($model, "doctorEducations[{$key}][id]") ?>
									<?= $education->medicalSchool->name ?>
								</td>
								<td>
									<?= $form->hiddenField($model, "doctorEducations[{$key}][typeOfEducationId]") ?>
									<?= $education->typeOfEducation->name ?>
								</td>
								<td>
									<?= $form->hiddenField($model, "doctorEducations[{$key}][yearOfStady]") ?>
									<?= $education->yearOfStady ?>
								</td>

								<td>
									<!--									<a href="#">Редактировать</a>
																		/ -->
									<a class="del_sod" href="#">Удалить</a>
								</td>
							</tr>
						<?php endforeach; ?>

					</tbody>
				</table>
				
			</td>
		</tr>
		<tr class="search-result-box">
			<td></td>
			<td class="search-result-info">
				<a class="btn-red" title="Отменить" href="<?= $this->createUrl('experts/index') ?>">Отменить</a>
				<button class="btn-green" type="submit" title="Сохранить">Сохранить</button>
			</td>
		</tr>
	</tbody>
</table>
<?php $this->endWidget(); ?>