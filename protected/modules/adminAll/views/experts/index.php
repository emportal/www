<div class="flash-error">Внимание! Внесенные изменения появятся на сайте после подтверждения администратором</div>
<? if (Yii::app()->user->hasFlash('experts_add')):?>
<div class="flash-success"><?= Yii::app()->user->getFlash('experts_add')?></div>
<? endif; ?>
<?php
echo CHtml::link('Добавить специалиста', array('experts/add'), array('class' => 'btn'));
?>

<?=CHtml::link('Импорт врачей из другого адреса клиники', array('experts/import'), array('class' => 'btn import_experts'));?>

<br/><br/>

<?php if(true): ?>
<h4>Специалисты из других адресов (редактировать можно в главном адресе)</h4>
<?
	$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'order-grid2',
			'dataProvider'=>$dataProvider,
			#'filter'=>$oAgrDoctors,
			'enableSorting'=>false,
			'itemsCssClass'	 => 'service-table',
			'summaryText'	 => '',		
			'columns'=>array(
					array(
							'name'=>'name',
							//s'value'=>'$data->name',
					),
					array(
							'name'	 => 'birthday',
							'value'	 => '($data->birthday !== "0000-00-00 00:00:00") ? Yii::app()->dateFormatter->formatDateTime(strtotime($data->birthday), "medium", null) : "Не указана"',
							'type'	 => 'html',
					),
					array(
							'name'	 => 'sexId',
							'value'	 => '$data->sex->name',
					),
					array(
							'name'	 => 'experience',
							'value'	 => '$data->experienceNum',
					),
					array(
							'name'	 => 'specialties.name',
							'value'	 => 'reset($data->specialties)->name',
							#'filter'=>CHtml::activeTextField($oAgrDoctors->doctorSpecialty, 'name'), //CHtml::textField('specialties[name]', $data->specialites->name),
							//'type'	 => 'html',
					),
					/*array(
							'class'		 => 'CButtonColumn',
							'template'	 => '{delete}{update}{price}',
							'buttons'	 => array(
								'update' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
								),
								'delete' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link,"agr"=>1))',
								),
								'price' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
								),
							),
							'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
					),*/
	   ),
	));
?>
<?php endif; ?>


<?php if($oAgrDoctors->search($model)->getData()): ?>
<h4>Неподтвержденные</h4>
<?
	$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'order-grid2',
			'dataProvider'=>$oAgrDoctors->search($model),
			'filter'=>$oAgrDoctors,
			'enableSorting'=>true,
			'itemsCssClass'	 => 'service-table',
			'summaryText'	 => '',		
			'columns'=>array(
					array(
							'name'=>'name',
							//s'value'=>'$data->name',
					),
					array(
							'name'	 => 'birthday',
							'value'	 => '($data->birthday !== "0000-00-00 00:00:00") ? Yii::app()->dateFormatter->formatDateTime(strtotime($data->birthday), "medium", null) : "Не указана"',
							'type'	 => 'html',
					),
					array(
							'name'	 => 'sexId',
							'value'	 => '$data->sex->name',
					),
					array(
							'name'	 => 'experience',
							'value'	 => '$data->experienceNum',
					),
					array(
							'name'	 => 'specialties.name',
							'value'	 => 'reset($data->specialties)->name',
							'filter'=>CHtml::activeTextField($oAgrDoctors->doctorSpecialty, 'name'), //CHtml::textField('specialties[name]', $data->specialites->name),
							//'type'	 => 'html',
					),
					array(
							'class'		 => 'CButtonColumn',
							'template'	 => '{delete}{update}{price}',
							'buttons'	 => array(
								'update' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
								),
								'delete' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link,"agr"=>1))',
								),
								'price' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
								),
							),
							'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
					),


	   ),
	));
?>
<?php endif; ?>

<h4>Подтвержденные</h4>
<?
$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'order-grid',
		'dataProvider'=>$oDoctors->search($model),
		'filter'=>$oDoctors,
		'enableSorting'=>true,
		'itemsCssClass'	 => 'service-table',
		'summaryText'	 => '',		
		'columns'=>array(
				array(
						'name'=>'name',
						//s'value'=>'$data->name',
				),
				array(
						'name'	 => 'birthday',
						'value'	 => '($data->birthday !== "0000-00-00 00:00:00") ? Yii::app()->dateFormatter->formatDateTime(strtotime($data->birthday), "medium", null) : "Не указана"',
						'type'	 => 'html',
				),
				array(
						'name'	 => 'sexId',
						'value'	 => '$data->sex->name',
				),
				array(
						'name'	 => 'experience',
						'value'	 => '$data->experienceNum',
				),
				array(
						'name'	 => 'specialties.name',
						'value'	 => 'reset($data->specialties)->name',
						'filter'=>CHtml::activeTextField($oDoctors->doctorSpecialty, 'name'), //CHtml::textField('specialties[name]', $data->specialites->name),
						//'type'	 => 'html',
				),
				array(
						'class'		 => 'CButtonColumn',
						'template'	 => '{delete}{update}{price}',
						'buttons'	 => array(
							'update' => array(
								'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
							),
							'delete' => array(
								'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link))',
							),
							'price' => array(
								'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
							),
						),
						'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
				),
				

   ),
));
?>
<?php
/*
echo CHtml::link('Добавить специалиста', array('experts/add'), array('class' => 'btn'));
echo CHtml::tag('p', array(), Yii::app()->user->getFlash('experts_add'));
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'	 => $dataProvider,
		
	'summaryText'	 => '',
	'itemsCssClass'	 => 'service-table',
	'columns'		 => array(
		array(
			'name'	 => 'ФИО',
			'value'	 => '$data->name',
		),
		array(
			'name'	 => 'Дата рождения',
			'value'	 => 'Yii::app()->dateFormatter->formatDateTime($data->birthday, "medium", null)',
			'type'	 => 'html',
		),
		array(
			'name'	 => 'Пол',
			'value'	 => '$data->sex->name',
		),
		array(
			'name'	 => 'Стаж',
			'value'	 => '$data->experience',
		),
		array(
			'name'	 => 'Специальность',
			'value'	 => 'reset($data->specialties)->name',
			'type'	 => 'html',
		),
		array(
			'class'		 => 'CButtonColumn',
			'template'	 => '{delete}{update}{price}',
			'buttons'	 => array(
				'update' => array(
					'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
				),
				'delete' => array(
					'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link))',
				),
				'price' => array(
					'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
				),
			),
			'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
		),
	),
));*/
?>