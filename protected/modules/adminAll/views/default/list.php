<?php
/* @var $this Controller */
/* @var $model Clinic */
/* @var $company Company */
?>
<script>
	function runScript(e) {
		if (e.keyCode == 13) {
			searchList();
			return false;
		}
	}

	function searchList() {
		var text = $("#search").val().toLowerCase();
		
		$(".cName").each(function() {
			var rowText = $(this).html().toLowerCase();
			var parent = $(this).closest("tr");
			var ind = rowText.indexOf(text);
			if(ind != -1) {
				parent.show();
			} else {
				parent.hide();
			}
		});
	}
</script>
	
Выберите компанию из списка:

<input class="custom-text" onkeypress="return runScript(event)" type="text" id="search"/>
<input class="btn btn-blue" value="Искать" type="submit" onclick="searchList();return false;" />
<div class="company-list">
	
	<table class="table-custom">
		<tr>
			<th>Название</th>
			<th>Адрес</th>
			<th>Вход</th>
		</tr>
		<?php foreach($list as $row): ?>
			<tr>			
				<td class="cName" style="width:100px;"><?= $row->company->name ?> <?= empty($row->userMedicals) ? '': ''?></td>
				<td><?= $row->name ?></td>
				<td><?= CHtml::link('Войти',array('list','link'=>$row->link),array('class'=>'btn btn-green')); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
		
	
</div>
