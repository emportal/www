<? /* @var $model Clinic */ ?>
<?php
Yii::app()->clientScript->registerPackage('jquery.maskedinput')->registerScript('contacts', '
	$("#company-contacts").on("click", "a.add",function(){
		
		var count = $("#company-contacts").closest("td").find( "p" ).length;
		$(this).closest("td").append(
			$("<p>").addClass("count").append(
				$("p.new_phone").html()
			)
		).find("p.count input.custom-text").each(function(){
					var name = $(this).attr("name").replace("%new%", count+100);
					$(this).attr("name", name);
				});
		$(\'input[name$="[cityCode]"]\').mask("999?9",{placeholder:"_"});
		$(\'input[name$="[countryCode]"]\').mask("9",{placeholder:"_"});
		$(\'input[name$="[number]"]\').mask("99999?99",{placeholder:"_"});
	});

	$("#company-contacts").on("click", "a.del",function(){
		if ( $(this).closest("td").find("p").length > 2) {
			$(this).closest("p").remove();
		}
		else {
			$(this).closest("p").find("input").val("");
		}
	});
	

	$(\'input[name$="[cityCode]"]\').mask("999?99",{placeholder:"_"});
	$(\'input[name$="[countryCode]"]\').mask("9",{placeholder:"_"});
	$(\'input[name$="[number]"]\').mask("99999?99",{placeholder:"_"});
	
	', CClientScript::POS_READY);

$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-contacts',
	//'action'				 => CHtml::normalizeUrl(array('/default/updateLogo')),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data', 'class'=>'admin-form'),
		));
/* @var $form CActiveForm */
?>
<div class="flash-error">Внимание! Внесенные изменения появятся на сайте после подтверждения администратором</div>
<table class="search-result-table">
	<tbody>
	<?php /*?>
		<tr>
			<td colspan="2"><?php echo $model->postIndex?>, <?php echo $model->city->name?>, <?php echo $model->street?>, <?php echo $model->houseNumber?></td>
		</tr>
	
		<tr class="search-result-box odd">
			<td class="search-result-signup size-14">Индекс</td>
			<td class="search-result-info">
				<?= $form->textField($model, 'postIndex', array('class'			 => 'custom-text w280', 'autocomplete'	 => 'off', 'disabled'=>'disabled')) ?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Город</td>
			<td class="search-result-info">
				<?= CHtml::textField('cityName', $model->city->name, array('class'			 => 'custom-text w280', 'autocomplete'	 => 'off', 'disabled'=>'disabled'))?>
				
			</td>
		</tr>
		<tr class="search-result-box odd">
			<td class="search-result-signup size-14">Улица</td>
			<td class="search-result-info">
				<?= $form->textField($model, 'street', array('class'			 => 'custom-text w280', 'autocomplete'	 => 'off', 'disabled'=>'disabled')) ?>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Дом</td>
			<td class="search-result-info">
				<?= $form->textField($model, 'houseNumber', array('class'			 => 'custom-text w280', 'autocomplete'	 => 'off', 'disabled'=>'disabled')) ?>
			</td>
		</tr>
		<? */?>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Сайт
			<br />
			<smal class="label-comment">Пример: http://www.emportal.ru</smal>
			</td>
			<td class="search-result-info">
				<?php /*if($model->agr_www):*/?>
					<?= $form->textField($model, 'agr_www[refValue]', array('class' => 'custom-text w280', 'autocomplete'	 => 'off')) ?>
					<?php if($model->agr_www) {echo $form->error($model->agr_www, 'refValue');}?>
				<?php /*else:?>
					<?= $form->textField($model, 'agr_www[refValue]', array('class'	=> 'custom-text w280', 'autocomplete' => 'off')) ?>
					<?php if($model->www) {echo $form->error($model->www, 'refValue');}?>
				<?php endif;*/?>				
				
			</td>
		</tr>
		<tr class="search-result-box odd">
			<td class="search-result-signup size-14">Email
			<br />
			<smal class="label-comment">Пример: info@emportal.ru</smal>
			</td>
			<td class="search-result-info">
				<?php /*if($model->agr_email):*/?>
					<?= $form->textField($model, 'agr_email[name]', array('class' => 'custom-text w280', 'autocomplete'	 => 'off')) ?>
					<?php if($model->agr_email) {echo $form->error($model->agr_email, 'name');}?>
				<?php/*else:?>
					<?= $form->textField($model, 'email[name]', array('class' => 'custom-text w280', 'autocomplete'	 => 'off')) ?>
					<?php if($model->email) {echo $form->error($model->email, 'name');}?>
				<?php endif;*/?>
				
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Телефоны 
			<br />
			<smal class="label-comment">Пример: 7 812 123456</smal>
			</td>
			<td class="search-result-info">
				<p class="new_phone">
					<?//= $form->textField($model, "phones[][name]", array('class' => 'custom-text')) ?>
					<?= $form->textField($model, "agrphones[%new%][countryCode]", array('class' => 'custom-text', 'style'=>'width:20px;')) ?>
					<?= $form->textField($model, "agrphones[%new%][cityCode]", array('class' => 'custom-text', 'style'=>'width:40px;')) ?>
					<?= $form->textField($model, "agrphones[%new%][number]", array('class' => 'custom-text', 'style'=>'width:100px;')) ?>
					<a class="btn del" title="Удалить"  >-</a>
					<a class="btn add" title="Добавить"  >+</a>
				</p>
				<?php if(!empty($model->agrphones)): ?>
						<?php foreach ( $model->agrphones as $key => $phone ): ?>
						<p>
							<?= $form->hiddenField($model, "agrphones[{$key}][link]") ?>
							<? //= $form->textField($model, "phones[{$key}][name]", array('class' => 'custom-text')) ?>
							<?= $form->textField($model, "agrphones[{$key}][countryCode]", array('class' => 'custom-text', 'style'=>'width:20px;')) ?>
							<?= $form->textField($model, "agrphones[{$key}][cityCode]", array('class' => 'custom-text', 'style'=>'width:40px;')) ?>
							<?= $form->textField($model, "agrphones[{$key}][number]", array('class' => 'custom-text', 'style'=>'width:100px;')) ?>
							<a class="btn del" title="Удалить"  >-</a>
							<a class="btn add" title="Добавить"  >+</a>
						</p>
					<? endforeach; ?>							
				<? else:?>
					<p class="new_phone">
						<?//= $form->textField($model, "phones[0][name]", array('class' => 'custom-text')) ?>
						<?= $form->textField($model, "agrphones[0][countryCode]", array('class' => 'custom-text', 'style'=>'width:20px;')) ?>
						<?= $form->textField($model, "agrphones[0][cityCode]", array('class' => 'custom-text', 'style'=>'width:40px;')) ?>
						<?= $form->textField($model, "agrphones[0][number]", array('class' => 'custom-text', 'style'=>'width:100px;')) ?>
						<a class="btn del" title="Удалить"  >-</a>
						<a class="btn add" title="Добавить"  >+</a>
					</p>	
				<? endif; ?>
			</td>
		</tr>		
		<tr>
			<td colspan="2" class="search-result-signup">
				<div class="flash-notice">Установить рабочий день выходным можно выбрав начальное и конечное значение "00:00"</div>
				<br>
				<h4>Часы работы</h4>
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">
				<label>Обеденный перерыв</label>
				<br>
			<?=$form->error($model->workingHours,"{$day}Start")?>
			</td>
			<td class="search-result-info">
				<label>c</label>
				<?php echo $form->dropDownList($model,"breakStart", $model->breakHoursStep,array(
					'style'=>"width:122px"
				));?>					
				<label>по</label>
				<?php echo $form->dropDownList($model,"breakFinish", $model->breakHoursStep,array(
					'style'=>"width:122px"
				))?>					
			</td>
		</tr>
		<tr class="search-result-box odd">
			<td style="min-height:15px;display:block" class="search-result-signup size-14">
				&nbsp;
			</td>
			<td class="search-result-info">
								
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">
				<label>Заполнить все дни недели</label> 
				<?= $form->checkBox($model, 'workingHours[allCheck]', [
					'onclick' => ' if($(this).is(":checked")) {
						$(".allCheck").attr("disabled",false);
						$(".oneDay").attr("disabled",true);
					} else {
						$(".allCheck").attr("disabled",true);
						$(".oneDay").attr("disabled",false);
					}
						
					'
				]); ?>
				<br>
			<?=$form->error($model->workingHours,"{$day}Start")?>
			</td>
			<td class="search-result-info">
				<label>c</label>
				<?php echo $form->dropDownList($model,"workingHours[allStart]", $model->workingHoursStep,array(
					'style'=>"width:122px",
					'class' => 'allCheck',
					'disabled' => 'true'
				));?>					
				<label>по</label>
				<?php echo $form->dropDownList($model,"workingHours[allFinish]", $model->workingHoursStep,array(
					'style'=>"width:122px",
					'class' => 'allCheck',
					'disabled' => 'true'
				))?>					
			</td>
		</tr>		
		<?php foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $key=>$day):?>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">
			<?php echo $form->label($model->workingHours, "{$day}Start")?><br>
			<?=$form->error($model->workingHours,"{$day}Start")?>
			</td>
			<td class="search-result-info">
				<label>c</label>
				<?php echo $form->dropDownList($model,"workingHours[{$day}Start]", $model->workingHoursStep,[
					'class' => 'oneDay'
				]);?>				
				<?php
				/*$this->widget('CMaskedTextField', array(
					'model'			 => $model,
					'attribute'		 => "workingHours[{$day}Start]",
					'mask'			 => '99:99',
					'htmlOptions'	 => array('class' => 'custom-text w40')
				));*/
				?>
				<label>по</label>
				<?php echo $form->dropDownList($model,"workingHours[{$day}Finish]", $model->workingHoursStep,[
					'class' => 'oneDay'
				])?>
				<?php
				/*$this->widget('CMaskedTextField', array(
					'model'			 => $model,
					'attribute'		 => "workingHours[{$day}Finish]",
					'mask'			 => '99:99',
					//'charMap' => array('~'=>'[03]' , '='=>'[012]'),
					'htmlOptions'	 => array('class' => 'custom-text w40')
				));*/
				?>
			</td>
		</tr>
		<?php endforeach;?>
		<?php /*?>
		<?php foreach ( $model->workingHours as $key => $value ): ?>
			<tr class="search-result-box <?= $key % 2 ? 'odd' : '' ?>">
				<td class="search-result-signup size-14"><?= $value->weekday->name ?></td>
				<td class="search-result-info">
					<label>c</label>
					<?php
					$this->widget('CMaskedTextField', array(
						'model'			 => $model,
						'attribute'		 => "workingHours[{$key}][start]",
						'mask'			 => '99:99',
						'htmlOptions'	 => array('class' => 'custom-text w40')
					));
					?>
					<label>по</label>
					<?php
					$this->widget('CMaskedTextField', array(
						'model'			 => $model,
						'attribute'		 => "workingHours[{$key}][finish]",
						'mask'			 => '99:99',
						'htmlOptions'	 => array('class' => 'custom-text w40')
					));
					?>
				</td>
			</tr>
		<? endforeach; ?>
		
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Обеденный перерыв</td>
			<td class="search-result-info">
				<label>c</label>
				<?php
				$this->widget('CMaskedTextField', array(
					'model'			 => $model,
					'attribute'		 => "breakStart",
					'mask'			 => '99:99',
					'htmlOptions'	 => array('class' => 'custom-text w40')
				));
				?>
				<label>по</label>
				<?php
				$this->widget('CMaskedTextField', array(
					'model'			 => $model,
					'attribute'		 => "breakFinish",
					'mask'			 => '99:99',
					'htmlOptions'	 => array('class' => 'custom-text w40')
				));
				?>
		</tr>
<!--		<tr>
			<td colspan="2" class="search-result-signup"><br><h4>Главный врач</h4></td>
		</tr>
		<tr class="search-result-box odd">
			<td class="search-result-signup size-14">ФИО</td>
			<td class="search-result-info">
				<input type="text" name="contactFace" class="custom-text w280" value="Пупкин Василий Олегович" autocomplete="off">
			</td>
		</tr>
		<tr class="search-result-box">
			<td class="search-result-signup size-14">Email</td>
			<td class="search-result-info">
				<input type="text" name="Email" class="custom-text w280" value="email@site.ru" autocomplete="off">
			</td>
		</tr>
		<tr class="search-result-box odd">
			<td class="search-result-signup size-14">Телефон</td>
			<td class="search-result-info">
				<input type="text" name="phone" class="custom-text w280" value="(812) 222-22-22 доб. 145" autocomplete="off">
			</td>
		</tr>-->
		<?php */?>
		<tr class="search-result-box">
			<td class="search-result-signup size-14"></td>
			<td class="search-result-info">
				<button class="btn-red" name="action" value="" title="Отменить" >Отменить</button>
				<button class="btn-green" name="action" value="save" title="Сохранить" >Сохранить</button>
			</td>
		</tr>
	</tbody>
</table>
<?php $this->endWidget(); ?>