<?php
/* @var $this Controller */
/* @var $model Clinic */
?>
<style>
	#content .f-left {
		width: 550px;
	}
</style>
<script>
	$(document).ready(function() {
		var addrLink = '<?=$model->link?>';
		var doctorLink = '';
		$("#doctorCalendar").change(function() {
			$(".clinic-tip").show();
			doctorLink = $(this).val();				
			if(doctorLink) {
				var dt = new Date();
				var month = (parseInt(dt.getMonth())+1);
				/* console.log(dt.getFullYear() + "-" + (month > 9 ? month : "0"+month) + "-" + dt.getDate());*/
				dt.setDate(parseInt(dt.getDate())+1);
				var date = dt.getFullYear() + "-" + (month > 9 ? month : "0"+month) + "-" + dt.getDate();
				$.ajax({
					url: "/ajax/getDatesForCalendar",
					async: false,
					data: ({
					   /* date: date,
						type: "forward",*/
						link: addrLink,
						doctor: doctorLink,
						backend:1,
					}),
					dataType: 'json',
					type: "GET",
					success: function(msg) {
						if (msg.html != "Клиника не указала часы работы") {                    
							$('#content').html('<div id="calendar">' + msg.html + '</div>\n\
							<input type="hidden" id="AppointmentToDoctors_plannedTime" name="AppointmentToDoctors[plannedTime]">\n\
							<input type="hidden" id="AppointmentToDoctors_address_link" value="'+addrLink+'">\n\
							<input type="hidden" id="AppointmentToDoctors_doctorId" value="'+doctorLink+'">\n\
							<span id="disableDate" class="btn btn-blue" style="float:right;margin-left:10px">Отключить номерок</span>\n\
							<span id="enableDate" class="btn btn-green" style="float:right;">Включить номерок</span>\n\
							');
							 $('#content').append('<input type="hidden" id="timeblock_date" value="'+date+'">');
						}
					},
				}); 
			} else {
				$('#content').html("");
				$(".clinic-tip").hide();
			}
		});
		$('#disableDate').live('click',function() {
			$.ajax({
					url: "/adminClinic/ajax/disableDate",
					async: false,
					data: ({
					   /* date: date,
						type: "forward",*/
						addrLink: addrLink,
						doctorLink: doctorLink,
						plannedTime: $('#AppointmentToDoctors_plannedTime').val()
					}),
					dataType: 'json',
					type: "GET",
					success: function(msg) {
						if (msg.success) {                    
							$('#calendar .btn.active').removeClass('active').addClass('btn-blue');	
							$('#AppointmentToDoctors_plannedTime').val("");
							/*$('#content').html('<div id="calendar">' + msg.html + '</div>\n\
							<input type="hidden" id="AppointmentToDoctors_plannedTime" name="AppointmentToDoctors[plannedTime]">\n\
							<input type="hidden" id="AppointmentToDoctors_address_link" value="'+addrLink+'">\n\
							<input type="hidden" id="AppointmentToDoctors_doctorId" value="'+doctorLink+'">\n\
							<span id="disableDate" class="btn btn-blue" style="float:right">Отключить номерок<span>\n\
							');
							 $('#content').append('<input type="hidden" id="timeblock_date" value="'+date+'">');*/
						}
					},
				});
		});
		$('#enableDate').live('click',function() {
			$.ajax({
					url: "/adminClinic/ajax/enableDate",
					async: false,
					data: ({
					   /* date: date,
						type: "forward",*/
						addrLink: addrLink,
						doctorLink: doctorLink,
						plannedTime: $('#AppointmentToDoctors_plannedTime').val()
					}),
					dataType: 'json',
					type: "GET",
					success: function(msg) {
						if (msg.success) {                    
							$('#calendar .btn.active').removeClass('btn-blue');
							$('#AppointmentToDoctors_plannedTime').val("");
							/*$('#content').html('<div id="calendar">' + msg.html + '</div>\n\
							<input type="hidden" id="AppointmentToDoctors_plannedTime" name="AppointmentToDoctors[plannedTime]">\n\
							<input type="hidden" id="AppointmentToDoctors_address_link" value="'+addrLink+'">\n\
							<input type="hidden" id="AppointmentToDoctors_doctorId" value="'+doctorLink+'">\n\
							<span id="disableDate" class="btn btn-blue" style="float:right">Отключить номерок<span> \n\
							');
							 $('#content').append('<input type="hidden" id="timeblock_date" value="'+date+'">');*/
						}
						$('#calendar .btn.active').removeClass('active');
					},
				});
		});
	});
	
</script>
<?php 
$this->widget('ESelect2',[
	'name' => 'doctorCalendar',
	'data' => CHtml::listData($doctors,'link','name'),
	'options' => [
		'placeholder' => 'Выберите врача',
		'allowClear' => true,
		'width' => '300px'
	]
]);
?>
<?=CHtml::hiddenField('backend',1); ?>
<div style="width:550px;float:left" id="content">
	
</div>
<div class="clinic-tip">
	<div class="flash-notice">
		Памятка!
	</div>
	<div class="tip-block"><a href="#" class="btn btn-green padding5 w20 align-center box "></a> - выбранное время<br/></div>
	<div class="tip-block"><a href="#" class="btn btn-red padding5 w20 align-center box "></a> - запись на прием<br/></div>
	<div class="tip-block"><a href="#" class="btn w20 padding5 align-center box "></a> - свободно <br/></div>
	<div class="tip-block"><a href="#" class="btn btn-blue w20 padding5 align-center box"></a> - номерок отключен  <br/></div>
</div>

<?php #$this->endWidget(); ?>
