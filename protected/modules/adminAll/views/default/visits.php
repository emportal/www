<?php
/* @var $this Controller */
/* @var $model Clinic */
?>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
'id' => 'mydialog',
        'options' => array(
            'title' => 'Изменение времени',
            'autoOpen' => false,
            'modal' => true,
			'width' => 550,
            'resizable'=> false,
			'draggable'=> false
        ),
    ));
?>
<?php $this->endWidget(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'method' => 'post',
	'id' => 'phoneForAlert',
	'action' => $this->createUrl('default/visits'),
	'enableAjaxValidation' => false,
	'htmlOptions' => array(),
		));
?>
<table class="search-result-table">
	<tbody><tr class="search-result-box">
			<td class="search-result-signup size-14">Мобильный телефон для уведомлений: </td>
			<td class="search-result-info">
				<?php
				$this->widget('CMaskedTextField', array(
					'model' => Yii::app()->user->model,
					'attribute' => 'phoneForAlert',
					'mask' => '+79999999999',
					'placeholder' => '_',
					'htmlOptions' => array('class' => 'custom-text'),
				));
				?>
				<button class="btn-green">Сохранить</button>
				<button onclick="$('#User_phoneForAlert').val('')" class="btn-red">Удалить</button>
			</td>
	</tbody>
</table>



<?php $this->endWidget(); ?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	'summaryText' => '',
	'enableSorting' => false,
	'rowCssClassExpression' => '$data->link == "'.$link.'" ? "green" : ""',
	'itemsCssClass' => 'service-table visits-table',
	'pagerCssClass' => 'pages',
	'pager' => array(
		'cssFile' => false,
		'header' => 'Страницы: '
	),
	'template' => "{items}\n{pager}",
	'nullDisplay' => 'Не указано',
	'columns' => array(
		'createdDate' => array(
			'name' => 'createdDate',
			'value' => 'Yii::app()->dateFormatter->formatDateTime($data->createdDate, "medium", "short");'
		),
		'service' => array(
			'header' => 'Услуга',
			'name' => 'service',
			'value' => '$data->service->name',
			'htmlOptions' => array(
				'class' => 'service_name'
			)
		),
		'doctor' => array(
			'header' => 'Врач',
			'name' => 'doctor',
			'value' => '$data->doctor->shortname'
		),
		'plannedTime' => array(
			'name' => 'plannedTime',
			'value' => '$data->plannedTime == "0000-00-00 00:00:00" ? "Не указано" : Yii::app()->dateFormatter->formatDateTime($data->plannedTime, "medium", "short");'
		),
		'name',
		'phone_email' => array(
			"header" => "Тел. и Email",
			"name" => "phone_email",
			"value" => '$data->hidePhoneEmail',
			"type" => "raw",
		),
		/* 'email' => array(
		  "name"=>"email",
		  "value"=>'$data->hideEmail',
		  "type"=>"raw",
		  ), */
		'statusId' => array(
			'name' => 'statusId',
			'value' => '$data->statusText',
			'htmlOptions' => array(
				'class' => 'status'
			),
		),
		'action' => array(
			'header' => 'Действия',
			'value' => '$data->actions',
			'type' => 'raw',
			'htmlOptions' => array(
				'class' => 'actions'
			),
		)
//		'user' => array(
//			'value' => '$data->user->shortname'
//		),
	),
));
?>