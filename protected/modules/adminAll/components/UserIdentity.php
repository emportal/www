<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

	private $_id;

	public function __construct($username, $password, $id = null) {
		$this->username = $username;
		$this->password = $password;
		$this->_id = $id;
	}

	public function authenticate() {
		#findByLink
		//if ($this->username === "allAdmin5" && $this->password === "letmeinplease26") {
		if ($this->username === "zKjrt80z0" && $this->password === "A0vcz832i") {
			#$this->_id = "5dda6a2a-9923-4593-b835-2928cc0a4980";
			$this->errorCode = self::ERROR_NONE;
			return true;
		} else {
			return false;
		}


		$user = User::model()->findByAttributes(array("name" => $this->username));



		if (empty($user))
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (!$user->validatePassword($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			$this->_id = $user->id;
			$this->errorCode = self::ERROR_NONE;
		}

		return !$this->errorCode;
	}

	public function getId() {
		return $this->_id;
	}

}
