<?php

class ActionsController extends AdminAllController {

	public function actionIndex() {
		

		$model = $this->loadModel();

		$criteria = new CDbCriteria();

		$criteria->compare('addressId', $model->id);
		
		$dataProvider = new CActiveDataProvider('Action', array(
			'criteria'	 => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
		));
		
		$AGRdataProvider = new CActiveDataProvider('AgrAction', array(
			'criteria'	 => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
		));


		$this->render('index', array('model' => $model, 'dataProvider' => $dataProvider,'AGRdataProvider'=>$AGRdataProvider));
	}

	public function actionAdd() {

		$clinic = $this->loadModel();
		$model = new AgrAction();
		$model->unlimited = 1;
		$model->type = 0;		
		
		$currentLink = Yii::app()->db->createCommand()->
						select("max(`link`)")->
						from(Action::model()->tableName())->
						queryScalar();				
		$currentLink = intval($currentLink);
		$currentLink++;
		
		$model->link = str_pad($currentLink, 9, '0', STR_PAD_LEFT);
		
		$attr = Yii::app()->request->getParam(get_class($model));
		
		if ( $attr ) {
			$model->attributes = $attr;
			
			if(empty($model->serviceId)) {
				$model->serviceId = null;
			}
			if($model->type==1) {
				if($model->serviceId) {
					$model->setScenario('Discount');
				} else {
					$model->priceNew = "";
					$model->priceOld = "";
					$model->setScenario('DiscountGroup');
				}
			}
			$model->company = $clinic->company;
			$model->address = $clinic;
			
			if ( $model->save() ) {
				$this->redirect(array('actions/index'));
			}
			else {
				//var_dump($model->getErrors());
			}
		}


		$this->render('update', array('model' => $model, 'clinic'=>$clinic));
	}

	public function actionUpdate($link) {

		$clinic = $this->loadModel();
		$model = Action::model()->findByAttributes(array(
			'link'		 => $link,
			'addressId'	 => $clinic->id,
		));		
	
		$AgrModel=AgrAction::model()->findByAttributes(array(
				'link'		 => $link,
				'addressId'	 => $clinic->id,
			));
		if(!$AgrModel) {			
			$AgrModel = new AgrAction();
		}
		
		$AgrModel->attributes = $model->attributes;
		/*
		if(!$model) {
			$AgrModel=AgrAction::model()->findByAttributes(array(
				'link'		 => $link,
				'companyId'	 => $clinic->company->id,
			));		
			$model=$AgrModel;
		} else {
			$AgrModel = AgrAction::model()->findByAttributes(array(
				'link'		 => $link,
				'companyId'	 => $clinic->company->id,
			));	
			$AgrModel->attributes=$model->attributes;
		}
		*/
		if ( $_POST[get_class($AgrModel)] ) {
			$AgrModel->attributes = $_POST[get_class($AgrModel)];
			
			$arr = $model->attributes;
			unset($arr['id']);
			unset($arr['addressId']);
			unset($arr['link']);
			unset($arr['companyId']);
			unset($arr['employeeId']);
			unset($arr['completed']);
			unset($arr['shortDescription']);
			if(!array_diff($arr,$_POST[get_class($AgrModel)])) {
				$dontsave=true;
			}
			#var_dump($model->attributes);echo "<br>"; var_dump($_POST[get_class($AgrModel)]);
			
			if(empty($AgrModel->serviceId)) {
				$AgrModel->serviceId = null;
			}
			if($AgrModel->type==1) {
				if($AgrModel->serviceId) {
					$AgrModel->setScenario('Discount');
				} else {
					$AgrModel->priceNew = "";
					$AgrModel->priceOld = "";
					$AgrModel->setScenario('DiscountGroup');
				}
			}
			if($dontsave)
				$this->redirect(array('actions/index'));
			
			if ($AgrModel->save() ) 
				$this->redirect(array('actions/index'));
			
		}


		$this->render('update', array('model' => $AgrModel, 'clinic'=>$clinic));
	}
	public function actionAgrDelete($link) {
		$model = AgrAction::model()->findByAttributes(array(
			'link'		 => $link,
			'addressId'	 => $this->loadModel()->id,
		));

		$model->delete();

		$this->redirect(array('actions/index'));
	}
	public function actionDelete($link) {

		$model = Action::model()->findByAttributes(array(
			'link'		 => $link,
			'addressId'	 => $this->loadModel()->id,
		));

		$model->delete();

		$this->redirect(array('actions/index'));
	}

	public function actionStatusChange($link) {

		$model = Action::model()->findByAttributes(array(
			'link'		 => $link,
			'addressId'	 => $this->loadModel()->id,
		));
		/* @var $model Action */

		$model->update(array('publish' => !$model->publish));

		$this->redirect(array('actions/index'));
	}

}