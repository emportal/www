<?php

class AjaxController extends AdminAllController {

	public $layout = 'empty';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'cities' => array(
				'class'			 => 'AutoCompleteAction',
				'model'			 => 'City',
				'attributeValue' => 'name',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ( $error = Yii::app()->errorHandler->error ) {
			if ( Yii::app()->request->isAjaxRequest )
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionServiceByServiceActivite($link) {
		
		$clinic = $this->loadModel();
		
		$array = array();

		$creteria = new CDbCriteria(array(
			'order'	 => '`service`.`name`',
			'with'	 => array(
				'service'
			)
		));
		$creteria->compare('`t`.`addressId`', $clinic->id);
		$creteria->compare('`t`.`companyActivitesId`', $link);
		if(Yii::app()->request->getParam('term')) {
			$creteria->compare('service.name', Yii::app()->request->getParam('term'), true);
		}

		foreach (AddressServices::model()->findAll($creteria) as $group) {
			$array[] = array(
				'id'	 => $group->service->id,
				'value'	 => $group->service->name
			);
		}


		if(Yii::app()->request->getParam('json')) {
			header('Content-Type: application/json');
			echo CJSON::encode($array);
		}
	}
	public function actionSetAppointmentStatus($link,$status) {
		if(Yii::app()->request->getParam('json')) {
			$model = AppointmentToDoctors::model()->findByLink($link);
			switch($status) {
				case "seen":
					$model->statusId = AppointmentToDoctors::SEEN;
					if($model->update()) {						
						$array['msg'] = AppointmentToDoctors::$STATUSES[AppointmentToDoctors::SEEN];
						$array['actions'] = $model->actions;
						$array['success'] = true;
					} else {
						$array['success'] = false;
						$array['msg']=$model->errors;					
					}				
					break;
				case "accept":
					$model->statusId = AppointmentToDoctors::ACCEPTED_BY_REGISTER;
					if($model->update()) {						
						//Отправка сообщения пользователю о подтверждении 
						$model->sendMailToUser(AppointmentToDoctors::ACCEPTED_BY_REGISTER);
						#$model->sendSmsToUser(AppointmentToDoctors::ACCEPTED_BY_REGISTER);
						
						$array['msg'] = AppointmentToDoctors::$STATUSES[AppointmentToDoctors::ACCEPTED_BY_REGISTER];
						$array['actions'] = $model->actions;
						$array['success'] = true;
					} else {
						$array['success'] = false;
						$array['msg']=$model->errors;					
					}	
					break;
				case "decline":						
					
					$model->comment = CHtml::encode(Yii::app()->request->getParam('reason'));
					$model->statusId = AppointmentToDoctors::DECLINED_BY_REGISTER;
					if($model->update()) {
						//Отправка сообщения пользователю о подтверждении 
						$model->sendMailToUser(AppointmentToDoctors::DECLINED_BY_REGISTER);
						#$model->sendSmsToUser(AppointmentToDoctors::DECLINED_BY_REGISTER);
						
						$array['msg'] = AppointmentToDoctors::$STATUSES[AppointmentToDoctors::DECLINED_BY_REGISTER];
						$array['actions'] = $model->actions;
						$array['success'] = true;
					} else {
						$array['success'] = false;
						$array['msg']=$model->errors;					
					}	
					break;
			}
		
		
			header('Content-Type: application/json');
			echo CJSON::encode($array);
		}		
	}
	
	
	public function actionAddExpert($doctor = null) {
		$doctor = Doctor::model()->findByLink($doctor);
		if($doctor) {
			$pow = new PlaceOfWork();
			$pow->doctorId  = $doctor->id;
			$pow->companyId = Yii::app()->user->model->companyId;
			$pow->addressId = $this->loadModel()->id;
			$pow->current = 2;
			if($pow->save()) {
				echo CJSON::encode(['success'=>true]);
			} else {
				var_dump($pow->getErrors());
			}			
		}
		Yii::app()->end();
		
	}
	
	public function actionRemoveExpert($doctor = null) {
		$doctor = Doctor::model()->findByLink($doctor);
		if($doctor) {
			$pow = PlaceOfWork::model()->findByAttributes([
				'addressId'	=>	$this->loadModel()->id,
				'doctorId'	=>	$doctor->id,
				'current'	=>	2
			]);
			if($pow->delete()) {
				echo CJSON::encode(['success'=>true]);
			} else {
				echo CJSON::encode(['success'=>false,'msg'=>'not found']);
			}
		}
		Yii::app()->end();
	}
}
