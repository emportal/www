<?php

class ServicesController extends AdminAllController {

	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->with = array(
			'serviceSection',
			'companyActivite',
		);
		$dataProvider = new CActiveDataProvider('Service', array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
		));

		$cmd = Yii::app()->db->createCommand();

		$cmd->
				from(Service::model()->tableName() . ' s')->
				leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId')->
				leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->
				leftJoin(AddressServices::model()->tableName() . ' as', 's.id = as.serviceId');

		$cmd->select(array(
					'as.id as id',
					'IF(LOWER(`ca`.`name`) = \'нет данных\',\'\',`ca`.`name`) as CompanyActiviteName',
					'IF(LOWER(`ss`.`name`) = \'нет данных\',\'\',`ss`.`name`)  as ServiceSectionName',
					's.name as ServiceName',
					's.link as link',
					'IFNULL(as.price,0) as Price',
					'IFNULL(as.free,0) as free'
				))->
				where('as.addressId = :id', array(':id' => Yii::app()->user->model->clinic->id));
		$cmd->order("ServiceSectionName ASC");

		$rows = $cmd->queryAll();

		$cmd = Yii::app()->db->createCommand();

		$cmd->
				from(ServicesRequest::model()->tableName() . ' s')->
				leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId');
		//leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->

		$cmd->select(array(
					//'ca.name as CompanyActiviteName',
					'ss.name as ServiceSectionName',
					's.name as ServiceName',
					's.status as Status',
					's.rejectionText as Reject',
					'IFNULL(s.price,0) as Price',
				))
				->where('s.addressId = :id', array(':id' => Yii::app()->user->model->clinic->id))
				->order('s.status ASC');

		$ServicesRequest = $cmd->queryAll();

		$this->render('index', array('dataProvider' => $dataProvider, 'rows' => $rows, 'ServicesRequest' => $ServicesRequest));
	}

	public function actionAjaxUpdate() {
		$params = Yii::app()->request->getParam('AddressServices');

		if (count($params)) {
			foreach ($params as $id => $price) {
				$model = AddressServices::model()->findByPk($id);
				$model->price = $price;				
				$model->save();
				
			}
		}
	}
	
	public function actionAjaxSetFree() {
		$params = Yii::app()->request->getParam('AddressServices');

		if (count($params)) {
			foreach ($params as $id => $free) {
				$model = AddressServices::model()->findByPk($id);
				$model->free = $free;				
				$model->save();
				
			}
		}
	}

	public function actionGetfile() {
		$cmd = Yii::app()->db->createCommand();

		$cmd->
				from(Service::model()->tableName() . ' s')->
				leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId')->
				leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->
				leftJoin(AddressServices::model()->tableName() . ' as', 's.id = as.serviceId AND as.addressId=:addressId', array(':addressId' => Yii::app()->user->model->clinic->id));

		//$cmd->where('as.addressId=:addressId', array(':addressId'=>Yii::app()->user->model->clinic->id));

		$cmd->select(array(
			"IF(ca.name = 'Нет данных','',ca.name) as CompanyActiviteName",
			"IF(ss.name = 'Нет данных','',ss.name) as ServiceSectionName",
			"s.name as ServiceName",
			'IFNULL(as.price,0) as Price',
		));
		$cmd->where("s.name <> 'Нет данных'");
		$cmd->order('CompanyActiviteName ASC, ServiceSectionName ASC');
		$rows = $cmd->queryAll();

		$outstream = fopen("php://temp", 'r+');
		//fputs($outstream, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		fputcsv($outstream, array('ПрофильДеятельности', 'МедицинскийРаздел', 'Услуга', 'Стоимость'), ';');
		foreach ($rows as $row)
			fputcsv($outstream, $row, ';');
		rewind($outstream);
		Yii::app()->request->sendFile('Каталог услуг.csv', mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
		fclose($outstream);
		die();
	}

	public function actionUpload() {
		$uploadDir = Yii::app()->user->model->clinic->uploadPath . '/services';
		Yii::app()->session['datainfo'] = array(
			'uploadDir' => $uploadDir,
			'clinic_name' => Yii::app()->user->model->clinic->name,
			'usermedical_name' => print_r(Yii::app()->user->model->attributes, true)
		);
		
		$model = new AddressServices();
		
		$file = CUploadedFile::getInstanceByName('file');

		$arr = glob($uploadDir . "/current_old*");

		for ($i = 0; $i < 3; $i++) {
			$str = explode("/", $arr[$i]);
			$files[] = $str[count($str) - 1];
		}

		if (!is_dir($uploadDir))
			mkdir($uploadDir, 0775, true);
		
		$filename = $uploadDir . '/current.' . $file->extensionName;
		//Удаляем последний файл
		//сдвигаем все файлы			

		if (file_exists($filename)) {
			$fileTpl = str_replace("." . $file->extensionName, "", $filename);
			$files[0] ? $f1 = $uploadDir . "/" . $files[0] : $f1 = "";
			$files[1] ? $f2 = $uploadDir . "/" . $files[1] : $f2 = "";
			$files[2] ? $f3 = $uploadDir . "/" . $files[2] : $f3 = "";


			if (file_exists($f3)) {
				unlink($f3);
			}

			if (file_exists($f2)) {
				$newF2 = str_replace("old2", "old3", $f2);
				rename($f2, $newF2);
			}
			if (file_exists($f1)) {
				$newF1 = str_replace("old1", "old2", $f1);
				rename($f1, $newF1);
			}
			$newFilename = str_replace("current", "current_old1_" . date("d-m-Y"), $filename);
			rename($filename, $newFilename);
		}

		$file->saveAs($filename);

		$data = array();
		$fp = fopen($filename, 'r');

		$delim = '';

		foreach (array(chr(9), ',', ' ', ';') as $delim) {
			if (count(fgetcsv($fp, 0, $delim)) == 4) {
				$encoding = mb_detect_encoding(fgets($fp), 'CP1251,UTF-8');

				break;
			}
			rewind($fp);
		}
		rewind($fp);
		while ($row = fgetcsv($fp, 0, $delim)) {
			$data[] = $row;
		}
		fclose($fp);


		$tr = Yii::app()->db->beginTransaction();

		$cmd = Yii::app()->db->createCommand();

		//$cmd->delete(AddressServices::model()->tableName(), 'addressId = :id', array(':id' => Yii::app()->user->model->clinic->id));
		$rowsToDelete = AddressServices::model()->findAll('addressId = :id', array(':id' => Yii::app()->user->model->clinic->id));
		foreach ($rowsToDelete as $row) {
			$row->delete();
		}
		ActiveRecord::$filterEmptyData = false;
		$rowsToDelete = CompanyActivites::model()->findAll('addressId = :id', array(':id' => Yii::app()->user->model->clinic->id));
		foreach ($rowsToDelete as $row) {
			$row->delete();
		}
		ActiveRecord::$filterEmptyData = true;

		$sql = 'INSERT INTO ' . AddressServices::model()->tableName() . '(' . implode(', ', AddressServices::model()->tableSchema->columnNames) . ') ';

		$cmd->
				reset()->
				from(Service::model()->tableName() . ' s')->
				leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId')->
				leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->
				select('(:id), :addressId, s.id, :price, ca.id,0')->
				where(array(
					'and',
					's.name = :s_name',
					'ss.name = :ss_name',
					'ca.name = :ca_name',
		));

		$cmd->text = $sql . $cmd->text;

		//echo $cmd->text;
		//Yii::app()->end();

		$row = array_shift($data);


		$mtime = microtime(true);
		$s_name = $ss_name = $ca_name = $price = '';

		$cmd->bindParam(':s_name', $s_name);
		$cmd->bindParam(':ss_name', $ss_name);
		$cmd->bindParam(':ca_name', $ca_name);
		$cmd->bindParam(':price', $price);
		$cmd->bindParam(':id', $mtime);
		$cmd->bindValue(':addressId', Yii::app()->user->model->clinic->id);

		//die($cmd->text);

		try {
			foreach ($data as $row) {
				if (iconv('WINDOWS-1251', 'UTF-8', $row[2]) == "Первоначальное обращение" || ($row[3] && $row[3] > 0)) {
					//$mtime = microtime(true);
					$mtime = ActiveRecord::create_guid(array(
								$row[0],
								$row[1],
								$row[2],
								Yii::app()->user->model->clinic->id
					));

					list($ca_name, $ss_name, $s_name, $price) = array_map(
							function($value) use($encoding) {
						//return mb_convert_encoding($value, 'UTF-8', $encoding);
						return iconv('WINDOWS-1251', 'UTF-8', $value);
					}, $row);
					if ($s_name == "Первоначальное обращение") {
						$cmd1 = Yii::app()->db->createCommand();

						$cmd1->
								reset()->
								from(Service::model()->tableName() . ' s')->
								select('(:id), :addressId, s.id, :price,:caId,0')->
								where('s.name = :s_name');
						$caId = Yii::app()->db->createCommand()
										->select("id")
										->from(CompanyActivite::model()->tableName())
										->where("name = 'Нет данных'")->queryScalar();
						$cmd1->text = $sql . $cmd1->text;
						$cmd1->bindParam(':s_name', $s_name);
						$cmd1->bindValue(':caId', $caId);
						$cmd1->bindParam(':price', $price);
						$cmd1->bindParam(':id', $mtime);
						$cmd1->bindValue(':addressId', Yii::app()->user->model->clinic->id);

						$cmd1->execute();
					} else {
						$cmd->execute();
					}

					$syncTablesCmd = Yii::app()->db->createCommand();
					$syncTablesCmd->insert('syncTables', array(
						'tableId' => $mtime,
						'table' => AddressServices::model()->tableName(),
						'action' => 'insert',
					));
					//$ca_name.;
				}
			}
		} catch (PDOException $e) {
			$tr->rollback();

			Yii::app()->user->setFlash('error', 'Ошибка при загрузке услуг в базу');

			$this->redirect('index');
		}

		$tr->commit();

		
		$data = AddressServices::model()->findAll([
			'group' => 'companyActivitesId',
			'condition' => "addressId = '".Yii::app()->user->model->clinic->id."'",
			'with' => [
				'companyActivite' => [
					'joinType' => 'INNER JOIN'
				]
			]
		]);		
				
		//add services into CompanyActivites
		foreach($data as $row) {
			
			$ca = new CompanyActivites();
			$ca->addressId = $row->address->id;
			$ca->companyActivitesId = $row->companyActivitesId;
			$ca->save();
		}
		

		$ca = CompanyActivite::model()->with('services.addressServices')->findAll('addressServices.addressId = :address', array(':address' => Yii::app()->user->model->clinic->id));

		Yii::app()->user->model->clinic->companyActivitesGroupByName = $ca;
		#var_dump(Yii::app()->user->model->clinic->companyActivitesGroupByName);die();
		Yii::app()->user->model->clinic->save();





		Yii::app()->user->setFlash('success', "Загрузка выполнена.");
		Yii::app()->session['datainfo'] = array();
		
		$this->redirect('index');
	}

	public function actionAddNew() {
		$model = new ServicesRequest();
		$clinic = $this->loadModel();

		if ($attr = Yii::app()->request->getParam(get_class($model))) {

			$model->attributes = $attr;
			$model->addressId = $clinic->id;
			$model->status = 0;
			#$model->id = md5(implode('', $model->attributes));

			if ($model->save()) {

				Yii::app()->user->setFlash('success', "Услуга отправлена на модерацию.");
				$this->redirect('index');
			}
		}

		$this->render('addNew', array('model' => $model, 'clinic' => $clinic));
	}

	public function actions() {
		return array(
			'exportTable' => array(
				'class' => 'ext.htmltableui.actions.HtmlExportCsv',
				'path' => '/csv/',
			),
		);
	}

	public function actionHandleHtmlTable() {
		if (isset($_POST)) {
			//return the POST variable back
			//the widget will show an alert() with this data
			print_r($_POST);
		}
	}

}
