<?php

class adminAllModule extends CWebModule
{

	public $layout = 'main';
	protected $_modulemenu = array(
		'Описание'				 => 'default/index',
		'Контакты'	 			 => 'default/contacts',
		'Услуги'				 => 'services/index',
		'Специалисты'			 => 'experts/index',
		'Регистратура'			 => 'default/visits',
		'Акции'					 => 'actions/index',
		//'Рекламные банеры'		 => 'banners/index',
		'Отзывы'				 => 'comments/index',
		'Галерея'				 => 'default/gallery',
				
	);

	public function init()
	{
		
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		// import the module-level models and components
		$this->setImport(array(
			'adminAll.models.*',
			'adminAll.components.*',			
		));
		
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			
			
			if(  $_SERVER['PHP_AUTH_USER'] === "admin" &&   $_SERVER['PHP_AUTH_PW'] === "admin001") {
				#unset($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW']);
				return true;
				
			} else {
				Header ("WWW-Authenticate: Basic realm=\"Admin Page\"");
				Header ("HTTP/1.0 401 Unauthorized");
				
			}
				
	

			// this method is called before any module controller action is performed
			// you may place customized code here
			
		}
		else
			return false;
	}

	public function getModuleMenu()
	{

		$menuItems = array();

		foreach ($this->_modulemenu as $key => $value) {
			if(is_array($value)) {
				$url = $value['url'];
				$htmlOptions = $value['htmlOptions'];
			}
			else {
				$url = $value;
				$htmlOptions = array();
			}
			$menuItems[] = array(
				'label'	 	  => $key,
				'url'	 	  => array(CHtml::normalizeUrl(array($url))),
				'htmlOptions' => $htmlOptions,
			);
		}

		return $menuItems;
	}

}
