<?php

class NewAdminModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		$this->setImport(array(
			'newAdmin.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			if (!Yii::app()->user->isGuest AND Yii::app()->user->model->hasRight('ACCESS_NEW_ADMIN')) {
				Yii::app()->user->loginUrl = Yii::app()->createUrl("/newAdmin/default/login");
			} else {
				echo 'Недостаточно прав на совершение действия. <a href="/">На главную</a>.'; exit();
			}
			return true;
		} else {
			return false;
        }
	}
}
