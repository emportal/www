<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    private $_id;

	public function authenticate()
	{
		
		if($this->username == 'adm' && $this->password=='adm')
		{
			$this->_id = 1111111111111111111111;
			$this->errorCode = self::ERROR_NONE;
			
			return !$this->errorCode;
		}
		
		//--Потом подключить реальных юзеров
        $user = User::model()->findByAttributes( array("name"=>$this->username) );


        if ( empty($user) )
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if ( !$user->validatePassword($this->password) )
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $user->id;
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;

	}

    public function getId(){
        return $this->_id;
    }
}