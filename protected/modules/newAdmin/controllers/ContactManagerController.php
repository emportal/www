<?php

class ContactManagerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/nocolumn';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'setManagers', 'update', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionIndex()
	{
		$this->redirect(array('view'));
	}


	/**
	 * Lists all models.
	 */
	public function actionView()
	{
		$filter = Yii::app()->request->getParam('filter');		
		Yii::app()->session['newsReturnUrl']=$this->createAbsoluteUrl("/".Yii::app()->urlManager->ParseUrl(Yii::app()->request),$filter ? array('filter'=>$filter): array());
		
		//Address::$showInactive = true;
		
		$managerId = Yii::app()->user->id;
		if(!empty(Yii::app()->request->getParam('manager_id')))
		{
			if (Yii::app()->request->getParam('manager_id') != $managerId) {
				if ( Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') ) {
					$managerId = Yii::app()->request->getParam('manager_id');
				} else {
					$this->render('//site/error', ['message' => 'У Вас недостаточно прав для доступа к запрашиваемым данным!', 'code' => 403]);
					Yii::app()->end();
				}
			}
		}
		
		
		$criteria=new CDbCriteria;
		$criteria->with = ['userMedical' => 
							[
								'together' => true,
								'with' => 
								['company' => 
									[
										'together' => true,
									],
								],
							],
		            	];

		if($managerId != 'all') {
			$criteria->with['userMedical']['with']['company']['with'] = ['managerRelations' => 
													[
							            				'together' => true,
							            				'condition' => "userId = :userId",
							            				'params' => [":userId" => $managerId],
													]
												];
		}
		
		$dataProvider = new CActiveDataProvider('ContactManager', array(
	        'criteria' => $criteria,
	        'pagination' => array(
	            'pageSize' => Yii::app()->params['postsPerPage'],
	        ),
			'sort'=>array(
				'defaultOrder' => 't.createDate DESC',
			),
	    ));
		
		
		$model = Company::model();
		$model->managerId = $managerId;

		$this->render('view',array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
			'managerListData'=>User::model()->getManagerList(),
			'filter'=>$filter
		));
	}
}
