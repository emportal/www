<?php

class ContrAgentsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/nocolumn';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'create', 'update', 'delete', 'MarkToRemoval'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionIndex()
	{
		$this->redirect(array('view'));
	}


	/**
	 * Lists all models.
	 */
	public function actionView()
	{
		$model = Company::model();
		$model->attributes = Yii::app()->request->getParam(get_class($model));
		$model->filterSamozapis = 'no-samozapis';
		$model->managerId = Yii::app()->request->getParam('manager_id');
		$model->nameLike = Yii::app()->request->getParam('company_name');
		$model->companyTypeArtificial = Yii::app()->request->getParam('company_type');
		
		$dtOptions = [
			'showAddressCount'=>1,
			'region'=> (Yii::app()->request->getParam('region')) ? Yii::app()->request->getParam('region') : Yii::app()->session['selectedRegion'],
			'allowSearchOnAllManagers' => 'true'
		];
		$dataprovider = $model->companyDataProviderForNewAdminPanel($dtOptions);

		if(Yii::app()->request->getParam('download')) {
			$dataprovider->setPagination(false);
			FileMaker::createContrAgentsListReport($dataprovider);
		} else {
			$this->render('view',array(
				'dtOptions' => $dtOptions,
				'dataprovider' => $dataprovider,
				'managerListData' => User::model()->getManagerList(),
			));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{/*
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}*/
	}
	
//ALTER TABLE `company` ADD COLUMN `removalFlag` INT(4) NOT NULL DEFAULT '0' COMMENT '-1 = Помечен к удалению; 1 = удалён';
	public function actionMarkToRemoval() {
		$response = [];
		$model = Company::model();
		if(empty($_POST["id"])) {
			$response["status"] = "ERROR";
			$response["text"] = "Идентификатор не задан!";
		}
		elseif ( !Yii::app()->user->model->hasRight('ACCESS_NEW_ADMIN') ) { /* ACCESS_ALL_MANAGERS_DATA */
			$response["status"] = "ERROR";
			$response["text"] = "У вас не достаточно прав для совершения этого действия!";
		}
		elseif($model->updateByPk($_POST["id"],array('removalFlag'=>$_POST["value"]))) {
			$response["status"] = "OK";
			$response["text"] = "Запись помечена!";
		} else {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка!";
		}
		echo json_encode($response);
	}
}
/*
 * в таблице company добавлен столбец removalFlag
 * по умолчанию removalFlag = 0 (не удалён)
 * removalFlag = -1 (Помечен к удалению)
 * removalFlag = 1 (Удалён)
 *
 * В модель Company добавлена статическая переменная
 * Company::$showRemoved = false; // Отображать удалённые
 * 
 * Команда RemoveMarkedCompaniesCommand переводит помеченные компании в удалённые
 */