<?php

class BillsController extends Controller
{
	public $layout='/layouts/nocolumn';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'getEnvelope', 'logAdminClinic', 'GetMatching', 'GetBill'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$this->redirect(array('view'));
	}
	
	public function actionView()
	{
		Address::$showInactive = true;
		Company::$showRemoved = true;
		$user = Yii::app()->user->model;
		
		$model = Address::model();
		$model->managerId = Yii::app()->request->getParam('manager_id');
		$model->nameLike = Yii::app()->request->getParam('company_name');
		if (!$user->hasRight('ACCESS_ALL_MANAGERS_DATA'))
			$model->managerId = $user->id;
		$dtOptions = [
			'appointmentPeriod' => Yii::app()->request->getParam('report_month'),
			'includeFixedContracts' => 'true',
			'region' => Yii::app()->request->getParam('region'),
			'samozapis' => '0',
		];
		
		$appModel = AppointmentToDoctors::model();
		if (!empty($dtOptions['appointmentPeriod']))
			$appModel->visit_time_period = MyTools::getDateInterval($dtOptions['appointmentPeriod']);
		else 
			exit('report_month should be set');
		
		$reportMonth = MyTools::convertIntervalToReportMonth($appModel->visit_time_period);
		
		if(Yii::app()->request->getParam('download')) {
			FileMaker::createBillsListReport($model->dataProviderForNewAdminPanel($dtOptions),$reportMonth);
		}
		elseif(Yii::app()->request->getParam('download1C')) {
			FileMaker::createBillsList1CReport($model->dataProviderForNewAdminPanel($dtOptions),$reportMonth);
		} else {
			$this->render('view', array(
				'model' => $model,
				'dtOptions' => $dtOptions,
				'managerListData' => User::model()->getManagerList(),
				'reportMonth' => $reportMonth, //rf
			));
		}
	}
	
	public function actionLogAdminClinic()
	{
		$this->layout = null;
		$reportMonth = Yii::app()->request->getParam('report_month');
		$this->render('logAdminClinic', array(
				'reportMonth' => $reportMonth,
		));
	}
    
    public function actionGetEnvelope($addressId, $type = 'view', $print = true)
	{
        Address::$showInactive = true;
		Company::$showRemoved = true;
		$address = Address::model()->findByPk($addressId);
		if ($address === null)
			throw new CHttpException(404,'The requested page does not exist.');
        switch($type) {
            case 'download':
                FileMaker::createEnvelope($address, 'pdf');
                break;
            default:
                FileMaker::createEnvelope($address, '', $print);
                break;
        }
		return $model;
	}

	public function actionGetBill($download=false) {
		Doctor::$filterDeleted = false;
		Address::$showInactive = true;
		Company::$showRemoved = true;
		$reportMonth = Yii::app()->request->getParam('reportMonth');
		if (!$reportMonth)
			$reportMonth = date('Y-m', strtotime('-1 month'));
		$clinic = Address::model()->findByPk(Yii::app()->request->getParam('addressId'));
		$file = FileMaker::createHTMLBill($clinic, MyTools::getDateInterval($reportMonth), !$download);
		if(!empty($file) && !headers_sent()) {
			header('Content-Description: File Transfer');
			header('Content-Type: '.$file[2]);
			header('Content-Disposition: inline; filename="'.$file[0].'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . strlen($file[1]));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Expires: 0');
			header('Pragma: public');
				
			echo $file[1];
		}
	}
	
	public function actionGetMatching($download=false) {
		Doctor::$filterDeleted = false;
		Address::$showInactive = true;
		Company::$showRemoved = true;
		$withFaksimile = boolval(Yii::app()->request->getParam('withFaksimile'));
		$reportMonth = Yii::app()->request->getParam('reportMonth');
		if (!$reportMonth)
			$reportMonth = date('Y-m', strtotime('-1 month'));
		$clinic = Address::model()->findByPk(Yii::app()->request->getParam('addressId'));
		$file = FileMaker::createHTMLAppointmentReport($clinic, MyTools::getDateInterval($reportMonth), !$download, "pdf", true, $withFaksimile);
		if(!empty($file) && !headers_sent()) {
			header('Content-Description: File Transfer');
			header('Content-Type: '.$file[2]);
			header('Content-Disposition: inline; filename="'.$file[0].'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . strlen($file[1]));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Expires: 0');
			header('Pragma: public');
				
			echo $file[1];
		}
	}
	
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
