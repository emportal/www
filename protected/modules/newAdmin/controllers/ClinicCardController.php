<?php

class ClinicCardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/nocolumn';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'editManagerRelations', 'editCompanySalesInfo', 'getInfo'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		Address::$showInactive = true;
		Company::$showRemoved = true;
		
		$criteria = new CDbCriteria;
		$criteria->compare('id', $id, false);			
		$model = Company::model()->find($criteria);
		if(empty($model)) {
			throw new CHttpException(404,'Указанная запись не найдена');
		}
		
		$modelCompanyContact = CompanyContact::model();
		$modelCompanyContact->companyId = $id;
		
		$modelAddress = Address::model();
		$modelAddress->ownerId = $id;
		
		$modelUser = User::model();
		$modelUser->managerListType = 'full';
		$managerListData = $modelUser->getManagerList();
		
		$salesContractTypeData = SalesContractType::model()->findAll();
		$response = ["status"=>"", "text"=>"", "result"=>""];
		switch (Yii::app()->request->getParam('action')) {
		case "editCompanyPhones":
			$transaction=Yii::app()->db->beginTransaction();
			try {
				$phones = Yii::app()->request->getParam('phone');
				foreach ($phones as $key=>$phone) {
					$modelphone = null;
					if($key === "newrecord") { continue; }
					if(!empty($phone['id'])) {
						$modelphone = Phone::model()->findByPk($phone['id']);
					}
					if(!empty($phone['delete'])) {
						if($modelphone) {
							if(!$modelphone->delete()) {
								$response["status"] = "ERROR";
								$response["text"] .= MyTools::errorsToString($modelphone->getErrors());
								break;
							}
						}
					} else {
						if(!$modelphone) {
							$modelphone = new Phone();
						}
						$modelphone->ownerId = $id;
						if(isset($phone['addressId'])) $modelphone->addressId = $phone['addressId'];
						if(isset($phone['countryCode'])) $modelphone->countryCode = $phone['countryCode'];
						if(isset($phone['cityCode'])) $modelphone->cityCode = $phone['cityCode'];
						if(isset($phone['number'])) $modelphone->number = $phone['number'];
						$modelphone->beforeSave();
						if(!$modelphone->save()) {
							$response["status"] = "ERROR";
							$response["text"] .= MyTools::errorsToString($modelphone->getErrors());
							break;
						}
						$response["result"][$key] = $modelphone->getAttributes();
					}
				}
			} catch (Exception $e) {
				$response["status"] = "ERROR";
				$response["text"] = "".$e->getMessage();
			}
			if($response["status"] != "ERROR") {
		    	$transaction->commit();
				$response["status"] = "OK";
				$response["text"] = "Сохранено!";
			} else {
				$transaction->rollback();
			}
			break;
		default:
			break;
		}
		
		if (isset($_REQUEST['JSON']) && $_REQUEST['JSON'] == 1) {
			echo CJSON::encode($response);
		} else {
			$this->render('view',array(
				'model'=>$model,
				'modelCompanyContact'=>$modelCompanyContact,
				'managerListData'=>$managerListData,
				'modelAddress'=>$modelAddress,
				'salesContractTypeData'=>$salesContractTypeData,
				'response'=>$response,
			));
		}
		Yii::app()->end();
	}
	
	
	public function actionEditManagerRelations() {
		
		if(!empty($_POST['params'])) {
		
			$params = Yii::app()->request->getParam('params');
			
			$i=0;
			$keys = array_keys($params);
			foreach($keys as $key){
				$params[$i] = $_POST['params'][$key];
				$i++;
			}
			
			$userId = $params[0];
			$companyId = $params[1];
			
			if (!Yii::app()->user->model->hasRight('CHANGE_MANAGER_RELATIONS')) $this->redirect(['view', 'id' => $companyId]);
			
			$model = ManagerRelation::model()->findByAttributes(['companyId' => $companyId]);
			if (!$model) {
				$newManagerRelation = new ManagerRelation;
				$newManagerRelation->userId = $userId;
				$newManagerRelation->companyId = $companyId;
				if ($newManagerRelation->save()) {
					echo 'saved new model';
				} else {
					echo 'error occured while saving';
				}
			} else {
				if (!$userId) {//удаляем связь
					if ($model->delete()) {
						echo 'deleted current managerRelation instance';
					} else {
						echo 'error occured while deleting';
					}
				} else {
					$model->userId = $userId;
					if ($model->save()) {
						echo 'updated current model';
					} else {
						echo 'error occured while updating';
					}
				}
			}
			$this->redirect(['view', 'id' => $companyId]);
		} else {
			$this->redirect(['view', 'id' => $companyId]);
		}
	}
	
	
	public function actionEditCompanySalesInfo() {
		
		$params = Yii::app()->request->getParam('params');
		
		if(!empty($params)) {
			
			$model = CompanySalesInfo::model()->findByAttributes(['companyId'=>$params['companyId']]);
			if (!count($model)) {
				$model = new CompanySalesInfo;
			} else {
				//если уже существует инфа, не давать ее записывать неуполномоченным менеджерам				
				if ( !Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') AND ($model->company->managerRelations->userId != Yii::app()->user->id) ) {
					$this->redirect(['view', 'id' => $params['companyId']]);
					exit();
				}
			}
			
			$model->attributes = $params;
			
			if ($model->save()) {echo 'Все сохранено';} else {echo 'Возникла ошибка';}
			$this->redirect(['view', 'id' => $params['companyId']]);
		}
	}

	
	public function actionEditSalesContract() {
		
		Address::$showInactive = true;
		
		$params = Yii::app()->request->getParam('params');
		$addressModel = Address::model()->findByPK($params['addressId']);
		$companyId = $addressModel->company->id;
		$model = SalesContract::model()->findByAttributes(['addressId'=>$params['addressId']]);
		
		if (!count($model))
			$model = new SalesContract;
			
		if (!$model->isNewRecord AND $addressModel->isActive == 1 AND !Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')) {
			echo 'Не получается сменить тариф у адреса с действующим тарифом.';
			echo '<br><a href="view?id=' . $companyId . '#addresses">Вернуться назад</a>';
			//$this->redirect(['view', 'id' => $companyId, '#'=>'addresses']);
			exit();
		}
		
		if ( !Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') AND ($addressModel->company->managerRelations->userId != Yii::app()->user->id) ) {
			echo 'Не хватает полномочий для смены тарифа';
			echo '<br><a href="view?id=' . $companyId . '#addresses">Вернуться назад</a>';
			exit();
		}

		$oldSalesContractTypeId = $model->salesContractTypeId;
		$model->attributes = $params;
		if ($model->save()) {
			echo 'Все сохранено';
			try {
				$log = new LogSalesContract();
				$log->addressId = $model->addressId;
				$log->oldSalesContractTypeId = $oldSalesContractTypeId;
				$log->newSalesContractTypeId = $model->salesContractTypeId;
				$log->userId = Yii::app()->user->model->id;
				$log->datetime = date("Y-m-d H:i:s", time());
				$log->save();
			} catch (Exception $e) {
				MyTools::ErrorReport(date("Y-m-d H:i:s", strtotime("NOW"))." > Ошибка логирования в newAdmin/ClinicCardController->actionEditSalesContract()");
			}
		} else {
			echo 'Возникла ошибка';
		}
		
		$this->redirect(['view', 'id' => $companyId, '#'=>'addresses']);
	}
	
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		echo 'no index file here';
	}
}
