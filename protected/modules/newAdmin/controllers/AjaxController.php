<?php

/**
 * 	@var $instance ActiveRecord
 */
class AjaxController extends Controller {
	//public $layout = 'empty';

	/**
	 * Declares class-based actions.вы можете
	 */
	public function actions() {
		return array(
			'cities' => array(
				'class' => 'AutoCompleteAction',
				'model' => 'City',
				'attributeValue' => 'name',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	
	public function actionGetTips() {
		
		$type = Yii::app()->request->getParam('type');
		$nameLike = Yii::app()->request->getParam('nameLike');
		
		if (!$nameLike) {$nameLike = '/////';}
		
		$criteria = new CDbCriteria();
		
		$criteria->with = [
			'addresses' => [
				'together' => true
			]
		];
		
		//$criteria->compare('t.name', $nameLike, false);
		$criteria->compare('t.categoryId', 'a3969945e0f59fa84e0e7740c2e8cc87', true); //смотрим только медучреждения a3969945e0f59fa84e0e7740c2e8cc87
		//$criteria->compare('t.id', 'bfcb3955-065b-11e2-9aa0-e840f2aca94f');
		$criteria->addCondition('addresses.cityId LIKE "534bd8b8-e0d4-11e1-89b3-e840f2aca94f"'); //смотрим только питер
		$criteria->addCondition('t.name LIKE "'.$nameLike.'%"');
		$criteria->limit = 12;
		
		Address::$showInactive = true;
		$model = Company::model()->findAll($criteria);
		
		echo CJSON::encode(['success' => true, 'data' => $model]);
	}

	public function actionAcceptAction() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAction::model()->find("id=:id", array(':id' => $id));
		$model = Action::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			if (!$model) {
				$model = new Action();
			}
			$model->attributes = $agrModel->attributes;

			$model->link = "";
			if ($model->save()) {
				$agrModel->delete();
			} else {
				
			}



			$error = 0;
		}

		/*
		  if($agrModel && $model) {
		  $model->attributes=$agrModel->attributes;
		  if($model->save()) {
		  $agrModel->delete();
		  $error=0;
		  }
		  }
		 */

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptEmail() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrEmail::model()->find("addressId=:id", array(':id' => $id));
		$model = Email::model()->find("addressId=:id", array(':id' => $id));
		//Если пустая модель, значит новую запись создаем
		if (!$model) {
			$model = new Email();
		}
		if ($agrModel && $model) {
			$model->attributes = $agrModel->attributes;
			//Если пусто, значит при подтверждении удаляем
			if ($agrModel->name == "") {
				$model->delete();
				$agrModel->delete();
				$error = 0;
				//Если есть имя, значит заменяем
			} else {
				if ($model->isNewRecord) {
					$model->link = "";
					$model->save();
				} else {
					if ($model->save()) {
						$agrModel->delete();
					}
				}
				$error = 0;
			}
		}


		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptPhones() {
		/* id -> addressId */
		$id = Yii::app()->request->getParam('id');
		$error = 1;
		if ($id) {
			$ph = Phone::model()->findAll("addressId=:id", array(':id' => $id));
			foreach ($ph as $p) {
				PhoneCompanyUnit::model()->deleteAll("phoneId = :id", array(':id' => $p->id));
				$p->delete();
			}

			$agrModel = AgrPhone::model()->findAll("addressId=:id", array(':id' => $id));

			foreach ($agrModel as $phone) {
				$phone->syncThis = false;
				if ($phone->name == "remove") {
					$phone->delete();
					continue;
				}
				$m = new Phone();
				$m->attributes = $phone->attributes;
				$m->save();
				$phone->delete();
			}
			$error = 0;
		} else {
			$response['errors'][] = "id not found";
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptRef() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrRef::model()->find("ownerId=:id", array(':id' => $id));
		$model = Ref::model()->find("ownerId=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if (!$model) {
			$model = new Ref();
		}
		if ($agrModel && $model) {
			$model->attributes = $agrModel->attributes;
			//Если пусто, значит при подтверждении удаляем
			if ($agrModel->refValue == "") {
				$model->delete();
				$agrModel->delete();
				$error = 0;
				//Если есть имя, значит заменяем
			} else {
				if ($model->isNewRecord) {
					$model->link = "";
					$model->save();
					$agrModel->delete();
				} else {
					if ($model->save()) {
						$agrModel->delete();
					}
				}
				$error = 0;
			}
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptCompany() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrCompany::model()->find("id=:id", array(':id' => $id));
		$model = Company::model()->find("id=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if ($agrModel) {
			if (!$model) {
				$model = new Company();
			}
			$model->attributes = $agrModel->attributes;
			if ($model->isNewRecord) {
				$model->link = "";
				$model->save();
			} else {
				$model->save();
			}
			$agrModel->delete();

			$error = 0;
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptAddress() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAddress::model()->find("id=:id", array(':id' => $id));
		$model = Address::model()->find("id=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if ($agrModel) {
			if (!$model) {
				$model = new Address();
			}
			$model->attributes = $agrModel->attributes;
			if ($model->isNewRecord) {
				$model->link = "";
				$model->save();
			} else {
				$model->save();
			}
			$agrModel->delete();

			$error = 0;
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptExpert() {
		$id = Yii::app()->request->getParam('id');
		$addressId = Yii::app()->request->getParam('addressId');
		$error = 1;


		/* Перенос главной модели */
		$doctor = Doctor::model()->find("id=:id", array(':id' => $id));
		$doctorDeleted = Yii::app()->db->createCommand()
				->select('deleted')
				->from(Doctor::model()
						->tableName())
				->where('id=:id', [':id' => $id])
				->queryScalar();

		if ($doctorDeleted) {
			$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));

			AgrDoctorScientificDegree::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrSpecialtyOfDoctor::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrDoctorEducation::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrPlaceOfWork::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrDoctorServices::model()->deleteAll("doctorId=:id", array('id' => $id));

			if ($agrdoctor) {
				$agrdoctor->syncThis = false;
				$agrdoctor->delete();
				$error = 0;
			}


			if (!$error) {
				$response['success'] = true;
			} else {
				$response['success'] = false;
			}
			echo CJSON::encode($response);
			Yii::app()->end();
		}


		$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));
		if ($agrdoctor) {
			if (!$doctor) {
				$doctor = new Doctor();
			}
			$doctor->attributes = $agrdoctor->attributes;
			$doctor->name = implode(' ', array($doctor->surName, $doctor->firstName, $doctor->fatherName));
			if ($doctor->name && $doctor->isNewRecord) {
				
				$doctor->linkUrl = MyTools::makeLinkUrl($doctor->name);				
				if (Doctor::model()->findByAttributes(['linkUrl' => $doctor->linkUrl])) {
					$count = 0;
					do {
						$count++;
						$newLink = $doctor->linkUrl . '-' . $count;
					} while (Doctor::model()->findByAttributes(['linkUrl' => $newLink]));

					$doctor->linkUrl = $newLink;
				}
			}
			if ($doctor->save()) {
				/* перенос зависимых моделей */
				$dsd = AgrDoctorScientificDegree::model()->findAll("doctorId=:id", array('id' => $id));
				if ($dsd) {
					$vals = DoctorScientificDegree::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($dsd as $d) {
						$new = new DoctorScientificDegree();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$sod = AgrSpecialtyOfDoctor::model()->findAll("doctorId=:id", array('id' => $id));
				if ($sod) {
					$vals = SpecialtyOfDoctor::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($sod as $d) {
						$new = new SpecialtyOfDoctor();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$de = AgrDoctorEducation::model()->findAll("doctorId=:id", array('id' => $id));
				if ($de) {
					$vals = DoctorEducation::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($de as $d) {
						$new = new DoctorEducation();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$pow = AgrPlaceOfWork::model()->findAll("doctorId=:id", array(
					'id' => $id,
				));
				if ($pow) {
					$vals = PlaceOfWork::model()->findAll("doctorId=:id AND addressId=:aId", array(
						'id' => $id,
						'aId' => $addressId
					));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($pow as $d) {
						$new = new PlaceOfWork();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$ds = AgrDoctorServices::model()->findAll("doctorId=:id", array('id' => $id));
				if ($ds) {
					$vals = DoctorServices::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($ds as $d) {
						$new = new DoctorServices();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}
				$agrdoctor->syncThis = false;
				$agrdoctor->delete();

				$error = 0;
			} else {
				
			}
		}




		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	/* delete */

	public function actionDeclineAction() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAction::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineEmail() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrEmail::model()->find("addressId=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclinePhones() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;
		if ($id) {

			$agrModel = AgrPhone::model()->deleteAll("addressId=:id", array(':id' => $id));

			$error = 0;
		} else {
			$response['errors'][] = "error #01"; // no id
		}


		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineRef() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrRef::model()->find("ownerId=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineCompany() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrCompany::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineAddress() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAddress::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineExpert() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;


		$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));

		AgrDoctorScientificDegree::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrSpecialtyOfDoctor::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrDoctorEducation::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrPlaceOfWork::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrDoctorServices::model()->deleteAll("doctorId=:id", array('id' => $id));

		$doctor = Doctor::model()->findByPk($id);
		if (!$doctor && is_file($agrdoctor->photoPath)) {
			unlink($agrdoctor->photoPath);
		}
		if ($agrdoctor) {
			$agrdoctor->syncThis = false;
			$agrdoctor->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionChangeData() {
		$modelName = $model = $_POST['model'];
		$attribute = $_POST['attribute'];
		$newData = $_POST['newData'];
		$id = $_POST['rowId'];
		if ($attribute == "birthday") {
			$newData = date("Y-m-d", strtotime($newData));
		}
		/* Получаем объект модели через вызов МОДЕЛЬ::model() */
		$reflectionMethod = new ReflectionMethod($model, 'model');
		$model = $reflectionMethod->invoke(null);
		$object = $model->find('id=:id', array(':id' => $id));

		$response = array();
		if ($modelName == "AgrPhone") {
			$arr = explode(" ", $newData);
			$object->setAttribute('countryCode', $arr[0]);
			$object->setAttribute('cityCode', $arr[1]);
			$object->setAttribute('number', $arr[2]);
		}
		//Меняем значение атрибута
		$object->setAttribute($attribute, $newData);
		//проводим валидацию
		$object->syncThis = false;
		$object->validate(array($attribute));
		//Если нет ошибок, сохраняем, иначе выводим ошибки в консоль
		if ($object->getErrors()) {
			$response['error_message'] = $object->getErrors();
			$response['error'] = true;
		} else {
			if ($object->save()) {
				$response['error'] = false;
			} else {
				$response['error'] = true;
				$response['error_message'] = "Не получилось обновить запись";
			}
		}

		/* switch($modelName) {
		  case 'AgrAction':

		  break;
		  case 'AgrCompany':

		  break;
		  case 'AgrPhone':

		  break;
		  case 'AgrEmail':

		  break;
		  case 'AgrRef':

		  break;
		  case 'AgrDoctor':

		  break;
		  } */


		#var_dump($model->attributes);

		echo CJSON::encode($response);
	}

	public function actionUnblockUser($id) {

		$user = User::model()->find("id = :id", array(
			':id' => $id,
		));

		$user->isBlockedAppointment = 0;
		$user->blockedTime = 0;
		if ($user->save()) {
			$response['success'] = true;
		}

		echo CJSON::encode($response);
	}
	
	public function actionChangeAppointmentAttribute() {
		
		if (!Yii::app()->user->model->hasRight('ACCESS_CALL_CENTER_FUNCTIONALITY')) {
			$response = ['success' => 'false', 'msg' => 'Данная функциональность доступна только операторам call-центра'];
			echo CJSON::encode($response);
			Yii::app()->end();
			exit();
		}
		
		$appointmentLink = (int)Yii::app()->request->getParam('appointmentLink');
		$attributeName = Yii::app()->request->getParam('attributeName');
		$attributeNewValue = Yii::app()->request->getParam('attributeNewValue');
		$scenario = Yii::app()->request->getParam('scenario');
		
		$appointment = AppointmentToDoctors::model()->findByLink($appointmentLink);
		if ($scenario)
			$appointment->scenario = $scenario;

		if($attributeName == 'managerStatusId' && $attributeNewValue == '3' && $appointment->managerStatusId != 3)  {
			$appointment->sendDataGA();
		}
			
		$appointment->{$attributeName} = $attributeNewValue;
		$result = $appointment->update();
		
		$response = ['success' => $result];
		echo CJSON::encode($response);
	}
	
	public function actionChangeBillAttribute()
	{
		Address::$showInactive = true;
		
		$billId = (int)Yii::app()->request->getParam('billId');
		$attributeName = Yii::app()->request->getParam('attributeName');
		$attributeNewValue = Yii::app()->request->getParam('attributeNewValue');
		$scenario = Yii::app()->request->getParam('scenario');
		
		$bill = Bill::model()->findByPk($billId);
		if (
            $bill->address->company->managerRelations->user->id != Yii::app()->user->model->id
            AND 
            !Yii::app()->user->model->hasRight('CHANGE_MANAGER_RELATIONS')
		) {
			$response = ['success' => 'false', 'msg' => 'Вы можете работать со счетами только закрепленных за вами клиник
			 '// . $bill->address->company->managerRelations->user->id . ' != ' . Yii::app()->user->model->id
			];
			echo CJSON::encode($response);
			Yii::app()->end();
			exit();
		}
			
		if ($scenario)
			$bill->scenario = $scenario;
			
		$bill->{$attributeName} = $attributeNewValue;
		$result = $bill->update();
		$response = [
            'success' => $result,
            'leftToPay' => $bill->address->debtSum,
            'addressAttributes' => $bill->address->attributes,
        ];
        
		echo CJSON::encode($response);
	}
	
	public function actionFeedZoon()
    {
		$response = ['status'=>''];

		$id = Yii::app()->request->getParam('id');
		$value = ((Yii::app()->request->getParam('checked') === 'true') ? 1 : 0);
		$model = UserMedical::model()->findByAttributes(["id"=>$id]);
		if(is_object($model)) {
			if(Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') || $model->company->managerRelations->user->id == Yii::app()->user->model->id)
			{
				$model->feedZoon = $value;
				if($model->update()) {
					$response['status'] = "OK";
					$response['text'] = "Внесены изменения";
				} else {
					$response['status'] = "ERROR";
					$response['text'] = "Ошибка БД";
				}
			} else {
				$response['status'] = 'ERROR';
				$response['text'] = 'Не достаточно прав для внесения изменения';
			}
		} else {
			$response['status'] = 'ERROR';
			$response['text'] = 'Не найдено';
		}
		echo CJSON::encode($response);
	}
    
	public function actionChangeAddressAttribute($id)
	{
		Address::$showInactive = true;
		
		$attributeName = Yii::app()->request->getParam('attributeName');
		$attributeNewValue = Yii::app()->request->getParam('attributeNewValue');
		$scenario = Yii::app()->request->getParam('scenario');
		
		$address = Address::model()->findByPk($id);
		if (
            $address->company->managerRelations->user->id != Yii::app()->user->model->id
            AND 
            !Yii::app()->user->model->hasRight('CHANGE_MANAGER_RELATIONS')
		) {
			$response = ['success' => 'false', 'msg' => 'Вы не можете изменять свойства данного адреса'];
			echo CJSON::encode($response);
			Yii::app()->end();
		}
        
		if ($scenario)
			$address->scenario = $scenario;
			
		$address->{$attributeName} = $attributeNewValue;
		$result = $address->update();
		$response = [
            'success' => $result,
            'attributes' => $address->attributes,
        ];
        
		echo CJSON::encode($response);
	}
}
