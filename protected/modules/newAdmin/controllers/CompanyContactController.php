<?php

class CompanyContactController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/nocolumn';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('list', 'view', 'create'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionIndex()
	{
		$this->redirect(array('list'));
	}

	/**
	 * Lists all models.
	 */
	public function actionList()
	{
		$model = CompanyContact::model();
		$model->managerId = Yii::app()->request->getParam('manager_id');
		$model->companyNameLike = Yii::app()->request->getParam('company_name');
		$model->resultTypeId = Yii::app()->request->getParam('result_status');
		$model->time_period = Yii::app()->request->getParam('time_period');
		$model->task_status = Yii::app()->request->getParam('task_status');
		
		$dtOptions = [
			'region'=> Yii::app()->request->getParam('region')
		];
		
		$this->render('list',array(
			'model' => $model,
			'managerListData' => User::model()->getManagerList(),
			'dtOptions' => $dtOptions
		));
	}
	
	
	
	public function actionCreate()
	{
		$model = CompanyContact::model();
		
		if(isset($_POST['params']))
		{
			$params = Yii::app()->request->getParam('params');
			
			$newCompanyContact = new CompanyContact;
			$newCompanyContact->attributes = $_POST['params'];
			$newCompanyContact->authorId = Yii::app()->user->id;			
			if (!$newCompanyContact->managerId) $newCompanyContact->managerId = Yii::app()->user->id;
			
			if (!$newCompanyContact->companyId)
			{
				$newCompanyContact->companyId = NULL;
			} else {
				$newCompanyContact->companyName = '';
			}
			
			if ($newCompanyContact->validate())
			{
				$newCompanyContact->save();
				Yii::app()->user->setFlash('contactUpdated', 'Новый контакт успешно создан');
				$this->redirect(array('view', 'id' => $newCompanyContact->id));
			}
			//print_r($newCompanyContact->attributes);
		}
		
		$options = Yii::app()->request->getParam('options');
		if (isset($options['companyId'])) $options['companyName'] = Company::model()->findByPk($options['companyId'])->name;
		
		
		
		$this->render('create',array(
			'model'=>$model,
			'managerListData'=>User::model()->getManagerList(),
			'options'=>$options
		));
	}
	
	
	
	public function actionView($id)
	{
		$model = CompanyContact::model();
		$companyContact = CompanyContact::model()->findByPk($id); //resultTypeId
		if(isset($_POST['params']) && !empty($_POST['params']['plannedTime'])) {
		
			if ($companyContact->manager->id == Yii::app()->user->id) { //редактировать может только автор контакта
		
				$params = Yii::app()->request->getParam('params');
				//$companyContact->plannedTime = $params['plannedTime'];
				$companyContact->resultTypeId = $params['resultTypeId'];
				$companyContact->comment = $params['comment'];
				if (!$companyContact->save()) {
					echo print_r($companyContact);
					Yii::app()->user->setFlash('contactUpdatedError', 'При обновлении контакта произошла ошибка');
				} else {
					
					//если поставили результат "позвонить позже", сразу создаем новый контакт из исходного
					if ($companyContact->resultTypeId == '4')
					{
						$flashMessage = 'Внимание! При сохранении контакта с результатом "Позвонить позже" 
						необходимо создать новый контакт и НАЗНАЧИТЬ НОВОЕ ВРЕМЯ ЗВОНКА';
						Yii::app()->user->setFlash('contactUpdatedError', $flashMessage);
						$newContactString = $companyContact->newContactString;
						$this->redirect(array('create?' . $newContactString));
					} else {
						Yii::app()->user->setFlash('contactUpdated', 'Изменения успешно сохранены');
					}
				}
			} else {
				Yii::app()->user->setFlash('contactUpdatedError', 'Ошибка: Редактировать уже созданный контакт может только автор контакта');
			}
		}
		
		$this->render('view',array(
			'model'=>$model,
			'companyContact'=>$companyContact,
			'managerListData'=>User::model()->getManagerList(),
		));
	}
	
	public function getTaskHeader($task_status_value)
	{
		$ts_all = ['name' => 'показать c задачами и без', 'value' => ''];
		$ts_with_taks_only = ['name' => 'показать только с задачами', 'value' => '1'];
		$new_task_status = ($task_status_value == 1) ? $ts_all : $ts_with_taks_only;
		return '<a href="#" onclick="change_filter(\'input_task_status\', \'' . $new_task_status['value'] . '\', 1); return false;">' . $new_task_status['name'] . '</a>';
	}
}