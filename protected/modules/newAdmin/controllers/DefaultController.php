<?php

class DefaultController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow',
				'actions' => array('login', 'error', 'logout' ),
				'users' => array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('index', 'view', 'create', 'update', 'delete','gallery', 'addAlbum','delAlbum'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionIndex() {
		if (Yii::app()->user->model->hasRight('ACCESS_NEW_ADMIN')) {
			$this->redirect(array('companyContact/list'));
			$this->render('index');
		} else {
			$this->redirect(array('../'));
		}
	}

	public function actionLogin() {
		if (Yii::app()->user->model->hasRight('ACCESS_NEW_ADMIN')) {
			$this->redirect(array('companyContact/list'));
		}

		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
				$this->redirect(array('index'));
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}

	public function actionGallery() {
		$albums = Album::model()->findAll();
		$name = Yii::app()->request->getParam('name');

		/* Истинно, если не указан определенный альбом */
		if (empty($name)) {
			
		} else {
			$album = Album::model()->findByAttributes([
				'name' => $name
			]);
			if (!$album) {
				unset($name);
			}
		}

		$this->render('gallery', [
			'albums' => $albums,
			'album' => $album,
			'name' => $name
		]);
	}

	public function actionAddAlbum() {
		$model = new Album();
		$model->syncThis = false;
		$data = $_POST[get_class($model)];
		if ($data) {
			$model->attributes = $data;
			if ($model->save()) {
				$this->redirect("gallery");
			}
		}
		$this->render('addAlbum', [
			'model' => $model
		]);
	}

	public function actionDelAlbum() {
		Yii::import("application.modules.gallery.models.*");
		
		$name = Yii::app()->request->getParam('name');
		$model = Album::model()->findByAttributes([
			'name' => $name,
		]);
		
			

		if (!empty($model)) {
		
			$model->syncThis = false;
		
			$gallery = Gallery::model()->findByAttributes([
				'owner' => 'gallery' . $model->name
			]);
		
			$gallery->delete();
			if ($model->delete()) {
				$this->redirect("gallery");
			}
		}
		echo 2;
		$this->redirect("gallery");
		Yii::app()->end();
	}

	public function actionLogout() {
		Yii::app()->user->logout();

		$this->redirect(Yii::app()->homeUrl);
	}

}
