<?php

class RegistryListController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/nocolumn';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'add', 'createReport'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$this->redirect(array('view'));
	}

	public function actionView()
	{
		$model = AppointmentToDoctors::model();
		$model->managerId = Yii::app()->request->getParam('manager_id');
		$model->companyNameLike = Yii::app()->request->getParam('company_name');
		$model->apt_status = Yii::app()->request->getParam('apt_status');
		$model->time_period = Yii::app()->request->getParam('time_period');
		$model->visit_time_period = Yii::app()->request->getParam('visit_time_period');
		$model->app_type = Yii::app()->request->getParam('app_type');
        $model->patientId = Yii::app()->request->getParam('patient_id');
		
		$visitType = Yii::app()->request->getParam('visit_type');
		
		$dtOptions = [
			'region' => Yii::app()->request->getParam('region'), //Yii::app()->session['selectedRegion'],
			'manStatus' => Yii::app()->request->getParam('man_status'),
			'showServices' => ($visitType == 'service') ? true : (($visitType == '') ? 'withVisits' : false),
			'userId' => Yii::app()->request->getParam('patient_id'),
			'has_prePaid' => Yii::app()->request->getParam('has_prePaid'),
		];
		
		$createReport = Yii::app()->request->getParam('createReport');
		$companyId = Yii::app()->request->getParam('cid');
		if ($companyId)
		{
			$dtOptions['companyId'] = $companyId;
			$company = Company::model()->findByPk($companyId);
			$companyName = $company->name;
		}
		if ($createReport)
		{
			$dtOptions = array_merge($dtOptions, [
				'pageSize' => '10000',
			]);
			$dataProvider = $model->dataProviderForNewAdminPanel($dtOptions);
			FileMaker::createAppointmentReport($dataProvider);
			exit();
		}
		
		$this->render('view',array(
			'model' => $model,
			'managerListData' => User::model()->getManagerList(),
			'dtOptions' => $dtOptions,
		));
	}
	
	public function actionAdd($id) {
		
		if (!Yii::app()->user->model->hasRight('ACCESS_CALL_CENTER_FUNCTIONALITY'))
			$this->redirect(array('view'));
		
		$company = Company::model()->findByLink($id);
		$params = [
			'company' => $company
		];
		$this->render('add', $params);
	}
	
	public function actionCreateReport($clinicId = null) {
		//echo 'OK';
		//$criteria = new CDbCriteria();
		//$model = AppointmentToDoctors::model();
		//Yii::app()->end();
	}
}
