<?php

class ClinicListController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/nocolumn';
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'setManagers', 'assignClinics', 'ChangeManagers', 'add', 'addAddress', 'debts'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionIndex()
	{
		$this->redirect(array('view'));
	}


	/**
	 * Lists all models.
	 */
	public function actionView()
	{
		$model = Company::model();
		//$model->attributes = Yii::app()->request->getParam(get_class($model));
		$model->filterSamozapis = 'no-samozapis';
		$model->managerId = Yii::app()->request->getParam('manager_id');
		$model->nameLike = Yii::app()->request->getParam('company_name');
		$model->companyTypeArtificial = Yii::app()->request->getParam('company_type');
		
		$dtOptions = [
			'showAddressCount' => 1,
			'region' => Yii::app()->session['selectedRegion']
		];
		
		$dataProvider = $model->companyDataProviderForNewAdminPanel($dtOptions);
		
		$this->render('view',array(
			'model' => $model,
			'managerListData' => User::model()->getManagerList(),
			'dataProvider' => $dataProvider,
		));
	}

	public function actionSetManagers()
	{
		$model = Company::model();
		$model->managerId = -1; //все без менеджеров
		$model->companyTypeArtificial = 'all'; //'inactive';
		$dataProvider = $model->companyDataProviderForNewAdminPanel(['pageSize' => 2000, 'region' => 'moskva']);
		
		foreach($dataProvider->getData() as $clinic)
		{
			$clinics[] = $clinic->name;
			$randVar = round(mt_rand(0,100));
			if ($randVar < 67) {$newManagerId = 'EMPORTAL-0dbf-2c16-7a80-5f4e17e63ab7'; $c[0]++;} //милан
			//if ($randVar>=50 AND $randVar<75) {$newManagerId = 'EMPORTAL-8447-941c-545c-bcf7064466a3'; $c[1]++;} //патимат
			if ($randVar >= 67) {$newManagerId = 'EMPORTAL-dad9-82ec-223b-d578261a8699'; $c[2]++;} //таня
			
			$newRelation = new ManagerRelation;
			$newRelation->userId = $newManagerId;
			$newRelation->companyId = $clinic->id;
			//$newRelation->save();
		}
		
		echo 'count = '.count($clinics);
		echo '<br>Милан count = '.$c[0];
		echo '<br>Патимат count = '.$c[1];
		echo '<br>Таня count = '.$c[2];
		echo '<br>allCount = '.($c[0] + $c[1] + $c[2]);
	}
	
	public function actionChangeManagers()
	{
		$managerEmail = 'tatiana.m@emportal.ru'; //ta@emportal.ru //tatiana.m@emportal.ru
		$newManagerEmail = 'ta@emportal.ru';
		
		$manager = User::model()->findByAttributes(['email' => $managerEmail]);
		$newManager = User::model()->findByAttributes(['email' => $newManagerEmail]);
		//echo '<br>found manager: ' . $manager->name;
		//echo '<br>new manager: ' . $newManager->name;
		
		Address::$showInactive = true;
		
		$model = Company::model();
		$criteria = new CDbCriteria();
		$criteria->with = [
			'managerRelations' => [
				//'joinType' => 'INNER JOIN'
			],
			'address'
		];
		$criteria->compare('managerRelations.userId', $newManager->id);
		$criteria->compare('address.samozapis', '1', false);
		//$criteria->compare('t.denormCitySubdomain', $manager->id);
		//$criteria->addCondition('t.denormCitySubdomain IS NOT NULL');
		//$criteria->addCondition('t.id NOT IN (SELECT companyId FROM managerRelations)');
		
		$companies = $model->findAll($criteria);
		echo '<br> count($companies) = ' . count($companies);
		
		foreach($companies as $company)
		{
			$company->managerRelations->delete();
			/*$newRelation = new ManagerRelation;
			$newRelation->userId = $newManager->id;
			$newRelation->companyId = $company->id;
			if ($newRelation->save())
			{
				echo '<br>ok, ' . $company->name . '(' . $company->denormCitySubdomain . ')';
			} else {
				echo '<br> an error occured';
				echo $company->name . '(' . $company->denormCitySubdomain . ')';
			}*/
		}
		
		for($i=0; $i<20; $i++)
		{
			echo '<br>Company: ' . $companies[$i]->name . '; subdomain = ' . $companies[$i]->denormCitySubdomain;
		}
	}
	
	public function actionAdd()
	{
		$model = new Company();
		$data = Yii::app()->request->getParam(get_class($model));
		
		if ($data)
		{
			if (!empty(Yii::app()->session['selectedRegion']))
			{
				$data['denormCitySubdomain'] = Yii::app()->session['selectedRegion'];
			}
			$data['nameENG'] = 'viaNewAdmin';
			$model->attributes = $data;
			if ($model->save())
			{
				$newRelation = new ManagerRelation;
				$newRelation->userId = Yii::app()->user->model->id;
				$newRelation->companyId = $model->id;
				$newRelation->save();
				$this->redirect(['addAddress', 'id' => $model->id]);
			}
		}
		
		$this->render('add', [
			'model' => $model
		]);
	}
	
	public function actionAddAddress($id)
	{
		$company = $this->loadModel($id);
		$model = new Address();
		$data = Yii::app()->request->getParam(get_class($model));
		
		if ($data)
		{
			$model->attributes = $data;
			$model->ownerId = $id;
			$model->countryId = '534bd8ae-e0d4-11e1-89b3-e840f2aca94f'; // ru
			$model->name = $model->street;
			if (!empty($model->houseNumber))
                $model->name .= ', ' . $model->houseNumber;

			$metroList = [];
			if (is_array($_REQUEST['metroList'])) 
				foreach ($_REQUEST['metroList'] as $metro) {
					$metroList[$metro['value']] = ['value' => $metro['value'], 'distance' => $metro['distance']];
				}
			if ($model->save()) {
				if (empty($company->addressId)) {
					$company->addressId = $model->id;
					$company->save();
				}
				if (!empty($metroList)) $model->saveMetroList($metroList);
				
				$this->redirect(['ClinicCard/view', 'id' => $company->id]);
			}
		} else {
			if ($subdomain = Yii::app()->session['selectedRegion'])
			{
				$city = City::model()->findByAttributes(['subdomain' => $subdomain]);
				$model->cityId = $city->id;
			}
		}

		$this->render('addAddress', [
			'model' => $model,
			'company' => $company
		]);
	}
	
	public function loadModel($id)
	{
		$this->_model = Company::model()->findByPk($id);

		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}
    
    public function actionDebts()
    {
        //1. получаем клиники с долгами
		$debtData = Address::getDebtDataForAllAddresses();
        $renderData = [];
        
        //var_dump($debtData);
        //exit;
        
        //2. собираем данные для рендера
        foreach($debtData['addresses'] as $index => $address) {
            
            if (Yii::app()->request->getParam('showInactive') != true AND !$address->isActive)
                continue;
            
            $chunk['companyName'] = $address->company->name;
            $chunk['name'] = $address->shortName;
            $chunk['url'] = $address->getPagesUrls(true);
            $chunk['debtSum'] = $debtData['debtSums'][$index];
            $chunk['debtMonths'] = [];
            foreach($address->debtData['notPayedBills'] as $notPayedBill)
                $chunk['debtMonths'][] = $notPayedBill['reportMonth'];
            $chunk['debtMonths'] = implode(', ', $chunk['debtMonths']);
            $chunk['mailList'] = implode(', ', $address->mailListForAppReports);
            
            $render_data[] = $chunk;
        }
        
        $this->render('debts', [
            'render_data' => $render_data,
		]);
    }
}
