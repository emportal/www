<?php

class PaymentController extends Controller
{
	public $layout='/layouts/nocolumn';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'create', 'update', 'approve', 'log1CPayment', 'balance'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$this->redirect(array('view'));
	}
	
	public function actionView()
	{
		$model = new Payment();
		$model->attributes = $_REQUEST['Payment'];
		Address::$showInactive = true;
		Company::$showRemoved = true;
		$this->render('view', array(
			'model' => $model,
			'managerListData' => User::model()->getManagerList(),
		));
	}
	
    public function actionUpdate($id) 
    { 
        $model=$this->loadModel($id);
        
        // Uncomment the following line if AJAX validation is needed 
        // $this->performAjaxValidation($model); 

        if(isset($_POST['Payment'])) 
        { 
            $model->attributes=$_POST['Payment'];
            try {
	            if($model->save()) {
	                $this->redirect(array('view','id'=>$model->id)); 
	            }
	            else {
	            	print_r($model->getErrors());
	            }
            } catch (Exception $e) {
            	$model->addError('',$e->getMessage());
            }
        } 

        $this->render('update',array( 
            'model'=>$model, 
        )); 
    }
    
    public function actionCreate()
    {
    	$model=new Payment;
    	$model->attributes=$_REQUEST['Payment'];
    	
    	// Uncomment the following line if AJAX validation is needed
    	// $this->performAjaxValidation($model);
    
    	if(isset($_POST['Payment']))
    	{
            $model->statusId = Payment::STATUS_APPROVED;
    		$model->attributes=$_POST['Payment'];
    		if($model->save())
    			$this->redirect(array('view','id'=>$model->id));
    	}
    
    	$this->render('create',array(
    			'model'=>$model,
    	));
    }
	
    public function actionApprove() 
    { 
    	$id = $_REQUEST['id'];
        $model=$this->loadModel($id); 
        $response = [
        		'success' => false,
    	];
        
        if(Yii::app()->request->getRequestType() == "POST") 
        { 
        	$statusId = $_REQUEST['statusId'];
            $model->statusId = $statusId; 
            if($model->save()) {
	        	$response['success'] = true;
	        	$response['statusId'] = $model->statusId;
            } else {
		        $response['success'] = false;
		        $response['errors'] = $model->getErrors();
            }
        } else {
	        $response['success'] = false;
	        $response['errors'] = ["Не верный запрос!"];
        }
        
        echo CJSON::encode($response);
        return true;
    } 
	
	public function actionLog1CPayment() {
		$model = Log1CPayment::model()->findByPk($_REQUEST['id']);
		if(!is_object($model)) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		$this->render('log1CPayment', array(
			'model' => $model,
		));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Payment the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Payment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionBalance($clinicId)
	{
		Company::$showRemoved = true;
		Address::$showInactive = true;
		$clinic = Address::model()->findByAttributes(['id'=>$clinicId]);
		$model = $clinic->getBalance();
		$this->renderPartial('adminClinic.views.balance.index', array('model' => $model, 'clinic' => $clinic, 'from'=>'newAdmin'));
	}
}
