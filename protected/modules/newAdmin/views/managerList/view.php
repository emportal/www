<?php
/* @var $this ClinicListController */
/* @var $model Address */
?>

<div>
<h1>Назначения сотрудник-клиника</h1>
<div style="float: right; margin: 10px 0 20px 0;">
	Показать КЛИНИКИ СОТРУДНИКА
    <select id="input_manager_id" onchange="submit_input_forms();">
        <option value="0">Все менеджеры</option>
		<option value="-1">Без менеджеров</option>
		<?php foreach($managerListData as $key=>$data): ?>
			<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
		<?php endforeach; ?>
    </select>	
</div>
</div>

<div style="clear: both;">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $model->companyDataProviderForNewAdminPanel(['oneOffer'=>1, 'showAddressCount'=>1]),
	#'filter' => $model,
	#'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed',
	'columns' => array(
		[
			'name' => 'name',
			'header' => 'Название клиники',
			'value' => '$data->name',
			'filter' => false,
		],
		[
			'name' => 'name',
			'header' => 'Менеджер',
			'value' => '$data->managerRelations->user->id ? $data->managerRelations->user->name." ".$data->managerRelations->user->email : "-"',
			'filter' => false,
		],
		[
			'name' => 'name',
			'value' => '', 
			'header' => 'Действия',
			'sortable' => true,
		],

	),
));
?>
</div>


<script>
//$(document).ready(function() {
	var data_values = {
		
		'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
		'user_id': 0
	};
	
function update_input_forms() {
	
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = 0;
		if (typeof $('#input_'+key) != 'undefined') {
			
			$('#input_'+key).val(data_values[key]);
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		
		if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
			
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != 0) {query += '&' + key + '=' + data_values[key];}
		}
	}
	url = '/newAdmin/clinicList/index?'+query;
	document.location.href = url;
}

update_input_forms();
//});
</script>