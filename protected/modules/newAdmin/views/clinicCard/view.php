<?php
/* @var $this ClinicListController */
/* @var $model Address */
?>

<h1 class="smi_link">
    <a href="<?= is_object($model->address) ? $model->address->getPagesUrls(true, false) : '#' ?>" target="_blank">
        <?= $model->name ?>
    </a>
</h1>

<h2>Менеджер</h2>
<form enctype="multipart/form-data" method="post" action="editManagerRelations">
	<select name="params['userId']" class="btn">
		<option value="">Нет</option>
		<?php foreach($managerListData as $key=>$data): ?>
			<?php if ($key == $model->managerRelations->userId) {$selected = 'selected';} else {$selected = '';} ?>
			<option value="<?=$key?>" <?=$selected?>><?=CHtml::encode($data)?></option>
		<?php endforeach; ?>
	</select>
	<input type="hidden" name="params['companyId']" value="<?= $model->id ?>">
	<input type="submit" value="Сохранить" class="btn btn-success">
</form>

<?php
$emails = $model->emails;
$phones = $model->phones;
$staff = $model->contactPersonCompanies;
$refs = $model->refs; //ссылки компанииs
?>

<div class="extaInfoBlock">
	<?php if ($emails) : ?>
	<h2>E-mail'ы из 1С</h2>
	<table class="form-table valign-middle table-striped table-bordered table-condensed">
		<tr>
			<td style="width: 250px;">Адрес</td>
			<td>Email</td>
		</tr>
		<?php
		foreach($emails as $email)
		{
			$addressName = (!$email->address->shortName) ? '------------------------' : $email->address->shortName;
			echo '<tr><td>' . $addressName . '</td><td><input type="text" class="autowidth" value="' . $email->name . '" readonly></td></tr>';
		}
		?>
	</table>
	<?php endif; ?>
	
	<h2>Телефоны из 1С</h2>
	<form id="editCompanyPhones" enctype="multipart/form-data" method="post" action="">
		<table class="form-table valign-middle table-striped table-bordered table-condensed" style="width: 100%;">
			<tr>
				<td style="width: 175px;">Адрес</td>
				<td>Телефон</td>
			</tr>
			<?php
			$phones["newrecord"] = null;
			foreach($phones as $key=>$phone) : ?>
			<tr id="row<?=$key?>" style="<?=($key==='newrecord')?'display: none!important;':''?>">
				<td>
					<?=
					CHtml::hiddenField('phone['.$key.'][id]', $phone->id);
					?>
					<?php 
					if(is_array($model->addresses)) {
					echo CHtml::dropDownList('phone['.$key.'][addressId]', $phone->address->id,
						CHtml::listData($model->addresses, 'id', 'shortName'),
						array('empty' => '(выберите адрес)', 'style' => 'width:175px!important;'));
					}
					?>
				</td>
				<td>
					<?=
					CHtml::textField('phone['.$key.'][countryCode]', $phone->countryCode,
						array('maxlength' => 3, 'style' => 'width:35px!important; padding:0!important; text-align:center!important;'));
					?>
					<?=
					CHtml::textField('phone['.$key.'][cityCode]', $phone->cityCode,
						array('maxlength' => 10, 'style' => 'width:45px!important; padding:0!important; text-align:center!important;'));
					?>
					<?=
					CHtml::textField('phone['.$key.'][number]', $phone->number,
						array('maxlength' => 15, 'style' => 'width:90px!important; padding:0!important; text-align:center!important;'));
					?>
				</td>
				<td>
					<?= CHtml::checkBox('phone['.$key.'][delete]', false, array( "class"=>"remove_selected", "style"=>"width: 15px!important; display:none;" )); ?>
					<?= CHtml::label("Удалить","phone_".$key."_delete", array('class'=>'')); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
		<br>
		<a class="add_phone" style="cursor: pointer;float:right;">Добавить телефон</a>
		<input type="hidden" name="action" value="editCompanyPhones">
		<button type="submit" class="btn btn-success">Сохранить</button>
		<div class="formresponse" style="float:right; margin: 0 15px 0 0; display:none;"></div>
		<script type="text/javascript">
		$(document).ready(function() {
			initAjaxForm($('#editCompanyPhones'));
		});
		function initAjaxForm(form) {
			form.find("[type=submit]").live('click', function() {
				var formdata = form.serializeArray();
				var data = { JSON:1 };
				for(obj in formdata) { data[formdata[obj].name] = formdata[obj].value; }
				form.find(".formresponse").text('');
				form.find(".formresponse").hide();
				$.ajax({
					url: window.location.pathname + window.location.search,
					async: true,
					data: data,
					dataType: 'json',
					type: "POST",
					success: function(response) {
						if(response.status == "OK") {
							form.find(".remove_selected:checked").closest("tr").remove();
							form.find(".formresponse").css('color', 'green');
							form.find(".formresponse").html(response.text);
							setTimeout(function(){ form.find(".formresponse").fadeOut(500); },5000);
							for(var key in response.result) {
								var row = $("#row"+key);
								if(row.length > 0) {
									var rowid = $('#phone_'+key+'_id');
									if(rowid.length > 0){
										rowid.val(response.result[key]['id']);
									}
								}
							}
						} else {
							this.error(null,response.text,null);
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						form.find(".formresponse").css('color', 'red');
						form.find(".formresponse").html('Ошибка: ' + textStatus);
					},
					complete: function() {
						form.find(".formresponse").fadeIn(500);
					},
				});
				return false;
			});

			form.find(".remove_selected").live('click', function() {
				$(this).closest("tr").fadeOut(150);
			});

			form.find(".add_phone").live('click', function() {
				var number = $('#editCompanyPhones').find("tr").length + 1;
				while($("#row"+number).length > 0) {
					number++;
				}
				var row = form.find("#rownewrecord").clone();
				row.attr("id","row"+number);
				row.html(row.html().replace(/newrecord/g,number));
				form.find("table").append(row)
				row.fadeIn(500);
			});
		}
		</script>
	</form>
	
	<?php if ($staff) : ?>
	<h2>Контакты из 1С</h2>
	<table class="form-table valign-middle table-striped table-bordered table-condensed">
		<tr>
			<td style="width: 250px;">ФИО</td>
			<td>Телефон/email</td>
		</tr>
		<?php
		foreach($staff as $staffMember)
		{
			$staffPosition = (!$staffMember->position) ? '' : ' (' . $staffMember->position . ') ';
			$contacts = [];
			if ($staffMember->phonesString) $contacts[] = $staffMember->phonesString;
			if ($staffMember->emailString) $contacts[] = $staffMember->emailString;
			
			echo '<tr><td>' . $staffMember->name . $staffPosition . '</td>';
			echo '<td><input type="text" class="autowidth" value="' . implode(', ', $contacts) . '" readonly></td></tr>';
		}
		?>
	</table>
	<?php endif; ?>
	
	<?php if ($refs) : ?>
	<h2>Ссылки из 1С</h2>
	<table class="form-table valign-middle table-striped table-bordered table-condensed">
		<tr>
			<td style="width: 250px;">Название</td>
			<td>Ссылка</td>
		</tr>
		<?php
		foreach($refs as $ref)
		{
			echo '<tr><td>' . $ref->name . '</td><td><input type="text" class="autowidth" value="' . $ref->refValue . '" readonly></td></tr>';
		}
		?>
	</table>
	<?php endif; ?>
</div>

<h2>Служебная информация</h2>
<form enctype="multipart/form-data" method="post" action="editCompanySalesInfo">
	<table class="form-table">
		<tr>
			<td>
				Категория клиента
			</td>
			<td>
				<select id="salesCategory" name="params[salesCategory]">
					<option value="">Нет данных</option>
					<option>Обычный</option>
					<option>Ключевой</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 140px;">
				Имя ЛПР
			</td>
			<td>
				<input type="text" name="params[lprName]" value="<?=$model->companySalesInfo->lprName?>">
			</td>
		</tr>
		<tr>
			<td>
				Должность ЛПР
			</td>
			<td>
				<input type="text" name="params[lprPosition]" value="<?=$model->companySalesInfo->lprPosition?>">
			</td>
		</tr>
		<tr>
			<td>
				Телефон ЛПР
			</td>
			<td>
				<input type="text" name="params[lprPhone]" value="<?=$model->companySalesInfo->lprPhone?>">
			</td>
		</tr>
		<tr>
			<td>
				Используемое ПО
			</td>
			<td>
				<input type="text" name="params[poType]" value="<?=$model->companySalesInfo->poType?>">
			</td>
		</tr>
		<tr>
			<td>
				Комментарий
			</td>
			<td>
				<textarea name="params[managerComment]" class="h100"><?=$model->companySalesInfo->managerComment?></textarea>
			</td>
		</tr>
		<tr>
			<td>
				E-mail
			</td>
			<td>
				<input type="text" name="params[email]" value="<?=$model->companySalesInfo->email?>" class="custom-tooltip-parent">
                <div class="custom-tooltip w280">
                    На этот адрес будут приходить письма со документами от ЕМП (счета, сверки, акты и пр.). 
                    Можно использовать несколько e-mail адресов, разделенных запятыми или пробелами.
                    Тогда письма будут приходить на все указанные адреса.
                </div>
			</td>
		</tr>
		<tr>
			<td>
				Web-сайт
			</td>
			<td>
				<input type="text" name="params[website]" value="<?=$model->companySalesInfo->website?>">
			</td>
		</tr>
	</table>

	<input type="hidden" name="params[companyId]" value="<?=$model->id?>">
	<input type="submit" value="Сохранить" class="btn btn-success">
</form>

<!-- <h2>Юридическая информация</h2>
ИНН, ОГРЭН, ЕГРЮЛ, Юр. адрес, Физ. адрес, факт. адрес и т.п.

<h2>Контактные лица</h2>
Список лиц, с кем общаемся, возможно имеет смысл генерить автоматически? Имя, телефон, емаил, должность, привязка к адресу?? -->

<h2 style="display: inline;">История контактов</h2>
<a href="../companyContact/create?&options[companyId]=<?=$model->id?>&options[contactTypeId]=2&options[managerId]=<?=$model->managerRelations->userId?>&options[personName]=<?=$model->companySalesInfo->lprName?>&options[personPhone]=<?=$model->companySalesInfo->lprPhone?>" style="padding: 0 0 0 20px;"> Cоздать новый</a>

<?php if ($companyContactsAmount = count($model->companyContacts)): ?>
	<div>
		<a href="../companyContact/list?company_name=<?=urlencode($model->name)?>">Посмотреть (<?=$companyContactsAmount?>)</a>
	</div>	
<?php endif; ?>

<?php if (is_object($model) AND $companyAppointmentAmount = $model->getAppointmentsCount()): ?>
	<h2>Регистратура</h2>
	<a href="../registryList/view?company_name=<?=urlencode($model->name)?>">
		Посмотреть (<?=$companyAppointmentAmount?>)
	</a>
	<?php if ($appointmentsInLastMonth = $model->hasAppointments(MyTools::getDateInterval('lastMonth'))): ?>
	<div>
		<h3>Отчет по записям за прошлый месяц (дошедшие + не обработанные)</h3>
		<a href="../registryList/view?cid=<?=$model->id?>&time_period=<?=MyTools::getDateInterval('lastMonth')?>&man_status=r">
			Просмотреть (<?=$appointmentsInLastMonth?>)
		</a>
		|
		<a href="../registryList/view?cid=<?=$model->id?>&time_period=<?=MyTools::getDateInterval('lastMonth')?>&man_status=r&createReport=true">
			Скачать
		</a>
	</div>
	<?php endif; ?>
<?php endif; ?>

<div class="clearfix"></div>
<h2>
	<a class="nl">Адреса клиники (+ЛКК)</a>
</h2>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $modelAddress->dataProviderForNewAdminPanel(),
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small valign-middle',
	'columns' => array(
		[
			'name' => 'name',
			'value' => function($data) {
					return '<a href="' . $data->getPagesUrls(true, false) .'" target="_blank">' . $data->name . '</a>';
				},
			'type' => 'raw',
			'sortable' => false,
		],
		[
			'header' => 'Статус',
			'value' => '$data->isActive ? "Активно" : "Неактивно"',
		],
		[
			'header' => 'Тариф',
			'value' => function($data) use ($salesContractTypeData) {
					$result = '-';
					if ($data->salesContract) $result = $data->salesContract->salesContractType->description;
					
					$result = '';
					
					foreach($salesContractTypeData as $key=>$value) {
						$selected = '';
						if ($value->id == $data->salesContract->salesContractType->id) $selected = ' selected';
						$result .= '<option'.$selected.' value="'.$value->id.'">'.$value->description.'</option>';
					}
					
					$result = '<select name="params[salesContractTypeId]" class="salesContractSelect">'.$result.'</select>';
					$result .= '<input type="hidden" name="params[addressId]" value="'.$data->id.'">';
					$result .= '<button type="submit" class="btn btn-success no-display" value="OK" title="Сохранить изменения"></button>';
					$result = '<form style="margin: 0; padding: 0;" enctype="multipart/form-data" method="post" action="editSalesContract">'.$result.'</form>';
					
					return $result;
				},
			'htmlOptions' => [
				'style'=> 'width:240px'
			],
			'type' => 'raw'
			
		],
		[
			'header' => 'Врачей',
			'value' => 'count($data->doctors)'
		],
		[
			'header' => 'Приняли оферту',
			'value' => '$data->userMedicals->agreementNew ? $data->userMedicals->createDate : "не принимали"',
			'htmlOptions' => [
				'style'=> 'width:140px'
			]
		],
		[
			'header' => 'Выгружать в zoon',
			'value' => function($data) {
				return CHtml::CheckBox("feedZoon[{$data->userMedicals->id}]",($data->userMedicals->feedZoon == true), array ('class'=>'feedZoon'));
			},
			'htmlOptions' => [ ],
			'type' => 'raw'
		],
		[
			'name' => 'none',
			'header' => 'ЛКК',
			'value' => function($data) {
					$result = [];
					if(Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') OR $data->company->managerRelations->user->id == Yii::app()->user->id) {
						if ($data->userMedicals) {
							$result[] = '<a href="/adminClinic/default/login?id='.$data->userMedicals->id.'" target="_blank">Вход</a>';
						} else {
							$result[] = '-';
						}
					}
					/*{
						if (!$data->userMedicals->agreementNew) {
							$result[] = "Не принят договор.";
						}
						if(!$data->isActive) {
							$result[] = "Неактивна клиника.";
						}
					}*/
					return implode("<br>", $result);
				},
			'filter' => false,
			'type' => 'raw'
		],
	),
));
?>

<script>
$('#salesCategory').val('<?=$model->companySalesInfo->salesCategory?>');

$(document).ready(function(){
	
	$('.salesContractSelect').change(function(){
		
		var $form = $(this).parent();
		var $button = $form.find('button').first();
		$button.css('display', 'inline');
		$(this).css('display', 'none');
		$button.html('идет обновление тарифа..');
		$form.submit();
	});

	$('.feedZoon').live('click', function() {
		var self = this;
		var elId = self.getAttribute("id").split('_');
		var id = false;
		if(1 in elId) { id = elId[1]; }
		var success = false;
		$.ajax({
			url: '/newAdmin/ajax/feedZoon',
			async: false,
			data: {
				'id':id,
				'checked':self.checked,
			},
			dataType: 'json',
			type: 'POST',
			success: function(response) {
				if(response.status === "OK") {
					$(self).closest("td").css("background-color", "lightgreen");
					success = true;
				} else {
					this.error(null,JSON.stringify(response),null);
				}
			},
			error: function(XHR,textStatus,errorThrown) {
				$(self).closest("td").css("background-color", "lightpink");
			},
			complete: function() {
				setTimeout(function() { $(self).closest("td").css("background-color", ""); }, 500);
			}
		});
		return success;
	});
});
</script>