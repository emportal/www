  <style type="text/css">
   TABLE {
    border-collapse: collapse; /* Убираем двойные линии между ячейками */
    width: 300px; /* Ширина таблицы */
   }
   TH { 
    background: #fc0; /* Цвет фона ячейки */
    text-align: left; /* Выравнивание по левому краю */
   }
   TD {
    background: #fff; /* Цвет фона ячеек */
    text-align: center; /* Выравнивание по центру */
   }
   TH, TD {
    border: 1px solid black; /* Параметры рамки */
    padding: 4px; /* Поля вокруг текста */
   }
  </style>
<?php
		Yii::app()->getModule('adminClinic');
		$criteria = new CDbCriteria();
		$criteria->with = [ ];
		$criteria->compare("date_format(t.createdDate, '%Y-%m')",$reportMonth);
		
		if(isset($_REQUEST["LogAdminClinic"]["addressId"])) {
			$criteria->compare("t.addressId",$_REQUEST["LogAdminClinic"]["addressId"],true);
		}
		if(isset($_REQUEST["LogAdminClinic"]["companyName"])) {
			$criteria->compare("company.name",$_REQUEST["LogAdminClinic"]["companyName"],true);
		}
		
		$LogAdminClinic_dataProvider = new CActiveDataProvider(LogAdminClinic::model(), array(
			'criteria' => $criteria,
			'pagination' => false,			
		));
		$this->widget('bootstrap.widgets.TbGridView', array(
			'id' => "LogAdminClinic_GridView",
		    'type'=>'striped bordered condensed',
		    'dataProvider' => $LogAdminClinic_dataProvider,
		    'template'=>"{items}{pager}",
		    'columns' => [
				[	
					'header'=>'Время',
					'name'=>'createdDate',
					//'filter'=> CHtml::textField('LogAdminClinic[companyName]', Yii::app()->request->getParam('LogAdminClinic')['companyName']),
					'htmlOptions' => [ 'style'=>'text-align:left!important; width:100px!important;' ],
				],
				[	
					'header'=>'Тип события',
					'value'=>function($data) {
						return $data->logAdminClinicType->description;
					},
					'htmlOptions' => [ 'style'=>'text-align: left !important;' ],
				],
				[	
					'header'=>'Пользователь',
					'value'=>function($data) {
						return $data->user->name . ", " . $data->user->email;
					},
					'htmlOptions' => [ 'style'=>'text-align: left !important;' ],
				],
				[	
					'header'=>'Измененные атрибуты',
					'value'=>function($data) {
						$rows = [];
						$actions = json_decode($data->data);
						if(is_array($actions)) {
							foreach ($actions as $action) {
								$rows[] = "<tr>"."<td>".$action->attributeName."</td><td style='width: 50px;'>".$action->oldValue."</td><td style='width: 75px;'>".$action->newValue."</td>"."</tr>";
							}
						}
						return "<table>" . join($rows, "") . "</table>";
					},
					'type'=>'raw',
					'htmlOptions' => [ 'style'=>'text-align: left !important;' ],
				],
		    ],
		));
?>