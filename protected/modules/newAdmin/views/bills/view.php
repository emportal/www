<style>
select, input {
	margin-bottom: 1px !important;
}
.postalAddressAnnotation {
    text-align: left;
    color: gray;
    margin-top: 10px;
}
</style>
<div style="float: left; border: 0px solid; margin-top: -10px;">
	<h1 class="smi_link">Счета</h1>
	<br>
	<div class="data-table vmargin10" style="float: left;">
		<div>
			<div>Общая сумма</div>
			<div><span id="totalSum"></span> руб.</div>
		</div>
		<div>
			<div>Оплачено</div>
			<div><span id="payedSum"></span> руб.</div>
		</div>
		<div>
			<div>Задолженность</div>
			<div><span id="debtSum"></span> руб.</div>
		</div>
	</div>
	<div class="data-table vmargin10" style="float: left; margin-left: 40px;">
		<div>
			<div>Всего заявок</div>
			<div><span id="totalNum"></span></div>
		</div>
		<div>
			<div>Подтверждено</div>
			<div><span id="approved"></span></div>
		</div>
		<div>
			<div>Отклонено</div>
			<div><span id="declined"></span></div>
		</div>
	</div>
</div>
<div style="float: right; margin: 0px 0 5px 0; border: 0px solid;">
	<!-- <a href="#">Фильтры</a> -->
	<table style="margin-top: 15px;">
		<tr>
			<td>
				Регион
			</td>
			<td>
				<select id="input_region" onchange="submit_input_forms();">
					<option value="">Все</option>
					<?php foreach(Yii::app()->params['regions'] as $region): ?>
						<option value="<?=$region['subdomain']?>"><?=$region['name']?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 200px;">
				Показать клиники сотрудника
			</td>
			<td>
				<select id="input_manager_id" onchange="submit_input_forms();">				
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
						<option value="">Все</option>
					<?php endif; ?>
					
					<?php foreach($managerListData as $key=>$data): ?>
						<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
					<?php endforeach; ?>
					
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
						<option value="-1">Без менеджеров</option>
					<?php endif; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Название клиники
			</td>
			<td>
				<input id="input_company_name" type="text">
			</td>
		</tr>
		<tr>
			<td>
				Рассматриваемый месяц
			</td>
			<td>
				<select id="input_report_month" onchange="submit_input_forms();">
					<!-- <option value="">Все</option> -->
					<?php 
                    $printReportMonthOptions = function() {
                        $dateStart = new Datetime('2015-07-01'); //актуальные счета начинаются с июля 2015
	                    $dateNow = new Datetime(); 
	                    $yearDiff = $dateStart->diff($dateNow)->y;
	                    $monthDiff = $yearDiff*12 + $dateStart->diff($dateNow)->m + 1;
	                    for ($i = 0; $i <= $monthDiff; $i++) {
	                        $value = $dateStart->format('Y-m'); //date('Y') . '-' . sprintf('%02d', $i);
	                        $name = MyTools::getRussianMonthsNames()[$dateStart->format('n') - 1] . ' ' . $dateStart->format('Y');
	                        $dateStart->modify('+1 month');
	                        echo '<option value="' . $value . '"> ' . $name . ' </option>';
	                    }
                    };
                    $printReportMonthOptions();
                    ?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<br>Скачать список счетов: <a href="<?= $_SERVER["REQUEST_URI"]."&download=1" ?>">стандартный</a> | <a href="<?= $_SERVER["REQUEST_URI"]."&download1C=1" ?>">1С</a>
			</td>
		</tr>
	</table>
</div>
<br><br><br><br><br><br><br><br>

<!-- \\\\\\\\\\ Подключение плагина сортировки ////////// -->
<?php Yii::app()->clientScript->registerPackage('jquery.tablesorter'); ?>
<style>
<!--
th.headerSortUp { 
    background-image: url('data:image/gif;base64,R0lGODlhFQAEAIAAACMtMP///yH5BAEAAAEALAAAAAAVAAQAAAINjB+gC+jP2ptn0WskLQA7');
    background-color: #3399FF; 
}
th.headerSortDown { 
    background-image: url('data:image/gif;base64,R0lGODlhFQAEAIAAACMtMP///yH5BAEAAAEALAAAAAAVAAQAAAINjI8Bya2wnINUMopZAQA7'); 
    background-color: #3399FF; 
}
th.header { 
    cursor: pointer; 
    font-weight: bold; 
    background-repeat: no-repeat; 
    background-position: center left; 
    padding-left: 20px; 
    border-right: 1px solid #dad9c7; 
    margin-left: -1px; 
} 
-->
</style>
<script type="text/javascript">
$(document).ready(function() { 
		$(".table-bills").tablesorter( ); 
	} 
); 
</script>
<!-- //////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<?php
$dataProvider = $model->dataProviderForNewAdminPanel($dtOptions);
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small table-bills',
	'afterAjaxUpdate' => 'f_afterAjaxUpdate',
	//'filter' => $dataProvider->model,
	'rowCssClassExpression'=>'(
			intval($data->getBill("'.$reportMonth.'")->appointmentTotal) > 0
		AND intval($data->getBill("'.$reportMonth.'")->appointmentDeclinedTotal) > 2
		AND floatval(floatval($data->getBill("'.$reportMonth.'")->appointmentDeclinedTotal)/floatval($data->getBill("'.$reportMonth.'")->appointmentTotal)) > 0.6
		)
			? "reminder_highlight"
			: ""',
	'rowHtmlOptionsExpression' => '(intval($data->getBill("'.$reportMonth.'")->appointmentTotal) == 0 && intval($data->getBill("'.$reportMonth.'")->sum) == 0) ? array("style"=>"display:none") : []',
	'columns' => array(
		[
			'name' => 'company',
			'header' => 'Компания',
			'value' => function($data) use ($reportMonth) {
				$output = '<a href="../ClinicCard/view?id=' . $data->company->id . '">' . $data->company->name . '</a>';
                $output .= ', <br>счет №' .$data->getBill($reportMonth)->id;
				if(Yii::app()->params['nbsMode'] || Yii::app()->params['devMode']) $output .= '<br><button class="" onclick="$.fancybox({href:\'/newAdmin/payment/balance?clinicId='.urlencode($data->id).'\'})">Баланс</button>';
				return $output;
			},
			'type' => 'raw',
			'filter'=>false,
			'htmlOptions' => [
				'style' => 'max-width: 150px;'
			],
		],
		[
			'name' => 'shortName',
			'header' => 'Адрес',
			'value' => function($data) use ($reportMonth) {
				return "{$data->shortName}<div class=\"postalAddressAnnotation\">почтовый адрес:<br>{$data->postalAddress}</div> 
				<a href=\"getEnvelope?addressId={$data->id}\" target=\"_blank\">печатать конверт</a> | 
				<a href=\"getMatching?addressId={$data->id}&reportMonth={$reportMonth}\" target=\"_blank\">акт</a> | 
				<a href=\"getBill?addressId={$data->id}&reportMonth={$reportMonth}\" target=\"_blank\">счет</a>";
			},
            'type' => 'raw',
			'filter'=>false,
			'htmlOptions' => [
				'style' => 'max-width: 190px;'
			],
		],
		[
			'header' => 'Заявок всего',
			'value' => function($data) use ($reportMonth) {
				$output = $data->getBill($reportMonth)->appointmentTotal;
				return "<span class='totalNumWrapper'>" . $output . "</span>";
			},
			'type' => 'raw',
			'filter'=>false,
		],
		[
			'header' => 'Подтверждено',
			'value' => function($data) use ($reportMonth) {
				$bill = $data->getBill($reportMonth);
				$output = $bill->appointmentTotal - $bill->appointmentDeclinedTotal;
				return "<span class='approvedWrapper'>" . $output . "</span>";
			},
			'type' => 'raw',
			'filter'=>false,
		],
		[
			'header' => 'Отклонено',
			'value' => function($data) use ($reportMonth) {
				Yii::app()->getModule('adminClinic');
				$criteria = new CDbCriteria();
				$criteria->compare("t.addressId",$data->id);
				$criteria->compare("DATE_FORMAT(t.createdDate,'%Y-%m')",$reportMonth);
				$bill = $data->getBill($reportMonth);
				$output = $bill->appointmentDeclinedTotal;
				$haveWorked = "";
				if($bill->appointmentDeclinedByClinic > 0 || LogAdminClinic::model()->count($criteria) > 0) {
					$haveWorked = "работал с ЛКК";
                    $haveWorked = "<br><a href=\"javascript:void(0)\" onclick=\"showLogAdminClinicForm('$data->id','$reportMonth')\">" . $haveWorked . "</a>";
				}
				$output .= $haveWorked;
				$output .= "<span style='display:none;'>(<span class='declinedByClinicWrapper'>" . $bill->appointmentDeclinedByClinic . "</span>)</span>";
				return "<span class='declinedWrapper'>" . $output . "</span>";
			},
			'type' => 'raw',
			'filter'=>false,
		],
		[
			'header' => 'Сумма, руб.',
			'value' => function($data) use ($reportMonth) {
				$output = '<span class="empComissionWrapper">' . $data->getBill($reportMonth)->sum . '</span>';
				$salesContractType = $data->salesContract->salesContractType;
				if ($salesContractType->price > 0 AND $salesContractType->isFixed == 1)
					$output .= '<div>абонентская <br>плата</div>';
				return $output;
			},
			'type' => 'raw',
			'filter'=>false,
		],
		[
			'header' => 'Статус оплаты',
			'value' => function($data) use ($reportMonth) {
				//$output = 'Поле для выставления статуса';
				$bill = $data->getBill($reportMonth);
				$output = '<span class="button_wrapper" data-paymentStatusId="' . $bill->paymentStatusId . '" data-id="' . $bill->id . '">';
				$output .= '<a href="#" data-statusId="1" class="button fullwidth bg-darkgreen c-white nl non-active btn-manager-ok">';
				$output .= Bill::$MANAGER_STATUS_ID_TYPES[1];
				$output .= '</a>';
				$output .= '<a href="#" data-statusId="0" class="button fullwidth bg-darkblue c-white nl non-active btn-manager-not-ok">';
				$output .= Bill::$MANAGER_STATUS_ID_TYPES[0];
				$output .= '</a>';
				$output .= '</span>';
				
				if ($bill->sumPayedOnline > 0) {
					$output .= '<a href="#" class="button fullwidth bg-yellow nl non-active button-payments" onclick="return false;">';
					$output .= 'Оплачено онлайн:<br> ' . $bill->sumPayedOnline . ' руб.';
					$output .= '</a>';
				}
				if(Yii::app()->params['nbsMode'] || Yii::app()->params['devMode']) $output .= '<br><a target="_blank" href="/newAdmin/payment/create?Payment[billId]='.urlencode($bill->id).'&Payment[sum]='.urlencode($bill->sum).'&Payment[addressId]='.urlencode($bill->addressId).'&Payment[purpose]='.urlencode('Оплата по счету №'.$bill->id).'">
					<button>Добавить платёж</button></a>';
				
				return $output;
			},
			'type' => 'raw',
			'filter'=>false,
			'htmlOptions' => [
				//'style' => 'min-width: 110px;'
			]
		],
		[
			'header' => 'Комментарий',
			'value' => function($data) use ($reportMonth) {
				$bill = $data->getBill($reportMonth);
				//$output = 'Поле для коммента';
				$output .= '<textarea data-noSubmit="1" class="manager-comment h60">';
				$output .= $bill->managerComment;
				$output .= '</textarea>';
				$output .= '<a href="#" data-id="' . $bill->id . '" class="button fullwidth c-black nl non-active btn-manager-comment-save" style="background-color: #eee;">';
				$output .= 'Сохранить';
				$output .= '</a>';
				return $output;
			},
			'type' => 'raw',
			'filter'=>false,
			'htmlOptions' => [
				'style' => 'width: 110px;'
			]
		],
	),
));
?>

<script>
function showLogAdminClinicForm (addressId,reportMonth) {
	$.fancybox({
		id: "LogAdminClinic_fancyView",
		width: 1000,
		height: 1000,
		content: '/newAdmin/bills/LogAdminClinic?report_month='+reportMonth+'&LogAdminClinic[addressId]='+addressId,
		type: 'iframe',
	});
};
</script>

<script> var submit_path = '/newAdmin/bills/view?'; </script>
<script>
//$(document).ready(function() {
	var data_values = {
		'region': '<?=Yii::app()->request->getParam('region')?>',
		'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
		'company_name': '<?=Yii::app()->request->getParam('company_name')?>',
		'report_month': '<?=Yii::app()->request->getParam('report_month')?>',
	};
	
function update_input_forms() {
	
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = '';
		if (typeof $('#input_'+key) != 'undefined') {
			
			if ($('#input_'+key).prop('type') == 'select-one') {
			
				$('#input_'+key+' option').each(function(){
					
					if (this.value == data_values[key]) {
						
						$('#input_'+key).val(data_values[key]);
					}
				});
			} else {
				$('#input_'+key).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		
		if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
			
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != '') {query += '&' + key + '=' + encodeURIComponent(data_values[key]);}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

////////

function highlight_unprocessed_appointments() {
	//$('.unprocessed_marker').parent().parent().parent().addClass("reminder_highlight");
}

function listen_button_clicks() {
	var button_status_mapper = {
		0: 'btn-manager-not-ok',
		1: 'btn-manager-ok',
	};
	var $buttons = $('.btn-manager, .btn-manager-ok, .btn-manager-not-ok');
	var $button_wrappers = $('.button_wrapper');
	
	$buttons.click(function() {
		var $button = $(this);
		var $button_wrapper = $button.parent();
		
		if ( $button.hasClass('btn-manager') || $button.hasClass('btn-manager-should-check-if-visited') ) {
			
			$button_wrapper.children().css('display', 'block');
			$button.hide();
			$button_wrapper.addClass('is_opened');
		} else {
			
			var existingPaymentStatusId = $button_wrapper.attr('data-paymentStatusId'); //rf
			var ajaxParams = {				
				'billId': $button_wrapper.attr('data-id'),
				'attributeName': 'paymentStatusId',
				'attributeNewValue': $button.attr('data-statusId'),
				'scenario': 'changeManagerStatus',
			};
			
			if (!$button_wrapper.hasClass('is_opened')) {
				//console.log('показываем кнопки на выбор');
				$button_wrapper.children().css('display', 'block');
				$button_wrapper.find('.btn-manager, .btn-manager-should-check-if-visited').hide();
				$button_wrapper.addClass('is_opened');
			} else {
				//console.log('прячем все кнопки');
				$button_wrapper.children().hide();
				$button_wrapper.removeClass('is_opened');
				
				$.ajax({
					url: '/newAdmin/ajax/changeBillAttribute',
					async: true,
					data: ajaxParams,
					dataType: 'json',
					type: 'GET',
					success: function(response) {
						if (response.success == 'false') {
							alert(response.msg);
							document.location.reload();
							return false;
						} else {
                            //console.log('response: ', response);
                            if (response.leftToPay <= 0 
                                && response.addressAttributes.isActive == 0
                                && window.confirm('Долг клиники полностью погашен. Сделать ее активной?')
                            ) {
                                var ajaxParams = {
                                    'id': response.addressAttributes.id,
                                    'attributeName': 'isActive',
                                    'attributeNewValue': 1,
                                    'scenario': 'changeActivityStatus',
                                };
                                changeAddressAtrribute(ajaxParams, function() {alert('Клиника теперь активна');});
                            }
                            if (response.leftToPay > 0 
                                && response.addressAttributes.isActive == 1
                                && window.confirm('Клиника активна, но у нее теперь есть долг в размере ' + response.leftToPay + ' руб. Отключить ее?')
                            ) {
                                var ajaxParams = {
                                    'id': response.addressAttributes.id,
                                    'attributeName': 'isActive',
                                    'attributeNewValue': 0,
                                    'scenario': 'changeActivityStatus',
                                };
                                changeAddressAtrribute(ajaxParams, function() {alert('Клиника теперь неактивна');});
                            }
                        }
					},
				});
			}
			$button_wrapper.attr('data-paymentStatusId', ajaxParams.newPaymentStatusId); //rf
			$button.css('display', 'block');
		}
		return false;
	});
	
	$.each($button_wrappers, function() {
		var $button_wrapper = $(this);
		var $button_wrapper_buttons = $button_wrapper.find($buttons);
		var paymentStatusId = $button_wrapper.attr('data-paymentStatusId');
		var button_class_to_show = '.' + button_status_mapper[paymentStatusId];		
		$button_wrapper_buttons.hide();
		$button_wrapper.find( button_class_to_show ).css('display', 'block');
	});
	
	//manager comments
	var save_manager_comment = function(billId, newValue, callback) {
		$.ajax({
			url: '/newAdmin/ajax/changeBillAttribute',
			async: true,
			data: {
				'billId': billId,
				'attributeName': 'managerComment',
				'attributeNewValue': newValue,
				//'scenario': 'changeManagerComment',
			},
			dataType: 'json',
			type: 'GET',
			success: function(msg) {
				callback(msg);
			},
		});
	}
	
	var $save_buttons = $('.btn-manager-comment-save');
	$save_buttons.click(function() {
		var $button = $(this);
		var billId = $button.attr('data-id');
		var newValue = $button.prev().val();
		$button.html('...🕑...');
		save_manager_comment(billId, newValue, function(ajaxMessage) {
			if (ajaxMessage.success == 'false') {
				alert(ajaxMessage.msg);
				$button.html('Ошибка');
				$button.removeClass('bg-darkgreen bg-red c-white c-black');
				$button.addClass('bg-red c-white');
				window.setTimeout(function() {
					$button.removeClass('bg-darkgreen bg-red c-white c-black');
					$button.addClass('c-black');
					$button.html('Сохранить');
				}, 500);
				return false;
			}
			$button.html('ОК');
			$button.addClass('bg-darkgreen c-white');
			$button.removeClass('c-black');
			window.setTimeout(function() {
				$button.addClass('c-black');
				$button.removeClass('bg-darkgreen c-white');
				$button.html('Сохранить');
			}, 500);
		});
		return false;
	});
    
    function changeAddressAtrribute(ajaxParams, callback) {
        $.ajax({
            url: '/newAdmin/ajax/changeAddressAttribute',
            async: true,
            data: ajaxParams,
            dataType: 'json',
            type: 'GET',
            success: function(response) {
                if (response.success == 'false') {
                    alert(response.msg);
                    document.location.reload();
                    return false;
                } else {
                    if (typeof callback == 'function')
                        callback();
                }
            },
        });
    }
}

function f_afterAjaxUpdate() {
	//highlight_unprocessed_appointments();
	listen_button_clicks();
	count_total_sum();
}

$(document).keypress(function(e) {
	
    if (e.which == 13) 
		if ( !$(':focus').attr("data-noSubmit") )
			submit_input_forms();
});

function count_total_sum() {
	var totalSum = 0;
	var payedSum = 0;
	var totalNum = 0;
	var approved = 0;
	var declined = 0
	var declinedByClinic = 0;
	var $rows = $('.table-bills tr');
	
	$.each($rows, function() {
		var $row = $(this);
		var $empComissionWrapper = $row.find('.empComissionWrapper');
		var $empComissionPayedWrapper = $row.find('.button_wrapper').first();
		
		var comissionValue = parseInt($empComissionWrapper.html());
		
		if (!isNaN(comissionValue)) {
			totalSum += comissionValue;
			//console.log($empComissionPayedWrapper.attr('data-paymentStatusId'));
			if ($empComissionPayedWrapper.attr('data-paymentStatusId') == 1) {
				//console.log('atr = ' . $empComissionPayedWrapper.attr('data-paymentstatusid'));
				payedSum += comissionValue;
			}
		}
		var totalNumWrapper = parseInt($row.find('.totalNumWrapper').first().text().trim())
		if(!isNaN(totalNumWrapper)) { totalNum += totalNumWrapper; }
		var approvedWrapper = parseInt($row.find('.approvedWrapper').first().text().trim())
		if(!isNaN(approvedWrapper)) { approved += approvedWrapper; }
		var declinedWrapper = parseInt($row.find('.declinedWrapper').first().text().trim())
		if(!isNaN(declinedWrapper)) { declined += declinedWrapper; }
		var declinedByClinicWrapper = parseInt($row.find('.declinedByClinicWrapper').first().text().trim())
		if(!isNaN(declinedByClinicWrapper)) { declinedByClinic += declinedByClinicWrapper; }
	});
	
	$('#totalSum').html(totalSum);
	$('#payedSum').html(payedSum);
	$('#debtSum').html(totalSum - payedSum);
	$('#totalNum').html(totalNum);
	$('#approved').html(approved);
	$('#declined').html(declined + " (" + declinedByClinic + ")");
}
$(document).ready(function() {
	update_input_forms();
	f_afterAjaxUpdate();
});
//});
</script>