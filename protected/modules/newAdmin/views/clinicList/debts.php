<div>
    Показывать: 
    <a href="debts?showInactive=true">все клиники</a> | 
    <a href="debts">только активные (кандидаты на отключение)</a>
</div>
<?php 
if (!empty($render_data)) {
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => new CArrayDataProvider($render_data, [
        'pagination'=>array(
            'pageSize'=>1000,
        ),
    ]),
    'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
    'columns' => array(
        [
            'name' => 'companyName',
            'header' => 'Компания',
        ],
        [
            'name' => 'name',
            'header' => 'Адрес',
        ],
        [
            'name' => 'url',
            'header' => 'url',
        ],
        [
            'name' => 'debtSum',
            'header' => 'Долг (руб.)',
        ],
        [
            'name' => 'debtMonths',
            'header' => 'за периоды',
        ],
        [
            'name' => 'mailList',
            'header' => 'E-mail',
        ]
    ),
));
} else {
    echo '<br>Клиники не найдены';
}
?>