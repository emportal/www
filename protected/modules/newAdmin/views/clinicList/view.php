<?php
/* @var $this ClinicListController */
/* @var $model Address */
?>
<h1 class="smi_link">Клиники</h1>
<div style="float: right; margin: 0px 0 5px 0;">
	<table>
		<tr>
			<td style="width: 200px;">
				Тип клиники 
			</td>
			<td>
				<select id="input_company_type" onchange="submit_input_forms();">				
					<option value="">Активные на сайте</option>
					<option value="all">Все</option>
					<option value="inactive">Неактивные + стадо</option>
					<option value="inactive_only">Только неактивные</option>
					<option value="herd_only">Только стадо</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 200px;">
				Показать клиники сотрудника 
			</td>
			<td>
				<select id="input_manager_id" onchange="submit_input_forms();">				
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
						<option value="">Все</option>
					<?php endif; ?>
					
					<?php foreach($managerListData as $key=>$data): ?>
						<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
					<?php endforeach; ?>
					
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
						<option value="-2">Только с менеджерами</option>
						<option value="-1">Без менеджеров</option>
					<?php endif; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Название клиники
			</td>
			<td>
				<input id="input_company_name" type="text" class="w220">
			</td>
		</tr>
        <tr>
			<td colspan="2">
				<a href="debts?showInactive=true" class="float_right">Список клиник с долгами</a>
			</td>
		</tr>
	</table>
</div>
<div style="clear: both;">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
	'columns' => array(
		[
			'name' => 'name',
			'header' => 'Название клиники',
			'value' => function($data)
			{
				return '<a target="_blank" href="../ClinicCard/view?id='.$data->id.'">'.$data->name.'</a>';
			},
			'filter' => false,
			'type' => 'raw'
		],
		[
			'name' => 'userMedicals.createDate',
			'header' => 'Дата подключения',
			'value' => function($data)
			{
				$userMedicals = $data->userMedicals;
				$output = [];
				foreach($userMedicals as $userMedical)
				{
					if ($userMedical->agreementNew && strlen($userMedical->createDate) == 19)
						return $userMedical->createDate;
				}
				return 'не принимали';
			},
			'type' => 'raw',
		],
		[
			'name' => 'name',
			'value' => 'count($data->doctors)', 
			'header' => 'Врачей',
		],
		[
			'name' => 'name',
			'header' => 'Записей (+прошлый мес.)',
			'value' => function($data)
			{
				$result = '';
				$linkHrefAll = '../registryList/view?cid=' . $data->id;
				$linkHrefLastMonth = '../registryList/view?cid=' . $data->id . '&time_period=' . MyTools::getDateInterval('lastMonth') . '&man_status=r';
				
				$result .= '<a href="' . $linkHrefAll . '&createReport=true">';
				$result .= $data->getAppointmentsCount();
				$result .= '</a>';
				$result .= ' ( <a href="' . $linkHrefLastMonth . '">';
				$result .= '+' . $data->hasAppointments(MyTools::getDateInterval('lastMonth'));
				$result .= '</a>)';
				
				return $result;
			},
			'filter' => false,
			'type' => 'raw'
		],
		[
			'name' => 'name',
			'header' => 'Менеджер',
			'value' => '$data->managerRelations->user->id ? $data->managerRelations->user->name." ".$data->managerRelations->user->email : "-"',
			'filter' => false,
		],
		[
			'name' => 'none',
			'header' => 'Действие',
			'value' => function($data) {
				
				$link = '../registryList/add?id=' . $data->linkUrl;
				$output = '';
				$output .= '<a href="' . $link . '" title="Добавить новую запись" class="button fullwidth bg-darkgreen c-white nl">';
				$output .= 'Новая запись';
				$output .= '</a>';
				
				if (!Yii::app()->user->model->hasRight('ACCESS_CALL_CENTER_FUNCTIONALITY'))
					$output = '-';
				
				return $output;
			},
			'type' => 'raw',
		],

	),
));
?>
</div>
<script> var submit_path = '/newAdmin/clinicList/view?'; </script>
<script>
//$(document).ready(function() {
	var data_values = {
		
		'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
		'company_name': '<?=Yii::app()->request->getParam('company_name')?>',
		'apt_status': '<?=Yii::app()->request->getParam('apt_status')?>',
		'company_type': '<?=Yii::app()->request->getParam('company_type')?>',
	};
	
function update_input_forms() {
	
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = '';
		if (typeof $('#input_'+key) != 'undefined') {
			
			if ($('#input_'+key).prop('type') == 'select-one') {
			
				$('#input_'+key+' option').each(function(){
					
					if (this.value == data_values[key]) {
						
						$('#input_'+key).val(data_values[key]);
					}
				});
			} else {
				$('#input_'+key).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for (var key in data_values) {
		if ($('#input_'+key).length) {
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != '') {query += '&' + key + '=' + encodeURIComponent(data_values[key]);}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

update_input_forms();

$(document).keypress(function(e) {
    if(e.which == 13) {submit_input_forms();}
});
//});
</script>