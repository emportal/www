<style>
.table.table-striped tr td:not(:first-child) {
	text-align: left !important;
}
.items {
	padding-bottom: 0;
}
</style>
<h1>Добавление новой клиники (контрагента)</h1>
<?php
	$this->renderPartial('application.modules.admin.views.company._companyForm', [
		'model' => $model,
		'dontShowTitle' => true
	]);
?>