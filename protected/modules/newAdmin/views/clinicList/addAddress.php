<style>
.table.table-striped tr td:not(:first-child) {
	text-align: left !important;
}
.items {
	padding-bottom: 0;
}
</style>
<h1>Добавить адрес (клиника "<?= $company->name ?>")</h1>
<?php
	$this->renderPartial('application.modules.admin.views.company._addressForm', [
		'model' => $model,
		'dontShowTitle' => true
	]);
?>