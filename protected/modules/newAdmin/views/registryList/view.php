<?php
/* @var $this ClinicListController */
/* @var $model Address */
$reportParamsStr = str_replace(['?', '&createReport=true', 'createReport=true'], '', $_SERVER['QUERY_STRING']) . '&createReport=true';
Yii::app()->clientScript->registerCssFile('/shared/css/datepicker.min.css');
Yii::app()->clientScript->registerScriptFile('/shared/js/datepicker.min.js', CClientScript::POS_END);
$timeStart = strtotime(explode('-',$model->time_period)[0])*1000;
$timeEnd = strtotime(explode('-',$model->time_period)[1])*1000;
$timeEnd = $timeEnd ? $timeEnd : $timeStart;
Yii::app()->clientScript->registerScript('air-date','
	$("#input_time_period").datepicker({
		maxDate: new Date(),
		onHide: function(inst, animationCompleted) {
			if(animationCompleted && $("#input_time_period").val() != "" && $("#input_time_period").val() != $("#input_time_period_h").val()) {
				submit_input_forms();
			}
		}
	});
	//$("#input_time_period").val($("#input_time_period_h").val());
	var myDatepicker = $("#input_time_period").datepicker().data("datepicker");
	myDatepicker.selectDate([new Date('.$timeStart.'),new Date('.$timeEnd.')]);
	');
?>
<style>
select, input {
	margin-bottom: 1px !important;
}
</style>
<div style="width: 100%; display: inline-block;">
	<div style="float: left; border: 0px solid;">
		<h1 class="smi_link">Регистратура</h1>
		<br><a href="?<?=$reportParamsStr?>">скачать все отфильтрованные записи отчетом</a>
	</div>
	<div style="float: right; margin: 0px 0 5px 0; border: 0px solid;">
		<!-- <a href="#">Фильтры</a> -->
		<table style="margin-top: -10px;">
			<tr>
				<td>
					Регион
				</td>
				<td>
					<select id="input_region" onchange="submit_input_forms();">
						<option value="">Все</option>
						<?php foreach(Yii::app()->params['regions'] as $region): ?>
							<option value="<?=$region['subdomain']?>"><?=$region['name']?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td style="width: 200px;">
					Показать клиники сотрудника
				</td>
				<td>
					<select id="input_manager_id" onchange="submit_input_forms();">				
						<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
							<option value="">Все</option>
						<?php endif; ?>
						
						<?php foreach($managerListData as $key=>$data): ?>
							<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
						<?php endforeach; ?>
						
						<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
							<option value="-1">Без менеджеров</option>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Название клиники
				</td>
				<td>
					<input id="input_company_name" type="text">
				</td>
			</tr>
			<tr>
				<td>
					Системный статус
				</td>
				<td>
					<select id="input_apt_status" onchange="submit_input_forms();">
						<option value="">Все</option>
						<?php foreach($model::$STATUSES as $key=>$data): ?>
							<option value="<?=$key?>"><?=$data?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Статус
				</td>
				<td>
					<select id="input_man_status" onchange="submit_input_forms();">
						<option value="">Все</option>
						<?php foreach($model::$MANAGER_STATUS_ID_TYPES as $key=>$data): ?>
							<?php if ($key == 5) continue; ?>
							<option value="<?=$key?>"><?=$data?></option>
						<?php endforeach; ?>
						<option value="r">Те, за которые хотим взять деньги</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Время появления
				</td>
				<td>
				<!-- 	<select id="input_time_period" onchange="submit_input_forms();">
						<option value="">Все</option>
						<option value="<?= MyTools::getDateInterval('lastMonth') ?>">за прошлый месяц</option>
						<?php if (date('j') != 1): ?>
						<option value="<?= MyTools::getDateInterval('currentMonth') ?>">за этот месяц</option>
						<?php endif; ?>
						<option value="<?= MyTools::getDateInterval('pastWeek') ?>">за прошлую неделю</option>
						<option value="<?= MyTools::getDateInterval('currentWeek') ?>">за эту неделю</option>
						<option value="<?= MyTools::getDateInterval('yesterday') ?>">за вчера</option>
						<option value="<?= MyTools::getDateInterval('today') ?>">за сегодня</option>
					</select> -->

					<input onchange="submit_input_forms();" id="input_time_period" type="text" data-toggle-selected="false" data-range="true" data-multiple-dates-separator="-" class="datepicker-here" value="<?=$model->time_period?>">
					<input id="input_time_period_h" type="hidden" value="<?=$model->time_period?>">
				</td>
			</tr>
			<tr>
				<td>
					Источник заявки
				</td>
				<td>
					<select id="input_app_type" onchange="submit_input_forms();">
						<option value="">Все</option>
						<?php foreach($model::getStatusesAppTypeForDate($model->time_period) as $key => $data): ?>
							<?php if (in_array($key, $model::$HIDDEN_APP_TYPES)) continue; ?>
							<option value="<?=$key?>"><?=$data?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Тип записи
				</td>
				<td>
					<select id="input_visit_type" onchange="submit_input_forms();">
						<option value="">Все</option>
						<option value="doctor">Запись на прием</option>
						<option value="service">Запись на услугу</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Наличие предоплаты
				</td>
				<td>
					<select id="input_has_prePaid" onchange="submit_input_forms();">
						<option value="">Все</option>
						<option value="1">Предоплаченные</option>
						<option value="0">Без предоплаты</option>
					</select>
				</td>
			</tr>
		</table>
	    <input type="hidden" id="input_patient_id">
	</div>
</div>
<?php if(Yii::app()->user->model->hasRight('CAN_SEE_EMP_COMISION_NEW_ADMIN')): ?>
	<div style="text-align: right;">Сумма ЕМП комиссии: <strong><span id="summa"></span> руб.</strong></div>
<?php endif; ?>
<?php
$columns = array(
		[
			'name' => 'createdDate',
			'header' => 'Дата записи',
			'value' => function($data) {
					
					$result = '';
					$plannedTimeUnix = strtotime($data->createdDate);
					$result .= date('H:i d.m.Y', $plannedTimeUnix);
					//$result = $data->createdDate;
					return $result;
				},
			'htmlOptions' => [
				'style' => 'width: 90px; text-align:center'
			]
		],
		[
			'name' => 'plannedTime',
			'header' => 'Время визита',
			'value' => function($data) {
					
					$result = '';
					$plannedTimeUnix = strtotime($data->plannedTime);
					$result .= date('H:i d.m.Y', $plannedTimeUnix);
					
					if ($data->visitType == 'service'){
						$result .= '<br><b>';
						$result .= 'Услуга "' . $data->addressService->service->name . '" ';
						$result .= 'за ' . $data->addressService->price . ' руб.';
						$result .= '</b>';
					}
					if(floatval($data->prePaid) > 0) {
						$result .=
							'<br><span style="color:red;">'.
								'Предоплачено: '.$data->prePaid.
							'</span>';
					}
					
					return $result;
				},
			'htmlOptions' => [
				'style' => 'width: 90px; text-align:center'
			],
			'type' => 'raw'
		],
		[
			'name' => 'appType',
			'header' => 'Источник заявки',
			'value' => function($data) {
				
				if ($data->createdDate < '2014-11-28 00:00:00') return '';
				
				$optionsHTML = '';
				if ($data->hasChangeableApptype()) {
					$statusesAppType = [];
					foreach(AppointmentToDoctors::$CHANGEABLE_APP_TYPES as $key => $value) {
						$statusesAppType[$value] = AppointmentToDoctors::getStatusesAppType()[$value];
					}
				} else {
					$statusesAppType = AppointmentToDoctors::getStatusesAppType();
				}
				
				foreach($statusesAppType as $key => $value) {
					$optionsHTML .= '<option value="' . $key . '">' . $value . '</option>';
				}
				
				$output = '<span class="select_button_wrapper" data-appType="' . $data->appType . '" data-link = "' . $data->link . '" data-disabled = "' . !$data->hasChangeableApptype() . '">';
				$output .= '<select class="btn-select-phoneSource" style="width: 172px;">';
				$output .= $optionsHTML;
				$output .= '</select>';
				$output .= '</span>';
                if ($data->sourcePageUrl)
                    $output .= '<br>Реферер: ' . $data->sourcePageUrl;
				return $output;
			},
			'type' => 'raw'
		],
		/*[
			'name' => 'none',
			'header' => 'Ответственный',
			'value' => '$data->company->managerRelations->user->name',
		],*/
		[
			'name' => 'none',
			'header' => 'Клиника, адрес, врач, email, регистратура',
			'value' => function($data) {
					
					$address = $data->address;
					
					$output = '<a href="../ClinicCard/view?id=' . $data->company->id . '">' . CHtml::encode($data->company->name) . '</a>';
					$output .= ', <a href="' . $address->linkHref . '" target="_blank">';
					$output .= CHtml::encode($address->shortName);
					$output .= '</a>';
					if ($data->visitType != 'service' AND method_exists($data->doctor, 'getPagesUrls')) {
						$doctorUrl = $data->doctor->getPagesUrls($address->id, true)[0];
						$output .= ', ' . CHtml::link(CHtml::encode($data->doctor->shortName), [$doctorUrl], ['target'=>'_blank']);
					}
					$output .= '<div>' . $address->userMedicals->email . '</div>';
					if (!empty($phones = $address->phones)) {
						foreach($phones as $phone) {
							$output .= '<div style="font-weight: bold;">' . CHtml::encode($phone->name) . '</div>';
						}
					}
					return $output;
				},
			'filter' => false,
			'htmlOptions' => [
				'style' => 'width: 150px;'
			],
			'type' => 'raw'
		],
		[
			'name' => 'none',
			'header' => 'Посетитель',
			'value' => '$data->name.\' \'.$data->phone.\' \'.$data->email.\'\'',
		],
		[
			'name' => 'statusId',
			'header' => 'Системный статус',
			'value' => function($data) {
					return $data->statusText . "<br>" . 
						(!empty($data->adminClinicComment) ? CHtml::Link('<i class="icon-comment"></i>', null, array(
						    'class' => 'ipopover',
						    'data-trigger' => 'hover',
						    //'data-title' => 'Your title',
						    'data-content' => $data->adminClinicComment,
						)) : '');
				},
			'type' => 'raw'
		],
		[
			'name' => 'managerStatusId',
			'header' => 'Статус',
			'value' => function($data) {
				
				$plannedTimeHasPassed = strtotime($data->plannedTime) < time();
				$shouldCheckIfVisited = (($data->managerStatusId < 2) AND $plannedTimeHasPassed);
				
				if ($shouldCheckIfVisited)
					$data->managerStatusId = 5;
				
				if ($data->appType == AppointmentToDoctors::APP_TYPE_SAMOZAPIS) {
					echo '<a href="#" onclick="return false" class="button fullwidth bg-darkgreen c-white nl">';
					echo 'По самозаписи не звоним';
					echo '</a>';
					return false;
				}
				
				if ($data->company->id == 'bfcb3955-065b-11e2-9aa0-e840f2aca94f') {
					echo '<a href="#" onclick="return false" class="button fullwidth bg-darkgreen c-white nl">';
					echo 'В клиники "Меди" не звоним';
					echo '</a>';
					return false;
				}
				$output = '<span class="button_wrapper" data-managerStatusId="' . $data->managerStatusId . '" data-link="' . $data->link . '">';
				
				if ($plannedTimeHasPassed AND $data->managerStatusId != 2) {
					//показываем назначение статусов для записей, где прием должен был уже завершиться
					$output .= '<a href="#" data-statusId="3" class="button fullwidth bg-darkgreen c-white nl non-active btn-manager-visited">';
					$output .= AppointmentToDoctors::$MANAGER_STATUS_ID_TYPES[3];
					$output .= '</a>';
					$output .= '<a href="#" data-statusId="4" class="button fullwidth bg-darkblue c-white nl non-active btn-manager-not-visited">';
					$output .= AppointmentToDoctors::$MANAGER_STATUS_ID_TYPES[4];
					$output .= '</a>';
					$output .= '<a href="#" data-statusId="5" class="button fullwidth bg-red c-white nl btn-manager-should-check-if-visited">';
					$output .= AppointmentToDoctors::$MANAGER_STATUS_ID_TYPES[5];
					$output .= '</a>';
				} else {
					$output .= '<a href="#" data-statusId="0" class="button fullwidth bg-red c-white nl btn-manager">';
					$output .= AppointmentToDoctors::$MANAGER_STATUS_ID_TYPES[0];
					$output .= '</a>';
					$output .= '<a href="#" data-statusId="1" class="button fullwidth bg-darkgreen c-white nl non-active btn-manager-ok">';
					$output .= AppointmentToDoctors::$MANAGER_STATUS_ID_TYPES[1];
					$output .= '</a>';
					$output .= '<a href="#" data-statusId="2" class="button fullwidth bg-darkblue c-white nl non-active btn-manager-not-ok">';
					$output .= AppointmentToDoctors::$MANAGER_STATUS_ID_TYPES[2];
					$output .= '</a>';
				}
				
				
				if ($data->managerStatusId == 0)
					$output .= '<span class="unprocessed_marker"></span>';
				
				$output .= '</span>';
				return $output;
			},
			'type' => 'raw',
			'htmlOptions' => [
				'style' => 'min-width: 110px;'
			]
		],
		'comissionEMP' => [
			'name' => 'none',
			'header' => 'Комиссия ЕМП',
			'value' => function($data) {
				$comission = $data->comission;
				if(in_array($data->appType, [AppointmentToDoctors::APP_TYPE_SAMOZAPIS, AppointmentToDoctors::APP_TYPE_FROM_APP_FORM, AppointmentToDoctors::APP_TYPE_OWN,AppointmentToDoctors::APP_TYPE_VK_APP])) {
					$comission = 0;
				}
				return '<span class="comission">'.$comission.'</span> руб.';
			},
			'type' => 'raw',
		],
		[
			'name' => 'none',
			'header' => 'Комментарий',
			'value' => function($data) {
				$output = '';
				$output .= '<textarea data-noSubmit="1" class="manager-comment">';
				$output .= CHtml::encode($data->managerComment);
				$output .= '</textarea>';
				$output .= '<a href="#" data-link="' . $data->link . '" class="button fullwidth c-black nl non-active btn-manager-comment-save" style="background-color: #eee;">';
				$output .= 'Сохранить';
				$output .= '</a>';
				return $output;
			},
			'type' => 'raw',
		],
	);

if(!Yii::app()->user->model->hasRight('CAN_SEE_EMP_COMISION_NEW_ADMIN')) {
	unset($columns['comissionEMP']);
}

$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $model->dataProviderForNewAdminPanel($dtOptions),
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
	'afterAjaxUpdate' => 'f_afterAjaxUpdate',
	'columns' => $columns,
));
?>
<script> var submit_path = '/newAdmin/registryList/view?'; </script>
<script>
//$(document).ready(function() {
	var data_values = {
		'region': '<?=Yii::app()->request->getParam('region')?>',
		'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
		'company_name': '<?=Yii::app()->request->getParam('company_name')?>',
		'apt_status': '<?=Yii::app()->request->getParam('apt_status')?>',
		'man_status': '<?=Yii::app()->request->getParam('man_status')?>',
		'time_period': '<?=Yii::app()->request->getParam('time_period')?>',
		'app_type': '<?=Yii::app()->request->getParam('app_type')?>',
		'visit_type': '<?=Yii::app()->request->getParam('visit_type')?>',
        'patient_id': '<?=Yii::app()->request->getParam('patient_id')?>',
        'has_prePaid':  '<?=Yii::app()->request->getParam('has_prePaid')?>',
	};
	
function update_input_forms() {
	
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = '';
		if (typeof $('#input_'+key) != 'undefined') {
			
			if ($('#input_'+key).prop('type') == 'select-one') {
			
				$('#input_'+key+' option').each(function(){
					
					if (this.value == data_values[key]) {
						
						$('#input_'+key).val(data_values[key]);
					}
				});
			} else {
				$('#input_'+key).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		
		if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
			
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != '') {query += '&' + key + '=' + encodeURIComponent(data_values[key]);}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

////////

function highlight_unprocessed_appointments() {
	
	$('.unprocessed_marker').parent().parent().parent().addClass("reminder_highlight");
}

function listen_button_clicks() {
	var button_status_mapper = {
		0: 'btn-manager',
		1: 'btn-manager-ok',
		2: 'btn-manager-not-ok',
		3: 'btn-manager-visited',
		4: 'btn-manager-not-visited',
		5: 'btn-manager-should-check-if-visited'
	};
	var $buttons = $('.btn-manager, .btn-manager-ok, .btn-manager-not-ok, .btn-manager-visited, .btn-manager-not-visited, .btn-manager-should-check-if-visited');
	var $button_wrappers = $('.button_wrapper');
	
	$buttons.click(function() {
		var $button = $(this);
		var $button_wrapper = $button.parent();
		
		if ( $button.hasClass('btn-manager') || $button.hasClass('btn-manager-should-check-if-visited') ) {
			
			$button_wrapper.children().css('display', 'inline-block');
			$button.hide();
			$button_wrapper.addClass('is_opened');
		} else {
			
			var existingManagerStatusId = $button_wrapper.attr('data-managerStatusId');
			var ajaxParams = {				
				'appointmentLink': $button_wrapper.attr('data-link'),
				'attributeName': 'managerStatusId',
				'attributeNewValue': $button.attr('data-statusId'),
				//'scenario': 'changeManagerStatus',
			};
			
			if (!$button_wrapper.hasClass('is_opened')) {
				//console.log('показываем кнопки на выбор');
				$button_wrapper.children().css('display', 'inline-block');
				$button_wrapper.find('.btn-manager, .btn-manager-should-check-if-visited').hide();
				$button_wrapper.addClass('is_opened');
			} else {
				//console.log('прячем все кнопки');
				$button_wrapper.children().hide();
				$button_wrapper.removeClass('is_opened');
				
				$.ajax({
					url: '/newAdmin/ajax/changeAppointmentAttribute',
					async: true,
					data: ajaxParams,
					dataType: 'json',
					type: 'GET',
					success: function(response) {
						if (response.success == 'false') {
							alert(response.msg);
							document.location.reload();
							return false;
						}
					},
				});
			}
			$button_wrapper.parent().parent().removeClass("reminder_highlight");
			$button_wrapper.attr('data-managerStatusId', ajaxParams.newManagerStatusId);
			$button.css('display', 'inline-block');
		}
		return false;
	});
	
	$.each($button_wrappers, function() {
		var $button_wrapper = $(this);
		var $button_wrapper_buttons = $button_wrapper.find($buttons);
		var managerStatusId = $button_wrapper.attr('data-managerStatusId');
		var button_class_to_show = '.' + button_status_mapper[managerStatusId];		
		$button_wrapper_buttons.hide();
		$button_wrapper.find( button_class_to_show ).css('display', 'inline-block');
	});
	
	
	//select appType
	var $select_button_wrappers = $('.select_button_wrapper');
	
	$.each($select_button_wrappers, function() {
		var $select_button_wrapper = $(this),
			$select_button = $select_button_wrapper.find('select'),
			selectedAppType = $select_button_wrapper.attr('data-appType')
		;
		$select_button.val(selectedAppType);
		if ($select_button_wrapper.attr('data-disabled'))
			$select_button.prop('disabled', true);
		
		$select_button.change(function() {
			var newAppType = $(this).val(),
				appointmentLink = $(this).parent().attr('data-link');
			
			$.ajax({
				url: '/newAdmin/ajax/changeAppointmentAttribute',
				async: true,
				data: {
					'appointmentLink': appointmentLink,
					'attributeName': 'appType',
					'attributeNewValue': newAppType,
					'scenario': 'changeAppType',
				},
				dataType: 'json',
				type: 'GET',
				success: function(response) {
					if (response.success == 'false') {
						alert(response.msg);
						document.location.reload();
						return false;
					}
				},
			});
			
		});
	});
	
	//manager Comments
	var save_manager_comment = function(appointmentLink, newValue, callback) {
		$.ajax({
			url: '/newAdmin/ajax/changeAppointmentAttribute',
			async: true,
			data: {
				'appointmentLink': appointmentLink,
				'attributeName': 'managerComment',
				'attributeNewValue': newValue,
				'scenario': 'changeManagerComment',
			},
			dataType: 'json',
			type: 'GET',
			success: function(msg) {
				callback(msg);
			},
		});
	}
	
	var $save_buttons = $('.btn-manager-comment-save');
	$save_buttons.click(function() {
		var $button = $(this);
		var appointmentLink = $button.attr('data-link');
		var newValue = $button.prev().val();
		$button.html('...🕑...');
		save_manager_comment(appointmentLink, newValue, function(ajaxMessage) {
			if (ajaxMessage.success == 'false') {
				alert(ajaxMessage.msg);
				$button.html('Ошибка');
				$button.removeClass('bg-darkgreen bg-red c-white c-black');
				$button.addClass('bg-red c-white');
				window.setTimeout(function() {
					$button.removeClass('bg-darkgreen bg-red c-white c-black');
					$button.addClass('c-black');
					$button.html('Сохранить');
				}, 500);
				return false;
			}
			$button.html('ОК');
			$button.addClass('bg-darkgreen c-white');
			$button.removeClass('c-black');
			window.setTimeout(function() {
				$button.addClass('c-black');
				$button.removeClass('bg-darkgreen c-white');
				$button.html('Сохранить');
			}, 500);
		});
		return false;
	});
}

function initPopover() {
	$('.ipopover').popover();
}

function summComission() {
	var summa = 0;
	$('.comission').map(function() {
		summa += $(this).html()*1;
		return this;
	});
	$('#summa').html(summa);
}

function f_afterAjaxUpdate() {
	highlight_unprocessed_appointments();
	listen_button_clicks();
	summComission();
	initPopover()
}

$(document).ready(initPopover);

$(document).keypress(function(e) {
	
    if (e.which == 13) 
		if ( !$(':focus').attr("data-noSubmit") )
			submit_input_forms();
});



update_input_forms();
f_afterAjaxUpdate();
//});
</script>