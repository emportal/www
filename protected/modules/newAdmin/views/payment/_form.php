<div class="form"> 

<?php $form=$this->beginWidget('CActiveForm', array( 
    'id'=>'payment-form', 
    'enableAjaxValidation'=>false, 
));

Company::$showRemoved = true;
Address::$showInactive = true;
?>

    <!-- <p class="note">Обязательные поля отмечены звёздочкой <span class="required">*</span>.</p> -->
	
    <div class="error"> 
    <?php echo $form->errorSummary($model); ?>
    </div>

    <?php if(!$model->isNewRecord): ?>
    <div class=""> 
        <?php echo $form->labelEx($model,'createDate'); ?>
        <?php echo $form->textField($model,'createDate',array('readonly'=>1)); ?>
        <?php echo $form->error($model,'createDate'); ?>
    </div>
    <?php endif; ?>

    <div class="addressInput"> 
        <?php echo $form->labelEx($model,'addressId'); ?>
        <?php echo $form->textField($model,'addressId',array('size'=>36,'maxlength'=>36,'readonly'=>1,'style'=>'display:none; cursor: pointer;background: white;')); ?>
        <?php echo CHtml::textField('Payment[addressName]', join(", ",([$model->address->company->name, $model->address->name])), array('size'=>36,'maxlength'=>36,'readonly'=>1,'style'=>'cursor: pointer;background: white;')) ?>
        <?php echo $form->error($model,'addressId'); ?>
    </div>

    <div class=""> 
        <?php echo $form->labelEx($model,'billId'); ?>
        <?php echo $form->textField($model,'billId'); ?>
        <?php echo $form->error($model,'billId'); ?>
    </div> 

    <div class=""> 
        <?php echo $form->labelEx($model,'sum'); ?>
        <?php echo $form->textField($model,'sum',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->error($model,'sum'); ?>
    </div> 

    <div class=""> 
        <?php echo $form->labelEx($model,'purpose'); ?>
        <?php echo $form->textField($model,'purpose',array('size'=>60,'maxlength'=>180)); ?>
        <?php echo $form->error($model,'purpose'); ?>
    </div>

    <div class="buttons"> 
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
    </div> 

<?php $this->endWidget(); ?>

</div>

<assets style="display: none">
	<div id="SelectAddressForm">
	<?php
	if(!isset($_REQUEST["ajax"]) OR $_REQUEST["ajax"] === "SelectAddress_GridView") {
		Address::$showInactive = true;
		$criteria = new CDbCriteria();
		$criteria->select = "link, name";
		$criteria->with = [
			"company" => [
				"select" => "name",
				"together" => true,
			]
		];
		if(isset($_REQUEST["SelectAddress"]["link"]) && !empty(($_REQUEST["SelectAddress"]["link"]))) {
			$link = $_REQUEST["SelectAddress"]["link"];
			if(is_numeric($link)) {
				$criteria->addCondition("t.link=".intval($link));
			} else {
				$criteria->compare("t.link",$link,true);
			}
		} else {
			$criteria->with["company"]["with"] = [
					"companyType" => [
						"select" => false,
						"together" => true,
						"condition" => "companyType.isMedical = 1",
					]
				];
		}
		if(isset($_REQUEST["SelectAddress"]["name"]) && !empty(($_REQUEST["SelectAddress"]["name"]))) {
			$criteria->compare("t.name",$_REQUEST["SelectAddress"]["name"],true);
		}
		if(isset($_REQUEST["SelectAddress"]["companyName"]) && !empty(($_REQUEST["SelectAddress"]["companyName"]))) {
			$criteria->compare("company.name",$_REQUEST["SelectAddress"]["companyName"],true);
		}
			
		$selectAddress_dataProvider = new CActiveDataProvider(Address::model(), array(
			'criteria' => $criteria,			
		));
		$this->widget('bootstrap.widgets.TbGridView', array(
			'id' => "SelectAddress_GridView",
		    'type'=>'striped bordered condensed',
		    'dataProvider' => $selectAddress_dataProvider,
			'filter' => $selectAddress_dataProvider->model,
		    'template'=>"{items}{pager}",
		    'columns' => [
				[	
					'name'=>'link',
					'header'=>'Номер аккаунта',
					'filter'=> CHtml::textField('SelectAddress[link]', Yii::app()->request->getParam('SelectAddress')['link']),
					'htmlOptions' => [
						'style'=>'width: 10px,;text-align: left !important; cursor: pointer;'
					],
				],
				[	
					'name'=>'company.name',
					'header'=>'Название компании',
					'filter'=> CHtml::textField('SelectAddress[companyName]', Yii::app()->request->getParam('SelectAddress')['companyName']),
					'htmlOptions' => [
						'style'=>'text-align: left !important; cursor: pointer;'
					],
				],
				[	
					'name'=>'name',
					'header'=>'Адрес',
					'filter'=> CHtml::textField('SelectAddress[name]', Yii::app()->request->getParam('SelectAddress')['name']),
					'htmlOptions' => [
						'style'=>'text-align: left !important; cursor: pointer;'
					],
				],
		    ],
			'selectionChanged'=>'function(id){
				var key = $.fn.yiiGridView.getSelection(id);
				var row = 0;
				while($.fn.yiiGridView.getKey(id,row) != key && row < $.fn.yiiGridView.getColumn("SelectAddress_GridView",1).length) {
					row++;
				}
				var name = $.fn.yiiGridView.getColumn("SelectAddress_GridView",1).get(row).textContent + ", " + $.fn.yiiGridView.getColumn("SelectAddress_GridView",2).get(row).textContent;
				$("#Payment_addressId").val(key);
				$("#Payment_addressName").val(name);
				parent.$.fancybox.close();
			}',
		));
	}
	?>
	</div>
</assets>

<script>
$(function() {
    $(document).ready(function() {
    	var SelectAddressForm = $( "#SelectAddressForm" ).clone();
    	$( "#SelectAddressForm" ).remove();
    	var showSelectAddressForm = function (obj) {
    		var form = SelectAddressForm.clone();
    		//form.attr("id",form.attr("id")+"|");
    		$.fancybox({
    			id: "SelectAddress_fancyView",
    			content: form,
    		});
    	};
        $('.addressInput').live('click', function() {
        	showSelectAddressForm(this);
        });
    });
});
</script>