
<?php
/* @var $this Log1CPaymentController */ 
/* @var $model Log1CPayment */ 

$this->breadcrumbs=array( 
    'Log1 Cpayments'=>array('index'), 
    $model->id, 
);

$this->menu=array( 
    array('label'=>'List Log1CPayment', 'url'=>array('index')), 
    array('label'=>'Create Log1CPayment', 'url'=>array('create')), 
    array('label'=>'Update Log1CPayment', 'url'=>array('update', 'id'=>$model->id)), 
    array('label'=>'Delete Log1CPayment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')), 
    array('label'=>'Manage Log1CPayment', 'url'=>array('admin')), 
); 
?> 

<h1>View Log1CPayment #<?php echo $model->id; ?></h1> 

<?php $this->widget('zii.widgets.CDetailView', array( 
    'data'=>$model, 
    'attributes'=>array( 
        'id',
        'dateUploaded',
        'date',
        'dateOfWithdrawal',
        'sum',
        'payerAccount',
        'payer1Name',
        'payer2Name',
        'payerSettlementAccount',
        'payerINN',
        'payerKPP',
        'payerBank1Name',
        'payerBank2Name',
        'payerBIK',
        'payerCorrespAccount',
        'receiverAccount',
        'receiverINN',
        'receiverKPP',
        'receiver1Name',
        'receiver2Name',
        'receiverSettlementAccount',
        'receiverBank1Name',
        'receiverBank2Name',
        'receiverBIK',
        'receiverCorrespAccount',
        'paymentType',
        'paymentMethod',
        'paymentNumber',
        'paymentPurpose',
        'rawData',
    ), 
)); ?> 