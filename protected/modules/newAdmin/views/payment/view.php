
<style>
select, input {
	margin-bottom: 1px !important;
}
.animation {
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
    -o-transition: all 0.5s;
    transition: all 0.5s;
    display: block;
    overflow: hidden;
}
.hideButton {
    height: 0px;
    padding: 0px;
    margin: 0px;
}
</style>
<div style="display: inline-block; width: 100%;">
	<div style="float: left; border: 0px solid;">
	<h1 class="smi_link">Платежи</h1>
	<br><br>
	<a href="create" class="button nl bg-green c-white border_radius_5px">Добавить платёж</a>
	</div>
	<div style="float: right; margin: 0px 0 5px 0; border: 0px solid;">
		<!-- <a href="#">Фильтры</a> -->
		<table style="margin-top: -10px;">
			<tr>
				<td style="width: 200px;">
					Менеджер
				</td>
				<td>
					<select id="input_manager_id" onchange="submit_input_forms();">				
						<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
							<option value="">Все</option>
						<?php endif; ?>
						
						<?php foreach($managerListData as $key=>$data): ?>
							<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
						<?php endforeach; ?>
						
						<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
							<option value="-1">Без менеджеров</option>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Название клиники
				</td>
				<td>
					<input id="input_company_name" type="text" style="width: 100%;">
				</td>
			</tr>
			<tr>
				<td>
					Рассматриваемый месяц
				</td>
				<td>
					<select id="input_report_month" onchange="submit_input_forms();">
						<!-- <option value="">Все</option> -->
						<?php 
	                    $printReportMonthOptions = function() {
							echo '<option value=""> Все </option>';
							$mindate = Yii::app()->db->createCommand("SELECT min(createDate) FROM payment;")->queryScalar();
	                        $dateStart = new Datetime($mindate);
	                        $dateNow = new Datetime(); 
	                        $yearDiff = $dateStart->diff($dateNow)->y;
	                        $monthDiff = $yearDiff*12 + $dateStart->diff($dateNow)->m + 1;
	                        for ($i = 0; $i <= $monthDiff; $i++) {
	                            $value = $dateStart->format('Y-m'); //date('Y') . '-' . sprintf('%02d', $i);
	                            $name = MyTools::getRussianMonthsNames()[$dateStart->format('n') - 1] . ' ' . $dateStart->format('Y');
	                            $dateStart->modify('+1 month');
	                            echo '<option value="' . $value . '"> ' . $name . ' </option>';
	                        }
	                    };
	                    $printReportMonthOptions();
	                    ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label>Показывать не обработанные</label>
				</td>
				<td>
					<input type="checkbox" data-default='checked' id="input_show_notprocessed" onchange="submit_input_forms();">
				</td>
			</tr>
			<tr>
				<td>
					<label>Показывать подтверждённые</label>
				</td>
				<td>
					<input type="checkbox" data-default='checked' id="input_show_approved" onchange="submit_input_forms();">
				</td>
			</tr>
			<tr>
				<td>
					<label>Показывать отклонённые</label>
				</td>
				<td>
					<input type="checkbox" data-default='' id="input_show_rejects" onchange="submit_input_forms();">
				</td>
			</tr>
		</table>
	    <input type="hidden" id="input_patient_id">
	</div>
</div>
<?php
$dataProvider = $model->search([
		'managerId'=>Yii::app()->request->getParam('manager_id'),
		'companyName'=>Yii::app()->request->getParam('company_name'),
		'reportMonth'=>Yii::app()->request->getParam('report_month'),
		'showRejects'=>Yii::app()->request->getParam('show_rejects'),
		'showApproved'=>Yii::app()->request->getParam('show_approved'),
		'showNotProcessed'=>Yii::app()->request->getParam('show_notprocessed'),
	]);
$dataProviderForTotal = $dataProvider;
$dataProviderForTotal->setPagination(false);
$allData = $dataProviderForTotal->getData();
$totalItem = count($allData);
$totalSum = 0;
foreach ($allData as $data) {
	$totalSum += $data->sum;
}
?>
<div style="text-align: right;font-size: 18px;font-weight: bold;"><?= $totalItem ?> платежей на общую сумму <?= $totalSum ?> рублей</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => new CArrayDataProvider($allData, ['pagination'=>[ 'pageSize' => '100' ]]),
	'id' => 'payment-list',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
	'afterAjaxUpdate' => 'f_afterAjaxUpdate',
	'columns' => array(
		[
			'name' => 'addressId',
			'header' => 'Клиника плательщик',
			'value' => function($data) {
				$output = '';
				$address = $data->address;
				if(is_object($address)) {
					$output = '<a href="../ClinicCard/view?id=' . $address->company->id . '">' . CHtml::encode($address->company->name) . '</a>';
					$output .= ', <a href="' . $address->getPagesUrls(true,true) . '" target="_blank">';
					$output .= CHtml::encode($address->shortName);
					$output .= '</a>';
					$output .= '<br><button class="" onclick="$.fancybox({href:\'/newAdmin/payment/balance?clinicId='.urlencode($address->id).'\'})">Баланс</button>';
				}
				return $output;
			},
			'htmlOptions' => [
				'style' => 'width: 90px; text-align:center'
			],
			'type' => 'raw'
		],
		[
			'name' => 'billId',
			'header' => 'По счёту №',
			'value' => function($data) {
				$output = $data->billId . "<br>";
				if(is_object($data->bill)) {
					if($data->bill->addressId != $data->addressId) {
						//$output .= "<span class='error'>От другого адреса!<span>";
					} else {
						$reportMonthDate = $data->bill->reportMonth."-01";
						$reportMonthTimestamp = strtotime($reportMonthDate);
						$m = date('m',$reportMonthTimestamp);
						$output .= MyTools::getRussianMonthsNames()[$m-1] . date(' Y',$reportMonthTimestamp) . "<br>";
					}
				} else {
					//$output .= "<span class='error'>не найден!<span>";
				}
				return $output;
			},
			'htmlOptions' => [
				'style' => 'width: 60px; text-align:center'
			],
			'type' => 'raw'
		],
		[
			'name' => 'createDate',
			'header' => 'Дата',
			'value' => function($data) {
					$result = RuDate::post($data->createDate,false,false) ;
					return $result;
				},
			'htmlOptions' => [
				'style' => 'width: 90px; text-align:center'
			]
		],
		[
			'name' => 'purpose',
			'header' => 'Назначение платежа',
			'value' => function($data) {
					$result = $data->purpose;
					return $result;
				},
			'htmlOptions' => [
				'style' => 'width: 90px; text-align:center'
			]
		],
		[
			'name' => 'sum',
			'header' => 'Сумма',
			'value' => function($data) {
					$result = $data->sum;
					return $result;
				},
			'htmlOptions' => [
				'style' => 'width: 90px; text-align:center'
			]
		],
		[
			'name' => 'statusId',
			'header' => 'Статус',
			'value' => function($data) {
				$output = '<span class="button_wrapper" data-statusId="' . $data->statusId . '" data-id="' . $data->id . '">';
				$output .= '<a href="#" data-statusId="1" class="button '.(($data->statusId != 1)?'hideButton':'').' animation fullwidth bg-darkgreen c-white nl non-active">';
				$output .= "Подтверждён";
				$output .= '</a>';
				$output .= '<a href="#" data-statusId="0" class="button '.(($data->statusId != 0)?'hideButton':'').' animation fullwidth bg-darkblue c-white nl non-active">';
				$output .= "Не обработан";
				$output .= '</a>';
				$output .= '<a href="#" data-statusId="-1" class="button '.(($data->statusId != -1)?'hideButton':'').' animation fullwidth bg-red c-white nl non-active">';
				$output .= "Отклонён";
				$output .= '</a>';
				if ($data->statusId == 0) $output .= '<span class="unprocessed_marker"></span>';
				
				$output .= '</span>';
				return $output;
			},
			'type' => 'raw',
			'htmlOptions' => [
				'style' => 'width: 90px; height: 90px; vertical-align: middle;'
			]
		],
		/*
		[
			'name' => 'statusId',
			'header' => 'Статус',
			'value' => function($data) {
					$result = '';
					switch ($data->statusId) {
						case -1:
							$result = 'Отклонён';
							break;
						case 1:
							$result = 'Подтверждён';
							break;
						case 0:
						default:
							$result = 'Не обработан';
							break;
					}
					return $result;
				},
			'htmlOptions' => [
				'style' => 'width: 90px; text-align:center'
			]
		],
		*/
		[
			'class' => 'CButtonColumn',
			'header' => 'Действие',
			'template' => '{update}',
			'buttons' => [
				'update' => [
					'url' => 'CHtml::normalizeUrl(array("payment/update","id"=>$data->id))',
					'label' => 'Редактировать',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-primary',
						'style' => 'height:20px; width: 80%;',
					]
				],
				/*
				'logInfo' => [
					'url' => 'CHtml::normalizeUrl(array("payment/log1CPayment","id"=>$data->logId))',
					'label' => 'Расширенная информация',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-primary',
						'style' => 'height:40px; width: 80%;',
					]
				],
				*/
			],
			'htmlOptions' => [
				'style' => 'width: 90px; height: 90px; vertical-align: middle;'
			]
		],
	),
));
?>
<script> var submit_path = '/newAdmin/payment/view?'; </script>
<script>
//$(document).ready(function() {
var data_values = {
	'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
	'company_name': '<?=Yii::app()->request->getParam('company_name')?>',
	'report_month': '<?=Yii::app()->request->getParam('report_month')?>',
	'show_rejects': '<?=Yii::app()->request->getParam('show_rejects')?>',
	'show_approved': '<?=Yii::app()->request->getParam('show_approved')?>',
	'show_notprocessed': '<?=Yii::app()->request->getParam('show_notprocessed')?>',
};
	
function update_input_forms() {
	
	for(var key in data_values) {
		var input = $('#input_'+key).get(0);
		if (!data_values[key]) data_values[key] = '';
		if (typeof $(input) != 'undefined') {
			
			if ($(input).prop('type') == 'select-one') {
			
				$(input).find('option').each(function(){
					
					if (this.value == data_values[key]) {
						
						$(input).val(data_values[key]);
					}
				});
			}
			else if($(input).prop('type') == "checkbox") {
				if(!isNaN(parseInt(data_values[key]))){
					console.log('>' + $(input).attr('id') + ':' + (data_values[key] ? 'true' : 'false'));
					if(parseInt(data_values[key])) {
						$(input).prop('checked',1);
					}
					else {
						$(input).removeProp('checked');
					}
				}
				else {
					console.log('>>' + $(input).attr('id') + ':' + (($(input).data('default') == 'checked') ? 'true' : 'false'));
					if($(input).data('default') == 'checked') {
						$(input).prop('checked',$(input).data('default'))
					}
					else {
						$(input).removeProp('checked');
					}
				}
			}
			else {
				$(input).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		var input = $('#input_'+key).get(0);
		if ($(input).length) { //если существует на странице такой DOM элемент

			if($(input).prop('type') == "checkbox") {
				data_values[key] = $(input).is(':checked') ? '1' : '0';
			} else {
				data_values[key] = $(input).val();
			}
			if (data_values[key] != '') {query += '&' + key + '=' + encodeURIComponent(data_values[key]);}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

////////

function closeAllWrappers() {
	var $button_wrappers = $('.button_wrapper.is_opened');
	$button_wrappers.each(function(key, wrapper) {
		$(wrapper).removeClass('is_opened');
		$(wrapper).find("a").each(function(key, button) {
			if($(wrapper).attr('data-statusId') != $(button).attr('data-statusId')) {
				//$(button).fadeOut(500);
				if(!$(button).hasClass("hideButton")) $(button).addClass("hideButton");
			}
		});
	});
}

function listen_button_clicks() {
	
	var $button_wrappers = $('.button_wrapper');
	
	$button_wrappers.find("a").not('.found').click(function() {
		var $button = $(this);
		$button_wrapper = $button.closest('.button_wrapper');

		if($button_wrapper.hasClass('is_opened')) {
			$button_wrapper.removeClass('is_opened');
			var complete = function() {
    			$button_wrapper.find("a").each(function(key, el) {
    				if($button_wrapper.attr('data-statusId') != $(el).attr('data-statusId')) {
    					//$(el).fadeOut(500);
						if(!$(el).hasClass("hideButton")) $(el).addClass("hideButton");
    				}
    			});
			};
			if($button_wrapper.attr('data-statusId') == $button.attr('data-statusId')) {
				complete();
			} else {
	            $.ajax({
	                url: '/newAdmin/payment/approve',
	                async: true,
	                data: {
	                	id : $button_wrapper.attr('data-id'),
	                	statusId : $button.attr('data-statusId')
	                },
	                dataType: 'json',
	                type: 'POST',
					beforeSend: function() {
					
					},
	                success: function(response) {
	                    if(response.success) {
	            			$button_wrapper.attr('data-statusId', $button.attr('data-statusId'));
	                    } else {
	                        this.error(null,response.errors,null);
	                    }
					},
	                error: function(XHR,errorText,errorThrown) {
	                	alert(JSON.stringify([errorText].join(';')));
					},
	                complete: complete,
				});
			}
		} else {
			closeAllWrappers();
			$button_wrapper.addClass('is_opened');
			//$button_wrapper.find("a").fadeIn(500);
			$button_wrapper.find("a").removeClass("hideButton");
		}
		return false;
	}).addClass('found');
}

function f_afterAjaxUpdate() {
	listen_button_clicks();
}

$(document).keypress(function(e) {
	
    if (e.which == 13) 
		if ( !$(':focus').attr("data-noSubmit") )
			submit_input_forms();
});

update_input_forms();
f_afterAjaxUpdate();
//});
</script>