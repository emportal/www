<?php
/* @var $this ClinicListController */
/* @var $model Address */
?>

<div style="margin: 00px 0 0 0;">
<h1 class="smi_link">Продажи</h1>
<div style="float: right; margin: 0px 0 5px 0;">
	<table>
		<tr>
			<td style="width: 200px;">
				Тип клиники 
			</td>
			<td>
				<select id="input_company_type" onchange="submit_input_forms();" disabled>				
					<option value="">Активные на сайте</option>
					<option value="all">Все</option>
					<option value="inactive">Неактивные + стадо</option>
					<option value="inactive_only">Только неактивные</option>
					<option value="herd_only">Только стадо</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 200px;">
				Показать клиники сотрудника 
			</td>
			<td>
				<select id="input_manager_id" onchange="submit_input_forms();">				
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
						<option value="">Все</option>
					<?php endif; ?>
					
					<?php foreach($managerListData as $key=>$data): ?>
						<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
					<?php endforeach; ?>
					
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
						<option value="-2">Только с менеджерами</option>
						<option value="-1">Без менеджеров</option>
					<?php endif; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Название клиники
			</td>
			<td>
				<input id="input_company_name" type="text">
			</td>
		</tr>
	</table>
</div>

<div style="clear: both;">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $model->dataProviderForNewAdminPanel(),
	#'filter' => $model,
	#'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
	'columns' => array(
		[
			'name' => 'name',
			'header' => 'Клиника',
			'value' => function($data) {
					
					return '<a target="_blank" href="../ClinicCard/view?id='.$data->company->id.'">'.$data->company->name.'</a>';
				},
			'filter' => false,
			'htmlOptions' => [
				'style'=> 'width: 300px'
			],
			'type' => 'raw'
		],
		[
			'name' => 'name',
			'header' => 'Адрес',
			'value' => '$data->name',
			'filter' => false,
		],
		[
			'name' => 'name',
			'header' => 'Состоялось визитов',
			'value' => function($data) {
					
					$result = $data->company->getAppointmentsCount(['type'=>'contemporary', 'addressId'=> $data->id, 'apt_status'=>'4']);
					return $result;
				},
			'filter' => false,
		],
		[
			'name' => 'name',
			'header' => 'Сумма',
			'value' => function($data) {
					
					$result = '';
					$salesContract = $data->salesContract;
					if ($salesContract) {
						
						
						$result = $salesContract->salesContractType->price * $data->company->getAppointmentsCount(['type'=>'contemporary', 'addressId'=> $data->id, 'apt_status'=>'4']);
					}
					//if (!$data->salesContract->salesContractType->) $data->salesContract->salesContractType->description;				
					return $result;
				},
			'filter' => false,
		],
		[
			'name' => 'name',
			'header' => 'Менеджер',
			'value' => '$data->company->managerRelations->user->id ? $data->company->managerRelations->user->name : "-"',
			'filter' => false,
		],

	),
));
?>
</div>

<script> var submit_path = '/newAdmin/contracts/view?'; </script>
<script>
//$(document).ready(function() {
	var data_values = {
		
		'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
		'company_name': '<?=Yii::app()->request->getParam('company_name')?>',
		'company_type': '<?=Yii::app()->request->getParam('company_type')?>',
	};
	
function update_input_forms() {
	
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = '';
		if (typeof $('#input_'+key) != 'undefined') {
			
			if ($('#input_'+key).prop('type') == 'select-one') {
				
				$('#input_'+key+' option').each(function(){
					
					if (this.value == data_values[key]) {
						
						$('#input_'+key).val(data_values[key]);
					}
				});
			} else {
				$('#input_'+key).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		
		if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
			
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != '') {query += '&' + key + '=' + data_values[key];}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

update_input_forms();

$(document).keypress(function(e) {
	
    if(e.which == 13) {submit_input_forms();}
});
//});
</script>