<?php
/* @var $this ClinicListController */
/* @var $model Address */
?>
<style>
select, input {
	margin-bottom: 1px !important;
}
</style>
<h1 class="smi_link">История контактов</h1>
<div style="float: right; margin: 0px 0 5px 0; border: 0px solid;">
	<table>
		<tr>
			<td>
				Регион
			</td>
			<td>
				<select id="input_region" onchange="submit_input_forms();">
					<option value="">Все</option>
					<?php foreach(Yii::app()->params['regions'] as $region): ?>
						<option value="<?=$region['subdomain']?>"><?=$region['name']?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 160px;">
				Сотрудник
			</td>
			<td>
				<select id="input_manager_id" onchange="submit_input_forms();">		
					<?php if (!empty(Yii::app()->request->getParam('company_name')) OR Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
						<option value="">Все</option>
					<?php endif; ?>
					<?php foreach($managerListData as $key=>$data): ?>
						<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Название клиники
			</td>
			<td>
				<input id="input_company_name" type="text">
			</td>
		</tr>
		<tr>
			<td>
				Результат
			</td>
			<td>
				<select id="input_result_status" onchange="submit_input_forms();">
					<option value="">Все</option>
					<?php foreach($model::$RESULT_TYPES as $key=>$data): ?>
						<option value="<?=$key?>"><?=$data?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Показать контакты
			</td>
			<td>
				<?php
				$timeint = [
					'prevmonth' => date('d.m.Y', strtotime('-1 month')),
					'prevweek' => date('d.m.Y', strtotime('-1 week')),
					'prevday' => date('d.m.Y', strtotime('-1 day')),
					'today' => date('d.m.Y', time()),
					'nextweek' => date('d.m.Y', strtotime('+1 week')),
					'nextmonth' => date('d.m.Y', strtotime('+1 month')),
				];
				?>
				<select id="input_time_period" onchange="submit_input_forms();">
					<option value="">все имеющиеся</option>
					<option value="<?= $timeint['prevmonth'] . '-' . $timeint['today'] ?>">за прошедший месяц</option>
					<option value="<?= $timeint['prevweek'] . '-' . $timeint['today'] ?>">за прошедшую неделю</option>
					<option value="<?= $timeint['prevday'] . '-' . $timeint['today'] ?>">за прошедшие сутки</option>
					<option value="<?= $timeint['today'] . '-' . $timeint['nextweek'] ?>">в ближайшую неделю</option>
					<option value="<?= $timeint['today'] . '-' . $timeint['nextmonth'] ?>">в ближайший месяц</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="vhidden">
				Тип
			</td>
			<td class="vhidden">
				<select id="input_task_status" onchange="submit_input_forms();">
					<option value="">все</option>
					<option value="1">только с задачами</option>
					<option value="2">без задач</option>
				</select>
			</td>
		</tr>
	</table>
</div>
<br><br>
<a href="create" class="btn btn-success">Добавить новый</a>
<div style="border: 0px solid; margin: 35px 0 0 0;">
	<span style="background-color: #F9FF85; border-radius: 3px; padding: 4px;">
	Подсвечены</span> контакты, которые должны состояться в ближайшие два часа
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $model->dataProviderForNewAdminPanel($dtOptions),
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
	'afterAjaxUpdate' => 'highlight_current_tasks',
	'columns' => array(
		[
			'name' => 'plannedTime',
			'header' => 'Планируемое время',
			'value' => function($data) {
					
					$result = '';
					$plannedTimeUnix = strtotime($data->plannedTime);
					
					//$chunks_g = explode(" ", $data->plannedTime);
					//$chunks[0] = explode("-", $chunks_g[0]);
					//$chunks[1] = explode(":", $chunks_g[1]);					
					//$result = $chunks[0][2].'.'.$chunks[0][1].' | '.$chunks[1][0].':'.$chunks[1][1];
					$result = date('H:i d.m.Y', $plannedTimeUnix);
					
					$contactTypeStr = CompanyContact::$TYPES[$data->contactTypeId];
					if ($contactTypeStr != 'Телефонный звонок') $result .= '<div class="bold">' . $contactTypeStr . '</div>';
					
					$shouldContactSoon = (($plannedTimeUnix > time()) && ($plannedTimeUnix < (time() + 7200)));
					$contactTimePassed = ($plannedTimeUnix < time());
					$haveNotContactedYet = ($data->resultTypeId == $data::RESULT_NOT_SET_YET);
					
					if (($shouldContactSoon || $contactTimePassed) && $haveNotContactedYet)
						$result .= '<span class="remind_marker"></span>';
					
					return $result;
				},
			'type' => 'raw',
			'htmlOptions' => [
				'style' => 'width: 80px; text-align: center;',
			],
		],
		[
			'name' => 'doctorId',
			'header' => 'Клиника',
			'value' => '$data->company ? $data->company->name : $data->companyName',
			'value' => function($data) {
					
					$result = $data->company ? '<a href="../ClinicCard/view?id='.$data->company->id.'">'.$data->company->name.'</a>' : $data->companyName;
					return $result;
				},
			'type' => 'raw'
		],
		[
			'name' => 'doctorId',
			'header' => 'Ответственный',
			'value' => '$data->manager->name',
		],
		[
			'name' => 'resultTypeId',
			'header' => 'Результат',
			'value' => 'CompanyContact::$RESULT_TYPES[$data->resultTypeId]',
		],
		[
			'name' => 'taskMessage',
			'header' => $this->getTaskHeader( Yii::app()->request->getParam('task_status') ),
			'value' => '"<b>" . $data->taskMessage . "</b>"',
			'sortable' => 'false',
			'type' => 'raw'
		],
		[
			'name' => 'comment',
			'header' => 'Комментарий',
			'value' => '$data->comment',
			'sortable' => 'false',
			'type' => 'raw'
		],
		[
			'header' => 'Действия',
			'class' => 'CButtonColumn',
			'template' => '{edit} {add}',
			'buttons' => [
				'edit' => [
					'url' => 'CHtml::normalizeUrl(array("companyContact/view", "id"=>$data->id))',
					'label' => '<i class="icon-pencil icon-white"></i>',
					'imageUrl' => false,
					'options' => [
						'title' => 'Смотреть контакт',
						'class' => 'btn btn-primary'
					]
				],
				'add' => [
					'url' => 'CHtml::normalizeUrl(array("companyContact/create?" . $data->newContactString))',
					'label' => '<i class="icon-plus icon-white"></i>',
					'imageUrl' => false,
					'options' => [
						'title' => 'Создать контакт из этих данных',
						'class' => 'btn btn-success'
					]
				]
			],
			'htmlOptions' => [
				'style' => 'width: 100px; text-align: center;'
			]
		]
	),
));
?>
<script>
	var submit_path = '/newAdmin/companyContact/list?'; 
	
	var data_values = {
		'region': '<?=Yii::app()->request->getParam('region')?>',
		'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
		'company_name': '<?=Yii::app()->request->getParam('company_name')?>',
		'result_status': '<?=Yii::app()->request->getParam('result_status')?>',
		'time_period': '<?=Yii::app()->request->getParam('time_period')?>',
		'task_status': '<?=Yii::app()->request->getParam('task_status')?>',
	};
	
	function highlight_current_tasks() {
		$('.remind_marker').parent().parent().addClass("reminder_highlight");
	}
	
	highlight_current_tasks();
	
	function change_filter(filterId, value, shouldSubmit) {
		
		console.log('filterId = ' + filterId);
		var $filter = $('#' + filterId);
		$filter.val(value);
		if (shouldSubmit == 1) submit_input_forms();
		return true;
	}
</script>

<script>	
function update_input_forms() {
	
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = '';
		if (typeof $('#input_'+key) != 'undefined') {
			
			if ($('#input_'+key).prop('type') == 'select-one') {
			
				$('#input_'+key+' option').each(function(){
					
					if (this.value == data_values[key]) {
						
						$('#input_'+key).val(data_values[key]);
					}
				});
			} else {
				$('#input_'+key).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		
		if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
			
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != '') {query += '&' + key + '=' + encodeURIComponent(data_values[key]);}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

update_input_forms();

$(document).keypress(function(e) {
	
    if(e.which == 13) {submit_input_forms();}
});

</script>