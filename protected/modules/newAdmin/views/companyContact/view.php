<?php
/* @var $this СompanyContactController */
/* @var $model СompanyContact */

?>

<h1>Просмотр контакта #<?=$companyContact->id?></h1> 
<small>
	создан <?=$companyContact->createdTime?> 
	сотрудником <?=$companyContact->manager->name?>
</small>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'companyContact'=>$companyContact, 'managerListData'=>$managerListData, 'input_disabled'=> ' disabled', 'input_readonly'=> ' readonly')); ?>