<?php
$portableAttributes = ['companyId', 'contactTypeId', 'personName', 'personPhone', 'companyName', 'managerId'];
foreach($portableAttributes as $key=>$val) $newContactOptions .= '&options['.$val.']='.$companyContact->$val;

if (isset($options) AND !$companyContact) {
	foreach($portableAttributes as $key=>$val) {
		$companyContact[$val] = $options[$val]; 
	}
	$companyContact = (object)$companyContact;
}

if (!$options['companyId']) $options['companyId'] = $companyContact->company->id;
$isDisabled = (!$companyContact->id) ? 'disabled' : '';
?>

<style>
.tip-box {
	float: left;
	padding: 2px 6px;
	margin: 0px 0 4px 0;
	border: 0px solid;
	background-color: #eee;
	border-radius: 4px;
	-webkit-user-select: none;  /* Chrome all / Safari all */
	-moz-user-select: none;     /* Firefox all */
	-ms-user-select: none;      /* IE 10+ */
	-o-user-select: none;
	user-select: none;
}
.tip-box:not(:first-child) {
	margin-left: 4px;
}
.tip-box-block:hover {
	background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
	color: #fff;
}
.no-background {
	background-color: transparent;
	background-image: none;
}
.cursor-pointer {
	cursor: pointer;
	cursor: hand;
}
.noselect {
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
}
textarea, input[type="text"] {
	width: 220px;
}
textarea {
	height: 60px;
}

</style>

<?php if (Yii::app()->user->hasFlash('contactUpdated')) : ?>
<div class="flash-success">
	<?= Yii::app()->user->getFlash('contactUpdated');  ?>
</div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('contactUpdatedError')) : ?>
<div class="flash-error">
	<?= Yii::app()->user->getFlash('contactUpdatedError');  ?>
</div>
<?php endif; ?>


<form id="contactForm" enctype="multipart/form-data" method="post">

	<br><br>
	<table>
	<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')) : ?>
		<tr>
			<td>
				Исполнитель
			</td>
			<td>
				<select id="managerId" name="params[managerId]"<?=$input_disabled?>>
					<?php foreach($managerListData as $key=>$data): ?>
						<?php 
							if ($companyContact->id)
							{
								$input_selected = ($key == $companyContact->managerId) ? ' selected' : '';
							} else {
								if ($companyContact->managerId) {
									$input_selected = ($key == $companyContact->managerId) ? ' selected' : '';
								} else {
									$input_selected = ($key == Yii::app()->user->id) ? ' selected' : '';
								}
							}							
						?>
						<option value="<?=$key?>"<?=$input_selected?>><?=CHtml::encode($data)?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td></td>
		</tr>
	<?php endif; ?>
		<tr>
			<td>
				Задача
			</td>
			<td>
				<textarea id="taskMessage" name="params[taskMessage]" placeholder="(не обязательно)"<?=$input_disabled?>><?=$companyContact->taskMessage?></textarea>
			</td>
			<td></td>
		</tr>
		<tr>
			<td style="width: 200px;">
				Тип контакта
			</td>
			<td style="width: 230px;">
				<select id="contactTypeId" name="params[contactTypeId]"<?=$input_disabled?>>
					<?php foreach($model::$TYPES as $key=>$data): ?>
						<?php
						$selected = ($companyContact->contactTypeId == $key) ? 'selected' : '';
						?>
						<option value="<?=$key?>" <?=$selected?>><?=$data?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td style="width: 500px;"></td>
		</tr>
		<tr>
			<td>
				Запланированное время
			</td>
			<td>
				<input type="text" id="plannedTime" name="params[plannedTime]"<?=$input_readonly?> value="<?=substr($companyContact->plannedTime, 0, 16)?>">
			</td>
			<td>
				<?php if ($companyContact->id) : ?>
					<a href="create?<?=$newContactOptions?>">Создать новый контакт из исходного</a>				
				<?php endif; ?>				
			</td>
		</tr>
		<tr>
			<td style="vertical-align: top;">
				Организация
			</td>
			<td style="height: 40px; vertical-align: top; padding: 0 0 8px 0;">
				<input type="hidden" id="companyName_id" name="params[companyId]" value="<?=$options['companyId']?>">
				<input type="text" id="companyName" name="params[companyName]" style="opacity: 0.8; display: none;" onfocus="request_tips({'type': 'companyName'});" onkeydown="request_tips({'type': 'companyName'});" <?=$input_disabled?>>
				<div id="companyName_button" onclick="select_from_tips({'type': 'companyName', 'showTips': 'true'}); return false;" class="tip-box btn-info cursor-pointer" style="margin: 0px 0 0 0;" title="выбрать организацию из списка">
					<?php
					if ($companyContact->contactTypeId) {					
						echo ($companyContact->company->name) ? $companyContact->company->name : $companyContact->companyName;
					} else {						
						echo 'Выбрать';
					}
					?>
				</div>
			</td>
			<td rowspan="3" style="height: 40px; vertical-align: top;">				
				<div id="companyName_tips" style="position: absolute; width: 400px; height: 400px; margin-left: 0px; display: none;">					
					<!-- <div class="tip-box cursor-pointer" onclick="select_from_tips({'type': 'companyName', 'id': '0b98dfd5-0baa-11e2-a1cd-e840f2aca94f', 'companyName': 'Аймед'}); return false;">
						Аймед
					</div>-->
				</div>				
			</td>
		</tr>
		<tr>
			<td>
				Имя
			</td>
			<td>
				<input type="text" id="personName" name="params[personName]" value="<?=$companyContact->personName?>" <?=$input_disabled?>>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				Телефон
			</td>
			<td>
				<input type="text" id="personPhone" name="params[personPhone]" value="<?=$companyContact->personPhone?>" <?=$input_disabled?>>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				Результат
			</td>
			<td>
				<select id="resultTypeId" name="params[resultTypeId]" <?= $isDisabled ?>>
					<?php foreach($model::$RESULT_TYPES as $key=>$data): ?>
						<?php
						$selected = ($companyContact->resultTypeId == $key) ? 'selected' : '';
						?>
						<option value="<?=$key?>" <?=$selected?>><?=$data?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				Комментарий
			</td>
			<td>
				<textarea id="comment" name="params[comment]" <?= $isDisabled ?>><?= $companyContact->comment ?></textarea>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				
			</td>
			<td colspan="2">
				<input type="submit" value="Сохранить" class="btn btn-success">
				<a href="/newAdmin/companyContact/list"><div class="btn btn-danger">Отменить и вернуться назад</div></a>
			</td>
		</tr>
	</table>
</form>




<script>
	
	var no_ajax_pickup = '<?=$companyContact->contactTypeId?>';
	
	function select_from_tips(options) {
		
		if (!no_ajax_pickup) {} else {return false;}
		if (!options || typeof options == 'undefined') {alert('options not defined'); return false;}
		
		if (!options['showTips']) {
			
			$('#companyName').val(options['companyName']);
			$('#'+options['type']+'_tips').css('display', 'none');
			$('#'+options['type']+'_button').css('display', 'block');
			$('#'+options['type']).css('display', 'none');
			$('#'+options['type']+'_button').html(options['companyName']);
			$('#'+options['type']+'_id').val(options['id']);
		} else {
			
			$('#'+options['type']+'_tips').css('display', 'block');
			$('#'+options['type']+'_button').css('display', 'none');
			$('#'+options['type']).css('display', 'inline');
			$('#'+options['type']).val('');
			$('#'+options['type']).focus();
			
			$('#'+options['type']+'_tips').html('');
			$('#'+options['type']+'_tips').html('<i>уточните название организации</i>');
			
			$('#'+options['type']+'_id').val('');		
		}		
	}
	
	function request_tips(options) {
		
		clearTimeout(window.ftext_ti); 
		window.ftext_ti = setTimeout(function(){get_tips(options);}, 400);
	}
	
	function get_tips(options) {
		
		if (!options || typeof options == 'undefined') {alert('options not defined'); return false;}
		
		$.ajax({
			url: "/newAdmin/ajax/getTips",
			async: true,
			data: ({
				type: options['type'],
				nameLike: $('#'+options['type']).val()
			}),
			dataType: 'json',
			type: "GET",
			success: function(msg) {
				
				if (msg.success) {
					
					inject_tips({'type': options['type'], 'data': msg.data});
				}
			},
		});
		
	}
	
	function cleanify(str) {
	
		return str = str.toString().replace(/"/g, '').replace(/'/g, "");
	}
	
	function inject_tips(options) {
		
		var html_data = '';
		for(key in options['data']) {
			
			var name = cleanify(options['data'][key]['name']);
			var description = cleanify(options['data'][key]['description']);
			
			var this_html = "<div class=\"tip-box tip-box-block cursor-pointer\" title=\"" + description + "\" onclick=\"select_from_tips({'type': '" + options['type'] + "', 'id': '" + options['data'][key]['id'] + "', 'companyName': '" + name + "'}); return false;\">" + name + "</div>";
			html_data += this_html;
		}
		
		if (!html_data) {
			if (!$('#'+options['type']).val()) {
				html_data = '<i>уточните название организации</i>';
			} else {
				html_data = '<i>ничего не найдено</i>';
			}
		} else {
			html_data = '<div class="tip-box no-background">Варианты:</div>' + html_data;
		}
		
		$('#'+options['type']+'_tips').html('');
		$('#'+options['type']+'_tips').html(html_data);
	}
	
<?php if (!$companyContact->id) : ?>
	$(document).ready(function() {
		
		$('#plannedTime').datetimepicker({
			lang: 'ru',				
			format: 'Y-m-d H:i'
		});
	});
<?php endif; ?>
	
	
	$("#contactForm").submit(function(e) {
		
		var self = this;
		e.preventDefault();
		if (!$('#companyName_id').val()) {
			
			alert('Ошибка! Не выбрана организация!');
			return false;
		} else if (!$('#plannedTime').val()) {
			
			alert('Ошибка! Не выбрано время проведения!');
			return false;
		} else {
			self.submit();
		}
		return false;
	});
</script>