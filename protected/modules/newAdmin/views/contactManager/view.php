<?php
/* @var $this ClinicListController */
/* @var $model Address */
?>

<h1 class="smi_link">Сообщения от клиник</h1>

<div style="float: right; margin: 0px 0 5px 0;">
	<table>
		<tr>
			<td style="width: 200px;">
				Привязанные к сотруднику 
			</td>
			<td>
				<select id="input_manager_id" onchange="submit_input_forms();">
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')): ?>
					<option value="all">Все</option>
					<?php endif; ?>
					
					<?php foreach($managerListData as $key=>$data): ?>
					<?php if (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') || $key == $model->managerId): ?>
						<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
					<?php endif; ?>
					<?php endforeach; ?>
					
					<!--<option value="-2">Только с менеджерами</option>-->
					<!--<option value="-1">Без менеджеров</option>-->
				</select>
			</td>
		</tr>
	</table>
</div>

<div style="clear: both;">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	#'filter' => $model,
	#'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
	'columns' => array(
		[
			'name' => 'id',
			'header' => 'id',
			'value' => function($data) {
					return $data->id;
				},
			'filter' => false,
			'type' => 'raw'
		],
		[
			'name' => 'company',
			'header' => 'Название компании',
			'value' => function($data) {
					return $data->userMedical->company->name;
				},
			'filter' => false,
			'type' => 'raw'
		],
		[
			'name' => 'address',
			'header' => 'Адрес',
			'value' => function($data) {
					return $data->userMedical->address->name;
				},
			'filter' => false,
			'type' => 'raw'
		],
		[
			'name' => 'subject',
			'header' => 'Тема сообщения',
			'value' => function($data) {
					return $data->subject;
				},
			'filter' => false,
			'type' => 'raw',
    		'htmlOptions'=>array('style' => 'text-align: left!important;',),
		],
		[
			'name' => 'msgbody',
			'header' => 'Текст сообщения',
			'value' => function($data) {
					return $data->msgbody;
				},
			'filter' => false,
			'type' => 'raw',
    		'htmlOptions'=>array('style' => 'text-align: left!important; width: 50%;',),
		],
		[
			'name' => 'createDate',
			'header' => 'Дата создания',
			'value' => function($data) {
					return $data->createDate;
				},
			'filter' => false,
			'type' => 'raw',
    		'htmlOptions'=>array('width'=>'100px'),
		],

	),
));
?>
</div>
<script> var submit_path = '/newAdmin/contactManager/view?'; </script>
<script>
//$(document).ready(function() {
	var data_values = {
		'manager_id': '<?=$model->managerId?>',
	};
	
function update_input_forms() {
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = '';
		if (typeof $('#input_'+key) != 'undefined') {
			if ($('#input_'+key).prop('type') == 'select-one') {
				$('#input_'+key+' option').each(function(){
					if (this.value == data_values[key]) {
						$('#input_'+key).val(data_values[key]);
					}
				});
			} else {
				$('#input_'+key).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		
		if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
			
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != '') {query += '&' + key + '=' + data_values[key];}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

update_input_forms();

$(document).keypress(function(e) {
	
    if(e.which == 13) {submit_input_forms();}
});
//});
</script>