<?php
/* @var $this ClinicListController */
/* @var $model Address */
?>
<style>
.btn, .btn-green, .btn-red, .btn-blue, .nav_cal .btn_nav {background: none!important; background-color: #eee!important; border-radius: 0px!important; text-shadow: none!important; cursor: pointer;}
.btn-green, .btn.active {background-color: #4bc628!important; color: #000!important;}
.btn-red {background-color: #FF5500!important; color: #fff!important;}
.btn-blue {background-color: #45B1C6!important; color: #fff!important;}
</style>

<div style="margin: 00px 0 0 0;">
<h1 style="display: inline;">Контрагенты</h1>
<br><br>
<a href="../clinicList/add" class="button nl bg-green c-white border_radius_5px">Добавить нового</a>
<div style="float: right; margin: -55px 0 5px 0;">
	<table>
		<tr>
			<td>
				Регион
			</td>
			<td>
				<select id="input_region" onchange="submit_input_forms();">
					<option value="all">Все</option>
					<?php foreach(Yii::app()->params['regions'] as $region): ?>
						<option value="<?=$region['subdomain']?>"><?=$region['name']?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 200px;">
				Тип контрагента 
			</td>
			<td>
				<select id="input_company_type" onchange="submit_input_forms();">				
					<option value="">Активные на сайте</option>
					<option value="all">Все</option>
					<option value="inactive">Неактивные + стадо</option>
					<option value="inactive_only">Только неактивные</option>
					<option value="herd_only">Только стадо</option>
					<option value="self_registered">Поданные заявки</option>
					<option value="activePlusInactiveWithContact">Активные + неактивные/стадо с контактами</option>
					<option value="inactiveWithoutContact">Неактивные/стадо без контактов</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 200px;">
				Привязанные к сотруднику 
			</td>
			<td>
				<select id="input_manager_id" onchange="submit_input_forms();">
					<option value="">Все</option>
					
					<?php foreach($managerListData as $key=>$data): ?>
						<option value="<?=$key?>"><?=CHtml::encode($data)?></option>
					<?php endforeach; ?>
					
					<option value="-2">Только с менеджерами</option>
					<option value="-1">Без менеджеров</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Название контрагента
			</td>
			<td>
				<input id="input_company_name" type="text">
			</td>
		</tr>
		<tr>
			<td>
				<a href="<?= $_SERVER["REQUEST_URI"]."&download=1" ?>">Скачать список контрагентов</a>
			</td>
			<td>
				
			</td>
		</tr>
	</table>
</div>

<div style="clear: both;">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataprovider,
	'itemsCssClass' => 'table table-striped table-bordered table-condensed font-small',
	'columns' => array(
		[
			'name' => 'none',
			'header' => 'Название',
			'value' => function($data) {
					
					return '<a href="../ClinicCard/view?id='.$data->id.'">'.$data->name.'</a>';
				},
			'filter' => false,
			'type' => 'raw'
		],
		[
			'name' => 'none',
			'header' => 'Категория',
			'value' => '$data->companySalesInfo->salesCategory',
			'filter' => false,
		],
		[
			'name' => 'none',
			'header' => 'ЛПР', //
			'value' => function($data) {					
					
					$salesInfo = $data->companySalesInfo;
					$result = $salesInfo->lprPosition . ' ' . $salesInfo->lprName . ' <br>' . $salesInfo->lprPhone;					
					if ($salesInfo->email)
						$result .= '<br>' . $salesInfo->email;
					if ($salesInfo->website)
						$result .= ' <a href="http://' . $salesInfo->website . '" target="_blank">' . $salesInfo->website . '</a>';
					return $result;
				},
			'type' => 'raw'
		],
		[
			'name' => 'companySalesInfo.poType',
			'header' => 'ПО',
			'value' => '$data->companySalesInfo->poType',
		],
		[
			'name' => 'companySalesInfo.managerComment',
			'header' => 'Комментарий',
			'value' => '$data->companySalesInfo->managerComment',
		],
		[
			'name' => 'none',
			'header' => 'Менеджер',
			'value' => '$data->managerRelations->user->id ? $data->managerRelations->user->name." ".$data->managerRelations->user->email : "-"',
			'filter' => false,
			'htmlOptions' => [
				'style' => 'text-align:center'
			]
		],
		[
			'name' => 'none',
			'header' => 'История контактов',
			'value' => function($data) {					
				
				if ($data->companyContacts) {
					$result = '<a href="/newAdmin/companyContact/list?company_name=';
					$result .= urlencode($data->name) . '">смотреть ('.count($data->companyContacts).')</a>';
				}
				return $result;
			},
			'filter' => false,
			'type' => 'raw',
			'htmlOptions' => [
				'style' => 'width:120px; text-align:center'
			]
		],
		[
			'name' => 'action',
			'header' => 'Действие',
			'value' => function($data) {
				$result = "удалено";
				if($data->removalFlag == 0) {
					$result = "<button onclick=\"markToRemoval({obj:this, data: {id:this.id, value:this.value}}); return false;\" value='-1' id=\"$data->id\" style='width: 100%;'>Отметить к удалению</button>";
				} elseif($data->removalFlag == -1) {
					$result = "<b>помечена к удалению</b><button onclick=\"markToRemoval({obj:this, data: {id:this.id, value:this.value}}); return false;\" value='0' id=\"$data->id\" style='width: 100%;' class='btn-red'>восстановить?</button>";
				}
				return $result;
			},
			'filter' => false,
			'type' => 'raw',
    		'htmlOptions'=>array('style'=>'width: 135px;'),
		],
	),
));
?>
</div>

<script> var submit_path = '/newAdmin/contrAgents/view?'; </script>
<script>
//$(document).ready(function() {
	var data_values = {
		'manager_id': '<?=Yii::app()->request->getParam('manager_id')?>',
		'company_name': '<?=Yii::app()->request->getParam('company_name')?>',
		'apt_status': '<?=Yii::app()->request->getParam('apt_status')?>',
		'company_type': '<?=Yii::app()->request->getParam('company_type')?>',
		'region': '<?= $dtOptions['region'] ?>',
	};
	
function update_input_forms() {
	
	for(var key in data_values) {
		if (!data_values[key]) data_values[key] = '';
		if (typeof $('#input_'+key) != 'undefined') {
			
			if ($('#input_'+key).prop('type') == 'select-one') {
			
				$('#input_'+key+' option').each(function(){
					
					if (this.value == data_values[key]) {
						
						$('#input_'+key).val(data_values[key]);
					}
				});
			} else {
				$('#input_'+key).val(data_values[key]);
			}			
		}
	}
}

function submit_input_forms() {
	
	var query = '',
		url = '',
		manager_id = $('#input_manager_id').val();
		
	for(var key in data_values) {
		
		if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
			
			data_values[key] = $('#input_'+key).val();
			if (data_values[key] != '') {query += '&' + key + '=' + encodeURIComponent(data_values[key]);}
		}
	}
	url = submit_path + query;
	document.location.href = url;
}

update_input_forms();

$(document).keypress(function(e) {
	
    if(e.which == 13) {submit_input_forms();}
});
//});
</script>

<script>
function markToRemoval(args) {
	var set = {
		url: "/newAdmin/contrAgents/MarkToRemoval",
		data: args.data,
		method:'POST',
		dataType:'json',
		success: function(data, textStatus, XHR) {
			if(data.status == "OK") {
				console.log(args.obj);
				if(args.obj.value == 0) {
					args.obj.parentNode.innerHTML = "<button onclick=\"markToRemoval({obj:this, data: {id:this.id, value:this.value}}); return false;\" value='-1' id=\""+args.obj.id+"\" style='width: 100%;'>Отметить к удалению</button>";
				}
				else {
					args.obj.parentNode.innerHTML = "<b>помечена к удалению</b><button onclick=\"markToRemoval({obj:this, data: {id:this.id, value:this.value}}); return false;\" value='0' id=\""+args.obj.id+"\" style='width: 100%;' class='btn-red'>восстановить?</button>";
				}
				//alert("OK");
			} else {
				this.error(XHR, textStatus, null)
			}
		},
		error: function(XHR, textStatus, errorThrown) {
			//alert("Ошибка");
		}
	};
	$.ajax(set);
}
</script>