<?php
	$appFormSource = (AppForm::isLocalVersion()) ? 'local.emportal.ru' : 'emportal.ru';
?>
<h1 class="smi_link">
	Добавление новой записи на прием <br>(клиника "<?= $company->name ?>")
</h1>
<div class="vmargin20">
	<script>
	(function(o) {
		var r = new XMLHttpRequest();
		r.onreadystatechange = function(data) {
			if (r.readyState == 4 && r.status == 200) {
				document.write(r.responseText);
				console.log('window.empAppFormOptions');
				console.log(window.empAppFormOptions);
			}
		}
		var params = '?id=' + o.company;
		if (o.appType)
			params += '&at=' + o.appType;
		if (o.address) params += '&a=' + o.address;
		r.open('GET', (document.location.protocol == "https:" ? "https:" : "http:") + '//' + o.source + params, false);
		r.send();
	})({
		'source': '<?= $appFormSource ?>/ajax/getAppForm',
		'company': '<?= $company->linkUrl ?>',
		//'address': 'turistskaya-10'
		'appType': '<?= AppointmentToDoctors::APP_TYPE_PHONE_EMPORTAL ?>', //rm
	})
	</script>
</div>