<?php $this->beginContent('/layouts/main'); ?>
<?php $this->renderPartial('/layouts/_header'); ?>
	<div class="container" style="margin-left: auto; margin-right: auto; float: none;">
		<?php echo $content; ?>
		<div class="clearfix"></div><!-- /clearfix -->
	</div><!-- /container-->
<footer class="footer" />
</footer><!-- /footer -->
<?php $this->endContent(); ?>