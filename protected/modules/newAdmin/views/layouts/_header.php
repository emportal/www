<style>
.wrapper {
	width: 980px;
	margin: 0 auto;
	/*background-color: #eee;*/
}
.na_menu {
	width: 100%;
	margin: 30px 0;
	user-select: none;
}
.na_menu a {
	text-decoration: none;
	display: block;
	padding: 7px 0;
	text-align: center;
}
.upper_panel {
	margin-top: 30px;
}
.login_info  {
	float: right;
	display: inline;
}
ul.yiiPager li a {
	color: gray!important;
	border-radius: 0!important;
	border: none!important;
}
ul.yiiPager li a:hover {
	color: #fff!important;
	background-color: green;
}
ul.yiiPager .selected a {
	color: #fff!important;
	border: none!important;
	font-weight: normal!important;
}
.font-small {
	font-size: 12px;
}
.form-table tr td:nth-child(2) {
	border: 0px solid;
	padding-top: 8px;
}
.form-table tr td textarea, .form-table tr td input {
	width: 280px;
}
.form-table tr td textarea {
	height: 60px;
}
h2:not(:first-of-type) {
	margin-top: 40px;
}
.content_wrapper {
	padding: 0 0 60px 0;
}
.table.table-striped tr td:not(:first-child) {
	text-align: center!important;
}
.table.table-striped tr th:not(:first-child) {
	text-align: center!important;
}
.nopadding {
	padding: 0;
}
.valign-middle tr td {
	vertical-align: middle !important;
}
.salesContractSelect {
	width: 200px; margin: 0 4px 0 0; padding: 0;
}
.no-display {
	display: none;
}
.nl, .nl:hover, .nl:visited, .nl:active {
	text-decoration: none !important;
	color: #000;
}
.reminder_highlight>td, .reminder_highlight>div {
	background-color: #F9FF85!important;
}
.btn, input[type="submit"].btn {
	color: #fff !important;
}
select.btn, input.btn {
	color: #000 !important;
}
.bold {
	font-weight: bold;
}
.autowidth {
	width: auto!important;
}
.extaInfoBlock {
	float: right;
	width: 450px;
	min-height: 200px;
	padding: 10px;
	box-shadow: 1px 1px 3px #000;
	/*background-color: #eee;*/
	text-align: left;
}
.manager-comment {
	width: 140px;
	height: 100px;
	font-size: 12px;
	color: #000;
	line-height: 1.4;
	margin: 0 0 4px 0;
}
.h60 {
	height: 60px;
}
.button-payments {
	border: 1px dashed gray;
	border-top: none;
	cursor: default;
}
</style>
<div class="wrapper">
	<header class="header">
		<div class="upper_panel">
			<a href="/" style="float: left;"><img src="<?= $this->assetsImageUrl . '/logo.png' ?>" alt="Единый медицинский портал"></a>
			<div class="your_city" style="margin: 0 0 0 190px;">
				<?php 
					$this->widget('UserRegion', [
						'regions' => Yii::app()->params['regions'], 
						'selectedRegion' => Yii::app()->session['selectedRegion'],
						//'readonly' => 'true'
					]); 
				?>
			</div>
			
			<div class="login_info">
				<div>
				<a href="/user/profile">
					<?=CHtml::encode(Yii::app()->user->name)?>
				</a></div>
				
				<?php 
				$isContain=false;
				foreach (Yii::app()->user->model->rights as $right) {
    			  	if($right->zRightId == RightType::CAN_WORK_AS_SITE_EDITOR) {
    					$isContain=true;
    					
      				}
				}
				if($isContain) :
				?>
				<div>
					<a href="/admin">
					Админка редактора сайта
					</a>
				</div>
					
			<? endif ?>
				
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="data-table data-table-centered na_menu">
			<div>
				<div>
					<a href="/newAdmin/clinicList/view">Клиники</a>
				</div>
				<div>
					<a href="/newAdmin/contrAgents/view?company_type=inactive">Контрагенты</a>
				</div>
				<div>
					<a href="/newAdmin/registryList/view?time_period=<?= date('d.m.Y-d.m.Y') ?>">Регистратура</a>
				</div>
				<div>
					<a href="/newAdmin/bills/view?report_month=<?= date('Y-m', strtotime(date('Y-m-d') .' -1 month')) ?>">Счета</a>
				</div>
				<div>
					<a href="/newAdmin/companyContact/list">История контактов</a>
				</div>
				<div>
					<a href="/newAdmin/contactManager/view">Сообщения от клиник</a>
				</div>
				<div>
					<?php 
						$linkEnding = (Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA')) ? '' : '&manager_id=' . Yii::app()->user->id;
					?>
					<a href="/newAdmin/contrAgents/view?&company_type=self_registered&region=all<?= $linkEnding ?>">
						Заявки от клиник
					</a>
				</div>
			</div>
		</div>
		
	</header>
</div>