<?php
/* @var $this ReviewController */
/* @var $model Review */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'companyId'); ?>
		<?php echo $form->textField($model,'companyId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addressId'); ?>
		<?php echo $form->textField($model,'addressId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'appointmentToDoctorsId'); ?>
		<?php echo $form->textField($model,'appointmentToDoctorsId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'doctorId'); ?>
		<?php echo $form->textField($model,'doctorId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId',array('size'=>36,'maxlength'=>36)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userName'); ?>
		<?php echo $form->textArea($model,'userName',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userPhone'); ?>
		<?php echo $form->textField($model,'userPhone',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reviewText'); ?>
		<?php echo $form->textArea($model,'reviewText',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'answerText'); ?>
		<?php echo $form->textArea($model,'answerText',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createDate'); ?>
		<?php echo $form->textField($model,'createDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'statusId'); ?>
		<?php echo $form->textField($model,'statusId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ratingDoctor'); ?>
		<?php echo $form->textField($model,'ratingDoctor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ratingClinic'); ?>
		<?php echo $form->textField($model,'ratingClinic'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'confirmCode'); ?>
		<?php echo $form->textField($model,'confirmCode',array('size'=>12,'maxlength'=>12)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->