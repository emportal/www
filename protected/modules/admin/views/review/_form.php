<?php
/* @var $this ReviewController */
/* @var $model Review */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'review-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'companyId'); ?>
		<?php echo $form->textField($model,'companyId',array('size'=>36,'maxlength'=>36)); ?>
		<?php echo $form->error($model,'companyId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'addressId'); ?>
		<?php echo $form->textField($model,'addressId',array('size'=>36,'maxlength'=>36)); ?>
		<?php echo $form->error($model,'addressId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'appointmentToDoctorsId'); ?>
		<?php echo $form->textField($model,'appointmentToDoctorsId',array('size'=>36,'maxlength'=>36)); ?>
		<?php echo $form->error($model,'appointmentToDoctorsId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'doctorId'); ?>
		<?php echo $form->textField($model,'doctorId',array('size'=>36,'maxlength'=>36)); ?>
		<?php echo $form->error($model,'doctorId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userId'); ?>
		<?php echo $form->textField($model,'userId',array('size'=>36,'maxlength'=>36)); ?>
		<?php echo $form->error($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userName'); ?>
		<?php echo $form->textArea($model,'userName',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'userName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userPhone'); ?>
		<?php echo $form->textField($model,'userPhone',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'userPhone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reviewText'); ?>
		<?php echo $form->textArea($model,'reviewText',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'reviewText'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'answerText'); ?>
		<?php echo $form->textArea($model,'answerText',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'answerText'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createDate'); ?>
		<?php echo $form->textField($model,'createDate'); ?>
		<?php echo $form->error($model,'createDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'statusId'); ?>
		<?php echo $form->textField($model,'statusId'); ?>
		<?php echo $form->error($model,'statusId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ratingDoctor'); ?>
		<?php echo $form->textField($model,'ratingDoctor'); ?>
		<?php echo $form->error($model,'ratingDoctor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ratingClinic'); ?>
		<?php echo $form->textField($model,'ratingClinic'); ?>
		<?php echo $form->error($model,'ratingClinic'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'confirmCode'); ?>
		<?php echo $form->textField($model,'confirmCode',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'confirmCode'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->