<?php
/* @var $this ReviewController */
/* @var $data Review */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('companyId')); ?>:</b>
	<?php echo CHtml::encode($data->companyId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressId')); ?>:</b>
	<?php echo CHtml::encode($data->addressId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('appointmentToDoctorsId')); ?>:</b>
	<?php echo CHtml::encode($data->appointmentToDoctorsId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('doctorId')); ?>:</b>
	<?php echo CHtml::encode($data->doctorId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userName')); ?>:</b>
	<?php echo CHtml::encode($data->userName); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('userPhone')); ?>:</b>
	<?php echo CHtml::encode($data->userPhone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reviewText')); ?>:</b>
	<?php echo CHtml::encode($data->reviewText); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('answerText')); ?>:</b>
	<?php echo CHtml::encode($data->answerText); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createDate')); ?>:</b>
	<?php echo CHtml::encode($data->createDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('statusId')); ?>:</b>
	<?php echo CHtml::encode($data->statusId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ratingDoctor')); ?>:</b>
	<?php echo CHtml::encode($data->ratingDoctor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ratingClinic')); ?>:</b>
	<?php echo CHtml::encode($data->ratingClinic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('confirmCode')); ?>:</b>
	<?php echo CHtml::encode($data->confirmCode); ?>
	<br />

	*/ ?>

</div>