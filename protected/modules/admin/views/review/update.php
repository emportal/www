<?php
/* @var $this ReviewController */
/* @var $model Review */

$this->breadcrumbs=array(
	'Отзывы'=>array('index'),
	//$model->id=>array('view','id'=>$model->id),
	'Редактирование',
);

?>

<h1>Редактирование отзыва №<?php echo $model->id; ?></h1>

<div style="margin: 20px 0 0 30px;">
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>