<?php
/* @var $this ReviewController */
/* @var $model Review */

$this->breadcrumbs=array(
	'Отзывы'=>array('index'),
	//$model->id=>array('view','id'=>$model->id),
	'Просмотр',
);
?>

<h1>Просмотр отзыва №<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'companyId',
		'addressId',
		'appointmentToDoctorsId',
		'doctorId',
		'userId',
		'userName',
		'userPhone',
		'reviewText',
		'answerText',
		'createDate',
		'statusId',
		'ratingDoctor',
		'ratingClinic',
		'confirmCode',
	),
)); ?>
