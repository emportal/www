<?php
/* @var $this ReviewController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs=array(
	'Reviews',
);

$this->menu=array(
	array('label'=>'Create Review', 'url'=>array('create')),
	array('label'=>'Manage Review', 'url'=>array('admin')),
);
?>
<style>
.table {
	max-width: none;
}
</style>

<h1>Отзывы пациентов</h1>

<?php

/*
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
*/
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	'filter' => $model,
	'id' => 'review-list',
	'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed',
	'columns' => array(
		[
			'name' => 'createDate',
			'header' => 'Дата',
			'value' => '$data->createDate',
			'htmlOptions' => [
				'style'=>'width:80px'
			],
			'filter' => false,
		],
		[
			'name' => 'doctorId',
			'header' => 'Врач',
			'value' => function($data) {
				$absoluteLink = Yii::app()->request->getParam('absoluteLink',false) ? MyTools::url_origin($_SERVER, false) : "";
				$subdomain = $data->doctor->currentPlaceOfWork->address->city->subdomain;
				$domain = City::getRedirectUrl($subdomain, $data->doctor->currentPlaceOfWork->address->samozapis, "");
				$doctorLink = $absoluteLink . (($domain !== MyTools::url_origin($_SERVER, false)) ? $domain : "") . Yii::app()->createUrl('doctor/view', array('linkUrl' => $data->doctor->linkUrl, 'companyLinkUrl' => $data->doctor->currentPlaceOfWork->address->company->linkUrl, 'addressLinkUrl' => $data->doctor->currentPlaceOfWork->address->linkUrl));

				return CHtml::link($data->doctor->name, $doctorLink, array("target"=>"_blank"));
			},
			'type' => 'raw',
			'htmlOptions' => [
				'style'=>'width:120px'
			],
			'filter' => false,
		],
		[
			'name' => 'userName',
			'header' => 'Отправитель',
			'value' => '$data->userName',
			'htmlOptions' => [
				'style'=>'width:120px'
			],
			'filter' => CHtml::textField('Review[userName]', $model->userName, [
				'autofocus' => 'true'
				]
			),
		],
		[
			'name' => 'ratingDoctor',
			'header' => 'Оценка врача',
			'value' => '$data->ratingDoctor',
			'htmlOptions' => [
				'style'=>'width:50px'
			],
			'filter' => false,
		],
		[
			'name' => 'ratingClinic',
			'header' => 'Оценка клиники',
			'value' => '$data->ratingClinic',
			'htmlOptions' => [
				'style'=>'width:50px'
			],
			'filter' => false,
		],
		[
			'name' => 'reviewText',
			'header' => 'Отзыв',
			'value' => '$data->reviewText',
			'filter' => false,
		],
		[
			'name' => 'answerText',
			'header' => 'наличие ответа',
			'value' => '$data->answerText ? "да" : "нет"',
			'htmlOptions' => [
				'style'=>'width:50px'
			],
			'filter' => false,
		],
		[
			'name' => 'statusId',
			'header' => 'Статус',
			'value' =>  '$data->statusId==0 ? "не опубликован (телефон не подтвержден) " : 
						($data->statusId==1 ? "не опубликован (телефон подтвержден)" : 
						($data->statusId==2 ? "опубликован" : 
						($data->statusId==3 ? "отклонен" : "не задан")))',
			'htmlOptions' => [
				'style'=>'width:100px'
			],
			'filter' => CHtml::dropDownList('Review[statusId]', Yii::app()->request->getParam('Review')['statusId'], [''=>'Все',3=>'отклонен',2=>'опубликован',1=>'не опубликован (телефон подтвержден)',0=>'не опубликован (телефон не подтвержден)'])
		],
		[
			'class' => 'CButtonColumn',
			'header' => 'Действие',
			'template' => '{public}{reject}{update}',
			'buttons' => [
				'public' => [
					'url' => 'CHtml::normalizeUrl(array("review/publish","id"=>$data->id,"statusId"=>2))',
					'label' => 'Опубликовать',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-success',
						'style' => 'height:20px; width: 80%;',
					],
					'click'=>"function(){
						$.fn.yiiGridView.update('review-list', {
							type:'POST',
							url:$(this).attr('href'),
							success:function(text,status) {
								$.fn.yiiGridView.update('review-list');
							}
						});
						return false;
					}",
				],
				'reject' => [
					'url' => 'CHtml::normalizeUrl(array("review/publish","id"=>$data->id,"statusId"=>3))',
					'label' => 'Отклонить',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-danger',
						'style' => 'height:20px; width: 80%;',
					],
					'click'=>"function(){
						$.fn.yiiGridView.update('review-list', {
							type:'POST',
							url:$(this).attr('href'),
							success:function(text,status) {
								$.fn.yiiGridView.update('review-list');
							}
						});
						return false;
					}",
				],
				'update' => [
					'url' => 'CHtml::normalizeUrl(array("review/update","id"=>$data->id))',
					'label' => 'Редактировать',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-primary',
						'style' => 'height:20px; width: 80%;',
					]
				],
			],
			'htmlOptions' => [
				'style'=>'width:100px'
			],
		],
	)
));

?>