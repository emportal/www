<?php
/* @var $this ServicesController */

$this->breadcrumbs=array(
	'Services'=>array('/admin/services'),
	'Request',
);

?>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'user-grid',
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'dataProvider'=> $dataProvider,
		'columns'=>array(
				array(					
					'name' => 'address.company.name',
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'address.name',
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'name',
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array(
						#'class' => 'ext.editable.EditableColumn',
						'header'=>'Раздел',
						'name' => 'serviceSection.name',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				),
				array(
						#'class' => 'ext.editable.EditableColumn',
						'name' => 'companyActiviteId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'select',
								'title'=>'Профиль деятельности',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(CompanyActivite::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'description',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'textarea',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'price',
						'headerHtmlOptions' => array('style' => 'width: 50px'),
						'editable' => array(
								'url' => $this->createUrl('RequestAccept'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array
				(
						'class'=>'CButtonColumn',
						'htmlOptions' => array('style' => 'width: 85px'),
						'template'=>'{confirm btn btn-success} {reject btn btn-danger}',
						'buttons'=>array
						(
								'confirm btn btn-success' => array
								(
										'label'=>'<i class="icon-ok icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestAccept", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
								'reject btn btn-danger' => array
								(
										'label'=>'<i class="icon-ban-circle icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestCancel", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
						),
				),

		),
));
?>