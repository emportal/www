<?php
/* @var $this ServicesController */
/* @var $model Service */
/* @var $form CActiveForm */
?>


<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'service-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('class'=>'well'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'name', array('class'=>'span8')); ?>
	<?php echo $form->textFieldRow($model, 'link', array('class'=>'span8', 'size'=>100,'maxlength'=>150)); ?>
	<?php echo $form->textFieldRow($model, 'fullName', array('class'=>'span8', 'size'=>100,'maxlength'=>150)); ?>
	<?php echo $form->dropDownListRow($model,'serviceSectionId', CHtml::listData(ServiceSection::model()->findAll(array('order'=>'name')), 'id', 'name'), array('empty'=>'(выбрать)')); ?>
	<?php echo $form->dropDownListRow($model,'companyActiviteId',CHtml::listData(CompanyActivite::model()->findAll(array('order'=>'name')), 'link', 'name'), array('empty'=>'(выбрать)')); ?>
	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	<?php echo $form->checkBoxRow($model,'forChildren'); ?>
	<?php echo $form->checkBoxRow($model,'forAdult'); ?>
<br />
<a href="index" class="btn btn-danger"><i class="icon-ban-circle icon-white"></i> Отмена</a>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Сохранить', 'htmlOptions'=>array('class'=>'btn-success'))); ?>
<?php $this->endWidget(); ?>