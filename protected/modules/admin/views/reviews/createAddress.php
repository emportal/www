<style>
.radiobuttons_horizontal .rb_wrapper {
    float:left;
	margin-right:5px;
}
.radiobuttons_horizontal {
	line-height:12px;
}
.radiobuttons_horizontal td {
	padding:5px;
}
</style>
<div class="page-header">
	<h2>Добавление отзыва о клинике</h2>
</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'comment-form',
	'enableAjaxValidation' => false,
		));
/* @var $form CActiveForm */
?>

<table class="items table table-striped table-bordered table-condensed">
	<!--<tr>
		<td valign="middle">Ваша оценка</td>
		<td>
<?php
#echo $form->dropDownList($model, 'rating', Comment::$ratingList,
#	array('class' => 'custom-select')); 
?>
		</td>
	</tr>-->
	<tr>
		<td>Выберите клинику</td>
		<td>
			<?=$form->listBox($model, 'addressId', $listData,[
				'size' => '20',
				'style' => 'width:500px'
			]); ?>
		</td>
	</tr>
<?php foreach ($types as $key => $value): ?>
		<tr class="radiobuttons_horizontal">
			<td valign="middle" class="valign-mid"><?= $value ?></td>
			<td valign="middle">
	<?=
	$form->radioButtonList($model, "grades[" . $key . "]", array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5), array(
		'separator' => "",
		'template' => '<div class="rb_wrapper">{label}{input}</div>'
	));
	?><br/>						
			</td>
		</tr>
	<?php endforeach; ?>
	<?php if ($form->error($model, "grades")): ?>
		<tr>
			<td valign="middle" class="valign-mid"></td>
			<td valign="middle" class="valign-mid"><?= $form->error($model, "grades"); ?></td>						
		</tr>
	<?php endif; ?>



	<tr>
		<td valign="middle" class="valign-mid">Рекомендую</td>
		<td valign="middle"><?=
			$form->checkbox($model, 'isRecomend', array(
				'style' => 'margin-top:8px;'
			));
			?></td>
	</tr>
	<tr>
		<td valign="middle" class="valign-mid">Дата</td>
		<td valign="middle"><?php
			$form->widget('zii.widgets.jui.CJuiDatePicker', array(
						'model'			 => $model,
						'attribute'		 => 'date',
						'language'		 => 'ru',
						'htmlOptions'	 => array('class' => 'custom-text','autocomplete' => 'off'),
						'options'		 => array(
							'changeMonth'	 => true,
							'changeYear'	 => true,
							/*'altFormat'		 => "dd.mm.yy",*/
							'dateFormat'	 => "yy-mm-dd",
							'yearRange'		 => '-80:+0',
						),
					));
			?>
		</td>
	</tr>
	<tr>
		<td>Имя</td>
		<td>
			<?php
			echo $form->textField($model, 'name', array('class' => 'custom-text'));
			?>
		</td>
	</tr>
	<tr>
		<td>Текст отзыва</td>
		<td>
			<?php
			echo $form->textArea($model, 'review', array('class' => 'custom-textarea','style'=>'width:500px;height:150px;'));
			?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<button type="submit" class="btn-green" title="Отправить">
				Отправить
			</button>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>