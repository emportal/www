<?php if ( Yii::app()->user->hasFlash('success') ): ?>
<div class="flash-success">
	<span><?php echo Yii::app()->user->getFlash('success')?></span>
</div>

<?php endif;?>

<a href="/admin/reviews/createAddress" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить отзыв о клинике</a>
<br/><br/>
<a href="/admin/reviews/createExpert" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить отзыв о враче</a>
<br/><br/>
<? /**/ ?>
<?= CHtml::link('Отзывы о клинике', array('reviews/index'), array('class'=> 'btn clinic'))?>
&nbsp;
<?= CHtml::link('Отзывы о врачах', array('reviews/experts'), array('class'=> 'btn active doctor'))?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dataProvider,
		'summaryText' => '',
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'columns'=>array(
				array(
					'name'=>'Компания',
					'value'=>'$data->doctor->currentPlaceOfWork->company->name',					
				),
				array(
					'name'=>'Адрес',
					'value'=>'$data->doctor->currentPlaceOfWork->address->name',
				),
				array(
					'name'=>'Доктор',
					'value'=>'$data->doctor->name',
				),
				array(
						'name'=>'Дата',
						'value'=>'Yii::app()->dateFormatter->formatDateTime($data->date, "medium", null);',
						'type'=>'html',

				),
				/*array(
						'name'=>'Текст',
						'value'=>'$data->content',
				),*/
				array(
						'class'=>'CLinkColumn',
						'urlExpression'=>'Yii::app()->controller->createUrl("viewExpert", array("id"=>$data->id))',
						'header'=>'Текст отзыва',
						'labelExpression'=>'$data->review',

				),
				array(
				 		'name'=>'Ответ клиники',
						'value'=>'$data->answer ? "да" : "нет"',
				),
				array(
						'name'=>'Статус',
						'value'=>'$data->status ? "<span style=\"color:#62c462\">опубликован</span>" : "<span style=\"color:#DA4F49\">не опубликован</span>"',
						'type'=>'html'
				),		
				[
					'class' => 'CButtonColumn',
					'header' => 'Действия',
					'template' => '{delete}',
					'buttons' => [
						'delete' => [
							'url' => 'CHtml::normalizeUrl(array("reviews/deleteExpert","id"=>$data->id))',
							'label' => '<i class="icon-remove icon-white"></i> Удалить',			
							'imageUrl' => false,
							'options' => [
								'class'=>'btn btn-danger'
							]
						]
					],
					'htmlOptions' => [
						'style'=>'width:100px'
					]
				]
		),
));
?>