<?php if (Yii::app()->user->hasFlash('success')): ?>
	<div class="flash-success">
		<span><?php echo Yii::app()->user->getFlash('success') ?></span>
	</div>
<?php endif; ?>
<? /**/ ?>

<a href="/admin/reviews/createAddress" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить отзыв о клинике</a>
<br/><br/>
<a href="/admin/reviews/createExpert" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить отзыв о враче</a>
<br/><br/>

<?= CHtml::link('Отзывы о клинике', array('reviews/index'), array('class' => 'btn active clinic')) ?>
&nbsp;
<?= CHtml::link('Отзывы о врачах', array('reviews/experts'), array('class' => 'btn doctor')) ?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	'filter' => $model,
	'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed',
	'columns' => array(
		[
			'name' => 'Компания',
			'value' => '$data->company->name',
			'filter' => CHtml::textField('Company[name]', $model->company->name, [
				'autofocus' => 'true'
				]
			),
		],
		[
			'name' => 'address.name',
			'value' => '$data->address->name',
		],
		[
			'header' => 'Дата публикации',
			'filter' => false,
			'name' => 'date',
			'value' => '$data->date == "0000-00-00 00:00:00" ? "не указано" : Yii::app()->dateFormatter->formatDateTime($data->date, "medium", null);',
			'type' => 'html',
		],
		/* array(
		  'name'=>'Текст',
		  'value'=>'$data->content',
		  ), */
		[
			'class' => 'CLinkColumn',
			'urlExpression' => 'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
			'header' => 'Текст отзыва',
			'labelExpression' => '$data->review',
		],
		[
			'header' => 'Ответ клиники',
			'name' => 'answer',
			'value' => '$data->answer ? "да" : "нет"',
			'filter' => CHtml::dropDownList('RaitingCompanyUsers[answer]', Yii::app()->request->getParam('RaitingCompanyUsers')['answer'], ['','да','нет'])
		],
		[
			'header' => 'Статус',
			'name' => 'status',
			'value' => '$data->status ? "<span style=\"color:#62c462\">опубликован</span>" : "<span style=\"color:#DA4F49\">не опубликован</span>"',
			'type' => 'html',
			'filter' => CHtml::dropDownList('RaitingCompanyUsers[status]', Yii::app()->request->getParam('RaitingCompanyUsers')['status'], [''=>'',1=>'да',0=>'нет'])
		],
		[
			'class' => 'CButtonColumn',
			'header' => 'Действия',
			'template' => '{delete}',
			'buttons' => [
				'delete' => [
					'url' => 'CHtml::normalizeUrl(array("reviews/deleteAddress","id"=>$data->id))',
					'label' => '<i class="icon-remove icon-white"></i> Удалить',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-danger'
					]
				]
			],
			'htmlOptions' => [
				'style'=>'width:100px'
			]
		]
	),
));
?>