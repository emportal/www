<?php /** @var $form CActiveForm   */

$form = $this->beginWidget('CActiveForm',[
	'id' => 'company-form',
	'enableAjaxValidation' => false,	
]);

$cityId = ($model->isNewRecord) ? City::model()->selectedCityId : $model->cityId;
?>
<?php 
 $arr = [];
 foreach($model->metroStations as $m) { $arr[] = $m->name;}
?>
<script>
	var metroCount = <?=count($model->metroStations)?>;
	var metroItems = ['<?= implode("','",$arr) ?>'];
	var modelId = '<?$model->id?>';
	
	function setCommonRegistry() {
		
		/*if (!modelId) return false;
		var CommonRegistryValue = $('#commonRegistrySelect').val();		
		console.log('sv = ' + CommonRegistryValue);
		
		$.ajax({
			url: "http://fiddle.jshell.net/favicon.png",
			beforeSend: function( xhr ) {
				xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
			}
		})*/
	}
	
	function addMetroStation() {
		
		var ind = metroItems.indexOf($("#Address_metroStationList option:selected").html());
		if(ind == -1) {
			
			metroItems.push($("#Address_metroStationList option:selected").html());
			
			var row = '<div class="rowMetro">\n\
<span  style="width:150px;margin:0px 10px;display:inline-block;"class="metroName">\n\
'+$("#Address_metroStationList option:selected").html()+'\n\
</span><input name="metroList['+metroCount+'][value]" type="hidden" value="'+$("#Address_metroStationList").val()+'"/>\n\
дистанция(м) <input style="width:50px" type="text" name="metroList['+metroCount+'][distance]" value=""/>\n\
<span onclick="removeRow(this);" style="margin-bottom:10px;" class="btn btn-danger"><i class="icon-remove icon-white"></i>Удалить</span>\n\
</div>';
	metroCount++;
			$("#metroList").append(row);
		}	
	}
	
	function removeRow(row) {		
		var name = $(row).siblings('.metroName').html().trim();		
		metroItems.splice(metroItems.indexOf(name),1);
		$(row).closest('.rowMetro').remove();
		metroCount--;
	}
	
</script>

<?php if (!$dontShowTitle): ?>
<a href="/admin/company/addresses?id=<?= Yii::app()->request->getParam('ownerId'); ?>" class="btn btn-info">
	<i class="icon-arrow-left icon-white"></i>
	Назад к списку адресов
</a>
<div class="page-header">
	<h2><?= $model->isNewRecord ? 'Добавление' : 'Редактирование' ?> адреса</h2>
</div>
<?php endif; ?>

<table class="items table table-striped table-bordered table-condensed">
	<?php /* ?>
	<tr>
		<td>Единая регистратура <div style="color: darkgray;">определяет, может ли оператор клиники в своей регистратуре miniCRM видеть заявки по всем адресам клиники</div></td>
		<td>
			<select id="commonRegistrySelect">
				<option value="2">Нет</option>
				<option value="1" <? if ($model->userMedicals->commonRegistry == 1) {echo 'selected';} ?>>Да</option>
			</select>
			<span style="margin-bottom: 10px;" onclick="setCommonRegistry(); return false;" class="btn btn-success">
				Установить
			</span>
		</td>
	</tr>
	<?php */ ?>
	<tr>
		<td>link</td>
		<td><?= $model->link;?></td>
	</tr>
	<tr>
		<td class="bold">Город</td>		
		<td style="opacity: 1;">
		<?= $form->dropDownList($model,'cityId',CHtml::listData(City::model()->findAll([
			'select'=>'id, name',
			'order'=>'population DESC',
			'limit'=>150,
			'condition'=>'countryId = \'534bd8ae-e0d4-11e1-89b3-e840f2aca94f\' AND subdomain IS NOT NULL'
		]),'id','name'), 
		[
		//'readonly' => true,
		'disabled' => true,
		]); ?>
		<?= $form->hiddenField($model, 'cityId', ['value' => $cityId]) ?>
		<div style="float: right; max-width: 220px; font-size: 12px;">
			Для выбора города при добавлении адреса используйте переключатель региона (в верхней части экрана).
		</div>
		</td>
	</tr>
	<tr>
		<td class="bold">Район</td>		
		<td>
		<?= $form->dropDownList($model,'cityDistrictId',CHtml::listData(CityDistrict::model()->findAll([
			'select'=>'id, name, cityId',
			//'order'=>'population DESC',
			'limit'=>1000,
			'condition'=>'cityId = "' . $cityId . '"'
		]),'id','name')); ?>		
		</td>
	</tr>
	<tr>
		<td class="bold">Метро</td>
		<td><?= CHtml::dropDownList(CHtml::activeName($model, 'metroStationList'), '', CHtml::listData(MetroStation::model()->findAll([
			'select'=>'id,name',
			'condition' => "cityId = '" . $cityId . "'", //Yii::app()->session['selectedRegion'] //534bd8b8-e0d4-11e1-89b3-e840f2aca94f
			'order'=>'name ASC',	
			]),'id','name')); ?> <span style="margin-bottom:10px;" onclick="addMetroStation();" class="btn btn-success">Добавить метро в список</span>
			<div id="metroList">
				<?php foreach($model->nearestMetroStations as $key=>$ms):?>
					<div class="rowMetro">
						<span style="width:150px;margin:0px 10px;display:inline-block;" class="metroName">
						<?=$ms->metroStation->name?>
						</span>
						<input name="metroList[<?= $key ?>][value]" type="hidden" value="<?=$ms->metroStationId ?>"/>				
						дистанция(м) <input style="width:50px" type="text" name="metroList[<?= $key ?>][distance]" value="<?=$ms->distance ?>"/>
						<span onclick="removeRow(this);" style="margin-bottom:10px;" class="btn btn-danger"><i class="icon-remove icon-white"></i>Удалить</span>
					</div>
				<?php endforeach; ?>
			</div>
		</td>
	</tr>
	<tr>
		<td class="bold">Улица</td>
		<td><?= $form->textField($model,'street');?></td>
	</tr>
	<tr>
		<td class="bold">Номер дома</td>
		<td><?= $form->textField($model,'houseNumber');?></td>
	</tr>			
	<tr>
		<td>Индекс</td>
		<td><?= $form->textField($model,'postIndex');?></td>
	</tr>	
	<tr>
		<td>Широта - <a href="http://api.yandex.ru/maps/tools/getlonglat/" target="_blank">ссылка</a> на получение координат</td>
		<td><?= $form->textField($model,'latitude');?></td>
	</tr>
	<tr>
		<td>Долгота</td>
		<td><?= $form->textField($model,'longitude');?></td>
	</tr>
	<tr>
		<td>Описание(необязательно заполнять если поле такое же как у компании)</td>
		<td><?= $form->textarea($model,'description',[ 'style' => 'width:400px']);?></td>
	</tr>
	<tr>
		<td>Активность</td>
		<td><?= $form->dropDownList($model,'isActive',['Неактивно','Активно']); ?></td>
	</tr>
	<tr>
		<td>seoName</td>
		<td><?= $form->textField($model,'seoName')?></td>
	</tr>
	<tr>
		<td>seoTitle<br>(если заполнено, подставляется в качестве тега title в карточу соотв. адреса)</td>
		<td><?= $form->textField($model,'seoTitle')?></td>
	</tr>
	<tr>
		<td><b>Жирным шрифтом</b> выделены поля, обязательные для заполнения</td>
		<td></td>
	</tr>
	
	<tr>
		<td></td>
		<td>
			<button class="btn btn-success">Сохранить</button>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>