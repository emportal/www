<?php if (Yii::app()->user->hasFlash('success')): ?>
	<div class="flash-success">
		<span><?php echo Yii::app()->user->getFlash('success') ?></span>
	</div>
<?php endif; ?>
<? /**/ ?>

<a href="/admin/company/addCompany" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить клинику</a>
<br/><br/>

<?php
$samozapisValue = Yii::app()->request->getParam('Company')['filterSamozapis'];

$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $model->companyDataProviderForAdminPanel(),
	'filter' => $model,
	#'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed',
	'columns' => array(
		[
			'name' => 'samozapis_search',
			'value' => '$data->address->city->name',
			'header' => '',
			'filter' => CHtml::dropDownList('Company[filterSamozapis]', $samozapisValue, [''=>'Все', 'no-samozapis'=>'Коммерческие', 'samozapis'=>'Самозапись'])
		],
		[
			'name' => 'name',
			'filter' => CHtml::textField('Company[name]', $model->name, [
				'autofocus' => 'true'
				]
			),			
			'filterHtmlOptions' => [
				'style' => 'padding:4px 25px 4px 5px'
			]
		],
		[
			'name' => 'companyContracts.name',
			'value' => '$data->companyContracts->contractNumber." / ".$data->managerRelations->user->name',
			'header' => 'Договор / Менеджер',
			'htmlOptions' => [
				'style' => 'width:200px'
			]
		],
		[
			'name' => 'OneOfferNotAccepted',
			'value' => '$data->OneOfferNotAccepted ? "принята" : "не принята"', 
			'header' => 'Договор-оферта',
			#'sortable' => true,
			'filter' => CHtml::dropDownList('Company[oneOffer]', Yii::app()->request->getParam('Company')['oneOffer'], ['','принята','не принята'])
		],
		[
			'name' => 'LastCreateDate',
			#'value' => '$data->LastCreateDate',
			'filter' => false,
			'header' => 'Последняя дата принятия',
		],
		[
			'name' => 'reviewsCount',
			#'value' => '$data->reviewsCount',
			'filter' => false,
			'header' => 'отзывов',
		],
		[
			'header' => 'Действия',
			'class' => 'CButtonColumn',
			'template' => '{addr} {edit} {contract}',
			'buttons' => [
				'addr' => [
					'url' => 'CHtml::normalizeUrl(array("company/addresses","id"=>$data->id))',
					'label' => '<i class="icon-search icon-white"></i> Адреса',
					'imageUrl' => false,
					'options' => [
						'title' => 'Адреса',
						'class' => 'btn btn-info'
					]
				],
				'contract' => [
					'url' => 'CHtml::normalizeUrl(array("company/contract","id"=>$data->id))',
					'label' => '<i class="icon-file icon-white"></i>',					
					'imageUrl' => false,
					'options' => [
						'title' => 'Добавить контракт',
						'class' => 'btn btn-success'
					],
					'visible' => '!$data->companyContracts->name'
				],
				'edit' => [
					'url' => 'CHtml::normalizeUrl(array("company/editCompany","id"=>$data->id))',
					'label' => '<i class="icon-pencil icon-white"></i>',
					'imageUrl' => false,
					'options' => [
						'title' => 'Редактировать компанию',
						'class' => 'btn btn-primary'
					]
				],
				'delete' => [
					'url' => 'CHtml::normalizeUrl(array("company/deleteCompany","id"=>$data->id))',
					'label' => '<i class="icon-remove icon-white"></i>',
					'imageUrl' => false,
					'options' => [
						'title' => 'Удалить компанию',
						'class' => 'btn btn-danger'
					]
				]
			],
			'htmlOptions' => [
				'style' => 'width:200px'
			]
		]
	),
));
?>