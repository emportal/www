<?php
/** @var CActiveForm $form  */

$form = $this->beginWidget('CActiveForm',[
	'id' => 'company-form',
	'enableAjaxValidation' => false,	
]);
?>
<?php if (!$dontShowTitle): ?>
<a href="/admin/company/" class="btn btn-info"><i class="icon-arrow-left icon-white"></i> Назад к списку клиник</a>
<div class="page-header">
	<h2><?= $model->isNewRecord ? 'Добавление' : 'Редактирование' ?> компании</h2>
</div>
<?php endif; ?>
<table class="items table table-striped table-bordered table-condensed">
	<tr>
		<td class="bold">Название</td>
		<td><?= $form->textField($model,'name');?></td>
	</tr>
	<tr>
		<td>Название (расширенное)</td>
		<td><?= $form->textField($model,'nameRUS');?></td>
	</tr>
	<tr>
		<td>Описание</td>
		<td><?= $form->textarea($model,'description');?></td>
	</tr>	
	<tr>
		<td>Категория</td>
		<td><?= $form->dropDownList($model,'categoryId',CHtml::listData(Category::model()->findAll(['condition'=>'id="a3969945e0f59fa84e0e7740c2e8cc87"', 'order'=>'name ASC']),'id','name')); ?></td>
	</tr>
	<tr>
		<?php
		$condition = [
			'эстетическая',
			'центр',
			'клиника',
			'стомат',
			'медицинский центр',
			'центр урологии',
		];
		$conditionStr = 'name LIKE "%' . implode('%" OR name LIKE "%', $condition) . '%"';
		?>
		<td class="bold">Тип компании (медучреждения)</td>
		<td><?= $form->dropDownList($model,'companyTypeId',CHtml::listData(CompanyType::model()->findAll(['condition'=>$conditionStr,'order'=>'name ASC']),'id','name')); ?></td>
	</tr>
	<!--<tr>
		<td>Есть платные услуги</td>
		<td></td>
	</tr>
	<tr>
		<td>Есть бесплатные услуги</td>
		<td></td>
	</tr>
	<tr>
		<td>ОМС</td>
		<td></td>
	</tr>
	<tr>
		<td>ДМС</td>
		<td></td>
	</tr>
	<tr>
		<td>Выезд врача на дом</td>
		<td></td>
	</tr>	-->
	<tr>
		<td>Дата открытия</td>
		<td><?= $form->textField($model,'dateOpening'); ?></td>
	</tr>
	<tr>
		<td><b>Жирным шрифтом</b> выделены поля, обязательные для заполнения</td>
		<td></td>
	</tr>
	<?php if(!$model->isNewRecord): ?>
	<tr>
		<td>Основной адрес</td>
		<td><?= $form->dropDownList($model,'addressId',CHtml::listData(
				Address::model()->findAll([
					'condition' => 'ownerId = :id',
					'params' => [
						':id' => $id
					],
					'order'=>'name ASC'
				]),'id','name'),['style'=>'width:400px']); ?></td>
	</tr>
	<?php endif; ?>
	<tr>
		<td></td>
		<td>
			<button class="btn btn-success">Сохранить</button>
		</td>
	</tr>
</table>

<?php $this->endWidget(); ?>