<?php /** @var $form CActiveForm   */

$form = $this->beginWidget('CActiveForm',[
	'id' => 'company-form',
	'enableAjaxValidation' => false,	
]);
?>

<!-- <a href="/admin/company/" class="btn btn-info"><i class="icon-arrow-left icon-white"></i> Назад к списку клиник</a> -->
<a href="/admin/company/addresses?id=<?= $companyId ?>" class="btn btn-info"><i class="icon-arrow-left icon-white"></i> Назад к списку адресов</a>
<div class="page-header">
	<h2><?= $model->isNewRecord ? 'Добавление' : 'Редактирование' ?> адреса</h2>
</div>
<script>
	function makePassword() {
		$.ajax({
			url : location.href + "&makepass=1",
			dataType: 'JSON',
			type: 'GET',
			success: function(response) {
				$('.password').html(response.pass);
			}
		})  
	}
</script>
<div class="alert alert-warning alert-block">
	<p>Пароль не предоставляется в открытом в виде, если забыт - нужно сгенерировать заново</p>
</div>
<table class="items table table-striped table-bordered table-condensed">
	<tr>
		<td>Логин</td>
		<td><?= $model->name;?></td>
	</tr>
	<tr>
		<td>Пароль</td>
		<td class="password"> ****** <?= CHtml::link('создать новый пароль','#',['onclick' => 'makePassword();return false;']); ?></td>
	</tr>
	<tr>
		<td>Электронная почта</td>
		<td><?= $form->textField($model,'email');?></td>
	</tr>		
	
	<!--<tr>
		<td>Есть платные услуги</td>
		<td></td>
	</tr>
	<tr>
		<td>Есть бесплатные услуги</td>
		<td></td>
	</tr>
	<tr>
		<td>ОМС</td>
		<td></td>
	</tr>
	<tr>
		<td>ДМС</td>
		<td></td>
	</tr>
	<tr>
		<td>Выезд врача на дом</td>
		<td></td>
	</tr>	-->
	
	
	
	<tr>
		<td></td>
		<td>
			<button class="btn btn-success">Сохранить</button>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>

<?php $this->beginWidget('CActiveForm',[ ]); ?>
	<input type="hidden" name="action" value="reset-agreement" />
	<button class="btn btn-success" onclick="if (confirm('Вы уверены?')) { return true; } else { return false; }">Сбросить факт принятия оферты</button>
<?php $this->endWidget(); ?>