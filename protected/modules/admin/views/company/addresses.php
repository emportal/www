<?php if (Yii::app()->user->hasFlash('success')): ?>
	<div class="flash-success">
		<span><?php echo Yii::app()->user->getFlash('success') ?></span>
	</div>
<?php endif; ?>
<? /**/ ?>

<a href="/admin/company/" class="btn btn-info"><i class="icon-arrow-left icon-white"></i> Назад к списку клиник</a>
<br/><br/>
<a href="/admin/company/addAddress?id=<?=$id?>" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить адрес</a>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	#'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed',
	'columns' => array(
		[
			'name' => 'name'	
		],	
		[
			'header' => 'Город',
			'value' => '$data->city->name'
		],
		[
			'header' => 'Статус',
			'value' => '$data->isActive ? "Активно" : "Неактивно"',
		],
		[
			'header' => 'Договор-оферта',
			'value' => '$data->userMedicals->agreementNew == 1 ? "Принята" : "Не принята"'
		],
		[
			'header' => 'Дата принятия',
			'value' => '$data->userMedicals->createDate',
			'htmlOptions' => [
				'style'=> 'width:140px'
			]
		],
		[
			'header' => 'Действия',
			'class' => 'CButtonColumn',
			'template' => '{account} {accountCreate} {edit}',
			'buttons' => [				
				'edit' => [
					'url' => 'CHtml::normalizeUrl(array("company/editAddress","id"=>$data->id,"ownerId"=>$data->ownerId))',
					'label' => '<i class="icon-pencil icon-white"></i>',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-primary'
					]
				],
				'delete' => [
					'url' => 'CHtml::normalizeUrl(array("company/deleteAddress","id"=>$data->id))',
					'label' => '<i class="icon-remove icon-white"></i>',			
					'imageUrl' => false,
					'options' => [
						'class' => 'btn btn-danger',
                        /* 'data-message' => 'Сделать данный адрес неактивным?', */
					]
				],
				'account' => [
					'url' => 'CHtml::normalizeUrl(array("company/account","id"=>$data->id))',
					'label' => '<i class="icon-home icon-white"></i> Личный кабинет',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-info'
					],
					'visible' => '$data->userMedicals ? true : false'
				],
				'accountCreate' => [
					'url' => 'CHtml::normalizeUrl(array("company/account","id"=>$data->id))',
					'label' => '<i class="icon-home icon-white"></i> Создать аккаунт',			
					'imageUrl' => false,
					'options' => [
						'class'=>'btn btn-success'
					],
					'visible' => '$data->userMedicals ? false : true'
				]
			],
			'htmlOptions' => [
				'style'=> 'width:250px'
			]
		]
	),
));
?>