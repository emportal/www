<?php /** @var $form CActiveForm   */

$form = $this->beginWidget('CActiveForm',[
	'id' => 'contract-form',
	'enableAjaxValidation' => false,	
]);
?>


<a href="/admin/company/" class="btn btn-info"><i class="icon-arrow-left icon-white"></i>Назад к списку клиник</a>
<div class="page-header">
	<h2>Добавление контракта </h2>
</div>

<table class="items table table-striped table-bordered table-condensed">	
	<tr>
		<td>Название Юр.лица</td>
		<td><?= $form->textField($model,'name');?></td>
	</tr>
	<tr>
		<td>Номер контракта</td>
		<td><?= $form->textField($model,'contractNumber');?></td>
	</tr>
	<tr>
		<td>Срок действия</td>
		<td>
			 <?php 
					$form->widget('zii.widgets.jui.CJuiDatePicker', array(
						'model'			 => $model,
						'attribute'		 => 'periodOfValidity',
						'language'		 => 'ru',
						'htmlOptions'	 => array('class' => 'custom-text','autocomplete' => 'off'),
						'options'		 => array(
							'changeMonth'	 => true,
							'changeYear'	 => true,
							/*'altFormat'		 => "dd.mm.yy",*/
							'dateFormat'	 => "yy-mm-dd",
							'yearRange'		 => '-1:+5',
						),
					));
				 ?>
		</td>
		
	</tr>
	<tr>
		<td></td>
		<td>
			<button class="btn btn-success">Сохранить</button>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>