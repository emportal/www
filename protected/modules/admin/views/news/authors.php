
<h1>Управление авторами</h1>
<a href="/admin/news/createAuthor" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить</a><br/><br/>


<table class="items table table-striped table-bordered table-condensed">
	<thead>
	 	<tr>
	 		<td>Имя</td>
	 		<td>Информация</td>
	 		<td style="width: 60px">Действия</td>
	 	</tr>
 	</thead>
 	<?php foreach ($users as $user): ?>
	  	<tr>
	 		<td><?=$user->name;?></td>
	 		<td><?=$user->info;?></td>
	 		<td><a class="update" title="Редактировать" rel="tooltip" href="/admin/news/updateAuthor?id=<?=$user->id;?>"><i class="icon-pencil"></i></a> <a class="delete" title="Удалить" rel="tooltip" href="/admin/news/deleteAuthor?id=<?=$user->id;?>"><i class="icon-trash"></i></a></td>
	 	</tr>
 	<?php endforeach; ?>
</table>