<?php
/* @var $this NewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'News',
);

$this->menu=array(
	array('label'=>'Create News', 'url'=>array('news/create')),
	array('label'=>'Manage News', 'url'=>array('admin')),
);
?>

<h1>Управление новостями</h1>
<a href="/admin/news/create" class="btn btn-info"><i class="icon-plus icon-white"></i> Добавить</a><br/><br/>

<?= CHtml::link('Все новости',array('index'),array('class'=>'btn btn-small')); ?> 
<?= CHtml::link('Новости клиник',array('index','filter'=>'clinic'),array('class'=>'btn btn-small')); ?>
<?= CHtml::link('Авторы',array('authors'),array('class'=>'btn btn-small', 'style' => 'margin-left:15px;')); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
    'template'=>"{items}{pager}",
	'filter' => $dataProvider->model,
    'columns'=>array(
		$filter == 'clinic' ? array(			
			'header'=>'Название клиники',
			'type'=>'raw',
			'value'=>'$data->clinic->company->name',
			'htmlOptions'=>array(
				'style'=>'width:150px;'
			)
		):array(			
			'value'=>'0',
			'visible'=>false
		),
        array(
			'name'=>'name',
			'header'=>'Название',
			'htmlOptions'=>array(
				'style'=>'width:680px;'
			)
		),
        //array('name'=>'datePublications'),
		array('name'=>'datePublications', 'filter'=>false),
        array('name'=>'Date', 'filter'=>false),
        array(
			'name'	=>	'publish',
			'value'	=>	'$data->publish ? "да":"нет"',
        	'filter' => false
		),
        array(
			'header'=>'Действия',
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'htmlOptions'=>array('style'=>'width: 60px'),
        ),
    ),
)); ?>