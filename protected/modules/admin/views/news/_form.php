<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>
<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'news-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('class'=>'well', 'enctype' => 'multipart/form-data')
)); ?>

<?php echo $form->errorSummary($model); ?>


<?php echo $form->checkboxRow($model, 'publish'); ?>
<?php echo $form->textFieldRow($model, 'datePublications', array('id'=>'timeblockNews')); ?>

<script>
	$(document).ready(function() {
		$('#timeblockNews').datetimepicker({
			lang: 'ru',				
			format: 'Y-m-d H:i'
		});
	});
</script>

<?php //echo $form->textFieldRow($model, 'Date', array('class'=>'span2')); ?>
<?php echo $form->textFieldRow($model, 'name', array('class'=>'span8')); ?>
<?php echo $form->textFieldRow($model, 'title', array('class'=>'span8')); ?>
<?php echo $form->textFieldRow($model, 'tagDescription', array('class'=>'span8')); ?>
<?php echo $form->dropDownListRow($model,'typeNewsId',$model->typeNews); ?>
<?php echo $form->textAreaRow($model,'preview',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
<div style="width:775px; margin: 0 0 10px 0;">
	<?php $this->widget('ImperaviRedactorWidget', array(
    // You can either use it for model attribute
    'model' => $model,
    'attribute' => 'Text',
    // Some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'lang' => 'ru',
        'toolbar' => true,
        'iframe' => true,
		'minHeight'=> 300 ,
		'maxWidth' => 756,
        'css' => 'wym.css',
        'imageGetJson' => Yii::app()->createAbsoluteUrl('/admin/news/imageGetJson'),
        'imageUpload' => Yii::app()->createAbsoluteUrl('/admin/news/imageUpload'),
        'clipboardUploadUrl' => Yii::app()->createAbsoluteUrl('/admin/news/imageUpload'),
        'fileUpload' => Yii::app()->createAbsoluteUrl('/admin/news/fileUpload')
    ),
)); ?>
Превью-картинка: <?php echo $form->fileField($model, 'mainImage'); ?>

<?php echo $form->dropDownListRow($model,'authorId',$model->getAutorList($users)); ?>
<?php echo $form->textFieldRow($model, 'doctorListTitle', array('class'=>'span8')); ?>
<?php echo $form->textFieldRow($model, 'doctorListQuery', array('class'=>'span8')); ?>
<?php echo $form->textFieldRow($model, 'clinicListTitle', array('class'=>'span8')); ?>
<?php echo $form->textFieldRow($model, 'clinicListQuery', array('class'=>'span8')); ?>
<?php echo $form->textFieldRow($model, 'shareButtonsTitle', array('class'=>'span8')); ?>
<?php echo $form->textFieldRow($model, 'allTagsString', array('class'=>'span8')); ?>

<?php #echo $form->textAreaRow($model,'Text',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
<br />
<a href="index" class="btn btn-danger"><i class="icon-ban-circle icon-white"></i> Отмена</a>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Сохранить и просмотреть', 'htmlOptions'=>array(
	'class'=>'btn-success',
	'style'=>'margin-right:5px;',
	'name'=>'submit_view'
	))); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Сохранить и вернуться', 'htmlOptions'=>array(
	'class'=>'btn-success',
	'name'=>'submit_back'
	))); ?>
<?php $this->endWidget(); ?>
