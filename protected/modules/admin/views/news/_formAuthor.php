<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>
<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'news-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('class'=>'well', 'enctype' => 'multipart/form-data')
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'name', array('class'=>'span8')); ?>
<?php echo $form->textAreaRow($model,'info',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
<br>Превью-картинка: <?php echo $form->fileField($model, 'foto'); ?>

<br />
<a href="authors" class="btn btn-danger"><i class="icon-ban-circle icon-white"></i> Отмена</a>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Сохранить', 'htmlOptions'=>array(
	'class'=>'btn-success',
	'style'=>'margin-right:5px;',
	'name'=>'submit_view'
	))); ?>
<?php $this->endWidget(); ?>
