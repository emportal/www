<?php


/* Yii::app()->clientScript->registerScript('login-page', '
  $("html, body").animate({
  scrollTop: $("div.search-result").offset().top()
  }, 100);
  ', CClientScript::POS_READY)
 */
?>

<div class="content">
	<div class="cont">
		<?php if (empty($name)): ?>	
			<a style="margin-bottom:10px;"class="btn btn-primary" href="/admin/">На главную</a><br/>
			<a style="margin-bottom:10px;" class="btn btn-success" href="/admin/default/addAlbum">Добавить альбом</a><br/>
			<?php if (empty($albums)): ?>
				Вы еще не загрузили ни одного альбома.
			<?php else: ?>
				<div class="albums">
					
					<?php foreach (empty($albums) ? array() : $albums as $row): ?>
					<?php 
					
					$img = $row->galleryBehavior->getGalleryPhotos('gallery',1000) ? CHtml::image($row->galleryBehavior->getGalleryPhotos("gallery",1000)[0]->getPreview()) : "Нет фото";
					
					
					?>
						<div class="album">
							<?= CHtml::Link($img, $this->createUrl("gallery", ['name' => $row->name])); ?>
							<br/>
							<?= $row->title ?>
							<br/>
							<?= Chtml::link("Удалить",$this->createUrl('delAlbum',['name'=>$row->name]),['class'=>'btn btn-danger'])?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>				
		<?php else: ?>
			<a class="btn btn-primary" href="/admin/default/gallery/">Назад</a>
			<h3>Альбом: <?= $album->name ?></h3>
			<div class="photos">
				<?php
				$this->widget('gallery.GalleryManager', array(
					'gallery' => $album->galleryBehavior->getGallery('gallery', 1000),
					'_model' => $album,
					'controllerRoute' => '/gallery/gallery', //route to gallery controller
						//'maxcount' => 101,
						//'prefix'=>'gallery'
				));

				/*
				  $photos = $album->galleryBehavior->getGalleryPhotos('gallery');

				  if (!empty($photos) && count($photos)) {
				  foreach ($photos as $photo) {
				  echo CHtml::link(CHtml::image($photo->getPreview()), $photo->getUrl(), array('class' => 'fancybox-thumb', "rel" => "fancybox-thumb", "title" => $photo->name));
				  }
				  } */
				?>
			</div>
			<?php endif; ?>
	</div>
</div><!-- form -->

<style>	
	.photos {
		border: 1px solid #CCCCCC;
		border-radius: 6px;
		min-height: 500px;
	}
	.albums {
		border: 1px solid #CCCCCC;
		border-radius: 6px;
		min-height: 500px;
		padding: 10px;
		text-align:justify;
	}

	.albums .album {
		display: inline-block;
		max-width: 250px;
		padding: 5px 25px;
		vertical-align: top;
	}
	.albums .album:hover {
		outline:1px solid #ccc;
	}
	
</style>