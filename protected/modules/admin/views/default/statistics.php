<style>
<!--
table.admin-default-statistics {
	border: 1px solid black;
}
.admin-default-statistics td,
.admin-default-statistics th {
	border: 1px solid black;
	padding: 3px;
}
-->
</style>
<h2>Статистика количества записей по специальностям за месяц <?= Yii::app()->db->createCommand("SELECT DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 MONTH),'%b %Y')")->queryScalar() ?></h2>
<table class="admin-default-statistics">
	<thead>
		<tr>
			<th>Название</th>
			<th>Количество</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$total = 0;
		foreach ($specialties as $specialty):
		$total += intval($specialty["count"]);
		?>
		<tr>
			<td><?=$specialty["name"]?></td>
			<td><?=$specialty["count"]?></td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<td>Итого</td>
			<td><?=$total?></td>
		</tr>
	</tbody>
</table>