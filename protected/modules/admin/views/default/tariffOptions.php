<style>
table.tariffOptions {
    border-collapse: collapse;
    border-spacing: 0;
}
table.tariffOptions td,
table.tariffOptions th {
    border: 1px solid black;
    text-align: center;
    vertical-align: middle;
    width: 120px;
    height: 40px;
}
table.tariffOptions th {
    vertical-align: top;
}
table.tariffOptions td:first-child {
    text-align: left;
    padding: 0px 10px 0px 10px;
}
</style>
<table class="tariffOptions">
  <tr>
    <th style="width: 250px;"></th>
  <?php  foreach ($salesContractTypes as $salesContractType): if($salesContractType->id==0){continue;} ?>
    <th><?= $salesContractType->name ?><br><?= $salesContractType->description ?></th>
  <?php endforeach; ?>
  </tr>
<?php foreach ($tariffOptionTypes as $tariffOptionType): ?>
  <tr data-id="<?= $tariffOptionType->id ?>">
    <td><?= $tariffOptionType->name ?></td>
  <?php  foreach ($salesContractTypes as $salesContractType): if($salesContractType->id==0){continue;} ?>
	<td data-id="<?= $salesContractType->id ?>">
		<input type="checkbox" <?php if($salesContractType->hasTariffOption($tariffOptionType->id)): ?>checked<?php endif; ?> >
	</td>
  <?php endforeach; ?>
  </tr>
<?php endforeach; ?>
</table>
<script>
$(function(){
	$("table.tariffOptions input[type='checkbox']").live("change",function(){
		var input = $(this);
		var tariffId = $(this).closest('tr').data('id');
		var salesContractId = $(this).closest('td').data('id');
		var status = $(this).is(':checked');
		console.log(tariffId + ':' + salesContractId + '=' + status);
		$.ajax({
            url: '',
            async: false,
            data: {
            	'tariffOptionTypeId': tariffId,
            	'salesContractTypeId': salesContractId,
            	'status': status ? 1 : 0,
            },
            dataType: 'json',
            type: 'POST',
			beforeSend: function() {
			},
            success: function(response) {
                if(response.success) {
                    console.log("Успех");
                } else {
                    this.error(null,JSON.stringify(response),null);
                }
			},
            error: function(XHR,errorText,errorThrown) {
            	input.prop("checked",!status);
                console.log(errorText);
			},
            complete: function() {
			},
		});
	});
});
</script>