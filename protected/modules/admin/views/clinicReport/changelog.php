<?php
/* @var $this ClinicReportController */

$this->breadcrumbs=array(
	'Модерация ЛКК',
);
?>

<h1>Журнал изменений в ЛКК</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
    'template'=>"{items}{pager}",
    'columns'=>array(
		array(
			'header'=>'Дата',
			'name'=> 'datePublications',
			'type'=>'raw',
			'htmlOptions' => [ 'style'=>'width:80px', ],
		),
        array(			
			'header'=>'Клиника',
			'value'=>'"<b>" . $data->company->name . "</b>" . "<br>" . $data->address->city->name . ", " . $data->address->street . ", " . $data->address->houseNumber',
			'type'=>'raw',
			'htmlOptions' => [ 'style'=>'width:120px', ],
		),
        array(			
			'header'=>'Действие',
			'value'=>'$data->action',
			'type'=>'raw',
		),
        array(			
			'header'=>'Таблица',
			'value'=>'$data->table',
			'type'=>'raw',
		),
        array(			
			'header'=>'Изменения',
			'value'=>'"<pre style=\"height: 150px; overflow: auto;\">" . trim(str_replace("stdClass Object","",print_r(json_decode($data->changes),true))) . "</pre>"',
			'type'=>'raw',
		),
    ),
)); ?>


<?php
/*
$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'user-grid',
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'dataProvider'=> null,
		//'filter'=>$model,	
		//'enableSorting'=>true, 
		'columns'=>array(
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'company.name',
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 't.name',
						'value' => '$data->name',*/
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						/*'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)*/
		/*		),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'serviceSectionId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'companyActiviteId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(CompanyActivite::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
			/*	array(
						'class' => 'ext.editable.EditableColumn',						
						'name' => 't.status',
						'type'=>'raw',	
						'value'=> '$data->status',*/
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'textarea',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				/*),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'price',
						'headerHtmlOptions' => array('style' => 'width: 50px'),
						'editable' => array(
								'url' => $this->createUrl('RequestAccept'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array
				(
						'class'=>'CButtonColumn',
						'htmlOptions' => array('style' => 'width: 85px'),*/
						/*'template'=>'{confirm btn btn-success} {reject btn btn-danger}',
						'buttons'=>array
						(
								'confirm btn btn-success' => array
								(
										'label'=>'<i class="icon-ok icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestAccept", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
								'reject btn btn-danger' => array
								(
										'label'=>'<i class="icon-ban-circle icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestCancel", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
						),*/
			/*	),

		),
));*/
?>