<?php
/* @var $this ClinicReportController */

$this->breadcrumbs=array(
	'Модерация ЛКК',
);

Yii::app()->clientScript->registerScript('button_actions','
	$(".button_view").click(function() {
            var id = $(this).attr("data-id");
            location.href="/admin/clinicReport/view?id="+id
        });
	')
?>

<script>
	$(document).ready(function() {
		$(".unblockUser").click(function() {
			var id = $(this).attr('data-id');		
			var obj = $(this);
			$.ajax({
				url: "/admin/ajax/unblockUser", 
				async: false,
				data: ({
					'id':id,                        
				}),                        
				dataType: 'json',
				type: "GET",
				success: function(response){					
					if(response.success) {						
						obj.closest("tr").fadeOut(500);
						var counter = $("#countBlockedUsers");
						var count = parseInt(counter.html());
						if(count == 1) {
							counter.fadeOut(500);
						} else {
							count--;
							counter.html(count);
						}
					}                        
				},
            });
		});
	});	
</script>
<h1>Заблокированные пользователи</h1>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
    'template'=>"{items}{pager}",
    'columns'=>array(
        array(			
			'header'=>'Данные пользователя',
			'value'=>'"Имя: $data->firstName $data->fatherName<br>Телефон: $data->telefon"',
			'type'=>'raw',
		),
        //array('name'=>'datePublications'),
		array(
			
			'header'=>'Восстановление доступа',
			'type'=>'raw',
			'value'=>'"<span data-id=\"$data->id\" class=\"btn btn-success unblockUser\"> Подтвердить</span>"',
		
		),
          
    ),
)); ?>

<?php
/*
$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'user-grid',
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'dataProvider'=> null,
		//'filter'=>$model,	
		//'enableSorting'=>true, 
		'columns'=>array(
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'company.name',
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 't.name',
						'value' => '$data->name',*/
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						/*'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)*/
		/*		),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'serviceSectionId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'companyActiviteId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(CompanyActivite::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
			/*	array(
						'class' => 'ext.editable.EditableColumn',						
						'name' => 't.status',
						'type'=>'raw',	
						'value'=> '$data->status',*/
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'textarea',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				/*),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'price',
						'headerHtmlOptions' => array('style' => 'width: 50px'),
						'editable' => array(
								'url' => $this->createUrl('RequestAccept'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array
				(
						'class'=>'CButtonColumn',
						'htmlOptions' => array('style' => 'width: 85px'),*/
						/*'template'=>'{confirm btn btn-success} {reject btn btn-danger}',
						'buttons'=>array
						(
								'confirm btn btn-success' => array
								(
										'label'=>'<i class="icon-ok icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestAccept", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
								'reject btn btn-danger' => array
								(
										'label'=>'<i class="icon-ban-circle icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestCancel", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
						),*/
			/*	),

		),
));*/
?>