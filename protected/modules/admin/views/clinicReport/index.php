<?php
/* @var $this ClinicReportController */

$this->breadcrumbs=array(
	'Модерация ЛКК',
);

Yii::app()->clientScript->registerScript('button_actions','
	$(".button_view").click(function() {
            var id = $(this).attr("data-id");
            location.href="/admin/clinicReport/view?id="+id
        });
	')
?>
<style>
	.button-block img {
		cursor:pointer;
	}
</style>
<h1>Модерация ЛКК</h1>

<button onclick="location.href=String(location.pathname+'?published=1')" class="btn btn-success" class="btn-green">Подтвержденные</button>
<button onclick="location.href=String(location.pathname+'?published=2')" class="btn btn-danger">Неподтвержденные</button>
<button onclick="location.href=String(location.pathname+'?published=3')" class="btn btn-info">Все</button>
<div style="float:right; text-align:right;">
	<a href="/admin/clinicReport/changelog">Журнал изменений</a>
	<br>
	<a href="/admin/clinicReport/doubleOfDoctor">Смотреть повторяющихся врачей</a>
</div>
<div style="margin-top:20px;">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<th style="width:240px">Название компании</th>
			<th>Адрес</th>
			<th>Статус</th>
			<th>Действия</th>
		</thead>
		<tbody>
			<?php foreach($data as $row):			
			$next=false;
			switch($published) {
				case 1: 					
					if($row->status)
						$next=true;
					break;
				case 2: 
					if(!$row->status)
						$next=true;
					break;				
			}	
			if($next) continue;
				?>
			<tr>
				<td><?=$row->company->name?></td>
				<td><?=$row->name?></td>
				<td><?=($row->status ?'<span style="color:#DA4F49">есть изменения</span>':'<span style="color:#5BB75B">нет изменений</span>');?></td>
				<td class="button-block"><img data-id="<?=$row->link?>" class="button_view" src="/shared/images/button_view.png"/></td>
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>
</div>
<?php
/*
$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'user-grid',
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'dataProvider'=> null,
		//'filter'=>$model,	
		//'enableSorting'=>true, 
		'columns'=>array(
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'company.name',
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 't.name',
						'value' => '$data->name',*/
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						/*'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)*/
		/*		),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'serviceSectionId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'companyActiviteId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(CompanyActivite::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
			/*	array(
						'class' => 'ext.editable.EditableColumn',						
						'name' => 't.status',
						'type'=>'raw',	
						'value'=> '$data->status',*/
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'textarea',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				/*),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'price',
						'headerHtmlOptions' => array('style' => 'width: 50px'),
						'editable' => array(
								'url' => $this->createUrl('RequestAccept'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array
				(
						'class'=>'CButtonColumn',
						'htmlOptions' => array('style' => 'width: 85px'),*/
						/*'template'=>'{confirm btn btn-success} {reject btn btn-danger}',
						'buttons'=>array
						(
								'confirm btn btn-success' => array
								(
										'label'=>'<i class="icon-ok icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestAccept", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
								'reject btn btn-danger' => array
								(
										'label'=>'<i class="icon-ban-circle icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestCancel", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
						),*/
			/*	),

		),
));*/
?>