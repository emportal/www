<?php
/* @var $this ClinicReportController */
/* @var $model Address */
$this->breadcrumbs = array(
	'Модерация ЛКК',
);
Yii::app()->clientScript->registerPackage('jquery.maskedinput');
?>
<script type="text/javascript">
	$(document).ready(function() {
		$("div[id^=date]").hide();
		$(".button_view").click(function() {
			var id = $(this).attr("data-id");
			location.href = "/admin/clinicReport/view?id=" + id
		});

		$(".accept_btn").click(function() {
			var btn = $(this);
			var id = btn.attr("data-id");
			var addressId = btn.attr("data-addressId");
			var action = btn.attr("data-action");
			if (confirm("Вы уверены, что хотите подтвердить запись?")) {
				$.ajax({
					url: "/admin/ajax/accept" + action,
					type: "POST",
					data: {
						id: id,
						addressId: addressId ? addressId : ""
					},
					success: function(html) {
						btn.closest("tr").fadeOut("slow");
					},
				});
			}
		});
		$(".decline_btn").click(function() {
			var btn = $(this);
			var id = btn.attr("data-id");
			var action = btn.attr("data-action");
			if (confirm("Вы уверены, что хотите подтвердить запись?")) {
				$.ajax({
					url: "/admin/ajax/decline" + action,
					type: "POST",
					data: {
						id: id
					},
					success: function(html) {
						btn.closest("tr").fadeOut("slow");
					},
				});
			}
		});

		$(".ajax_update").on("click", function() {
			var btn = $(this);
			var modelName = btn.attr("model-name");
			var attributeName = btn.attr("attribute-name");
			var fieldType = btn.attr("field-type");
			var rowId = btn.attr("rowId");
			var text = "";
			var error = 0;
			text = btn.html();
			var inputfield, input;
			//console.log(modelName + " " + attributeName + " " + fieldType);
			switch (fieldType) {
				case 'number' :
					input = '<input value="' + text + '" type="number" class="' + modelName + "_" + attributeName + '"/>';
					break;
				case 'text':
					input = '<input value="' + text + '" type="text" class="' + modelName + "_" + attributeName + '"/>';
					if (modelName == "AgrPhone") {
						input = '<input style="width:20px;" value="' + text.split(' ')[0] + '" type="text" class="phone" dataAttr="countryCode"/>' +
								'<input style="width:40px;" value="' + text.split(' ')[1] + '" type="text" class="phone" dataAttr="cityCode"/>' +
								'<input style="width:70px;" value="' + text.split(' ')[2] + '" type="text" class="phone" dataAttr="number"/>';
					}
					break;

				case 'textarea':
					input = '<textarea class="' + modelName + "_" + attributeName + '">' + text + '</textarea>';
					break;

				case 'select':

					switch (attributeName) {
						case 'type':
							input = '<select class="' + modelName + "_" + attributeName + '">' +
									'<option ' + (text == 'Бесплатно' ? 'selected="true"' : '') + 'value="0">Бесплатно</option>' +
									'<option ' + (text == 'Скидка' ? 'selected="true"' : '') + 'value="1">Скидка</option>' +
									'<option ' + (text == 'Подарок' ? 'selected="true"' : '') + 'value="2">Подарок</option>' +
									'</select><span data-tooltip="Страница будет перезагружена после изменения типа акции!" class="my_tooltip"></span>';
							break;

						case 'publish':
							input = '<select class="' + modelName + "_" + attributeName + '">' +
									'<option ' + (text == 'Да' ? 'selected="true"' : '') + 'value="1">Да</option>' +
									'<option ' + (text == 'Нет' ? 'selected="true"' : '') + 'value="0">Нет</option>' +
									'</select>';
							break;
						case 'unlimited':
							input = '<select class="' + modelName + "_" + attributeName + '">' +
									'<option ' + (text == 'Да' ? 'selected="true"' : '') + 'value="1">Да</option>' +
									'<option ' + (text == 'Нет' ? 'selected="true"' : '') + 'value="0">Нет</option>' +
									'</select><span data-tooltip="Страница будет перезагружена после изменения типа акции!" class="my_tooltip"></span>';
							break;
						case 'sexId':
							input = '<select class="' + modelName + "_" + attributeName + '">' +
									'<option ' + (text == 'Мужской' ? 'selected="true"' : '') + 'value="81160944c16a392048918e487f6c328a">Мужской</option>' +
									'<option ' + (text == 'Женский' ? 'selected="true"' : '') + 'value="b4a45cdfcb1a97ee4ba9baf3c3013f69">Женский</option>' +
									'</select>';
							break;
					}
					break;
				case 'datepicker':
					switch (attributeName) {
						case 'birthday':
							text = btn.html();
							input = '';
							var dp = $("#date_" + rowId);
							dp.find("#AgrDoctor_birthday").val(text);
							dp.show();
							break;
						case 'start':
							text = btn.html();
							input = '';
							var dp = $("#start-date");
							dp.find("#AgrAction_start").val(text);
							dp.show();
							break;
						case 'end':
							text = btn.html();
							input = '';
							var dp = $("#end-date");
							dp.find("#AgrAction_end").val(text);
							dp.show();
							break;
					}

					break;
			}
			input += '&nbsp;<button class="btn btn-success">OK</button>&nbsp;<button class="btn btn-danger">ОТМЕНА</button>';
			btn.after(input);
			btn.hide();
			inputfield = $("." + modelName + "_" + attributeName);
			inputfield.css("border-color", "none");
			//inputfield.focus();			
			var btnOk = btn.closest("p").find(".btn-success");
			var btnBad = btn.closest("p").find(".btn-danger");
			if (modelName == "AgrPhone") {
				$('input[dataAttr="countryCode"]').mask("9", {placeholder: "_"});
				$('input[dataAttr="cityCode"]').mask("999?9", {placeholder: "_"});
				$('input[dataAttr="number"]').mask("99999?99", {placeholder: "_"});
			}

			btnOk.on('click', function() {
				var newData = inputfield.val();
				var rowId = $(this).siblings(".ajax_update").attr('rowId');
				switch (fieldType) {
					case 'datepicker':
						switch (attributeName) {
							case 'birthday':
								newData = $("#AgrDoctor_birthday").val();
								$("#date_" + rowId).hide();
								break;
							case 'start':
								newData = $("#AgrAction_start").val();
								$("#start-date").hide();
								break;
							case 'end':
								newData = $("#AgrAction_end").val();
								$("#end-date").hide();
								break;
						}

				}
				if (modelName == "AgrPhone") {
					if ($('.phone[dataattr="countryCode"]').val()
							&& $('.phone[dataattr="cityCode"]').val()
							&& $('.phone[dataattr="number"]').val()) {
						newData = $('.phone[dataattr="countryCode"]').val() + " "
								+ $('.phone[dataattr="cityCode"]').val() + " "
								+ $('.phone[dataattr="number"]').val();
						btn.html(newData);
					}
				}
				$.ajax({
					async: false,
					url: "/admin/ajax/changeData",
					type: "POST",
					dataType: "JSON",
					data: {
						model: modelName,
						attribute: attributeName,
						newData: newData,
						rowId: rowId
					},
					success: function(response) {
						if (response.error == true) {
							error = 1;
							$(".error_popup").remove();
							$("body").append('<div class="error_popup"></div>');
							$(".error_popup").hide();
							for (var key in response.error_message) {
								$(".error_popup").append('<p>' + response.error_message[key] + '</p>');
							}
							$(".error_popup").show();
							setTimeout(function() {
								$(".error_popup").fadeOut();
							}, 1000);

							//response.error_message

							//console.log(response.error);
							//console.log(response.error==true)
						} else {
							error = 0;
						}
					},
				});
				if (error) {


					//inputfield.css("background-color","#E10000");					
					inputfield.css("border-color", "#E10000");
					setTimeout(function() {
						inputfield.css("border-color", "#CCCCCC");
					}, 700);
				} else {
					var newValue = newData;
					if (modelName == "AgrPhone") {
						$(".phone").hide();
						$(".phone").remove();
					}
					if (modelName == 'AgrAction' && (attributeName == "type" || attributeName == "unlimited")) {
						location.reload();
					}
					/*Если селект, подставляем название выбранной опции*/
					switch (fieldType) {
						case "select":
							newValue = inputfield.find("option:selected").html();
							break;
					}

					btn.html(newValue);
					inputfield.hide();
					inputfield.remove();
					btn.parent().find(".my_tooltip").remove();
					btnOk.remove();
					btnBad.remove();
					btn.show();
				}
			});



			btnBad.on('click', function() {
				var rowId = $(this).siblings(".ajax_update").attr('rowId');
				if (modelName == "AgrPhone") {
					$(".phone").hide();
					$(".phone").remove();
				}
				if (fieldType == "datepicker") {

					$("#date_" + rowId).hide();
					switch (attributeName) {
						case 'birthday':							
							$("#date_" + rowId).hide();
							break;
						case 'start':
							
							$("#start-date").hide();
							break;
						case 'end':
							$("#end-date").hide();
							
							break;
					}

				} else {

					inputfield.hide();
					inputfield.remove();

				}
				btn.parent().find(".my_tooltip").remove();
				btn.show();
				btnOk.remove();
				btnBad.remove();
			});
		});

	});
</script>

<h3><?= $model->company->name ?></h3>
<h3>Адрес: <?= $model->name ?></h3>
<style>
	.underline {
		text-decoration: underline;
	}		
</style>
<h4 class="text-center underline">Акции</h4>
<div class="ajax_inputs">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Название</th>
				<th>Данные</th>
				<th>Подтвердить</th>
			</tr>
		</thead>
		<tbody>
<?php if ($actions): $type = array('0' => 'Бесплатно', '1' => 'Скидка', '2' => 'Подарок') ?>
	<?php foreach ($actions as $act): ?>
					<tr>
						<td>
							<p>
								<span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="name" field-type="text" class="ajax_update"><?= $act->name ?></span></td>
						</p>
						<td>
							<p>Описание: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="description" field-type="textarea" class="ajax_update"><?= $act->description ?></span></p>
							<p>Статус публикации: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="publish" field-type="select" class="ajax_update"><?= ($act->publish ? 'Да' : 'Нет') ?></span></p>
							<p>Бессрочная: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="unlimited" field-type="select" class="ajax_update"><?= ($act->unlimited ? 'Да' : 'Нет') ?></span></p>					
							<?php if (!$act->unlimited): ?>
								<p>Дата начала: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="start" field-type="datepicker" class="ajax_update"><?= date("Y-m-d", strtotime($act->start)) ?></span></p>

								<?php
								$this->widget('ext.TDatePicker.TDatePicker', array(
									'model' => $act,
									'attribute' => 'start',
									'options' => array(
										'format' => 'yyyy-mm-dd',
										'language' => 'ru',
										'pickerPosition' => 'top-right',
									),
									'htmlOptions' => array(
										'id' => 'start-date',
										'value' => date("Y-m-d", strtotime(($act->start == '0000-00-00 00:00:00' ? 0 : $act->start)))
									)
								));
								?> 
								<p>Дата окончания: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="end" field-type="datepicker" class="ajax_update"><?= date("Y-m-d", strtotime($act->end)) ?></span></p>										
								<?php
								$this->widget('ext.TDatePicker.TDatePicker', array(
									'model' => $act,
									'attribute' => 'end',
									'options' => array(
										'format' => 'yyyy-mm-dd',
										'language' => 'ru',
										'pickerPosition' => 'top-right',
										'minView' => 1
									),
									'htmlOptions' => array(
										'id' => 'end-date',
										'value' => date("Y-m-d", strtotime(($act->end == '0000-00-00 00:00:00' ? 0 : $act->start)))
									)
								));
								?> 
		<?php endif; ?>
							<p>Тип акции: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="type" field-type="select" class="ajax_update"><?= $type[$act->type] ?></span></p>										
							<?php if ($act->type == 1): ?>
								<p>Старая цена: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="priceOld" field-type="number" class="ajax_update"><?= $act->priceOld ?></span></p>
								<p>Новая цена: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="priceNew" field-type="number" class="ajax_update"><?= $act->priceNew ?></span></p>
								<p>Скидка (%): <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="discount" field-type="number" class="ajax_update"><?= $act->discount ?></span></p>
		<?php endif; ?>
		<?php if ($act->type == 2): ?>
								<p>Текст подарка: <span rowId="<?= $act->id ?>" model-name="AgrAction" attribute-name="present" field-type="text" class="ajax_update"><?= $act->present ?></span></p>
		<?php endif; ?>					
						</td>
						<td class="button_actions">
							<button data-action="Action" data-id="<?= $act->id ?>" class="btn btn-success accept_btn">Подтвердить</button><br/><br/>
							<button data-action="Action" data-id="<?= $act->id ?>" class="btn btn-danger decline_btn">Отклонить</button>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="3">Нет изменений</td>
				</tr>	
	<?php endif; ?>		
		</tbody>	
	</table>
	<br/>
	<?php if ($model->id === $mainId): ?>
			<h4 class="text-center underline">Данные компании</h4>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Действующие данные</th>
						<th>Новые данные</th>
						<th>Подтвердить</th>
					</tr>
				</thead>
				<tbody>
		<?php if ($company): ?>			
				<tr>
					<td>					
						<p><strong>Описание:</strong><br/> <?= $activeCompany->description ?></p>
						<p><strong>Дата основания:</strong> <?= $activeCompany->dateOpening ?></p>
					</td>
					<td>
						<p><strong>Описание:</strong> <span rowId="<?= $company->id ?>" model-name="AgrCompany" attribute-name="description" field-type="textarea" class="ajax_update"><?= $company->description ?></span></p>
						<p><strong>Дата основания:</strong> <span rowId="<?= $company->id ?>" model-name="AgrCompany" attribute-name="dateOpening" field-type="text" class="ajax_update"><?= $company->dateOpening ?></span></p>				
					</td>
					<td class="button_actions">
						<button data-action="Company" data-id="<?= $company->id ?>" class="btn btn-success accept_btn">Подтвердить</button><br/><br/>
						<button data-action="Company" data-id="<?= $company->id ?>" class="btn btn-danger decline_btn">Отклонить</button>
					</td>
				</tr>
			<?php else: ?>
				<tr>
					<td colspan="3">Нет изменений</td>
				</tr>	
			<?php endif; ?>		
				</tbody>	
			</table>
	<?php else: ?>
			<h4 class="text-center underline">Данные адреса</h4>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Действующие данные</th>
						<th>Новые данные</th>
						<th>Подтвердить</th>
					</tr>
				</thead>
				<tbody>
		<?php if ($address): ?>			
				<tr>
					<td>					
						<p><strong>Описание:</strong><br/> <?= $model->description ?></p>						
					</td>
					<td>
						<p><strong>Описание:</strong> <span rowId="<?= $address->id ?>" model-name="AgrAddress" attribute-name="description" field-type="textarea" class="ajax_update"><?= $address->description ?></span></p>						
					</td>
					<td class="button_actions">
						<button data-action="Address" data-id="<?= $address->id ?>" class="btn btn-success accept_btn">Подтвердить</button><br/><br/>
						<button data-action="Address" data-id="<?= $address->id ?>" class="btn btn-danger decline_btn">Отклонить</button>
					</td>
				</tr>
			<?php else: ?>
				<tr>
					<td colspan="3">Нет изменений</td>
				</tr>	
			<?php endif; ?>		
				</tbody>	
			</table>
	<?php endif; ?>

	<br/><h4 class="text-center underline">Контактная информация</h4>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Тип данных</th>
				<th>Действующие данные</th>
				<th>Новые данные</th>
				<th>Подтвердить</th>
			</tr>
		</thead>
		<tbody>
<?php if ($email): ?>			
				<tr>
					<td>
						<p>Электронная почта</p>
					</td>
					<td>
						<p><?= $activeEmail->name ?></p>
					</td>					
					<td>
						<p><span rowId="<?= $email->id ?>" model-name="AgrEmail" attribute-name="name" field-type="text" class="ajax_update"><?= ($email->name ? $email->name : '<i>Будет удален</i>') ?></span></p>				
					</td>
					<td class="button_actions">
						<button data-action="Email" data-id="<?= $email->addressId ?>" class="btn btn-success accept_btn">Подтвердить</button><br/><br/>
						<button data-action="Email" data-id="<?= $email->addressId ?>" class="btn btn-danger decline_btn">Отклонить</button>
					</td>
				</tr>			
<?php endif; ?>	
<?php if ($ref): ?>			
				<tr>
					<td>
						<p>Адрес сайта</p>
					</td>
					<td>
						<p><?= $activeRef->refValue ?></p>
					</td>		
					<td>
						<p><span rowId="<?= $ref->id ?>" model-name="AgrRef" attribute-name="refValue" field-type="text" class="ajax_update"><?= ($ref->refValue ? $ref->refValue : '<i>Будет удален</i>') ?></span></p>				
					</td>
					<td class="button_actions">
						<button data-action="Ref" data-id="<?= $ref->ownerId ?>" class="btn btn-success accept_btn">Подтвердить</button><br/><br/>
						<button data-action="Ref" data-id="<?= $ref->ownerId ?>" class="btn btn-danger decline_btn">Отклонить</button>
					</td>
				</tr>			
<?php endif; ?>		
<?php if ($phones): ?>			
				<tr>
					<td>
						<p>Контактные телефоны</p>
					</td>
					<td>
						<?php foreach ($activePhones as $ph): ?>
							<p><?= $ph->name ?></p>				
						<?php endforeach; ?>
					</td>
					<td>
	<?php foreach ($phones as $ph): ?>
							<p><span rowId="<?= $ph->id ?>" model-name="AgrPhone" attribute-name="name" field-type="text" class="ajax_update"><?= ($ph->name == "remove" ? '<i>Будет удален</i>' : $ph->name) ?></span></p>				
	<?php endforeach; ?>					
					</td>
					<td class="button_actions">
						<button data-action="Phones" data-id="<?= $phones[0]->addressId ?>"class="btn btn-success accept_btn">Подтвердить</button><br/><br/>
						<button data-action="Phones" data-id="<?= $phones[0]->addressId ?>" class="btn btn-danger decline_btn">Отклонить</button>
					</td>
				</tr>			
<?php endif; ?>
		</tbody>	
	</table>

	<br/><h4 class="text-center underline">Доктора</h4>
	<table style="margin-bottom:200px;" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>Действующие данные</th>
				<th>Новые данные</th>
				<th>Подтвердить</th>
			</tr>
		</thead>
		<tbody>
<?php if ($doctors): ?>			
	<?php foreach ($doctors as $key => $d): ?>
					<tr>
						<td>
							<p><strong>Имя:</strong> <?= $activeDoctors[$key]->firstName ?></p>
							<p><strong>Отчество:</strong> <?= $activeDoctors[$key]->fatherName ?></p>
							<p><strong>Фамилия:</strong> <?= $activeDoctors[$key]->surName ?></p>

							<p><strong>Пол:</strong> <?= $activeDoctors[$key]->sex->name ?></p>
							<p><strong>Год начала карьеры:</strong> <?= $activeDoctors[$key]->experience ?></p>
							<p><strong>Описание врача:</strong> <?= $activeDoctors[$key]->description ?></p>
							<p><strong>Дата рождения:</strong> <?= date("d.m.Y", strtotime($activeDoctors[$key]->birthday)) ?></p>							
						</td>
						<td>
							<p><strong>Имя:</strong> <span rowId="<?= $doctors[$key]->id ?>" model-name="AgrDoctor" attribute-name="firstName" field-type="text" class="ajax_update"><?= $doctors[$key]->firstName ?></span></p>
							<p><strong>Отчество:</strong> <span rowId="<?= $doctors[$key]->id ?>" model-name="AgrDoctor" attribute-name="fatherName" field-type="text" class="ajax_update"><?= $doctors[$key]->fatherName ?></span></p>
							<p><strong>Фамилия:</strong> <span rowId="<?= $doctors[$key]->id ?>" model-name="AgrDoctor" attribute-name="surName" field-type="text" class="ajax_update"><?= $doctors[$key]->surName ?></span></p>

							<p><strong>Пол:</strong> <span rowId="<?= $doctors[$key]->id ?>" model-name="AgrDoctor" attribute-name="sexId" field-type="select" class="ajax_update"><?= $doctors[$key]->sex->name ?></span></p>
							<p><strong>Год начала карьеры:</strong> <span rowId="<?= $doctors[$key]->id ?>" model-name="AgrDoctor" attribute-name="experience" field-type="text" class="ajax_update"><?= $doctors[$key]->experience ?></span></p>
							<p><strong>Описание врача:</strong> <span rowId="<?= $doctors[$key]->id ?>" model-name="AgrDoctor" attribute-name="description" field-type="textarea" class="ajax_update"><?= $doctors[$key]->description ?></span></p>
							
							<p><strong>Дата рождения:</strong><span rowId="<?= $doctors[$key]->id ?>" model-name="AgrDoctor" attribute-name="birthday" field-type="datepicker" class="ajax_update"><?= date("d.m.Y", strtotime($doctors[$key]->birthday)) ?></span></p>
							<?php
							$this->widget('ext.TDatePicker.TDatePicker', array(
								'model' => $doctors[$key],
								'attribute' => 'birthday',
								'options' => array(
									'format' => 'dd.mm.yyyy',
									'language' => 'ru',
									'pickerPosition' => 'top-right'
								),
								'htmlOptions' => array(
									'id' => 'date_' . $doctors[$key]->id,
									'value' => $doctors[$key]->birthday == "0000-00-00 00:00:00" ? "" : date("d.m.Y", strtotime($doctors[$key]->birthday))
								)
							));
							?> 


						</td>
						<td class="button_actions">
							<button data-action="Expert" data-id="<?= $key ?>" data-addressId="<?= $model->id ?>" class="btn btn-success accept_btn">Подтвердить</button><br/><br/>
							<button data-action="Expert" data-id="<?= $key ?>" class="btn btn-danger decline_btn">Отклонить</button>
						</td>
					</tr>	
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="3">Нет изменений</td>
				</tr>	
<?php endif; ?>			

		</tbody>	
	</table>
</div>
<style>
	#start-date {
		display: none;
	}
	#end-date {
		display: none;
	}
	.error_popup {
		background-color: rgba(0, 0, 0, 0.9);
		border: 1px solid #CCCCCC;
		border-radius: 5px;
		color: #FFFFFF;
		min-height: 80px;    
		padding: 5px;
		position: fixed;
		right: 20px;
		top: 51px;
		width: 300px;
		z-index: 1000;
	}
	.error_popup p {
		margin-bottom:8px;
	}

	.my_tooltip {
		border-bottom: 1px dotted #0077AA;
		cursor: default;
	}

	.my_tooltip::after {
		background: rgba(0, 0, 0, 0.8);
		border-radius: 8px 8px 8px 0px;
		box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.5);
		color: #FFF;
		content: attr(data-tooltip); /* Главная часть кода, определяющая содержимое всплывающей подсказки */
		margin-top: -29px;
		margin-left:-220px;
		opacity: 0; /* Наш элемент прозрачен... */
		padding: 3px 7px;
		position: absolute;
		visibility: visible; /* ...и скрыт. */            
		transition: all 0.4s ease-in-out; /* Добавить плавности по вкусу */
	}

	select:hover + .my_tooltip:after {
		opacity: 1; /* Показываем его */
		visibility: visible;    
	}	

	.ajax_inputs input,
	.ajax_inputs select,
	.ajax_inputs textarea {
		display: inline-block;
		height: 28px;
		padding: 4px 6px;
		margin-bottom: 0px !important;
		font-size: 14px;
		line-height: 20px;
		color: #555555;
		vertical-align: middle;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}
	.ajax_inputs textarea {
		padding:4px 6px;
		height: 400px;
		width: 400px;
		resize:none;
	}
	.ajax_update {
		background-color: #DADADA;
		cursor: pointer;
		display: inline-block;
		padding: 0 6px;
		min-height: 20px;
		min-width: 20px;
	}
	.button_actions {
		width:200px;
		text-align:center!important;
		vertical-align: middle!important;
	}
	.button-block {
		padding-left:8px!important;

	}
	.button-block img{
		cursor:pointer;
	}
</style>