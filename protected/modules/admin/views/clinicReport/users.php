<?php
/* @var $this ClinicReportController */

$this->breadcrumbs=array(
	'Модерация ЛКК',
);

Yii::app()->clientScript->registerScript('button_actions','
	$(".button_view").click(function() {
            var id = $(this).attr("data-id");
            location.href="/admin/clinicReport/view?id="+id
        });
	')
?>

<?php						
	$this->widget('ESelect2',[						
		'name'=> '',						
		'data' =>  [],
		'value' => '',
		'options'=>[
			'placeholder'=>'',
			'allowClear'=>true,							
		],
		'htmlOptions' => [
			'style' => '
				display: none;'
		],
	]);
?>

<h1>Зарегистрированные пользователи ЕМП (<?=$dataProvider->getTotalItemCount()?>)</h1>
<br><a href="/admin/clinicReport/subscribers?downloadCSV=1">Скачать список пользователей, подписавшихся на рассылку (.csv)</a>
<style>
	table tr th, table tr td {
		text-align: center!important;
	}
</style>

<?php
if(!isset($_REQUEST["ajax"]) OR $_REQUEST["ajax"] === "users_GridView") {
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => "users_GridView",
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
	'filter' => $dataProvider->model,
    'template'=>"{items}{pager}",
	//'afterAjaxUpdate'=>'afterAjaxUpdate',
    'columns' => [
        [	
			'name'=>'name',
			'header'=>'Имя',
			'value'=>'$data->name',
			'type'=>'raw',
		],
		[
			'name'=>'sexId',
			'header'=>'Пол',
			'value'=> function($data) {
					$result = ($data->sexId == 'b4a45cdfcb1a97ee4ba9baf3c3013f69' ) ? 'Ж' : 'М';
					return $result;
				},
			'type'=>'raw',
			'filter'=>false,
		],
		[
			'name' => 'appointmentToDoctorsCount',
			'header'=>'Записей к врачу',
			'value'=> function($data) {
				return "<a href='/newAdmin/registryList/view?patient_id={$data->id}'>" . $data->appointmentToDoctorsCount . "</a>";
			},
			'type'=>'raw',
			'filter'=>false,
		],
		[
			'name'=>'email',
			'header'=>'E-mail',
			'value'=>'$data->email',
			'type'=>'raw',
		],
		[
			'name'=>'dateRegister',
			'header'=>'Дата регистрации',
			'value'=>'$data->dateRegister',
			'filter'=>false,
		],
		[
			'name'=>'telefon',
			'header'=>'Телефон',
			'value'=>'$data->telefon',
			'type'=>'raw',
		],
		[
			'name'=>'balance',
			'header'=>'Баланс',
			'value'=>'$data->balance',
			'type'=>'raw',
		],
		[
			'name'=>'registerPromoCodeUsed',
			'header'=>'Использовал промо-код',
			'value'=>function($data) {
				return (($data->registerPromoCodeUsed)
						? "<a href='javascript:void(0)' onclick='appendPromoCodeInfo(this,\"".$data->id."\")'>Да</a>"
						: "Нет");
			},
			'type'=>'raw',
			'htmlOptions' => [
				'style'=>'text-align: left !important; min-width: 200px !important;'
			],
			'filter' => CHtml::dropDownList('User[registerPromoCodeUsed]', Yii::app()->request->getParam('User')['registerPromoCodeUsed'], ['' => '-', '1'=>'Да', '0'=>'Нет']),
		],
		[
			'header'=>'Права',
			'value'=>function($data) {
				$result = "";
				$rightTypes = RightType::model()->findAll();
				$userRihts = Right::model()->findAll(['condition' => 'userId = '.Yii::app()->db->quoteValue($data->id)]);
				foreach ($rightTypes as $rightType) {
					$userRight_id = "userRight|".$data->id."|".$rightType->id;
					if(in_array($rightType->name, ['is_admin_in_address','is_owner_of_address'])) {
						$result .= "<div>";
						$result .= $rightType->description .":";
						$result .= "</div>";
						$result .= "<table>";
						foreach ($userRihts as $right) {
							if($rightType->id == $right->zRightId AND !empty($right->value)) {
								$result .= "<tr>";
								$userAddress = Address::model()->findByPk($right->value);
								$userAddressName = $userAddress->company->name . ", " . $userAddress->street . " " . $userAddress->houseNumber;
								$result .= "<td style='width:100%;text-align:left !important;'>" . $userAddressName . "</td>";
								$result .= "<td>" . "<button id='".$userRight_id."|".$right->value."' style='float:center' class='removeAddress remove_selected slim'>Удалить</button>" . "</td>";
								$result .= "</tr>";
							}
						}
						$result .= "<tr>";
						$result .= "<td style='width:100%;'><input id='userRightValue|".$data->id."|".$rightType->id."' class='w280 addressInput' type='text'></td>";
						$result .= "<td>" . "<button id='".$userRight_id."' style='float:right' class='addAddress save-changes slim'>Добавить</button>" . "</td>";
						$result .= "</tr>";
						$result .= "</table>";
					} else {
						$result .= "<label for='".$userRight_id."'>";
							$result .= "<input style='margin: 0 5px 0 0 !important;' class='userRight' id='".$userRight_id."' type='checkbox' ";
							foreach ($userRihts as $right) {
								if($rightType->id == $right->zRightId) {
									$result .= "checked"; break;
								}
							}
							$result .= ">";
							$result .= $rightType->description;
						$result .= "</label>";
					}
				}
				return "<div style='display:none;'>" . $result . "</div><a href='javascript:void(0)' onclick=\"$(this).closest('td').find('div').show(); $(this).remove();\">Показать</a>";
			},
			'type'=>'raw',
			'htmlOptions' => [
				'style'=>'text-align: left !important; min-width: 250px !important;'
			],
			'filter' => CHtml::dropDownList('Right[name]', Yii::app()->request->getParam('Right')['name'], 
						array_merge(['' => 'Все'],CHtml::listData(RightType::model()->findAll(), 'name', 'description'))
					),
		],
        //array('name'=>'datePublications'),
		/*[
			'header'=>'Восстановление доступа',
			'type'=>'raw',
			'value'=>'"<span data-id=\"$data->id\" class=\"btn btn-success unblockUser\"> Подтвердить</span>"',
		
		],*/
    ],
));
}			
?>
<assets style="">
	<div id="SelectAddressForm">
	<?php
	if(!isset($_REQUEST["ajax"]) OR $_REQUEST["ajax"] === "SelectAddress_GridView") {
		$criteria = new CDbCriteria();
		$criteria->select = "name, company.name";
		$criteria->with = [
			"company" => [
				"together" => true,
				"with" => [
					"companyType" => [
						"together" => true,
					]
				]
			]
		];
		$criteria->compare('companyType.isMedical',1);
		$criteria->compare('company.removalFlag',0);
		if(isset($_REQUEST["ajax"])) {
			if(isset($_REQUEST["SelectAddress"]["name"])) {
				$criteria->compare("t.name",$_REQUEST["SelectAddress"]["name"],true);
			}
			if(isset($_REQUEST["SelectAddress"]["companyName"])) {
				$criteria->compare("company.name",$_REQUEST["SelectAddress"]["companyName"],true);
			}
		}
		$selectAddress_dataProvider = new CActiveDataProvider(Address::model(), array(
			'criteria' => $criteria,			
		));
		$this->widget('bootstrap.widgets.TbGridView', array(
			'id' => "SelectAddress_GridView",
		    'type'=>'striped bordered condensed',
		    'dataProvider' => $selectAddress_dataProvider,
			'filter' => $dataProvider->model,
		    'template'=>"{items}{pager}",
		    'columns' => [
				[	
					'name'=>'company.name',
					'filter'=> CHtml::textField('SelectAddress[companyName]', Yii::app()->request->getParam('SelectAddress')['companyName']),
					'htmlOptions' => [
						'style'=>'text-align: left !important; cursor: pointer;'
					],
				],
				[	
					'name'=>'name',
					'filter'=> CHtml::textField('SelectAddress[name]', Yii::app()->request->getParam('SelectAddress')['name']),
					'htmlOptions' => [
						'style'=>'text-align: left !important; cursor: pointer;'
					],
				],
		    ],
			'selectionChanged'=>'function(id){
				var id_code = $("[id^=\'SelectAddressForm\']").attr(\'id\').split(\'|\');
				id_code.splice(0,1);
				$("#" + id_code.join("\\\|")).val($.fn.yiiGridView.getSelection(id));
				parent.$.fancybox.close();
			}',
		));
	}
	?>
	</div>
</assets>
<script>
function appendPromoCodeInfo(obj, userId) {
	getPromoCodeInfo(userId, function(response) {
		$(obj).attr('onclick','');
		if('referer' in response) {
			$(obj).after("<br>" + "Реферер" + " : " + response['referer']);
		}
		if('promocodes' in response) {
			for(var i in response.promocodes) {
				$(obj).after("<br>" + "Дата" + " : " + response.promocodes[i]['createdDate']);
				$(obj).after("<br>" + "Промокод" + " : " + response.promocodes[i]['promocode']);
			}
		}
	});
}
function getPromoCodeInfo(userId, success) {
	$.ajax({
		url: 'getPromoCodeInfo',
		async: false,
		data: {
			userId: userId,
		},
		dataType: 'json',
		type: 'GET',
		success: function(response) {
			if(response.status === "OK") {
				success(response);
			} else {
				this.error(null,JSON.stringify(response),null);
			}
		},
		error: function(XHR,textStatus,errorThrown) {
			alert(textStatus);
		},
		complete: function() {
		
		}
	});
}
$(function() {
    $(document).ready(function() {
    	var SelectAddressForm = $( "#SelectAddressForm" ).clone();
    	$( "#SelectAddressForm" ).remove();
    	var showSelectAddressForm = function (obj) {
    		var form = SelectAddressForm.clone();
    		form.attr("id",form.attr("id")+"|"+$(obj).attr("id"));
    		$.fancybox({
    			id: "SelectAddress_fancyView",
    			content: form,
    		});
    	};
        $('.addressInput').live('click', function() {
        	showSelectAddressForm(this);
        });
        
		$('.userRight').live('change', function() {
			var self = this;
			var data = $(this).attr('id').split('|');
			$.ajax({
				url: '/admin/ajax/userRight',
				async: false,
				data: {
					action: data[0],
					userId: data[1],
					rightType: data[2],
					checked: self.checked ? 1 : 0,
				},
				dataType: 'json',
				type: 'POST',
				success: function(response) {
					if(response.status === "OK") {
						$(self).closest('label').css("background-color","lightgreen");
					} else {
						this.error(null,JSON.stringify(response),null);
					}
				},
				error: function(XHR,textStatus,errorThrown) {
					self.checked = !self.checked;
					$(self).closest('label').css("background-color","lightpink");
					alert(textStatus);
				},
				complete: function() {
					setTimeout(function() {
						$(self).closest('label').css("background-color","");
					}, 1000);
				}
			});
		});
		
		$('.removeAddress').live('click', function() {
			var self = this;
			var data = $(this).attr('id').split('|');
			$.ajax({
				url: '/admin/ajax/userRight',
				async: false,
				data: {
					action: data[0],
					userId: data[1],
					rightType: data[2],
					value: data[3],
					checked: 0,
				},
				dataType: 'json',
				type: 'POST',
				success: function(response) {
					if(response.status === "OK") {
						$(self).closest('tr').remove();
					} else {
						this.error(null,JSON.stringify(response),null);
					}
				},
				error: function(XHR,textStatus,errorThrown) {
					alert(textStatus);
				},
				complete: function() {
				
				}
			});
		});
		
		$('.addAddress').live('click', function() {
			var self = this;
			var data = $(this).attr('id').split('|');
			data[3] = $("#userRightValue\\|" + data[1] + "\\|" + data[2]).val();
			if(data[3].trim() == "") {
				return false;
			}
			console.log(this);
			$.ajax({
				url: '/admin/ajax/userRight',
				async: false,
				data: {
					action: data[0],
					userId: data[1],
					rightType: data[2],
					value: data[3],
					checked: 1,
				},
				dataType: 'json',
				type: 'POST',
				success: function(response) {
					if(response.status === "OK") {
						$(self).closest("tr").before("<tr><td style='width:100%;text-align:left !important;'>"+response.text+"</td><td><button id='userRight|"+data[1]+"|"+data[2]+"|"+data[3]+"' style='float:center' class='removeAddress remove_selected slim'>Удалить</button></td></tr>")
					} else {
						this.error(null,JSON.stringify(response),null);
					}
				},
				error: function(XHR,textStatus,errorThrown) {
					alert(textStatus);
				},
				complete: function() {
				
				}
			});
		});
    });
});

</script>
<?php
/*
$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'user-grid',
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'dataProvider'=> null,
		//'filter'=>$model,	
		//'enableSorting'=>true, 
		'columns'=>array(
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'company.name',
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 't.name',
						'value' => '$data->name',*/
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						/*'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)*/
		/*		),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'serviceSectionId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
				/*array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'companyActiviteId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(CompanyActivite::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)
				),*/
			/*	array(
						'class' => 'ext.editable.EditableColumn',						
						'name' => 't.status',
						'type'=>'raw',	
						'value'=> '$data->status',*/
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'textarea',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				/*),
				array(
						'class' => 'ext.editable.EditableColumn',
						'name' => 'price',
						'headerHtmlOptions' => array('style' => 'width: 50px'),
						'editable' => array(
								'url' => $this->createUrl('RequestAccept'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)
				),
				array
				(
						'class'=>'CButtonColumn',
						'htmlOptions' => array('style' => 'width: 85px'),*/
						/*'template'=>'{confirm btn btn-success} {reject btn btn-danger}',
						'buttons'=>array
						(
								'confirm btn btn-success' => array
								(
										'label'=>'<i class="icon-ok icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestAccept", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
								'reject btn btn-danger' => array
								(
										'label'=>'<i class="icon-ban-circle icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/admin/services/requestCancel", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
						),*/
			/*	),

		),
));*/
?>