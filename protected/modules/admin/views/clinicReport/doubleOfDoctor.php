<?php
/* @var $this ClinicReportController */

$this->breadcrumbs=array(
	'Модерация ЛКК' => '/admin/clinicReport',
	'Дубликаты врачей',
);
?>
<h2>Дубликаты врачей</h2>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	#'summaryText' => '',
	'itemsCssClass' => 'table table-striped table-bordered table-condensed',
	'columns' => array(
		[
			'name' => 'city',	//Город
			'type' => 'raw',
			'value' => function($data){
				return $data->currentPlaceOfWork->address->city->name;
			},
		],
		[
			'name' => 'company',	//Компания
			'type' => 'raw',
			'value' => function($data){
				return $data->currentPlaceOfWork->company->name;
			},
		],
		[
			'name' => 'address',	//Адрес
			'type' => 'raw',
			'value' => function($data){
				$value = "";
				if(!empty($data->currentPlaceOfWork->address->street)) {
					if($value !== "") $value .= ", ";
					$value .= $data->currentPlaceOfWork->address->street;
				}
				if(!empty($data->currentPlaceOfWork->address->houseNumber)) {
					if($value !== "") $value .= ", ";
					$value .= $data->currentPlaceOfWork->address->houseNumber;
				}
				return CHtml::link(
						@$value,
						@$data->currentPlaceOfWork->address->getPagesUrls()[0]
					);
			},
		],
		[
			'name' => 'name',	//ФИО
			'type' => 'raw',
			'value' => function($data){
				return CHtml::link(
						@$data->name,
						@$data->getPagesUrls($data->currentPlaceOfWork->address->id)[0]
					);
			},
		],
	),
));
?>