<?php $this->beginContent('/layouts/main'); ?>
<?php $this->renderPartial('/layouts/_header'); ?>
<section class="middle">
	<div class="container">
		<?php if ( isset($this->breadcrumbs) ): ?>
			<?php
			$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
				'links' => $this->breadcrumbs,
			));
			?><!-- breadcrumbs -->
		<? endif; ?>
		<?php echo $content; ?>
		<div class="clearfix"></div><!-- /clearfix -->
	</div><!-- /container-->
</section><!-- /middle-->
<footer class="footer">
</footer><!-- /footer -->
<?php $this->endContent(); ?>