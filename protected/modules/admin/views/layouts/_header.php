<?php /* @var $this Controller */ ?>
<!--<header class="header" style="height: 50px;">-->
	<?php
	$stats = Yii::app()->cache->get('moduleAdminStats');
	if (!Yii::app()->user->isGuest) {
		
		/*$this->widget('bootstrap.widgets.TbNavbar', array(
			'type' => null, // null or 'inverse'
			'brand' => 'ЕМП',
			'brandUrl' => '/',
			'collapse' => true, // requires bootstrap-responsive.css
			'items' => array(
				'<ul class="nav nav-sidebar">'
				. '<li><a href="/admin/news/">Новости '.($clinicNewsCount > 0 ? ""
						. "<sup><span class=\"badge badge-important\">{$clinicNewsCount}</span></sup>" : '') .'</a></li>'
				.'<li><a href="/admin/default/gallery">Фотогалерея</a></li>'
				
				. '<li><a href="/admin/clinicReport/">Модерация ЛКК '
				. ($clinicCount ? "<sup><span class=\"badge badge-important\">{$clinicCount}</span></sup></a></li>" : '</a></li>')
				. '<li><a href="/admin/reviews">Модерация отзывов '
				. ($countReview ? "<sup><span class=\"badge badge-important\">{$countReview}</span></sup></a></li>" : '</a></li>')
				. '<li><a href="/admin/clinicReport/blockedusers">Заблокированные пользователи '
				. ($countBlockedUsers ? "<sup><span id=\"countBlockedUsers\" class=\"badge badge-important\">{$countBlockedUsers}</span></sup></a></li>" : '</a></li>')
				. '<li></li><li><a href="/logout">Выход</a></li></ul>',
			//'<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
			),
		));*/
		/*. '<li><a href="/admin/services/request">Модерация услуг ' . (($countRequest > 0) ? ""
				. "<sup><span class=\"badge badge-important\">{$countRequest}</span></sup>" : '') . '</a></li>'*/
	
	?>
	<div class="sidebar">
	
	<div class="your_city" style="float: none; margin: 0; padding: 4px 4px 0 0;">
		<?php 
			$this->widget('UserRegion', [
				'regions' => Yii::app()->params['regions'], 
				'selectedRegion' => Yii::app()->session['selectedRegion'],
				//'readonly' => 'true'
			]); 
		?>
	</div>
	
	<a class="brand" href="/">ЕМП</a><br/><br/>

	<?php 
				$isContain=false;
				// var_dump(Yii::app()->user->model->rights);
				
				if(Yii::app()->user->model->rights){
				foreach (Yii::app()->user->model->rights as $right) {
    			  	if($right->zRightId == RightType::ACCESS_NEW_ADMIN) {
    					$isContain=true;
    					
      					}
					}
				}
				if($isContain) :
				?>
				<div>
					<a href="/newAdmin">
					Админка сотрудника ЕМП
					</a>
				</div>
					
	<? endif ?>

	
	<br/><br/>
	<?php 
		echo '<ul class="nav nav-sidebar">'
		. '<li><a href="/admin/news/">Новости <sup class="clinicNewsChangesCount">'
								. ($stats['clinicNewsChangesCount'] ? ("<span class='badge badge-important'>" . $stats['clinicNewsChangesCount'] . "</span>") : '')
								.'</sup></a></li>'
		.'<li><a href="/admin/default/gallery">Фотогалерея</a></li>'
		/*. '<li><a href="/admin/services/request">Модерация услуг ' . (($countRequest > 0) ? ""
				. "<sup><span class=\"badge badge-important\">{$countRequest}</span></sup>" : '') . '</a></li>'*/
		/*. '<li><a href="/admin/priority/">Приоритетное размещение'
		. '<li><a href="/admin/banners/">Баннерная реклама'*/
		. '<li><a href="/admin/clinicReport/">Модерация ЛКК <sup class="clinicChangesCount">'
								. ($stats['clinicChangesCount'] ? ("<span class='badge badge-important'>" . $stats['clinicChangesCount'] . "</span>") : '')
								.'</sup></a></li>'
		. '<li><a href="/admin/review">Управление отзывами <sup class="reviewChangesCount">'
								. ($stats['reviewChangesCount'] ? ("<span class='badge badge-important'>" . $stats['reviewChangesCount'] . "</span>") : '')
								.'</sup></a></li>'
		. '<li><a href="/admin/company/">Управление клиниками и адресами</a></li>'
		. '<li><a href="/admin/handbook/">Управление справочниками</a></li>'		
		. '<li><a href="/admin/clinicReport/activeusers">Управление пользователями '
		//. ($countBlockedUsers ? "<sup><span id=\"countBlockedUsers\" class=\"badge badge-important\">{$countBlockedUsers}</span></sup></a></li>" : '</a></li>')
		. '<li></li><li><a href="/logout">Выход</a></li></ul>'; 				
	?>
	<hr>
	<a href="/admin/default/statistics">Статистика</a>:<br/>
	Записей: <span class="stat_AppointmentToDoctorsCount"><?=$stats['stat_AppointmentToDoctorsCount']?></span> <br/>
	Записей за сегодня: <span class="stat_AppointmentToDoctorsCountToDay"><?=$stats['stat_AppointmentToDoctorsCountToDay']?></span> <br/>
	Записей за вчера: <span class="stat_AppointmentToDoctorsCountYesterday"><?=$stats['stat_AppointmentToDoctorsCountYesterday']?></span> <br/>
	Записей за последние 7 дней: <span class="stat_AppointmentToDoctorsCountLastSevenDays"><?=$stats['stat_AppointmentToDoctorsCountLastSevenDays']?></span> <br/>
	Регистраций на сайте: <span class="stat_UsersRegistry"><?=$stats['stat_UsersRegistry']?></span> <br/>
    Общее количество отзывов: <span class="stat_ReviewsCount"><?=$stats['stat_ReviewsCount']?></span><br/>
	<a href="<?= $this->createUrl('clinicReport/clinicOfferAcceptedXLS'); ?>">Клиник с подтвержденной офертой (.csv)</a>
	</div>
<?php  } ?>
<!--</header>--><!-- /header-->
<script>
function refreshChangesCount() {
	$.ajax({
	    url: '/admin/ajax/adminChangesCount',
	    async: true,
	    data: { },
	    type: 'GET',
	    dataType: 'json',
		beforeSend: function() {  },
	    success: function(data) {
	        if(('stat_AppointmentToDoctorsCount' in data)) {
	            $(".stat_AppointmentToDoctorsCount").html(data['stat_AppointmentToDoctorsCount']);
	        }
	        if(('stat_AppointmentToDoctorsCountToDay' in data)) {
	            $(".stat_AppointmentToDoctorsCountToDay").html(data['stat_AppointmentToDoctorsCountToDay']);
	        }
	        if(('stat_AppointmentToDoctorsCountYesterday' in data)) {
	            $(".stat_AppointmentToDoctorsCountYesterday").html(data['stat_AppointmentToDoctorsCountYesterday']);
	        }
	        if(('stat_AppointmentToDoctorsCountLastSevenDays' in data)) {
	            $(".stat_AppointmentToDoctorsCountLastSevenDays").html(data['stat_AppointmentToDoctorsCountLastSevenDays']);
	        }
	        if(('stat_UsersRegistry' in data)) {
	            $(".stat_UsersRegistry").html(data['stat_UsersRegistry']);
	        }
	        if(('stat_ReviewsCount' in data)) {
	            $(".stat_ReviewsCount").html(data['stat_ReviewsCount']);
	        }
	        var badge_clinicChangesCount = "";
	        if(('clinicChangesCount' in data) && parseInt(data['clinicChangesCount']) > 0) {
	        	badge_clinicChangesCount = "<span class='badge badge-important'>" + data['clinicChangesCount'] + "</span>";
	        }
	        $(".clinicChangesCount").html(badge_clinicChangesCount);

	        var badge_reviewChangesCount = "";
	        if(('reviewChangesCount' in data) && parseInt(data['reviewChangesCount']) > 0) {
	        	badge_reviewChangesCount = "<span class='badge badge-important'>" + data['reviewChangesCount'] + "</span>";
	        }
            $(".reviewChangesCount").html(badge_reviewChangesCount);

	        var badge_clinicNewsChangesCount = "";
	        if(('clinicNewsChangesCount' in data) && parseInt(data['clinicNewsChangesCount']) > 0) {
	        	badge_clinicNewsChangesCount = "<span class='badge badge-important'>" + data['clinicNewsChangesCount'] + "</span>";
	        }
            $(".clinicNewsChangesCount").html(badge_clinicNewsChangesCount);
	    },
	    error: function(XHR,errorText,errorThrown) {  },
	    complete: function() {
		    setTimeout(function() { refreshChangesCount();
		}, 30000) },
	});
}
refreshChangesCount();
</script>