<?php
//Yii::app()->clientScript->reset();
Yii::app()->clientScript->scriptMap['main.css'] = false;
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?= CHtml::encode($this->pageTitle) ?></title>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/shared/js/jquery.pie.min.js"></script>
	<![endif]-->
	<?php Yii::app()->bootstrap->register(); ?>
	<style>
		.brand {
			color: #777;
			display: block;
			float: left;
			font-size: 20px;
			font-weight: 200;
			margin-left: -20px;
			padding: 10px 20px;
			text-shadow: 0 1px 0 #fff;
		}
		.sidebar li a {
			display: block;
			height: 18px;
			padding: 5px 1px;
			color: #777;   
			text-decoration: none;
			text-shadow: 0 1px 0 #fff;
		}
		.sidebar {
			background-image: linear-gradient(to right, #fff, #e8e8e8);
			background-repeat: repeat-y;
			z-index: 1000;
			border-bottom: 1px solid #d6d6d6;
			border-right: 1px solid #d6d6d6;
			height: 100%;
			margin-right: 10px;
			padding: 2px 7px 0;
			position: fixed;
			width: 250px;
		}
		section {
			padding-top: 15px !important;
			padding-left: 250px !important;
			padding-right: 15px !important;
			max-width: 1000px !important;
		}
		.container,
		.navbar-static-top .container,
		.navbar-fixed-top .container,
		.navbar-fixed-bottom .container {
			width: 990px;
		}
		
		.main_column, .wrapper {
			width: auto;
		}
		
		.btn {
			color: #fff !important;
		}
		
		.btn-small {
			color: #000 !important;
		}
		
		.container {
			margin-left: 30px;
		}
	</style>
</head>
<body>
	<div style="min-width: 1250px;"><?php echo $content; ?></div>
</body>
</html>