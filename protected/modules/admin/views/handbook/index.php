<div>
	<div class="page-header"><h4>Управление справочниками</h4></div>
	<?=
	CHtml::dropDownList('handbook_select', '', [
		'' => 'Не выбрано',
		'CompanyActivite' => 'Профили деятельности',
		'DoctorSpecialty' => 'Специальности врачей',
		'Service' => 'Услуги',
		'DiseaseGroup' => 'Болезни',
		'MedicalSchool' => 'Учебные заведения',
		'CityDistrict' => 'Районы города',
		'MetroStation' => 'Станции метро',
		'Config' => 'Конфигурация сайта',
		'PromoCode' => 'Промокоды',
		#'Address' => 'Address',
	],['size'=>10,'style'=>'width:300px;float:left;margin-right:20px']);
	?>
	<span class="btn btn-primary addElement" style="float:left;display:none;">Добавить элемент</span>
	<div class="clearfix"></div>
	<table>
		<tr>
			<td class="dataContainer">

			</td>
			<td style="vertical-align:top;padding:0px 20px" class="editContainer">

			</td>
		</tr>
	</table>
</div>
<script>
	$(document).ready(function() {
		var table = '';
		var id = '';
		
		$(".addElement").click(function() {
			if(table) {
				$.ajax({
					url: "/admin/handbook/editBlock",
					async: false,
					data: ({
						table: table,
						'new':1
					}),
					dataType: 'html',
					type: "GET",
					success: function(response) {
						$('.editContainer').html(response);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert('Ошибка: ' + textStatus);
					},
				});
			}
		});
		
		$("#handbook_select").change(function() {
			table = $(this).val();
			$('.editContainer').html('');
			if(table) {				
				$('.addElement').show();
				$.ajax({
					url: "/admin/handbook/list",
					async: false,
					data: ({
						table: table,
					}),
					dataType: 'html',
					type: "GET",
					success: function(response) {
						$('.dataContainer').html(response);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert('Ошибка: ' + textStatus);
					},
				});
			} else {
				$('.addElement').hide();
				$('.dataContainer').html('');
			}
		});
		
		$("#listBox").live('change', function() {
			id = $(this).val();
			$.ajax({
				url: "/admin/handbook/editBlock",
				async: false,
				data: ({
					table: table,
					id: id
				}),
				dataType: 'html',
				type: "GET",
				success: function(response) {
					$('.editContainer').html(response);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert('Ошибка: ' + textStatus);
				},
			});
		});
		
		function saveData(formData, newData) {
			var pageLength = 100;
			$.ajax({
				url: "/admin/handbook/submitData",
				async: false,
				data: ({
					table: table,
					id: id,
					data: formData.splice(0,pageLength),
					'new':newData
				}),
				dataType: 'json',
				type: "POST",
				beforeSend: function() {
					$(".alert-success").remove();
					$(".alert-error").remove();
					document.body.style.cursor = 'wait';
				},
				success: function(response) {
					if(formData.length>0) {
						id = response.id;
						saveData(formData,0);
					}
					else {
						if(response.success) {
							$('.editContainer').prepend('<div style="display:none;padding:5px;margin-bottom: 5px" class="alert-success">\n\
				 				<h4 style="padding:0px;margin: 0">Изменения внесены</h4>\n\</div>');
							$(".alert-success").fadeIn(500);
							if(newData) {
								$("#handbook_select").change();
							}
						} else {
							this.error(null,JSON.stringify(response));
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('.editContainer').prepend('<div style="display:none;padding:5px;margin-bottom: 5px" class="alert-error">\n\<h4 style="padding:0px;margin: 0">Изменения сохранены</h4>\n\</div>');
					$(".alert-error").text('Ошибка: ' + textStatus);
					$(".alert-error").fadeIn(500);
				},
				complete: function() {
					document.body.style.cursor = 'default';
					window.scrollTo(0,0);
				},
			});
		}
		
		$("#goSaveNew").live('click', function() {
			saveData($('#blockForm').serializeArray(), 1);
		});
		
		$("#goSave").live('click', function() {
			saveData($('#blockForm').serializeArray(), 0);
		});

		$("#goDecline").live('click', function() {
			$('.editContainer').html('');
		});

		function deleteData(anyway) {
			anyway = typeof anyway !== 'undefined' ? anyway : false;
			$.ajax({
				url: "/admin/handbook/deleteData",
				async: true,
				data: ({
					table: table,
					id: id,	
					anyway: anyway ? 1 : 0,	
				}),
				dataType: 'json',
				type: "POST",
				beforeSend: function() {
					$(".row_btnset").hide();
					$(".row_loading").show();
					$(".alert-success").remove();
					$(".alert-error").remove();
					document.body.style.cursor = 'wait';
				},
				success: function(response) {
					if(response.used) {
						if (confirm(response.text)) {
							deleteData(true);
						} else {
							this.error(null,response.text);
						}
					} else
					if(response.success) {						
						$('.editContainer').html('');
						$('.editContainer').prepend('<div style="display:none;padding:5px;margin-bottom: 5px" class="alert-success">\n\
			 				<h4 style="padding:0px;margin: 0">Запись удалена</h4>\n\</div>');
						$(".alert-success").fadeIn(500);
						$("#handbook_select").change();	
					} else {
						this.error(null,response.text);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('.editContainer').prepend('<div style="display:none;padding:5px;margin-bottom: 5px" class="alert-error">\n\<h4 style="padding:0px;margin: 0">Изменения сохранены</h4>\n\</div>');
					$(".alert-error").text('Ошибка: ' + textStatus);
					$(".alert-error").fadeIn(500);
				},
				complete: function() {
					$(".row_btnset").show();
					$(".row_loading").hide();
					document.body.style.cursor = 'default';
					window.scrollTo(0,0);
				},
			});			
		};
		$("#goDelete").live('click', function() { deleteData(false); });
	});
</script>