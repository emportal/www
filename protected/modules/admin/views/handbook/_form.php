<?php
/**
 * @var ActiveRecord $model
 */
?>

<?php
$form = $this->beginWidget('CActiveForm', [
	'id' => 'blockForm',
	'enableAjaxValidation' => false,
		]);
?>
<style>
	.blockData .inputArea {
		padding:5px;
	}
	.inputArea input[type="text"] {
		width:400px;
	}
</style>
<table class="blockData">
	<?php if ($model->hasAttribute('id')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'id',array('label'=>'Идентификатор:')); ?>
			</td>
			<td class="inputArea">
				<?= ($model->id !== NULL) ? $model->id : "Не создан" ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('name')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'name',array('label'=>'Название')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'name'); ?>
			</td>		
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('locative')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'locative',array('label'=>'Местный падеж')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'locative'); ?>
			</td>		
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('fullName')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'fullName',array('label'=>'Полное название')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'fullName'); ?>
			</td>		
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('description')): ?>
        <?php
        $textAreaOptions = ['style' => 'width: 400px; height: 230px'];
        foreach ($model->getValidators('description') as $validator) {
            if ($validator instanceof CStringValidator && $validator->max !== null) {
                $textAreaOptions['maxlength'] = $validator->max;
                break;
            }
        }
        ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'description',array('label'=>'Описание')); ?>
			</td>
			<td class="inputArea">
				<?php
					if(method_exists($model,'getParameter')) {
						$model->description = $model->getParameter('description', City::model()->selectedCityId);
					}
				?>
				<?= $form->textArea($model, 'description', $textAreaOptions); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('tagH1')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'tagH1',array('label'=>'H1 тег')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'tagH1'); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('seoText')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'seoText',array('label'=>'SEO текст')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textArea($model, 'seoText', ['class' => 'admin_handbook_textarea_tag', 'style' => 'height: 120px;']); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('tagTitle')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'tagTitle',array('label'=>'Title тэг')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textArea($model, 'tagTitle', ['rows' => '1', 'style' => 'width: 100%;']); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('tagDescription')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'tagDescription',array('label'=>'Description тег')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textArea($model, 'tagDescription', ['class' => 'admin_handbook_textarea_tag']); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('tagKeywords')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'tagKeywords',array('label'=>'Keywords тег')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textArea($model, 'tagKeywords', ['class' => 'admin_handbook_textarea_tag']); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	
	<?php if ($model->hasAttribute('plural')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model, 'plural', array('label'=>'Множественное число (именительный падеж)')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'plural'); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('genitive')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model, 'genitive', array('label'=>'Родительный падеж (кого? чего?)')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'genitive'); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('genitivePlural')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model, 'genitivePlural', array('label'=>'Родительный падеж множ. числа')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'genitivePlural'); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('prepositive')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model, 'prepositive', array('label'=>'Предложный падеж (на ком? на чем?)')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'prepositive'); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('dative')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'dative',array('label'=>'Дательный падеж (кому? чему?)')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'dative'); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php
	 $cityModel = City::model()->findByAttributes([ "id" => City::model()->getSelectedCityId() ]);
	 if ($model->hasAttribute('latitude')): ?>
		<tr>
			<td>
				<span>Широта/Долгота</span>
				<span class="btn btn-primary" style="float: left;" onclick="
				$.ajax({
					url: '/?r=ajax/GetMetro',
					async: false,
					data: ({ metroName: $('#MetroStation_name').val(), cityName: <?= (!empty($cityModel)) ? Yii::app()->db->quoteValue($cityModel->name) : "NULL" ?> }),
					dataType: 'JSON',
					type: 'GET',
					beforeSend: function() { $('#geocoder_loading').css('visibility', 'visible'); },
					success: function(response) {
						if(response.status == 'OK') {
							$('#MetroStation_latitude').val(response.result.latitude);
							$('#MetroStation_longitude').val(response.result.longitude);
						} else { alert(response.text); }
					},
					error: function(jqXHR, textStatus, errorThrown) { alert('Ошибка: ' + textStatus); },
					complete: function(jqXHR, textStatus) { $('#geocoder_loading').css('visibility', 'hidden'); }
				});">Геокодер</span><div id="geocoder_loading" class="icon_loading" style="display: flex; visibility: hidden;"></div>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'latitude'); ?>
				<?= $form->textField($model, 'longitude'); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('value')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'value',array('label'=>'значение')); ?>
			</td>
			<td class="inputArea">
				<?php
					$type = "textField";
					if ($model->hasAttribute('type')) {
						$type = $model->type;
						if($type !== "checkBox" && $type !== "textField" && $type !== "textArea") {
							$type = "textField";
						}
					}
					echo $form->{$type}($model, 'value');
				?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('serviceSectionId')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'serviceSectionId',array('label'=>'Категория')); ?>
			</td>
			<td class="inputArea">
				<?= 
					$form->dropDownList($model, 'serviceSectionId',
						CHtml::listData(ServiceSection::model()->findAll(['order'=>'name ASC']), 'id', 'name'),
						array('empty' => '(не выбран)', 'style' => ''));
				?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('companyActiviteId')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'companyActiviteId',array('label'=>'Профиль деятельности')); ?>
			</td>
			<td class="inputArea">
				<?= 
					$form->dropDownList($model, 'companyActiviteId',
						CHtml::listData(CompanyActivite::model()->findAll(['order'=>'name ASC']), 'id', 'name'),
						array('empty' => '(не выбран)', 'style' => ''));
				?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('linkUrl')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'linkUrl',array('label'=>'ЧПУ урл')); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'linkUrl'); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php
	if($model->tableName() == 'cityDistrict'):
	$cityId = ($model->isNewRecord) ? City::model()->selectedCityId : $model->cityId;
	?>

		<tr>
			<td class="bold">Соседний район</td>
			<td><?= CHtml::dropDownList(CHtml::activeName($model, 'regionList'), '', CHtml::listData(CityDistrict::model()->findAll([
					'select'=>'id,name',
					'condition' => "cityId = '" . $cityId . "'",
					'order'=>'name ASC',
				]),'id','name')); ?> <span style="margin-bottom:10px;" onclick="addRegion();" class="btn btn-success">Добавить район в список</span>
				<div id="metroList">
					<?php
					$arrNearbyDistrict = $model->getNearbyDistrict();
					if(isset($arrNearbyDistrict) and count($arrNearbyDistrict) > 0):
						foreach($arrNearbyDistrict as $key=>$ms):?>
							<div class="rowRegion">
							<span style="width:150px;margin:0px 10px;display:inline-block;" class="regionName">
							<?=$ms->name?>
							</span>
								<input name="metroList[<?= $key ?>][value]" type="hidden" value="<?=$ms->attributes['cityNearbyDistrictId'] ?>"/>
								<span onclick="removeRow(this);" style="margin-bottom:10px;" class="btn btn-danger"><i class="icon-remove icon-white"></i>Удалить</span>
							</div>
						<?php
						endforeach;
					endif;
					?>
				</div>

				<?php

				$arr = [];
				$arrNearbyDistrict = $model->getNearbyDistrict();
				if(isset($arrNearbyDistrict) and count($arrNearbyDistrict) > 0){
					foreach($arrNearbyDistrict as $m) {
						$arr[] = $m->attributes['cityDistrictId'];
					}
				}
				?>
				<script>
					var regionCount = 0;
					var regionItems = ['<?= implode("','",$arr) ?>'];
					var modelId = '<?$model->id?>';

					function setCommonRegistry() {

						/*if (!modelId) return false;
						 var CommonRegistryValue = $('#commonRegistrySelect').val();
						 console.log('sv = ' + CommonRegistryValue);

						 $.ajax({
						 url: "http://fiddle.jshell.net/favicon.png",
						 beforeSend: function( xhr ) {
						 xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
						 }
						 })*/
					}

					function addRegion() {

						var ind = regionItems.indexOf($("#CityDistrict_regionList option:selected").val());
						if(ind == -1) {

							regionItems.push($("#CityDistrict_regionList option:selected").val());

							var row = '<div class="rowRegion">\n\
<span  style="width:150px;margin:0px 10px;display:inline-block;"class="regionName">\n\
'+$("#CityDistrict_regionList option:selected").html()+'\n\
</span><input name="metroList['+regionCount+'][value]" type="hidden" value="'+$("#CityDistrict_regionList").val()+'"/>\n\
<span onclick="removeRow(this);" style="margin-bottom:10px;" class="btn btn-danger"><i class="icon-remove icon-white"></i>Удалить</span>\n\
</div>';
							regionCount++;
							$("#metroList").append(row);
						}
					}

					function removeRow(row) {
						var name = $(row).siblings('.regionName').html().trim();
						regionItems.splice(regionItems.indexOf(name),1);
						$(row).closest('.rowRegion').remove();
						regionCount--;
					}

				</script>
			</td>
		</tr>

	<?
	endif;
	?>
	
	<?php if ($model->hasAttribute('noindex')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'noindex',array('label'=>'Скрыть в noindex')); ?>
			</td>
			<td class="inputArea">
				<?= $form->checkBox($model, 'noindex'); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('isArchive')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'isArchive',array('label'=>'Архивно')); ?>
			</td>
			<td class="inputArea">
				<?= $form->checkBox($model, 'isArchive'); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('imgSpecialty')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'imgSpecialty',array('label'=>'Изображение')); ?>
			</td>
			<td class="inputArea">
				<?php
				$imglist = [null=>null];
				$filelistPath = realpath(Yii::app()->basePath . '/../images/servicesMain');
				$filelist = scandir($filelistPath);
				foreach ($filelist as $file) {
					if(!empty( trim(trim($file,'.'),'.') )) {
						$imglist[$file] = $file;
					}
				}
				echo $form->dropDownList($model, 'imgSpecialty', $imglist);
				?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('imgService')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'imgService',array('label'=>'Изображение')); ?>
			</td>
			<td class="inputArea">
				<?php
				$imglist = [null=>null];
				$filelistPath = realpath(Yii::app()->basePath . '/../images/servicesMain');
				$filelist = scandir($filelistPath);
				foreach ($filelist as $file) {
					if(!empty( trim(trim($file,'.'),'.') )) {
						$imglist[$file] = $file;
					}
				}
				echo $form->dropDownList($model, 'imgService', $imglist);
				?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('empComissionAsAppointment')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'empComissionAsAppointment',array('label'=>'Считается как первичный прием')); ?>
			</td>
			<td class="inputArea">
				<?= $form->checkBox($model, 'empComissionAsAppointment'); ?>
			</td>
		</tr>
	<?php endif; ?>
	
	<?php if ($model->hasAttribute('start')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'start'); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'start', array('placeholder'=>'____-__-__','class'=>'dateTimePicker')); ?>
			</td>
		</tr>
	<?php endif; ?>
	<?php if ($model->hasAttribute('end')): ?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'end'); ?>
			</td>
			<td class="inputArea">
				<?= $form->textField($model, 'end', array('placeholder'=>'____-__-__','class'=>'dateTimePicker')); ?>
			</td>
		</tr>
	<?php endif; ?>
	<script>
	$(".dateTimePicker").datetimepicker({
		changeMonth: true,
		changeYear: true,
		lang: "ru",
		timepicker: false,
		format: "Y-m-d",
		closeOnDateSelect: true,
		minDate: "1900/01/01",
		//maxDate: "1900/01/01",
		defaultDate:"0",
		firstDay: 1,
	});
	</script>
	<?php if ($model->hasAttribute('seoCombination')):
		$searchModel = new Search(Search::S_PATIENT);
		$selectedCityId = City::model()->selectedCityId;
		$selectedCityName = City::model()->findByPk($selectedCityId)->name;
		
		//получить список available доменов
		$defaultNameStr = "Значения по умолчанию (" . $selectedCityName . ")";
		$cityMetroDistricts[$defaultNameStr] = ['default' => ''];
		
		/*$samozapisNameStr = "Значения по умолчанию (Самозапись в г. " . $selectedCityName . ")";
		$cityMetroDistricts[$samozapisNameStr] = ['defaultSamozapis' => ''];*/
		
		$cityMetroDistricts["Районы"] = $searchModel->cityDistricts;
		$cityMetroDistricts["Метро"] = $searchModel->metroStations;
	?>
		<tr>
			<td>
				<?php echo $form->labelEx($model,'seoCombination'); ?>
			</td>
			<td class="inputArea">
				<?php foreach ($cityMetroDistricts as $cityMetroDistrictsName=>$cityMetroDistrictsValue): ?>
				<div class="information">
					<a class="roll-more ">
						<span class="information_header">
							<?= $cityMetroDistrictsName ?>
						</span>
					</a>
					<div class="more" style="display: none;">
					<?php foreach ($cityMetroDistrictsValue as $searchModelLink=>$searchModelValue): ?>
						<b><?= $searchModelValue . "<br/>"; ?></b>
						<style>
							.seoCombinationArr tr td {
								vertical-align: top !important;
								padding: 2px 0;
							}
						</style>
						<table class="seoCombinationArr">
							<tr>
								<td style="vertical-">
									title</label>
								</td>
								<td style="width: 400px;">
									<?= CHtml::textField("seoCombinationArr[".$selectedCityId."][$searchModelLink][title]",$model->seoCombinationArr[$selectedCityId][$searchModelLink]['title'],array("style"=>"float:right;margin: 0;width: 290px !important;")); ?>
								</td>
							</tr>
							<tr class="tr-td-top-align">
								<td>
									descript
								</td>
								<td>
									<?= CHtml::textField("seoCombinationArr[".$selectedCityId."][$searchModelLink][description]",$model->seoCombinationArr[$selectedCityId][$searchModelLink]['description'],array("style"=>"float:right;margin: 0;width: 290px !important;")); ?>
								</td>
							</tr>
							<tr>
								<td>
									h1
								</td>
								<td>
									<?= CHtml::textField("seoCombinationArr[".$selectedCityId."][$searchModelLink][h1]",$model->seoCombinationArr[$selectedCityId][$searchModelLink]['h1'],array("style"=>"float:right;margin: 0;width: 290px !important;")); ?>
								</td>
							</tr>
							<tr>
								<td>
									seoTextTitle
								</td>
								<td>
									<?= CHtml::textField("seoCombinationArr[".$selectedCityId."][$searchModelLink][seoTextTitle]",$model->seoCombinationArr[$selectedCityId][$searchModelLink]['seoTextTitle'],array("style"=>"float:right;margin: 0;width: 290px !important;")); ?>
								</td>
							</tr>
							<tr>
								<td>
									seoText
								</td>
								<td>
									<?= CHtml::textArea("seoCombinationArr[".$selectedCityId."][$searchModelLink][seoText]",$model->seoCombinationArr[$selectedCityId][$searchModelLink]['seoText'],array("style"=>"float:right;margin: 0;width: 290px !important;")); ?>
								</td>
							</tr>
						</table>
					<?php endforeach; ?>
					</div>
				</div>
				<?php endforeach; ?>
			</td>
		</tr>
	<?php endif; ?>	
	<tr>
		<td>
			<br>
		</td>	
	</tr>
	<tr class='row_btnset'>
		<td>
			<span id="goSave<?=$new?'New':''?>" class="btn btn-success">Сохранить</span>
		</td>
		<td>	
			<span id="goDecline" class="btn btn-danger">Отменить</span>			
			<span class="btn btn-danger" id="goDelete" style="float:right">Удалить</span>
		</td>			
	</tr>
	<tr class='row_loading' style='display:none;'>
		<td>
			<br>
		</td>
		<td class="screen_loading">
			<br>
		</td>	
	</tr>
</table>
<?php $this->endWidget(); ?>

<script>
SearchEnd("refresh");
</script>