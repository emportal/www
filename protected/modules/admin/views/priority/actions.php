<?php
/* @var $this ClinicReportController */

?>

<table class="table-bordered table-condensed table-striped">
	<tr>
		<th>Название акции</th>
		<th>Компания</th>
		<th>Адрес</th>
		<th>Статус</th>
		<th>Старт</th>
		<th>Окончание</th>
		<th>Бессрочно</th>
		<th>Приоритет</th>
	</tr>
	
	<?php foreach($actions as $act):?>
	<tr>
		<td>
			<?=$act->name; ?>
		</td>
		<td>
			<?=$act->company->name; ?>
		</td>
		<td>
			<?=$act->address->name; ?>
		</td>
		<td>
			<?=$act->publish ? 'опубликовано' : 'не опубликовано'; ?>
		</td>
		<td>
			<?=$act->start != '0000-00-00 00:00:00' ? $act->start : '' ?>
		</td>
		<td>
			<?=$act->end != '0000-00-00 00:00:00' ? $act->end : ''?>
		</td>
		<td>
			<?=$act->unlimited ? 'бессрочно' : ''; ?>
		</td>
		<td>
			<?=$act->priority ? 'приоритет' : ''; ?>
		</td>
	</tr>
	<?php endforeach;?>
</table>