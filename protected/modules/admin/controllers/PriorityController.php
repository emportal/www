<?php

class PriorityController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('index','actions'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}
	
	public function actionIndex() {
		
		
		$this->render('index',[
			
		]);
	}
	
	public function actionActions() {
		$actions = Action::model()->published()->active()->findAll([
			'order'=>'t.name ASC',
			'with' => [
				'company' => [
					'together' => true
				],
				'address' => [
					'together' => true
				],
				'priority' => [
					'together' => true
				],
			]
		]);
		
		$this->render('actions',[
			'actions' => $actions
		]);
	}
	
	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}
