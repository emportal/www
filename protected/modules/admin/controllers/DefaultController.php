<?php

class DefaultController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow',
				'actions' => array('login', 'error', 'logout' ),
				'users' => array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionIndex() {
		$this->redirect(array('news/index'));
		$this->render('index');
	}

	public function actionLogin() {
		if (!Yii::app()->user->isGuest)
			$this->redirect('index', true);

		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
				$this->redirect(array('index'));
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}

	public function actionGallery() {
		$albums = Album::model()->findAll();
		$name = Yii::app()->request->getParam('name');

		/* Истинно, если не указан определенный альбом */
		if (empty($name)) {
			
		} else {
			$album = Album::model()->findByAttributes([
				'name' => $name
			]);
			if (!$album) {
				unset($name);
			}
		}

		$this->render('gallery', [
			'albums' => $albums,
			'album' => $album,
			'name' => $name
		]);
	}

	public function actionAddAlbum() {
		$model = new Album();
		$model->syncThis = false;
		$data = $_POST[get_class($model)];
		if ($data) {
			$model->attributes = $data;
			if ($model->save()) {
				$this->redirect("gallery");
			}
		}
		$this->render('addAlbum', [
			'model' => $model
		]);
	}

	public function actionDelAlbum() {
		Yii::import("application.modules.gallery.models.*");
		
		$name = Yii::app()->request->getParam('name');
		$model = Album::model()->findByAttributes([
			'name' => $name,
		]);
		
			

		if (!empty($model)) {
		
			$model->syncThis = false;
		
			$gallery = Gallery::model()->findByAttributes([
				'owner' => 'gallery' . $model->name
			]);
		
			$gallery->delete();
			if ($model->delete()) {
				$this->redirect("gallery");
			}
		}
		echo 2;
		$this->redirect("gallery");
		Yii::app()->end();
	}

	public function actionLogout() {
		Yii::app()->user->logout();

		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionPhpinfo() {
		phpinfo();
	}

	public function actionStatistics() {
		$command = Yii::app()->db->createCommand()
		->select("ds.name, COUNT(ds.name) `count`")
		->from("doctorSpecialty ds")
		->join("specialtyOfDoctor sod" , "sod.doctorSpecialtyId = ds.id")
		->join("doctor d" , "d.id = sod.doctorId")
		->join("appointmentToDoctors atd" , "atd.doctorId = d.id")
		->where("DATE_FORMAT(DATE(atd.createdDate),'%Y-%m') = DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 MONTH),'%Y-%m')
				AND atd.statusId != ".AppointmentToDoctors::DISABLED)
		->group("ds.name")
		->order("count DESC");
		$specialties = $command->queryAll();
		$this->render('statistics', [
			'specialties' => $specialties
		]);
	}
	
	public function actionTariffOptions() {
		if(Yii::app()->request->getRequestType() == "POST") {
			$success = false;
			$tariffOptionTypeId = $_REQUEST['tariffOptionTypeId'];
			$salesContractTypeId = $_REQUEST['salesContractTypeId'];
			$status = $_REQUEST['status'];
			$list = TariffOptionTypeOfSalesContractType::model()->findAllByAttributes(["tariffOptionTypeId"=>$tariffOptionTypeId, "salesContractTypeId"=>$salesContractTypeId]);
			if($status && empty($list)) {
				$tariffOfContract = new TariffOptionTypeOfSalesContractType;
				$tariffOfContract->tariffOptionTypeId = $tariffOptionTypeId;
				$tariffOfContract->salesContractTypeId = $salesContractTypeId;
				if($tariffOfContract->save()) {
					$success = true;
				}
			} else {
				foreach ($list as $tariffOfContract) {
					if($tariffOfContract->delete()) {
						$success = true;
					}
				}
			}
            $response = [
            	'success' => $success,
                'errors' => [],
            ];
            echo CJSON::encode($response);
		} else {
	        $tariffOptionTypes = TariffOptionType::model()->findAll();
	        $salesContractTypes = SalesContractType::model()->findAll();
	        $this->layout = null;
			$this->render('tariffOptions', [
				'tariffOptionTypes' => $tariffOptionTypes,
				'salesContractTypes' => $salesContractTypes
			]);
		}
	}

	public function actionApiTest() {
		$this->renderPartial('apiTest');
	}
}
