<?php

class ClinicReportController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('index', 'view', 'changeLog', 'blockedusers', 'activeusers', 'subscribers', 'ClinicOfferAcceptedXLS', 'doubleOfDoctor', 'getPromoCodeInfo'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionView($id) {
		$model = Address::model()->findByLink($id);
		$mainId = $model->company->addressId;
		$actions = AgrAction::model()->findAll("addressId=:id", array(':id' => $model->id));
		$address = AgrAddress::model()->findByLink($id);
		$company = AgrCompany::model()->find("id=:id", array(':id' => $model->company->id));
		$email = AgrEmail::model()->find("addressId=:id", array(':id' => $model->id));
		$phones = AgrPhone::model()->findAll("addressId=:id", array(':id' => $model->id));
		$ref = AgrRef::model()->find("ownerId=:id", array(':id' => $model->id));

		$activeCompany = Company::model()->find("id=:id", array(':id' => $model->company->id));
		$activeEmail = Email::model()->find("addressId=:id", array(':id' => $model->id));
		$activePhones = Phone::model()->findAll("addressId=:id", array(':id' => $model->id));
		$activeRef = Ref::model()->find("ownerId=:id", array(':id' => $model->id));



		$doctors = AgrDoctor::model()->with(array(
					'currentPlaceOfWork' => array(
						'together' => true
			)))->findAll("currentPlaceOfWork.addressId=:id", array(':id' => $model->id));
		$keys = array();
		foreach ($doctors as $d) {
			$keys[] = $d->id;
		}
		$criteria = new CDbCriteria();
		$criteria->compare('currentPlaceOfWork.addressId', $model->id);
		$criteria->addInCondition('t.id', $keys);

		$activeDoctors = Doctor::model()->with(array(
					'currentPlaceOfWork' => array(
						'together' => true
			)))->findAll($criteria);

		foreach ($activeDoctors as $d) {
			$activeDoctorsArr[$d->id] = $d;
		}
		foreach ($doctors as $d) {
			$doctorsArr[$d->id] = $d;
		}


		$this->render('view', array(
			'model' => $model,
			'actions' => $actions,
			'company' => $company,
			'email' => $email,
			'phones' => $phones,
			'ref' => $ref,
			'doctors' => $doctorsArr,
			'activeDoctors' => $activeDoctorsArr,
			'activeCompany' => $activeCompany,
			'activePhones' => $activePhones,
			'activeRef' => $activeRef,
			'activeEmail' => $activeEmail,
			'mainId' => $mainId,
			'address' => $address,
		));
	}

	public function actionIndex() {

		$criteria = new CDbCriteria();
		$criteria->select = 't.name,t.link';
		$criteria->with = array(
			'userMedicals' => array(
				'select' => false,
				'together' => true,
				'joinType' => 'INNER JOIN'
			),
			'company' => array(
				'scopes' => 'clinicByAddress',
				'select' => 'company.name,company.addressId',
				'together' => true,
				'with' => array(
					'agr_companyCount' => array('together' => true),
				)
			),
			'agr_actionCount' => array('together' => true),
			'agr_phoneCount' => array('together' => true),
			'agr_refCount' => array('together' => true),
			'agr_emailCount' => array('together' => true),
			'agr_companyLicenseCount' => array('together' => true),
			'agr_doctorCount' => array('together' => true),
			'agr_address'
		);
		$criteria->order = 'company.name ASC';
		if (!Yii::app()->request->getParam('published')) {
			$published = 2;
		} else {
			$published = (int) Yii::app()->request->getParam('published');
		}

		$data = Address::model()->findAll($criteria);



		//$criteria->order = 'status ASC';


		/* $dataProvider = new CActiveDataProvider('Address', array(
		  'criteria' => $criteria,
		  'sort' => array(
		  'attributes' => array(
		  'company.name' => array(
		  'asc' => 'company.name',
		  'desc' => 'company.name DESC',
		  ),
		  't.name' => array(
		  'asc' => 't.name',
		  'desc' => 't.name DESC',
		  ),
		  '*',
		  ),
		  'defaultOrder'=>'company.name'
		  ),
		  'pagination' => array(
		  'pageSize' => 100,
		  ),
		  ));
		  $dataProvider->data = null; */
		$this->render('index', array('data' => $data, 'published' => $published/* ,'dataProvider' => $dataProvider */));
	}

	public function actionWithAccess() {
		$this->render('with_access');
	}
	
	public function actionActiveUsers() {
		$model = User::model();
		$param = Yii::app()->request->getParam(get_class($model));
		$model->attributes = $param;
		if(isset($param['balance'])) $model->balance = $param['balance'];
		if(isset($param['registerPromoCodeUsed'])) $model->registerPromoCodeUsed = $param['registerPromoCodeUsed'];
		$dataProvider = $model->search();
		#$dataProvider->criteria->addCondition("t.email is NULL OR (t.email NOT LIKE 'klka1@live.ru' AND t.email NOT LIKE '%@eldario%' AND t.email NOT LIKE 'a.f.dyachkov@gmail.com')");
		$dataProvider->pagination = ['pageSize' => '10'];
		$dataProvider->sort = [
				'defaultOrder'=>'t.dateRegister DESC'
		];
		if(!empty($_REQUEST['Right']['name'])) {
			$dataProvider->criteria->with = [
				'rights' => [
					'together' => true,
					'with' => [
						'rightType' => [
							'together' => true,	
						],
					],
				],
			];
			$dataProvider->criteria->compare('rightType.name',$_REQUEST['Right']['name'],false);
		}

		$this->render('users', array(
			'dataProvider' => $dataProvider,
		));
	}
	
	public function actionGetPromoCodeInfo() {
		$result = ['status'=>'ERROR'];
		$userId = Yii::app()->request->getParam('userId');
		$user = User::model()->findByPk($userId);
		$logVarious = LogVarious::model()->findAllByAttributes(['type'=>LogVarious::TYPE_PROMOCODE]);
		if(is_array($logVarious)) {
			$result['status'] = 'OK';
			$result['result'] = [];
			foreach ($logVarious as $logValue) {
				try {
					$resultItem = json_decode($logValue->value);
					if($resultItem->userId === $userId) {
						$result['promocodes'][] = ['promocode'=>$resultItem->promocode, 'createdDate'=>$logValue->createdDate];
					}
				} catch (Exception $e) { }
			}
			if(trim(strval($user->refererId)) !== "") {
				if($referer = User::model()->findByPk($user->refererId)) {
					$result['referer'] = $referer->name;
				}
			}
		}
		echo json_encode($result);
	}
	
	public function actionSubscribers() {
		$subscribers = [];
		$users = User::model()->findAll(['condition'=>'email IS NOT NULL', 'order'=>'name']);
		foreach ($users as $user) {
			if($user->getSubscriber()) {
				$subscribers[] = [$user->name, $user->email];
			}
		}
		if($downloadCSV = (int) Yii::app()->request->getParam('downloadCSV')) {
			FileMaker::createCSV("subscribers", $subscribers, ["Имя","e-mail"], null, false);
		} else {
			echo "<table>" . PHP_EOL;
			foreach ($subscribers as $row) {
				echo "	<tr>" . PHP_EOL;
				foreach ($row as $value) {
					echo "		<td>$value</td>" . PHP_EOL;
				}
				echo "	</tr" . PHP_EOL;
			}
			echo "</table>" . PHP_EOL;
		}
	}

	public function actionBlockedUsers() {
		$criteria = new CDbCriteria();

		$criteria->condition = "isBlockedAppointment = " . User::APPOINT_BLOCKED . " AND UNIX_TIMESTAMP() - blockedTime > " . (60 * 30);

		$dataProvider = new CActiveDataProvider('User', array(
			'criteria' => $criteria
		));

		$this->render('blockedusers', array(
			'dataProvider' => $dataProvider
		));
	}

	public function actionClinicOfferAcceptedXLS() {
		$criteria = new CDbCriteria();
		$criteria->with = [
			'company' => [
				'together' => true,
			],
			'address' => [
				'together' => true,
			]
		];
		$criteria->compare('t.agreementNew', 1);
		$criteria->order = 'company.name ASC';
		$cabinets = UserMedical::model()->findAll($criteria);
		$rows = [];
		foreach ($cabinets as $cab) {
			$rows[] = [$cab->company->name, $cab->address->street . ', ' . $cab->address->houseNumber];
		}

		$outstream = fopen("php://temp", 'r+');
		fputcsv($outstream, array('Название компании', 'Адрес'), ';');

		foreach ($rows as $row) {
			fputcsv($outstream, $row, ';');
		}

		rewind($outstream);
		Yii::app()->request->sendFile('Клиники с подтвержденной офертой.csv', mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
		fclose($outstream);
		Yii::app()->end();
	}

	public function actionChangeLog() {
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'address' => array(
				'together' => true,
				'joinType' => 'INNER JOIN',
				'with' => [
					'city' => [
						'together' => true,
					],
				],
			),
			'company' => array(
				'together' => true,
				'joinType' => 'INNER JOIN'
			),
		);
		$criteria->addCondition('datePublications >= (now() - INTERVAL 60 DAY)','AND');
		
		$dataProvider = new CActiveDataProvider('LogAgr', array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 100,
			),
			'sort' => array(
				'attributes' => array(
					'datePublications' => array(
						'asc' => 'datePublications ASC',
						'desc' => 'datePublications DESC',
					),
				),
				'defaultOrder'=>'datePublications DESC'
			),
		));
		//Yii::app()->request->getParam('published');
		$this->render('changelog', array('dataProvider' => $dataProvider));
	}
	
	public function actionDoubleOfDoctor() {
		$command = Yii::app()->db->createCommand("
			SELECT t.name FROM
				(
					SELECT DISTINCT t.id, t.name FROM doctor t
					JOIN placeOfWork pow ON pow.doctorId = t.id
					JOIN company c ON c.id = pow.companyId
					JOIN address a ON a.id = pow.addressId
					JOIN userMedical um ON um.addressId = a.id
					WHERE t.deleted=0 AND a.isActive=1 AND c.removalFlag=0 AND um.agreementNew=1 AND a.samozapis=0
				) t
			GROUP BY t.name HAVING COUNT(t.name)>1
		");
		$namesTmp = $command->queryAll();
		$names = [];
		foreach ($namesTmp as $value) {
			$names[] = $value["name"];
		}
		
		$criteria = new CDbCriteria();
		$criteria->with = [
			"placeOfWorks" => [
				"together" => true,
				"with" => [
					"address" => [
						"together" => true,
						"with" => [
							"userMedicals" => [
								"together" => true,
							],
							"city" => [
								"together" => true,
							],
						]
					],
					"company" => [
						"together" => true,
					],
				]
			]
		];
		$criteria->addCondition("t.deleted=0 AND address.isActive=1 AND company.removalFlag=0 AND userMedicals.agreementNew=1 AND address.samozapis=0");
		$criteria->addInCondition("t.name",$names);
		$criteria->order = "city.name DESC, company.name DESC, t.name ASC";
		//$doctors = Doctor::model()->findAll($criteria);

		$dataProvider = new CActiveDataProvider(
				Doctor::model(),
				array(
						'criteria' => $criteria,
				)
		);
		$this->render('doubleOfDoctor', [
				'dataProvider' => $dataProvider
		]);
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}
