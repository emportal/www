<?php

class HandbookController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('index', 'list', 'editBlock','submitData','deleteData'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	private $_model;

	public function actionIndex() {
		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionList() {
		$table = Yii::app()->request->getParam('table');
		$model = ActiveRecord::model($table);
		$criteria = new CDbCriteria();
		$criteria->select = 't.id,t.name';
		$criteria->group = 't.id';
		$criteria->order = 't.name ASC';
		if($model->hasAttribute('cityId')) {
			$criteria->compare('cityId', City::model()->selectedCityId);
		}
/* */
		if($table == "DoctorSpecialty") {
			$SScriteria = new CDbCriteria();
			$SScriteria->group = 't.id';
			$SScriteria->compare('samozapis',0);
			$SSmodel = ShortSpecialtyOfDoctor::model()->findAll($SScriteria);
			$ids = [];
			foreach ($SSmodel as $value) {
				$ids[] = $value->id;
			}
			$SScriteria = new CDbCriteria();
			$SScriteria->select = 't.specialtyId';
			$SScriteria->addNotInCondition('t.specialtyId', $ids);
			$SSmodel = ExtSpeciality::model()->findAll($SScriteria);
			$ids = [];
			foreach ($SSmodel as $value) {
				$ids[] = $value->specialtyId;
			}
			$criteria->addNotInCondition('t.id', $ids);
		}
		
		$models = $model->findAll($criteria);
		$this->renderPartial('_list', array(
			'models' => $models
		));
	}

	public function actionEditBlock() {
		$table = Yii::app()->request->getParam('table');
		$id = Yii::app()->request->getParam('id');
		$new = Yii::app()->request->getParam('new');
		if($new) {
			$model = new $table();
		} else {
			$model = ActiveRecord::model($table)->findByPk($id);		
		}
		
		$this->renderPartial('_form', array(
			'model' => $model,
			'new' => $new
		));
	}

	public function actionDeleteData() {
		try {
			$table = Yii::app()->request->getParam('table');
			$id = Yii::app()->request->getParam('id');
			$anyway = Yii::app()->request->getParam('anyway', false);
			if($table === 'Service') {
				if(boolval($anyway)) {
					AddressServices::model()->deleteAll("serviceId=:serviceId",[":serviceId"=>$id]);
				} else {
					$addressServicesCount = AddressServices::model()->count("serviceId=:serviceId",[":serviceId"=>$id]);
					if($addressServicesCount > 0) {
						echo CJSON::encode(['success'=>false, 'used'=>true, 'text'=>"Эта услуга используется пользователями портала (в ЛКК) и после удаления станет повсеместно недоступна. Количество случаев использования: ".$addressServicesCount.". Продолжить удаление?"]);
						return;
					}
				}
			}
			$model = ActiveRecord::model($table)->findByPk($id);	
			if(!$model OR $model->delete()) {
				echo CJSON::encode(['success'=>true]);
				MyTools::StartCommand("RemoveCache");
			} else {
				echo CJSON::encode(['success'=>false, 'text'=>print_r($model->getErrors(),true)]);
			}
		} catch (Exception $e) {
			echo CJSON::encode(['success'=>false, 'text'=>($e->getMessage())]);
		}
	}
	public function actionSubmitData() {
		$table = Yii::app()->request->getParam('table');
		$id = Yii::app()->request->getParam('id');
		$data = Yii::app()->request->getParam('data');
		$new = Yii::app()->request->getParam('new');

		if($new) {
			$model = new $table();
			if($model->hasAttribute('cityId')) {
				$model->cityId = City::model()->selectedCityId;
			}
		} else {
			$model = ActiveRecord::model($table)->findByPk($id);	
		}
		NearbyDistrict::model()->deleteAll("cityDistrictId = :id", array(':id' => $model->attributes['id']));
		$parameters = [];
		foreach($data as $attr) {

			$a = str_replace($table."[",'[',$attr['name']);
			if(mb_stripos($a, '[') === 0 && mb_stripos($a, ']') === mb_strlen($a)-1) {
				$a = ltrim($a, '[');
				$a = rtrim($a, ']');
			}
			$result = MyTools::stringToArgs([$a=>$attr['value']]);

			foreach ($result as $key=>$value) {
				if($key == 'metroList') {
					$team = new NearbyDistrict();
					$team->cityDistrictId = $model->attributes['id'];
					$team->cityNearbyDistrictId = array_shift($value)['value'];
					$team->save();
					continue;
				}
				if(is_array($value) && is_array($model->{$key})) {
					$model->{$key} = array_replace_recursive($model->{$key}, $value);
				} else {
					if($model->hasAttribute($key)) {
						$model->{$key} = $value;
					} else {
						if(method_exists($model,'setParameter')) {
							$model->setParameter($key, $value, City::model()->selectedCityId);
							continue;
						}
					}
				}
			}
		}

		if($model->save()) {
			echo CJSON::encode(['success'=>true, 'id'=>$model->id, 'data'=>$parameters,]);
			MyTools::StartCommand("RemoveCache");
		} else {
			echo CJSON::encode($model->getErrors());
		}
	}
	
	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}
