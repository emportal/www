<?php

/**
 * 	@var $instance ActiveRecord
 */
class AjaxController extends Controller {
	//public $layout = 'empty';
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'cities' => array(
				'class' => 'AutoCompleteAction',
				'model' => 'City',
				'attributeValue' => 'name',
			),
		);
	}

	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules() {
		return array(
				array('allow',
						'users' => array('@'),
				),
				array('deny', // deny all users
						'users' => array('*'),
				),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionAcceptAction() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAction::model()->find("id=:id", array(':id' => $id));
		$model = Action::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			if (!$model) {
				$model = new Action();
			}
			$model->attributes = $agrModel->attributes;

			$model->link = "";
			if ($model->save()) {
				$agrModel->delete();
			} else {
				
			}



			$error = 0;
		}

		/*
		  if($agrModel && $model) {
		  $model->attributes=$agrModel->attributes;
		  if($model->save()) {
		  $agrModel->delete();
		  $error=0;
		  }
		  }
		 */

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptEmail() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrEmail::model()->find("addressId=:id", array(':id' => $id));
		$model = Email::model()->find("addressId=:id", array(':id' => $id));
		//Если пустая модель, значит новую запись создаем
		if (!$model) {
			$model = new Email();
		}
		if ($agrModel && $model) {
			$model->attributes = $agrModel->attributes;
			//Если пусто, значит при подтверждении удаляем
			if ($agrModel->name == "") {
				$model->delete();
				$agrModel->delete();
				$error = 0;
				//Если есть имя, значит заменяем
			} else {
				if ($model->isNewRecord) {
					$model->link = "";
					$model->save();
				} else {
					if ($model->save()) {
						$agrModel->delete();
					}
				}
				$error = 0;
			}
		}


		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptPhones() {
		/* id -> addressId */
		$id = Yii::app()->request->getParam('id');
		$error = 1;
		if ($id) {
			$ph = Phone::model()->findAll("addressId=:id", array(':id' => $id));
			foreach ($ph as $p) {
				PhoneCompanyUnit::model()->deleteAll("phoneId = :id", array(':id' => $p->id));
				$p->delete();
			}

			$agrModel = AgrPhone::model()->findAll("addressId=:id", array(':id' => $id));

			foreach ($agrModel as $phone) {
				$phone->syncThis = false;
				if ($phone->name == "remove") {
					$phone->delete();
					continue;
				}
				$m = new Phone();
				$m->attributes = $phone->attributes;
				$m->save();
				$phone->delete();
			}
			$error = 0;
		} else {
			$response['errors'][] = "id not found";
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptRef() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrRef::model()->find("ownerId=:id", array(':id' => $id));
		$model = Ref::model()->find("ownerId=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if (!$model) {
			$model = new Ref();
		}
		if ($agrModel && $model) {
			$model->attributes = $agrModel->attributes;
			//Если пусто, значит при подтверждении удаляем
			if ($agrModel->refValue == "") {
				$model->delete();
				$agrModel->delete();
				$error = 0;
				//Если есть имя, значит заменяем
			} else {
				if ($model->isNewRecord) {
					$model->link = "";
					$model->save();
					$agrModel->delete();
				} else {
					if ($model->save()) {
						$agrModel->delete();
					}
				}
				$error = 0;
			}
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptCompany() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrCompany::model()->find("id=:id", array(':id' => $id));
		$model = Company::model()->find("id=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if ($agrModel) {
			if (!$model) {
				$model = new Company();
			}
			$model->attributes = $agrModel->attributes;
			if ($model->isNewRecord) {
				$model->link = "";
				$model->save();
			} else {
				$model->save();
			}
			$agrModel->delete();

			$error = 0;
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptAddress() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAddress::model()->find("id=:id", array(':id' => $id));
		$model = Address::model()->find("id=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if ($agrModel) {
			if (!$model) {
				$model = new Address();
			}
			$model->attributes = $agrModel->attributes;
			if ($model->isNewRecord) {
				$model->link = "";
				$model->save();
			} else {
				$model->save();
			}
			$agrModel->delete();

			$error = 0;
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptExpert() {
		
		$id = Yii::app()->request->getParam('id');
		$addressId = Yii::app()->request->getParam('addressId');
		$error = 1;

		/* Перенос главной модели */
		$doctor = Doctor::model()->find("id=:id", array(':id' => $id));
		$doctorDeleted = Yii::app()->db->createCommand()
				->select('deleted')
				->from(Doctor::model()
						->tableName())
				->where('id=:id', [':id' => $id])
				->queryScalar();

		if ($doctorDeleted) {
			$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));

			AgrDoctorScientificDegree::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrSpecialtyOfDoctor::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrDoctorEducation::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrPlaceOfWork::model()->deleteAll("doctorId=:id", array('id' => $id));
			AgrDoctorServices::model()->deleteAll("doctorId=:id", array('id' => $id));

			if ($agrdoctor) {
				$agrdoctor->syncThis = false;
				$agrdoctor->delete();
				$error = 0;
			}


			if (!$error) {
				$response['success'] = true;
			} else {
				$response['success'] = false;
			}
			echo CJSON::encode($response);
			Yii::app()->end();
		}


		$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));
		if ($agrdoctor) {
			if (!$doctor) {
				$doctor = new Doctor();
				$doctor->link = str_pad((intval(Yii::app()->db->createCommand()->select("max(`link`)")->from($doctor->tableName())->queryScalar()) + 1), 9, '0', STR_PAD_LEFT);
			}
			$doctor->attributes = $agrdoctor->attributes;
			$doctor->name = implode(' ', array($doctor->surName, $doctor->firstName, $doctor->fatherName));
			if ($doctor->name && $doctor->isNewRecord || ($doctor->name !== $agrdoctor->name) ) {
				
				$doctor->linkUrl = MyTools::makeLinkUrl($doctor->name);				
				if (Doctor::model()->findByAttributes(['linkUrl' => $doctor->linkUrl])) {
					$count = 0;
					do {
						$count++;
						$newLink = $doctor->linkUrl . '-' . $count;
					} while (Doctor::model()->findByAttributes(['linkUrl' => $newLink]));

					$doctor->linkUrl = $newLink;
				}
			}
			if ($doctor->save()) {
				
				/* сбрасываем кеш у карточки доктора по всем адресам */
				$pagesToRefresh = $doctor->pagesUrls;
				Controller::clearCache($pagesToRefresh);
				
				/* перенос зависимых моделей */
				$dsd = AgrDoctorScientificDegree::model()->findAll("doctorId=:id", array('id' => $id));
				if ($dsd) {
					$vals = DoctorScientificDegree::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($dsd as $d) {
						$new = new DoctorScientificDegree();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$sod = AgrSpecialtyOfDoctor::model()->findAll("doctorId=:id", array('id' => $id));
				if ($sod) {
					$vals = SpecialtyOfDoctor::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($sod as $d) {
						$new = new SpecialtyOfDoctor();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$de = AgrDoctorEducation::model()->findAll("doctorId=:id", array('id' => $id));
				if ($de) {
					$vals = DoctorEducation::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($de as $d) {
						$new = new DoctorEducation();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$pow = AgrPlaceOfWork::model()->findAll("doctorId=:id", array(
					'id' => $id,
				));
				if ($pow) {
					$vals = PlaceOfWork::model()->findAll("doctorId=:id AND addressId=:aId", array(
						'id' => $id,
						'aId' => $addressId
					));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($pow as $d) {
						$new = new PlaceOfWork();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}

				$ds = AgrDoctorServices::model()->findAll("doctorId=:id", array('id' => $id));
				if ($ds) {
					$vals = DoctorServices::model()->findAll("doctorId=:id", array('id' => $id));
					foreach ($vals as $v) {
						$v->delete();
					}
					foreach ($ds as $d) {
						$new = new DoctorServices();
						$new->attributes = $d->attributes;
						$new->save();
						$d->syncThis = false;
						$d->delete();
					}
				}
				$agrdoctor->syncThis = false;
				$agrdoctor->delete();

				$error = 0;
			} else {
				
			}
		}


		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	/* delete */

	public function actionDeclineAction() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAction::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineEmail() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrEmail::model()->find("addressId=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclinePhones() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;
		if ($id) {

			$agrModel = AgrPhone::model()->deleteAll("addressId=:id", array(':id' => $id));

			$error = 0;
		} else {
			$response['errors'][] = "error #01"; // no id
		}


		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineRef() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrRef::model()->find("ownerId=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineCompany() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrCompany::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineAddress() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAddress::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineExpert() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;


		$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));

		AgrDoctorScientificDegree::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrSpecialtyOfDoctor::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrDoctorEducation::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrPlaceOfWork::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrDoctorServices::model()->deleteAll("doctorId=:id", array('id' => $id));

		$doctor = Doctor::model()->findByPk($id);
		if (!$doctor && is_file($agrdoctor->photoPath)) {
			unlink($agrdoctor->photoPath);
		}
		if ($agrdoctor) {
			$agrdoctor->syncThis = false;
			$agrdoctor->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionChangeData() {
		$modelName = $model = $_POST['model'];
		$attribute = $_POST['attribute'];
		$newData = $_POST['newData'];
		$id = $_POST['rowId'];
		if ($attribute == "birthday") {
			$newData = date("Y-m-d", strtotime($newData));
		}
		/* Получаем объект модели через вызов МОДЕЛЬ::model() */
		$reflectionMethod = new ReflectionMethod($model, 'model');
		$model = $reflectionMethod->invoke(null);
		$object = $model->find('id=:id', array(':id' => $id));

		$response = array();
		if ($modelName == "AgrPhone") {
			$arr = explode(" ", $newData);
			$object->setAttribute('countryCode', $arr[0]);
			$object->setAttribute('cityCode', $arr[1]);
			$object->setAttribute('number', $arr[2]);
		}
		//Меняем значение атрибута
		$object->setAttribute($attribute, $newData);
		//проводим валидацию
		$object->syncThis = false;
		$object->validate(array($attribute));
		//Если нет ошибок, сохраняем, иначе выводим ошибки в консоль
		if ($object->getErrors()) {
			$response['error_message'] = $object->getErrors();
			$response['error'] = true;
		} else {
			if ($object->save()) {
				$response['error'] = false;
			} else {
				$response['error'] = true;
				$response['error_message'] = "Не получилось обновить запись";
			}
		}

		/* switch($modelName) {
		  case 'AgrAction':

		  break;
		  case 'AgrCompany':

		  break;
		  case 'AgrPhone':

		  break;
		  case 'AgrEmail':

		  break;
		  case 'AgrRef':

		  break;
		  case 'AgrDoctor':

		  break;
		  } */


		#var_dump($model->attributes);

		echo CJSON::encode($response);
	}

	public function actionUnblockUser($id) {

		$user = User::model()->find("id = :id", array(
			':id' => $id,
		));

		$user->isBlockedAppointment = 0;
		$user->blockedTime = 0;
		if ($user->save()) {
			$response['success'] = true;
		}

		echo CJSON::encode($response);
	}
	
	public function actionChangeCommonRegistry($id) { //id адреса
		
		$CommonRegistryValue = Yii::app()->request->getParam('CommonRegistryValue');
		$model = Address::model()->findByAttributes(['id' => $id]);
		$userMedical = $model->Usermedicals;
		$userMedical->commonRegistry = $CommonRegistryValue;
		$userMedical->update();
	}
	
	public function actionUserRight() {
		$response = array();
		
		$searchAttributes = ['userId'=>$_REQUEST['userId'], 'zRightId'=>$_REQUEST['rightType']];
		if(isset($_REQUEST['value'])) {
			$searchAttributes['value'] = $_REQUEST['value'];
		}
		$right = Right::model()->findByAttributes($searchAttributes);
		
		if($_REQUEST['checked']) {
			if(!$right) {
				$right = new Right();
				$right->userId = $_REQUEST['userId'];
				$right->zRightId = $_REQUEST['rightType'];
				$right->value = @$_REQUEST['value'];
				if($right->save()) {
					$response['status'] = "OK";
					$response['text'] = "Добавил " . @$right->value;
					$addressModel = Address::model()->findByAttributes(["id"=>$right->value]);
					if($addressModel) {
						$response['text'] = $addressModel->company->name . ", " . $addressModel->street . ", " . $addressModel->houseNumber;
					}
				} else {
					$response['status'] = "ERROR";
					$response['text'] = MyTools::errorsToString($right->getErrors());
				}
			} else {
				$response['status'] = "ERROR";
				$response['text'] = "Уже добавлено";
			}
		} else {
			if(!$right) {
				$response['status'] = "ERROR";
				$response['text'] = "Уже удалено";
			} else {
				if($right->delete()) {
					$response['status'] = "OK";
					$response['text'] = "Удалил";
				} else {
					$response['status'] = "ERROR";
					$response['text'] = MyTools::errorsToString($right->getError());
				}
			}
		}
		
		echo CJSON::encode($response);
	}
	
	public function actionAdminChangesCount() {
		$newsCriteria = new CDbCriteria();
		$newsCriteria->scopes = 'clinic';
		$newsCriteria->addCondition('clinicId IS NOT NULL');
		$newsCriteria->compare('publish', 0);
		
		#$countRequest = ServicesRequest::model()->count('`t`.`status`<1 OR `t`.`status` IS NULL');
		#$countBlockedUsers = User::model()->count("isBlockedAppointment = " . User::APPOINT_BLOCKED . " AND UNIX_TIMESTAMP() - blockedTime > " . (60 * 30));
		
		$criteria = new CDbCriteria();
		$criteria->select = 't.name,t.link';
		$criteria->with = array(
				'userMedicals' => array(
						'select' => false,
						'together'=>true,
						'joinType' => 'INNER JOIN'
				),
				'company' => array(
						'scopes' => 'clinicByAddress',
						'select'=>'company.name,company.addressId',
						'together' => true,
						'with' => array(
								'agr_companyCount' =>array('together'=>true),
						)
				),
				'agr_actionCount' => array('together' => true),
				'agr_phoneCount' => array('together' => true),
				'agr_refCount' => array('together' => true),
				'agr_emailCount' => array('together' => true),
				'agr_companyLicenseCount' => array('together' => true),
				'agr_doctorCount' => array('together' => true),
				'agr_address'
		);
		$data = Address::model()->findAll($criteria);
		
		$clinicCount = 0;
		foreach ($data as $row) {
			if ($row->status) {
				$clinicCount++;
			}
		}
		$stats = [
				'clinicChangesCount' => $clinicCount,
				'reviewChangesCount' => Review::model()->count('statusId = 1'),
				'clinicNewsChangesCount' => News::model()->count($newsCriteria),
				'stat_AppointmentToDoctorsCount' => AppointmentToDoctors::model()->count("statusId<>'".AppointmentToDoctors::DISABLED."'"),
				'stat_AppointmentToDoctorsCountToDay' => AppointmentToDoctors::model()->count("createdDate > '".date('Y-m-d 00:00:00')."' AND statusId <> '".AppointmentToDoctors::DISABLED."'"),
				'stat_AppointmentToDoctorsCountYesterday' => AppointmentToDoctors::model()->count("createdDate < ('".date('Y-m-d 00:00:00')."') AND createdDate > ('".date('Y-m-d 00:00:00')."' - INTERVAL 1 DAY) AND statusId <> '".AppointmentToDoctors::DISABLED."'"),
				'stat_AppointmentToDoctorsCountLastSevenDays' => AppointmentToDoctors::model()->count('createdDate > (NOW() - INTERVAL 1 WEEK) AND statusId<>\''.AppointmentToDoctors::DISABLED.'\''),
				'stat_UsersRegistry' => User::model()->count('status = 1'),
				'stat_ReviewsCount' => Config::model()->findByAttributes(["name" => "Общее количество отзывов"])->value,
		];
		Yii::app()->cache->set('moduleAdminStats', $stats, 6000);
		echo CJSON::encode($stats);
	}
}
