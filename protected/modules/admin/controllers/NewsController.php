<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'create', 'update', 'delete','authors','createAuthor','updateAuthor','deleteAuthor','imageUpload','fileUpload','imageGetJson'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new News;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->mainImage = CUploadedFile::getInstance($model,'mainImage');
			$_POST['News']['link'] = MyTools::convertRussianToTranslit($_POST['News']['name']);
			$model->attributes=$_POST['News'];
			$model->Date = date("Y-m-d H:i:s");
			if($model->save()) {
				MyTools::StartCommand("RemoveCache");
				if(is_object($model->mainImage)) {
					$mainImagePath = 'uploads/blogImage/'.$model->link."_".$model->mainImage->name;
					$model->mainImage->saveAs($mainImagePath);
					$model->mainImage = "/".$mainImagePath;
					$model->update();
				}
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$users = User::model()->findAll("isAuthor = 1 AND deleted = 0");
		$this->render('create',array(
			'model'=>$model,
			'users' => $users,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$oldMainImage = $model->mainImage;
			$model->mainImage = CUploadedFile::getInstance($model,'mainImage');
			$_POST['News']['link'] = MyTools::convertRussianToTranslit($_POST['News']['name']);
			$model->attributes=$_POST['News'];
			if(!$model->mainImage) {
				$model->mainImage = $oldMainImage;
			}
			if($model->save())	{
				MyTools::StartCommand("RemoveCache");
				if(is_object($model->mainImage)) {
					$mainImagePath = 'uploads/blogImage/'.$model->link."_".$model->mainImage->name;
					$model->mainImage->saveAs($mainImagePath);
					$model->mainImage = "/".$mainImagePath;
					$model->update();
				}
				if(isset($_POST['submit_view'])) {
					$this->redirect(array('view','id'=>$model->id));
				} 
				if(isset($_POST['submit_back'])) {
					$this->redirect(Yii::app()->session['newsReturnUrl']);
				} 
			}			
				#
				
		}
		$users = User::model()->findAll("isAuthor = 1 AND deleted = 0");
		$this->render('update',array(
			'model'=>$model,
			'users' => $users,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new News('search');
		$model->unsetAttributes();
		
		Yii::app()->session['newsReturnUrl']=$this->createAbsoluteUrl("/".Yii::app()->urlManager->ParseUrl(Yii::app()->request),$filter ? array('filter'=>$filter): array());
		
		if(isset($_GET['News'])) { $model->attributes=$_GET['News']; }
		$dataProvider = $model->search();
		$dataProvider->pagination = array( 'pageSize'=>20, );
		$dataProvider->sort = array( 'defaultOrder'=>'datePublications DESC, publish DESC' );
		
		$filter = Yii::app()->request->getParam('filter');
		if($filter == 'clinic') {
			$dataProvider->criteria->scopes = 'clinic';
			#$criteria->addCondition('clinicId IS NOT NULL');
			$dataProvider->criteria->with = array(
				'clinic'=>array(
					'together'=>true
				)
			);
		} else {
			
		}
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'filter'=>$filter
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionImageUpload() {
		$path =  str_replace("protected", "", Yii::app()->basePath);
	    $dir = $path . 'uploads'. DIRECTORY_SEPARATOR .'blogImage/';

	    $image=CUploadedFile::getInstanceByName('file');
	    $filename = 'a_'.date('YmdHis').'_'.substr(md5(time()), 0, rand(7, 13)).'.'.$image->extensionName;
	    $path = $dir .$filename;
	    $image->saveAs($dir.$filename);
	    $array = array(
	        'filelink' => Yii::app()->request->hostInfo.'/uploads/blogImage/'.$filename,
	        'filename' => $filename
	    );
	    echo stripslashes(json_encode($array));
	}

	public function actionFileUpload() {
		$path =  str_replace("protected", "", Yii::app()->basePath);
	    $dir = $path . 'uploads'. DIRECTORY_SEPARATOR .'blogImage/';

	    $file=CUploadedFile::getInstanceByName('file');
	    $filename = md5(time()).'.'.$file->extensionName;
	    $path = $dir.$filename;
	    copy($_FILES['file']['tmp_name'], $path);
	    $array = array(
	        'filelink' => Yii::app()->request->hostInfo.'/uploads/blogImage/'.$filename,
	        'filename' => $filename
	    );
	    echo stripslashes(json_encode($array));
	}

	public function actionImageGetJson() {
		$path =  str_replace("protected", "", Yii::app()->basePath);
	    $dir = $path . 'uploads'. DIRECTORY_SEPARATOR .'blogImage/';
	    $files = array();
	    if (is_dir($dir)) {
	        if ($dh = opendir($dir)) {
	            while (($file = readdir($dh)) !== false) {
	                if ($file != '.' && $file != '..')
	                    $files[] = array(
	                        'thumb' => Yii::app()->request->hostInfo.'/uploads/blogImage/'.$file,
	                        'image' => Yii::app()->request->hostInfo.'/uploads/blogImage/'.$file,
	                        'title' => $file,
	                    );
	            }
	            closedir($dh);
	        }
	    }
	    echo json_encode($files);
	}

	public function actionAuthors() {
		$users = User::model()->findAll("isAuthor = 1 AND deleted = 0");
		
		$this->render('authors',array(
			'users'=>$users,
		));		
	}

	public function actionCreateAuthor() {
		$model=new User('addAuthor');

		if(isset($_POST['User']))
		{
			$model->foto = CUploadedFile::getInstance($model,'foto');
			$model->attributes=$_POST['User'];
			$model->isAuthor = 1;
			if($model->save()) {
				if(is_object($model->foto)) {
					$fotoPath = 'uploads/blogImage/authors/'.$model->link."_".$model->foto->name;
					$model->foto->saveAs($fotoPath);
					$model->foto = "/".$fotoPath;
					$model->update();
				}
				$this->redirect(array('authors'));
			}
		}

		$this->render('createAuthor',array(
			'model'=>$model,
		));
	}

	public function actionUpdateAuthor($id) {

		$model= User::model()->findByPk($id);
		$model->scenario = 'addAuthor';

		if(isset($_POST['User']))
		{
			$oldfoto = $model->foto;
			$model->foto = CUploadedFile::getInstance($model,'foto');
			$model->attributes=$_POST['User'];
			if(!$model->foto) {
				$model->foto = $oldfoto;
			}
			if($model->save())	{
				if(is_object($model->foto)) {
					$fotoPath = 'uploads/blogImage/authors/'.$model->link."_".$model->foto->name;
					$model->foto->saveAs($fotoPath);
					$model->foto = "/".$fotoPath;
					$model->update();
				}
				$this->redirect(array('authors'));
			}						
		}
		$this->render('updateAuthor',array(
			'model'=>$model,
		));
	}

	public function actionDeleteAuthor($id) {
		$model = User::model()->findByPk($id);
		$model->scenario = 'addAuthor';
		$model->deleted = 1;
		if($model->save()) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('authors'));
		}
	}


}
