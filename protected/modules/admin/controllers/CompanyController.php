<?php

class CompanyController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('index', 'addresses', 'addCompany', 'editCompany', 'addAddress', 'editAddress', 'account', 'contract'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	private $_model;

	public function beforeAction($action) {
		parent::beforeAction($action);
		Address::$showInactive = true;
		return true;
	}
	public function actionIndex() {

		$criteria = new CDbCriteria();

		$criteria->with = array(
		);
		$model = Company::model()->clinic();
		$model->attributes = Yii::app()->request->getParam(get_class($model));

		$this->render('index', array(
			'model' => $model,
				#'dataProvider' => $dataProvider
		));
	}

	public function actionAddresses($id)
    {
		$this->loadModel($id);		
		$criteria = new CDbCriteria();
		$criteria->compare('ownerId', $id);
		$criteria->with = [
			'city',
			'userMedicals'
		];
		$criteria->order = 'city.name ASC,street ASC';
		$dataProvider = new CActiveDataProvider('Address', array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50,
			),
		));

		$this->render('addresses', array(
			'dataProvider' => $dataProvider,
			'id' => $id
		));
	}

	public function actionAddCompany()
	{
		$model = new Company();
		$data = Yii::app()->request->getParam(get_class($model));
		
		if ($data)
		{
			if (!empty(Yii::app()->session['selectedRegion']))
			{
				$data['denormCitySubdomain'] = Yii::app()->session['selectedRegion'];
			}
			$model->attributes = $data;
			if ($model->save())
			{
				$this->redirect(['company/index']);
			}
		}
		
		$this->render('_companyForm', [
			'model' => $model
		]);
	}

	public function actionContract($id) {
		$model = new CompanyContract();
		$model->contractNumber = 'КО-1/' . (Yii::app()->db->createCommand('SELECT MAX(indexNum) FROM `' . CompanyContract::model()->tableName() . '`')->queryScalar() + 1);
		$post = Yii::app()->request->getParam(get_class($model));
		if ($post) {
			$model->attributes = $post;
			$model->companyId = $id;
			$model->indexNum = explode('/', $model->contractNumber)[1];
			if ($model->save()) {
				$this->redirect(['company/index']);
			} else {
				
			}
		}
		$this->render('contract', [
			'model' => $model
		]);
	}

	public function actionEditCompany($id) {
		$model = $this->loadModel($id);

		$data = Yii::app()->request->getParam(get_class($model));
		if ($data) {
			$model->attributes = $data;
			if ($model->save()) {
				$this->redirect(['company/index']);
			} else {
				
			}
		}

		$this->render('_companyForm', [
			'model' => $model,
			'id' => $id
		]);
	}

	public function actionAddAddress($id) {
		
		$company = $this->loadModel($id);
		$model = new Address();
		$data = Yii::app()->request->getParam(get_class($model));
		
		if ($data) {

			$model->attributes = $data;
			$model->ownerId = $id;
			$model->countryId = '534bd8ae-e0d4-11e1-89b3-e840f2aca94f'; // ru
			$model->name = $model->street;
			if(!empty($model->houseNumber)) $model->name .= ', ' . $model->houseNumber;

			$metroList = [];
			if (is_array($_REQUEST['metroList'])) 
				foreach ($_REQUEST['metroList'] as $metro) {
					$metroList[$metro['value']] = ['value' => $metro['value'], 'distance' => $metro['distance']];
				}
			if ($model->save()) {
				if (empty($company->addressId)) {
					$company->addressId = $model->id;
					$company->save();
				}
				if (!empty($metroList)) $model->saveMetroList($metroList);

				$this->redirect(['company/addresses', 'id' => $id]);
			} else {
				
			}
		} else {
			
			if ($subdomain = Yii::app()->session['selectedRegion']) {
				
				$city = City::model()->findByAttributes(['subdomain' => $subdomain]);
				$model->cityId = $city->id;
			}
		}

		$this->render('_addressForm', [
			'model' => $model
		]);
	}

	public function actionEditAddress($id, $ownerId) {
		$model = $this->loadAddressModel($id);

		$data = Yii::app()->request->getParam(get_class($model));
		if ($data) {
			$model->attributes = $data;
            $model->name = $model->street;
			if (!empty($model->houseNumber))
                $model->name .= ', ' . $model->houseNumber;
			$metroList = [];
			foreach ($_REQUEST['metroList'] ? $_REQUEST['metroList'] : [] as $metro) {
				$metroList[$metro['value']] = ['value' => $metro['value'], 'distance' => $metro['distance']];
			}
			if ($model->save()) {		
				$model->saveMetroList($metroList);
				$this->redirect(['company/addresses', 'id' => $ownerId]);
			} else {
				
			}
		}

		$this->render('_addressForm', [
			'model' => $model
		]);
	}

	public function actionAccount($id) {
		
		$model = $this->loadAddressModel($id);
		$companyId = $model->ownerId;

		if (!$um = $model->userMedicals) {
			$um = new UserMedical();
			$um->addressId = $model->id;
			$um->companyId = $model->company->id;
			$um->companyContractId = $model->company->companyContracts->id;
			$um->status = 0;
			$um->agreement = 0;
			$um->save();
		}
		if(!$um->companyContractId) {			
			$um->companyContractId = $model->company->companyContracts->id;
			$um->save();
		}
		if (!$um->name) {
			if (!UserMedical::model()->findByAttributes(['name' => $model->link])) {
				$um->name = $model->link;
				$um->update();
			} else {
				$um->name = $um->link;
				$um->update();
			}
		}
		if(Yii::app()->request->getParam('action') === "reset-agreement") {
			$um->agreementNew = 0;
			$um->createDate = NULL;
			$um->update();
		}

		if (Yii::app()->request->getParam('makepass')) {
			$pass = User::generate_password(6, true);
			$um->password = md5($pass);
			$um->update();
			echo CJSON::encode(['pass' => $pass]);
			Yii::app()->end();
		}

		$post = Yii::app()->request->getParam(get_class($um));
		if ($post) {
			$um->attributes = $post;
			$um->save();
		}
		$this->render('account', [
			'model' => $um,
			'companyId' => $companyId,
		]);
	}

	public function loadModel($id) {
		$this->_model = Company::model()->findByPk($id);

		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}

	public function loadAddressModel($id) {
		$this->_model = Address::model()->findByPk($id);

		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}
