<?php

class ServicesController extends Controller
{

	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules() {
		return array(
				array('allow',
						'actions'	 => array('login', 'error', 'logout'),
						'users'		 => array('*'),
				),
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('index', 'view', 'create', 'update', 'delete', 'requestAccept', 'request', 'requestCancel'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}
	public function actionRequest()
	{
		//$model = ServicesRequest::model();
		
		/*$cmd = Yii::app()->db->createCommand();
		
		$cmd->
		from(ServicesRequest::model()->tableName() . ' s')->
		leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId');
		//leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->
		
		$cmd->select(array(
				//'ca.name as CompanyActiviteName',
				'ss.name as ServiceSectionName',
				's.name as ServiceName',
				'IFNULL(s.price,0) as Price',
		));
		//where('s.addressId = :id', array(':id' => Yii::app()->user->model->clinic->id));
		
		$ServicesRequest = $cmd->queryAll();
		*/
		$dataProvider=new CActiveDataProvider('ServicesRequest', array(
											    'criteria'=>array(
											        'condition'=>'`t`.`status`<1 OR `t`.`status` IS NULL',
											        //'order'=>'create_time DESC',
											        'with'=>array('serviceSection'),
											    ),

											    'pagination'=>array(
											        'pageSize'=>30,
											    ),
											));
		
		$this->render('request', array('dataProvider'=>$dataProvider));
	}
	public function actionRequestAccept($id)
	{
		$model=new Service;
		$oServicesRequest = ServicesRequest::model()->findByPk($id);
	
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
	
			if(isset($_POST['Service']))
			{
				
				$model->attributes=$_POST['Service'];
				$model->companyActiviteId = CompanyActivite::model()->findByLink($model->companyActiviteId)->id;				
				if($model->save()) {
					$oAddressServices = new AddressServices();
					
					$oAddressServices->attributes = $model->attributes;
					
					$oAddressServices->addressId = $oServicesRequest->addressId;
					$oAddressServices->price 	 = $oServicesRequest->price;
					$oAddressServices->serviceId = $model->id;
					
					if($oAddressServices->save()) {
						$oServicesRequest->status = 2;
						$oServicesRequest->save();
					}
					
					
					$this->redirect(array('request'));
				}
			}
			
			else {
				
				if($oServicesRequest) {
					$model->attributes = $oServicesRequest->attributes;
				}
			}
	
			$this->render('create',array(
					'model'=>$model,
			));
	}

	
		/**
		 * Displays a particular model.
		 * @param integer $id the ID of the model to be displayed
		 */
		public function actionView($id)
		{
			$this->render('view',array(
					'model'=>$this->loadModel($id),
			));
		}
	
		/**
		 * Creates a new model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 */
		public function actionCreate($id=0)
		{
			$model=new Service;
	
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
	
			if(isset($_POST['Service']))
			{
				$model->attributes=$_POST['Service'];
				if($model->save())
					$this->redirect(array('view','id'=>$model->id));
			}
			else {
				$ServicesRequest = ServicesRequest::model()->findByPk($id);
				if($ServicesRequest) {
					$model->attributes = $ServicesRequest->attributes;
					//print_r($model->attributes);
					//print_r($oService->validate());
				}
			}
	
			$this->render('create',array(
					'model'=>$model,
			));
		}
	
		/**
		 * Updates a particular model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 * @param integer $id the ID of the model to be updated
		 */
		public function actionUpdate($id)
		{
			$model=$this->loadModel($id);
	
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
	
			if(isset($_POST['Service']))
			{
				$model->attributes=$_POST['Service'];
				if($model->save())
					$this->redirect(array('view','id'=>$model->id));
			}
	
			$this->render('update',array(
					'model'=>$model,
			));
		}
	
		/**
		 * Deletes a particular model.
		 * If deletion is successful, the browser will be redirected to the 'admin' page.
		 * @param integer $id the ID of the model to be deleted
		 */
		public function actionRequestCancel($id)
		{
			$oServicesRequest = ServicesRequest::model()->findByPk($id);
			if($oServicesRequest) {
				$oServicesRequest->status = 1;
				$oServicesRequest->save();
			}
	
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('request'));
		}
	
		/**
		 * Lists all models.
		 */
		public function actionIndex()
		{
			$this->redirect('request');
			$dataProvider=new CActiveDataProvider('Service');
			$this->render('index',array(
					'dataProvider'=>$dataProvider,
			));
		}
	
		/**
		 * Manages all models.
		 */
		public function actionAdmin()
		{
			$model=new Service('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Service']))
				$model->attributes=$_GET['Service'];
	
			$this->render('admin',array(
					'model'=>$model,
			));
		}
	
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return Service the loaded model
		 * @throws CHttpException
		 */
		public function loadModel($id)
		{
			$model=Service::model()->findByPk($id);
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
			return $model;
		}
	
		/**
		 * Performs the AJAX validation.
		 * @param Service $model the model to be validated
		 */
		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='service-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
}