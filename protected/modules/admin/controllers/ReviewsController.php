<?php

class ReviewsController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('index', 'experts', 'view', 'viewExpert', 'createAddress', 'createExpert', 'deleteAddress', 'deleteExpert'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	private $_model;

	public function actionIndex() {
		#$clinic = $this->loadModel();
		$criteria = new CDbCriteria();

		$criteria->with = array(
			'company' => array(
				'together' => true,
			),
			'address' => array(
				'together' => true
			)
		);
		#$criteria->addCondition('status <> -1');
		$model = RaitingCompanyUsers::model();
		$model->company = Company::model();
		$model->attributes = Yii::app()->request->getParam(get_class($model));
		$companyName = Yii::app()->request->getParam('Company')['name'];
		
		if($companyName) {					
			$model->company->name = $companyName;
			$criteria->compare('company.name',$companyName,true);
		}
		if($model->answer == 1) {
			$criteria->addCondition("t.answer <> ''");
		} elseif($model->answer == 2) {
			$criteria->addCondition("t.answer = ''");
		}
		$criteria->compare('t.status',$model->status);
				
		$dataProvider = new CActiveDataProvider($model, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50,
			),
			'sort' => array(
				'defaultOrder' => 'status ASC,date DESC'
			)
		));

		$this->render('index', array(
			'dataProvider' => $dataProvider,
			'model' => $model
			
		));
	}

	public function actionExperts() {
		#$clinic = $this->loadModel();
		$criteria = new CDbCriteria();

		$criteria->with = array(
			'doctor' => array(
				'together' => true
			)
		);

		$dataProvider = new CActiveDataProvider('RaitingDoctorUsers', array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
			'sort' => array(
				'defaultOrder' => 'date DESC'
			)
		));


		$this->render('experts', array('dataProvider' => $dataProvider));
	}

	public function actionCreateAddress() {
		$model = new RaitingCompanyUsers();
		$type = TypeOfCompanyRaitingUsers::model()->findAll();
		$types = CHtml::listData($type, 'id', 'name');

		$criteria = new CDbCriteria();
		$criteria->select = 'id,link,street,houseNumber';
		$criteria->with = [
			'company' => [
				'select' => 'id,link,name',
				'scopes' => 'clinicByAddress',
				'with' => [
					'companyType' => array(
						'select' => 'id,name',
					),
				]
			]
		];
		$criteria->order = 'company.name ASC,t.name ASC';
		$criteria->compare('t.cityId', (new Search())->cityId);
		$criteria->compare('companyType.isMedical', 1);
		$criteria->addNotInCondition('companyType.id', Address::$arrGMU);
		$addresses = Address::model()->findAll($criteria);

		$listData = [];
		foreach ($addresses as $address) {
			$listData[$address->id] = $address->company->name . ' на ' . $address->street . ', ' . $address->houseNumber;
		}


		$post = Yii::app()->request->getParam(get_class($model));
		if ($post) {
			$model->attributes = $post;
			$model->grades = $post['grades'];
			$model->companyId = Address::model()->findByPk($model->addressId)->ownerId;


			if ($model->save()) {
				foreach ($model->grades as $key => $value) {
					$companyValue = new RaitingCompanyValue();
					$companyValue->typeOfCompanyRaitingUsersId = $key;
					$companyValue->raitingCompanyUsersId = $model->id;
					$companyValue->value = $value;
					$companyValue->save();
				}

				Yii::app()->user->setFlash('registrySuccess', 'Отзыв успешно добавлен');
				$this->redirect(array('reviews/index'));
			}
		}
		$this->render('createAddress', [
			'model' => $model,
			'types' => $types,
			'listData' => $listData
		]);
	}

	public function actionCreateExpert() {
		
		$model = new RaitingDoctorUsers();
		$type = TypeOfDoctorRaitingUsers::model()->findAll();
		$types = CHtml::listData($type, 'id', 'name');

		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = [
			'currentPlaceOfWork' => [
				'together' => true,
				'select' => 'id,companyId',
				'with' => [
					'address' => [
						'select' => false,
						'together' => true,
					],
					'company' => [
						'together' => true,
						'select' => 'id,name,link',
					]
				],
			],
		];
		$criteria->select = 'name,id';
		$criteria->group = '`t`.`id`';
		
		$criteria->compare('address.cityId', (new Search())->cityId);
		$doctors = Doctor::model()->findAll($criteria);
		
		$listData = [];
		foreach ($doctors as $key=>$d) {
			$listData[$d->id] = $d->currentPlaceOfWork->company->name . ' - ' . $d->name;				
		}


		$post = Yii::app()->request->getParam(get_class($model));
		if ($post) {
			$model->attributes = $post;
			$model->grades = $post['grades'];
			#$model->companyId = Address::model()->findByPk($model->addressId)->ownerId;
			
			if ($model->save()) {
				foreach ($model->grades as $key => $value) {				
					$companyValue = new RaitingDoctorValue();
					$companyValue->typeOfDoctorRaitingUsersId = $key;
					$companyValue->raitingDoctorUsersId = $model->id;
					$companyValue->value = $value;
					$companyValue->save();
				}
				Yii::app()->user->setFlash('registrySuccess', 'Отзыв успешно добавлен');
				$this->redirect(array('reviews/experts'));
			}			
		}

		$this->render('createExpert', [
			'model' => $model,
			'types' => $types,
			'listData' => $listData
		]);
	}

	public function actionDeleteAddress($id) {
		$model = RaitingCompanyUsers::model()->findByPk($id);
		foreach ($model->raitingCompanyValues as $v) {
			$v->delete();
		}
		$model->delete();
	}
	
	public function actionDeleteExpert($id) {
		$model = RaitingDoctorUsers::model()->findByPk($id);
		foreach ($model->raitingDoctorValues as $v) {
			$v->delete();
		}
		$model->delete();
	}

	public function actionView($id) {
		$model = new RaitingCompanyUsers('answer');


		if ($attr = Yii::app()->request->getParam(get_class($model))) {

			if (is_array($attr)) {
				$comment = $model->findByPk($id);

				$comment->attributes = $attr;

				if ($comment->save()) {
					Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
					$this->redirect('index');
				}
			}
		}

		$this->render('answer', array(
			'model' => $this->loadModelCompanyComment($id),
		));
	}

	public function actionViewExpert($id) {
		$model = new RaitingDoctorUsers('answer');


		if ($attr = Yii::app()->request->getParam(get_class($model))) {

			if (is_array($attr)) {
				$comment = $model->findByPk($id);

				$comment->attributes = $attr;

				if ($comment->save()) {
					Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
					$this->redirect('experts');
				}
			}
		}

		$this->render('answer', array(
			'model' => $this->loadModelExpertComment($id),
			'viewExpert' => 1,
		));
	}

	public function loadModelCompanyComment($id) {
		$this->_model = RaitingCompanyUsers::model()->findByPk($id);

		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}

	public function loadModelExpertComment($id) {
		$this->_model = RaitingDoctorUsers::model()->findByPk($id);

		if ($this->_model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $this->_model;
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}
