<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			if(Yii::app()->user->isGuest || Yii::app()->user->model->hasRight('CAN_WORK_AS_SITE_EDITOR')){
			Yii::app()->user->loginUrl = Yii::app()->createUrl("/admin/default/login");
			}else{	
				echo 'Недостаточно прав на совершение действия. <a href="/">На главную</a>.';
				Yii::app()->end();
			}
			//print_r(Yii::app()->user);
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
