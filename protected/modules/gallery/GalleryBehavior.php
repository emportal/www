<?php
/**
 * Behavior for adding gallery to any model.
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */
class GalleryBehavior extends CActiveRecordBehavior
{
    /** @var string Model attribute name to store created gallery id */
    public $idAttribute;
    /**
     * @var array Settings for image auto-generation
     * @example
     *  array(
     *       'small' => array(
     *              'resize' => array(200, null),
     *       ),
     *      'medium' => array(
     *              'resize' => array(800, null),
     *      )
     *  );
     */
    public $versions;
    /** @var boolean does images in gallery need names */
    public $name;
    /** @var boolean does images in gallery need descriptions */
    public $description;
    public $prefix;
    private $_gallery;

    /** Will create new gallery after save if no associated gallery exists */
    public function beforeSave($event)
    {
        parent::beforeSave($event);
        if ($event->isValid) {
            if (!$this->getOwner()->isNewRecord && empty($this->getOwner()->{$this->idAttribute})) {
                $gallery = new Gallery();
                $gallery->name = $this->name;
                $gallery->description = $this->description;
                $gallery->versions = $this->versions;
                $gallery->save();

                $this->getOwner()->{$this->idAttribute} = $gallery->id;
            }
        }
    }

    /** Will remove associated Gallery before object removal */
    public function beforeDelete($event)
    {
        $gallery = $this->getGallery();
        if ($gallery !== null) {
            $gallery->delete();
        }
        parent::beforeDelete($event);
    }

    /** Method for changing gallery configuration and regeneration of images versions */
    public function changeConfig()
    {
        $gallery = $this->getGallery();
        if ($gallery == null) return;

        if ($gallery->versions_data != serialize($this->versions)) {
            foreach ($gallery->galleryPhotos as $photo) {
                $photo->removeImages();
            }

            $gallery->name = $this->name;
            $gallery->description = $this->description;
            $gallery->versions = $this->versions;
            $gallery->save();

            foreach ($gallery->galleryPhotos as $photo) {
                $photo->updateImages();
            }
        }
    }

    /** @return Gallery Returns gallery associated with model */
    public function getGallery($prefix='', $maxcount=50)
    {
    	$this->prefix = $prefix;
    	$owner = $this->prefix . $this->getOwner()->{$this->idAttribute};
    	Yii::import('gallery.*');
    	Yii::import('gallery.models.*');
    	
    	
        if (empty($this->_gallery)) {				
            $this->_gallery = Gallery::model()->find('owner=:owner',array(":owner"=>$owner));        
            if(!empty($this->_gallery)) {
	            $this->_gallery->maxcount = $maxcount;
	            $this->_gallery->save();
            }
        }
        
        if (empty($this->_gallery)) {				
        	$oGallery = new Gallery();
        	
        	
        	$oGallery->maxcount = $maxcount;

        	$oGallery->attributes = array('name'=>$this->prefix, 'owner'=>$owner);
        	$oGallery->id = ActiveRecord::create_guid($oGallery->attributes);
			
        	if($oGallery->save()) {
        		$this->_gallery = $oGallery;
        	}
        }

        return $this->_gallery;
    }

    /** @return GalleryPhoto[] Photos from associated gallery */
    public function getGalleryPhotos($prefix='',$count = 50)
    {
    	$this->getGallery($prefix,$count);
    	
    	    	
        $criteria = new CDbCriteria();
        $criteria->condition = 'gallery_id = :gallery_id';
        $criteria->params[':gallery_id'] = $this->_gallery->id;
        $criteria->order = '`rank` asc';
        
        $photos = GalleryPhoto::model()->findAll($criteria);
        foreach ($photos as $key=>$photo) {
			if(!is_file( Yii::getPathOfAlias('uploads.gallery'). DIRECTORY_SEPARATOR . $photo->id.'.'.$photo->galleryExt )) {
				unset($photos[$key]);
			}
        }
        return $photos;
    }
}
