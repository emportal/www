<?php
/**
* Gallery Manager- ImageManagerModule.php
*
* @author Spiros Kabasakalis <kabasakalis@gmail.com>,myspace.com/spiroskabasakalis
* @copyright Copyright &copy; 2011 Spiros Kabasakalis
* @since 1.0
* @license The MIT License
*/
class GalleryModule extends CWebModule
{

     public $defaultController='pl';

     //The following default values can be changed in config/main.php,
     //in module initialization.
     

     //Default upload directory,subdirectory of application's  root folder (same level as protected).
     //You have to create this directory.
     public  $upload_directory="gal_images";

     //Maximum number of images that can be uploaded at once.
     public $max_file_number=1000;

     //Maximum file size allowed.
     public $max_file_size= '1mb';


    public function init()
	{

		$this->setImport(array(
				'gallery.models.*',
				'gallery.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
                 
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}


     
}
