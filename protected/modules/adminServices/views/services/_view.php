<?php
/* @var $this ServicesController */
/* @var $data Service */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link')); ?>:</b>
	<?php echo CHtml::encode($data->link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullName')); ?>:</b>
	<?php echo CHtml::encode($data->fullName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serviceSectionId')); ?>:</b>
	<?php echo CHtml::encode($data->serviceSectionId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('forChildren')); ?>:</b>
	<?php echo CHtml::encode($data->forChildren); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('forAdult')); ?>:</b>
	<?php echo CHtml::encode($data->forAdult); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('companyActiviteId')); ?>:</b>
	<?php echo CHtml::encode($data->companyActiviteId); ?>
	<br />

	*/ ?>

</div>