<?php
/* @var $this ServicesController */

$this->breadcrumbs=array(
	'Модерация услуг'=>array('/adminServices/services/request'),
	'Заявки',
);

?>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'user-grid',
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'dataProvider'=> $dataProvider,
		'columns'=>array(
				array(					
					'name' => 'address.company.name',
					'header' => 'Название, Адрес',
					'value' => function($data) {
							return $data->address->company->name . ((!empty($data->address->company->name) && !empty($data->address->name)) ? "<br>" : "") . $data->address->name;
						},
					'headerHtmlOptions' => array('style' => 'width: 200px'),
					'type' => 'raw'
				),
				array(
						#'class' => 'ext.editable.EditableColumn',
						'name' => 'name',
						//'headerHtmlOptions' => array('style' => 'width: 110px'),
						/*'editable' => array(
								'url' => $this->createUrl('user/update'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)*/
				),
				array(
						#'class' => 'ext.editable.EditableColumn',
						'header'=>'Раздел',
						'name' => 'serviceSection.name',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'select',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				),
				/*array(
						#'class' => 'ext.editable.EditableColumn',
						'name' => 'companyActiviteId',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
						/*'editable' => array(
								'type' => 'select',
								'title'=>'Профиль деятельности',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(CompanyActivite::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				#),
				array(
						/*'class' => 'ext.editable.EditableColumn',*/
						'name' => 'description',
						//we need not to set value, it will be auto-taken from source
						//'headerHtmlOptions' => array('style' => 'width: 60px'),
					/*	'editable' => array(
								'type' => 'textarea',
								'url' => $this->createUrl('user/update'),
								'source' => CHtml::listData(ServiceSection::model()->findAll(), 'id', 'name'),
								'prepend' => 'Hidden'
						)*/
				),
				array(
						/*'class' => 'ext.editable.EditableColumn',*/
						'name' => 'price',
						'headerHtmlOptions' => array('style' => 'width: 50px'),
						/*'editable' => array(
								'url' => $this->createUrl('RequestAccept'),
								'placement' => 'right',
								'inputclass' => 'span3',
						)*/
				),
				array(
					'name' => 'checkDate',
					'header' => 'Дата создания'
				),
				array
				(
						'class'=>'CButtonColumn',
						'htmlOptions' => array('style' => 'padding: 5px; text-align: center;'),
						'template'=>'{confirm btn btn-success}<br>{reject btn btn-danger}',
						'buttons'=>array
						(
								'confirm btn btn-success' => array
								(
										'label'=>'<i class="icon-ok icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/adminServices/services/requestAccept", array("id"=>$data->id))',
										'click'=>'function(){return true;}',
								),
								'reject btn btn-danger' => array
								(
										'label'=>'<i class="icon-ban-circle icon-white"></i>',
										'url'=>'Yii::app()->createUrl("/adminServices/services/requestCancel", array("id"=>$data->id))',
										'click'=>'function(){									
											var rejectionText = prompt("Укажите причину отклонения");											
											if(!rejectionText) { 
												return false;
											} else {
												var href = $(this).attr("href");
												$(this).attr("href",href + "&text=" +rejectionText);
												return true;
											}
										}',
								),
						),
				),
				array(
					'class'=>'CButtonColumn',
					'header' => 'Услуги с похожим названием',
					'template'=>'{btn btn-showsimilar}',
					'buttons'=>array
					(
						'btn btn-showsimilar' => array
						(
							'label'=>'Показать услуги с похожим названием',
							'url'=>'Yii::app()->createUrl("/adminServices/ajax/getSimilarServies", array("id"=>$data->id))',
							'click'=> "function(){ showSimilar(this); return false;}",
						),
					),
					'htmlOptions' => array('style' => 'text-align: left;'),
				),
		),
));
?>
<script>
function showSimilar(obj) {
	console.log($(obj).attr('href'));
	var td = $(obj).closest('td');
    td.children().remove();
    td.text("Поиск...");
    $.ajax({
        url: $(obj).attr('href'),
        data: { },
        type: "GET",
        dataType: 'JSON',
        success: function(response) {
            td.text("");
            for(var i in response.result) {
            	var el = $( '<a></a>' );
            	el.attr('href',"/adminServices/services/admin?Service[id]="+i);
            	el.text(response.result[i]);
            	td.append(el);
            	td.append($( '<br>' ));
            	td.append($( '<br>' ));
            }
            if(td.children().length == 0) {
	            td.text("не найдено");
	        }
        },
        error: function (error) {
            td.text("Ошибка");
        }
    });
}
</script>