<?php $this->beginContent('/layouts/main'); ?>
<div class="wrapper">
	<?php $this->renderPartial('/layouts/_header'); ?>
	<section class="middle">
		<div class="container">
			<div class="clearfix">
				<?php echo $content; ?>
			</div><!-- /clearfix -->
		</div><!-- /container-->
	</section><!-- /middle-->
</div><!-- /wrapper -->
<footer class="footer">
</footer><!-- /footer -->
<?php $this->endContent(); ?>