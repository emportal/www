<?php
//Yii::app()->clientScript->reset();
Yii::app()->clientScript->scriptMap = array(
'main.css' => false
);
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?= CHtml::encode($this->pageTitle) ?></title>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/shared/js/jquery.pie.min.js"></script>
	<![endif]-->
	<?php Yii::app()->bootstrap->register(); ?>
</head>
<body>
	<div><?php echo $content; ?></div>
</body>
</html>