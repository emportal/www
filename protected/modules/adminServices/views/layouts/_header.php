<?php /* @var $this Controller */ ?>
<header class="header" style="height: 50px;">
	<?php
	if (!Yii::app()->user->isGuest) {
		$dataProvider = new CActiveDataProvider('ServicesRequest', array(
			'criteria' => array(
				'condition' => '`t`.`status`<1 OR `t`.`status` IS NULL',
			//'order'=>'create_time DESC',
			//'with'=>array('serviceSection'),
			),
			'pagination' => array(
				'pageSize' => 200,
			),
		));		

		$countRequest = count($dataProvider->getData());	
		
		
		$this->widget('bootstrap.widgets.TbNavbar', array(
			'type' => null, // null or 'inverse'
			'brand' => 'ЕМП',
			'brandUrl' => '/adminServices',
			'collapse' => true, // requires bootstrap-responsive.css
			'items' => array(
				'<ul class="nav">'				
				. '<li><a href="/adminServices/services/request">Модерация услуг ' . (($countRequest > 0) ? ""
						. "<sup><span class=\"badge badge-important\">{$countRequest}</span></sup>" : '') . '</a></li>'							
				. '<li></li><li><a href="/logout">Выход</a></li></ul>',
			//'<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
			),
		));
	}
	?>
</header><!-- /header-->
