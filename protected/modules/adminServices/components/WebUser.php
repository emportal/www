<?php

/**
 * @property User $model
 */
class WebUser extends CWebUser {

	// Store model to not repeat query.
	private $_model;

	public function init() {
		parent::init();

		$this->setStateKeyPrefix('adminServices_user_');
	}

	public function getModel() {
		if (!$this->isGuest && $this->_model === null) {
			$this->_model = User::model()->findByPk($this->id);
		}
		return $this->_model;
	}	
}

?>