<?php

/**
 * 	@var $instance ActiveRecord
 */
class AjaxController extends Controller {
	//public $layout = 'empty';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'cities' => array(
				'class' => 'AutoCompleteAction',
				'model' => 'City',
				'attributeValue' => 'name',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionAcceptAction() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAction::model()->find("id=:id", array(':id' => $id));
		$model = Action::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			if (!$model) {
				$model = new Action();
			}
			$model->attributes = $agrModel->attributes;
			if ($model->isNewRecord) {
				$model->link = "";
				$model->save();
			} else {
				$model->save();
			}
			$agrModel->delete();

			$error = 0;
		}

		/*
		  if($agrModel && $model) {
		  $model->attributes=$agrModel->attributes;
		  if($model->save()) {
		  $agrModel->delete();
		  $error=0;
		  }
		  }
		 */

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptEmail() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrEmail::model()->find("addressId=:id", array(':id' => $id));
		$model = Email::model()->find("addressId=:id", array(':id' => $id));
		//Если пустая модель, значит новую запись создаем
		if (!$model) {
			$model = new Email();
		}
		if ($agrModel && $model) {
			$model->attributes = $agrModel->attributes;
			//Если пусто, значит при подтверждении удаляем
			if ($agrModel->name == "") {
				$model->delete();
				$agrModel->delete();
				$error = 0;
				//Если есть имя, значит заменяем
			} else {
				if ($model->isNewRecord) {
					$model->link = "";
					$model->save();
				} else {
					if ($model->save()) {
						$agrModel->delete();
					}
				}
				$error = 0;
			}
		}


		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptPhones() {
		/*id -> addresId*/
		$id = Yii::app()->request->getParam('id');
		$error = 1;
		if ($id) {
			$ph = Phone::model()->findAll("addressId=:id", array(':id' => $id));
			foreach($ph as $p) {				
				PhoneCompanyUnit::model()->deleteAll("phoneId = :id",array(':id' => $p->id));
				$p->delete();
			}

			$agrModel = AgrPhone::model()->findAll("addressId=:id", array(':id' => $id));

			foreach ($agrModel as $phone) {
				$phone->syncThis = false;
				if ($phone->name == "remove") {
					$phone->delete();
					continue;
				}
				$m = new Phone();
				$m->attributes = $phone->attributes;
				$m->save();
				$phone->delete();
			}
			$error = 0;
		} else {
			$response['errors'][] = "id not found";
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptRef() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrRef::model()->find("ownerId=:id", array(':id' => $id));
		$model = Ref::model()->find("ownerId=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if (!$model) {
			$model = new Ref();
		}
		if ($agrModel && $model) {
			$model->attributes = $agrModel->attributes;
			//Если пусто, значит при подтверждении удаляем
			if ($agrModel->refValue == "") {
				$model->delete();
				$agrModel->delete();
				$error = 0;
				//Если есть имя, значит заменяем
			} else {
				if ($model->isNewRecord) {
					$model->link = "";
					$model->save();
					$agrModel->delete();
				} else {
					if ($model->save()) {
						$agrModel->delete();
					}
				}
				$error = 0;
			}
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptCompany() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrCompany::model()->find("id=:id", array(':id' => $id));
		$model = Company::model()->find("id=:id", array(':id' => $id));
		$agrModel->syncThis = false;
		if ($agrModel) {
			if (!$model) {
				$model = new Company();
			}
			$model->attributes = $agrModel->attributes;
			if ($model->isNewRecord) {
				$model->link = "";
				$model->save();
			} else {
				$model->save();
			}
			$agrModel->delete();

			$error = 0;
		}



		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionAcceptExpert() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;


		/* Перенос главной модели */
		$doctor = Doctor::model()->find("id=:id", array(':id' => $id));
		$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));
		if ($agrdoctor) {
			if (!$doctor) {
				$doctor = new Doctor();
			}
			$doctor->attributes = $agrdoctor->attributes;
			if ($doctor->isNewRecord) {
				$doctor->link = "";
				$doctor->save();
			} else {
				$doctor->save();
			}

			/* перенос зависимых моделей */
			$dsd = AgrDoctorScientificDegree::model()->findAll("doctorId=:id", array('id' => $id));
			if ($dsd) {
				$vals = DoctorScientificDegree::model()->findAll("doctorId=:id", array('id' => $id));
				foreach ($vals as $v) {
					$v->delete();
				}
				foreach ($dsd as $d) {
					$new = new DoctorScientificDegree();
					$new->attributes = $d->attributes;
					$new->save();
					$d->syncThis = false;
					$d->delete();
				}
			}

			$sod = AgrSpecialtyOfDoctor::model()->findAll("doctorId=:id", array('id' => $id));
			if ($sod) {
				$vals = SpecialtyOfDoctor::model()->findAll("doctorId=:id", array('id' => $id));
				foreach ($vals as $v) {
					$v->delete();
				}
				foreach ($sod as $d) {
					$new = new SpecialtyOfDoctor();
					$new->attributes = $d->attributes;
					$new->save();
					$d->syncThis = false;
					$d->delete();
				}
			}

			$de = AgrDoctorEducation::model()->findAll("doctorId=:id", array('id' => $id));
			if ($de) {
				$vals = DoctorEducation::model()->findAll("doctorId=:id", array('id' => $id));
				foreach ($vals as $v) {
					$v->delete();
				}
				foreach ($de as $d) {
					$new = new DoctorEducation();
					$new->attributes = $d->attributes;
					$new->save();
					$d->syncThis = false;
					$d->delete();
				}
			}

			$pow = AgrPlaceOfWork::model()->findAll("doctorId=:id", array('id' => $id));
			if ($pow) {
				$vals = PlaceOfWork::model()->findAll("doctorId=:id", array('id' => $id));
				foreach ($vals as $v) {
					$v->delete();
				}
				foreach ($pow as $d) {
					$new = new PlaceOfWork();
					$new->attributes = $d->attributes;
					$new->save();
					$d->syncThis = false;
					$d->delete();
				}
			}

			$ds = AgrDoctorServices::model()->findAll("doctorId=:id", array('id' => $id));
			if ($ds) {
				$vals = DoctorServices::model()->deleteAll("doctorId=:id", array('id' => $id));
				foreach ($vals as $v) {
					$v->delete();
				}
				foreach ($ds as $d) {
					$new = new DoctorServices();
					$new->attributes = $d->attributes;
					$new->save();
					$d->syncThis = false;
					$d->delete();
				}
			}
			$agrdoctor->syncThis = false;
			$agrdoctor->delete();

			$error = 0;
		}




		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	/* delete */

	public function actionDeclineAction() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrAction::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineEmail() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrEmail::model()->find("addressId=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclinePhones() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;
		if ($id) {
			
			$agrModel = AgrPhone::model()->deleteAll("addressId=:id", array(':id' => $id));

			$error = 0;
		} else {
			$response['errors'][] = "error #01"; // no id
		}


		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineRef() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrRef::model()->find("ownerId=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineCompany() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;

		$agrModel = AgrCompany::model()->find("id=:id", array(':id' => $id));

		if ($agrModel) {
			$agrModel->syncThis = false;
			$agrModel->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionDeclineExpert() {
		$id = Yii::app()->request->getParam('id');
		$error = 1;


		$agrdoctor = AgrDoctor::model()->find("id=:id", array(':id' => $id));

		AgrDoctorScientificDegree::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrSpecialtyOfDoctor::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrDoctorEducation::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrPlaceOfWork::model()->deleteAll("doctorId=:id", array('id' => $id));
		AgrDoctorServices::model()->deleteAll("doctorId=:id", array('id' => $id));

		if ($agrdoctor) {
			$agrModel->syncThis = false;
			$agrdoctor->delete();
			$error = 0;
		}

		if (!$error) {
			$response['success'] = true;
		} else {
			$response['success'] = false;
		}
		echo CJSON::encode($response);
	}

	public function actionChangeData() {
		$modelName = $model = $_POST['model'];
		$attribute = $_POST['attribute'];
		$newData = $_POST['newData'];
		$id = $_POST['rowId'];
		if ($attribute == "birthday") {
			$newData = date("Y-m-d", strtotime($newData));
		}
		/* Получаем объект модели через вызов МОДЕЛЬ::model() */
		$reflectionMethod = new ReflectionMethod($model, 'model');
		$model = $reflectionMethod->invoke(null);
		$object = $model->find('id=:id', array(':id' => $id));

		$response = array();
		if ($modelName == "AgrPhone") {
			$arr = explode(" ", $newData);
			$object->setAttribute('countryCode', $arr[0]);
			$object->setAttribute('cityCode', $arr[1]);
			$object->setAttribute('number', $arr[2]);
		}
		//Меняем значение атрибута
		$object->setAttribute($attribute, $newData);
		//проводим валидацию
		$object->syncThis = false;
		$object->validate(array($attribute));
		//Если нет ошибок, сохраняем, иначе выводим ошибки в консоль
		if ($object->getErrors()) {
			$response['error_message'] = $object->getErrors();
			$response['error'] = true;
		} else {
			if ($object->save()) {
				$response['error'] = false;
			} else {
				$response['error'] = true;
				$response['error_message'] = "Не получилось обновить запись";
			}
		}

		/* switch($modelName) {
		  case 'AgrAction':

		  break;
		  case 'AgrCompany':

		  break;
		  case 'AgrPhone':

		  break;
		  case 'AgrEmail':

		  break;
		  case 'AgrRef':

		  break;
		  case 'AgrDoctor':

		  break;
		  } */


		#var_dump($model->attributes);

		echo CJSON::encode($response);
	}

	public function actionUnblockUser($id) {

		$user = User::model()->find("id = :id", array(
			':id' => $id,
		));

		$user->isBlockedAppointment = 0;
		$user->blockedTime = 0;
		if ($user->save()) {
			$response['success'] = true;
		}

		echo CJSON::encode($response);
	}

	public function actionGetSimilarServies($id) {
		$response = ['id' => $id];
		$model = ServicesRequest::model()->findByPk($id);
		$SearchText = mb_strtolower($model->name);
		$SearchText = str_replace("лечение", "", $SearchText);
		$SearchText = str_replace("консультация", "", $SearchText);
		$SearchText = str_replace("исследование", "", $SearchText);
		$response = ['SearchText' => $SearchText];
		if($SearchText) {
			$regex = Search::regexpForQuery($SearchText);
			$similarList = [];
			if (!empty($regex)) {
				$regexCriteria = new CDbCriteria;
				$regexCriteria->select = "t.id,t.name";
				$regexCriteria->limit = '5';
				$regexCriteria->params = array(':search' => $regex);
				$regexCriteria->index = 'id';
				$regexCriteria->addNotInCondition('id',array($id));
			
				$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :search",), 'AND');
			
				$similarModel = Service::model()->findAll($regexCriteria);
				$similarList = array_merge($similarList, CHtml::listData($similarModel, 'id', 'name'));
			}
			if(count($companyIdsBySearchText) < 5) {
				$regex = Search::regexpForQuery($SearchText, 2);
			
				if (!empty($regex)) {
					$regexCriteria = new CDbCriteria;
					$regexCriteria->select = "t.id,t.name";
					$regexCriteria->limit = '5';
					$regexCriteria->params = array(':search' => $regex);
					$regexCriteria->index = 'id';
					$regexCriteria->order = 'id DESC, RAND()';
					$regexCriteria->addNotInCondition('id',array($id));
			
					$regexCriteria->addCondition(array("LOWER(`t`.`name`) REGEXP :search"), 'AND');

					$similarModel = Service::model()->findAll($regexCriteria);
					$similarList = array_merge($similarList, CHtml::listData($similarModel, 'id', 'name'));
				}
			}
			$response['result'] = $similarList;
		}
		echo CJSON::encode($response);
	}
}
