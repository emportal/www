<?php
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-info',
	//'action'				 => $this->createUrl('comments/view'),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
		));
/* @var $form CActiveForm */
?>
<?php if ( Yii::app()->user->hasFlash('success') ): ?>
<div class="flash-success">
	<span><?php echo Yii::app()->user->getFlash('success')?></span>
</div>
<?php endif;?>
<table class="search-result-table">
	<tbody>
		<tr class="search-result-box">
			<td colspan="2" class="search-result-info">
				<label><strong>Отзыв пациента:</strong></label>
				<p style="padding:4px 5px;"><?=$model->review?></p>
				<?php #echo $form->textArea($model, 'review', array('class' => 'custom-textarea', 'readonly'=>true))?>
				
				<!--<?php# echo $form->checkBox($model, 'status')?> Опубликовать отзыв на сайте-->
			</td>
		</tr>
		<tr class="search-result-box">
			<td colspan="2" class="search-result-info">
				<label><b>Ответ клиники:</b></label>
				<?php echo $form->textArea($model, 'answer', array('class' => 'custom-textarea'))?>
				<?php echo $form->error($model, 'answer');?>
			</td>
		</tr>
		<tr class="search-result-box">
			
			<td class="search-result-info" colspan="2" style="text-align: center;">
				<a class="btn-red" title="Отменить" href="<?=$this->createUrl('comments/')?>">Отменить</a>
				<button class="btn-green" type="submit" title="Сохранить">Сохранить</button>
			</td>
		</tr>
	</tbody>
</table>
<?php $this->endWidget();?>