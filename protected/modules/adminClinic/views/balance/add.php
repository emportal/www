<style>
.ibox-content {
    clear: both;
}
.ibox-content {
    background-color: #fff;
    color: inherit;
    padding: 15px 20px 20px 20px;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 0;
}
.form-group {
    display: table;
    margin-bottom: 15px;
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
}
.contr_second_box {
    cursor: pointer;
    position: relative;
    float: left;
    padding: 0 0 0 0;
    margin: 10px 20px 0 0;
    width: 158px;
    height: 53px;
    border: 1px solid #e6e8ea;
    -webkit-border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -o-border-radius: 4px;
    border-radius: 4px;
    background-position: 50% 50%;
}
.contr_second_box a.clicked {
    border: 2px solid #04b0cf;
}
.contr_second_box a:hover {
    border: 2px solid #04b0cf;
}
.contr_second_box a {
    margin: -1px;
    width: 158px;
    height: 53px;
    display: block;
    -webkit-border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -o-border-radius: 4px;
    border-radius: 4px;
}
.contr_second_box.bank { background: url('/images/contract_sp.png') -162px -396px no-repeat; }
.contr_second_box.plast { background: url('/images/contract_sp.png') 0px -451px no-repeat; }
.contr_second_box.kiwi { background: url('/images/contract_sp.png') -160px -449px no-repeat; }
.contr_second_box.yand { background: url('/images/contract_sp.png') -320px -396px no-repeat; }
.contr_second_box.kiwi_wallet { background: url('/images/contract_sp.png') -320px -451px no-repeat; }
.contr_second_box.euroset { background: url('/images/contract_sp.png') -208px -344px no-repeat; }
.contr_second_box.svyznoy { background: url('/images/contract_sp.png') -364px -344px no-repeat;}
.contr_second_box.loreal { background: url('/images/contract_sp.png') -159px -504px no-repeat;}
.contr_second_box.cert { background: url('/images/contract_sp.png') -312px -504px no-repeat;}
</style>
<form style="display:none;/**/" id="yandexPaymentForm" name="14f40c4743d6cb8f_ShopForm" method="POST" action="http://money.yandex.ru/eshop.xml">
	<!-- тестовый обработчик: https://demomoney.yandex.ru/eshop.xml -->
	<!-- боевой обработчик: https://money.yandex.ru/eshop.xml -->
	<br><input name="paymentType" value="PC" type="radio"> Яндекс деньги
	<br><input name="paymentType" value="AC" type="radio"> Банковская карта
	<br><br>
	
	<!-- Обязательные поля --> 
	<input name="shopId" value="<?= Yii::app()->params['api']['kassa']['shopId'] ?>" type="hidden"/> 
	<input name="scid" value="<?= Yii::app()->params['api']['kassa']['scid'] ?>" type="hidden"/> 
	<input name="sum" value="0" type="text" readonly> руб 
	<input name="customerNumber" value="c<?= $clinic->link ?>" type="hidden"/>
	<input name="orderNumber" value="i" type="hidden"/>
	
	<!-- Необязательные поля --> 
	<!-- <input name="shopArticleId" value="001" type="hidden"/>
	<input name="cps_phone" value="79110000000" type="hidden">
	<input name="cps_email" value="user@domain.com" type="hidden">-->
	<input name="shopSuccessURL" value="<?= Yii::app()->getBaseUrl(true) ?>/adminClinic/default/documents?paymentSuccess=true" type="hidden">
	<input name="shopFailURL" value="<?= Yii::app()->getBaseUrl(true) ?>/adminClinic/default/documents?paymentSuccess=false" type="hidden">
	<input name="invoiceNumber" value="" type="hidden">
	<input id="yandexPaymentButton" type="submit" class="btn-green" value="Оплатить"/> 
</form>
<form class="form-horizontal" method="POST" action="add">
	<div class="flash-error"></div>
	<fieldset>
		<input type="hidden" id="salon_id" value="34211">
		<div class="form-group">
			<label class="col-sm-3 control-label">Введите сумму для оплаты</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
					<input class="form-control pay_amount" type="text" name="balance_any" value="3000" maxlength="30">
					<span class="input-group-addon">рублей</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label">Выберите способ оплаты</label>
			<div class="col-sm-8">
				<div class="contr_second_box bank">
					<a href="javascript:void(0)" rel="bill" class="clicked"></a>
				</div>
				<div class="contr_second_box plast">
					<a href="javascript:void(0)" rel="yandex_card"></a>
				</div>
				<div class="contr_second_box yand">
					<a href="javascript:void(0)" rel="YandexMerchantOceanR"></a>
				</div>
				<!-- 
				<div class="contr_second_box kiwi">
					<a href="#" rel="QiwiR"></a>
				</div>
				<div class="contr_second_box kiwi_wallet">
					<a href="#" rel="QiwiR"></a>
				</div>
				<div class="contr_second_box euroset">
					<a href="#" rel="RapidaInR"></a>
				</div>
				<div class="contr_second_box svyznoy">
					<a href="#" rel="RapidaInSvyaznoyR"></a>
				</div>
				<div class="contr_second_box loreal">
					<a href="#" rel="certificate"></a>
				</div>
				<div class="contr_second_box cert">
					<a href="#" rel="certificate"></a>
				</div>
				-->
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label"></label>
			<div class="col-sm-8">
				<div class="checkbox">
					<label> <input type="checkbox" class="oferta" name="oferta" value="1" checked> Я принимаю условия <a href="javascript:void(0)">договора-оферты</a> </label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label"></label>
			<div class="col-sm-8">
				<a class="btn btn-primary" id="paybtn" href="javascript:void(0)">Оплатить</a>
			</div>
		</div>
	</fieldset>
</form>
<script>
$(function() {
	$(document).ready(function() {
		function newBill(sum, type, success, onerror) {
			var result = {
					number: 123456,
					sum: sum,
					type: type,
			};
			//success(result);
			$.ajax({
                url: 'newBill',
                async: false,
                data: {
                	sum: sum,
                	type: type,
                },
                dataType: 'json',
                type: 'POST',
				beforeSend: function() { },
                success: function(response) {
                    if(response.success) {
                    	success(response);
                    } else {
                        this.error(null,JSON.stringify(response),null);
                    }
				},
                error: function(XHR,errorText,errorThrown) {
                	onerror(errorText)
				},
                complete: function() { },
			});
		}
		$('.contr_second_box a').live('click',function() {
			$('.contr_second_box a').removeClass('clicked');
			$(this).addClass('clicked');
		});
		$('#paybtn').live('click',function() {
			var form = $(this).closest('form').get(0);
			$(form).find('.flash-error').text('');
			if(!$(form).find('.oferta').is(':checked')) {
				$(form).find('.flash-error').text('Подтвердите договор оферты!');
			} else {
				var sum = $(form).find('.pay_amount').val();
				var type = $(form).find('.contr_second_box a.clicked').attr('rel');
				switch(type) {
				case 'yandex_card':
					newBill(sum,type,function(bill) {
						$('#yandexPaymentForm input[name="sum"]').val(bill.sum);
						$('#yandexPaymentForm input[name="billNumber"]').val(bill.number);
						$('#yandexPaymentForm input[name="orderNumber"]').val("i"+bill.number);
						$('#yandexPaymentForm input[name="paymentType"][value="AC"]').prop('checked', true);
						$('#yandexPaymentForm').submit();
					}, function(errorText) {
						$(form).find('.flash-error').text(errorText);
					});
					break;
				case 'YandexMerchantOceanR':
					newBill(sum,type,function(bill) {
						$('#yandexPaymentForm input[name="sum"]').val(bill.sum);
						$('#yandexPaymentForm input[name="billNumber"]').val(bill.number);
						$('#yandexPaymentForm input[name="orderNumber"]').val("i"+bill.number);
						$('#yandexPaymentForm input[name="paymentType"][value="PC"]').prop('checked', true);
						$('#yandexPaymentForm').submit();
					}, function(errorText) {
						$(form).find('.flash-error').text(errorText);
					});
					break;
				case 'bill':
					newBill(sum,type,function(bill) {
						var url = "bill?id="+encodeURIComponent(bill.number);
						window.open(url+"&download=1", '_blank');
						window.location.href = url;
					}, function(errorText) {
						$(form).find('.flash-error').text(errorText);
					});
					break;
				default:
					$(form).find('.flash-error').text('Способ оплаты не выбран!');
					break;
				}
			}
		});
	});
});
</script>