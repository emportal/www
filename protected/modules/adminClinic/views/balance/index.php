<style>
table.balance_detail {
    width: 100%;
}
table.balance_detail {
    background: lightcyan;
}
table.balance_detail tr.payment {
    background: lightgreen;
}
table.balance_detail tr.bill {
	background: linen;
}
table.balance_detail tr td,
table.balance_detail tr th {
    font-size: 12px !important;
    border: 1px solid gray;
    padding: 8px;
    color: #000;
}
.btn-balanceAdd {
    cursor: pointer;
    border: 0;
    background: rgb(141,198,63);
    box-sizing: border-box;
    display: inline-block;
    word-spacing: normal;
    vertical-align: top;
    min-height: 25px !important;
    padding-top: 6px;
    padding-bottom: 6px;
    padding-left: 25px;
    padding-right: 25px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    font-size: 14px;
    color: #fff !important;
    text-decoration: none;
    text-align: center;
    text-shadow: 0 0 0 !important;
    text-transform: none;
    white-space: normal !important;
    position: relative;
}
.btn-balanceAdd:hover {
    background: rgb(100,157,22);
    color:#fff;
}
.btn-balanceAdd:active {
    background: rgb(86,143,8);
    color:#fff;
}
</style>
<?php if($from == 'newAdmin'): ?>
<span>
	<b>Баланс:</b> 
	<b style="color:<?= ($model->balance < 0) ? 'red' : 'green' ?>;">
		<?= $model->balance ?> руб.
	</b>
</span>
<?php else: ?>
<h2>Баланс составляет: <b style="color:<?= ($model->balance < 0) ? 'red' : 'green' ?>;"><?= $model->balance ?> руб.</b></h2>
<a href="/adminClinic/balance/add" target="_blank" class="btn-balanceAdd">Пополнить</a>
<br><br>
<?php endif; ?>
<h4 style="text-align: center;">Детальный отчёт по балансу</h4>
<table class="balance_detail">
  <tr>
    <th>Дата</th>
    <th>Стоимость</th>
    <th>Операция</th>
  </tr>
<?php foreach ($model->details as $detail): ?>
  <tr class="<?= $detail->type ?>">
    <td><?= date("Y-m-d",strtotime($detail->date)) ?></td>
    <td><?= $detail->sum ?> руб.</td>
    <td><?= $detail->description ?></td>
  </tr>
<?php endforeach; ?>
</table>

<script>
$(function() {
	$(document).ready(function() {
		
	});
});
</script>