<?php /* @var $this AdminClinicController */ 
 
$countRegistry=Yii::app()->db->createCommand()->select("COUNT(*)")
	->from(AppointmentToDoctors::model()->tableName())
	->where("addressId=:aId AND statusId=".AppointmentToDoctors::SEND_TO_REGISTER,array(':aId'=>Yii::app()->user->model->addressId))
	->queryScalar();

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/shared/css/minicrm.css?v=1');

$isTablet = ( substr(Yii::app()->request->url, 0, 19) == '/adminClinicTablet/' ) ? 1 : 0;
if ($isTablet)
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/shared/css/minicrm_compact.css');

?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="wrapper clinic">
	<?php $this->renderPartial('/layouts/_header_mini_crm'); ?>
	<section class="middle">
		<div class="container">
			<div class="search-result cabinet" style="margin: 15px 10px;">
				<?php echo $content; ?>
			</div>
			<div class="clearfix">

			</div><!-- /clearfix -->
		</div><!-- /container-->
	</section><!-- /middle-->
</div><!-- /wrapper -->

<div id="dark_bg" class="dark_bg"></div>

<footer class="footer">
	<p class="copyright" style="text-align: center;">© 2012-<?= date("Y") ?> все права защищены. Все права на любые материалы, опубликованные на сайте, защищены в соответствии с российским и международным<br>законодательством об авторском праве. При использовании текстовых материалов в интернете и при использовании текстов в печатных и электронных<br>СМИ ссылка на данный ресурс обязательна.</p>
</footer><!-- /footer -->

<?php
//если ранее не видели попап с услугами, находятся в питере и не является сотрудником ЕМП, зашедшим из админки
$userMedical = $this->loadModel()->userMedical;
if ($userMedical->hasReadLatestNews == 0 AND !$isTablet AND !Yii::app()->user->isSuperUser)
{
	$this->renderPartial('/layouts/_popup_latest_news', ['regionName' => $userMedical->company->denormCitySubdomain]);
}
?>
<?php $this->renderPartial('/layouts/_fill_the_field_please'); ?>
<?php Yii::import('application.views.layouts._manager_lkk_modal', true); ?>
<?php Yii::import('application.views.layouts._counters', true); ?>
<?php $this->endContent(); ?>