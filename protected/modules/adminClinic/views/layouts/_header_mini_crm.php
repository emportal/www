<style>
h4 {
	display: block;
	margin: 10px 0;
}
</style>
<?php /* @var $this Controller */
$clinic = $this->loadModel();
?>
<div class="header">
	<table class="ctable htable">
		<tr>
			<td  style="text-align: left;">
				<a href="/">
					<?= CHtml::image($this->assetsImageUrl.'/logo.png', "Единый медицинский портал") ?>
				</a>
			</td>
			<td style="width: 50%;">
				<div class="hide_when_compact">
					<?= $clinic->relativeLinkTag ?> | <a href="../default/logout">(x) выйти</a>
					<br>
					<?php
                    /*$clinic->company->companyContracts ?
									"Договор №{$clinic->company->companyContracts->contractNumber} до ". Yii::app()->dateFormatter->formatDateTime($clinic->company->companyContracts->periodOfValidity) :
									'Нет данных о контрактах'*/
                    if ($clinic->debtSum > 0): ?>
                    <a href="/adminClinic/default/documents#bills" class="c-red">
                        Задолженность по оплате <?= $clinic->debtSum ?> руб.
                    </a>
                    <?php else : 
                        echo $clinic->company->companyContracts ?
									"Договор №{$clinic->company->companyContracts->contractNumber} от ". Yii::app()->dateFormatter->formatDateTime($clinic->company->companyContracts->periodOfValidity) :
									'Нет данных о контрактах';
                    endif; ?>
                    <?php if($clinic->notAllRequiredFieldsAreFilled()): ?>
                    <br>
					<a href="/adminClinic/default/contacts" class="c-red">
						Заполните обязательные поля
					</a>
                    <?php endif; ?>
					<?php echo $this->clips['head_content']; ?>
				</div>
			</td>
			<td style="text-align: right;">
				<?php
					$manager_img_src = 'images/test.jpg';
					$manager_id = Yii::app()->user->getModel()->company->managerRelations->user->id;
					if ($manager_id == 'EMPORTAL-0dbf-2c16-7a80-5f4e17e63ab7') $manager_img_src = 'images/managers/m1.jpg'; //Милан					
					elseif ($manager_id == 'EMPORTAL-dad9-82ec-223b-d578261a8699') $manager_img_src = 'images/managers/m2.jpg'; //Татьяна
					elseif ($manager_id == 'EMPORTAL-da80-4adb-6e44-9bf2d1812f7a') $manager_img_src = 'images/managers/m3.jpg'; //Анастасия
					elseif ($manager_id == 'EMPORTAL-9321-bc14-8799-725c54c302df') $manager_img_src = 'images/managers/m4.jpg'; //Оксана					
					$manager_img_src = Yii::app()->getBaseUrl(true) . '/' . $manager_img_src;
				?>
				<img class='crm_manager_img' src="<?=$manager_img_src?>"></img>
				<p style="margin: 0px; padding-top: 9px; width: 300px; text-align: right;">Ваш менеджер: <?=Yii::app()->user->getModel()->company->managerRelations->user->name?></p>
				<a href="#ContactManager" onclick="mdContactManager.show({
						mdheader: '<img class=\'crm_manager_img\' src=\'<?=$manager_img_src?>\' style=\'float: left;\'></img><p style=\'font-size: 12px;\'>Ваш менеджер: <?=Yii::app()->user->getModel()->company->managerRelations->user->name?>
					<br> <?=Yii::app()->user->getModel()->company->managerRelations->user->telefon?>
					<br><a href=\'mailto:<?=Yii::app()->user->getModel()->company->managerRelations->user->email?>\'><?=Yii::app()->user->getModel()->company->managerRelations->user->email?></a><br></p>'
					}); return false;">Связаться с менеджером</a>
			</td>
		</tr>
	</table>
	<table class="ctable hide_when_compact">
		<tr>
			<td class="ctable_th" style="background-color: #D9F7FF;" colspan="3">Информация о клинике</td>
			<td class="ctable_th" style="background-color: #8EFF8A;" colspan="3">Прием пациентов</td>
			<td class="ctable_th" style="background-color: #F1FFA3;" colspan="3">Работа с порталом</td>
		</tr>
		<tr>
			<td class="c1">
				<a href="/adminClinic/default/index">Описание клиники</a>
			</td>
			<td class="c1">
				<a href="/adminClinic/default/contacts">Настройки</a>
			</td>
			<td class="c1">
				<a href="/adminClinic/services/index">Услуги</a>
			</td>
			
			<td class="c2">
				<a href="/adminClinic/default/visits">Регистратура</a>
			</td>
			<td class="c2">
				<a href="/adminClinic/experts/index">Специалисты</a>
			</td>
			<td class="c2">
				<a href="/adminClinic/default/calendar">Расписание</a>
			</td>
			
			<td class="c3">
				<a href="/adminClinic/default/documents">Счета и документы</a>
			</td>
			<td class="c3">
				<a href="/adminClinic/default/stats">Просмотры</a>
			</td>
			<td class="c3 bold">
				<a href="/adminClinic/default/etc">Форма записи</a>
			</td>
		</tr>
	</table>
</div>