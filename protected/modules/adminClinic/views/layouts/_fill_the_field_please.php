<div id="assets" style="display: none;">
	<div id="fillTheFieldPlease_view" style="width: 480px; height: auto;font-size: 30px;line-height: 1.5em;text-align: center;padding: 5px 0 10px 0;">
		Для продолжения работы пожалуйста
		<br><a href="/adminClinic/default/contacts" class="c-red">заполните обязательные поля</a>.
		<br>Эти данные необходимы нам для корректной работы с вашей клиникой.
	</div>
</div>
<?php
$clinic = $this->loadModel();
if(strtotime($clinic->userMedicals->createDate)>strtotime("2015-11-10 23:59:59") AND $clinic->notAllRequiredFieldsAreFilled() AND ($this->uniqueid !== 'adminClinic/default' OR $this->action->Id !== 'contacts')) {
	Yii::app()->clientScript->registerScript('fillTheFieldPlease_fancyView','
		$.fancybox({
			id: "fillTheFieldPlease_fancyView",
			content: $("#fillTheFieldPlease_view").clone(),
			showCloseButton: false,
			closeBtn: false,
			closeClick: false,
			closeEffect: "none",
	        hideOnOverlayClick:false,
	        hideOnContentClick:false,
			helpers: { 
				overlay: { closeClick: false }
			}
		});
	', CClientScript::POS_READY);
}
?>