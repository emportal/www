<?php /* @var $this Controller */ ?>
<header class="header">
	<div class="section-top">
		<div class="right-corner-top"></div>
		<div class="section-bottom min">
			<div class="right-corner-bot"></div>
			<div class="section">
				<div class="logo">
					<div class="logo-image">
						<?= CHtml::link(CHtml::image($this->assetsImageUrl.'/logo.png?v=2', "Единый медицинский портал"), '/')?>
					</div><!-- /logo-image -->
				</div><!-- /logo -->
				<?php if(!Yii::app()->user->isGuest):?>
					<div class="auth">
						<span class="f-left">
							<strong><?= $this->loadModel()->getName()?></strong>
							<br />
							<?= $this->loadModel()->company->companyContracts ?
								"Договор №{$this->loadModel()->company->companyContracts->contractNumber} от ". Yii::app()->dateFormatter->formatDateTime($this->loadModel()->company->companyContracts->periodOfValidity) :
								'Нет данных о контрактах'?>
								<?php echo $this->clips['head_content']; ?>
						</span>
						
						<span class="f-right">
								<a href="<?= $this->createUrl('default/logout')?>" class="btn">Выход</a>
						</span>
					</div><!-- /auth -->
				<?php endif;?>
			</div><!-- /section -->
		</div><!-- /section-bottom -->
	</div><!-- /section-top -->
</header><!-- /header-->
