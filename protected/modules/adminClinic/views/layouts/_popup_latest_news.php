<div id="popup_services" class="md-modal popup" style="width: 650px; top: 240px;">
	<div class="md-close">X</div>
	<div class="md-content">
		<div class="md-form-container">
			<div>
				<div>
					<table class="service-table no-border no-margin">
						<tr>
							<td colspan="2" class="no-border">
								<h3>Уважаемые клиенты!</h3>
							</td>
						</tr>
					</table>
				</div>
				<div>
					<table class="service-table no-border no-margin">
						<tr>
							<td class="no-border vpadding20" style="vertical-align: middle;" colspan="2">
								<p>
                                    Для удобства взаимодействия с Единым медицинским порталом мы сделали единый многоканальный телефонный номер - (812) 648-22-36. По всем вопросам, связанным с работой с Единым медицинским порталом, Вы можете звонить на этот номер. Мы будем рады ответить на Ваши вопросы.
                                </p>
                                <p>
                                    В связи с появлением этого номера телефона мы добавили его в наш
                                    <a href="/adminClinic/default/documents">Договор-оферту.</a>
                                </p>
								<br><p>
									С уважением, 
                                    <br>команда ЕМП
								</p>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
<script>
function ajax_popupShown() {
	$.ajax({
		url: '/adminClinic/ajax/popupShown',
		type: 'GET',
		async: false,
		data: {},
		success: function(data) {
			console.log('popup shown');
			console.log(data);
		},
		error: function(data) {
			console.log('error while getting response from popupShown');
		},
	});
}
$(function() {
	var options = {
		'id': 'popup_services'
	};
	var newServicesModal = new ModalWindowConstructor(options);
	newServicesModal.show();
	$('body').on('click', '.md-overlay, #popup_services .md-close', function() {
		newServicesModal.close();
		ajax_popupShown();
	});
	$('body').on('click', '#popup_services a', function() {
		ajax_popupShown();
	});
});
</script>