<?php /* @var $this Controller */ 

$isTablet = ( substr(Yii::app()->request->url, 0, 19) == '/adminClinicTablet/' ) ? 1 : 0;
if ($isTablet)
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/shared/css/minicrm_compact.css');
?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="wrapper">
	<?php $this->renderPartial('//layouts/_header_nulled'); ?>
	<section class="middle">
		<div class="container">
			<div class="search-result cabinet">
				<div class="crumbs invisible">
					<?php
					$this->widget('Breadcrumbs', array('links' => $this->breadcrumbs));
					?>
				</div>
				<?php echo $content; ?>
			</div>
			<div class="clearfix">

			</div><!-- /clearfix -->
		</div><!-- /container-->
	</section><!-- /middle-->
</div><!-- /wrapper -->
<footer class="footer">
	<p class="copyright" style="text-align: center;">© 2012-<?= date("Y") ?> все права защищены. Все права на любые материалы, опубликованные на сайте, защищены в соответствии с российским и международным<br>законодательством об авторском праве. При использовании текстовых материалов в интернете и при использовании текстов в печатных и электронных<br>СМИ ссылка на данный ресурс обязательна.</p>
</footer><!-- /footer -->
<?php $this->endContent(); ?>