<form action="description" method="post" style="text-align: center;">
	<div>
		<input type="hidden" name="id" value="<?= urlencode($model->id) ?>">
		<h1>
			Описание услуги
			<br>
			<b><?=$model->service->name?></b>
		</h1>
		<div style="margin: 20px 0;">
			<textarea name="<?=get_class($model)?>[description]" style="width: 450px;height: 150px;"><?=$model->description?></textarea>
			<div class="descriptionResponse" style="text-align: left;"></div>
		</div>
		<div>
			<button class="btn-green" onclick="try{submitDescription(this)}catch(e){} return false;">Сохранить</button>
		</div>
	</div>
</form>
<script>
 function submitDescription(obj) {
	var form = $(obj).closest('form');
	var formData = new FormData(form.get(0));
	$.ajax({
		url: form.attr('action'),
		async: true,
		data: formData,
		cache:false,
		contentType: false,
		processData: false,
		dataType: 'json',
		type: 'POST',
		beforeSend: function() {
			form.addClass('screen_loading');
			form.children().css('visibility','hidden');
			form.find('.descriptionResponse').removeClass('error');
			form.find('.descriptionResponse').removeClass('success');
		},
		success: function(response) {
			if(response.success) {
				form.find('.descriptionResponse').addClass('success');
				form.find('.descriptionResponse').text('Сохранено!');
				form.find('.descriptionResponse').css('visibility', 'visible');
				form.find('.descriptionResponse').show();
				setTimeout(function(){
					parent.$.fancybox.close();
				},100);
			} else {
				try{ var errorText = response.errors.join("<br>"); } catch(e){}
				this.error(null,errorText,null);
			}
		},
		error: function(XHR,errorText,errorThrown) {
			var text = "Ошибка: ";
			try{ 
				form.find('.descriptionResponse').addClass('error');
				text = "Ошибка: "+errorText;
				if('responseText' in XHR) {
					text += " "+XHR.responseText;
				}
			} catch(e){}
			form.find('.descriptionResponse').text(text);
		},
		complete: function() {
			form.removeClass('screen_loading');
			form.children().css('visibility','visible');
		},
	});
 }
</script>