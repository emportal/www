
<?php if ( Yii::app()->user->hasFlash('error') ): ?>
<div class="flash-error">
	<span><?php echo Yii::app()->user->getFlash('error')?></span>
</div>
<?php endif;?>
<?php if ( Yii::app()->user->hasFlash('success') ): ?>
<div class="flash-success">
	<span><?php echo Yii::app()->user->getFlash('success')?></span>
</div>
<?php endif;?>

<?php

if($ServicesRequest):?>
<div id="ServicesRequest">
<h4>Услуги на модерации</h4>
	<table class="service-table custom-cells-bg">
		<thead>
			<tr>
				<?php /*?><td>Профиль</td><?php*/?>
				<td style="width: 260px;">Профиль</td>
				<td>Услуга</td>
				<td>Статус</td>
				<td>Стоимость</td>
				<td>Причина отклонения</td>
			</tr>
		</thead>
		<?php foreach ($ServicesRequest as $row):?>
		<tr>
			<?php /*?><td><?php echo $row['ServiceSectionName']?></td><?php*/?>
			<td>
				<?php echo $row['ServiceSectionName']?>
			</td>
			<td>
				<?php echo $row['ServiceName']?>
			</td>
			<td>
				<?php 
				switch($row['Status']) {
					case 1: echo '<span class="service_declined">Отклонена</span>';break;
					case 2: echo '<span class="service_accepted">Подтверждена</span>';break;
					default: echo '<span class="service_moderate">На модерации</span>';
				}
				?>
			</td>
			<td style="text-align: center;">
				<span name="AddressServices[<?php echo $row['id']?>]">
					<?php echo $row['Price']?>
				</span>
			</td>
			<td>
				<?=$row['Reject']?>
			</td>
		</tr>
		<?php endforeach;?>
	</table>
</div>
<?php endif; ?>

<ul>
	<li>Устанавливая стоимость услуги равной 0, вы обозначаете, что такая услуга является бесплатной</li>
</ul>
<table id="addNewService" class="table-custom vmargin20 displaynone" style="width: 400px;">
	<tr>
		<td style="border-bottom: none;">
			<?php
				$availableServices = Service::model()->findAll("link <> 'zapnp'",['order' => 'name']);
				$availableServicesList = [];
				$availableServicesInfo = [];
				foreach($availableServices as $availableService)
				{
					$availableServicesList[$availableService->id] = $availableService->name;
					$availableServicesInfo[$availableService->id] = [
						'name' => $availableService->name,
						'description' => $availableService->description,
						'empComissionAsAppointment' => $availableService->empComissionAsAppointment,
					];
					
				}
				$this->widget('ESelect2', [
					'name' => 'sService',
					'id' => 'input_s_service',
					'events' => [
						'change' => 'js: function(e) {loadServiceInformation();}',
					],
					'data' => $availableServicesList,
					'options' => [
						'placeholder' => 'Выберите услугу',
						'allowClear' => true,
						'width' => '650px'
					]
				]);
			?>
		</td>
		<td>
			<a id="addNewServiceButton" class="button nl bg-lightgreen c-white" onclick="addService(); return false;" href="#">
				Добавить
			</a>
			<div class="screen_loading displaynone" style="width: 24px; height: 24px;"></div>
		</td>
	</tr>
	<tr>
		<td id="addNewServiceCommission" style="border-top: none; border-bottom: none;" class="vpadding10" colspan="2">
		</td>
	</tr>
	<tr>
		<td id="addNewServiceDescription" style="border-top: none; border-bottom: none;" class="vpadding10" colspan="2">
		</td>
	</tr>
	<tr>
		<td class="vpadding10" colspan="2" style="border-top: none;">
			<a href="#" id="iCannotFindService">Не могу найти услугу, что делать?</a>
			<div class="displaynone">
				<p>
					Если вы убедились, что оказываемой вашей организацией услуги нет в списке, вы можете предложить добавить ее в общий справочник услуг ЕМП.
					Все предлагаемые услуги проходят модерацию, и в случае положительного решения добавляются в общий справочник.
				</p>
				<p>
					<?php
					echo CHtml::link('Моей услуги нет в списке', array('addNew'), array('class' => 'btn'));
					?>
				</p>
			</div>
		</td>
	</tr>
</table>

<h4 class="acting vmargin20 inline-block">Добавленные услуги</h4>

<a id="addNewServiceLink" class="button nl bg-lightgreen c-white inline-block" style="margin-left: 80px;" href="#">Добавить услугу</a>
<?php if($_GET['devMode']): ?>
<a class="button nl bg-blue c-white inline-block" href="getFile">Выгрузить услуги (.csv excel)</a>
<a id="btn_myFileInput" class="button nl bg-yellow c-black inline-block">Загрузить услуги (.csv excel)</a>
<?php endif; ?>
<form class="inline-block" action="<?= $this->createUrl('services/upload')?>" method="post" enctype="multipart/form-data">
	<input id="myFileInput" style="display:none;" type="file" value="" name="file" autocomplete="off">
	<button id="myFileSubmit" style="display:none;" title="Добавить">Загрузить</button>
</form>
<script type="text/javascript">
$(function () {
    $('#btn_myFileInput').data('default', $('label[for=btn_myFileInput]').text()).click(function () {
        $('#myFileInput').click()
    });
    $('#myFileInput').on('change', function () {
        var files = this.files;
        if (files.length) {
        	$('#myFileSubmit').click()
            return;
        }
    });
});
</script>
<table id="AddressServices" class="service-table custom-cells-bg">
	<thead>
		<tr>
			<td>Профиль</td>
			<td>Услуга</td>
			<td style="width: 100px;">Акция?</td>
			<td style="width: 200px;">Цена</td>
			<td style="width: 100px;">Действия</td>
		</tr>
	</thead>
<?php foreach ($rows as $row):?>
<tr data-id="<?=$row['id']?>">
	<td><?=$row['ServiceSectionName']?></td>
	<td>
		<?=($row['link']=='first')? ('<b>'.$row['ServiceName'].'</b>'):$row['ServiceName'];?> 
	</td>
	<td>
		<?php /*
		($row['empComissionAsAppointment'] == true)
			? ((!$this->loadModel()->salesContract->salesContractType) ? 600 : $this->loadModel()->salesContract->salesContractType->isFixed ? 0 : $this->loadModel()->salesContract->salesContractType->price) . " руб. за дошедшего пациента."
			: (AppointmentToDoctors::EMP_COMISSION * 100) . "% от стоимости услуги."
		*/ ?>
		<div class="button bg-lightgreen c-white novpadding service-old-price-show <?= ($row['oldPrice']) ? 'displaynone' : ''; ?>">акция?</div>
		<div class="service-old-price-container <?= (!$row['oldPrice']) ? 'displaynone' : ''; ?>">
			<input type="text" title="старая цена" maxlength="6" class="service-old-price-input" style="width: 60px;" value="<?=$row['oldPrice']?>"></input>
			<span class="button nl bg-red c-white novpadding service-old-price-hide" title="удалить акцию по этой услуге">X</span>
			<div class="service-input-hint <?= (!$row['oldPrice']) ? 'displaynone' : ''; ?>">^ старая <br>цена</div>
		</div>
		<!--<s> 1500 </s>-->
	</td>
	<td>
		<a href="#" name="<?=$row['link']?>"></a>
		<input type="text" maxlength="6" class="service-price-input" style="width: 60px;" value="<?=$row['Price']?>"></input> руб.
		<span class="vhidden button nl bg-lightgreen c-white novpadding">🕑</span>
		<div class="service-input-hint <?= (!$row['oldPrice']) ? 'displaynone' : ''; ?>">^ новая <br>цена</div>
	</td>
	<td>
		<?php if (!$row['isActual']): ?>
		<a class="btn-experts btn-blue align-center update_service_actuality" title="Услуга устарела и скрыта от посетителей сайта, т.к. ее стоимость не обновлялась более 3 месяцев. Нажмите, чтобы подтвердить актуальность" href="#">
			подтвердить актуальность
		</a>
		<?php endif; ?>
		<a class="btn-experts btn-red align-center delete_price" title="удалить эту услугу" href="#">
			удалить
		</a>
		<a class="btn-experts btn-blue align-center add_description" title="Добавить описание" href="description?id=<?=urlencode($row['id'])?>">
			Добавить описание
		</a>
	</td>
</tr>
<?php endforeach;?>
</table>
<script type="text/javascript">
$(function() {
	
	var changePricesQueue = {};
	
	function changePrices($container) {
		var $input = $container.find('.service-price-input');
		var $oldPriceInput = $container.find('.service-old-price-input');
		var serviceId = $container.attr('data-id');
		var key = ('AddressServices[' + serviceId + ']').toString();
		var dataParams = {};
		var $indicator = $input.next();
		
		dataParams[key] = {
			'price': $input.val(),
			'oldPrice': $oldPriceInput.val(),
		};
		$indicator.addClass('bg-yellow');
		$indicator.addClass('c-black');
		$indicator.removeClass('vhidden');
		//console.log(dataParams);
		
        $.ajax({
        	type: "POST",
        	url: '/adminClinic/services/ajaxUpdate',
        	data: dataParams,
			success: function() {
				$indicator.html('сохранено');
				$indicator.removeClass('bg-yellow');
				$indicator.removeClass('c-black');
				window.setTimeout(function() {
					$indicator.html('🕑');
					$indicator.addClass('vhidden');
				}, 3000);
			}
        });
	}
	
	//установить цены на услугу
    $('.service-price-input, .service-old-price-input').on('keydown', function() {
		var $container = $(this).closest('tr');
		window.clearTimeout(changePricesQueue[$container.attr('data-id')]);
		changePricesQueue[$container.attr('data-id')] = window.setTimeout(function(){changePrices($container);}, 500);
	});
	
	//подтвердить актуальность услуги
	$('.update_service_actuality').live('click', function() {
		var $button = $(this);
		var $container = $(this).closest('tr');
		changePrices($container);
		$button.hide('slow');
		return false;
	});
	
	//показать поля для специальной цены (акция)
	$('.service-old-price-show').live('click', function() {
		var $container = $(this).closest('tr');
		var $showButton = $container.find('.service-old-price-show');
		$container.find('.service-old-price-container, .service-input-hint').show();
		$showButton.hide();
	});
	
	//спрятать поля для специальной цены (акция)
	$('.service-old-price-hide').live('click', function() {
		var $container = $(this).closest('tr');
		var $showButton = $container.find('.service-old-price-show');
		var $oldPriceInput = $container.find('.service-old-price-input');
		$oldPriceInput.val('');
		changePrices($container);
		$container.find('.service-old-price-container, .service-input-hint').hide();
		$showButton.show();
		$showButton.css( "display", "inline-block");
	});
	
	$('#addNewServiceLink').live('click', function() {
		$('#addNewService').show('slow');
		
		$('#iCannotFindService').live('click', function() {
			$(this).next().show('slow');
			return false;
		});
		
		$(this).addClass('vhidden');
		return false;
	});
	
	$(".delete_price").live('click', function() {
		if (confirm("Удалить услугу?")) {
			var $deleteButton = $(this);
			var serviceId = $deleteButton.parent().parent().attr('data-id');
			var key = ('AddressServices[' + serviceId + ']').toString();
			var dataParams = {};
			dataParams[key] = 0;
			
			$.ajax({
				type: "POST",
				url: '/adminClinic/services/delete',
				data: dataParams,
			});
			$rowToRemove = $deleteButton.closest("tr");
			
			$rowToRemove.children().each(function() {
				$(this).addClass('bg-yellow');		
				$(this).animate({
						opacity: 0,
					}, 1000, function() {
					$(this).parent().remove();
				});
			});
			
			return false;
		}
	});

	$(".add_description").live('click', function() {
		$.fancybox({
			href: this.href,
		});
		return false;
	});
	
	var url = document.location.toString();
	var anchor = url.substring(url.indexOf("#")+1);		
	var $anchor = $('a[name="' + anchor + '"]');
	
	if ($anchor.length == 0) {return false;}
	
	var $row = $anchor.parent().parent();
	
	$('html, body').stop(true,true).animate({
		scrollTop: $row.offset().top - 10
	}, 800);
	
	$row.children().each(function() {
		$(this).addClass('bg-yellow');		
        $(this).animate({
				opacity: 1,
			}, 8000, function() {
			$(this).removeClass('bg-yellow');
        });
	});
});
	
	var availableServicesInfo = <?=json_encode($availableServicesInfo)?>;
	
	function loadServiceInformation() {
		var serviceId = $('#input_s_service').val();
		var serviceName = availableServicesInfo[serviceId]['name'];
		var serviceDescription = availableServicesInfo[serviceId]['description'];
		var insertedText = '';
		if (serviceDescription && serviceDescription != serviceName)
			insertedText = 'Описание: <br>' + serviceDescription;
		$('#addNewServiceDescription').html(insertedText);
		var commissionText = ((availableServicesInfo[serviceId]['empComissionAsAppointment'] == true)
					? "<?= (!$this->loadModel()->salesContract->salesContractType) ? 600 : $this->loadModel()->salesContract->salesContractType->isFixed ? 0 : $this->loadModel()->salesContract->salesContractType->price ?> руб. за дошедшего пациента."
					: "<?= (AppointmentToDoctors::EMP_COMISSION * 100) ?>% от стоимости услуги.");
		$('#addNewServiceCommission').html('Комиссия: <span style="color:red;">' + commissionText + "</span>");
	}
	
	function addService() {
		var $addServiceButton = $('#addNewServiceButton');
		var serviceId = $('#input_s_service').val();
		if (!serviceId)
			return false;
		$addServiceButton.next().removeClass('displaynone');
		$addServiceButton.remove();
		window.location.href = 'addServiceToAddress?serviceId=' + serviceId;
	}
//-->
</script>