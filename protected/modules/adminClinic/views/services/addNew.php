<h1 class="vmargin20">Добавление новой услуги в справочник ЕМП</h1>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'services-request-form',
    'enableAjaxValidation'=>false,
	'htmlOptions'			 => array(
		'class'=>'admin-form',
		)
)); ?>
<table class="search-result-table">
<tr class="search-result-box">
		<td class="search-result-signup size-14"><?php echo $form->labelEx($model,'name'); ?></td>
		<td class="search-result-info">
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>150)); ?>
			 <?php echo $form->error($model,'name'); ?>
		</td>
	</tr>


 <?php /*?> <td class="search-result-signup size-14">
        <?php echo $form->labelEx($model,'companyActiviteId'); ?>
        <?php echo $form->dropDownList($model,'companyActiviteId', CHtml::listData(CompanyActivite::model()->findAll(array('order'=>'name')), 'id', 'name'), array('empty'=>'(выбрать)', 'style'=>'width:292px;', 'onchange'=>'')); ?>
        <?php echo $form->error($model,'companyActiviteId'); ?>
    </div>
  <?php */?>  
<tr class="search-result-box">
    <td class="search-result-signup size-14">
        <?php echo $form->labelEx($model,'serviceSectionId'); ?></td>
        <td class="search-result-info"><?php echo $form->dropDownList($model,'serviceSectionId', CHtml::listData(ServiceSection::model()->findAll(array('order'=>'name')), 'id', 'name'), array('empty'=>'(выбрать)', 'style'=>'width:292px;', 'onchange'=>'')); ?>        
        <?php echo $form->error($model,'serviceSectionId'); ?>
		</td>
	</tr>
<tr class="search-result-box">
    <td class="search-result-signup size-14">
        <?php echo $form->labelEx($model,'description'); ?></td>
        <td class="search-result-info"><?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'description'); ?>
		</td>
	</tr>
<tr class="search-result-box">
	<td class="search-result-signup size-14">
        <?php echo $form->labelEx($model,'price'); ?></td>
        <td class="search-result-info"><?php echo $form->textField($model,'price',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->error($model,'price'); ?>
		</td>
	</tr>
	<tr class="search-result-box">
		<td class="search-result-signup size-14"></td>
		<td class="search-result-info">
			<a class="btn-red" title="Отменить" href="<?= $this->createUrl('index')?>">Отменить</a>
			<button type="submit" title="Сохранить" class="btn-green">Сохранить</button>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>