<?php if ( Yii::app()->user->hasFlash('error') ): ?>
<div class="flash-error">
	<span><?php echo Yii::app()->user->getFlash('error')?></span>
</div>
<?php endif;?>
<?php
	if (empty($returnUrl)) {
		$returnUrl = empty(Yii::app()->request->urlReferrer) ? "/adminClinic/" : Yii::app()->request->urlReferrer;
	}
?>
<a href="<?= $returnUrl ?>" class="btn">Выход</a>