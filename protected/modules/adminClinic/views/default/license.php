<?php
$this->endWidget();
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-licenses',
	'action'				 => $this->createUrl('default/updateLicenses'),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
		));
/* @var $form CActiveForm */
?>
<h4>Лицензии</h4>
<div id="service-table">
<table class="service-table">
	<tbody>
		<tr>
			<td>Название</td>
			<td>Описание</td>
			<td>Скан</td>
			<td>Действие</td>
		</tr>
		<tr>
			<td>
				<?= CHtml::textField('licenseNumber', '', array('class' => 'custom-text')) ?>
			<td>
				<?= CHtml::textField('licenseKindOfActivity', '', array('class' => 'custom-text')) ?>
			</td>
			<td></td>
			<td>
				<a class="btn-green add_cl" title="Добавить" href="">Добавить</a>
			</td>
		</tr>
		<?php foreach ($model->companyLicenses as $key => $license): ?>
			<tr>
				<td>
					<?= $form->hiddenField($model, "companyLicenses[{$key}][number]") ?>
					<?= $license->number ?>
				</td>
				<td>
					<?= $form->hiddenField($model, "companyLicenses[{$key}][kindOfActivity]") ?>
					<?= $license->kindOfActivity ?>
				</td>
				<td>
					<?= $form->hiddenField($model, "companyLicenses[{$key}][link]") ?>
					<?= CHtml::link(CHtml::image($this->assetsImageUrl . "/update.png"), array('licenseImageUpload', 'link' => $license->link), array('class' => 'fancybox')) ?>
					<?php if ( $license->image ): ?>
						<?= CHtml::link(CHtml::image($this->assetsImageUrl . "/view.png"), $license->imageUrl, array('class' => 'fancybox')) ?>
						<?= CHtml::link(CHtml::image($this->assetsImageUrl . "/delete.png"), array('licenseImageDelete', 'link' => $license->link), array()) ?>
					<? endif; ?>
					<!--<a class="btn" title="Добавить" href="#">Добавить</a>-->
				</td>
				<td>
					<a class="del_cl" href="">Удалить</a>
				</td>
			</tr>
		<? endforeach; ?>

	</tbody>
	<tfoot>
		<tr class="">
			<td class="" colspan="4" style="text-align: center;">
				<a class="btn-red" title="Отменить"href="">Отменить</a>
				<button class="btn-green" type="submit" title="Сохранить">Сохранить</button>
			</td>
		</tr>
	</tfoot>
</table>
</div>
<?php $this->endWidget(); ?>