<?php
//Настройки уведомлений
$this->renderpartial('contactSettings', ['clinic' => $model]);
//Настройки реквизитов
$this->renderpartial('contactRequisites', ['clinic' => $model]);

$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-contacts',
	//'action'				 => CHtml::normalizeUrl(array('/default/updateLogo')),
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data', 'class'=>'admin-form'),
		));
/* @var $form CActiveForm */
?>
<table class="service-table tmargin30">
	<tr>
		<td colspan="2" class="no-border">
			<h1>Часы работы</h1>
			<div class="flash-notice vmargin20">Установить рабочий день выходным можно выбрав начальное и конечное значение "00:00"</div>
		</td>
	</tr>
	<tr>
		<td>
			<label>Обеденный перерыв</label>
			<br>
		<?=$form->error($model->workingHours,"{$day}Start")?>
		</td>
		<td>
			<label>c</label>
			<?php echo $form->dropDownList($model,"breakStart", $model->breakHoursStep,array(
				'style'=>"width:122px"
			));?>					
			<label>по</label>
			<?php echo $form->dropDownList($model,"breakFinish", $model->breakHoursStep,array(
				'style'=>"width:122px"
			))?>					
		</td>
	</tr>
	<tr class="search-result-box odd">
		<td colspan="2" style="min-height:15px; display: block; border: none;">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<label>Заполнить все дни недели</label> 
			<?= $form->checkBox($model, 'workingHours[allCheck]', [
				'onclick' => ' if($(this).is(":checked")) {
					$(".allCheck").attr("disabled",false);
					$(".oneDay").attr("disabled",true);
				} else {
					$(".allCheck").attr("disabled",true);
					$(".oneDay").attr("disabled",false);
				}
				'
			]); ?>
			<br>
		<?=$form->error($model->workingHours,"{$day}Start")?>
		</td>
		<td>
			<label>c</label>
			<?php echo $form->dropDownList($model,"workingHours[allStart]", $model->workingHoursStep,array(
				'style'=>"width:122px",
				'class' => 'allCheck',
				'disabled' => 'true'
			));?>					
			<label>по</label>
			<?php echo $form->dropDownList($model,"workingHours[allFinish]", $model->workingHoursStep,array(
				'style'=>"width:122px",
				'class' => 'allCheck',
				'disabled' => 'true'
			))?>					
		</td>
	</tr>
	<?php foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $key=>$day):?>
		<tr>
			<td>
				<?php echo $form->label($model->workingHours, "{$day}Start")?><br>
				<?=$form->error($model->workingHours,"{$day}Start")?>
			</td>
			<td>
				<label>c</label>
				<?php echo $form->dropDownList($model,"workingHours[{$day}Start]", $model->workingHoursStep,[
					'class' => 'oneDay'
				]);?>				
				<label>по</label>
				<?php echo $form->dropDownList($model,"workingHours[{$day}Finish]", $model->workingHoursStep,[
					'class' => 'oneDay'
				])?>
			</td>
		</tr>
	<?php endforeach;?>
	<tr>
		<td colspan="2" class="vpadding20">
			<button id="saveWorkingHoursButton" class="button nl bg-lightgreen c-white no-border" style="display: inline; line-height: 20px;" name="action" value="save" title="Сохранить" >Сохранить</button>
		</td>
	</tr>
</table>
<?php $this->endWidget(); ?>
<script>
$(function() {
	$('#saveWorkingHoursButton').click(function() {
		$(this).html('Идет сохранение...');
	});
});
</script>