<?php
/* @var $this Controller */
$this->breadcrumbs = array(
	'Вход на сайт',
);

/*Yii::app()->clientScript->registerScript('login-page', '
	$("html, body").animate({
    scrollTop: $("div.search-result").offset().top
}, 100);
', CClientScript::POS_READY)*/
?>

<div class="cont">
	<?php if ( Yii::app()->user->hasFlash('error') ): ?>
	<div class="flash-error">
		<span><?php echo Yii::app()->user->getFlash('error')?></span>
	</div>
	<?php endif;?>
	<div class="middle-auth w500">
		<!--<p class="error">Пароль введен не верно! Попробуйте еще раз.</p>-->
		<?php
		/* @var $form CActiveForm */
		$form	 = $this->beginWidget('CActiveForm', array(
			'id'					 => 'login-form',
			'enableAjaxValidation'	 => true,
		));
		?>
		<?php if ( $error	 = $model->getError('password') ): ?>
			<p class="error"><?= $error ?></p>
		<? endif; ?>
		<fieldset>
			<div class="auth-field">
				<label>Логин</label>
				<?php
					echo $form->textField($model, 'username', array('class' => 'custom-text'));
				?>
				<!--<a title="зарегистрироваться" href="/register" name="">зарегистрироваться</a>-->
			</div><!-- /field -->
			<div class="auth-field">
				<label>Пароль</label>
				<?php
					echo $form->passwordField($model, 'password', array('class' => 'custom-text'));
				?>
				<div class="align-right" style="visibility: hidden;">
					<?php
					echo $form->checkBox($model, 'rememberMe', array('class' => 'niceCheck'));
					?>
					<label for="remember">запомнить</label>
				</div><!-- /align-right -->
			</div><!-- /field -->
			<div>
				<input id="login-button" type="submit" class="btn btn-green bg-green" value="Войти">
				<a href="/" style="margin: 3px 10px 0 0; float: right;">Вернуться назад</a>
			</div>
		</fieldset>
		<?php $this->endWidget(); ?>
		
		<fieldset>
			<div class="auth-field" style="margin-top: 20px;">
			<?php if(Yii::app()->user->hasFlash('sendRecoveryMessage')):?>
			    <div class="flash-success">
			        <?php echo Yii::app()->user->getFlash('sendRecoveryMessage'); ?>
			    </div>
			<?php else: ?>
				<?php
				$forgotPasswordForm	 = $this->beginWidget('CActiveForm', array(
					'id'					 => 'forgotPassword-form',
					'enableAjaxValidation'	 => true,
				));
				?>
				<a href="#forgotPassword" style="margin-top: 20px;" onclick="$('#forgotPassword').slideDown(500); return false;">Забыли пароль?</a>
				<?php $forgotPasswordError = $forgotPasswordModel->getError('username'); ?>
				<div class="auth-field" id="forgotPassword" style="<?= (empty($forgotPasswordError)) ? 'display:none;' : '' ?>">
					<label>Укажите ваш логин или e-mail</label>
					<p class="error"><?= $forgotPasswordError ?></p>
					<?php
						echo $forgotPasswordForm->textField($forgotPasswordModel, 'username', array('class' => 'custom-text'));
					?>
					<div style="text-align: right;">
						<button class="btn btn-green bg-green">OK</button>
					</div>
				</div>
				<?php $this->endWidget(); ?>
			<?php endif; ?>
			</div>
		</fieldset>
			
			<div class="auth-field" style="margin-top: 20px;">
				Еще не зарегистрированы?
				<div class="vmargin10">
					<a href="/site/contact#signup_form">
						Подать заявку на подключение клиники
					</a>
				</div>
			</div>
	</div>
</div>
