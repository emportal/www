<?php
	Yii::app()->clientScript->registerPackage('jquery.maskedinput');
?>
<style>
input[type='text'] {
	width: 350px;
}
input:invalid {
	border: solid 2px red;
}
</style>
<h1>Уведомления</h1>
<table class="service-table vmargin20">
	<tr>
		<td>
			<b>Контактный email</b>
		</td>
		<td>
			<input id="email" type="email" value="<?=$clinic->userMedicals->email?>" required='required' style="width:350px;">
		</td>
		<td>
			На этот email адрес будут приходить email-уведомления о записях на прием
		</td>
	</tr>
	<tr>
		<td>
			<b>Телефон для уведомлений</b>
		</td>
		<td>
			<input id="phoneInput" type="text" value="<?=$clinic->userMedicals->phoneForAlert?>" required='required' placeholder="+7 (###) ###-##-##" style="width:350px;">
		</td>
		<td>
			На этот телефонный номер будут приходить sms-уведомления о записях на прием
		</td>
	</tr>
	<tr>
		<td>
			<b>Login Skype</b>
		</td>
		<td>
			<input id="skypeInput" type="text" value="<?=$clinic->userMedicals->loginSkype?>" style="width:350px;">
		</td>
		<td>
			для связи с клиникой
		</td>
	</tr>
	<tr>
		<td colspan="3" class="vpadding20">
			<a id="saveButton" class="button nl bg-lightgreen c-white" href="#">Сохранить</a>
		</td>
	</tr>
</table>
<script>
$(function() {
	
	var $saveContactsButton = $('#saveButton');
	
	$('#phoneInput').mask("+7 (999) 999-99-99", {placeholder: '_'});
	$saveContactsButton.click(function() {
		var dataParams = {
			'email': $('#email').val(),
			'phoneForAlert': $('#phoneInput').val(),
			'loginSkype':$('#skypeInput').val()
		};
		$saveContactsButton.html('Идет сохранение...');
        $.ajax({
        	type: "POST",
        	url: 'updateContactSettings',
        	data: dataParams,
			success: function(data) {
				//console.log('ajax success, data = ');
				//console.log(data);
				$saveContactsButton.addClass('bg-yellow');
				$saveContactsButton.addClass('c-black');
				$saveContactsButton.removeClass('bg-green');
				$saveContactsButton.removeClass('c-white');
				$saveContactsButton.html('Сохранено');
				window.setTimeout(function() {
					$saveContactsButton.addClass('bg-green');
					$saveContactsButton.addClass('c-white');
					$saveContactsButton.removeClass('bg-yellow');
					$saveContactsButton.removeClass('c-black');
					$saveContactsButton.html('Сохранить');
				}, 1000);
			}
        });
		return false;
	});
});
</script>