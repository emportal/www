<h1>Просмотры клиники на портале за последние 30 дней</h1>

<span id="waitWhileLoading">Идет загрузка...</span>
<canvas id="myChart" width="900" height="450"></canvas>
<?php
	$path = Yii::getPathOfAlias('shared');
	$pathToChart = $path . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'Chart.min.js';
	$urlScript = Yii::app()->assetManager->publish($pathToChart);
	Yii::app()->clientScript->registerScriptFile($urlScript, CClientScript::POS_HEAD);
?>
<script>
window.onload = function() {
	document.getElementById('waitWhileLoading').outerHTML = '';
	var ctx = document.getElementById("myChart").getContext("2d");
	var data = {
	
    labels: <?=$statData['chartLabels']?>,
    datasets: [
        {
            label: "Посещения адреса",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: <?=$statData['chartData']?>
        }
    ]
};
	var myLineChart = new Chart(ctx).Line(data, {showTooltips: false, bezierCurve: true, bezierCurveTension: 0.15});
}
</script>