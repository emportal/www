<?php
/* @var $this Controller */
$this->breadcrumbs = array(
	'Восстановление доступа к личному кабинету',
);
$this->pageTitle = 'Восстановление доступа к личному кабинету - '.Yii::app()->name;
?>

<div class="cont">
	<h1 style="text-align: center;">Восстановление доступа к личному кабинету</h1>
	<?php if ( Yii::app()->user->hasFlash('error') ): ?>
	<div class="flash-error">
		<span><?php echo Yii::app()->user->getFlash('error')?></span>
	</div>
	<?php elseif ( Yii::app()->user->hasFlash('success') ): ?>
	<div class="flash-success">
		<span><?php echo Yii::app()->user->getFlash('success')?></span>
		<script>
			$(function(){
				setTimeout(function() {
					window.location.href = "/adminClinic/default/visits"
				}, 1000);
			});
		</script>
	</div>
	<?php else: ?>
	<div class="middle-auth w500">
		<?php
		$form	 = $this->beginWidget('CActiveForm', array(
			'id'					 => 'recovery-form',
			'enableAjaxValidation'	 => true,
		));
		?>
			<fieldset>
				<div class="auth-field">
					<label>Введите новый пароль</label>
					<?php if ( $error	 = $recoveryModel->getError('newPassword') ): ?>
						<p class="error"><?= $error ?></p>
					<? endif; ?>
					<?php
						echo $form->passwordField($recoveryModel, 'newPassword', array('class' => 'custom-text'));
					?>
				</div>
				<div class="auth-field">
					<label>Повторите новый пароль</label>
					<?php if ( $error	 = $recoveryModel->getError('repeatPassword') ): ?>
						<p class="error"><?= $error ?></p>
					<? endif; ?>
					<?php
						echo $form->passwordField($recoveryModel, 'repeatPassword', array('class' => 'custom-text'));
					?>
				</div>
				<div>
					<input id="recovery-button" type="submit" class="btn btn-green bg-green" value="ОК">
				</div>
			</fieldset>
		<?php $this->endWidget(); ?>
	</div>
	<?php endif;?>
</div>