<?php
/* @var $this Controller */
/* @var $model Clinic */
?>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
'id' => 'mydialog',
        'options' => array(
            'title' => 'Изменение времени',
            'autoOpen' => false,
            'modal' => true,
			'width' => 550,
            'resizable'=> false,
			'draggable'=> false
        ),
    ));
?>
<?php $this->endWidget(); ?>

<style>
.search-result.cabinet {
	padding: 0!important;
	margin: 0!important;
}
</style>
<div class="crm_filters">
	<a href="../crm/newApp" class="btn btn-green" style="float: right; margin: 0px 0 20px 0; display: none;">
		Новая запись
	</a>
	<a class="nl btn-small btn-small-blue inline-block margin4 btn-small-wide" href="#" onclick="change_input_values({'id': 'input_s_date', 'value': '<?=date('d.m.Y', ($startPeriod['time']-1) )?>', 'reload': 1}); return false;"> < </a>
	с <?=date('d.m.Y', ($startPeriod['time']) )?> по <?=date('d.m.Y', ($startPeriod['time'] + 6*24*60*60) )?> 
	<a class="nl btn-small btn-small-blue inline-block margin4 btn-small-wide" href="#" onclick="change_input_values({'id': 'input_s_date', 'value': '<?=date('d.m.Y', ($startPeriod['time']+ 7*24*60*60) )?>', 'reload': 1}); return false;"> > </a>
	
	<input id="input_s_date" value="<?=date('d.m.Y', ($startPeriod['time']) )?>" type="hidden"/>
	
	<div style="float: right;">
		Показывать записи
		<select id="input_s_type" style="width: 180px;" onchange="submit_input_forms();">
			<option value="">по адресу <?=$model->street.', '.$model->houseNumber?></option>
			<option value="1">по всей клинике</option>
			<option value="2">по отдельному врачу</option>
		</select>
		
		<span id="doc_filter">
		<?php
			$this->widget('ESelect2', [
				'name' => 'sDoctor',
				'id' => 'input_s_doctor',
				'events' => [
					'change' => 'js: function(e) {submit_input_forms();}',
				],
				'data' => CHtml::listData($doctors, 'link', 'name'),
				'options' => [
					'placeholder' => 'Выберите врача',
					'allowClear' => true,
					'width' => '300px'
				]
			]);
		?>
		</span>
	</div>
</div>
<style>
.appointment_status0 {
background-color: #F1FFA3;
}
.appointment_status1 {
background-color: #D9F7FF;
}
.appointment_status2 {
background-color: #D9F7FF;
}
.appointment_status3 {
background-color: #D9F7FF;
}
.appointment_status4 {
background-color: #8EFF8A;
}
.appointment_status5 {
background-color: #D9F7FF;
}
</style>


<?php
$arr_day_names = MyTools::getRussianDayNames();
$arr_day_names[-1] = 'Время';
$html_app_template = '';

function curPageURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return urlencode($pageURL);
}
	
function generate_calendar_app_block($data) {
	
	$dataActions = ($data->actions == 'Нет действий') ? '' : $data->actions;	
	$dataActionDelete = ($data->appType == AppointmentToDoctors::APP_TYPE_OWN) ? '<a href="../crm/deleteApp?id='.$data->id.'&callback='.curPageURL().'" class="nl nomp"><div class="btn-small btn-small-red">Удалить</div></a>' : '';
	
	$cellTitle = ($data->serviceId) ? 'Запись на услугу' : AppointmentToDoctors::$STATUSES[$data->statusId];
	
	return '<div class="calendar_app appointment_status'.$data->statusId.'" data-appId="'.$data->id.'">
	
	<div class="calendar_app_header status" id="app_' . $data->link . '">
		'. $cellTitle .'
	</div>	
	
	<div class="calendar_app_block">
		<div class="tr">
			<div>
				Тип
			</div>
			<div>
				Запись на '. $data->getVisitTypeText(true) .'
				<div class="calendar_app_close_button btn-small-red">X</div>
			</div>
		</div>
		<div class="tr">
			<div>
				Адрес
			</div>
			<div>
				'.$data->address->street.' '.$data->address->houseNumber.'
				<div class="calendar_app_close_button btn-small-red">X</div>
			</div>
		</div>
		<div class="tr">
			<div>
				Запланированное время приема
			</div>
			<div>
				'.substr($data->plannedTime, 0, 16).'
			</div>
		</div>
		<div class="tr">
			<div>
				Статус
			</div>
			<div class="status">
				'.AppointmentToDoctors::$STATUSES[$data->statusId].'
			</div>
		</div>
		<div class="tr">
			<div>
				Врач
			</div>
			<div>
				'.CHtml::encode($data->doctor->name).'
			</div>
		</div>
		<div class="tr">
			<div>
				Пациент
			</div>
			<div>
				'.CHtml::encode($data->name).' '.$data->hidePhoneEmail.'
			</div>
		</div>
		<div class="tr">
			<div>
				Дополнительно
			</div>
			<div>
				'. (is_numeric($data->prePaid) ? 'Талон #'.$data->getVoucherNumber().'<br>' . (CHtml::encode($data->prePaid) . ' рублей предоплачено через программу лояльности Единого Медицинского Портала') : '') .'
			</div>
		</div>
		<div class="tr">
			<div>
				Действия
			</div>
			<div class="actions" style="line-height: 1.0;">
				<a href="../crm/editApp?id='.$data->id.'&callback='.curPageURL().'" style="display:'.($data->appType == AppointmentToDoctors::APP_TYPE_OWN ? '' : 'none').'" class="nl nomp"><div class="btn-small btn-small-green">Редактировать</div></a>
				'.$dataActionDelete.$dataActions.'
			</div>
		</div>
	</div>
	</div>';
}
?>

<?php
$allApps = [];
foreach($dataProvider as $data) {
	
	$arrkey = date("d.m.Y-H-i", strtotime($data->plannedTime));
	$allApps[$arrkey][] = $data;
	//echo '<br> arrkey = '.$arrkey.';';
}
?>
<div id="calendar_loading" class="center-x center-y" style="z-index:1500; position: fixed !important; top: 200px !important; width: 90px; height: 90px; background: ghostwhite;border: 1px solid #ccc;border-radius: 10px; display:none;">
	<div class="screen_loading" style="height: 45px;"></div>
	<div style=" text-align: center; height: 35px; font-size: 15px;">Идёт обновление</div>
</div>
<div id="calendar_loading_successful" style="display: none;">
	<div class="center-x center-y" style="z-index: 1500; width: 110px; height: 50px; border: 1px solid rgb(204, 204, 204); border-radius: 10px; position: fixed !important; top: 200px !important; background: palegreen; display: table;">
		<div style=" text-align: center; display: table-cell; vertical-align: middle; font-size: 15px;">Информация обновлена</div>
	</div>
</div>
<div id="calendar_loading_error" style="display: none;">
	<div class="center-x center-y" style="z-index: 1500; width: 110px; height: 50px; border: 1px solid rgb(204, 204, 204); border-radius: 10px; position: fixed !important; top: 200px !important; background: mistyrose; display: table;">
		<div style=" text-align: center; font-size: 15px; display: table-cell; vertical-align: middle;">Произошла ошибка</div>
	</div>
</div>

<table class="ctable fixed_table_layout calendar_table vmargin">
	
	<tr>
		<?php
			for($i=-1; $i<7; $i++) {
				
				$addtxt = ($i == -1) ? '' : '<br>'.date('d.m.Y', ($startPeriod['time'] + $i*24*60*60) ); echo '<td>'.$arr_day_names[$i].''.$addtxt.'</td>';
			}
		?>
	</tr>
	
	<?php 
	
		$html_table = '';
		$html_tr = '';
		$html_apps = '';
		$cursor_time = $startPeriod['time'];
		$seconds_in_day = 24 * 60 * 60;
		for($ii = $activeHours['minStartHour']; $ii <= $activeHours['maxFinishHour']; $ii+=0.5) {
			
			$html_tr_td = '';			
			for($i=-1; $i<7; $i++) {
				
				$html_apps = '';				
				if ($i === -1) {
					$shown_hours = floor($ii);
					$shown_minutes = ($ii - $shown_hours) * 6 . '0';
					$html_tr_td .= '<td style="vertical-align: middle;">'.$shown_hours.':'.$shown_minutes.'</td>';
				} else {
					
					$used_time = $cursor_time + ($seconds_in_day*$i) + $ii*60*60;
					$arrkey = date("d.m.Y-H-i", strtotime(date('d.m.Y H:i', $used_time )));
					if ($allApps[$arrkey]) {
						
						foreach($allApps[$arrkey] as $data) {
							
							$result = generate_calendar_app_block($data);
							$html_apps .= $result;
						}
					}					
					$html_tr_td .= '<td><a href="../crm/newApp?plannedTime='.date('Y-m-d H:i', $used_time).'" class="nl add" title="добавить новую запись на это время">+</a>'.$html_apps.'</td>';
				}
			}			
			$html_tr .= '<tr class="calendar_row">'.$html_tr_td.'</tr>';
		}
		
		echo $html_tr;
	
	?>
	
</table>
<div style="padding: 0 20px;">
	<a href="contacts">Настроить</a> телефон для sms-уведомлений о записях на прием
</div>
<script> var submit_path = '?'; </script>
<script>
//$(document).ready(function() {
	var data_values = {
		
		's_date': '<?=date('d.m.Y', $startPeriod['time']) ?>',
		's_type': '<?=Yii::app()->request->getParam('s_type') ?>',
		's_doctor': '<?=Yii::app()->request->getParam('s_doctor') ?>',
	},
		shown_app_id = '<?=Yii::app()->request->getParam('link') ?>';
	;
	
	
	function update_input_forms() {
		
		for(var key in data_values) {
			if (!data_values[key]) data_values[key] = ' ';
			if (typeof $('#input_'+key) != 'undefined') {
				
				if ($('#input_'+key).prop('type') == 'select-one') {
				
					$('#input_'+key+' option').each(function(){
						
						if (this.value == data_values[key]) {
							
							$('#input_'+key).val(data_values[key]);
						}
					});
				} else {
					$('#input_'+key).val(data_values[key]);
				}			
			}
		}
		//!!
		check_doctor_filter();
	}
	
	
	function submit_input_forms() {
		
		var query = '',
			url = '';
			
		if ( $('#input_s_type').val() != 2 ) $('#input_s_doctor').val(''); //!!
			
		for(var key in data_values) {
			
			if ($('#input_'+key).length) { //если существует на странице такой DOM элемент
				
				data_values[key] = $('#input_'+key).val();
				if (data_values[key] != '') {query += '&' + key + '=' + data_values[key];}
			}
		}
		url = submit_path + query;
		document.location.href = url;
	}
	
	$(document).keypress(function(e) {
		
		//if(e.which == 13) {submit_input_forms();}
	});
	
	function change_input_values(options) { //required: id, value
		
		$('#' + options.id).val(options.value);
		if (options.reload == 1) {submit_input_forms();}
	}
	
	function check_doctor_filter() {
		
		var $el = $('#input_s_type'),
			$doc_filter = $('#doc_filter');
			
		if ($el.val() == 2) {			
			$doc_filter.show();
		} else {			
			$doc_filter.hide();
		}
	}
	
	function show_app(obj) {
		
		var $this = obj,
			$appBlock = $this.parent(),
			$appId = $appBlock.attr('data-appId'),
			$popupWindow = $appBlock.find('.calendar_app_block')
			;
		$('.dark_bg').css('display', 'block');
		$popupWindow.css('display', 'table');
		
		$('html, body').animate({
			scrollTop: ($popupWindow.offset().top - 50)
		}, 100);
		
		$popupWindow.find('.calendar_app_close_button').click(function() {
			$('.dark_bg').css('display', 'none');
			$popupWindow.css('display', 'none');
		});
		
		$('.dark_bg').click(function() {
			if($popupWindow.css('display') !== 'none') {
				$popupWindow.find('.calendar_app_close_button').click();
			}
		});
	}
	
	$( document ).ready(function() {
		update_input_forms();
		
		$('.calendar_app_header').live('click', function() {
			show_app( $(this) );
		});
		
		if (shown_app_id) {
			var $elem = $('#app_' + shown_app_id);
			if ($elem.length == 1) show_app($elem);
		}
	});
</script>