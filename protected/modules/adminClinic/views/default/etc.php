<?php
$appFormCodeString = "	<span id=\"empAppFormSpan\"></span>
	<script>
	(function(o) {
		var r = new XMLHttpRequest();
		r.onreadystatechange = function(data) {
			if (r.readyState == 4 && r.status == 200) {
				var insertSpan = document.getElementById(\"empAppFormSpan\");
				insertSpan.innerHTML = r.responseText;
				var insertedScript = insertSpan.getElementsByTagName('script')[0];
				eval(insertedScript.innerHTML);
			}
		}
		var params = '?id=' + o.company;
		if (o.appType)
			params += '&at=' + o.appType;
		if (o.address) params += '&a=' + o.address;
		r.open('GET', (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//' + o.source + params, true);
		r.send();
	})({
		'source': '" . $appFormSource . "',
		'company': '" . $clinic->company->linkUrl . "',
		'address': '" . $clinic->linkUrl . "'
	})
	</script>";
?>

<h1 class="vmargin20">Форма записи для вашего сайта</h1>
<div class="vmargin20">
	<h2>Что это такое</h2>
	<p>
		Форма записи на прием для внешних сайтов - это виджет, позволяющий организовать онлайн запись к врачам вашей клиники не только на сайте emportal.ru, но и на любом другом (например, на сайте вашей клиники).
	</p>
	<h2>Что в этом хорошего</h2>
	<p>
		 + Онлайн-запись на прием на вашем сайте увеличивает его конверсию (количество записавшихся пациентов)
		<br> + Вы не платите за пациентов, записывающихся через эту форму
		<br> + Информация о вашей клинике (часы работы, специалисты и пр.) уже заполнена на ЕМП и подтягивается в форму
		<!-- <br> + Еще что-то -->
	</p>
	<h2>Сколько это стоит</h2>
	<p>
		Форма <b>бесплатна</b>, как и все записи на прием, совершенные через нее.
		<br>Иными словами, <b>вы не платите ЕМП за пациентов, записавшихся через форму, установленную на вашем сайте</b>.
	</p>
	<h2>Как это установить</h2>
	<p>
		Для установки формы на вашем сайте вам следует скопировать код формы (см. ниже) и вставить его на страничку (-и) вашего сайта, 
		<br>по аналогии с тем, как в нее вставляются коды счетчиков посещаемости сайта.
		<br>При возникновении вопросов вы можете связаться с нашим менеджером.
	</p>
</div>
<div class="vpadding20">
	<div class="appFormBlock">
		<?=$appFormCodeString?>
	</div>
	<div class="appFormBlock">
		Код формы для установки на сайт
		<div class="code vmargin10">
			<?= nl2br(htmlspecialchars($appFormCodeString)); ?>
		</div>
	</div>
</div>


