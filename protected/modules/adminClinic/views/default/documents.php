<script>
$(function() {
	$('#newOffer').live('click', function() {
		var btn = $(this);
		$.ajax({
			url: "/adminClinic/default/documents",
			async: false,
			data: ({
				json: 1,
				offer: 1,
			}),
			dataType: 'json',
			type: "GET",
			success: function(msg) {
				if (msg.success) {
					$('#newOfferStatus').show();
					btn.remove();
				}
			},
		});
	});
});
</script>
<h1 class="vmargin10">Юридические документы</h1>
<table class="service-table">
	<tbody>
		<tr>
			<td>
				Договор-оферта
			</td>
			<td>
				<?php $newContractType = [10,11,12,13,14]; ?>
				<?php if(in_array($salesContract->salesContractType->id, $newContractType)): ?>
					<a href="/uploads/regions/<?= Yii::app()->user->model->address->city->subdomain ?>/offerta_new.doc">скачать</a>
				<?php else: ?>
					<a href="/uploads/regions/<?= Yii::app()->user->model->address->city->subdomain ?>/offerta.doc">скачать</a>
				<?php endif; ?>
			</td>
			<td>
				<?php if (Yii::app()->user->model->agreementNew): ?>
					<span>принята вами</span>
				<?php else: ?>
					<span id="newOfferStatus" style="display:none;">принята вами</span>
					<span id="newOffer" class="btn btn-green" href="">Принять</span>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td>
				Дополнительное соглашение на перевод на тариф
			</td>
			<td colspan="2">
				<a href="/uploads/dop_soglashenie_na_perevod_na_tarif.docx">скачать</a>
			</td>
		</tr>
	</tbody>
</table>
<h1 class="vmargin10">Тарификация</h1>
<table class="service-table /*bordered-cells*/">
	<tr>
		<td style="width: 750px;" colspan="2">Действующий тариф</td>
		<td><?=$salesContract->salesContractType->description?> <a href="#ContactManager" onclick="mdContactManager.show({ msgbody: '', subject: 1 }); return false;" class="float_right">запрос смены тарифа</a></td>
	</tr>
	<tr>	
		<td colspan="2">Описание тарифа</td>
		<td><?=$salesContract->salesContractType->descriptionExtended?></td>
	</tr>
	<tr>
		<td>Возможность пациентов записываться на услуги</td>
        <td class="width100 t-center-align">
            <input id="agreementServiceProgramInput" data-type="agreementService" type="checkbox" <?php if ($clinic->userMedical->agreementService) echo 'checked'; ?>>
        </td>
		<td>
			включите для возможности пациентов записаться не только к конкретному врачу, но и на услуги
		</td>
	</tr>
<?php if(Yii::app()->params['regions'][$clinic->city->subdomain]['loyaltyProgram']): ?>
	<tr>	
		<td>Участие в программе лояльности</td>
        <td class="width100 t-center-align">
            <input id="agreementLoyaltyProgramInput" data-type="agreementLoyaltyProgram" type="checkbox" <?php if ($clinic->userMedical->agreementLoyaltyProgram) echo 'checked'; ?>>
        </td>
		<td>
			прием записавшихся пациентов ЕМП за бонусные баллы
			<br><a href="/adminClinic/default/loyalty">Как это работает</a>
		</td>
	</tr>
<?php endif; ?>
</table>
<?php
$criteria = new CDbCriteria();
$criteria->compare('t.addressId', $clinic->id, false);
if (!$clinic->salesContract->salesContractType->isFixed)
    $criteria->addCondition('t.reportMonth != "' . date('Y-m') . '"');
$criteria->addCondition("UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(t.reportMonth, '-01'), '%Y-%m-%d')) >= UNIX_TIMESTAMP(STR_TO_DATE('2015-07-01', '%Y-%m-%d'))");
$criteria->order = "t.reportMonth";
$bills = Bill::model()->findAll($criteria);
$billsHtTML = '';
foreach($bills as $bill) {
    $paymentStatusHTML = '';
    if (floatval($bill->sum) > 0) {
    	if ($bill->paymentStatusId) {
    		$paymentStatusHTML = 'оплачен';
    	} else {
    		$paymentStatusHTML = '<span class="button bg-red c-white cursordefault">не оплачен</span>';
    	}
    } else {
        $paymentStatusHTML = '';
    }
    $billsHtTML .=
    '<tr>
        <td>' . $bill->id . '</td>
        <td>' . MyTools::convertReportMonthToPeriodName($bill->reportMonth) . '</td>
        <td>
            <a href="bill?reportMonth=' . $bill->reportMonth . '" onclick="pagePopUp(this,\'Распечатать счёт\'); return false;">распечатать</a>
        </td>
        <td>
           <a href="matching?reportMonth=' . $bill->reportMonth . '" onclick="pagePopUp(this,\'Распечатать акт\'); return false;">распечатать</a>
        </td>
        <td>' . $bill->sum . ' руб.</td>
        <td>' . $paymentStatusHTML . '</td>
        <td>' . ($bill->isDebt ? '<a href="documents?reportMonth=' . $bill->reportMonth . '#payOnline">оплатить</a>' : '') . '</td>
    </tr>';
}

?>
<a href="#" name="bills" class="nl"></a>
<h1 class="vmargin20">
	Счета
</h1>
<table class="service-table">
    <tr>
        <td>№</td>
        <td>Месяц</td>
        <td>Счет</td>
        <td>Акт</td>
        <td>Сумма</td>
        <td>Статус</td>
        <td>Действие</td>
    </tr>
<?= $billsHtTML ?>
</table>
<div style="color: red; font-weight: bold;"><?= ($clinic->debtSum > 0) ? 'Задолженность по оплате составляет ' . $clinic->debtSum . ' руб.' : ''; ?></div>
<?php
if ($bill->isDebt) :

$bill->updateFigures(false); 
?>
<a href="#" name="payOnline" class="nl"></a>
<h1 class="vmargin20">
	Оплата онлайн (счет за <?= $periodName ?>)
</h1>
<p>
	<!-- тестовый обработчик: https://demomoney.yandex.ru/eshop.xml -->
	<!-- боевой обработчик: https://money.yandex.ru/eshop.xml -->
	<form name="14f40c4743d6cb8f_ShopForm" method="POST" action="https://money.yandex.ru/eshop.xml" target="_blank">
	
	<br><input name="paymentType" value="PC" type="radio" checked=""> Яндекс деньги
	<br><input name="paymentType" value="AC" type="radio"> Банковская карта
	<br><br>
	
	<!-- Обязательные поля --> 
	<input name="shopId" value="<?= Yii::app()->params['api']['kassa']['shopId'] ?>" type="hidden"/> 
	<input name="scid" value="<?= Yii::app()->params['api']['kassa']['scid'] ?>" type="hidden"/> 
	<input name="sum" value="<?= $bill->sum ?>" type="text" readonly> руб 
	<input name="customerNumber" value="c<?= $clinic->link ?>" type="hidden"/>
	<input name="orderNumber" value="b<?= $bill->id ?>" type="hidden"/>
	
	<!-- Необязательные поля --> 
	<!-- <input name="shopArticleId" value="001" type="hidden"/>
	<input name="cps_phone" value="79110000000" type="hidden">
	<input name="cps_email" value="user@domain.com" type="hidden">-->
	<input name="shopSuccessURL" value="<?= Yii::app()->getBaseUrl(true) ?>/adminClinic/default/documents?paymentSuccess=true" type="hidden">
	<input name="shopFailURL" value="<?= Yii::app()->getBaseUrl(true) ?>/adminClinic/default/documents?paymentSuccess=false" type="hidden">
	<input name="billNumber" value="<?= $bill->id ?>" type="hidden">
	<input id="yandexPaymentButton" type="submit" class="btn-green" value="Оплатить"/> 
	</form>
</p>
<?php Yii::app()->clientScript->registerScript("yandexPaymentButtonHandler","
		$('#yandexPaymentButton').click(function(){
			$.ajax({
				url: '/adminClinic/ajax/log?typeName=payViaKassa&reportMonth=".$reportMonth."',
				async: true,
				dataType: 'json',
				type: 'GET',
				success: function() {},
			});
		});
		", CClientScript::POS_READY);
?>
<?php endif; ?>
<?php if (!!$bill->paymentStatusId && !!$bill->sum) :
?>
<h1 class="vmargin20">
	Оплата счета
</h1>
<p>
	Счет на <?= $bill->sum ?> руб. за <?=$periodName?> оплачен.
</p>
<?php endif; ?>

<h1 class="vmargin20">
	<a name="receivedPatients" href="#receivedPatients" class="nl cursordefault">
		Принятые пациенты (<?=$periodName?>)
	</a>
</h1>
<span style="float: right; font-size: large;">
	<select id="reportMonthSelector" style="float: right;">
		<?php
        $showOptionsForReportMonthSelector = function() {
            $dateStart = new DateTime("2015-07");
        	$dateNow = new Datetime();
        	$yearDiff = $dateStart->diff($dateNow)->y;
            $monthDiff = $yearDiff*12 + $dateStart->diff($dateNow)->m + 1;
            for($i = 0; $i < $monthDiff; $i++) {
        		$value = $dateStart->format('Y-m'); //date('Y') . '-' . sprintf('%02d', $i);
        		$name = MyTools::getRussianMonthsNames()[$dateStart->format('n') - 1] . ' ' . $dateStart->format('Y');
        		$dateStart->modify('+1 month');
        		echo '<option value="' . $value . '"> ' . $name . ' </option>';
            }
        };
        $showOptionsForReportMonthSelector();
		?>
	</select>
	<div class="clearfix" style="margin-bottom: 20px;"></div>
	<?php if (!$bill->paymentStatusId) : ?>
		Сумма к оплате: <span class="totalSale"><?=$totalSale?></span> рублей
	<?php endif; ?>
</span>

<?php if ($totalSale > 0) : ?>
<p>
	<!-- <a href="?createReport=true">Скачать отчетом</a> -->
	<?php if (date('j') > Yii::app()->params['lastDayForAppCollation'] AND strtotime($reportMonth) < strtotime('now -1 month')) : ?>
	<br/>
	<a href="/adminClinic/default/bill?download=1&reportMonth=<?=$reportMonth?>">Скачать счёт</a> 
	<br/>
	<a href="/adminClinic/default/bill?reportMonth=<?=$reportMonth?>" onclick="pagePopUp(this,'Распечатать счёт'); return false;">Распечатать счёт</a>
	<?php endif; ?>
	<br/>
	<a href="/adminClinic/default/matching?download=1&reportMonth=<?=$reportMonth?>">Скачать сверку / акт об оказании услуг</a>
	<br/>
	<a href="/adminClinic/default/matching?reportMonth=<?=$reportMonth?>" onclick="pagePopUp(this,'Распечатать сверку'); return false;">Распечатать сверку / акт об оказании услуг</a>
	<script type="text/javascript">
	function pagePopUp(obj,title) {
		var page = window.open(obj.href, title, 'width=1000,height=700,resizable=yes,scrollbars=yes,status=no,directories=no,location=no,toolbar=no,menubar=no');
		page.addEventListener(  'load', function(){page.print();}, false);
    }
	</script>
</p>
<?php endif; ?>

<div style="clear: both;">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => $dataProvider,
	'ajaxUpdate'=>false,
	'itemsCssClass' => 'vtable',
	'columns' => array(
		[
			'name' => 'createdDate',
			'header' => 'Поступление заявки',
			'value' => function($data) {
					return substr($data->createdDate, 0, -3);
				}
		],
		[
			'name' => 'plannedTime',
			'header' => 'Предварительное время приема',
			'value' => function($data) {
					return substr($data->plannedTime, 0, -3);
				},
		],
		[
			'name' => 'type',
			'header' => 'Тип',
			'value' => '$data->serviceTypeText'
		],
		[
			'name' => 'plannedTime',
			'header' => 'Врач/Услуга',
			'value' => '$data->visitTypeSubject',
		],
		[
			'name' => 'name',
			'header' => 'Пациент',
			'value' => function($data) {
					return CHtml::encode($data->name) . ' ' . CHtml::encode($data->phone);
				},
			'type' => 'raw',
		],
		[
			'name' => 'totalPrice',
			'header' => 'Стоимость',
			'value' => '$data->totalPriceStr'
		],
		[
			'name' => 'empComission',
			'header' => 'Комиссия ЕМП',
			'value' => '$data->empComissionStr'
		],
		[
			'name' => 'noname',
			'header' => 'Отметка клиники',
			'value' => function($data) {
					
					$adminClinicStatusId = ($data->willBePayedFor) ? 0 : 1;
					
					$output = '';
					$output .= '<span class="button_wrapper" data-managerStatusId="' . $adminClinicStatusId . '" data-link="' . $data->link . '">';
					$output .= '<a href="#" data-statusId="2" class="button fullwidth bg-darkgreen c-white nl non-active btn-manager-ok">Подтверждено</a>';
					$output .= '<a href="#" data-statusId="1" class="button fullwidth bg-darkblue c-white nl non-active btn-manager-not-ok">Отклонено</a>';
					$output .= '</span>';
					return $output;
				},
			'type' => 'raw',
			'htmlOptions' => [
				'style' => 'width: 108px;'
			]
		],
		[
			'name' => 'noname',
			'header' => 'Комментарий',
			'value' => function($data) {
					$output = '';
					$output .= '<textarea class="manager-comment">' . $data->adminClinicComment . '</textarea>';
					$output .= '<a href="#" data-link="' . $data->link . '" class="button fullwidth c-black nl non-active btn-manager-comment-save" style="background-color: #eee; display: block;">Сохранить</a>';
					return $output;
				},
			'type' => 'raw',
		],
	),
));
?>
</div>
<script>
function listen_button_clicks() {
	var button_status_mapper = {
		//0: 'btn-manager',
		0: 'btn-manager-ok',
		1: 'btn-manager-not-ok',
		//3: 'btn-manager-visited',
		//4: 'btn-manager-not-visited',
		//5: 'btn-manager-should-check-if-visited'
	};
	var $buttons = $('.btn-manager, .btn-manager-ok, .btn-manager-not-ok, .btn-manager-visited, .btn-manager-not-visited, .btn-manager-should-check-if-visited');
	var $button_wrappers = $('.button_wrapper');
	
	$buttons.click(function() {
		var $button = $(this);
		var $button_wrapper = $button.parent();
		
		if ( $button.hasClass('btn-manager') || $button.hasClass('btn-manager-should-check-if-visited') ) {
			
			$button_wrapper.children().css('display', 'inline-block');
			$button.hide();
			$button_wrapper.addClass('is_opened');
		} else {
			
			var existingManagerStatusId = $button_wrapper.attr('data-managerStatusId');
			var ajaxParams = {
				'appointmentLink': $button_wrapper.attr('data-link'),
				'attributeName': 'adminClinicStatusId',
				'attributeNewValue': $button.attr('data-statusId'),
				'reportMonth': '<?=$reportMonth?>',
				//'scenario': 'changeAdminClinicStatus',
			};
			
			if (!$button_wrapper.hasClass('is_opened')) {
				//console.log('показываем кнопки на выбор');
				$button_wrapper.children().css('display', 'inline-block');
				$button_wrapper.find('.btn-manager, .btn-manager-should-check-if-visited').hide();
				$button_wrapper.addClass('is_opened');
			} else {
				//console.log('прячем все кнопки');
				$button_wrapper.children().hide();
				$button_wrapper.removeClass('is_opened');
				
				$.ajax({
					url: '/adminClinic/ajax/changeAppointmentAttribute',
					async: true,
					data: ajaxParams,
					dataType: 'json',
					type: 'GET',
					success: function(response) {
						if (response.success == 'false') {
							alert(response.msg);
							document.location.reload();
							return false;
						}	
						$(".totalSale").text(response["totalSale"]);	
					
						
					},
				});
			}
			$button_wrapper.parent().parent().removeClass("reminder_highlight")
			$button_wrapper.attr('data-managerStatusId', ajaxParams.newManagerStatusId);
			$button.css('display', 'inline-block');
		}
		return false;
	});
	
	$.each($button_wrappers, function() {
		var $button_wrapper = $(this);
		var $button_wrapper_buttons = $button_wrapper.find($buttons);
		var managerStatusId = $button_wrapper.attr('data-managerStatusId');
		var button_class_to_show = '.' + button_status_mapper[managerStatusId];		
		$button_wrapper_buttons.hide();
		$button_wrapper.find( button_class_to_show ).css('display', 'inline-block');
	});
	
	//manager Comments
	var save_manager_comment = function(appointmentLink, newValue, callback) {
		$.ajax({
			url: '/adminClinic/ajax/changeAppointmentAttribute',
			async: true,
			data: {
				'appointmentLink': appointmentLink,
				'attributeName': 'adminClinicComment',
				'attributeNewValue': newValue,
				'reportMonth': '<?=$reportMonth?>',
				//'scenario': 'changeAdminClinicStatus',
			},
			dataType: 'json',
			type: 'GET',
			success: function(msg) {
				callback(msg);
			},
		});
	}
	
	var $save_buttons = $('.btn-manager-comment-save');
	$save_buttons.click(function() {
		var $button = $(this);
		var appointmentLink = $button.attr('data-link');
		var newValue = $button.prev().val();
		$button.html('...🕑...');
		//console.log('should save');
		save_manager_comment(appointmentLink, newValue, function(ajaxMessage) {
			if (ajaxMessage.success == 'false') {
				alert(ajaxMessage.msg);
				$button.html('Ошибка');
				$button.removeClass('bg-darkgreen bg-red c-white c-black');
				$button.addClass('bg-red c-white');
				window.setTimeout(function() {
					$button.removeClass('bg-darkgreen bg-red c-white c-black');
					$button.addClass('c-black');
					$button.html('Сохранить');
				}, 500);
				return false;
			}
			$button.html('ОК');
			$button.addClass('bg-darkgreen c-white');
			$button.removeClass('c-black');
			window.setTimeout(function() {
				$button.addClass('c-black');
				$button.removeClass('bg-darkgreen c-white');
				$button.html('Сохранить');
			}, 500);
		});
		return false;
	});
	$('#reportMonthSelector').val('<?=$reportMonth?>');
	$('#reportMonthSelector').change(function() {
		//console.log('new value = ' + $(this).val());
		var dl = document.location;
		console.log(dl);
		dl.href = dl.origin + dl.pathname + '?reportMonth=' + $(this).val() + '#receivedPatients';
	});
	
	$('#agreementLoyaltyProgramInput, #agreementServiceProgramInput').change(function() {
        //console.log($(this).attr('data-action'));
		$.ajax({
			url: '/adminClinic/ajax/changeUserAttribute',
			async: true,
			data: {
				'accept':  $(this).is(":checked"),
                'type': $(this).attr('data-type'),
			},
			dataType: 'json',
			type: 'GET',
		});
	});
}

listen_button_clicks();
</script>
