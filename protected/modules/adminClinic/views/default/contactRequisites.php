<?php
	Yii::app()->clientScript->registerPackage('jquery.maskedinput');
?>
<style>
input[type='text'] {
	width: 350px;
}
input:invalid {
	border: solid 2px red;
}
</style>
<h1>Реквизиты</h1>
<table id="saveRequisitesForm" class="service-table vmargin20">
	<tr>
		<td class="w200">
			<b>
				<?php echo CHtml::label($clinic->attributeLabels()['legalINN'], 'legalINN'); ?>
			</b>
		</td>
		<td>
			<?php echo CHtml::textField('legalINN', $clinic->{'legalINN'}, array('class'=>'','placeholder'=>'','required'=>'required','style'=>'width:350px;')); ?>
		</td>
		<td>
			<?php echo CHtml::label($clinic->attributeDescription()['legalINN'], 'legalINN'); ?>
		</td>
	</tr>
	<tr>
		<td class="w200">
			<b>
				<?php echo CHtml::label($clinic->attributeLabels()['legalKPP'], 'legalKPP'); ?>
			</b>
		</td>
		<td>
			<?php echo CHtml::textField('legalKPP', $clinic->{'legalKPP'}, array('class'=>'','placeholder'=>'','required'=>'required','style'=>'width:350px;')); ?>
		</td>
		<td>
			<?php echo CHtml::label($clinic->attributeDescription()['legalKPP'], 'legalKPP'); ?>
		</td>
	</tr>
	<tr>
		<td class="w200">
			<b>
				<?php echo CHtml::label($clinic->attributeLabels()['legalOGRN'], 'legalOGRN'); ?>
			</b>
		</td>
		<td>
			<?php echo CHtml::textField('legalOGRN', $clinic->{'legalOGRN'}, array('class'=>'','placeholder'=>'','required'=>'required','style'=>'width:350px;')); ?>
		</td>
		<td>
			<?php echo CHtml::label($clinic->attributeDescription()['legalOGRN'], 'legalOGRN'); ?>
		</td>
	</tr>
	<tr>
		<td class="w200">
			<b>
				<?php echo CHtml::label($clinic->attributeLabels()['legalName'], 'legalName'); ?>
			</b>
		</td>
		<td>
			<?php echo CHtml::textField('legalName', $clinic->{'legalName'}, array('class'=>'','placeholder'=>'','required'=>'required','style'=>'width:350px;')); ?>
		</td>
		<td>
			<?php echo CHtml::label($clinic->attributeDescription()['legalName'], 'legalName'); ?>
		</td>
	</tr>
	<tr>
		<td class="w200">
			<b>
				<?php echo CHtml::label($clinic->attributeLabels()['legalAddress'], 'legalAddress'); ?>
			</b>
		</td>
		<td>
			<?php echo CHtml::textField('legalAddress', $clinic->{'legalAddress'}, array('class'=>'','placeholder'=>'','required'=>'required','style'=>'width:350px;')); ?>
		</td>
		<td>
			<?php echo CHtml::label($clinic->attributeDescription()['legalAddress'], 'legalAddress'); ?>
		</td>
	</tr>
	<tr>
		<td class="w200">
			<b>
				<?php echo CHtml::label($clinic->attributeLabels()['legalPostalAddressStreet'], 'legalPostalAddressStreet'); ?>
			</b>
		</td>
		<td>
			<span>Город: </span><?php echo CHtml::textField('legalPostalAddressCity', $clinic->{'legalPostalAddressCity'}, array('class'=>'','placeholder'=>$clinic->attributeLabels()['legalPostalAddressCity'],'required'=>'required','style'=>'width:180px;')); ?>
			<br>
			<div style="display: inline-block;">
			<span>Улица: </span><?php echo CHtml::textField('legalPostalAddressStreet', $clinic->{'legalPostalAddressStreet'}, array('class'=>'','placeholder'=>$clinic->attributeLabels()['legalPostalAddressStreet'],'required'=>'required','style'=>'width:180px;')); ?>
			<span>Дом: </span><?php echo CHtml::textField('legalPostalAddressHouse', $clinic->{'legalPostalAddressHouse'}, array('class'=>'','placeholder'=>$clinic->attributeLabels()['legalPostalAddressHouse'],'required'=>'required','style'=>'width:70px;')); ?>
			</div>
			<br><span>Индекс: </span><?php echo CHtml::textField('legalPostalAddressCode', $clinic->{'legalPostalAddressCode'}, array('class'=>'','placeholder'=>$clinic->attributeLabels()['legalPostalAddressCode'],'required'=>'required','style'=>'width:130px;')); ?>
		</td>
		<td>
			<?php echo CHtml::label($clinic->attributeDescription()['legalPostalAddressStreet'], 'legalPostalAddressStreet'); ?>
		</td>
	</tr>
	
	<tr>
		<td colspan="3" class="vpadding20">
			<a id="saveRequisitesButton" class="button nl bg-lightgreen c-white" href="#">Сохранить</a>
			<div class="saveRequisitesFormResponse" style="margin: 15px 15px 0 0;"></div>
		</td>
	</tr>
</table>
<script>
$(function() {
	var $saveRequisitesButton = $('#saveRequisitesButton');
	
	$saveRequisitesButton.click(function() {
		var dataParams = {
			'legalName': $('#legalName').val(),
			'legalAddress': $('#legalAddress').val(),
			'legalINN': $('#legalINN').val(),
			'legalPostalAddressCity': $('#legalPostalAddressCity').val(),
			'legalPostalAddressStreet': $('#legalPostalAddressStreet').val(),
			'legalPostalAddressHouse': $('#legalPostalAddressHouse').val(),
			'legalPostalAddressCode': $('#legalPostalAddressCode').val(),
            'legalKPP': $('#legalKPP').val(),
            'legalOGRN': $('#legalOGRN').val(),
		};
		$saveRequisitesButton.html('Идет сохранение...');
		$(".saveRequisitesFormResponse").html("");
        $.ajax({
        	type: "POST",
        	url: 'updateContactRequisites',
        	dataType: "JSON",
        	data: dataParams,
			success: function(response) {
				if(response.status == "OK") {
					$saveRequisitesButton.addClass('bg-yellow');
					$saveRequisitesButton.addClass('c-black');
					$saveRequisitesButton.removeClass('bg-green');
					$saveRequisitesButton.removeClass('c-white');
					$saveRequisitesButton.html('Сохранено');
					window.setTimeout(function() {
						$saveRequisitesButton.addClass('bg-green');
						$saveRequisitesButton.addClass('c-white');
						$saveRequisitesButton.removeClass('bg-yellow');
						$saveRequisitesButton.removeClass('c-black');
						$saveRequisitesButton.html('Сохранить');
					}, 1000);
				} else {
					this.error(null,response.text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$(".saveRequisitesFormResponse").html(textStatus);
				$saveRequisitesButton.addClass('bg-red');
				$saveRequisitesButton.html('Ошибка');
				window.setTimeout(function() {
					$saveRequisitesButton.removeClass('bg-red');
					$saveRequisitesButton.html('Сохранить');
				}, 3000);
			},
        });
		return false;
	});
});
</script>