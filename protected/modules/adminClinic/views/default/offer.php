<script>
 $(document).ready(function() {
  $("#offerAccept").change(function() {
   if($(this).is(":checked")) {
    $("#offerAcceptBtn").show();
   } else {
    $("#offerAcceptBtn").hide();
   }
  });
 }); 
</script>
<h1 style="font-size:22px;" class="align-center">Договор-оферта на оказание услуг на сайте «Единый медицинский портал»</h1>
<textarea style="padding:4px 8px;overflow-y:scroll;margin-bottom:10px;height:600px;width: 100%;" readonly="readonly">
	<?php
		error_reporting(FALSE);
    $newContractType = [10,11,12,13,14];
    if(in_array($salesContract->salesContractType->id, $newContractType)) {
		  readfile( Yii::getPathOfAlias('uploads.regions').DIRECTORY_SEPARATOR.Yii::app()->user->model->address->city->subdomain.DIRECTORY_SEPARATOR."offerta_new.txt" );
    }
    else {
      readfile( Yii::getPathOfAlias('uploads.regions').DIRECTORY_SEPARATOR.Yii::app()->user->model->address->city->subdomain.DIRECTORY_SEPARATOR."offerta.txt" );
    }
	?>
</textarea>
<form action="" name="offer">
<?= CHtml::checkBox('offerAccept')?> <span style="font-size:16px">Я прочитал(а) и принимаю Условия Договора-оферты на сайте "Единый медицинский портал"</span>
<br/>
<button style="display:none;margin-top:20px;" id="offerAcceptBtn"  type="submit" class="btn-green">Продолжить</button>
</form>


