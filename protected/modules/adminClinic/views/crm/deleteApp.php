
<?php if (isset($response["status"])) : ?>
	<div class="flash-success">
		<?=$response["status"]?>
		<br/><?=$response["text"]?>
		<br/><br/><button id="callBackButton" class="btn btn-red" style="display: none; width: 100px;" onclick="callBackUrl(); return false;">НАЗАД</button>
	</div>
<?php else:?>

<form method="post" type="multipart/form-data" style="margin: 15px 10px;text-align: center;">
	<h1>Действительно ли вы хотите удалить заявку?</h1>
	<table class="htable std_table" style="margin: auto;">
		<tr>
			<td style="padding-top: 20px;width: 200px;">
				<button id="okButton" class="btn btn-green" style="width: 100px;" name="id" value="<?= $editableApp->id ?>">ДА</button>
			</td>
			<td style="padding-top: 20px;width: 200px;">
				<button id="callBackButton" class="btn btn-red" style="display: none; width: 100px;" onclick="callBackUrl(); return false;">НЕТ</button>
			</td>
		</tr>
	</table>
</form>
<?php endif; ?>

<script>
	var getParameterByName = (function(a) {
	    if (a == "") return {};
	    var b = {};
	    for (var i = 0; i < a.length; ++i)
	    {
	        var p=a[i].split('=', 2);
	        if (p.length == 1)
	            b[p[0]] = "";
	        else
	            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	    }
	    return b;
	})(window.location.search.substr(1).split('&'));

	if(getParameterByName['callback']) {
		$("#callBackButton")[0].style.display = "";
	}
	
	function callBackUrl() {
		if(getParameterByName['callback']) {
			location.href = getParameterByName['callback'];
		}
	}
</script>