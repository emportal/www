<?php if($_GET["id"]) { ?>
	<h1>Редактирование записи на прием</h1>
<?php } else { ?>
	<h1>Добавление новой записи на прием</h1>
<?php } ?>

<?php if ($sysMessage) : ?>
	<div class="flash-error">
		<?=$sysMessage?>
	</div>
<?php endif; ?>
<?php if ($successMessage) : ?>
	<div class="flash-success">
		<?=$successMessage?>
	</div>
<?php endif; ?>

<form method="post" type="multipart/form-data">
	<table class="htable std_table">
		<tr>
			<td>
				Врач
			</td>
			<td>
				<?php
				$this->widget('ESelect2', [
					'name' => 'params[doctorId]',
					'value' => $newApp->doctorId,
					'data' => CHtml::listData($doctors, 'link', 'name'),
					'options' => [
						'placeholder' => 'Выберите врача',
						'allowClear' => true,
						'width' => '100%'
					]
				]);
				?>
			</td>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>
				Время приема
			</td>
			<td>
				<input type="text" name="params[plannedTime]" id="plannedTime" value="<?=$newApp->plannedTime?>">
			</td>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>
				ФИО пациента
			</td>
			<td>
				<input type="text" name="params[name]" value="<?=$newApp->name?>">
			</td>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>
				Телефон пациента
			</td>
			<td>
				<input type="text" name="params[phone]" value="<?=$newApp->phone?>">
			</td>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>
				Email пациента
			</td>
			<td>
				<input type="text" name="params[email]" value="<?=$newApp->email?>">
			</td>
			<td>
				не обязательно
			</td>
		</tr>
		<tr>
			<td style="padding-top: 20px;">
				<input type="submit" class="btn btn-small btn-green" value="Сохранить">
			</td>
			<td style="padding-top: 20px;">
				<input type="submit" id="callBackButton" class="btn btn-small btn-green btn-red" value="Отменить и вернуться назад" onclick="callBackUrl(); return false;">
			</td>
			<td>
				
			</td>
		</tr>
	</table>
	

</form>

<script>
	function TimeFormat(obj) {
		var plannedTime = new Date($(obj).val());
		if(plannedTime == "Invalid Date") return false;
		var year = plannedTime.getFullYear();
		var month = plannedTime.getMonth() + 1;
		var date = plannedTime.getDate();
		var hours = plannedTime.getHours();
		var min = plannedTime.getMinutes();
		var xor = min%30;
		if(xor != 0) {
			if(xor/30 > 0.5) {
				min += 30-xor;
			} else {
				min -= xor;
			}
		}
		if(min <= 0) min = "00";
		else if(min >= 60) min = 30;
		if(hours < 9) hours = 9;
		else if(hours >= 22) { hours = 22; min = "00" }
		if (month.toString().length == 1) month = '0' + month;
		if (date.toString().length == 1) date = '0' + date;
		if (hours.toString().length == 1) hours = '0' + hours;
		var result = year + "-" + month + "-" + date + " " + hours + ":" + min;
		$('#plannedTime').val(result);
	}

	$(document).ready(function() {
		$('#plannedTime').datetimepicker({
			lang: 'ru',				
			format: 'Y-m-d H:i',
			step: 30,
			minTime:'8:00',
			maxTime:'23:01',
		});
		$('#plannedTime').on('blur',function () {
			TimeFormat(this)
		});
	});

	var getParameterByName = (function(a) {
	    if (a == "") return {};
	    var b = {};
	    for (var i = 0; i < a.length; ++i)
	    {
	        var p=a[i].split('=', 2);
	        if (p.length == 1)
	            b[p[0]] = "";
	        else
	            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	    }
	    return b;
	})(window.location.search.substr(1).split('&'));

	if(getParameterByName['callback']) {
		$("#callBackButton")[0].style.display = "";
	}
	
	function callBackUrl() {
		if(getParameterByName['callback']) {
			location.href = getParameterByName['callback'];
		} else {
			<?php $isTabletStr = ( substr(Yii::app()->request->url, 0, 19) == '/adminClinicTablet/' ) ? 'Tablet' : ''; ?>
			//alert('isTablet = <?=$isTabletStr?>');
			location.href = '<?=Yii::app()->baseUrl?>/adminClinic<?= $isTabletStr ?>/default/visits';
		}
	}
</script>