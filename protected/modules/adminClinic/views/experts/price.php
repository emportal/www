<? /* @var $addressServices AddressServices[] */ ?>

<? if (Yii::app()->user->hasFlash('success')):?>
<div class="flash-success"><?= Yii::app()->user->getFlash('success')?></div>
<? endif; ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'method'				 => 'post',
	'id'					 => 'company-experts-price',
	'enableAjaxValidation'	 => false,
	'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
		));
/* @var $form CActiveForm */
?>
<h2><?= CHtml::encode($model->name) ?></h2>
<?php echo CHtml::activeLabel($model, 'onHouse')?> <?php echo $form->CheckBox($model, 'onHouse')?>
<table class="service-table">
	<thead>
		<tr>
			<th id="">Название</th>
			<th id="">Цена</th>
			<th id="">Цена в клинике</th>
			<th id="">Бесплатно</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($addressServices as $key => $service): ?>
			<tr>
				<td>
					<?= ($service->service->link == "first") ? "<b>".$service->service->name."</b>" : $service->service->name; ?>
				</td>
				<td>
					<nobr><?= CHtml::textField("services[$key][price]", isset($doctorServices[$key]) ? $doctorServices[$key]->price : 0,
							$doctorServices[$key]->free ? [
								'disabled' => 'true'
							]: []) ?>
					<div style="<?= $doctorServices[$key]->free ? 'display:none': '' ?>" class="btn-red del_price" onclick="$(this).parent().find('input').val(0);">Удалить</div></nobr>
				</td>
				<td>
					<a style="display:<?= $doctorServices[$key]->free ? 'display:none': '' ?>" href="#" onclick="$(this).parent().parent().find('input').val($(this).html()); return false;" title="Кликните для того, чтобы скопировать"><?= $service->price ?></a>
				</td>
				<td>
					<?= CHtml::checkBox("services[$key][free]",$doctorServices[$key]->free,[
						
						'onclick' => '
							if($(this).is(":checked")) {							;
								$(this).closest("tr").find("a,div").hide();
								$(this).closest("tr").find("input[type=text]").attr("disabled",true);
							} else {
								$(this).closest("tr").find("a,div").show();
								$(this).closest("tr").find("input[type=text]").attr("disabled",false);
							}
						'
					]); ?>
				</td>
			</tr>
		<? endforeach; ?>
		<tr class="search-result-box">
			<td class="search-result-info" colspan="4">
				<a class="btn-red" title="Отменить" href="<?= $this->createUrl('experts/index') ?>">Отменить</a>
				<button class="btn-green" type="submit" title="Сохранить">Сохранить</button>
			</td>
		</tr>
	</tbody>
</table>
<?php $this->endWidget(); ?>