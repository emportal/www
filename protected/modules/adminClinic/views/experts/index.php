<!-- <div class="flash-error">Внимание! Внесенные изменения появятся на сайте после подтверждения администратором</div> -->
<? if (Yii::app()->user->hasFlash('experts_add')):?>
<div class="flash-success"><?= Yii::app()->user->getFlash('experts_add')?></div>
<? endif; ?>
<?php
echo CHtml::link('Добавить специалиста', array('experts/add'), array('class' => 'btn'));
?>

<?=CHtml::link('Импорт врачей из другого адреса клиники', array('experts/import'), array('class' => 'btn import_experts'));?>

<br/><br/>

<?php if(true): ?>
<h4>Специалисты из других адресов (редактировать можно в главном адресе)</h4>
<?php
	$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'order-grid2',
			'dataProvider'=>$dataProvider,
			#'filter'=>$oAgrDoctors,
			'enableSorting'=>false,
			'itemsCssClass'	 => 'service-table',
			'summaryText'	 => '',		
			'columns'=>array(
					array(
							'name'=>'name',
							//s'value'=>'$data->name',
					),
					array(
							'name'	 => 'birthday',
							'value'	 => '($data->birthday !== "0000-00-00 00:00:00") ? Yii::app()->dateFormatter->formatDateTime(strtotime($data->birthday), "medium", null) : "Не указана"',
							'type'	 => 'html',
					),
					array(
							'name'	 => 'sexId',
							'value'	 => '$data->sex->name',
					),
					array(
							'name'	 => 'experience',
							'value'	 => '$data->experienceNum',
					),
					array(
							'name'	 => 'specialties.name',
							'value'	 => 'reset($data->specialties)->name',
							#'filter'=>CHtml::activeTextField($oAgrDoctors->doctorSpecialty, 'name'), //CHtml::textField('specialties[name]', $data->specialites->name),
							//'type'	 => 'html',
					),
					/*array(
							'class'		 => 'CButtonColumn',
							'template'	 => '{delete}{update}{price}',
							'buttons'	 => array(
								'update' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
								),
								'delete' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link,"agr"=>1))',
								),
								'price' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
								),
							),
							'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
					),*/
	   ),
	));
?>
<?php endif; ?>


<?php if($oAgrDoctors->search($model)->getData()): ?>
<h4>Неподтвержденные</h4>
<?
	$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'order-grid2',
			'dataProvider'=>$oAgrDoctors->search($model),
			'filter'=>$oAgrDoctors,
			'enableSorting'=>true,
			'itemsCssClass'	 => 'service-table',
			'summaryText'	 => '',		
			'columns'=>array(
					array(
							'name'=>'name',
							//s'value'=>'$data->name',
					),
					array(
							'name'	 => 'birthday',
							'value'	 => '($data->birthday !== "0000-00-00 00:00:00") ? Yii::app()->dateFormatter->formatDateTime(strtotime($data->birthday), "medium", null) : "Не указана"',
							'type'	 => 'html',
					),
					array(
							'name'	 => 'sexId',
							'value'	 => '$data->sex->name',
					),
					array(
							'name'	 => 'experience',
							'value'	 => '$data->experienceNum',
					),
					array(
							'name'	 => 'specialties.name',
							'value'	 => 'reset($data->specialties)->name',
							'filter'=>CHtml::activeTextField($oAgrDoctors->doctorSpecialty, 'name'), //CHtml::textField('specialties[name]', $data->specialites->name),
							//'type'	 => 'html',
					),
					array(
							'class'		 => 'CButtonColumn',
							'template'	 => '{delete}{update}{price}{workingHour}',
							'buttons'	 => array(
								'update' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
								),
								'delete' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link,"agr"=>1))',
								),
								'price' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
								),
								'workingHour' => array(
									'url' => 'CHtml::normalizeUrl(array("experts/workingHour","link"=>$data->link))',
								),
							),
							'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
					),


	   ),
	));
?>
<?php endif; ?>

<h4>Подтвержденные</h4>
<?
$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'order-grid',
		'dataProvider'=>$oDoctors->search($model),
		'filter'=>$oDoctors,
		'enableSorting'=>true,
		'itemsCssClass'	 => 'service-table',
		'summaryText'	 => '',		
		'columns'=>array(
				array(
						'name'=>'name',
						//s'value'=>'$data->name',
				),
				array(
						'name'	 => 'birthday',
						'value'	 => '($data->birthday !== "0000-00-00 00:00:00") ? Yii::app()->dateFormatter->formatDateTime(strtotime($data->birthday), "medium", null) : "Не указана"',
						'type'	 => 'html',
				),
				array(
						'name'	 => 'sexId',
						'value'	 => '$data->sex->name',
				),
				array(
						'name'	 => 'experience',
						'value'	 => '$data->experienceNum',
				),
				array(
						'name'	 => 'specialties.name',
						'value'	 => 'reset($data->specialties)->name',
						'filter'=>CHtml::activeTextField($oDoctors->doctorSpecialty, 'name'), //CHtml::textField('specialties[name]', $data->specialites->name),
						//'type'	 => 'html',
				),
				array(
						'class'		 => 'CButtonColumn',
						'template'	 => '{update2}{price2}{workingHour2}{delete2}',
						'buttons'	 => array(
							'update2' => array(
								'label' => 'редактировать врача',
								'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
								'options' => ['class' => 'btn-experts btn-green']
							),
							'price2' => array(
								'label' => 'редактировать стоимость',
								'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
								'options' => ['class' => 'btn-experts btn-blue']
							),
							'workingHour2' => array(
								'label' => 'часы работы',
								'url' => 'CHtml::normalizeUrl(array("experts/workingHour","link"=>$data->link))',
								'options' => ['class' => 'btn-experts btn-yellow', 'style' => 'color:#ffffff;']
							),
							'delete2' => array(
								'label' => 'удалить',
								'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link))',
								'options' => ['class' => 'btn-experts btn-red', 'style' => '/*text-transform: lowercase;*/']
							),
						),
						'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
				),
   ),
));
?>