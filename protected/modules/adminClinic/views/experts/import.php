<script>	
	$(document).ready(function() {
		$(".all_clients .expert").live('click',function() {
			$(this).hide();
			$(this).removeClass("searchReady");
			var cid = $(this).attr('data-cid');
			$.ajax({
	                url: '/adminClinic/ajax/addExpert',
	                data: ({
	                    'doctor': cid
	                }),
	                dataType: 'JSON',
	                type: "GET",
	                success: function(response) {                    
	                   console.log(response);
	                },
	        });
			$(".checked_clients").append('<div data-cid="'+cid+'" class="pexpert">'+$(this).html()+'</div>');
		});
		
		$(".checked_clients .pexpert").live('click',function() {	
			
			var cid = $(this).attr('data-cid');
			$.ajax({
	                url: '/adminClinic/ajax/removeExpert',
	                data: ({
	                    'doctor': cid
	                }),
	                dataType: 'JSON',
	                type: "GET",
	                success: function(response) {                    
	                   console.log(response);
	                },
	        });
			$(".expert[data-cid="+cid+"]").show();
			$(".expert[data-cid="+cid+"]").addClass("searchReady");
			$(this).remove();
		});
		
		document.getElementById("searchExperts").oninput = function() {			
			var txt  = $("#searchExperts").val().toLowerCase();
			
			if(txt) {		
				setTimeout(function() {					
					if(txt == $("#searchExperts").val().toLowerCase()) {						
						$(".searchReady").hide();
						$(".searchReady").each(function() {					
							var rowText = $(this).find("span").html().toLowerCase();
							var ind = rowText.indexOf(txt);
							if(ind != -1) {
								$(this).show();
							} 
						});
					}
				},250);				
			} else {
				$(".searchReady").show();
			}
		}
	});
	
</script>
<h3>Импорт врачей из другого адреса клиники:</h3>

<div class="flash-notice" style="width:325px;">
	Список выбранных специалистов сохраняется автоматически
</div>
<input placeholder="Введите имя специалиста" type="text" id="searchExperts" class="custom-text searchExperts">
<p class="picked_doctors_text">Выбранные специалисты</p>
<div class="both"></div>
<div class="clients">
	<div class="all_clients">
		<?php if($doctors): ?>
			<?php foreach($doctors as $c): ?>
			<div data-cid="<?=$c->link?>" <?= in_array($c->id, $ids) ? 'class="expert" style="display:none"' : 'class="expert searchReady" '?>>
				<?php if ($c->photo): ?>
					<?= CHtml::image($c->photoUrl, 'Врач',array('class'=>'expertsLogo'))?>
				<? else: ?>
					<?php if ($c->sex->order == 0): ?>
						<?= CHtml::image($this->assetsImageUrl . '/staff-m.jpg', 'Врач',array('class'=>'expertsLogo'))?>
					<? else: ?>
						<?= CHtml::image($this->assetsImageUrl . '/staff-f.jpg', 'Врач',array('class'=>'expertsLogo'))?>
					<? endif; ?>
				<? endif; ?>
				
				<span><?=$c->name?></span>
				</div>
			<?php endforeach;?>
		<?php endif; ?>
	</div>
	<div class="checked_clients">
		<?php if($importDoctors): ?>
			<?php foreach($importDoctors as $c): ?>
				<div data-cid="<?=$c->link?>" class="pexpert">
					<?php if ($c->photo): ?>
						<?= CHtml::image($c->photoUrl, 'Врач',array('class'=>'expertsLogo'))?>
					<? else: ?>
						<?php if ($c->sex->order == 0): ?>
							<?= CHtml::image($this->assetsImageUrl . '/staff-m.jpg', 'Врач',array('class'=>'expertsLogo'))?>
						<? else: ?>
							<?= CHtml::image($this->assetsImageUrl . '/staff-f.jpg', 'Врач',array('class'=>'expertsLogo'))?>
						<? endif; ?>
					<? endif; ?>
					<?=$c->name?>
				</div>
			<?php endforeach;?>
		<?php endif; ?>
	</div>	
	<div class="both"></div>
</div>
<?php
/*
echo CHtml::link('Добавить специалиста', array('experts/add'), array('class' => 'btn'));
echo CHtml::tag('p', array(), Yii::app()->user->getFlash('experts_add'));
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'	 => $dataProvider,
		
	'summaryText'	 => '',
	'itemsCssClass'	 => 'service-table',
	'columns'		 => array(
		array(
			'name'	 => 'ФИО',
			'value'	 => '$data->name',
		),
		array(
			'name'	 => 'Дата рождения',
			'value'	 => 'Yii::app()->dateFormatter->formatDateTime($data->birthday, "medium", null)',
			'type'	 => 'html',
		),
		array(
			'name'	 => 'Пол',
			'value'	 => '$data->sex->name',
		),
		array(
			'name'	 => 'Стаж',
			'value'	 => '$data->experience',
		),
		array(
			'name'	 => 'Специальность',
			'value'	 => 'reset($data->specialties)->name',
			'type'	 => 'html',
		),
		array(
			'class'		 => 'CButtonColumn',
			'template'	 => '{delete}{update}{price}',
			'buttons'	 => array(
				'update' => array(
					'url' => 'CHtml::normalizeUrl(array("experts/update","link"=>$data->link))',
				),
				'delete' => array(
					'url' => 'CHtml::normalizeUrl(array("experts/delete","link"=>$data->link))',
				),
				'price' => array(
					'url' => 'CHtml::normalizeUrl(array("experts/price","link"=>$data->link))',
				),
			),
			'deleteConfirmation' => 'Вы действительно хотите удалить данного врача?'
		),
	),
));*/
?>