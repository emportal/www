<?php

class BalanceController extends AdminClinicController
{
	public function actionIndex()
	{
		$clinic = $this->loadModel();
		$model = $clinic->getBalance();
		$this->render('index', array('model' => $model, 'clinic' => $clinic));
	}

	public function actionAdd()
	{
		$clinic = $this->loadModel();
		$this->render('add', array('model' => $model, 'clinic' => $clinic));
	}
	
	public function actionNewBill() {
		$clinic = $this->loadModel();
		if(Yii::app()->request->getRequestType() == "POST") {
			$bill = new Bill;
			$bill->addressId = $clinic->id;
			$bill->sum = $_REQUEST['sum'];
			$bill->type = $_REQUEST['type'];
			$bill->isPrepaid = 1;
			$bill->reportMonth = "0000-00";
			$response = ['success' => false];
			if($bill->save()) {
				$response['success'] = true;
				$response['sum'] = $bill->sum;
				$response['number'] = $bill->id;
			} else {
				$response['success'] = false;
				$response['errors'][] = $bill->getErrors();// "Неизвестная ошибка!";
			}
			echo json_encode($response);
		} else {
			throw new CHttpException(404);
		}
	}

	public function actionBill($id,$download=false) {
		$clinic = $this->loadModel();
		$bill = Bill::model()->findByAttributes(['id'=>$id, 'addressId'=>$clinic->id]);
		if(is_object($bill)) {
			$file = FileMaker::createInvoice($bill, $download);
			if(!empty($file) && !headers_sent()) {
				header('Content-Description: File Transfer');
				header('Content-Type: '.$file[2]);
				header('Content-Disposition: inline; filename="'.$file[0].'"');
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: ' . strlen($file[1]));
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Expires: 0');
				header('Pragma: public');
				
				echo $file[1];
			}
		} else {
			throw new CHttpException(404);
		}
	}
}