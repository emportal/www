<?php

class ExpertsController extends AdminClinicController
{

	public function actionIndex()
	{		
	
		header("Access-Control-Allow-Origin: *");
		$clinic = $this->loadModel();
		
		$criteria = new CDbCriteria();

		$criteria->with = array(
			'importPlaceOfWork',
			'importPlaceOfWork.address',
		);

		$criteria->compare('`address`.`id`', $clinic->id);

		$dataProvider = new CActiveDataProvider('Doctor', array(
			'criteria'	 => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),			
		));
		
		/*DOCTOR*/
		$oDoctors = new Doctor('search');
		$oDoctors->attributes = Yii::app()->request->getParam(get_class($oDoctors));
		
		$doctorSpecialties = new DoctorSpecialty('search');
		$doctorSpecialties->unsetAttributes();
		
		$oDoctors->doctorSpecialty = $doctorSpecialties;
		$arr=Yii::app()->request->getParam('DoctorSpecialty');
		if ($arr['name']) {
			$oDoctors->doctorSpecialty->name = $_GET['DoctorSpecialty']['name'];
		}
		/*DOCTOR*/
		
		
		/*AGR_DOCTOR*/
		$oAgrDoctors = new AgrDoctor('search');
		$oAgrDoctors->attributes = Yii::app()->request->getParam(get_class($oAgrDoctors));	
		
		$doctorSpecialties = new DoctorSpecialty('search');
		$doctorSpecialties->unsetAttributes();		
		
		$oAgrDoctors->doctorSpecialty = $doctorSpecialties;
		$arr=Yii::app()->request->getParam('DoctorSpecialty');
		if ($arr['name']) {
			$oAgrDoctors->doctorSpecialty->name = $_GET['DoctorSpecialty']['name'];
		}
		/*AGR_DOCTOR*/
		
		
		$this->render('index', array('model' => $clinic, 'doctors' => $clinic->doctors, 'dataProvider' => $dataProvider, 'oDoctors' => $oDoctors, 'oAgrDoctors'=>$oAgrDoctors));
	}

	public function actionAdd()
	{
		if($this->loadModel()->checkLimitNumberOfDoctors()) {
			Yii::app()->user->setFlash('error', "Ошибка! Действующий тариф не позволяет вам завести в этом адресе более ".$this->loadModel()->getDoctorsLimit()." специалистов.");
			$this->render('/default/error');
		}
		
		$model = new Doctor('insert');

		$attr = Yii::app()->request->getParam(get_class($model));
		$oErorrs = array();
	
		if ($attr) {
			//var_dump($attr);
			$date = strtotime($attr['birthday']);
			
			
			if(!$date) {
				$attr['birthday'] = "0000-00-00 00:00:00";
			} else {
				$attr['birthday'] = date("Y-m-d",  $date);
			}
			$model->attributes = $attr;
			
			//$model->id = $_SESSION['doctorIdTemp'] = !empty($_SESSION['doctorIdTemp']) ? $_SESSION['doctorIdTemp'] : md5(Yii::app()->user->id . time());

			$model->name = $model->surName . ' ' . $model->firstName . ' ' . $model->fatherName;

			$model->scientificTitle	= $attr['scientificTitle'] ? $attr['scientificTitle'] : null;			
			
			if($attr['scientificDegrees'] && reset($attr['scientificDegrees'])) {
				$model->scientificDegrees	 = $attr['scientificDegrees'];				
			} else {				
				DoctorScientificDegree::model()->deleteAllByAttributes(array('doctorId' => $model->id));
			}
			if(($model->scientificDegrees[0] != 'a0e63271-e562-11e1-8649-60fb4275079e') && ($model->scientificTitle == '81e0f6b8-947f-11e2-856f-e840f2aca94f')) {				
				$oErorrs['scientificTitle'] = "Профессором может быть только доктор медицинских наук.";
			}
		
			

			$model->photo = CUploadedFile::getInstance($model, 'photo');
			
			/*if(empty($model->scientificDegrees) || empty($model->scientificDegrees[0])) {
				$oErorrs['scientificDegrees'] = "Необходимо заполнить поле Ученая степень.";
			}*/
			/*if(empty($attr['doctorEducations'])) {
				$oErorrs['doctorEducations'] = "Необходимо заполнить поле Учебное заведение.";
			}*/
			if(empty($attr['specialtyOfDoctors'])) {
				$oErorrs['specialtyOfDoctors'] = "Необходимо заполнить поле Специальность.";
			}

			//print_r($oErorrs);
			if (!Yii::app()->request->isAjaxRequest & $model->validate() && count($oErorrs) === 0) {		
				
				$currentLink = Yii::app()->db->createCommand()->
						select("max(`link`)")->
						from(Doctor::model()->tableName())->
						queryScalar();
				$secondLink = Yii::app()->db->createCommand()->
						select("max(`link`)")->
						from(AgrDoctor::model()->tableName())->
						queryScalar();
				if((int)$secondLink>(int)$currentLink) {
					$currentLink = $secondLink;
				}
				$currentLink = intval($currentLink);
				$currentLink++;
				$model->link = str_pad($currentLink, 9, '0', STR_PAD_LEFT);

				$model->scientificTitleId = $attr['scientificTitle'] ? $attr['scientificTitle'] : 'a6f95ab7-441e-4b2b-91a2-ab3faf548d8e';
					
				if($model->save()) {
					if($attr['scientificDegrees'][0]) {
						$model->scientificDegrees[0] = $attr['scientificDegrees'][0];
						$scientificDegree = DoctorScientificDegree::model()->findByAttributes(['doctorId'=>$model->id, 'scientificDegreeId'=>$attr['scientificDegrees'][0]]);
						if(!$scientificDegree) {
							$scientificDegree = new DoctorScientificDegree;
							$scientificDegree->doctorId = $model->id;
							$scientificDegree->scientificDegreeId = $attr['scientificDegrees'][0];
							$scientificDegree->save();
						}
					} else {
						$model->scientificDegrees = [];
						DoctorScientificDegree::model()->deleteAllByAttributes(array('doctorId' => $model->id));
					}
					
					$model->setSpecialtyOfDoctors($attr['specialtyOfDoctors']);
					$model->setDoctorEducations($attr['doctorEducations']);
					$model->setCurrentPlaceOfWork($this->loadModel());

					$logModel = new LogAgr();
					$logModel->companyId = $this->loadModel()->ownerId;
					$logModel->addressId = $this->loadModel()->id;
					$logModel->action = "insert";
					$logModel->table = $model->tableName();
					$logModel->changes = CJSON::encode(array_merge($model->attributes,$attr));
					$logModel->save();

					if ($model->photo instanceof CUploadedFile) {
						$model->photo->saveAs($model->photoPath);
					}
					//unset($_SESSION['doctorIdTemp']);
					$this->redirect(array('price', 'link'=>$model->link));
				}
				
			}
			else {					
				if (!empty($oErorrs)) {
					
					foreach ($oErorrs as $label=>$error) {						
						$model->addError($label, $error);
					}
				} else {
					/*
					if(!empty($attr['specialtyOfDoctors'])) {
						$model->setSpecialtyOfDoctors($attr['specialtyOfDoctors']);
					}
					if(!empty($attr['doctorEducations'])) {
						$model->setDoctorEducations($attr['doctorEducations']);
					}*/
				}
				
				$this->performAjaxValidation($model);					
			}
			
			$this->performAjaxValidation($model);
			
		}

		$this->render('add', array('model' => $model));
	}

	public function actionUpdate($link)
	{

		$model = Doctor::model()->findByAttributes(array('link' => $link));
		/* @var $model Doctor */
		
		$attr = Yii::app()->request->getParam(get_class(AgrDoctor::model()));
		
		
		$agr_model = null; // AgrDoctor::model()->findByAttributes(array('link' => $link));
		$deleteAgr = true;
				
		if(!$agr_model && !$model) {
			$this->redirect(array('experts/index'));
		}
		
		if(!$agr_model) {
			$agr_model = new AgrDoctor();
			$agr_model->photo = $model->photo;
			$agr_model->attributes = $model->attributes;
			$agr_model->specialtyOfDoctors=$model->specialtyOfDoctors;
			$agr_model->doctorEducations=$model->doctorEducations;
			$agr_model->scientificDegrees=$model->scientificDegrees;
			$agr_model->scientificTitle=$model->scientificTitle;			
		}				
		$agr_model->scenario = 'update';
		
		if(Yii::app()->user->model->companyId <> $model->currentPlaceOfWork->companyId) {
			$this->redirect("index");
		}
		
		if ($attr) {
			$date = strtotime($attr['birthday']);
			
			if(!$date) {
				$date = strtotime("0000-00-00 00:00:00");
				$attr['birthday'] = "0000-00-00 00:00:00";
			} else {
				$attr['birthday'] = date("Y-m-d",  $date);
			}
			//$attr['birthday'] = $date['year'] .'-'. $date['month'] . '-' . $date['day'];
			
			$agr_model->attributes = $attr;
			
			$agr_model->name = $agr_model->surName . ' ' . $agr_model->firstName . ' ' . $agr_model->fatherName;
			if($date == strtotime($model->birthday) && $attr['surName']==$model->surName && $attr['firstName']==$model->firstName && $attr['fatherName']==$model->fatherName && $attr['experience']==$model->experience &&  $attr['sexId'] == $model->sexId &&  $attr['description'] == $model->description) {
				$deleteAgr = true;
			}
			
			$agr_model->scientificTitleId	= $attr['scientificTitle'] ? $attr['scientificTitle'] : 'a6f95ab7-441e-4b2b-91a2-ab3faf548d8e';
			
			if($attr['scientificDegrees'] && reset($attr['scientificDegrees'])) {
				$agr_model->scientificDegrees = array(0=>$attr['scientificDegrees'][0]);		
				#$agr_model->scientificDegrees[0]->id = $attr['scientificDegrees'][0];
			} else {				
				AgrDoctorScientificDegree::model()->deleteAllByAttributes(array('doctorId' => $agr_model->id));
				#var_dump($model->scientificDegrees[0]);die();				
			}
			
			if(( ($agr_model->isNewRecord ? $agr_model->scientificDegrees[0] : $agr_model->scientificDegrees[0]->id) != 'a0e63271-e562-11e1-8649-60fb4275079e') 
					&& ($agr_model->scientificTitleId == '81e0f6b8-947f-11e2-856f-e840f2aca94f')) {				
				$oErorrs['scientificTitle'] = "Профессором может быть только доктор медицинских наук.";
			}
			
			/*if(empty($model->scientificDegrees) || empty($model->scientificDegrees[0])) {
				$oErorrs['scientificDegrees'] = "Необходимо заполнить поле Ученая степень.";
			}*/
			/*if(empty($attr['doctorEducations'])) {
				$oErorrs['doctorEducations'] = "Необходимо заполнить поле Учебное заведение.";
			}*/
			if(empty($attr['specialtyOfDoctors'])) {
				$oErorrs['specialtyOfDoctors'] = "Необходимо заполнить поле Специальность.";
			}

			if (!Yii::app()->request->isAjaxRequest & $agr_model->validate() && count($oErorrs) === 0) {
				if($deleteAgr ) {
					if(!$agr_model->isNewRecord) {
						$agr_model->delete();	
					}
					$model->attributes = array_merge($model->attributes, $agr_model->attributes);
					$model->scientificTitleId = $attr['scientificTitle'] ? $attr['scientificTitle'] : 'a6f95ab7-441e-4b2b-91a2-ab3faf548d8e';	
					
					if($attr['scientificDegrees'][0]) {
						$model->scientificDegrees[0] = $attr['scientificDegrees'][0];
						$scientificDegree = DoctorScientificDegree::model()->findByAttributes(['doctorId'=>$model->id]);

						if(!$scientificDegree) {
							$scientificDegree = new DoctorScientificDegree;
							$scientificDegree->doctorId = $model->id;
							$scientificDegree->scientificDegreeId = $attr['scientificDegrees'][0];
							$scientificDegrees->save();
						}else{
							$scientificDegree->scientificDegreeId=$attr['scientificDegrees'][0];
							$scientificDegree->update();
						}
					} else {
						$model->scientificDegrees = [];
						DoctorScientificDegree::model()->deleteAllByAttributes(array('doctorId' => $model->id));
					}

					// var_dump($model->scientificDegrees);

					// var_dump($agr_model['scientificDegrees']);
					// $model->scientificDegrees;
					// echo "</br>";
					//var_dump($attr['scientificDegrees']);
					
					// var_dump($attr);
						
					
					
					if($model->save()){
						$attr['id'] = $model->id;
						$model->setSpecialtyOfDoctors($attr['specialtyOfDoctors']);						
						$model->setDoctorEducations($attr['doctorEducations']);					
						$model->setCurrentPlaceOfWork($this->loadModel());

						
						$logModel = new LogAgr();
						$logModel->companyId = $this->loadModel()->ownerId;
						$logModel->addressId = $this->loadModel()->id;
						$logModel->action = "update";
						$logModel->table = $model->tableName();
						$logModel->changes = CJSON::encode($attr);
						$logModel->save();
					}
					#$model->save();
				} else {									
					if($agr_model->save()) {
						$agr_model->setSpecialtyOfDoctors($attr['specialtyOfDoctors']);							
						$agr_model->setDoctorEducations($attr['doctorEducations']);							
						$agr_model->setCurrentPlaceOfWork($this->loadModel());
					}					
				}
				$this->redirect(array('price', 'link'=>$agr_model->link));
			} else {
				if (!empty($oErorrs)) {
					foreach ($oErorrs as $label=>$error) {
						$agr_model->addError($label, $error);
					}
				} else {
					/*
					if(!empty($attr['specialtyOfDoctors'])) {
						$agr_model->setSpecialtyOfDoctors($attr['specialtyOfDoctors']);
					}
					if(!empty($attr['doctorEducations'])) {
						$agr_model->setDoctorEducations($attr['doctorEducations']);
					}*/
				}
				$this->performAjaxValidation($agr_model);
			}
		}
		else {
			$date = strtotime($agr_model->birthday);
			if(!$date) {
				$agr_model->birthday = "0000-00-00 00:00:00";
			} else {
				$agr_model->birthday = date("d.m.Y",  $date);
			}
		}
		$this->render('add', array('model' => $agr_model));
	}
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='company-experts')
		{
			$result = array();
			foreach($model->getErrors() as $attribute=>$errors) {
				$result[CHtml::activeId($model,$attribute)]=$errors;
			}

			echo function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);
			Yii::app()->end();
		}
	}
	public function actionUploadPhoto($link)
	{
		$model = AgrDoctor::model()->findByAttributes(array('link' => $link));
		$workingModel = Doctor::model()->findByAttributes(array('link' => $link));
		
		if(empty($model) && empty($workingModel)) {
			throw new CHttpException(404,'Врач не найден');
		}
		if(empty($model)) {
			$model = new AgrDoctor();
		}
		if(!empty($workingModel)) {
			$model->attributes = $workingModel->attributes;
		}
		$model->photo = CUploadedFile::getInstance($model, 'photo');
		$path = "{$model->uploadPath}/";
		$image_cache_path = $path."cache/";

		$workingModel->originalPhotoUrl = null;
		if (Yii::app()->request->getParam('delete', false)) {
			unlink($model->photoPath);
			$model->hasPhoto = 0;
			$workingModel->hasPhoto = 0;
			if(is_dir($image_cache_path)) MyTools::deleteDir($image_cache_path);
		}
		elseif ($model->photo instanceof CUploadedFile) {
			if(is_dir($image_cache_path)) MyTools::deleteDir($image_cache_path);
			if(is_file($model->photoPath)) unlink($model->photoPath);
			$type = 'jpg';
			if($model->photo->type === 'image/png'){
				$type = 'png';
			}
			$img = "photo.".$type;
			$uploadPath = $path.$img;
			if($model->photo->saveAs($uploadPath)) {
				Yii::app()->image->load($uploadPath)->resize(160, 160,Image::WIDTH)->save($uploadPath);
				$model->hasPhoto = 1;
				$workingModel->hasPhoto = 1;
				$workingModel->originalPhotoUrl = $workingModel->getDoctorPhotoUrl();
			}
		}
		
		if(!$model->isNewRecord) $model->update();
		if(!$workingModel->isNewRecord) $workingModel->update();
		
		$this->redirect(Yii::app()->createUrl('/adminClinic/experts/update', array('link'=>$model->link)));
	}

	public function actionDelete($link)
	{
		$agr=Yii::app()->request->getParam('agr');
		$agr*=1;
		
		//Если удаляем из таблицы подтверждения,то только саму запись.
		//Если в главной таблице записи не существует,значит чистим связанные таблицы
		if($agr) {
			$agrModel = AgrDoctor::model()->findByAttributes(array('link' => $link));			

			if ($agrModel->currentPlaceOfWork->addressId == $this->loadModel()->id) {
				AgrDoctorScientificDegree::model()->deleteAllByAttributes(array('doctorId' => $agrModel->id));
				AgrDoctorServices::model()->deleteAllByAttributes(array('doctorId' => $agrModel->id));
				AgrDoctorEducation::model()->deleteAllByAttributes(array('doctorId' => $agrModel->id));
				AgrSpecialtyOfDoctor::model()->deleteAllByAttributes(array('doctorId' => $agrModel->id));
				/* @var $model Doctor */
			
				header("Access-Control-Allow-Origin: *");
				PlaceOfWork::model()->deleteAllByAttributes(array('doctorId' => $model->id));
				#$agrModel->currentPlaceOfWork->delete();
				$agrModel->delete();
			}
		} else {
			$model = Doctor::model()->findByAttributes(array('link' => $link));
			
			if ($model->currentPlaceOfWork->addressId == $this->loadModel()->id) {
				/*Хорошо бы это вынесети в базу*/
				DoctorScientificDegree::model()->deleteAllByAttributes(array('doctorId' => $model->id));
				DoctorServices::model()->deleteAllByAttributes(array('doctorId' => $model->id));
				DoctorEducation::model()->deleteAllByAttributes(array('doctorId' => $model->id));
				#SpecialtyOfDoctor::model()->deleteAllByAttributes(array('doctorId' => $model->id));
				/* @var $model Doctor */
				
				header("Access-Control-Allow-Origin: *");
				PlaceOfWork::model()->deleteAllByAttributes(array('doctorId' => $model->id));
				#$model->currentPlaceOfWork->delete();
				if($model->delete()){
					$logModel = new LogAgr();
					$logModel->companyId = $this->loadModel()->ownerId;
					$logModel->addressId = $this->loadModel()->id;
					$logModel->action = "delete";
					$logModel->table = $model->tableName();
					$logModel->changes = CJSON::encode(array('id' => $model->id, 'name' => $model->name));;
					$logModel->save();
				}
			}
		}
		
		$this->redirect(array('index'));
	}

	public function actionPrice($link) {
		$first_name = "Первоначальное обращение";
		
		if(empty($link)) {
			$this->redirect(array('experts/index'));
			Yii::app()->end();
		}
		$model = Doctor::model()->findByLink($link);
		
		if(!$model) {			
			$model = AgrDoctor::model()->findByLink($link);
		}
		
		if(!$model) {
			$this->redirect(array('experts/index'));
		}
		/*if(!$model) 
			$model = Doctor::model()->findByLink($link);*/
		
		$attr = Yii::app()->request->getParam(get_class($model));		
		$model->attributes = $attr;
		#$model->save();
		
		//<Создаем услугу первоначальный осмотр в системе если нет>
		$inspection = Service::model()->find("link='first'");
		$id=$inspection->id;
		if(empty($inspection))	{ 
			$new_inspection= new Service();
			$new_inspection->setIsNewRecord(true);
			$new_inspection->setPrimaryKey(NULL);			
			$new_inspection->name = $first_name;
			$new_inspection->fullName = $first_name;
			$new_inspection->forAdult = 0;
			$new_inspection->forChildren = 0;
			$new_inspection->description = "";			
			if($new_inspection->save(false)) {	
				$id=$new_inspection->id;				
			} 
		}
		//</Создаем Услугу Первоначального осмотра в системе если нет>
		//
		//<Создаем Услугу Первоначального осмотра у текущей компании>
		$as=AddressServices::model()->find("serviceId=:serviceId AND addressId=:addrId",array(
				'serviceId'=>$id,
				'addrId'=>Yii::app()->user->model->clinic->id));
		
		if(empty($as)) {
			$address_service = new AddressServices();
			$address_service->setIsNewRecord(true);
			$address_service->price=1;
			$address_service->addressId=Yii::app()->user->model->clinic->id;
			$address_service->serviceId=$id;
			$address_service->save();
		}
		//</Создаем Услугу Первоначального осмотра у текущей компании>	
		
		
		/* @var $model Doctor */
		
		if (($services = Yii::app()->request->getParam('services', false)) && count($services)) {			
			if($model->tableName()==AgrDoctor::model()->tableName()) {
					$ds = AgrDoctorServices::model()->findAllByAttributes(array('doctorId' => $model->id));	
					foreach($ds as $s) {
						$s->delete();
					}
					$doctorService = new AgrDoctorServices();
			} else {
					$ds = DoctorServices::model()->findAllByAttributes(array('doctorId' => $model->id));
					foreach($ds as $s) {
						$s->delete();
					}
					$doctorService = new DoctorServices();
			}
			
			foreach ($services as $id => $service) {
			
					$doctorService->setIsNewRecord(true);
					$doctorService->setPrimaryKey(NULL);

					$doctorService->attributes = array(
						'id' => ActiveRecord::create_guid(array(
							$model->id,
							$id
						)),
						'doctorId' => $model->id,
						'cassifierServiceId' => $id,
						'price' => $service['price'],
						'free' => $service['free']
					);

					$doctorService->save(false);
				
			}

			Yii::app()->user->setFlash('experts_add', 'Данные сохранены');
			$this->redirect(array('experts/index'));
		}
		
		if($model->tableName()==AgrDoctor::model()->tableName()) {			
			$activites = CompanyActivite::model()->with('doctorSpecialties.agr_doctors')->findAll(array('index'		 => 'id', 'condition'	 => ' `agr_doctors`.`link` = :link ', 'params'	 => array(':link' => $link)));
		} else {
			$activites = CompanyActivite::model()->with('doctorSpecialties.doctors')->findAll(array('index'		 => 'id', 'condition'	 => ' `doctors`.`link` = :link ', 'params'	 => array(':link' => $link)));
		}		
		#echo count($activites);
		$criteria		 = new CDbCriteria();
		$criteria->with	 = array(
			'service'=>array(
				'together'=>true
			)
		);		
		$criteria->addInCondition('service.companyActiviteId', array_keys($activites));
		$criteria->addCondition("service.link ='first'","OR");
		$criteria->index = 'serviceId';
		
		//todo:confirm this condition
		$criteria->compare('addressId',$model->currentPlaceOfWork->addressId);
		
		$addressServices = AddressServices::model()->findAll($criteria);		
		//foreach($addressServices as $ad) {
		//	var_dump($activites);	
		//}
			
				
				
		
		
		$criteria		 = new CDbCriteria();
		$criteria->with	 = array(
			'cassifierService'
		);
		$criteria->index = 'cassifierServiceId';
		$criteria->compare('doctorId', $model->id);
		if($model->tableName()==AgrDoctor::model()->tableName()) {
			$doctorServices = AgrDoctorServices::model()->findAll($criteria);
		} else {
			$doctorServices = DoctorServices::model()->findAll($criteria);
		}
		$this->render('price', array('addressServices'	 => $addressServices, 'doctorServices'	 => $doctorServices, 'model'=>$model));
	}
	
	public function actionImport() {
		if($this->loadModel()->checkLimitNumberOfDoctors()) {
			Yii::app()->user->setFlash('error', "Ошибка! Действующий тариф не позволяет вам завести в этом адресе более ".$this->loadModel()->getDoctorsLimit()." специалистов.");
			$this->render('/default/error');
		}
		
		$model = $this->loadModel();
		
		$criteria = new CDbCriteria();
		$criteria->with = [
			'currentPlaceOfWork' => [
				'together' => true,
				'with' => [
					'address'=>[						
						'together'=>true
					]
				]
			]
		];
		$addressIds = array_keys(Address::model()->findAll([
			'condition'	=>	"t.id <> '".$model->id."' AND t.ownerId = '".$model->company->id."'",
			'index'		=>	'id'
		]));
		
		$criteria->addInCondition('address.id',$addressIds);
		$doctors = Doctor::model()->findAll($criteria);
		$ids = array();
		$importDoctors = $model->importDoctors;
		foreach($importDoctors as $d) {
			$ids[] =$d->id;
		}
		
		$this->render("import",[
			'model'			=>	$model,
			'doctors'		=>	$doctors,
			'importDoctors' =>	$importDoctors,
			'ids'			=>	$ids,
		]);
	}

	public function actionWorkingHour($link) {

		$clinic = $this->loadModel();
		$model = Doctor::model()->findByLink($link);
		$attrs = Yii::app()->request->getParam(get_class($model));

		if ($attrs) {
			if($attrs['workingHours']['allCheck']) {
				foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $key=>$day) {					
					$attrs['workingHours'][$day.'Finish'] = $attrs['workingHours']['allFinish'];
					$attrs['workingHours'][$day.'Start'] = $attrs['workingHours']['allStart'];					
				}	
			}
			$attrs['workingHours']['addressId'] = $model->currentPlaceOfWork->addressId;
			$model->workingHours = $attrs['workingHours'];
			
			if ($model->save() && $model->workingHours->validate()) {				
				Yii::app()->user->setFlash('experts_add', 'Данные сохранены');
				$this->redirect(array('experts/index'));
			}
		}
		unset($attrs['agrphones']['%new%']);

		$this->render('workingHour', array('model' => $model, 'clinic' => $clinic));

	}
}