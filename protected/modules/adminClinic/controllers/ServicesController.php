<?php

class ServicesController extends AdminClinicController {

	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->with = array(
			'serviceSection',
			'companyActivite',
		);
		$dataProvider = new CActiveDataProvider('Service', array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
		));

		$cmd = Yii::app()->db->createCommand();

		$cmd->
				from(Service::model()->tableName() . ' s')->
				leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId')->
				leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->
				leftJoin(AddressServices::model()->tableName() . ' as', 's.id = as.serviceId');

		$cmd->select(array(
					'as.id as id',
					'IF(LOWER(`ca`.`name`) = \'нет данных\',\'\',`ca`.`name`) as CompanyActiviteName',
					'IF(LOWER(`ss`.`name`) = \'нет данных\',\'\',`ss`.`name`)  as ServiceSectionName',
					's.name as ServiceName',
					's.link as link',
					's.linkUrl as linkUrl',
					'as.isActual as isActual',
					'as.oldPrice',
					'IFNULL(as.price,0) as Price',
					'IFNULL(as.free,0) as free',
					's.empComissionAsAppointment as empComissionAsAppointment',
				))->
				where('as.addressId = :id AND s.link <> "zapnp"', array(':id' => Yii::app()->user->model->clinic->id));
		$cmd->order("ServiceSectionName ASC");

		$rows = $cmd->queryAll();

		$cmd = Yii::app()->db->createCommand();

		$cmd->
				from(ServicesRequest::model()->tableName() . ' s')->
				leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId');
		//leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->

		$cmd->select(array(
					//'ca.name as CompanyActiviteName',
					'ss.name as ServiceSectionName',
					's.name as ServiceName',
					's.status as Status',
					's.rejectionText as Reject',
					'IFNULL(s.price,0) as Price',
				))
				->where('s.addressId = :id AND s.status != 2', array(':id' => Yii::app()->user->model->clinic->id))
				->order('s.status ASC');

		$ServicesRequest = $cmd->queryAll();

		$this->render('index', array('dataProvider' => $dataProvider, 'rows' => $rows, 'ServicesRequest' => $ServicesRequest));
	}

	public function actionAjaxUpdate() {
		
		$params = Yii::app()->request->getParam('AddressServices');

		if (count($params)) {
			foreach ($params as $id => $data) {
				$model = AddressServices::model()->findByPk($id);
				$model->price = $data['price'];
				$model->oldPrice = ((int)$data['oldPrice']) ? $data['oldPrice'] : NULL;
				$model->confirmedDate = date('Y-m-d H:i:s', time());
				$model->isActual = 1;
				$model->update();
			}
		}
	}
	
	public function actionAjaxSetFree() {
		$params = Yii::app()->request->getParam('AddressServices');

		if (count($params)) {
			foreach ($params as $id => $free) {
				$model = AddressServices::model()->findByPk($id);
				$model->free = $free;				
				$model->save();
				
			}
		}
	}
	
	public function actionDelete() {
		$params = Yii::app()->request->getParam('AddressServices');
		if (count($params)) {
			foreach ($params as $id => $price) {
				$model = AddressServices::model()->findByPk($id);
				$model->delete();
				
			}
		}
	}
	public function actionGetfile() {
		$cmd = Yii::app()->db->createCommand();

		$cmd->
				from(Service::model()->tableName() . ' s')->
				leftJoin(ServiceSection::model()->tableName() . ' ss', 'ss.id = s.serviceSectionId')->
				leftJoin(CompanyActivite::model()->tableName() . ' ca', 'ca.id = s.companyActiviteId')->
				join(AddressServices::model()->tableName() . ' as', 's.id = as.serviceId AND as.addressId=:addressId', array(':addressId' => Yii::app()->user->model->clinic->id));

		//$cmd->where('as.addressId=:addressId', array(':addressId'=>Yii::app()->user->model->clinic->id));

		$cmd->select(array(
			"IF(ca.name = 'Нет данных','',ca.name) as CompanyActiviteName",
			//"IF(ss.name = 'Нет данных','',ss.name) as ServiceSectionName",
			"s.name as ServiceName",
			'IFNULL(as.price,0) as Price',
		));
		$cmd->where("s.name <> 'Нет данных' AND s.name <> 'Запись на прием'");
		//$cmd->order('CompanyActiviteName ASC, ServiceSectionName ASC');
		$cmd->order('CompanyActiviteName ASC');
		$rows = $cmd->queryAll();

		$outstream = fopen("php://temp", 'r+');
		//fputs($outstream, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		fputcsv($outstream, array('Профиль деятельности', /*'Медицинский Раздел',*/ 'Услуга', 'Стоимость (руб.)'), ';');
		foreach ($rows as $row)
			fputcsv($outstream, $row, ';');
		rewind($outstream);
		Yii::app()->request->sendFile('Каталог услуг.csv', mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
		fclose($outstream);
		die();
	}

	public function actionUpload() {
		$uploadDir = Yii::app()->user->model->clinic->uploadPath . '/services';
		Yii::app()->session['datainfo'] = array(
			'uploadDir' => $uploadDir,
			'clinic_name' => Yii::app()->user->model->clinic->name,
			'usermedical_name' => print_r(Yii::app()->user->model->attributes, true)
		);

		$clinic = $this->loadModel();
		$model = new AddressServices();

		$file = CUploadedFile::getInstanceByName('file');
		if(empty($file)) {
			Yii::app()->user->setFlash('error', 'Ошибка при загрузке услуг в базу');
			$this->redirect('index');
		}

		$arr = glob($uploadDir . "/current_old*");

		for ($i = 0; $i < 3; $i++) {
			$str = explode("/", $arr[$i]);
			$files[] = $str[count($str) - 1];
		}

		if (!is_dir($uploadDir))
			mkdir($uploadDir, 0775, true);

		$filename = $uploadDir . '/current.' . $file->extensionName;
		//Удаляем последний файл
		//сдвигаем все файлы			

		if (file_exists($filename)) {
			$fileTpl = str_replace("." . $file->extensionName, "", $filename);
			$files[0] ? $f1 = $uploadDir . "/" . $files[0] : $f1 = "";
			$files[1] ? $f2 = $uploadDir . "/" . $files[1] : $f2 = "";
			$files[2] ? $f3 = $uploadDir . "/" . $files[2] : $f3 = "";
			if (file_exists($f3)) {
				unlink($f3);
			}
			if (file_exists($f2)) {
				$newF2 = str_replace("old2", "old3", $f2);
				rename($f2, $newF2);
			}
			if (file_exists($f1)) {
				$newF1 = str_replace("old1", "old2", $f1);
				rename($f1, $newF1);
			}
			$newFilename = str_replace("current", "current_old1_" . date("d-m-Y"), $filename);
			rename($filename, $newFilename);
		}

		$file->saveAs($filename);
		$mimeType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $filename);
		if($mimeType !== "text/plain" AND $mimeType !== "text/csv") {
			Yii::app()->user->setFlash('error', "Не верный тип данных: ".$mimeType);
			$this->redirect('index');
		}
		$data = array();
		$fp = fopen($filename, 'r');
		$csvContents = stream_get_contents($fp);
		$encoding = CharsetEncoding::Detect($csvContents);
		$delim = '';
		rewind($fp);
		$formatSuccess = false;
		foreach (array(';', ',') as $delim) {
			$match = 0;
			for ($i = 0; $i < 10; $i++) {
				$csvLine = fgetcsv($fp, 0, $delim);
				$countOfColumn = count($csvLine);
				if(empty($csvLine[2])) { $i--; }
				if (2 < $countOfColumn AND $countOfColumn < 7 AND is_numeric($csvLine[2])) {
					if($match > 3) {
						$formatSuccess = true;
						break 2;
					} else {
						$match++;
					}
				}
			}
			rewind($fp);
		}
		rewind($fp);
		if(!$formatSuccess) {
			Yii::app()->user->setFlash('error', 'Не верный формат данных');
			$this->redirect('index');
		}
		$i = 0;
		while ($row = fgetcsv($fp, 0, $delim)) {
			foreach ($row as $j=>$str) {
				$data[$i][$j] = iconv($encoding, 'UTF-8', $str);
			}
			$i++;
		}
		fclose($fp);
		/* Удалить все услуги клиники
		$rowsToDelete = CompanyActivites::model()->findAll('addressId = :id', array(':id' => Yii::app()->user->model->clinic->id));
		foreach ($rowsToDelete as $row) {
			$row->delete();
		} */
		$sql = 'INSERT INTO ' . AddressServices::model()->tableName() . ' (id, addressId, serviceId, price, companyActivitesId, free, isActual) ';

		$transaction = Yii::app()->db->beginTransaction();
		try {
			foreach ($data as $row) {
				$ss_name = @$row[0];
				$s_name = @$row[1];
				$price = intval(@$row[2]);
				$description = empty($row[3]) ? "-" : $row[3];
				if($ss_name==="Профиль деятельности" OR $s_name==="Услуга" OR $price==="Стоимость (руб.)") continue;
				$serviceModel = Service::model()->findByAttributes(["name" => $s_name]);
				if($serviceModel) {
					$response = $clinic->addService($serviceModel->id, $price);
					if($response['status'] == "EXIST") {
						$response['addressServices']->price = $price;
						$response['addressServices']->confirmedDate = date('Y-m-d H:i:s', time());
						$response['addressServices']->update();
					}
				} else {
					$attributes = ['addressId'=>$clinic->id, 'name'=>$s_name, 'price'=>$price, 'description'=>"-"];
					if(ServicesRequest::model()->countByAttributes($attributes) < 1) {
						$servicesRequest = new ServicesRequest();
						$servicesRequest->attributes = $attributes;
						if($serviceSection = ServiceSection::model()->findByAttributes(["name"=>$ss_name])) {
							$servicesRequest->serviceSectionId = $serviceSection->id;
						} else {
							$servicesRequest->description = $ss_name . " " . $servicesRequest->description;
							$servicesRequest->serviceSectionId = "44f30210-9eb7-4e71-8f1b-e12e452be5c3";
						}
						$servicesRequest->status = 0;
						$servicesRequest->save();
					}
				}
			}
		} catch (PDOException $e) {
			$transaction->rollback();
			Yii::app()->user->setFlash('error', 'Ошибка при загрузке услуг в базу');
			$this->redirect('index');
		}
		$transaction->commit();
		$data = AddressServices::model()->findAll([
			'group' => 'companyActivitesId',
			'condition' => "addressId = '" . Yii::app()->user->model->clinic->id . "'",
			'with' => [
				'companyActivite' => [
					'joinType' => 'INNER JOIN'
				]
			]
		]);

		//add services into CompanyActivites
		foreach ($data as $row) {
			$ca = new CompanyActivites();
			$ca->addressId = $row->address->id;
			$ca->companyActivitesId = $row->companyActivitesId;
			$ca->save();
		}
		
		$ca = CompanyActivite::model()->with('services.addressServices')->findAll('addressServices.addressId = :address', array(':address' => Yii::app()->user->model->clinic->id));

		Yii::app()->user->model->clinic->companyActivitesGroupByName = $ca;
		#var_dump(Yii::app()->user->model->clinic->companyActivitesGroupByName);die();
		Yii::app()->user->model->clinic->save();
		
		Yii::app()->user->setFlash('success', "Загрузка выполнена.");
		Yii::app()->session['datainfo'] = array();

		$this->redirect('index');
	}

	public function actionAddNew() {
		$model = new ServicesRequest();
		$clinic = $this->loadModel();

		if ($attr = Yii::app()->request->getParam(get_class($model))) {

			$model->attributes = $attr;
			$model->addressId = $clinic->id;
			$model->status = 0;
			#$model->id = md5(implode('', $model->attributes));

			if ($model->save()) {

				Yii::app()->user->setFlash('success', "Услуга отправлена на модерацию.");
				$this->redirect('index');
			}
		}

		$this->render('addNew', array('model' => $model, 'clinic' => $clinic));
	}

	public function actionDescription() {
		$clinic = $this->loadModel();
		$id = Yii::app()->request->getParam('id');
		$model = AddressServices::model()->findByAttributes(['id'=>$id]);
		
		if(!is_object($model)) {
			throw new CHttpException(404,'Not Found');
		}
		elseif($model->addressId !== $clinic->id) {
			throw new CHttpException(403,'Forbidden');
		}
		else {
			$attributes = Yii::app()->request->getParam(get_class($model));
			switch(Yii::app()->request->getRequestType()) {
				case 'POST':
					$model->attributes = $attributes;
                    $response = [
                        'success' => false,
                        'message' => '',
                        'errors' => [],
                    ];
					if($model->save()) {
						$response['success'] = true;
						$response['message'] = 'Сохранено';
					} else {
						$response['success'] = false;
						$response['message'] = 'Ошибка';
						$response['errors'] = $model->getErrors()['description'];
					}
					echo json_encode($response);
					break;
				case 'GET':
					$renderParameters = array('model' => $model, 'clinic' => $clinic);
					if (Yii::app()->request->isAjaxRequest) {
						$this->renderPartial('description', $renderParameters);
					} else {
						$this->render('description', $renderParameters);
					}
					break;
			}
		}
	}

	public function actions() {
		return array(
			'exportTable' => array(
				'class' => 'ext.htmltableui.actions.HtmlExportCsv',
				'path' => '/csv/',
			),
		);
	}

	public function actionHandleHtmlTable() {
		if (isset($_POST)) {
			//return the POST variable back
			//the widget will show an alert() with this data
			print_r($_POST);
		}
	}
	
	public function actionAddServiceToAddress($serviceId) {
		$response = $this->loadModel()->addService($serviceId);
		switch (@$response['status']) {
			case "EXIST":
			case "OK":
				$this->redirect(array('index#' . $response['addressServices']->service->link));
			case "ERROR":
			default:
				exit($response['text']);
		}
	}

}
