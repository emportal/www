<?php

class AjaxController extends AdminClinicController {

	public $layout = 'empty';

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'cities' => array(
				'class'			 => 'AutoCompleteAction',
				'model'			 => 'City',
				'attributeValue' => 'name',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ( $error = Yii::app()->errorHandler->error ) {
			if ( Yii::app()->request->isAjaxRequest )
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionServiceByServiceActivite($link) {
		
		$clinic = $this->loadModel();
		
		$array = array();

		$creteria = new CDbCriteria(array(
			'order'	 => '`service`.`name`',
			'with'	 => array(
				'service'
			)
		));
		$creteria->compare('`t`.`addressId`', $clinic->id);
		$creteria->compare('`t`.`companyActivitesId`', $link);
		if(Yii::app()->request->getParam('term')) {
			$creteria->compare('service.name', Yii::app()->request->getParam('term'), true);
		}

		foreach (AddressServices::model()->findAll($creteria) as $group) {
			$array[] = array(
				'id'	 => $group->service->id,
				'value'	 => $group->service->name
			);
		}


		if(Yii::app()->request->getParam('json')) {
			header('Content-Type: application/json');
			echo CJSON::encode($array);
		}
	}
	public function actionSetAppointmentStatus($link,$status) {
		if(Yii::app()->request->getParam('json')) {
			$model = AppointmentToDoctors::model()->findByLink($link);
			$array = [];
			if($model) {
				switch($status) {
					case "seen":
						$model->statusId = AppointmentToDoctors::SEEN;
						if($model->update()) {						
							$array['msg'] = AppointmentToDoctors::$STATUSES[AppointmentToDoctors::SEEN];
							$array['actions'] = $model->actions;
							$array['success'] = true;
						} else {
							$array['success'] = false;
							$array['msg']=$model->errors;					
						}				
						break;
					case "accept":
						$model->statusId = AppointmentToDoctors::ACCEPTED_BY_REGISTER;
						if($model->update()) {						
							//Отправка сообщения пользователю о подтверждении 
							$model->sendMailToUser(AppointmentToDoctors::ACCEPTED_BY_REGISTER);
							$model->sendSmsToUser(AppointmentToDoctors::ACCEPTED_BY_REGISTER);
							$model->notifyOperator('accept');
							
							$model->sendMailToClinic(AppointmentToDoctors::ACCEPTED_BY_REGISTER);
							$model->sendSmsToClinic(AppointmentToDoctors::ACCEPTED_BY_REGISTER);
							
									
							$array['msg'] = AppointmentToDoctors::$STATUSES[AppointmentToDoctors::ACCEPTED_BY_REGISTER];
							$array['actions'] = $model->actions;
							$array['success'] = true;
						} else {
							$array['success'] = false;
							$array['msg'] = $model->errors;					
						}	
						break;
					case "decline":						
						
						$model->comment = CHtml::encode(Yii::app()->request->getParam('reason'));
						$model->statusId = AppointmentToDoctors::DECLINED_BY_REGISTER;
						$model->adminClinicComment = MyTools::createFromTemplate("Причина отклонения: %declineReason%", ["%declineReason%"=>$model->comment]);
						$model->adminClinicStatusId = AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willNotPay'];
						if($model->update()) {
							//Отправка сообщения пользователю о подтверждении 
							$model->sendMailToUser(AppointmentToDoctors::DECLINED_BY_REGISTER);
							$model->sendSmsToUser(AppointmentToDoctors::DECLINED_BY_REGISTER);
							$model->notifyOperator('decline');
							
							$model->sendMailToClinic(AppointmentToDoctors::DECLINED_BY_REGISTER);
							$model->sendSmsToClinic(AppointmentToDoctors::DECLINED_BY_REGISTER);
							
							$array['msg'] = AppointmentToDoctors::$STATUSES[AppointmentToDoctors::DECLINED_BY_REGISTER];
							$array['actions'] = $model->actions;
							$array['success'] = true;
						} else {
							$array['success'] = false;
							$array['msg'] = $model->errors;					
						}	
						break;
				}
				$array['statusId'] = $model->statusId;
			}
		
			header('Content-Type: application/json');
			echo CJSON::encode($array);
		}		
	}
	
	
	public function actionAddExpert($doctor = null) {
		$doctor = Doctor::model()->findByLink($doctor);
		if($doctor) {
			$pow = new PlaceOfWork();
			$pow->doctorId  = $doctor->id;
			$pow->companyId = Yii::app()->user->model->companyId;
			$pow->addressId = $this->loadModel()->id;
			$pow->current = 2;
			if($pow->save()) {
				echo CJSON::encode(['success'=>true]);
			} else {
				echo CJSON::encode($pow->errors);
			}			
		}
		Yii::app()->end();
		
	}
	
	public function actionRemoveExpert($doctor = null) {
		$doctor = Doctor::model()->findByLink($doctor);
		if($doctor) {
			$pow = PlaceOfWork::model()->findByAttributes([
				'addressId'	=>	$this->loadModel()->id,
				'doctorId'	=>	$doctor->id,
				'current'	=>	2
			]);
			if($pow->delete()) {
				echo CJSON::encode(['success'=>true]);
			} else {
				echo CJSON::encode(['success'=>false,'msg'=>'not found']);
			}
		}
		Yii::app()->end();
	}
	
	public function actionSetNewDate($link,$plannedTime) {
		$model = AppointmentToDoctors::model()->findByLink($link);
		if($model->address->link == Yii::app()->user->model->clinic->link) {
			$model->plannedTime = $plannedTime;
			if($model->save()) {
				$model->sendSmsToUser('plannedTimeChanged');
				echo CJSON::encode(['success' => true]);
			}
		}
	}
	
	public function actionDisableDate() {
		$addrLink = Yii::app()->request->getParam('addrLink');
		$doctorLink = Yii::app()->request->getParam('doctorLink');
		$plannedTime = Yii::app()->request->getParam('plannedTime');
		
		$address = Address::model()->findByLink($addrLink);
		$doctor = Doctor::model()->findByLink($doctorLink);
		if($address->link == Yii::app()->user->model->clinic->link) {
			$model = AppointmentToDoctors::model()->findByAttributes([
				'addressId' => $address->id,
				'doctorId' => $doctor->id,
				'plannedTime' => $plannedTime
			]);
			if(!$model) {
				$model = new AppointmentToDoctors();
				$model->addressId = $address->id;
				$model->doctorId = $doctor->id;
				$model->plannedTime = $plannedTime;
				$model->createdDate = new CDbExpression('NOW()');
				$model->statusId = AppointmentToDoctors::DISABLED;
				$model->save(false);
				echo CJSON::encode(['success' => true]);
			}			
		}
		/*
		 TODO: 
		 * 1. Запись - Адрес,компани айди, доктор и plannedtime + статус "отключение номерка"
		 * 2. Добавить метод EnableDate аналогичный, который удаляет
		 * 3. Добавить мануал как работать с календарем новым
		*/
	}
	
	public function actionEnableDate() {
		$addrLink = Yii::app()->request->getParam('addrLink');
		$doctorLink = Yii::app()->request->getParam('doctorLink');
		$plannedTime = Yii::app()->request->getParam('plannedTime');
		$address = Address::model()->findByLink($addrLink);
		$doctor = Doctor::model()->findByLink($doctorLink);
		if($address->link == Yii::app()->user->model->clinic->link) {
			$model = AppointmentToDoctors::model()->findByAttributes([
				'addressId' => $address->id,
				'doctorId' => $doctor->id,
				'plannedTime' => $plannedTime,
				'statusId' => AppointmentToDoctors::DISABLED
			]);
			
			if($model) {
				$model->delete();
				echo CJSON::encode(['success' => true]);
			}
		}
		
	}
	
	public function actionContactManagerSubmit(){
		$response = [];
		
		try {
			$postMessage = Yii::app()->request->getPost("message");
			$model = new ContactManager;
			$model->userMedicalId = $postMessage["sender"];
			$model->subject = $postMessage["subject"];
			$model->msgbody = $postMessage["msgbody"];
			$model->createDate = NULL;
			if ($model->save()) {
				$response["status"] = "OK";
				
				$userModel = UserMedical::model()->findByPk($model->userMedicalId);
				
				$mailmodel = new stdClass();
				$mailmodel->{"subject"} = $model->subject;
				$mailmodel->{"body"} = $model->msgbody;
				$mailmodel->{"email"} = $userModel->email;
				$mailmodel->{"company"} = $userModel->company->name;
				$mailmodel->{"address"} = $userModel->address->name;
				$mailmodel->{"manager"} = $userModel->company->managerRelations->user->email;
				
				$mail = new YiiMailer();
				$mail->setView('contactManager');
				$mail->setData(array( 'model' => $mailmodel ));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "[AdminClinic]: ".$mailmodel->subject;

				if(Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['devEmail']);
				else {
					$mail->AddAddress($mailmodel->manager);
					$mail->AddAddress("roman.z@emportal.ru");
					$mail->AddAddress("alexander.d@emportal.ru");
				}
				
				$mail->Send();
				
			} else {
				$response["status"] = "ERROR";
			}
		} catch(Exception $e)
		{
			$response["status"] = "ERROR";
		}
		echo json_encode($response);
	}
	
	public function actionChangeExpertsMainAddress() {
		
		$doctorLink = Yii::app()->request->getParam('expertLink');
		$newAddressId = Yii::app()->request->getParam('newMainAddressId');
		
		$doctor = Doctor::model()->findByLink($doctorLink);
		$currentPlaceOfWork = $doctor->currentPlaceOfWork;
		
		if ($currentPlaceOfWork->companyId != Yii::app()->user->model->clinic->ownerId)
		{			
			$response = ['result' => 'false', 'message' => 'access dened'];
			echo json_encode($response);
			Yii::app()->end();
		}
		
		$newPlaceOfWorkAttributes = [
			'doctorId' => $doctor->id,
			'addressId' => $newAddressId,
		];
		$newPlaceOfWork = PlaceOfWork::model()->findByAttributes($newPlaceOfWorkAttributes);
		if ($currentPlaceOfWork && $currentPlaceOfWork->id != $newAddressId)
		{
			//если врач импортирован в адрес, куда перемещаем, то просто делаем новый placeOfWork основным (current = 1)
			//и делаем не-основным (current = 2) старый адрес
			if (!$newPlaceOfWork)
			{
				$newPlaceOfWork = new PlaceOfWork();
			}			
			$newPlaceOfWork->scenario = 'changeExpertsMainAddress';
			$currentPlaceOfWork->scenario = 'changeExpertsMainAddress';
			
			$newPlaceOfWorkAttributes = array_merge($currentPlaceOfWork->attributes, $newPlaceOfWorkAttributes);
			unset($newPlaceOfWorkAttributes['id']);
			$newPlaceOfWork->attributes = $newPlaceOfWorkAttributes;
			
			$newPlaceOfWork->current = 1;			
			$currentPlaceOfWork->current = 2;
			
			if ($newPlaceOfWork->save())
			{
				$result = $currentPlaceOfWork->update();
			} else {
				$result = false;
			}
		} else {
			//не смогли получить currentPlaceOfWork, завершаем все
			$result = false;
		}
		
		$response = [
			'result' => $result
		];
		echo json_encode($response);
	}
	
	public function actionPopupShown() {
		
		$clinic = $this->loadModel();
		$userMedical = $clinic->userMedical;
		$userMedical->hasReadLatestNews = 1;
		$response = [
			'result' =>	$userMedical->update()
		];
		echo json_encode($response);
		Yii::app()->end();
	}
	
	public function actionChangeAppointmentAttribute() {
		
		$clinic = $this->loadModel();
		$appointmentLink = (int)Yii::app()->request->getParam('appointmentLink');
		$attributeName = Yii::app()->request->getParam('attributeName');
		$attributeNewValue = Yii::app()->request->getParam('attributeNewValue');
		$scenario = Yii::app()->request->getParam('scenario');
		
		$appointment = AppointmentToDoctors::model()->findByLink($appointmentLink);
		if ($clinic->id !== $appointment->address->id OR !in_array($attributeName, ['adminClinicStatusId', 'adminClinicComment'])) {
			$response = ['success' => 'false', 'msg' => 'У вас нет прав на редактирование атрибутов этой заявки'];
			echo CJSON::encode($response);
			Yii::app()->end();
		}
		
  		// $passDate = date('Y-m-', strtotime($appointment->plannedTime)) . Yii::app()->params['lastDayForAppCollation'] . ' +1 month +1day';
		// $collationDayPassed = (strtotime('now') > strtotime($passDate));
		// if (in_array($attributeName, ['adminClinicStatusId', 'adminClinicComment']) && $collationDayPassed && !Yii::app()->params['devMode'])
		// {
		// 	$response = ['success' => 'false', 'msg' => 'В этом месяце уже нельзя редактировать статус заявок, т.к. счет уже сформирован.'];
		// 	echo CJSON::encode($response);
		// 	Yii::app()->end();
		// }

        //$passDate = date('Y-m-', strtotime($appointment->plannedTime)) . Yii::app()->params['lastDayForAppCollation'] . ' +1 month +1day';
		//$collationDayPassed = (strtotime('now') > strtotime($passDate));
		$collationDayPassed = (date('Y-m-d', strtotime($appointment->createdDate)) != date('Y-m-d', time())); 
		if (in_array($attributeName, ['adminClinicStatusId', 'adminClinicComment']) && $collationDayPassed && !Yii::app()->params['devMode'])
		{
			$response = ['success' => 'false', 'msg' => 'Срок редактирования статуса заявок прошел.'];
			echo CJSON::encode($response);
			Yii::app()->end();
		}
		
		if ($scenario)
			$appointment->scenario = $scenario;

		$reportMonth = Yii::app()->request->getParam('reportMonth');
		$typeName = (($attributeName==="adminClinicComment") ? LogAdminClinicType::$NAME_CHANGE_ADMIN_CLINIC_COMMENT : 
					(($attributeName==="adminClinicStatusId") ? LogAdminClinicType::$NAME_CHANGE_ADMIN_CLINIC_STATUS : LogAdminClinicType::$NAME_UNDEFINED));
		$type = LogAdminClinicType::model()->findByAttributes(["name"=>$typeName]);
		$log = [
				'addressId'=>$clinic->id,
				'billId'=>($reportMonth ? $clinic->getBill($reportMonth)->id : null),
				'typeId'=>$type->id,
				'data'=>CJSON::encode([['attributeName'=>$attributeName, 'oldValue'=>$appointment->{$attributeName}, 'newValue'=>$attributeNewValue]]),
		];
			
		$appointment->{$attributeName} = $attributeNewValue;
		$result = $appointment->update();
        $response = ['success' => $result];

        //get totalSale from Bill
        $model = AppointmentToDoctors::model();
		$model->visit_time_period = MyTools::getDateInterval($reportMonth);
		$periodName = MyTools::convertReportMonthToPeriodName($reportMonth);
		$appReportsInfo = $clinic->getInfoForAppointmentReports($model, $periodName);
		$totalSale = $appReportsInfo['totalSale'];

        $response +=['totalSale'=>$totalSale];
        //end totalSale from Bill 
        
        if (true === Yii::app()->params['devMode']) {
            $bill = $clinic->getBill($reportMonth);
            $response += ['billUpdateFigures' => $bill->updateFigures(false)];
        }
		
		echo CJSON::encode($response);
		if($result) { LogAdminClinic::Add($log); }
		Yii::app()->end();
	}
	
	public function actionChangeUserAttribute() {
		$response = [];
		$userMedical = $this->loadModel()->userMedical;
        switch(Yii::app()->request->getParam('type')) {
            case 'agreementLoyaltyProgram':
                $userMedical->agreementLoyaltyProgram = (Yii::app()->request->getParam('accept') === 'true') ? 1 : 0;
                $response = ['success' => $userMedical->update()];
                break;
            case 'agreementService':
                $userMedical->agreementService = (Yii::app()->request->getParam('accept') === 'true') ? 1 : 0;
                $response = ['success' => $userMedical->update()];
                break;
            default:
                $response = ['success' => false, 'message' => 'no type passed'];
                break;
        }
        
		echo CJSON::encode($response);
		Yii::app()->end();
	}
	
	public function actionLog($typeName=null) {
		if($typeName === null) { $typeName = LogAdminClinicType::$NAME_UNDEFINED; }
		$type = LogAdminClinicType::model()->findByAttributes(["name"=>$typeName]);
		$clinic = $this->loadModel();
		$reportMonth = Yii::app()->request->getParam('reportMonth');
		$success = LogAdminClinic::Add([
				'addressId'=>$clinic->id,
				'billId'=>($reportMonth ? $clinic->getBill($reportMonth)->id : null),
				'typeId'=>$type->id,
				'data'=>CJSON::encode([]),
		]);
		return CJSON::encode(["success"=>$success]);
	}
}
