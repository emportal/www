<?php
/*
<?xml version="1.0" encoding="utf8"?>

<table_data name="appointmenttodoctors">
	<row>
		<field name="doctorId">06a316a2-03f0-11e2-930f-e840f2aca94f</field>
		<field name="companyId">15b7b540-edd0-11e1-b127-e840f2aca94f</field>
		<field name="userId" xsi:nil="true" />
		<field name="plannedTime">2014-12-15 18:00:00</field>
		<field name="serviceId" xsi:nil="true" />
		<field name="addressId">15b7b542-edd0-11e1-b127-e840f2aca94f</field>
		<field name="name">ТЕСТ3</field>
		<field name="email">test1@test1.ru</field>
		<field name="phone">79119999999</field>
		<field name="isOnHouse" xsi:nil="true" />
		<field name="link">000000456</field>
		<field name="createdDate">0000-00-00 00:00:00</field>
		<field name="statusId">1</field>
		<field name="id">EMPORTAL-b11f-a997-a40d-7e4f2a3d2a08</field>
		<field name="comment" xsi:nil="true" />
		<field name="isSentThanksMail">0</field>
		<field name="isNotVisited">0</field>
		<field name="isNotVisitedHash"></field>
		<field name="appType">own</field>
		<field name="visitType"></field>
	</row>
</table_data>

*/
class CrmController extends AdminClinicController
{
	public function actionDeleteApp() {
		$response = [];
		$appId;
		if(isset($_GET["id"])) {
			$appId = $_GET["id"];
		} elseif(isset($_POST["id"])) {
			$appId = $_POST["id"];
		}
		if(empty($appId)) {
			$response["status"] = "ERROR";
			$response["text"] = "Идентификатор записи не задан!";
		}
		else {
			$model = $this->loadModel();
			AppointmentToDoctors::$showOwn = true;
			$editableApp = AppointmentToDoctors::model()->findByPk($appId);
			if(empty($editableApp)) {
				$response["status"] = "ERROR";
				$response["text"] = "Нет такой записи!";
			}
			elseif($editableApp->addressId !== $model->id) {
				$response["status"] = "ERROR";
				$response["text"] = "У Вас нет прав для удаления этой записи!";
			}
			elseif($editableApp->appType !== AppointmentToDoctors::APP_TYPE_OWN) {
				$response["status"] = "ERROR";
				$response["text"] = "Эту запись нельзя удалять!";
			}
			if(isset($_POST["id"])) {
				if($editableApp->delete()) {
					$response["status"] = "OK";
					$response["text"] = "Запись удалена!";
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "";
				}
			}
		}
		if(isset($_GET["id"]) || $_SERVER['REQUEST_METHOD'] == "GET") {
			$this->render('deleteApp', [
				'model' => $model,
				'editableApp' => $editableApp,
				'response' => $response,
			]);
		} else {
			echo json_encode($response);
		}
	}
	
	public function actionEditApp() {
		if(empty($_GET["id"])) {
			die('<h2>Идентификатор записи не задан!</h2>');
		}
		if(isset($_GET["id"])) {
			$model = $this->loadModel();
			$doctors = Doctor::model()->findAll([			
				'order' => 't.name ASC',
				'with' => [
					'currentPlaceOfWork' => [
						'together' => true,
						'select' => false,
					]
				],
				'condition' => 'currentPlaceOfWork.addressId = :id',
				'params' => [
					':id' => $model->id
				]
			]);
			
			AppointmentToDoctors::$showOwn = true;
			$editableApp = AppointmentToDoctors::model()->findByPk($_GET["id"]);
			
			if(empty($editableApp)) {
				$this->render('//site/error', ['message' => 'Запись не найдена!', 'code' => 404]);
				Yii::app()->end();
			}
			elseif($editableApp->addressId !== $model->id) {
				$this->render('//site/error', ['message' => 'У Вас нет прав для редактирования этой записи!', 'code' => 403]);
				Yii::app()->end();
			}
			elseif($editableApp->appType !== AppointmentToDoctors::APP_TYPE_OWN) {
				$this->render('//site/error', ['message' => 'Вы не можете отредактировать эту запись, потому что она поступила от пользователя через ЕМПортал. Вы можете редактировать только те записи, которые добавили', 'code' => 405]);
				Yii::app()->end();
			}
			
			if(isset($_POST['params'])) {
					
				$params = Yii::app()->request->getParam('params');
				$successMessage = '';
				$editableApp->attributes = $_POST['params'];
				$doc = Doctor::model()->findByAttributes(['link' => $editableApp->doctorId]);
				$editableApp->doctorId = $doc->id;
				$editableApp->addressId = $model->id;
				$editableApp->companyId = $model->company->id;
				$editableApp->userId = NULL;
				$editableApp->serviceId = NULL;
				$editableApp->appType = AppointmentToDoctors::APP_TYPE_OWN;
				if (!empty($_POST['params']['plannedTime']) AND strlen($_POST['params']['plannedTime'])<17) $editableApp->plannedTime =  $_POST['params']['plannedTime'].':00';
				$editableApp->statusId = AppointmentToDoctors::ACCEPTED_BY_REGISTER;
				
				if(empty($editableApp->doctorId)) {
					$successMessage = 'Ошибка! Доктор не задан!';
				}
				elseif(empty($editableApp->plannedTime)) {
					$successMessage = 'Ошибка! Время не задано!';
				}
				elseif(empty($editableApp->name)) {
					$successMessage = 'Ошибка! ФИО не задано!';
				}
				elseif(empty($editableApp->phone)) {
					$successMessage = 'Ошибка! Телефон не задан!';
				}
				elseif ($editableApp->update()) {
					$successMessage = 'Запись на приём успешно отредактирована. Перейти в <a href="../default/visits">регистратуру</a>?';
				}
					
				$sysMessage = CHtml::errorSummary($editableApp);
			}

			//echo "<br>".$editableApp->doctorId." | ".$doc->id; exit;
			$doc = Doctor::model()->findByAttributes(['id' => $editableApp->doctorId]);
			$editableApp->doctorId = $doc->link;
			
			$this->render('newApp', [
				'model' => $model,
				'doctors' => $doctors,
				'sysMessage' => $sysMessage,
				'newApp' => $editableApp,
				'successMessage' => $successMessage,
			]);
		}
	}

	public function actionNewapp() {
		
		$model = $this->loadModel();
		$doctors = Doctor::model()->findAll([			
			'order' => 't.name ASC',
			'with' => [
				'currentPlaceOfWork' => [
					'together' => true,
					'select' => false,
				]
			],
			'condition' => 'currentPlaceOfWork.addressId = :id',
			'params' => [
				':id' => $model->id
			]
		]);
		
		if(isset($_POST['params'])) {
			
			$params = Yii::app()->request->getParam('params');
			$successMessage = '';			
			$newApp = new AppointmentToDoctors('visit_to_doctor');
			$newApp->attributes = $_POST['params'];			
			
			$doc = Doctor::model()->findByAttributes(['link' => $newApp->doctorId]);
			$newApp->doctorId = $doc->id;
			
			$newApp->addressId = $model->id;
			$newApp->companyId = $model->company->id;
			$newApp->userId = NULL;
			$newApp->serviceId = NULL;
			$newApp->appType = AppointmentToDoctors::APP_TYPE_OWN;			
			if ($newApp->plannedTime AND strlen($newApp->plannedTime)<17) $newApp->plannedTime =  $_POST['params']['plannedTime'].':00';
			$newApp->statusId = AppointmentToDoctors::ACCEPTED_BY_REGISTER;
			
			if ($newApp->validate()) {
				
				$newApp->save();
				$successMessage = 'Новая запись на прием успешно создана. Перейти в <a href="../default/visits">регистратуру</a>?';
			}
			
			$sysMessage = CHtml::errorSummary($newApp);
			if (!empty($successMessage)) $newApp = [];
		} else {
			$newApp = new stdClass();
			foreach($_GET as $key => $value)
			{
				$newApp->{$key} = $value;
			}
		}
		
		
		# = $model->doctors;
		$this->render('newApp',  [
			'model' => $model,
			'doctors' => $doctors,
			'sysMessage' => $sysMessage,
			'newApp' => $newApp,
			'successMessage' => $successMessage,
		]);
	}

}
