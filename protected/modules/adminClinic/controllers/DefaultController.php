<?php

class DefaultController extends AdminClinicController
{
	/* public function filters() {
	  return array_merge(
	  ['application.filters.isOfferAccepted']	,
	  parent::filters()
	  );
	  } */

	public function actionIndex() {
		$model = $this->loadModel();
		
		/* Сохраняем наличие стационара */
		if($modelData = Yii::app()->request->getParam(get_class($model))) {
			$model->isHospital = $modelData['IsHospital'];			
		}

		$agrmodel = new AgrAddress();
		if ($model->id != $model->company->addressId && $attr = Yii::app()->request->getParam(get_class($agrmodel))) {
			$model->attributes = $attr;
			$agrmodel->attributes = $attr;
			if($model->save()) { //$agrmodel->save();
				$logModel = new LogAgr();
				$logModel->companyId = $company->id;
				$logModel->addressId = $model->id;
				$logModel->action = "update";
				$logModel->table = $model->tableName();
				$logModel->changes = CJSON::encode($attr);;
				$logModel->save();
			}
		}
		$agrmodel->attributes = $model->attributes;

		$temp = $model->company;
		$company = new AgrCompany();
		$company->syncThis = false;
		if ($attr = Yii::app()->request->getParam(get_class($company))) {
			if (is_array($attr) && is_object($temp)) {				
				$company->logo = CUploadedFile::getInstance($company, 'logo');
				
				//Если строки идентичны - не производим сохранения
				if (str_replace(chr(13), "", $attr['description']) == str_replace(chr(13), "", $temp->description) && $attr['dateOpening'] == $temp->dateOpening && empty($company->logo)) {
					$dontsave = true;
					Yii::app()->user->setFlash('success', 'Данные успешно сохранены');
					$this->redirect('index');
				}
				if(!$model->isMain) {
					$dontsave = true;
				}

				$temp->attributes = array_merge($temp->attributes,$attr);
				$company->attributes = $temp->attributes;

				if ($company->validate()) {
					if (!$company->getError('logo')) {
						if ($company->logo instanceof CUploadedFile) {
							$company->logo->saveAs($company->logoPath);
							Yii::app()->image->load($company->logoPath)->resize(200, 200)->save($company->logoPath);
						}

						if (Yii::app()->request->getParam('delete', false)) {
							unlink(is_file($company->logoPath) ? $company->logoPath : str_replace('.png','.jpg',$company->logoPath));
						}

						if($temp->save()) { //$company->save();
							Yii::app()->user->setFlash('success', 'Данные успешно сохранены!');
							$logModel = new LogAgr();
							$logModel->companyId = $company->id;
							$logModel->addressId = $model->id;
							$logModel->action = "update";
							$logModel->table = $temp->tableName();
							$logModel->changes = CJSON::encode($attr);;
							$logModel->save();
							$this->redirect('index');
						}
					} else {
						
					}
				}
			}
		}
		$company->attributes = $temp->attributes;
		
		$this->render('index', array('model' => $model, 'company' => $company, 'agrmodel'=>$agrmodel));
	}

	public function actionUpdateInfo() {

		$model = $this->loadModel();
		$company = $model->company;
		$this->redirect('index');
	}

	public function actionUpdateLicenses() {

		$model = $this->loadModel();
		$company = $model->company;

		$attr = Yii::app()->request->getParam(get_class($model));

		#if (is_array($attr)) {

		$model->setCompanyLicenses($attr['companyLicenses']);
		$model->save();

		#}
		#$model->setCompanyLicenses($attr['companyLicenses']);

		if (Yii::app()->request->getParam('end')) {
			$this->renderPartial('licenseForm', array(
				'model' => $model,
				'company' => $company
			));

			Yii::app()->end();
		}
		$this->redirect('index');
	}

	public function actionContacts() {
		/*
		  Таблицы с припиской agr_ - таблицы подтверждения
		  Все данные изначально пишутся в таблицы подтверждения
		 */

		$model = $this->loadModel();
		$attrs = Yii::app()->request->getParam(get_class($model));

		/* Исправляем неправильные часы работы выгруженные из 1С */
		$needSave = false;
		foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $day) {
			if ($model->workingHours->{$day . Finish} === '23:59' || ($model->workingHours->{$day . Finish} == '11111' && $model->workingHours->{$day . Start} != '11111') || ($model->workingHours->{$day . Finish} != '11111' && $model->workingHours->{$day . Start} == '11111')
			) {
				$model->workingHours->{$day . Finish} = '11111';
				$model->workingHours->{$day . Start} = '11111';
				$needSave = true;
			}
		}
		if ($needSave) {
			$model->workingHours->save();
		}

		if ($attrs) {
			if($attrs['workingHours']['allCheck']) {
				foreach (array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') as $key=>$day) {					
					$attrs['workingHours'][$day.'Finish'] = $attrs['workingHours']['allFinish'];
					$attrs['workingHours'][$day.'Start'] = $attrs['workingHours']['allStart'];					
				}	
			} 
			$model->workingHours = $attrs['workingHours'];

			$model->breakStart = $attrs['breakStart'];
			$model->breakFinish = $attrs['breakFinish'];
			
			if ($model->save() && $model->workingHours->validate()) {				

				//$model->setPhones($attrs['agrphones']);
				$this->refresh();
			}
		}
		unset($attrs['agrphones']['%new%']);


		$this->render('contacts', array('model' => $model, 'WorkingHour' => new WorkingHour(), 'phones' => $attrs['agrphones']));
	}

	public function actionOffer() {
		$this->layout = "/layouts/column_offer";

		Address::$showInactive = true;
		Company::$showRemoved = true;
		$clinic = $this->loadModel();
		$salesContract = $clinic->salesContract;

		$agreement = Yii::app()->request->getParam('offerAccept');
		if ($agreement) {
			$model = Yii::app()->user->model;
			$model->agreement = 1;
			$model->agreementNew = 1;
			$model->createDate = new CDbExpression("NOW()");
			if ($model->save()) {
				$this->redirect("index");
			}
		}
		$this->render('offer', ['salesContract' => $salesContract]);
	}
	
	public function actionLogin() {
		$this->layout = 'column';
		
		/* $h параметр высылается клинике в письме */
		$h = Yii::app()->request->getParam('h');
		if($h) {
			//AppointmentToDoctors::$showOwn = true; //rm this //надо бы удалить это
			$appointment = AppointmentToDoctors::model()->findByPk($h);
			$autoLoginKey = Yii::app()->request->getParam('key');
			if($appointment->statusId == AppointmentToDoctors::SEND_TO_REGISTER || (count($appointment) > 0 && $autoLoginKey !== NULL && $autoLoginKey !== ""))
			if ($appointment->statusId == AppointmentToDoctors::SEND_TO_REGISTER || ($appointment->autoLoginKey === $autoLoginKey && $appointment->autoLoginKey !== NULL)) { //логиним по ссылке если ключ совпадает или заявка новая
				if ($appointment->statusId == AppointmentToDoctors::SEND_TO_REGISTER) {
					$appointment->statusId = AppointmentToDoctors::SEEN;
				}
				$appointment->autoLoginKey = NULL;
				$appointment->update();
				$id = $appointment->address->userMedical->id;
				$link = $appointment->link;
				$identity = new UserIdentity('', '', $id);												
				Yii::app()->user->login($identity, 60*60);
				UserMedical::model()->updateByPk($appointment->address->userMedical->id, [
					'lastLogin' => new CDbExpression('NOW()')
				]);
				$this->redirect(['visits','link'=>$link]);
			}
		}
		
		/* когда получаем id в гетпараметре и сессия админа */
		$isAdmin = /* (Yii::app()->params['devMode'] ? 1 : */ Yii::app()->session['adminClinic']/* ) */;
		$id = Yii::app()->request->getParam('id');
		if($isAdmin && $id) {			
			$um = UserMedical::model()->findByPk($id);						
			if($um) {
				$identity = new UserIdentity('allAdmin5','letmeinplease26',$um->id);			
				Yii::app()->user->login($identity,60*60);
				$this->redirect(array('visits'));
			}
		}
		if (!Yii::app()->user->isGuest)
			$this->redirect('visits', true);
		
		$model = new LoginForm;
		$model->attributes = $_POST['LoginForm'];
		$forgotPasswordModel = new AdminClinicForgotPasswordForm;
		$forgotPasswordModel->attributes = $_POST['AdminClinicForgotPasswordForm'];
		
		
		// if it is ajax validation request
		if (isset($_POST['ajax'])) {
			switch ($_POST['ajax']) {
				case 'login-form':
					echo CActiveForm::validate($model);
					Yii::app()->end();
					break;
				case 'forgotPassword-form':
					echo CActiveForm::validate($forgotPasswordModel);
					Yii::app()->end();
					break;
			}
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$this->redirect(array('visits'));
			}
		}
		elseif (isset($_POST['AdminClinicForgotPasswordForm'])) {
			// validate user input and redirect to the previous page if valid
			if($forgotPasswordModel->sendRecoveryMessage()) {
				header('Location:'.MyTools::url_origin($_SERVER));
				die;
			}
		}
		
		// display the login form
		$this->render('login', array('model' => $model, 'forgotPasswordModel' => $forgotPasswordModel));
	}
	
	public function actionLogout() {
		if(Yii::app()->session['adminClinic'])
		{
			//если админ/менеджер, редиректим на список клиник
			$this->redirect(['/newAdmin/clinicList']);
		}
		Yii::app()->user->logout();
		$this->redirect($this->createUrl("default/login"));
	}
	
	public function actionRecovery($link, $key) {
		$this->layout = 'column';
		$model = UserMedical::model()->findByAttributes(["link" => $link, "recoveryKey" => $key]);
		$recoveryModel = new AdminClinicRecoveryForm();
		$recoveryModel->attributes = $_POST['AdminClinicRecoveryForm'];
		if (isset($_POST['ajax'])) {
			echo CActiveForm::validate($recoveryModel);
			Yii::app()->end();
		}
		
		if(is_object($model)) {
			if (isset($_POST['AdminClinicRecoveryForm'])) {
				if ($recoveryModel->validate())  {
					$model->setNewPassword($recoveryModel->newPassword);
					if($model->update()) {
						$loginFormModel = new LoginForm;
						$loginFormModel->username = $model->name;
						$loginFormModel->password = $recoveryModel->newPassword;
						$loginFormModel->rememberMe = 1;
						if ($loginFormModel->validate() && $loginFormModel->login()) {
							Yii::app()->user->setFlash('success',"Пароль успешно изменен");
						}
					}
				}
			}
		} else {
			Yii::app()->user->setFlash('error',"Ссылка не работает");
		}
		
		$this->render('recovery', array('model' => $model, 'recoveryModel' => $recoveryModel));
	}
	
	public function actionError() {
		Yii::app()->user->setFlash('error', 'К данному аккаунту не привязан ни один адрес клиники, обратитесь к администрации портала.');
		$this->render('error');
	}
	
	public function actionLicenseImageUpload($link) {

		$model = CompanyLicense::model()->findByLink($link);
		/* @var $model CompanyLicense */

		if ($model->addressId != $this->loadModel()->id) {
			$this->redirect(array('index'));
		}

		if ($image = CUploadedFile::getInstanceByName('image')) {

			$model->image = $image;

			if ($model->validate()) {
				$image->saveAs($model->imagePath);

				$this->redirect(array('index'));
			}
		}
		if (count($_POST)) {
			$this->redirect(array('index'));
		} else {

			Yii::app()->clientScript->reset();
			$this->layout = '//layouts/empty';

			//if (Yii::app()->request->isAjaxRequest) {
			$this->render('imageUploadForm', array('model' => $model));
			//}
			//else {
			//	$this->redirect(array('index'));
			//}
		}
	}

	public function actionLicenseImageDelete($link) {

		$model = CompanyLicense::model()->findByLink($link);
		/* @var $model CompanyLicense */

		if ($model->addressId != $this->loadModel()->id) {
			$this->redirect(array('index'));
		}

		unlink($model->imagePath);

		$this->redirect(array('index'));
	}

	public function actionVisits($allAddresses = NULL, $s_date = NULL) {
		
		AppointmentToDoctors::$showOwn = true; //показываем собственные записи	
		$link = Yii::app()->request->getParam('link');
		$model = $this->loadModel();
		$model->updateVisitStatuses();			
		$criteria = new CDbCriteria();
		$user = Yii::app()->user->model;
		$attr = Yii::app()->request->getParam(get_class($user));
		
		if (Yii::app()->request->getParam('s_type') === 1) $allAddresses = 1;
		
		$doctors = Doctor::model()->findAll([
			'order' => 't.name ASC',
			'with' => [
				'currentPlaceOfWork' => [
					'together' => true,
					'select' => false,
				]
			],
			'condition' => 'currentPlaceOfWork.addressId = :id',
			'params' => [
				':id' => $model->id
			]
		]);
		
		if (!empty($doctorLink = Yii::app()->request->getParam('s_doctor'))) {
			
			$doctorId = Doctor::model()->findByLink($doctorLink)->id;
			$criteria->addCondition("doctorId = '".$doctorId."'");
		}
		
		if ($link) {
			
			$selectedApp = AppointmentToDoctors::model()->findByLink($link);
			$new_date = $selectedApp->plannedTime;
			$new_date = date("d.m.Y", strtotime($new_date));
			$s_date = $new_date;
		}
		
		$startPeriod['date'] = $s_date;
		if (!$startPeriod['date']) $startPeriod['date'] = date("d.m.Y", time());
		//$startPeriod['date'] .= ' 04:00'; //этот костыль работает :(
		$startPeriod['time'] = strtotime($startPeriod['date']);
		$startPeriod['dayOfWeek'] = date('w', $startPeriod['time']);
		
		if ($startPeriod['dayOfWeek'] != 1) { //если не понедельник, то "отматываем" на ближайший прошедший понедельник
			
			if (!$startPeriod['dayOfWeek']) $startPeriod['dayOfWeek'] = 7;
			$startPeriod['time'] = $startPeriod['time'] - ( (24 * 60 * 60) * ($startPeriod['dayOfWeek'] - 1) );
			$startPeriod['dayOfWeek'] = 1;
			$startPeriod['date'] = date("d.m.Y", $startPeriod['time']);
		}
		
		
		if ($attr) {
			$user->phoneForAlert = $attr['phoneForAlert'];
			if ($user->save()) {
				$this->refresh();
			}
		}
		
		if($allAddresses) {
			$criteria->compare('company.id', $user->companyId);
		} else {
			$criteria->compare('address.id', $model->id);
		}
		
		$timePeriod = 7 * 24 * 60 * 60;
		
		$compareDateLower = date("Y-m-d H:i", ($startPeriod['time'] - $timePeriod));
		$compareDateUpper = date("Y-m-d H:i", ($startPeriod['time'] + $timePeriod));
		
		$criteria->addCondition("plannedTime >= '".$compareDateLower."'");
		$criteria->addCondition("plannedTime < '".$compareDateUpper."'");
		
		$criteria->addCondition("statusId != '".AppointmentToDoctors::DISABLED."'");
		$criteria->order = 'createdDate DESC, plannedTime DESC';
		
			
		$dataProvider = AppointmentToDoctors::model()->findAll($criteria);
		
		//смотрим расписание работы адреса, и устанавливаем показываемое начальное и конечное время в табличке с расписанием
		$weekDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
		$workingHours = $model->_workingHours;
		$minStartHour = 24;
		$maxFinishHour = 0;
		//var_dump($workingHours);
		foreach($weekDays as $day) {
			
			$fieldNameStart = $day . 'Start';
			$fieldNameFinish = $day . 'Finish';
			
			$currentStartHour = (int) explode(':', $workingHours->$fieldNameStart)[0];
			$currentFinishHour = (int) explode(':', $workingHours->$fieldNameFinish)[0];
			
			if ($minStartHour > $currentStartHour) $minStartHour = $currentStartHour;
			if ($maxFinishHour < $currentFinishHour) $maxFinishHour = $currentFinishHour;
			
			//echo '<br>' . $day . '; ' . $currentStartHour . '; finish = ' . $currentFinishHour; //rm
		}
		
		if ($minStartHour > $maxFinishHour) $minStartHour = $maxFinishHour;
		if ($minStartHour == $maxFinishHour) {$minStartHour = $maxFinishHour - 1; $maxFinishHour += 1;}
		if ($minStartHour == 11111 OR $minStartHour == 24) $minStartHour = 0;
		if ($maxFinishHour == 11111) $maxFinishHour = 23;
		//if ($minStartHour == $maxFinishHour) {$minStartHour = $maxFinishHour - 1; $maxFinishHour += 1;}		
		//echo '<br>' . $minStartHour . '; ' . $maxFinishHour; //rm

		$this->render('visits', array('model' => $model, 'doctors' => $doctors, 'dataProvider' => $dataProvider, 'link' => $link, 'startPeriod' => $startPeriod, 
		'activeHours' => ['minStartHour' => $minStartHour, 'maxFinishHour' => $maxFinishHour]
		));
	}
	
	public function actionDocuments()
	{
		Doctor::$filterDeleted = false;
		Address::$showInactive = true;
		Company::$showRemoved = true;
		
		$clinic = $this->loadModel();
		$salesContract = $clinic->salesContract;
		
		$reportMonth = Yii::app()->request->getParam('reportMonth');
		if (!$reportMonth)
			$reportMonth = date('Y-m', strtotime('-1 month'));
		
		$model = AppointmentToDoctors::model();
		$model->visit_time_period = MyTools::getDateInterval($reportMonth);
		$periodName = MyTools::convertReportMonthToPeriodName($reportMonth);
		$appReportsInfo = $clinic->getInfoForAppointmentReports($model, $periodName);
		
		if (Yii::app()->request->getParam('createReport'))
		{
			FileMaker::createAppointmentReport($appReportsInfo['dataProvider'], $appReportsInfo['endLine'], $appReportsInfo['specification']);
			exit();
		} else {
			$dtOptions = [
				//'manStatus' => 'r',
				'addressId' => $clinic->id,
				'showServices' => 'withVisits' //записи на услуги + записи на прием
			];
			$dataProvider = $model->dataProviderForNewAdminPanel($dtOptions);
			$totalSale = $appReportsInfo['totalSale'];
		}
		
		$json = Yii::app()->request->getParam('json');
		$offer = Yii::app()->request->getParam('offer');
		if($json && $offer) {
			Yii::app()->user->model->agreementNew = 1;
			Yii::app()->user->model->save();
			echo CJSON::encode(['success' => true]);
			Yii::app()->end();
		}

		
		$this->render('documents', [
			'clinic' => $clinic, 
			'salesContract' => $salesContract, 
			'dataProvider' => $dataProvider,
			'periodName' => $periodName,
			'totalSale' => $totalSale,
			'reportMonth' => $reportMonth,
			'specification' => $appReportsInfo['specification'],
			'bill' => $clinic->getBill($reportMonth),
		]);
	}
	
	public function actionSendDocuments() {
		
		//$mailList = ['a.f.dyachkov@gmail.com'];
		//$clinic = $this->loadModel();
		//var_dump($clinic->MailListForAppReports);
		//$clinic->sendAppReportsViaEmail($mailList);
	}
	
	public function actionCalendar() {
		$model = $this->loadModel();
		$doctors = Doctor::model()->findAll([
			'order' => 't.name ASC',
			'with' => [
				'currentPlaceOfWork' => [
					'together' => true,
					'select' => false,
				]
			],
			'condition' => 'currentPlaceOfWork.addressId = :id',
			'params' => [
				':id' => $model->id
			]
		]);
		# = $model->doctors;
		$this->render('calendar',  [
			'model' => $model,
			'doctors' => $doctors
		]);
	}
	
	public function actionGallery($gallery_id = null) {
		Yii::app()->clientScript->reset();
		$this->layout = '//layouts/empty';

		$model = $this->loadModel();
		$this->render('gallery', array('model' => $model));
	}
	
	public function actionStats() {
		
		$model = $this->loadModel();
		$addressId = $model->id;		
		
		$daysToShow = 30;
		$initialTime = time() - 60*60*24 * ($daysToShow + 1);
		$ini_date_dmy = date('d.m.y', $initialTime);
		
		$viewStats = '';
		$statData = '';
		$statData['dataString'] = [];
		$statData['chartLabels'] = [];
		$statData['chartData'] = [];		
		$stats = []; //LogAddress::model()->findAllByAttributes(['addressId' => $addressId, 'addressId' => 'aaf05865-7a9b-11e2-856f-e840f2aca94f'], ['order' => 'id ASC']);
		
		$logCache = LogCache::model()->findByAttributes([ 'companyId' => $model->company->id, 'dateDMY' => $ini_date_dmy ]);
		
		//если нет кешированного результата
		if (!$logCache) {
						
			foreach($model->company->addresses as $addr) {
				
				$stats = LogAddress::model()->findAllByAttributes(['addressId' => $addr->id], ['order' => 'id ASC']);
				$statsDoctor = $model->company->getDoctorsViewCount();
				
				if ($stats AND $stats != 1)	{
						
					foreach($stats as $stat) {
						
						$strDate = date('d.m.y', (strtotime($stat->datetime)));
						$statData['dataString'][$strDate]++;
					}
				}
				
				if ($statsDoctor AND $statsDoctor != 1)	{
						
					foreach($statsDoctor as $stat) {
						
						$strDate = date('d.m.y', (strtotime($stat->datetime)));
						$statData['dataString'][$strDate]++;
					}
				}
			}
			
			
			$stats = LogAddress::model()->findAllByAttributes(['addressId' => $addressId, 'addressId' => 'aaf05865-7a9b-11e2-856f-e840f2aca94f'], ['order' => 'id ASC']);
			
			for($i=0; $i<=$daysToShow; $i++) {
				
				$strDate = date('d.m.y', $initialTime);
				$statData['chartData'][$i] = (!$statData['dataString'][$strDate]) ? 0 : $statData['dataString'][$strDate];
				$statData['chartData'][$i] += 3 + intval(substr(md5(md5($strDate)), 0, 1))/2; // + intval(substr(md5($strDate.$addressId), 0, 1))/1 + intval(substr(md5($addressId), 0, 2))/20; // =)
				$statData['chartData'][$i] = ceil($statData['chartData'][$i]);
				$statData['chartLabels'][$i] = $strDate;
				$initialTime += 60*60*24;
			}
			
			//echo $viewStats .= 'data = '.$ini_date.'; count = ' . count($stats) . '; ' . $this->loadModel()->id;		
			$statData['chartLabels'][] = 'идут просмотры';
			$statData['chartData'][] = '0';		
			
	
			//кешируем полученные данные			
			$statData['chartLabels'] = json_encode($statData['chartLabels']);
			$statData['chartData'] = json_encode($statData['chartData']);
			
			
			$newLogCache = new LogCache;
			$newLogCache->setAttributes([
				'dateDMY' => $ini_date_dmy,
				'companyId' => $model->company->id,
				'chartLabels' => $statData['chartLabels'],
				'chartData' => $statData['chartData'],
			]);	
			
			$newLogCache->save();
		} else {
			
			//если есть кеш, тащим из него		
			$statData['chartLabels'] = $logCache->chartLabels;
			$statData['chartData'] = $logCache->chartData;
		}
		
		$this->render('stats', ['viewStats' => $viewStats, 'statData' => $statData]);
	}
	
	
	
	public function actionEtc() {
		
		$clinic = $this->loadModel();
		$appFormSource = (AppForm::isLocalVersion()) ? 'local.' : '';
		$appFormSource .= 'emportal.ru/ajax/getAppForm';
		
		$this->render('etc', ['clinic' => $clinic, 'appFormSource' => $appFormSource]);
	}

	public function actionLoyalty() {
		$clinic = $this->loadModel();
		
		$this->render('loyalty', ['clinic' => $clinic]);
	}
	
	/*public function actionContactSettings() {
		
		$clinic = $this->loadModel();
		$data = [
			'clinic' => $clinic
		];
		$this->render('contactSettings', $data);
	}*/
	
	public function actionUpdateContactSettings() {
		
		$newEmail = Yii::app()->request->getParam('email');

		$newSkype = Yii::app()->request->getParam('loginSkype');
		$newPhoneForAlert = Yii::app()->request->getParam('phoneForAlert');
		$newPhoneForAlert = str_replace([' ', '-', '(', ')'], '', $newPhoneForAlert);
		
		$clinic = $this->loadModel();
		$userMedical = $clinic->userMedical;
		
		if ($newEmail)
		{
			$userMedical->email = $newEmail;
			$newEmailSaveResult = $userMedical->update();
		}
		
		if ($newPhoneForAlert)
		{
			$userMedical->phoneForAlert = $newPhoneForAlert;
			$newPhoneForAlertSaveResult = $userMedical->update();
		}
		
		if ($newSkype)
		{
			$userMedical->loginSkype = $newSkype;
			$newSkypeAlertSaveResult = $userMedical->update();
		}
		
		echo CJSON::encode(['newPhoneForAlertSaveResult' => $newPhoneForAlertSaveResult, 'newEmail' => $newEmail, 'newPhoneForAlert' => $newPhoneForAlert]);
	}
	
	public function actionUpdateContactRequisites() {
		$response = ['status' => '', 'text' => ''];
		$clinic = $this->loadModel();
        foreach ($clinic->legalFields as $field) {
            $clinic->{$field} = @Yii::app()->request->getParam($field) . "";
            $response['test'][$field] = $clinic->{$field};
        }
		if ($clinic->save()) {
			$response['status'] = "OK";
			$response['text'] = "Сохранено";
		} else {
			$response['status'] = "ERROR";
			$response['text'] = MyTools::errorsToString($clinic->getErrors());
		}
		echo CJSON::encode($response);
	}
	
	public function actionBill($download=false) {
		Doctor::$filterDeleted = false;
		Address::$showInactive = true;
		Company::$showRemoved = true;
		$reportMonth = Yii::app()->request->getParam('reportMonth');
		if (!$reportMonth)
			$reportMonth = date('Y-m', strtotime('-1 month'));
		$clinic = $this->loadModel();
		LogAdminClinic::Add([
			'addressId'=>$clinic->id,
			'billId'=>$clinic->getBill($reportMonth)->id,
			'typeId'=>LogAdminClinicType::model()->findByAttributes(['name'=>($download ? LogAdminClinicType::$NAME_DOWNLOAD_BILL : LogAdminClinicType::$NAME_PRINT_BILL)])->id,
			'data'=>CJSON::encode([]),
		]);
		$file = FileMaker::createHTMLBill($clinic, MyTools::getDateInterval($reportMonth), !$download);
		if(!empty($file) && !headers_sent()) {
			header('Content-Description: File Transfer');
			header('Content-Type: '.$file[2]);
			header('Content-Disposition: inline; filename="'.$file[0].'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . strlen($file[1]));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Expires: 0');
			header('Pragma: public');
			
			echo $file[1];
		}
	}
	
	public function actionMatching($download=false) {
		Doctor::$filterDeleted = false;
		Address::$showInactive = true;
		Company::$showRemoved = true;
		$reportMonth = Yii::app()->request->getParam('reportMonth');
		if (!$reportMonth)
			$reportMonth = date('Y-m', strtotime('-1 month'));
		$clinic = $this->loadModel();
		LogAdminClinic::Add([
			'addressId'=>$clinic->id,
			'billId'=>$clinic->getBill($reportMonth)->id,
			'typeId'=>LogAdminClinicType::model()->findByAttributes(['name'=>($download ? LogAdminClinicType::$NAME_DOWNLOAD_ACT : LogAdminClinicType::$NAME_PRINT_ACT)])->id,
			'data'=>CJSON::encode([]),
		]);
		$file = FileMaker::createHTMLAppointmentReport($clinic, MyTools::getDateInterval($reportMonth), !$download);
		if(!empty($file) && !headers_sent()) {
			header('Content-Description: File Transfer');
			header('Content-Type: '.$file[2]);
			header('Content-Disposition: inline; filename="'.$file[0].'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . strlen($file[1]));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Expires: 0');
			header('Pragma: public');
			
			echo $file[1];
		}
	}
}
