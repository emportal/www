<?php

class AdminClinicController extends Controller
{
	
	public $layout = '/layouts/mini_crm';
	
	public function init() {
		parent::init();

		Yii::app()->user->loginUrl = $this->createUrl("default/login");
	}

	public function filters() {
		return array(
			'accessControl',
			['application.filters.isOfferAccepted - offer'],
		);
	}

	public function accessRules() {
		return array(
			array('allow',
				'actions'	 => array('login', 'error', 'fastlogin', 'recovery'),
				'users'		 => array('?'),
			),
			array('deny',
				'actions'	 => array(),
				'users'		 => array('?'),
			),
		);
	}

	/**
	 * @return Clinic
	 */
	public function loadModel() {
		if(!empty(Yii::app()->user->model->clinic)) {
			return Yii::app()->user->model->clinic;
		}
		else {
			Yii::app()->user->logout();
			$this->redirect($this->createUrl("default/error"));
			return false;			
		}
	}
}