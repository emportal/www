<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    private $_id;

	public function __construct($username,$password,$id = null)
	{
		$this->username=$username;
		$this->password=$password;
		$this->_id = $id;
	}
	
	public function authenticate()
	{		
		
        $user = UserMedical::model()->findByAttributes( array("name"=>$this->username) );


        if ( empty($user) )
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if ( !$user->validatePassword($this->password) )
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $user->id;
			UserMedical::model()->updateByPk($user->id, [
				'lastLogin' => new CDbExpression('NOW()'),
				'recoveryKey' => null,
			]);
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;

	}

    public function getId(){
        return $this->_id;
    }
}