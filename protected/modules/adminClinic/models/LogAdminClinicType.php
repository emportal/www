<?php

/**
 * This is the model class for table "logAdminClinicType".
 *
 * The followings are the available columns in table 'logAdminClinicType':
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * The followings are the available model relations:
 * @property LogAdminClinic[] $logAdminClinic
 */
class LogAdminClinicType extends CActiveRecord
{
	STATIC $NAME_UNDEFINED						= 'undefined';
	STATIC $NAME_CHANGE_ADMIN_CLINIC_STATUS		= 'changeAdminClinicStatus';
	STATIC $NAME_CHANGE_ADMIN_CLINIC_COMMENT	= 'changeAdminClinicComment';
	STATIC $NAME_PAY_VIA_KASSA					= 'payViaKassa';
	STATIC $NAME_PRINT_ACT						= 'printAct';
	STATIC $NAME_PRINT_BILL						= 'printBill';
	STATIC $NAME_DOWNLOAD_ACT					= 'downloadAct';
	STATIC $NAME_DOWNLOAD_BILL					= 'downloadBill';
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogAdminClinicType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logAdminClinicType';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>150),
			array('description', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'logAdminClinic' => array(self::HAS_MANY, 'LogAdminClinic', 'typeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}