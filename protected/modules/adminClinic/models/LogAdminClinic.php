<?php

/**
 * This is the model class for table "logAdminClinic".
 *
 * The followings are the available columns in table 'logAdminClinic':
 * @property integer $id
 * @property string $addressId
 * @property integer $billId
 * @property integer $typeId
 * @property string $data
 * @property string $createdDate
 * @property string $userId
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property Bill $bill
 * @property LogAdminClinicType $logAdminClinicType
 * @property User $user
 */
class LogAdminClinic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogAdminClinic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logAdminClinic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('addressId, data, createdDate', 'required'),
			array('billId, typeId', 'numerical', 'integerOnly'=>true),
			array('addressId, userId', 'length', 'max'=>36),
			array('data', 'length', 'max'=>1500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, addressId, billId, typeId, data, createdDate, userId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'bill' => array(self::BELONGS_TO, 'Bill', 'billId'),
			'logAdminClinicType' => array(self::BELONGS_TO, 'LogAdminClinicType', 'typeId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'addressId' => 'Address',
			'billId' => 'Bill',
			'typeId' => 'Type',
			'data' => 'Data',
			'createdDate' => 'Created Date',
			'userId' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('billId',$this->billId);
		$criteria->compare('typeId',$this->typeId);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('userId',$this->userId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function Add($attributes) {
		$log = new self();
		$log->attributes = $attributes;
		$log->createdDate = date("Y-m-d H:i:s", time());
		$log->userId = MyTools::getUserFromCookie()->id;
		$result = $log->save();
		if(!$result) {
			ob_start();
			echo get_called_class()."<br>".PHP_EOL;
			var_dump($log->getErrors());
			MyTools::ErrorReport(ob_get_clean());
		}
		return $result;
	}
}