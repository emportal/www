<?php

/**
 * This is the model class for table "addressServices".
 *
 * The followings are the available columns in table 'addressServices':
 * @property string $id
 * @property string $addressId
 * @property string $serviceId
 * @property integer $price
 * @property integer $oldPrice
 * @property integer $free
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Service $service
 * @property Address $address
 */
class AddressServices extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AddressServices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'addressServices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('addressId, serviceId', 'required'),
			array('price,free,isActual', 'numerical', 'integerOnly'=>true),
			array('id, addressId, serviceId', 'length', 'max'=>36),
			array('description', 'length', 'max'=>300),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, addressId, serviceId, price, oldPrice, free, confirmedDate, isActual', 'safe', 'on'=>'search'),
			array('companyActivitesId, confirmedDate, isActual, description', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'service' => array(self::BELONGS_TO, 'Service', 'serviceId'),
			'companyActivite' => array(self::BELONGS_TO, 'CompanyActivite', 'companyActivitesId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'addressId' => 'Address',
			'serviceId' => 'Service',
			'price' => 'Цена',
			'oldPrice' => 'Старая цена',
			'free' => 'Бесплатно',
			'isActual' => 'IsActual',
			'description' => 'Описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('serviceId',$this->serviceId,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('oldPrice',$this->oldPrice);
		$criteria->compare('free',$this->free);
		
		$criteria->compare('confirmedDate', $this->confirmedDate);
		$criteria->compare('isActual', $this->isActual);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}