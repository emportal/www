<?php

/**
 * This is the model class for table "metroStation".
 *
 * The followings are the available columns in table 'metroStation':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $cityId
 * @property integer $longitude
 * @property integer $latitude
 *
 * The followings are the available model relations:
 * @property City $city
 * @property NearestMetroStation[] $nearestMetroStations
 */
class MetroStation extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MetroStation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'metroStation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, cityId', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, cityId, longitude, latitude', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
			'nearestMetroStations' => array(self::HAS_MANY, 'NearestMetroStation', 'metroStationId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'linkUrl' => 'ЧПУ урл',
			'cityId' => 'City',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('cityId',$this->cityId,true);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('latitude',$this->latitude);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function behaviors() {
		return array(
				'linkUrlBehavior' => array(
						'class' => 'application.components.behaviors.LinkUrlBehavior',
				)
		);
	}
	
	public static function getActualStations($cityId = '534bd8b8-e0d4-11e1-89b3-e840f2aca94f')
	{
		$actualMetroList = [];
		$actualSpecialties = ShortSpecialtyOfDoctor::model()->findAll();
		
		if(is_array($actualSpecialties)) {
			foreach($actualSpecialties as $actualSpecialty) {
				$parameters = json_decode($actualSpecialty->Parameters);
				$specialtyMetroList = (array) $parameters->Metro;
				if(is_array($specialtyMetroList)) {
					foreach($specialtyMetroList as $metro) {
						if (!in_array($metro, $actualMetroList) AND $metro) {
							$actualMetroList[] = substr($metro, 6);
						}
					}
				}
			}
		}
		
		$criteria = new CDbCriteria();
		$cityCriteria = new CDbCriteria();
		$cityCriteria->compare('cityId', $cityId);
		foreach($actualMetroList as $actualMetro) {
			$criteria->compare('linkUrl', $actualMetro, true, 'OR');
		}
		$criteria->mergeWith($cityCriteria);
		$metroStations = self::model()->findAll($criteria);
		return $metroStations;
	}
}