<?php

/**
 * This is the model class for table "producerNomenclatureGroup".
 *
 * The followings are the available columns in table 'producerNomenclatureGroup':
 * @property string $id
 * @property string $producerId
 * @property string $nomenclatureId
 * @property string $nomenclatureGroupId
 *
 * The followings are the available model relations:
 * @property NomenclatureGroup $nomenclatureGroup
 * @property Nomenclature $nomenclature
 * @property Producer $producer
 */
class ProducerNomenclatureGroup extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProducerNomenclatureGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producerNomenclatureGroup';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, producerId, nomenclatureId, nomenclatureGroupId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, producerId, nomenclatureId, nomenclatureGroupId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nomenclatureGroup' => array(self::BELONGS_TO, 'NomenclatureGroup', 'nomenclatureGroupId'),
			'nomenclature' => array(self::BELONGS_TO, 'Nomenclature', 'nomenclatureId'),
			'producer' => array(self::BELONGS_TO, 'Producer', 'producerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'producerId' => 'Producer',
			'nomenclatureId' => 'Nomenclature',
			'nomenclatureGroupId' => 'Nomenclature Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('producerId',$this->producerId,true);
		$criteria->compare('nomenclatureId',$this->nomenclatureId,true);
		$criteria->compare('nomenclatureGroupId',$this->nomenclatureGroupId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}