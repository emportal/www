<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;
	public $_authIdentity;
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Запомнить',
			'username'=>'Телефон или e-mail',
			'password'=>'Пароль',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity = new UserIdentity($this->username,$this->password);
			
			if(!$this->_identity->authenticate())
				$this->addError('password','Некорректное имя пользователя или пароль.');
		}
	}
	
	public function authenticateSocial($type)
	{
		if ($this->_identity === null)
		{
			$this->_identity = new SocialUserIdentity($this->username, $this->password);
			switch($type) {
				case SocialUserIdentity::TYPE_VK: 
					if ($this->_identity->authenticateVK())
					{
						
					}
					break;
			
				case SocialUserIdentity::TYPE_FB:
					$this->_identity->authenticateFB($this->_authIdentity);
					break;
			
				case SocialUserIdentity::TYPE_OK: 
					$this->_identity->authenticateOK($this->_authIdentity);
					break;
				
				case SocialUserIdentity::TYPE_TWT: 
					
					break;
			}
		}


		if ($this->_identity->errorCode === SocialUserIdentity::ERROR_NONE)
		{
			#echo "test2";
			$duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
			if (Yii::app()->user->login($this->_identity, $duration))
			{
				#$this->getErrors();
				#$this->redirect(Yii::app()->user->returnUrl);
				#echo 3;
			}
			return true;
		}
		else
			return false;
	}
	

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login($md5 = false)
	{
		if($this->_identity === null)
		{
			$this->_identity = new UserIdentity($this->username, $this->password);
			$this->_identity->authenticate($md5);
		}
		if($this->_identity->errorCode === UserIdentity::ERROR_NONE)
		{
			//$duration = $this->rememberMe ? 3600*24*30 : 0; // 30 days
			$duration = 3600*24*30; //always remember for 30 days
			Yii::app()->user->login($this->_identity, $duration);
			return true;
		}
		else {			
			return false;
		}
	}

	public function getErrorCode() {
		return $this->_identity !== null ? $this->_identity->errorCode : NULL;
	}
	
	public function convertPhoneToStandardForm()
	{
		//если ввели в качестве логина телефон разными способами (через 8..., +7... и пр.),
		//подгоняем его к стандартному виду
		if (strlen($phoneString))
			$this->username = self::convertPhoneStringToStandardForm($phoneString);
		return true;
	}
	
	public static function convertPhoneStringToStandardForm($phoneString)
	{
		$phoneString = preg_replace('/[^0-9]/', '', $phoneString);
		$phoneStringLength = strlen($phoneString);
		if ($phoneStringLength >= 10 AND $phoneStringLength <=11){
			$offset = $phoneStringLength - 10;
			$phoneString = '+7' . substr($phoneString, $offset, 10);
		}		
		return $phoneString;
	}
}
