<?php

/**
 * This is the model class for table "cllassifierCountries".
 *
 * The followings are the available columns in table 'cllassifierCountries':
 * @property string $idClassifierCountries
 * @property string $name
 * @property string $link
 * @property string $ClassifierCountriesFullName
 * @property string $ClassifierCountriesCountryCode
 */
class CllassifierCountries extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CllassifierCountries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cllassifierCountries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idClassifierCountries', 'length', 'max'=>36),
			array('name, link, ClassifierCountriesFullName, ClassifierCountriesCountryCode', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idClassifierCountries, name, link, ClassifierCountriesFullName, ClassifierCountriesCountryCode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idClassifierCountries' => 'Id Classifier Countries',
			'name' => 'Name',
			'link' => 'Link',
			'ClassifierCountriesFullName' => 'Classifier Countries Full Name',
			'ClassifierCountriesCountryCode' => 'Classifier Countries Country Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idClassifierCountries',$this->idClassifierCountries,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('ClassifierCountriesFullName',$this->ClassifierCountriesFullName,true);
		$criteria->compare('ClassifierCountriesCountryCode',$this->ClassifierCountriesCountryCode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}