<?php

/**
 * This is the model class for table "priceNomenclature".
 *
 * The followings are the available columns in table 'priceNomenclature':
 * @property string $companyId
 * @property string $serviceSectionId
 * @property string $serviceId
 *
 * The followings are the available model relations:
 * @property Service $service
 * @property Company $company
 * @property ServiceSection $serviceSection
 */
class PriceNomenclature extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PriceNomenclature the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'priceNomenclature';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('companyId, serviceSectionId, serviceId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('companyId, serviceSectionId, serviceId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'service' => array(self::BELONGS_TO, 'Service', 'serviceId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'serviceSection' => array(self::BELONGS_TO, 'ServiceSection', 'serviceSectionId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'companyId' => 'Company',
			'serviceSectionId' => 'Service Section',
			'serviceId' => 'Service',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('serviceSectionId',$this->serviceSectionId,true);
		$criteria->compare('serviceId',$this->serviceId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}