<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FeedbackForm extends CFormModel
{
	#public $name, $address, $city;
	public $email;
#	public $phone;
	public $body;
	public $subject;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('body,email, subject', 'required'), /*name,, city, address, phone*/
			// email has to be a valid email address
			array('email', 'email'),
			array('body', 'safe'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'subject'=>'Тема',
			'city'=>'Город',
			'address'=>'Адрес',
			'phone'=>'Телефон',
			'name'=>'Ваше имя',
			'body'=>'Комментарий',
			'verifyCode'=>'Код с картинки',
		);
	}
}