<?php

/**
 * This is the model class for table "email".
 *
 * The followings are the available columns in table 'email':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $ownerId
 * @property string $typeId
 * @property integer $general
 * @property string $addressId
 * @property integer $forCompany
 * @property integer $forProducers
 * @property integer $forDoctors
 *
 * The followings are the available model relations:
 * @property EmailType $type
 * @property EmailCompanyUnit[] $emailCompanyUnits
 */
class Email extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Email the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('general, forCompany, forProducers, forDoctors', 'numerical', 'integerOnly'=>true),
			array('id, typeId, addressId', 'length', 'max'=>36),
			array('name, link, ownerId', 'length', 'max'=>150),
			array('name', 'email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, ownerId, typeId, general, addressId, forCompany, forProducers, forDoctors', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'type' => array(self::BELONGS_TO, 'EmailType', 'typeId'),
			'emailCompanyUnits' => array(self::HAS_MANY, 'EmailCompanyUnit', 'emailId'),
			'company' => array(self::BELONGS_TO, 'Company', 'ownerId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Email',
			'link' => 'Link',
			'ownerId' => 'Owner',
			'typeId' => 'Type',
			'general' => 'General',
			'addressId' => 'Address',
			'forCompany' => 'For Company',
			'forProducers' => 'For Producers',
			'forDoctors' => 'For Doctors',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('ownerId',$this->ownerId,true);
		$criteria->compare('typeId',$this->typeId,true);
		$criteria->compare('general',$this->general);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('forCompany',$this->forCompany);
		$criteria->compare('forProducers',$this->forProducers);
		$criteria->compare('forDoctors',$this->forDoctors);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}