<?php

/**
 * This is the model class for table "bill".
 *
 * The followings are the available columns in table 'bill':
 * @property integer $id
 * @property string $addressId
 * @property string $createdDate
 * @property integer $paymentStatusId
 * @property string $managerComment
 * @property string $type
 * @property integer $isPrepaid
 */
class Bill extends CActiveRecord
{
	const MANAGER_STATUS_NOT_PAID = 0;
	const MANAGER_STATUS_PAID = 1;
	public static $MANAGER_STATUS_ID_TYPES =
	[
		Bill::MANAGER_STATUS_NOT_PAID	=>	'Не оплачен',
		Bill::MANAGER_STATUS_PAID		=>	'Оплачен',
	];

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bill';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('addressId, reportMonth', 'required'),
			array('paymentStatusId', 'numerical', 'integerOnly'=>true),
			array('addressId, type', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, addressId, createdDate, paymentStatusId, managerComment', 'safe', 'on'=>'search'),
			//['paymentStatusId', 'safe', 'on'=>'changeManagerStatus'],
			['appointmentTotal, appointmentDeclinedTotal, appointmentDeclinedByClinic, sum, isFinal, lastUpdate, salesContractTypeId', 'safe', 'on'=>'updateBill'],
			['sumPayedOnline', 'safe', 'on'=>'onlinePayment'],
            ['isFinal', 'safe', 'on'=>'finaliseBills'],
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
            'salesContractType' => array(self::BELONGS_TO, 'SalesContractType', 'salesContractTypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'addressId' => 'Address',
			'createdDate' => 'Created Date',
			'paymentStatusId' => 'Payment Status',
			'managerComment' => 'Manager Comment',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('paymentStatusId',$this->paymentStatusId);
		$criteria->compare('managerComment',$this->managerComment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Обновить основные изменяемые параметры счета
	 */
	public function updateFigures($saveSalesContractTypeId = true)
	{
		if($this->isFinal || $this->paymentStatusId || $this->isPrepaid) {
			return false;
		}
		
		$reportMonth = $this->reportMonth;
		$address = $this->address;
		
		$appModel = AppointmentToDoctors::model();
		$appModel->visit_time_period = MyTools::getDateInterval($reportMonth);
		$newAttributes = [];
		
		//appointmentTotal
		$appInfo = $address->getInfoForAppointmentReports($appModel, null, ['manStatus'=>'']);
		$newAttributes['appointmentTotal'] = $appInfo['dataProvider']->totalItemCount;
		
		//appointmentDeclinedTotal
		$appInfo = $address->getInfoForAppointmentReports($appModel, null, ['manStatus'=>'d']);
		$newAttributes['appointmentDeclinedByClinic'] = 0;
		foreach($appInfo['dataProvider']->getData() as $appointment)
		{
			if (in_array($appointment->adminClinicStatusId, [AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willNotPay']]))
				$newAttributes['appointmentDeclinedByClinic']++;
		}
		$newAttributes['appointmentDeclinedTotal'] = $appInfo['dataProvider']->totalItemCount;
        
        $createdInOrAfterReportMonth = (strtotime(date('Y-m', strtotime($address->userMedicals->createDate))) > strtotime($reportMonth));
        if (($address->salesContract->salesContractType->isFixed == 1) && $createdInOrAfterReportMonth) {
            $newAttributes['sum'] = 0;
            /* var_dump([
                '$address->userMedicals->createDate' => $address->userMedicals->createDate,
                '$address->userMedicals->createDate Y-m' => date('Y-m', strtotime($address->userMedicals->createDate)),
                //'strtotime' => strtotime($address->userMedicals->createDate),
                'strtotime 1st day reportMonth' => strtotime(date('Y-m', strtotime($address->userMedicals->createDate))),
                'reportMonth Y-m' => date('Y-m', strtotime($reportMonth)),
                'strtotime reportMonth' => strtotime($reportMonth),
                'createdInOrAfterReportMonth' => $createdInOrAfterReportMonth,
                '$address->salesContract->salesContractType' => $address->salesContract->salesContractType,
                '$address->salesContract->salesContractType->isFixed' => $address->salesContract->salesContractType->isFixed,
            ]); */
        } else {
            $appInfo = $address->getInfoForAppointmentReports($appModel);
            $newAttributes['sum'] = ($appInfo['totalSale']) ? $appInfo['totalSale'] : 0;
        }
		
		//make final
		//$newAttributes['isFinal'] = 1;
		
		$newAttributes['lastUpdate'] = date('Y-m-d H:i:s', time());
        if ($saveSalesContractTypeId)
            $newAttributes['salesContractTypeId'] = $address->salesContract->salesContractType->id;
		
		$this->scenario = 'updateBill';
		$this->attributes = array_merge($this->attributes, $newAttributes);
		try {
			$this->update();
            return true;
		} catch (Exception $e) {
            return false;
        }
	}
	
	public function getIsValid()
	{
		return (strtotime($this->reportMonth) >= strtotime('2015-07')) && (strtotime($this->reportMonth) < strtotime(date('Y-m'))); //до июля 2015 система счетов не существовала
	}
    
    /**
     * Узнать, является ли счет не оплаченным и висящим в качестве "долга"
     *
     * @return boolean
     */
    public function getIsDebt()
    {
        if (!$this->getIsValid() OR $this->isPrepaid) //до июля 2015 система счетов не существовала
            return false;
        $timingCondition = ($this->salesContractType->isFixed) ? (date('j', strtotime($this->address->userMedicals->createDate)) <= date('j')) : (strtotime($this->reportMonth) < strtotime(date('Y-m')));
        //var_dump($timingCondition, $this->reportMonth, strtotime($this->reportMonth), strtotime(date('Y-m')));
        return (!$this->paymentStatusId && floatval($this->sum) > 0 && !$this->sumPayedOnline && $timingCondition) ? true : false;
    }
}