<?php

/**
 * This is the model class for table "raitingDoctorUsers".
 *
 * The followings are the available columns in table 'raitingDoctorUsers':
 * @property string $id
 * @property string $usersId
 * @property string $doctorsId
 * @property integer $isRecomend
 * @property string $review
 * @property string $appointmentToDoctorsId
 *
 * The followings are the available model relations:
 * @property Doctor $doctor
 * @property User $user
 * @property RaitingDoctorValue $raitingDoctorValues
 */
class RaitingDoctorUsers extends ActiveRecord
{
	private $_grades;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RaitingDoctorUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'raitingDoctorUsers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isRecomend', 'numerical', 'integerOnly'=>true),
			array('grades','CheckGrades','on'=>'add'),
			array('id, usersId, doctorsId, appointmentToDoctorsId', 'length', 'max'=>36),
			array('status','default','value'=>0),
			#array('review,answer', 'safe'),
			array('name','length','max'=>50),
			array('date','safe'),
			array('review,answer','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, usersId, doctorsId, isRecomend, review, appointmentToDoctorsId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorsId'),
			'user' => array(self::BELONGS_TO, 'User', 'usersId'),
			'raitingDoctorValues' => array(self::HAS_MANY,'RaitingDoctorValue','raitingDoctorUsersId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'usersId' => 'Users',
			'doctorsId' => 'Doctors',
			'isRecomend' => 'Is Recomend',
			'review' => 'Review',
			'appointmentToDoctorsId' => 'Appointment To Doctors',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('usersId',$this->usersId,true);
		$criteria->compare('doctorsId',$this->doctorsId,true);
		$criteria->compare('isRecomend',$this->isRecomend);
		$criteria->compare('review',$this->review,true);
		$criteria->compare('appointmentToDoctorsId',$this->appointmentToDoctorsId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function scopes() {
		return array(
			'moderated' => array(
				'condition' => 'status = 1'
			),
		);
	}
	public function getGrades() {		
		return $this->_grades;
	}
	
	public function setGrades($value) {		
		$this->_grades = $value;
	}
	
	public function CheckGrades($data) {
		foreach($this->grades as $key=>$row) {
			if($row == "") {
				$this->addError('grades','Необходимо заполнить все критерии');
			}
		}
	}
}