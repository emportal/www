<?php

/**
 * This is the model class for table "EquipmentOfCompany".
 *
 * The followings are the available columns in table 'EquipmentOfCompany':
 * @property string $idEquipmentOfCompany
 * @property string $Owner_CompaniesId
 * @property string $EquipmentId
 *
 * The followings are the available model relations:
 * @property Equipment $equipment
 * @property Companies $ownerCompanies
 */
class EquipmentOfCompany extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EquipmentOfCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EquipmentOfCompany';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEquipmentOfCompany', 'length', 'max'=>150),
			array('Owner_CompaniesId, EquipmentId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idEquipmentOfCompany, Owner_CompaniesId, EquipmentId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'equipment' => array(self::BELONGS_TO, 'Equipment', 'EquipmentId'),
			'ownerCompanies' => array(self::BELONGS_TO, 'Companies', 'Owner_CompaniesId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEquipmentOfCompany' => 'Id Equipment Of Company',
			'Owner_CompaniesId' => 'Owner Companies',
			'EquipmentId' => 'Equipment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEquipmentOfCompany',$this->idEquipmentOfCompany,true);
		$criteria->compare('Owner_CompaniesId',$this->Owner_CompaniesId,true);
		$criteria->compare('EquipmentId',$this->EquipmentId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}