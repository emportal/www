<?php

/**
 * This is the model class for table "producerOffice".
 *
 * The followings are the available columns in table 'producerOffice':
 * @property string $id
 * @property string $producerId
 * @property string $countryId
 * @property string $cityId
 * @property string $addressId
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property City $city
 * @property Country $country
 * @property Producer $producer
 */
class ProducerOffice extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProducerOffice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producerOffice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, producerId, countryId, cityId, addressId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, producerId, countryId, cityId, addressId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
			'country' => array(self::BELONGS_TO, 'Country', 'countryId'),
			'producer' => array(self::BELONGS_TO, 'Producer', 'producerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'producerId' => 'Producer',
			'countryId' => 'Country',
			'cityId' => 'City',
			'addressId' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('producerId',$this->producerId,true);
		$criteria->compare('countryId',$this->countryId,true);
		$criteria->compare('cityId',$this->cityId,true);
		$criteria->compare('addressId',$this->addressId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}