<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $surName
 * @property string $firstName
 * @property string $fatherName
 * @property string $password
 * @property string $email
 * @property string $telefon
 * @property string $birthday
 * @property string $cityId
 * @property string $OMSSeries
 * @property string $OMSNumber
 * @property integer $isBlockedAppointment
 * @property integer $BlockedTime
 * @property integer $subscriber 
 * @property integer $status
 * @property datetime $dateRegister
 * @property integer $registerPromoCodeUsed
 * @property string $refererId
 * @property-read array $favoritesArray
 * @property-read string $shortname
 * @property int    $phoneActivationStatus
 * @property datetime    $phoneActivationDate
 * The followings are the available model relations:
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property appointmentToDoctors  appointmentToDoctorsCount
 * @property Comment[] $comments
 * @property Favorite[] $favorites
 * @property City $city
 * @property Sex $sexId
 * @property UserToken $userTokens
 * @property Right[] $rights
 * @property User $referer
 */
class User extends ActiveRecord {

	public $_oldhash;
	public $_telefon;
	private $_transaction;
	public $passwordNew;
	public $passwordConfirm;
	public $passwordOld;
	private $_favoritesArray;
	public $confirmation;
	public $md5;
	public $managerListType;
	public $_subscriber;
	
	public $refererIdTemp;
	public $promoCode;

	private $_oldEmail;

	const APPOINT_ALLOW = 0;
	const APPOINT_DISALLOW = 1;
	const APPOINT_BLOCKED = 2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, password, email, confirmation', 'required', 'on' => 'register'),
			array('confirmation', 'numerical', 'integerOnly' => true, 'tooSmall' => 'Вы не подтвердили согласие на обработку данных', 'min' => 1, 'on' => 'register'),			
			array('email', 'unique', 'message' => 'Данный адрес электронной почты уже зарегистрирован в системе', 'on' => 'register,emailchange'),
			array('telefon', 'unique', 'message' => 'Данный мобильный телефон уже зарегистрирован в системе', 'on' => 'register'),
			array('status, phoneActivationStatus, balance', 'numerical', 'integerOnly' => true),
			array('id, cityId', 'length', 'max' => 36),
			//array('telefon','numerical','integerOnly'=>true),
			array('password', 'CheckPasswordFromSite', 'on' => 'register'),
			array('passwordConfirm', 'safe', 'on' => 'register'),
			array('name', 'required', 'on' => 'update,updateProfile'),
			array('telefon', 'required', 'on' => 'update,reactivateByPhone'),
			array('telefon','unsafe','on'=>'updateProfile'),
			array('name', 'required', 'on' => 'addAuthor'),
			//array('sexId', 'RequiredNotFromApp', 'on' => 'register'),
			array('sexId', 'default', 'value' => 'ae11c45d855a991340c5f59edcb404da'),
			array(
				'telefon',
				'match',
				'pattern' => '/^((\+?7) ?\(?(-?\d{3})-?)?\)? ?(\d{3})(-?\d{2})(-?\d{2})$/',
				'message' => 'Некорректный формат поля {attribute}',
				'except' => 'register'
			),
			array('email', 'email', 'on' => 'register,emailchange'),
			array('email', 'email', 'on' => 'reactivate'),
			array('email', 'required', 'on' => 'reactivate'),
			array('name, link, surName, firstName, fatherName, password, passwordNew, email, telefon, comment', 'length', 'max' => 150),
			array('passwordConfirm', 'compare', 'compareAttribute' => 'passwordNew', 'on' => 'updateProfile'),
			array('passwordOld', 'validatePasswordProfile', 'message' => 'Следует указать для продолжения', 'on' => 'updateProfile','except' => 'activatePhone'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, surName, firstName, fatherName, password email, telefon, birthday, cityId, status, comment, OMSSeries, OMSNumber', 'safe', 'on' => 'search'),
			array('birthday, phoneActivationDate, promoCode, subscriber, OMSSeries, OMSNumber, info', 'safe'),
			array('balance', 'unsafe'),
			array('promoCode', 'checkPromoCode', 'on' => 'register,submitPromocode'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'userId'),
			'appointmentToDoctorsCount' => array(self::STAT, 'AppointmentToDoctors', 'userId'),
			'comments' => array(self::HAS_MANY, 'Comment', 'usersId'),
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
			'favorites' => array(self::HAS_MANY, 'Favorite', 'usersId'),
			'sex' => array(self::BELONGS_TO, 'Sex', 'sexId'),
			'userTokens' => array(self::HAS_MANY,'UserToken', 'userId'),
			'authorNews' => array(self::HAS_MANY,'News', 'authorId'),
			'phoneVerification' => array(self::HAS_ONE, 'PhoneVerification', 'userId', 'condition' => 'phoneVerification.status=' . PhoneVerification::CHANGEPHONE),
			//возвращает количество записей ( 0 или 1 ) для подтверждения смены телефона
			'phoneVerificationCount' => array(self::STAT, 'PhoneVerification', 'userId', 'condition' => 't.status=' . PhoneVerification::CHANGEPHONE),
            'rights' => array(self::HAS_MANY, 'Right', 'userId'),
			'managerRelations' => array(self::HAS_MANY, 'ManagerRelation', 'userId'),
			'purchasedServices' => array(self::HAS_MANY, 'PurchasedService', 'userId'),
			'referer' => array(self::BELONGS_TO, 'User', 'refererId'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'userId' => 'User',
			'email' => 'E-mail',
			'password' => 'Пароль',
			'salt' => 'Salt',
			'name' => 'Имя',
			'link' => 'Link',
			'UsersId' => 'Users',
			'surName' => 'Фамилия',
			'firstName' => 'Имя',
			'fatherName' => 'Отчество',
			'birthday' => 'Дата рождения',
			'sexId'		=> 'Пол',
			'telefon' => 'Мобильный телефон',
			'CitiesId' => 'Cities',
			'subscriber' => 'Subscriber',
			'passwordNew' => 'Новый пароль',
			'key' => 'Код активации',
			'passwordConfirm' => 'Подтверждение пароля',
			'passwordOld' => 'Текущий пароль',
			'confirmation' => 'Cогласен на обработку личных данных',
			'promoCode' => 'Промо-код',
			'info' => 'Описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('t.id', $this->id, true);
		$criteria->compare('t.email', $this->email, true);
		$criteria->compare('t.password', $this->password, true);
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.link', $this->link, true);
		$criteria->compare('t.surName', $this->surName, true);
		$criteria->compare('t.firstName', $this->firstName, true);
		$criteria->compare('t.fatherName', $this->fatherName, true);
		$criteria->compare('t.birthday', $this->birthday, true);
		$criteria->compare('t.telefon', $this->telefon, true);
		$criteria->compare('t.cityId', $this->cityId, true);
		$criteria->compare('t.balance', $this->balance, true);
		$criteria->compare('t.registerPromoCodeUsed', $this->registerPromoCodeUsed, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function afterFind() {
		parent::afterFind();
		
		$this->_telefon = $this->telefon;
		$this->_oldhash = $this->password;

		return true;
	}

	public function beforeSave() {

		parent::beforeSave();

		if ($this->isNewRecord) {
			$this->dateRegister = new CDbExpression("NOW()");
		}

		/* generates password hash before save */
		if ($this->passwordNew)
			$this->password = $this->passwordNew;

		if (!empty($this->password) && $this->password != $this->_oldhash) {
			$this->password = md5($this->password);
		} else {
			$this->password = $this->_oldhash;
		}

		/*if (!empty($this->surName) && !empty($this->firstName)) {
			$this->name = $this->surName . ' ' . $this->firstName . ' ' . $this->fatherName;
		}*/

//		if ( empty($this->id) )
//			$this->id = new CDbExpression('UUID()');;//$this->create_guid();

		$user = User::model()->findByAttributes(["id" => $this->id]);
		$this->_oldEmail = $user->email;

		return true;
	}

	public function afterSave() {
		parent::afterSave();

		$user = User::model()->findByAttributes(["id" => $this->id]);

		if($this->checkPromoCode('afterSave')) {
			if(!empty($user)) {
				if($this->refererIdTemp) {
					$user->refererId = $this->refererIdTemp;
				}// else {
                    //начисляем 500 руб. всегда, даже если пришел по реферальной программе
                    Yii::import('application.controllers.UserController');
                    $result = UserController::incrementBalance($user, 500);
                //}
				$user->registerPromoCodeUsed = 1;
				$user->update();
                
                $log = new LogVarious();
                $log->attributes = [
                    'type' => $log::TYPE_PROMOCODE,
                    'value' => CJSON::encode([
                        'userId' => $user->id,
                        'promocode' => $this->promoCode,
                    ])
                ];
                $log->save();
			}
		}
		
		if($this->email !== null AND ($this->subscriber || (!$this->_oldEmail && $this->scenario != 'register'))) {
			if(empty(EmailSubscription::model()->findByAttributes(['email'=>$this->email,'type'=> $subscriber]))) {
				$emailSubscription = new EmailSubscription();
				$emailSubscription->email = $user->email;
				$emailSubscription->type = 1;
				$emailSubscription->save();
			}
		}

		//подписываем на все уведомления при регистрации или когда заполняют email
		if($this->email !== null && ($this->scenario == 'register' || !$this->_oldEmail)) {
			$subscriberList = [3,5];
			foreach ($subscriberList as $subscriber) {
				if(empty(EmailSubscription::model()->findByAttributes(['email'=>$this->email,'type'=> $subscriber]))) {
					$emailSubscription = new EmailSubscription();
					$emailSubscription->email = $this->email;
					$emailSubscription->type = $subscriber;
					$emailSubscription->save();
				}
			}
		}

	}

	public function delete() {
		foreach($this->userTokens as $userToken) {
			$userToken->token->delete();
			$userToken->delete();
		}
		
		
		$this->saveAttributes(array(
			'status' => -1,
			'email' => new CDbExpression('MD5(:email)', array(':email' => $this->email . time())),
			'telefon' => new CDbExpression('MD5(:phone)', array(':phone' => $this->telefon . time())),
			'comment' => $this->email . "," . $this->telefon,
		));
	}

	public function generate_salt($number) {
		$arr = array('a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'r', 's',
			't', 'u', 'v', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'R', 'S',
			'T', 'U', 'V', 'X', 'Y', 'Z',
			'1', '2', '3', '4', '5', '6',
			'7', '8', '9', '0', '.', ',',
			'(', ')', '[', ']', '!', '?',
			'&', '^', '%', '@', '*', '$',
			'<', '>', '/', '|', '+', '-',
			'{', '}', '`', '~');

		// Генерируем пароль
		$pass = "";
		for ($i = 0; $i < $number; $i++) {
			// Вычисляем случайный индекс массива
			$index = rand(0, count($arr) - 1);
			$pass .= $arr[$index];
		}
		return $pass;
	}

	static public function generate_password($number = 10, $strtolower = false) {
		$arr = array('a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'r', 's',
			't', 'u', 'v', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'R', 'S',
			'T', 'U', 'V', 'X', 'Y', 'Z',
			'1', '2', '3', '4', '5', '6',
			'7', '8', '9', '0',
		);

		// Генерируем пароль
		$pass = "";
		for ($i = 0; $i < $number; $i++) {
			// Вычисляем случайный индекс массива
			$index = rand(0, count($arr) - 1);
			$pass .= $arr[$index];
		}

		return $strtolower ? mb_strtolower($pass) : $pass;
	}
	/**
	 * Если после регистрации у пользователя в графе телефон - NULL, то не выдавать ошибку при изменении данных в профиле
	 */
	public function requiredIfNotNull($attribute) {
		if($this->{'_'.$attribute} !== NULL) {
			$this->addError($attribute,'Необходимо заполнить поле '.$this->getAttributeLabel($attribute));
		}
	}
	
	
	/**
	 * 
	 * @param type $password поле passwordOld
	 * @param type $params  параметры правила
	 */
	public function validatePasswordProfile($password,$params = null) {
		if($this->hashPassword($this->$password) !== $this->password 
				&& $this->passwordNew !== "" 
				&& $this->password !== NULL) {			
			$this->addError($password,$params['message']);
		}
	}
	
	public function validatePassword($password) {		
		return $this->md5 ? ($password === $this->password) : ($this->hashPassword($password) === $this->password);
	}
	
	public function hashPassword($password) {
		return md5($password);
	}

	/* protected function create_guid() {   //Генераци GUID
	  return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
	  // 32 bits for "time_low"
	  mt_rand(0, 0xffff), mt_rand(0, 0xffff),
	  // 16 bits for "time_mid"
	  mt_rand(0, 0xffff),
	  // 16 bits for "time_hi_and_version",
	  // four most significant bits holds version number 4
	  mt_rand(0, 0x0fff) | 0x4000,
	  // 16 bits, 8 bits for "clk_seq_hi_res",
	  // 8 bits for "clk_seq_low",
	  // two most significant bits holds zero and one for variant DCE1.1
	  mt_rand(0, 0x3fff) | 0x8000,
	  // 48 bits for "node"
	  mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	  );
	  } */

	public function getFavoritesArray() {
		if (!$this->_favoritesArray)
			$this->_favoritesArray = array_map(create_function('$e', 'return $e->ownerId;'), $this->favorites);
		return $this->_favoritesArray;
	}

	public function getShortName() {
		return sprintf('%s %s.%s.', $this->surName, mb_substr($this->firstName, 0, 1), mb_substr($this->fatherName, 0, 1));
	}

	public function RequiredNotFromApp() {
		$sexId = $this->sexId;
		$JSON = Yii::app()->request->getParam('JSON');
		if (!$sexId && !$JSON) {
			$this->addError('sexId', 'Необходимо выбрать пол');
		}
	}

	public function CheckPasswordFromSite() {
		$JSON = Yii::app()->request->getParam('JSON');
		$password = $this->password;

		if (!$JSON) {
			if ($password) {
				if ($password != $password) { //$password != $this->passwordConfirm
					$this->addError('password', 'Подтвердите пароль ниже');
				} else {
					if (preg_match("/^[a-zA-Z]+$/", $password) || preg_match("/^[0-9]+$/", $password) || strlen($password) < 6) {
						$this->addError('password', 'Пароль должен содержать не менее 6 символов, содержать буквы и цифры');
					}
				}
			} else {
				$this->addError('password', 'Необходимо заполнить пароль');
			}
		}
	}
	
	public function hasRight($rightName, $value=null) {
		
		$rightId = constant('RightType::'.$rightName);
		
		foreach($this->rights as $right) {			
			if ($right->zRightId == $rightId && $right->value == $value) {
				return true;
			}
		}
		return false;
	}
	
	public function getManagerList() {
		
		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = [
			'rights' => [
				'together' => true,
			],
		];
		$criteria->addCondition('rights.userId != 1');
		$criteria->compare('rights.zRightId ', RightType::ACCESS_NEW_ADMIN, false);
		
		if (!Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') AND $this->managerListType != 'full')
		{
			//addCondition('managerRelations.userId = 1');
			$criteria->compare('rights.userId ', Yii::app()->user->id, false);
		}
		
		$managers = $this->findAll($criteria); //::model()
		$managerListData = [];		
		foreach ($managers as $manager)
		{
			$managerListData[$manager->id] = $manager->name;
		}
		
		return $managerListData;
	}
	
	public static function userWithThisPhoneExists($phone = null) {
		
		if (!$phone) return false;
		if (strlen($phone) == 11 AND substr($phone, 0, 1) == '8')
			$phone = preg_replace('/8/', '+7', $phone, 1);
		$user = self::model()->findByAttributes(['telefon' => $phone]);
		$result = ($user) ? true : false;
		return $result;		
	}
	
	public function hasEmailNotifications($notificationType = 3) {
		
		if (!$this->email) return false;		
		$searchAttributes = [
			'type' => $notificationType,
			'email' => $this->email
		];		
		$patientInfoEmailSub = EmailSubscription::model()->findByAttributes($searchAttributes);
		//$hasEmailNotifications = !!count($patientInfoEmailSub);
		return !!count($patientInfoEmailSub) ? true : false;
	}
	
	public function getLastAppointment() {
		
		$criteria = new CDbCriteria();
		//$criteria->with = [
		//	'user' => [
		//		//'together' => true,
		//	],
		//];
		$criteria->compare('t.userId', $this->id, false);
		$criteria->order = 'createdDate DESC';
		$lastAppointment = AppointmentToDoctors::model()->findAll($criteria)[0];
		return $lastAppointment;
	}
	
	public function getAvatarSrc()
	{
		$avatarSrc = '';
		switch($this->sexId)
		{
			case 'b4a45cdfcb1a97ee4ba9baf3c3013f69':
				$avatarSrc = '/images/newLayouts/icon_user_f.jpg';
				break;
			case '81160944c16a392048918e487f6c328a':
				$avatarSrc = '/images/newLayouts/icon_user_m.jpg';
				break;
			default: //ae11c45d855a991340c5f59edcb404da
				$avatarSrc = '/images/newLayouts/icon_user_default.jpg';
				break;
		}
		return $avatarSrc;
	}

	public function checkPromoCode($attribute) {
		$success = false;
		if($this->promoCode !== null && trim($this->promoCode) !== "")
		{
			if($this->registerPromoCodeUsed == 1) {
				$this->addError($attribute, "Вы уже использовали промо-код");
			}
			elseif($referer = User::model()->find(['condition'=>"CONVERT(link,UNSIGNED INTEGER) = ".Yii::app()->db->quoteValue($this->promoCode)])) {
				if($referer->id === $this->id) {
					$this->addError($attribute, "Нельзя активировать собственный промокод");
				} else {
					// валидация пройдена
					if($attribute === "afterSave") {
						$this->refererIdTemp = $referer->id;
					}
					$success = true;
				}
			}
			elseif(!in_array($this->promoCode, PromoCode::getAvailablePromoCodes())) {
				$this->addError($attribute, "Промо-код неправильный");
			}
			elseif(empty($this->email)) {
				$this->addError($attribute, "Промо-коды работают только для пользователей с заполненной электронной почтой");
			} else {
				// валидация пройдена
				$success = true;
			}
		} else {
			if($this->scenario === 'submitPromocode') {
				$this->addError($attribute, "Промо-код должен быть введён");
			}
		}
		return $success;
	}
	
	public function getMyPromocode() {
		return intval($this->link);
	}
	
	public function getSubscriber() {
		if($this->_subscriber === null AND $this->email !== null) {
			$emailSubscription = EmailSubscription::model()->findByAttributes(['email'=>$this->email,'type'=>1]);
			$this->_subscriber = !empty($emailSubscription);
		}
		return $this->_subscriber;
	}
	
	public function setSubscriber($value) {
		$this->_subscriber = $value;
	}
	
	public function giveAnAward() {
		//Начислить 500 бонусов
		Yii::import('application.controllers.UserController');
		$result = UserController::incrementBalance($this, 500);
		//Выслать уведомления по смс
		if($this->telefon!==null AND trim($this->telefon)!=="") {
			$sms = new SmsGate();
			$sms->phone = $this->telefon;
			$sms->setMessage('Вам начислено 500 бонусов на сайте emportal.ru');
			$sms->send();
		}
		//Выслать уведомления по емаил
		if($this->email!==null AND trim($this->email)!=="") {
			$mail = new YiiMailer();
			$mail->setView('giveAnAward');
			$mail->setData(array( 'model' => $this, ));
			$mail->render();
			$mail->From = 'robot@emportal.ru';
			$mail->FromName = Yii::app()->name;
			$mail->Subject = "Вам начислено 500 бонусов";;
			$mail->AddAddress($this->email);
			$mail->Send();
		}
	}
	
	public function getName() {
		return implode(" ", [$this->surName, $this->firstName, $this->fatherName]);
	}
	public function setName($FIO) {
		$data = MyTools::FIOExplode($FIO);
		$this->surName = @$data["surName"];
		$this->firstName = @$data["firstName"];
		$this->fatherName = @$data["fatherName"];
	}
}
