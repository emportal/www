<?php

/**
 * This is the model class for table "emailAccounts".
 *
 * The followings are the available columns in table 'emailAccounts':
 * @property string $idEmailAccounts
 * @property string $name
 * @property string $link
 * @property string $EmailAccountsSMTPServerAdress
 * @property string $EmailAccountsSMTPUser
 * @property string $EmailAccountsSMTPPassword
 * @property integer $EmailAccountsSMTPPort
 * @property integer $EmailAccountsSMTPAuthenticationRequired
 * @property string $EmailAccountsPOP3ServerAdress
 * @property integer $EmailAccountsPOP3Port
 * @property string $EmailAccountsUser
 * @property string $EmailAccountsPassword
 * @property string $EmailAccountsIMAPServerAdress
 * @property string $EmailAccountsIMAPUser
 * @property string $EmailAccountsIMAPPassword
 * @property integer $EmailAccountsIMAPPort
 * @property string $EmailAccountsPostProtocol
 */
class EmailAccounts extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailAccounts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emailAccounts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('EmailAccountsSMTPPort, EmailAccountsSMTPAuthenticationRequired, EmailAccountsPOP3Port, EmailAccountsIMAPPort', 'numerical', 'integerOnly'=>true),
			array('idEmailAccounts', 'length', 'max'=>36),
			array('name, link, EmailAccountsSMTPServerAdress, EmailAccountsSMTPUser, EmailAccountsSMTPPassword, EmailAccountsPOP3ServerAdress, EmailAccountsUser, EmailAccountsPassword, EmailAccountsIMAPServerAdress, EmailAccountsIMAPUser, EmailAccountsIMAPPassword, EmailAccountsPostProtocol', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idEmailAccounts, name, link, EmailAccountsSMTPServerAdress, EmailAccountsSMTPUser, EmailAccountsSMTPPassword, EmailAccountsSMTPPort, EmailAccountsSMTPAuthenticationRequired, EmailAccountsPOP3ServerAdress, EmailAccountsPOP3Port, EmailAccountsUser, EmailAccountsPassword, EmailAccountsIMAPServerAdress, EmailAccountsIMAPUser, EmailAccountsIMAPPassword, EmailAccountsIMAPPort, EmailAccountsPostProtocol', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEmailAccounts' => 'Id Email Accounts',
			'name' => 'Name',
			'link' => 'Link',
			'EmailAccountsSMTPServerAdress' => 'Email Accounts Smtpserver Adress',
			'EmailAccountsSMTPUser' => 'Email Accounts Smtpuser',
			'EmailAccountsSMTPPassword' => 'Email Accounts Smtppassword',
			'EmailAccountsSMTPPort' => 'Email Accounts Smtpport',
			'EmailAccountsSMTPAuthenticationRequired' => 'Email Accounts Smtpauthentication Required',
			'EmailAccountsPOP3ServerAdress' => 'Email Accounts Pop3 Server Adress',
			'EmailAccountsPOP3Port' => 'Email Accounts Pop3 Port',
			'EmailAccountsUser' => 'Email Accounts User',
			'EmailAccountsPassword' => 'Email Accounts Password',
			'EmailAccountsIMAPServerAdress' => 'Email Accounts Imapserver Adress',
			'EmailAccountsIMAPUser' => 'Email Accounts Imapuser',
			'EmailAccountsIMAPPassword' => 'Email Accounts Imappassword',
			'EmailAccountsIMAPPort' => 'Email Accounts Imapport',
			'EmailAccountsPostProtocol' => 'Email Accounts Post Protocol',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEmailAccounts',$this->idEmailAccounts,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('EmailAccountsSMTPServerAdress',$this->EmailAccountsSMTPServerAdress,true);
		$criteria->compare('EmailAccountsSMTPUser',$this->EmailAccountsSMTPUser,true);
		$criteria->compare('EmailAccountsSMTPPassword',$this->EmailAccountsSMTPPassword,true);
		$criteria->compare('EmailAccountsSMTPPort',$this->EmailAccountsSMTPPort);
		$criteria->compare('EmailAccountsSMTPAuthenticationRequired',$this->EmailAccountsSMTPAuthenticationRequired);
		$criteria->compare('EmailAccountsPOP3ServerAdress',$this->EmailAccountsPOP3ServerAdress,true);
		$criteria->compare('EmailAccountsPOP3Port',$this->EmailAccountsPOP3Port);
		$criteria->compare('EmailAccountsUser',$this->EmailAccountsUser,true);
		$criteria->compare('EmailAccountsPassword',$this->EmailAccountsPassword,true);
		$criteria->compare('EmailAccountsIMAPServerAdress',$this->EmailAccountsIMAPServerAdress,true);
		$criteria->compare('EmailAccountsIMAPUser',$this->EmailAccountsIMAPUser,true);
		$criteria->compare('EmailAccountsIMAPPassword',$this->EmailAccountsIMAPPassword,true);
		$criteria->compare('EmailAccountsIMAPPort',$this->EmailAccountsIMAPPort);
		$criteria->compare('EmailAccountsPostProtocol',$this->EmailAccountsPostProtocol,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}