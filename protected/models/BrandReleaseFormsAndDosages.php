<?php

/**
 * This is the model class for table "brandReleaseFormsAndDosages".
 *
 * The followings are the available columns in table 'brandReleaseFormsAndDosages':
 * @property string $idBrandsReleaseFormsAndDosages
 * @property string $brandId
 * @property string $ReleaseFormsId
 * @property string $DosagesId
 * @property integer $BrandsReleaseFormsAndDosagesVolume
 * @property integer $BrandsReleaseFormsAndDosagesCount
 *
 * The followings are the available model relations:
 * @property Dosage $dosages
 * @property Brand $brand
 * @property ReleaseForm $releaseForms
 */
class BrandReleaseFormsAndDosages extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BrandReleaseFormsAndDosages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brandReleaseFormsAndDosages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('BrandsReleaseFormsAndDosagesVolume, BrandsReleaseFormsAndDosagesCount', 'numerical', 'integerOnly'=>true),
			array('idBrandsReleaseFormsAndDosages, brandId, ReleaseFormsId, DosagesId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idBrandsReleaseFormsAndDosages, brandId, ReleaseFormsId, DosagesId, BrandsReleaseFormsAndDosagesVolume, BrandsReleaseFormsAndDosagesCount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dosages' => array(self::BELONGS_TO, 'Dosage', 'DosagesId'),
			'brand' => array(self::BELONGS_TO, 'Brand', 'brandId'),
			'releaseForms' => array(self::BELONGS_TO, 'ReleaseForm', 'ReleaseFormsId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idBrandsReleaseFormsAndDosages' => 'Id Brands Release Forms And Dosages',
			'brandId' => 'Brand',
			'ReleaseFormsId' => 'Release Forms',
			'DosagesId' => 'Dosages',
			'BrandsReleaseFormsAndDosagesVolume' => 'Brands Release Forms And Dosages Volume',
			'BrandsReleaseFormsAndDosagesCount' => 'Brands Release Forms And Dosages Count',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idBrandsReleaseFormsAndDosages',$this->idBrandsReleaseFormsAndDosages,true);
		$criteria->compare('brandId',$this->brandId,true);
		$criteria->compare('ReleaseFormsId',$this->ReleaseFormsId,true);
		$criteria->compare('DosagesId',$this->DosagesId,true);
		$criteria->compare('BrandsReleaseFormsAndDosagesVolume',$this->BrandsReleaseFormsAndDosagesVolume);
		$criteria->compare('BrandsReleaseFormsAndDosagesCount',$this->BrandsReleaseFormsAndDosagesCount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}