<?php

/**
 * This is the model class for table "employee".
 *
 * The followings are the available columns in table 'employee':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $physicalPersonId
 * @property string $nameForSign
 * @property string $phones
 * @property string $organizationPositionId
 * @property string $email
 * @property string $number
 *
 * The followings are the available model relations:
 * @property Action[] $actions
 * @property Company[] $companies
 * @property CompanyContract[] $companyContracts
 * @property ContactInformation[] $contactInformations
 * @property OrganizationPosition $organizationPosition
 * @property PhysicalPerson $physicalPerson
 */
class Employee extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Employee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, physicalPersonId, organizationPositionId', 'length', 'max'=>36),
			array('name, link, nameForSign, phones, email, number', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, physicalPersonId, nameForSign, phones, organizationPositionId, email, number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'actions' => array(self::HAS_MANY, 'Action', 'employeeId'),
			'companies' => array(self::HAS_MANY, 'Company', 'employeeId'),
			'companyContracts' => array(self::HAS_MANY, 'CompanyContract', 'employeeId'),
			'contactInformations' => array(self::HAS_MANY, 'ContactInformation', 'employeeId'),
			'organizationPosition' => array(self::BELONGS_TO, 'OrganizationPosition', 'organizationPositionId'),
			'physicalPerson' => array(self::BELONGS_TO, 'PhysicalPerson', 'physicalPersonId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'physicalPersonId' => 'Physical Person',
			'nameForSign' => 'Name For Sign',
			'phones' => 'Phones',
			'organizationPositionId' => 'Organization Position',
			'email' => 'Email',
			'number' => 'Number',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('physicalPersonId',$this->physicalPersonId,true);
		$criteria->compare('nameForSign',$this->nameForSign,true);
		$criteria->compare('phones',$this->phones,true);
		$criteria->compare('organizationPositionId',$this->organizationPositionId,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('number',$this->number,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}