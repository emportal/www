<?php

/**
 * This is the model class for table "Log1CPayment".
 *
 * The followings are the available columns in table 'Log1CPayment':
 * @property string $id
 * @property string $dateUploaded
 * @property string $date
 * @property string $dateOfWithdrawal
 * @property integer $sum
 * @property string $payerAccount
 * @property string $payer1Name
 * @property string $payer2Name
 * @property string $payerSettlementAccount
 * @property string $payerINN
 * @property string $payerKPP
 * @property string $payerBank1Name
 * @property string $payerBank2Name
 * @property string $payerBIK
 * @property string $payerCorrespAccount
 * @property string $receiverAccount
 * @property string $receiverINN
 * @property string $receiverKPP
 * @property string $receiver1Name
 * @property string $receiver2Name
 * @property string $receiverSettlementAccount
 * @property string $receiverBank1Name
 * @property string $receiverBank2Name
 * @property string $receiverBIK
 * @property string $receiverCorrespAccount
 * @property string $paymentType
 * @property string $paymentMethod
 * @property string $paymentNumber
 * @property string $paymentPurpose
 * @property string $rawData
 */
class Log1CPayment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Log1CPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log1CPayment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			/* array('dateUploaded, date, dateOfWithdrawal, payerAccount, payer1Name, payer2Name, payerSettlementAccount, payerINN, payerKPP, payerBank1Name, payerBank2Name, payerBIK, payerCorrespAccount, receiverAccount, receiverINN, receiverKPP, receiver1Name, receiver2Name, receiverSettlementAccount, receiverBank1Name, receiverBank2Name, receiverBIK, receiverCorrespAccount, paymentType, paymentMethod, paymentNumber, paymentPurpose, rawData', 'required'), */
			//array('sum', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dateUploaded, date, dateOfWithdrawal, sum, payerAccount, payer1Name, payer2Name, payerSettlementAccount, payerINN, payerKPP, payerBank1Name, payerBank2Name, payerBIK, payerCorrespAccount, receiverAccount, receiverINN, receiverKPP, receiver1Name, receiver2Name, receiverSettlementAccount, receiverBank1Name, receiverBank2Name, receiverBIK, receiverCorrespAccount, paymentType, paymentMethod, paymentNumber, paymentPurpose, rawData', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dateUploaded' => 'Date Uploaded',
			'date' => 'Date',
			'dateOfWithdrawal' => 'Date Of Withdrawal',
			'sum' => 'Sum',
			'payerAccount' => 'Payer Account',
			'payer1Name' => 'Payer1 Name',
			'payer2Name' => 'Payer2 Name',
			'payerSettlementAccount' => 'Payer Settlement Account',
			'payerINN' => 'Payer Inn',
			'payerKPP' => 'Payer Kpp',
			'payerBank1Name' => 'Payer Bank1 Name',
			'payerBank2Name' => 'Payer Bank2 Name',
			'payerBIK' => 'Payer Bik',
			'payerCorrespAccount' => 'Payer Corresp Account',
			'receiverAccount' => 'Receiver Account',
			'receiverINN' => 'Receiver Inn',
			'receiverKPP' => 'Receiver Kpp',
			'receiver1Name' => 'Receiver1 Name',
			'receiver2Name' => 'Receiver2 Name',
			'receiverSettlementAccount' => 'Receiver Settlement Account',
			'receiverBank1Name' => 'Receiver Bank1 Name',
			'receiverBank2Name' => 'Receiver Bank2 Name',
			'receiverBIK' => 'Receiver Bik',
			'receiverCorrespAccount' => 'Receiver Corresp Account',
			'paymentType' => 'Payment Type',
			'paymentMethod' => 'Payment Method',
			'paymentNumber' => 'Payment Number',
			'paymentPurpose' => 'Payment Purpose',
			'rawData' => 'Raw Data',
		);
	}
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('dateUploaded',$this->dateUploaded,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('dateOfWithdrawal',$this->dateOfWithdrawal,true);
		$criteria->compare('sum',$this->sum);
		$criteria->compare('payerAccount',$this->payerAccount,true);
		$criteria->compare('payer1Name',$this->payer1Name,true);
		$criteria->compare('payer2Name',$this->payer2Name,true);
		$criteria->compare('payerSettlementAccount',$this->payerSettlementAccount,true);
		$criteria->compare('payerINN',$this->payerINN,true);
		$criteria->compare('payerKPP',$this->payerKPP,true);
		$criteria->compare('payerBank1Name',$this->payerBank1Name,true);
		$criteria->compare('payerBank2Name',$this->payerBank2Name,true);
		$criteria->compare('payerBIK',$this->payerBIK,true);
		$criteria->compare('payerCorrespAccount',$this->payerCorrespAccount,true);
		$criteria->compare('receiverAccount',$this->receiverAccount,true);
		$criteria->compare('receiverINN',$this->receiverINN,true);
		$criteria->compare('receiverKPP',$this->receiverKPP,true);
		$criteria->compare('receiver1Name',$this->receiver1Name,true);
		$criteria->compare('receiver2Name',$this->receiver2Name,true);
		$criteria->compare('receiverSettlementAccount',$this->receiverSettlementAccount,true);
		$criteria->compare('receiverBank1Name',$this->receiverBank1Name,true);
		$criteria->compare('receiverBank2Name',$this->receiverBank2Name,true);
		$criteria->compare('receiverBIK',$this->receiverBIK,true);
		$criteria->compare('receiverCorrespAccount',$this->receiverCorrespAccount,true);
		$criteria->compare('paymentType',$this->paymentType,true);
		$criteria->compare('paymentMethod',$this->paymentMethod,true);
		$criteria->compare('paymentNumber',$this->paymentNumber,true);
		$criteria->compare('paymentPurpose',$this->paymentPurpose,true);
		$criteria->compare('rawData',$this->rawData,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	static function addExistsCondition($criteria,$name,$value) {
		if(strval($name) === ""){ $criteria->addCondition("{$name} IS NULL OR {$name}=''"); }
		else { $criteria->addCondition("{$name} = ".Yii::app()->db->quoteValue($value)); }
	}
	
	public function findExists() {
		$criteria=new CDbCriteria;

		self::addExistsCondition($criteria, 'date', $this->date);
		self::addExistsCondition($criteria, 'dateOfWithdrawal', $this->dateOfWithdrawal);
		self::addExistsCondition($criteria, 'sum', $this->sum);
		self::addExistsCondition($criteria, 'payerAccount', $this->payerAccount);
		self::addExistsCondition($criteria, 'payer1Name', $this->payer1Name);
		self::addExistsCondition($criteria, 'payer2Name', $this->payer2Name);
		self::addExistsCondition($criteria, 'payerSettlementAccount', $this->payerSettlementAccount);
		self::addExistsCondition($criteria, 'payerINN', $this->payerINN);
		self::addExistsCondition($criteria, 'payerKPP', $this->payerKPP);
		self::addExistsCondition($criteria, 'payerBank1Name', $this->payerBank1Name);
		self::addExistsCondition($criteria, 'payerBank2Name', $this->payerBank2Name);
		self::addExistsCondition($criteria, 'payerBIK', $this->payerBIK);
		self::addExistsCondition($criteria, 'payerCorrespAccount', $this->payerCorrespAccount);
		self::addExistsCondition($criteria, 'receiverAccount', $this->receiverAccount);
		self::addExistsCondition($criteria, 'receiverINN', $this->receiverINN);
		self::addExistsCondition($criteria, 'receiverKPP', $this->receiverKPP);
		self::addExistsCondition($criteria, 'receiver1Name', $this->receiver1Name);
		self::addExistsCondition($criteria, 'receiver2Name', $this->receiver2Name);
		self::addExistsCondition($criteria, 'receiverSettlementAccount', $this->receiverSettlementAccount);
		self::addExistsCondition($criteria, 'receiverBank1Name', $this->receiverBank1Name);
		self::addExistsCondition($criteria, 'receiverBank2Name', $this->receiverBank2Name);
		self::addExistsCondition($criteria, 'receiverBIK', $this->receiverBIK);
		self::addExistsCondition($criteria, 'receiverCorrespAccount', $this->receiverCorrespAccount);
		self::addExistsCondition($criteria, 'paymentType', $this->paymentType);
		self::addExistsCondition($criteria, 'paymentMethod', $this->paymentMethod);
		self::addExistsCondition($criteria, 'paymentNumber', $this->paymentNumber);
		self::addExistsCondition($criteria, 'paymentPurpose', $this->paymentPurpose);
		
		return Log1CPayment::model()->count($criteria) > 0;
	}
    
	/**
	 * Создать модель данных из массива
	 * @return int fieldsChanged количество полей, изменненных после операции
	 */
    public function setAttributesFromArray(Array $array)
    {
        $fieldsChanged = 0;
        foreach ($this->tableSchema->columns as $column) {
            if (array_key_exists($column->comment, $array))
                $this->{$column->name} = $array[$column->comment];
        }
        return $fieldsChanged;
    }
}