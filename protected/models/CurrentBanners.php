<?php

/**
 * This is the model class for table "currentBanners".
 *
 * The followings are the available columns in table 'currentBanners':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $bannerTypeId
 * @property string $start
 * @property string $finish
 */
class CurrentBanners extends ActiveRecord
{
	public $username;
	public $password;
	
	public $_file;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CurrentBanners the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'currentBanners';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, link', 'required'),
			array('id, bannerTypeId', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>50),
			array('start, end', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, bannerTypeId, start, end', 'safe', 'on'=>'search'),
			array('username,password','required','on'=>'uploadBanner'),
			array('password','validatePassword','on'=>'uploadBanner'),
			array(
				'file',
				'file',
				'types'		 => 'jpg,jpeg, gif, png',
				'maxSize'	 => 1024 * 1024 * 5, // 5MB
				'allowEmpty' => 'true',
				'tooLarge'	 => 'Файл более 5MБ. Пожалуйста, загрузите файл меньшего размера.',
				'on' => 'uploadBanner'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'bannerTypeId' => 'Banner Type',
			'start' => 'Start',
			'end' => 'End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('bannerTypeId',$this->bannerTypeId,true);
		$criteria->compare('start',$this->start,true);
		$criteria->compare('end',$this->finish,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function scopes() {
		return [
			'active' => [
				'condition' => 't.start < NOW() AND t.end > NOW()'
			]
		];
	}
	
	public function validatePassword() {
		$result = $this->username === 'uploadBanner' && $this->password === 'sendBanner1553' ? true : false;
		if(!$result) {
			$this->addError('password','Неверная пара логин/пароль');
		}
	}
	
	public function getUploadPath() {

		$path = Yii::getPathOfAlias('uploads.banners');

		if ( !is_dir($path) )
			mkdir($path, 0775, true);
		return $path;
	}
	
	public function getFilePath() {
		return "{$this->uploadPath}/".$this->name;
	}

	public function getFile() {		
		if ( !isset($this->_file) ) 			
			$this->_file = is_file($this->filePath) ? $this->name : false;
		
	
		return $this->_file;
	}

	public function setFile($value) {
		$this->_file = $value;
	}
	
}