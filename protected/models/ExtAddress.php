<?php

/**
 * This is the model class for table "extAddress".
 *
 * The followings are the available columns in table 'extAddress':
 * @property string $name
 * @property integer $extCompanyId
 * @property string $addressId
 * @property integer $extId
 * @property string $extShortName
 * @property integer $extLPUType
 * @property string $extDescription
 * @property string $extDistrictId
 * @property integer $sysTypeId
 * @property string $lastUpdate
 * @property integer $isActive
 *
 * The followings are the available model relations:
 * @property ExtCompany $extCompany
 * @property ExtSysType $sysType
 * @property ExtSpeciality[] $extSpecialities
 * @property ExtSpeciality[] $extSpecialities1
 */
class ExtAddress extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExtAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'extAddress';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, extCompanyId, extId, sysTypeId', 'required'),
			array('extCompanyId, extId, extLPUType, sysTypeId, isActive', 'numerical', 'integerOnly'=>true),
			array('addressId, extDistrictId', 'length', 'max'=>36),
			array('extShortName', 'length', 'max'=>128),
			array('extDescription, lastUpdate, addressId', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('name, extCompanyId, addressId, extId, extShortName, extLPUType, extDescription, extDistrictId, sysTypeId, lastUpdate, isActive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'extCompany' => array(self::BELONGS_TO, 'ExtCompany', 'extCompanyId'),
			'sysType' => array(self::BELONGS_TO, 'ExtSysType', 'sysTypeId'),
			'extSpecialities' => array(self::HAS_MANY, 'ExtSpeciality', 'extAddressId'),
			'extSpecialities1' => array(self::HAS_MANY, 'ExtSpeciality', 'sysTypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Name',
			'extCompanyId' => 'Ext Company',
			'addressId' => 'Address',
			'extId' => 'Ext',
			'extShortName' => 'Ext Short Name',
			'extLPUType' => 'Ext Lputype',
			'extDescription' => 'Ext Description',
			'extDistrictId' => 'Ext District',
			'sysTypeId' => 'Sys Type',
			'lastUpdate' => 'Last Update',
			'isActive' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('extCompanyId',$this->extCompanyId);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('extId',$this->extId);
		$criteria->compare('extShortName',$this->extShortName,true);
		$criteria->compare('extLPUType',$this->extLPUType);
		$criteria->compare('extDescription',$this->extDescription,true);
		$criteria->compare('extDistrictId',$this->extDistrictId,true);
		$criteria->compare('sysTypeId',$this->sysTypeId);
		$criteria->compare('lastUpdate',$this->lastUpdate,true);
		$criteria->compare('isActive',$this->isActive);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}