<?php

/**
 * This is the model class for table "EquipmentOrgans".
 *
 * The followings are the available columns in table 'EquipmentOrgans':
 * @property string $idEquipmentOrgans
 * @property string $Owner_EquipmentId
 * @property string $AnatomicalAtlasId
 *
 * The followings are the available model relations:
 * @property AnatomicalAtlas $anatomicalAtlas
 * @property Equipment $ownerEquipment
 */
class EquipmentOrgans extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EquipmentOrgans the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EquipmentOrgans';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEquipmentOrgans', 'length', 'max'=>150),
			array('Owner_EquipmentId, AnatomicalAtlasId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idEquipmentOrgans, Owner_EquipmentId, AnatomicalAtlasId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'anatomicalAtlas' => array(self::BELONGS_TO, 'AnatomicalAtlas', 'AnatomicalAtlasId'),
			'ownerEquipment' => array(self::BELONGS_TO, 'Equipment', 'Owner_EquipmentId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEquipmentOrgans' => 'Id Equipment Organs',
			'Owner_EquipmentId' => 'Owner Equipment',
			'AnatomicalAtlasId' => 'Anatomical Atlas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEquipmentOrgans',$this->idEquipmentOrgans,true);
		$criteria->compare('Owner_EquipmentId',$this->Owner_EquipmentId,true);
		$criteria->compare('AnatomicalAtlasId',$this->AnatomicalAtlasId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}