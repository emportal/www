<?php

/**
 * This is the model class for table "userAPIAddresses".
 *
 * The followings are the available columns in table 'userAPIAddresses':
 * @property integer $userAPIId
 * @property string $addressId
 */
class UserAPIAddresses extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserAPIAddresses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userAPIAddresses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userAPIId, addressId', 'required'),
			array('userAPIId', 'numerical', 'integerOnly'=>true),
			array('addressId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userAPIId, addressId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userAPI' => array(self::BELONGS_TO, 'UserAPI', 'userAPIId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userAPIId' => 'User Apiid',
			'addressId' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userAPIId',$this->userAPIId);
		$criteria->compare('addressId',$this->addressId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}