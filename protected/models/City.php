<?php

/**
 * This is the model class for table "city".
 *
 * The followings are the available columns in table 'city':
 * @property string $id
 * @property string $name
 * @property string $locative
 * @property string $link
 * @property integer $population
 * @property string $countryId
 * @property integer $isTheSubway
 * @property integer $longitude
 * @property integer $latitude
 * @property string $subdomain
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property Country $country
 * @property CityDistrict[] $cityDistricts
 * @property MedicalSchool[] $medicalSchools
 * @property MetroStation[] $metroStations
 * @property ProducerOffice[] $producerOffices
 * @property User[] $users
 */
class City extends ActiveRecord
{
	const MOSKVA = "534bd8b9-e0d4-11e1-89b3-e840f2aca94f";
	const SPB = "534bd8b8-e0d4-11e1-89b3-e840f2aca94f";
	const EKATERINBURG = "15b7b541-edd0-11e1-b127-e840f2aca94f";
	const DEFAULT_CITY = self::SPB;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return City the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'city';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('population, isTheSubway, longitude, latitude', 'numerical', 'integerOnly'=>true),
			array('id, countryId', 'length', 'max'=>36),
			array('name, locative, link', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, locative, link, population, countryId, isTheSubway, longitude, latitude, subdomain', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'cityId'),
			'country' => array(self::BELONGS_TO, 'Country', 'countryId'),
			'cityDistricts' => array(self::HAS_MANY, 'CityDistrict', 'cityId'),
			'medicalSchools' => array(self::HAS_MANY, 'MedicalSchool', 'cityId'),
			'metroStations' => array(self::HAS_MANY, 'MetroStation', 'cityId'),
			'producerOffices' => array(self::HAS_MANY, 'ProducerOffice', 'cityId'),
			'users' => array(self::HAS_MANY, 'User', 'cityId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'locative' => 'Местный падеж',
			'link' => 'Link',
			'population' => 'Population',
			'countryId' => 'Country',
			'isTheSubway' => 'Is The Subway',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
			'subdomain' => 'Subdomain',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('locative',$this->locative);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('population',$this->population);
		$criteria->compare('countryId',$this->countryId,true);
		$criteria->compare('isTheSubway',$this->isTheSubway);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('subdomain',$this->subdomain);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getSelectedCity()
	{
		static $city = null;
		if ($city === null) {
			if (!empty(Yii::app()->session['selectedRegion']))
			{
				$attributes = ['subdomain' => Yii::app()->session['selectedRegion']];
				$city = City::model()->findByAttributes($attributes);
			}
			if (!$city)
				$city = City::model()->findByPk(self::DEFAULT_CITY);
		}
		return $city;
	}


	public function checkGeoIp(){
		if(!isset(Yii::app()->request->cookies['georegion'])){
			$SxGeo = Yii::app()->sxGeo;
			$ip = $_SERVER['REMOTE_ADDR'];//"92.255.15.246";//
			$cityUtf8 = iconv('windows-1251', 'UTF-8', $SxGeo->get($ip)['city']['name_ru']);
			$criteria = new CDbCriteria;
			$criteria->addCondition('name = :name','AND');
			$criteria->params = array( ':name'=>$cityUtf8 );
			$city = $this->model()->find($criteria);
			Yii::app()->request->cookies['georegion'] = new CHttpCookie('georegion', $city->attributes['subdomain'], ['path'=>'/', 'domain'=>'.emportal.ru']);
			if(is_object($city) && $city->attributes['subdomain'] != Yii::app()->session['selectedRegion']){
				$arrUrl = parse_url(Yii::app()->request->hostInfo);
				if(count(explode('.',$arrUrl['host'])) > 2){
					$url = substr( Yii::app()->getRequest()->getHostInfo(), strpos( Yii::app()->getRequest()->getHostInfo(), "." ) + 1 );
				}else{
					$url = $arrUrl['host'];
				}
				$url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://'.(($city->attributes['subdomain']=='spb')?'':$city->attributes['subdomain'].'.').$url;
				//Yii::app()->request->redirect($url);
				return $url;
			}
		} else {
			Yii::app()->request->cookies['georegion'] = new CHttpCookie('georegion', Yii::app()->session['selectedRegion'], ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
	}

	//получить Id города, который выбрал пользователь для просмотра
	public function getSelectedCityId()
	{
		static $cityId = null;
		if($cityId === null) {
			if (!empty(Yii::app()->session['selectedRegion']))
			{
				$attributes = ['subdomain' => Yii::app()->session['selectedRegion']];
				$cityId = City::model()->findByAttributes($attributes)->id;
			}
			if (!$cityId) $cityId = self::DEFAULT_CITY;
		}
		return $cityId;
	}
	
	public function getCityIdBySubdomain($subdomain)
	{
		$attributes = ['subdomain' => $subdomain];
		$cityId = City::model()->findByAttributes($attributes)->id;
		return $cityId;
	}
	
	public function changeRegion($regionSubdomain = null) {
		
		if (!$regionSubdomain) return false;
		$regions = Yii::app()->params['regions'];
		foreach($regions as $region) {			
			if ($region['subdomain'] == $regionSubdomain) Yii::app()->session['selectedRegion'] = $region['subdomain'];
		}
		return $regionSubdomain;
	}
	
	public static function getRedirectUrl($subdomain, $samozapis=FALSE, $url) {
		if(!empty($subdomain)) {
			if(!MyTools::validateHOST(Yii::app()->params) || $subdomain !== Yii::app()->session['selectedRegion'] || $samozapis !== Yii::app()->params["samozapis"]) {
				if(City::model()->changeRegion($subdomain) !== false) {
					$domain = array_reverse(explode('.', $_SERVER['HTTP_HOST']));
					$devDomains = array_values(array_intersect(["dev1","dev2","dev3","dev4","dev5","dev6","dev7","dev8","dev9","spb","local"], $domain));
					$matches = [];
					if(isset($domain[0])) $matches['domain'] = $domain[0];
					if(isset($domain[1])) $matches['domain'] = $domain[1] . '.' . $matches['domain'];
					$host = explode(':', Yii::app()->request->hostInfo);
					if(isset($host[0])) $matches['protocol'] = $host[0];
					else $matches['protocol'] = 'http';
					$url = $matches['protocol'] . "://" . ($samozapis ? Yii::app()->params['regions'][$subdomain]['samozapisSubdomain'].'.' : '') . ((!$samozapis AND $subdomain !== 'spb') ? $subdomain . '.' : '') . (count($devDomains)>0 ? $devDomains[0] . '.' : "") . $matches['domain'] . $url;
					return $url;
				}
			}
		}
		return false;
	}
	
	public static function isSelfRegisterCity() {
		return (
			$_GET['selfRegister']
			||
			Yii::app()->params['regions'][Yii::app()->session['selectedRegion']]['selfRegisterScenario']
		) ? true : false;
	}
    
    public function getSelfRegisterCitySubdomains() {
        $subdomains = [];
        foreach(Yii::app()->params['regions'] as $region)
            if ($region['selfRegisterScenario'] === true)
                $subdomains[] = $region['subdomain'];
        return $subdomains;
    }
    
    public function getShortGenitive() {
		return (!empty($this->shortName)
					? $this->shortName
					: (!empty($this->genitive) ? $this->genitive : $this->name)
				);
    }
    
    public function getLocative() {
    	return (!empty($this->locative)
    			? $this->locative
    			: $this->name
    	);
    }
}