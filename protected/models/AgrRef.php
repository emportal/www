<?php

/**
 * This is the model class for table "agr_ref".
 *
 * The followings are the available columns in table 'agr_ref':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $ownerId
 * @property string $refValue
 * @property string $typeId
 * @property integer $forCompany
 * @property integer $forProducers
 * @property integer $forDoctors
 */
class AgrRef extends AgrActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrRef the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agr_ref';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ownerId, typeId, refValue', 'required'),
			array('forCompany, forProducers, forDoctors', 'numerical', 'integerOnly'=>true),
			array('id, ownerId typeId', 'length', 'max'=>36),
			array('name, link, refValue', 'length', 'max'=>150),
			array('refValue', 'url', 'validateIDN'=>true, 'message'=>'Неправильно введен адрес сайта', 'defaultScheme' => 'http', 'validSchemes'=>array('http', 'https')),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, companyId, doctorId, producerId, stateHealthAgencyId, insuranceCompanyId, refValue, typeId, forCompany, forProducers, forDoctors', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'brands' => array(self::HAS_MANY, 'Brand', 'refId'),
			'producerBrands' => array(self::HAS_MANY, 'ProducerBrand', 'refId'),
			'type' => array(self::BELONGS_TO, 'ReferenceType', 'typeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'ownerId' => 'Owner',
			'refValue' => 'Ref Value',
			'typeId' => 'Type',
			'forCompany' => 'For Company',
			'forProducers' => 'For Producers',
			'forDoctors' => 'For Doctors',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('ownerId',$this->ownerId,true);
		$criteria->compare('refValue',$this->refValue,true);
		$criteria->compare('typeId',$this->typeId,true);
		$criteria->compare('forCompany',$this->forCompany);
		$criteria->compare('forProducers',$this->forProducers);
		$criteria->compare('forDoctors',$this->forDoctors);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}