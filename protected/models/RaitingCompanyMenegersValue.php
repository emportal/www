<?php

/**
 * This is the model class for table "raitingCompanyMenegersValue".
 *
 * The followings are the available columns in table 'raitingCompanyMenegersValue':
 * @property string $id
 * @property string $raitingCompanyMenegersId
 * @property string $typeOfCompanyRaitingManagersId
 * @property integer $value
 */
class RaitingCompanyMenegersValue extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RaitingCompanyMenegersValue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'raitingCompanyMenegersValue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('value', 'numerical', 'integerOnly'=>true),
			array('id, raitingCompanyMenegersId, typeOfCompanyRaitingManagersId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, raitingCompanyMenegersId, typeOfCompanyRaitingManagersId, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'raitingCompanyMenegersId' => 'Raiting Company Menegers',
			'typeOfCompanyRaitingManagersId' => 'Type Of Company Raiting Managers',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('raitingCompanyMenegersId',$this->raitingCompanyMenegersId,true);
		$criteria->compare('typeOfCompanyRaitingManagersId',$this->typeOfCompanyRaitingManagersId,true);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}