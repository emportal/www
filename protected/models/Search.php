<?php

/**
 * Description of Search
 *
 * @author NeoSonic
 *
 * @property-read MetroStation[] $metroStations
 * @property-read CityDistrict[] $cityDistricts
 * @property-read DoctorSpecialty[] $DoctorSpecialties
 * @property-read AddressServices[] $CompanyServices
 *
 */
class Search extends CFormModel {

	const S_PATIENT = 'patient';
	const S_PATIENT_NEW = 'patient_new';
	const S_PATIENT_CLINIC = 'patient_clinic';
	const S_DOCTOR = 'doctor';
	const S_CLINIC = 'clinic';
	const S_INSURANCE = 'insurance';
	const S_TOURISM = 'tourism';
	const S_BEAUTY = 'beauty';
	const S_HANDBOOK = 'handbook';
	const S_DISEASE = 'disease';
	const S_MEDICAMENT = 'medicament';
	const S_DOCTORSPECIALTY = 'doctorSpecialty';
	const S_COMPANYACTIVITY = 'companyActivity';
	const S_PRODUCER = 'producer';
	const S_SERVICE = 'service';
	const S_SMALL = 'small';
	const S_ACTION = 'action';

	public static $letters = array(
		'ru_new' => array(
			'А' => 'а', 'Б' => 'б', 'В' => 'в', 'Г' => 'г', 'Д' => 'д', 'Е' => 'её', 'Ж' => 'ж', 'З' => 'з', 'И' => 'ий',
			'К' => 'к', 'Л' => 'л', 'М' => 'м', 'Н' => 'н', 'О' => 'о', 'П' => 'п', 'Р' => 'р', 'С' => 'с', 'Т' => 'т',
			'У' => 'у', 'Ф' => 'ф', 'Х' => 'х', 'Ц' => 'ц', 'Ч' => 'ч', 'Ш' => 'ш', 'Щ' => 'щ', 'Э' => 'э', 'Ю' => 'ю',
			'Я' => 'я'
		),
		'en_new' => array(
			'A' => 'a', 'B' => 'b', 'C' => 'c', 'D' => 'd', 'E' => 'e',
			'F' => 'f', 'G' => 'g', 'H' => 'h', 'I' => 'i', 'J' => 'j',
			'K' => 'k', 'L' => 'l', 'M' => 'm', 'N' => 'n', 'O' => 'o',
			'P' => 'p', 'Q' => 'q', 'R' => 'r', 'S' => 's', 'T' => 't',
			'U' => 'u', 'V' => 'v', 'W' => 'w', 'X' => 'x', 'Y' => 'y',
			'Z' => 'z',
		),
		'ru' => array(
			'А-В' => 'абв',
			'Г-Е' => 'гдеё',
			'Ж-И' => 'жзий',
			'К-М' => 'клм',
			'Н-П' => 'ноп',
			'Р-Т' => 'рст',
			'У-Х' => 'уфх',
			'Ц-Ш' => 'цчшщ',
			'Э-Я' => 'эюя',
		),
		'en' => array(
			'A-C' => 'abc',
			'D-F' => 'def',
			'G-I' => 'ghi',
			'J-L' => 'jkl',
			'M-O' => 'mno',
			'P-R' => 'pqr',
			'S-U' => 'stu',
			'V-X' => 'vwx',
			'Y-Z' => 'yz',
		)
	);
	public static $prices = array(
		2 => array(
			'min' => 0,
			'max' => 500
		),
		3 => array(
			'min' => 500,
			'max' => 1000
		),
		4 => array(
			'min' => 1000,
			'max' => 2000
		),
		5 => array(
			'min' => 2000,
			'max' => 3000
		),
		6 => array(
			'min' => 3000,
			'max' => 5000
		),
		7 => array(
			'min' => 5000,
			'max' => 0
		),
	);
	public $priceMin;
	public $priceMax;
	public $clinicLinkUrl;
	public $onlySale;
	public $loyaltyProgram;
	public $sort;

	/**
	 * @var string Текст поискового запроса
	 */
	public $text;

	/**
	 *
	 * @var boolean Наличие стационара
	 */
	public $IsHospital;

	/**
	 * @var GUID Станция метро
	 */
	public $metroStationId;

	public $addressLink;
	/**
	 * @var GUID Район города
	 */
	public $cityDistrictId;

	/**
	 *
	 * @var GUID Пол специалиста
	 */
	public $sexId;
	public $start;
	public $expandSearchArea;

	/**
	 * @var GUID Специальность доктора
	 */
	public $doctorSpecialtyId;

	/**
	 * @var GUID профиль деятельности клиники
	 */
	public $companyActiviteId;

	/**
	 * @var Услуги клиники
	 */
	public $companyServiceId;
	
	/**
	 * @var GUID тип контрагента
	 */
	public $companyTypeId;
	public $IsOMS, $IsDMS, $isGMU;

	/**
	 * @var boolean Вызов на дом
	 */
	public $onHouse = false;
	/**
	 * @var boolean доступна online запись
	 */
	public $offline = false;

	/**
	 *
	 * @var boolean Поиск точный или по регулярному выражению
	 */
	public $isStrictText;

	/**
	 * @var GUID Регион
	 */
	public $serviceId;
	public $serviceName;
	public $for_free;
	public $regionId;

	/**
	 * @var GUID Город
	 */
	//public $cityId =  '534bd8b8-e0d4-11e1-89b3-e840f2aca94f';
	//public $cityId;
	/* Внизу геттер !!!!! */

	/**
	 * @var GUID Город
	 */
	public $brandId;

	/**
	 * @var GUID Город
	 */
	public $nomenclatureGroupId;

	/**
	 * @var string
	 */
	public $chars;

	/**
	 *
	 * @var integer
	 */
	public $price;

	/**
	 * @var string
	 */
	public $chars_lang;

	/**
	 * @var string
	 */
	public $visit_time;
	public $longitude;
	public $latitude;

	/**
	 * @var integer|array
	 */
	public $service;

	public $addressName;
	public $companyName;

	public $OMSForm_surName;
	public $OMSForm_firstName;
	public $OMSForm_fatherName;
	public $OMSForm_birthday;
	public $OMSForm_extAddressId;
	public $OMSForm_patientId;
	public $OMSForm_omsNumber;
	public $OMSForm_omsSeries;
	public $OMSForm_specialityId;
	public $OMSForm_referralId;
	public $OMSForm_exceptions = [];

	public $doctorSpecialtiesResultData = [];
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text', 'length', 'min' => 3, 'max' => 150),
			array('metroStationId, price, priceMin, priceMax, clinicLinkUrl, addressLink, for_free, serviceName, onlySale,
				loyaltyProgram, expandSearchArea, longitude, latitude, serviceId, cityDistrictId, sexId, doctorSpecialtyId,
				companyServiceId, isStrictText, companyTypeId, companyActiviteId, start, onHouse, offline, sort,
				OMSForm_surName, OMSForm_firstName, OMSForm_fatherName, OMSForm_birthday, OMSForm_extAddressId, OMSForm_patientId, OMSForm_omsNumber, OMSForm_omsSeries, OMSForm_specialityId', 'safe'),
			array('IsOMS, IsHospital, IsDMS, isGMU', 'safe', 'on' => Search::S_PATIENT),
			array('IsOMS, IsHospital, IsDMS, isGMU', 'safe', 'on' => Search::S_PATIENT_NEW),
			array('IsOMS, IsHospital, IsDMS, isGMU', 'safe', 'on' => Search::S_CLINIC),
			array('IsOMS, IsDMS', 'safe', 'on' => array(Search::S_INSURANCE, Search::S_BEAUTY, Search::S_TOURISM)),
			array('for_free', 'numerical', 'integerOnly' => true),
			
			//array('', 'length', 'max' => 36, 'min'	=> 36, 'on'=>  array(Search::S_PATIENT, Search::S_DOCTOR, Search::S_CLINIC)),
			array('chars', 'length', 'min' => 3, 'max' => 3, 'on' => Search::S_HANDBOOK),
			array('start', 'date', 'format' => 'yyyy-mm-dd')
		);
	}
	public function initOMSForm() {
		$omsForm = new OMSForm;
		$omsForm->surName = $this->OMSForm_surName;
		$omsForm->firstName = $this->OMSForm_firstName;
		$omsForm->fatherName = $this->OMSForm_fatherName;
		$omsForm->birthday = $this->OMSForm_birthday;
		$omsForm->omsSeries = $this->OMSForm_omsSeries;
		$omsForm->omsNumber = $this->OMSForm_omsNumber;
		$omsForm->referralId = $this->OMSForm_referralId;
		$omsForm->initOMSForm();
		$this->OMSForm_surName = $omsForm->surName;
		$this->OMSForm_firstName = $omsForm->firstName;
		$this->OMSForm_fatherName = $omsForm->fatherName;
		$this->OMSForm_birthday = $omsForm->birthday;
		$this->OMSForm_omsSeries = $omsForm->omsSeries;
		$this->OMSForm_omsNumber = $omsForm->omsNumber;
		$this->OMSForm_specialityId = trim(strval($this->OMSForm_specialityId));
		$this->OMSForm_referralId = $omsForm->referralId;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'text' => 'Текст запроса',
			'metroStationId' => 'Станция метро',
			'cityDistrictId' => 'Район города',
			'doctorSpecialtyId' => 'Специальность врача',
			'companyServiceId' => 'Услуга',
			'companyActiviteId' => 'Профиль деятельности',
			'companyTypeId' => 'Тип контрагента',
			'start' => 'Начало акции после');
	}

	static public function regexpForQuery($searchquery, $type=1) {
		if(mb_strlen($searchquery, 'utf-8') < 3) return "";
		if (!function_exists('sort_by_length')) {

			function sort_by_length($cur_val, $next_val) {
				$cur = strlen($cur_val);
				$next = strlen($next_val);

				//if ($cur==$next) return 1;

				return ($cur < $next) ? -1 : 1;
			}

		}
		$search = array(
			"/\\\\/",
			"/([\r\n])[\s]+/",
			"/\s*(.+)\s*/",
		);

		$replace = array(
			"\\",
			" ",
			"$1",
		);

		$sorted_regexp = preg_replace($search, $replace, preg_quote($searchquery));
		$sorted_regexp_array = array();
		$sorted_regexp_array = preg_split("/[\s,]+/", $sorted_regexp);

		if($type == 1) {
			foreach ($sorted_regexp_array as $key => $word) {
				$word = trim($word);
				if (empty($word) || (count($sorted_regexp_array)>3 && mb_strlen($word) < 3) || (count($sorted_regexp_array)>4 && mb_strlen($word) < 4) || (count($sorted_regexp_array)>4 && $key>4)) {
					unset($sorted_regexp_array[$key]);
				} else {
					$sorted_regexp_array[$key] = $word;
				}
			}
			$sorted_regexp_array = array_unique($sorted_regexp_array);
			//usort($sorted_regexp_array, "sort_by_length");
			
			$permutations = self::permutation($sorted_regexp_array);
			$searchquery_regexp = "";
			foreach ($permutations as $permutation) {
				if(!empty($searchquery_regexp)) $searchquery_regexp .= "|";
				$searchquery_regexp .= preg_replace("/.+/", "($0)", implode(".*", $permutation));
			}
			$searchquery_regexp = preg_replace("/.+/", "($0)", $searchquery_regexp);
		} else {
			foreach ($sorted_regexp_array as $key => $word) {
				$word = trim($word);
				if (empty($word) || (count($sorted_regexp_array)>2 && mb_strlen($word) < 5) || (count($sorted_regexp_array)>1 && mb_strlen($word) < 3)) {
					unset($sorted_regexp_array[$key]);
				} else {
					$sorted_regexp_array[$key] = $word;
				}
			}
			$sorted_regexp_array = array_unique($sorted_regexp_array);
			usort($sorted_regexp_array, "sort_by_length");
			$searchquery_regexp = implode("|", $sorted_regexp_array);
			$searchquery_regexp = preg_replace("/.+/", "($0)", $searchquery_regexp);
		}

		$search = array(
			"/(\|)/",
			"/(\\\\\|\\\\\))/",
			"/'/",
			'/"/',
			"/^(.*)$/"
		);

		$replace = array(
			"|",
			")",
			"\'",
			"\"",
			"\$1"
		);

		//$searchquery_regexp = preg_replace($search, $replace, preg_quote($searchquery_regexp));
		$searchquery_regexp = preg_replace($search, $replace, $searchquery_regexp);
		return strtolower($searchquery_regexp);
	}
	
	
	static function permutation($arr) {
	    if(is_array($arr)&&count($arr)>1) {
	        foreach($arr as $k=>$v) {
	            $answer[][]=$v;
	        }
	        do {
	            foreach($arr as $k=>$v) {
	                foreach($answer as $key=>$val) {
	                    if(!in_array($v,$val)) {
	                        $tmpArr[]=array_merge(array($v),$val);
	                    }
	                }
	            }
	            $answer=$tmpArr;
	            unset($tmpArr);
	        }while(count($answer[0])!=count($arr));
	        return $answer;
	    }else
	        $answer=[$arr];
	    return $answer;
	}

	public function getMetroStations() {
		#MetroStation
		$criteria = new CDbCriteria;
		$criteria->condition = 'cityId=:cityId';
		$criteria->params = array(':cityId' => $this->cityId);
		$criteria->order = 'name';
		$oMetroStation = MetroStation::model()->findAll($criteria);
		$tempData = CHtml::listData($oMetroStation, 'linkUrl', 'name');
		foreach($tempData as $key=>$value) {
			$resultData['metro-'.$key]=$value;
		}
		return $resultData;
	}

	public function getCompanyActivities() {
		#companyActivite
		$criteria = new CDbCriteria;
		$criteria->order = 'name';
		$criteria->select = 'id,name,linkUrl';
		
		/* Выводим только те профили, по которым есть услуги */
		$criteria->with = [
			'addressServices' => [
				'select' => false,
				'together' => true,
				'joinType' => 'INNER JOIN'
			]
		];

		$oCompanyActivite = CompanyActivite::model()->findAll($criteria);
		$tempData = CHtml::listData($oCompanyActivite, 'linkUrl', 'name');
		foreach($tempData as $key=>$value) {
			$resultData['profile-'.$key]=$value;
		}
		return $resultData;
	}

	public function getCompanyTypes() {
		#companyType
		$excludeCriteria = new CDbCriteria();
		$excludeCriteria->with = [
			'companies' => [
				'select' => 'id',
				'together' => true,
				'with' => [
					'addresses' => [
						'select' => false,
						'together' => true,
						'with' => [
							'userMedicals' =>[
								'select' => false,
								'together' => true,
							]
						]
					]
				]
			]
		];
		$excludeCriteria->select = 'id,name';
		$excludeCriteria->order = "t.name ASC";
		$excludeCriteria->compare('t.isMedical', 1);
		$excludeCriteria->compare('addresses.cityId', $this->cityId);
		$excludeCriteria->addCondition("userMedicals.agreementNew = 1 AND addresses.isActive = 1 AND (`companies`.`categoryId` = 'a3969945e0f59fa84e0e7740c2e8cc87' OR `companies`.`categoryId` = '8f7f0e92e866321d4ac5f2a5378a4668')");	

		$excludeCompanyTypes = CompanyType::model()->findAll($excludeCriteria);
		$types = [];
		foreach ($excludeCompanyTypes as $ex) {			
			if (count($ex->companies)) {
				$types[] = $ex->id;
			}
		}


		$criteria = new CDbCriteria;
		$criteria->addCondition('isMedical', 1);
		$criteria->order = 'name';
		$criteria->addInCondition('id',$types);
		$oCompanyType = CompanyType::model()->findAll($criteria);

		return CHtml::listData($oCompanyType, 'id', 'name');
	}

	public function getSexTypes() {
		$criteria = new CDbCriteria;
		$sex = Sex::model()->findAll($criteria);

		return CHtml::listData($sex, 'id', 'name');
	}

	public function getCityDistricts() {
		$criteria = new CDbCriteria;
		//$criteria->condition = 'cityId=:cityId';
		//$criteria->params = array(':cityId' => $this->cityId);
		$criteria->compare('t.cityId', $this->cityId, false);
		$criteria->order = 'name';
		$oCityDistrict = CityDistrict::model()->findAll($criteria);
		
		$tempData = CHtml::listData($oCityDistrict, 'linkUrl', 'name');
		foreach($tempData as $key=>$value) {
			$resultData['raion-'.$key]=$value;
		}
		return $resultData;
	}
	
	public function loadSpecialtiesFromEmias() {
		$this->OMSForm_exceptions = [];
		$emaisLoader = new EmiasLoader;
		$emiasInfo = $emaisLoader->getSpecialitiesInfo([
				'omsSeries' => $this->OMSForm_omsSeries,
				'omsNumber' => $this->OMSForm_omsNumber,
				'birthDate' => date('Y-m-d\TH:i:s',strtotime($this->OMSForm_birthday)),
		]);
		if(isset($emiasInfo->return)) {
			$specialityList = $emaisLoader->populateSpecialityList($emiasInfo->return, false);
			foreach ($specialityList as $key=>$value) {
				$resultData['specialty-'.$value['linkUrl']] = $value['name'];
			}
		}
		elseif(isset($emiasInfo->detail->exception)) {
			$this->OMSForm_exceptions[] = $emiasInfo->detail->exception;
		}
		elseif(isset($emiasInfo->message)) {
			$this->OMSForm_exceptions[] = $emiasInfo->message;
		}
		else {
			$this->OMSForm_exceptions[] = "врачи не найдены";
		}
		$this->doctorSpecialtiesResultData = $resultData;
		return $resultData;
	}
	
	public function getDoctorSpecialties() {
		$resultData = [];
		if(!empty($this->doctorSpecialtiesResultData)) {
			$resultData = $this->doctorSpecialtiesResultData;
		}
		elseif((Yii::app()->params["samozapis"]) AND $this->cityId === City::MOSKVA AND (trim(strval($this->OMSForm_omsNumber)) !== "" AND trim(strval($this->OMSForm_birthday)) !== "")) {
			$resultData = $this->loadSpecialtiesFromEmias();
		} else {
			$criteria = new CDbCriteria;
			$criteria->order = 't.name';
			$criteria->select = 'name, link, id, linkUrl, cityId';
			$criteria->compare("samozapis", (Yii::app()->params["samozapis"]) ? 1 : 0);
			$criteria->compare("cityId", $this->cityId);
			/*$criteria->with = [
				'doctors' => [
					'select' => false,
					'joinType' => 'INNER JOIN'
				]
			];*/
			$oDoctorSpecialty = ShortSpecialtyOfDoctor::model()->findAll($criteria);
			$tempData = CHtml::listData($oDoctorSpecialty, 'linkUrl', 'name');
			foreach($tempData as $key=>$value) {
				$resultData['specialty-'.$key]=$value;
			}
			$this->doctorSpecialtiesResultData = $resultData;
		}
		return $resultData;		
	}
	
	public function getCompanyServices() {
		return self::GetSearchParametersService($this->cityId);
	}

	public function getRegions() {
		return array(); /* CHtml::listData(
		  Regions::model()->findAll(array('order' => 'name')),
		  'id', 'name'
		  ); */
	}

	public function getNomenclatureGroups() {
		return CHtml::listData(
						NomenclatureGroup::model()->findAll(array('order' => 'name')), 'id', 'name'
		);
	}

	public function getCities() {
		return CHtml::listData(
						Cities::model()->findAll(array('order' => 'name')), 'id', 'name'
		);
	}

	public function getBrands() {
		return CHtml::listData(
						Brands::model()->findAll(array('order' => 'name')), 'id', 'name'
		);
	}

	public function getServiceModel() {
		return Service::model()->find('t.id=:sId', array(":sId" => $this->serviceId));
	}

	public function getLetters() {
		if (!$this->chars)
			return array();

		foreach (Search::$letters as $lang)
			if (isset($lang[$this->chars]))
				return preg_split('//u', $lang[$this->chars], -1, PREG_SPLIT_NO_EMPTY);
	}

	/*
	 * Установка значений поиска по первым буквам
	 */

	public static function setLetters($letters = array()) {
		Search::$letters = $letters;
	}

	public function getCityId() {
		
		/*
		$link = '';
		if (!empty(Yii::app()->session['selectedRegion'])) {
			$link = City::model()->findByAttributes(['subdomain' => Yii::app()->session['selectedRegion']])->link;
		}
		if (!$link) $link = '1'; //по умолчанию - Питер
		return City::model()->findByAttributes(array('link' => $link))->id;
		*/
		
		return City::model()->selectedCityId;
	}

	public function getCityName() {
		return City::model()->findByAttributes(array('id' => $this->cityId))->name;
	}

	public function getCityPrepositionalName() {
		return City::model()->findByAttributes(array('id' => $this->cityId))->prepositional;
	}

	public function getRequestedLocative() {
		static $locative = NULL;
		static $metroDistrictStr = NULL;
		static $district_metro = NULL;
		if($locative === NULL || $metroDistrictStr === NULL) {
			$locative = "";
			$metroDistrictStr = "";
			$REQUEST_metro = is_array($_REQUEST["metro"]) ? $_REQUEST["metro"] : ((!empty($_REQUEST["metro"])) ? [$_REQUEST["metro"]] : []);
			$REQUEST_district = is_array($_REQUEST["district"]) ? $_REQUEST["district"] : ((!empty($_REQUEST["district"])) ? [$_REQUEST["district"]] : []);
			$getParamSearchMetroStationId = !empty($_REQUEST['Search']['metroStationId']) 
				? (is_array($_REQUEST['Search']['metroStationId']) 
						? $_REQUEST['Search']['metroStationId'] 
						: [$_REQUEST['Search']['metroStationId']])
				: [];
			$district_metro = array_merge(array_merge($REQUEST_metro,$REQUEST_district),$getParamSearchMetroStationId);
			if(!is_array($district_metro)) $district_metro = [ $district_metro ];
			if(!empty($district_metro)) {
				$searchDistrictString = "";
				foreach ($district_metro as $index=>$value) {
					if(isset($this->metroStations[$value])) {
						if(!empty($locative)) $locative .= ", ";
						else $locative .= "районе ";
						$locative .= "метро " . $this->metroStations[$value];
						
						if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
						$metroDistrictStr .= "метро " . $this->metroStations[$value];
					}
				}
				foreach ($district_metro as $index=>$value) {
					if(isset($this->cityDistricts[$value])) {
						if(!empty($locative)) $locative .= ", ";
						$locative .= $this->cityDistricts[$value] . " район";
						
						if(!empty($metroDistrictStr)) $metroDistrictStr .= ", ";
						$metroDistrictStr .= $this->cityDistricts[$value] . " район";
					}
				}
			} else { $locative = City::model()->findByPk(City::model()->getSelectedCityId())->locative; }
		}
		return [
			'district_metro' => $district_metro, // массив метро и районов
			'locative' => $locative, // строка в местном падеже
			'metroDistrict' => $metroDistrictStr, // метро и районы через запятую
		];
	}
	
	static public function GetSearchParametersMetro($cityId) {
		$MetroStation = MetroStation::model()->findAll('cityId=:cityId',array(':cityId' => $cityId));
		$metroArray = CHtml::listData($MetroStation, 'linkUrl', 'name');
		unset($MetroStation);
		$result = [];
		foreach($metroArray as $key=>$value) {
			$result['metro-'.$key] = $value;
		}
		unset($metroArray);
		return $result;
	}
	static public function GetSearchParametersDistrict($cityId) {
		$CityDistrict = CityDistrict::model()->findAll('cityId=:cityId',array(':cityId' => $cityId));
		$districtArray = CHtml::listData($CityDistrict, 'linkUrl', 'name');
		unset($CityDistrict);
		$result = [];
		foreach($districtArray as $key=>$value) {
			$result['raion-'.$key] = $value;
		}
		unset($districtArray);
		return $result;
	}

	static public function GetSearchParametersService($cityId) {
		$criteria = new CDbCriteria;
		$criteria->order = 't.name';
		$criteria->select = 't.linkUrl, t.name';
		$criteria->compare('isActual', 1); //не показываем услуги, которые давно не обновлялись
		$criteria->compare('t.link', "<>zapnp");
		$criteria->compare("address.cityId", $cityId);
		$criteria->compare('address.isActive', 1);
		$criteria->compare('company.removalFlag', 0);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->with = [
				'addressServices' => [
						'select' => false,
						'joinType' => 'INNER JOIN',
						'with' => [
								'address' => [
										'select' => false,
										'joinType' => 'INNER JOIN',
										'with' => [
												'company' => [
														'together' => true,
												],
												'userMedicals' => [
														'select' => false,
														'joinType' => 'INNER JOIN',
														'together' => true
												],
										],
								],
						],
				],
		];
		$oCompanyServices = Service::model()->findAll($criteria);
		unset($criteria);
		$tempData = CHtml::listData($oCompanyServices, 'linkUrl', 'name');
		unset($oCompanyServices);
		$result = [];
		foreach($tempData as $key=>$value) {
			$result['service-'.$key]=$value;
		}
		unset($tempData);
		return $result;
	}
	
	static public function GetSearchParametersOfService($serviceLinkUrl, $cityId, $samozapis) {
		$criteria = new CDbCriteria;
		$criteria->select = "id";
		$criteria->group = "t.id";
		$criteria->with = [
				'company' => [
						'together' => true,
						'with' => [
								'userMedicals' => [
										'together' => true,
								],
						],
				],
				'addressServices' => [
						'together' => true,
						'with' => [
								'service' => [
										'together' => true,
								],
						],
				],
		];
		$criteria->compare("service.linkUrl", $serviceLinkUrl);
		$criteria->compare("company.removalFlag", 0);
		$criteria->compare("userMedicals.agreementNew", 1);
		$criteria->compare("t.isActive", 1);
		$criteria->compare("t.cityId", $cityId);
		$criteria->compare("t.samozapis",($samozapis) ? 1 : 0);
		$addresses = Address::model()->findAll($criteria);
		unset($criteria);
		$addressList = [];
		foreach ($addresses as $value) {
			$addressList[] = $value->id;
		}
		unset($addresses);
		
		$criteria = new CDbCriteria;
		$criteria->select = "linkUrl";
		$criteria->group = "t.linkUrl";
		$criteria->with = [
				'nearestMetroStations' => [
						'together' => true,
						'with' => [
								'address' => [
										'together' => true,
								],
						],
				],
		];
		$criteria->addInCondition('address.id',$addressList);
		$metroModel = MetroStation::model()->findAll($criteria);
		unset($criteria);
		$Metro = [];
		foreach($metroModel as $value) {
			$Metro[] = 'metro-'.$value->linkUrl;
		}
			
		$criteria = new CDbCriteria;
		$criteria->select = "linkUrl";
		$criteria->group = "t.linkUrl";
		$criteria->with = [
				'addresses' => [
						'together' => true,
				],
		];
		$criteria->addInCondition('addresses.id',$addressList);
		$districtsModel = CityDistrict::model()->findAll($criteria);
		unset($criteria);
		$District = [];
		foreach($districtsModel as $value) {
			$District[] = 'raion-'.$value->linkUrl;
		}
			
		$criteria = new CDbCriteria;
		$criteria->select = "linkUrl";
		$criteria->group = "t.linkUrl";
		$criteria->with = [
				'address' => [
						'together' => true,
				],
		];
		$criteria->addInCondition('address.id',$addressList);
		$companyModel = Company::model()->findAll($criteria);
		unset($criteria);
		$Companies = [];
		foreach($companyModel as $value) {
			$Companies[] = $value->linkUrl;
		}

		return ["Metro"=>$Metro, "District"=>$District, "Companies"=>$Companies];
	}
	
	public $cache_OMSFormData_check = NULL;
	public function OMSFormData_check() {
		if($cache_OMSFormData_check === NULL) {
			$this->loadSpecialtiesFromEmias();
			$cache_OMSFormData_check = empty($this->OMSForm_exceptions);
		}
		return $cache_OMSFormData_check;
	}
}

?>
