<?php

/**
 * This is the model class for table "workingHoursOfDoctors".
 *
 * The followings are the available columns in table 'workingHoursOfDoctors':
 * @property string $doctorId
 * @property string $companyId
 * @property string $addressId
 * @property string $timeStart
 * @property string $timeEnd
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property Company $company
 * @property Doctor $doctor
 */
class WorkingHoursOfDoctors extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WorkingHoursOfDoctors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'workingHoursOfDoctors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctorId, companyId, addressId', 'length', 'max'=>36),
			array('timeStart, timeEnd', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('doctorId, companyId, addressId, timeStart, timeEnd', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'doctorId' => 'Doctor',
			'companyId' => 'Company',
			'addressId' => 'Address',
			'timeStart' => 'Time Start',
			'timeEnd' => 'Time End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('timeStart',$this->timeStart,true);
		$criteria->compare('timeEnd',$this->timeEnd,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}