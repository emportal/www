<?php

class AdminClinicForgotPasswordForm extends CFormModel
{
	public $username;
	
	public function rules()
	{
		return array(
			array('username', 'required'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'username'=>'Логин или e-mail',
		);
	}
	
	public function sendRecoveryMessage()
	{
		$users = UserMedical::model()->findAllByAttributes(["email" => $this->username]);
		
		if(empty($users)) {
			$users = UserMedical::model()->findAllByAttributes(["name" => $this->username]);
		}
		if(empty($users)) {
			$this->addError("username", "Аккаунта с указанными данными не найдено. Пожалуйста обратитесь в поддержку adm@emportal.ru");
		}
		elseif (count($users) > 1 OR empty($users[0]->email)) {
			$this->addError("username", "Произошла ошибка восстановления доступа. Пожалуйста обратитесь в поддержку adm@emportal.ru");
		}
		else {
			$mailLog = new LogVarious();
			$mailLog->attributes = [
				'type' => 'forgotPassword',
				'value' => json_encode([
					'addressId' => $users[0]->addressId,
				], JSON_UNESCAPED_UNICODE),
			];
			$mailLog->save();
			
			$users[0]->generateNewRecoveryKey();
		
			$mail = new YiiMailer();
			$mail->setView('recovery_password_adminClinic');
			$mail->setData(array(
				'link' => MyTools::url_origin($_SERVER, false).'/adminClinic/default/recovery?link='.$users[0]->link."&key=".$users[0]->recoveryKey,
			));
			$mail->render();
			$mail->From = 'robot@emportal.ru';
			$mail->FromName = Yii::app()->name;
			$mail->Subject = "Восстановление доступа к личному кабинету";

			$mail->AddAddress($users[0]->email);
	
			if ($users[0]->update("recoveryKey") && $mail->Send()) {
				Yii::app()->user->setFlash('sendRecoveryMessage',"На указанный email было отправленно письмо для восстановления доступа к личному кабинету!");
				$this->username = null;
				return true;
			} else {
				$this->addError("username", "Произошла ошибка восстановления доступа. Пожалуйста обратитесь в поддержку adm@emportal.ru");
			}
		}
		return false;
	}
}
