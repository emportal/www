<?php

/** 
 * This is the model class for table "sRights". 
 * 
 * The followings are the available columns in table 'sRights': 
 * @property integer $id
 * @property string $userId
 * @property integer $zRightId
 * @property string value
 */ 
class Right extends ActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sRights';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, zRightId', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userId, zRightId, value', 'safe', 'on'=>'search'),
			array('userId, zRightId, value', 'safe', 'on' => 'self_register'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'rightType' => array(self::BELONGS_TO, 'RightType', 'zRightId'),
		);
	}
	
	public function behaviors()
	{
		return array(
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('zRightId',$this->zRightId,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
}






