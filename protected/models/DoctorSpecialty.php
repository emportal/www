<?php

/**
 * This is the model class for table "doctorSpecialty".
 *
 * The followings are the available columns in table 'doctorSpecialty':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $description
 * @property string $companyActiviteId
 * @property string $seoCombination
 * @property integer $noindex
 *
 * The followings are the available model relations:
 * @property CompanyActivite $companyActivite
 * @property DoctorSpecialtyDisease[] $doctorSpecialtyDiseases
 * @property SpecialtyOfDoctor[] $specialtyOfDoctors
 * @property Doctor[] $doctors
 */
class DoctorSpecialty extends ActiveRecord {
	
	public $s_doctors;
	public static $Archived = TRUE;
	public $seoCombinationArray = NULL;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DoctorSpecialty the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'doctorSpecialty';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, companyActiviteId', 'length', 'max' => 36),
			array('name, link, tagH1, tagDescription, tagKeywords', 'length', 'max' => 150),
			array('seoText', 'length', 'max' => 12000),
			array('description', 'length', 'max' => 1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, description, companyActiviteId, seoCombination, noindex', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companyActivite' => array(self::BELONGS_TO, 'CompanyActivite', 'companyActiviteId'),
			'doctorSpecialtyDiseases' => array(self::HAS_MANY, 'DoctorSpecialtyDisease', 'doctorSpecialtyId'),
			'specialtyOfDoctors' => array(self::HAS_MANY, 'SpecialtyOfDoctor', 'doctorSpecialtyId'),
			'doctors' => array(self::HAS_MANY, 'Doctor', 'doctorId', 'through' => 'specialtyOfDoctors'),
			'agr_specialtyOfDoctors' => array(self::HAS_MANY, 'AgrSpecialtyOfDoctor', 'doctorSpecialtyId'),
			'agr_doctors' => array(self::HAS_MANY, 'AgrDoctor', 'doctorId', 'through' => 'agr_specialtyOfDoctors'),
			'shortSpecialtyOfDoctor' => [self::BELONGS_TO, 'ShortSpecialtyOfDoctor', 'shortSpecialtyId']
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Специальность',
			'link' => 'Link',
			'linkUrl' => 'ЧПУ урл',
			'noindex' => 'Скрыть в noindex',
			'description' => 'Описание',
			'companyActiviteId' => 'Профиль деятельности',
			'seoCombination' => 'Сео-тексты для комбинаций район/метро + специальность врача',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('companyActiviteId', $this->companyActiviteId, true);
		$criteria->compare('seoCombination', $this->seoCombination);
		$criteria->compare('noindex', $this->noindex);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Dative name
	 * Дательный падеж 
	 */
	public function getDatName() {
		return (!empty($this->dative) ? $this->dative : $this->name);
	}

	/**
	 * Genitive name
	 * Родительный падеж 
	 */
	public function getGenName() {
		return (!empty($this->genitive) ? $this->genitive : $this->name);
	}
	
	public function getDative() {
		return (!empty($this->dative) ? $this->dative : $this->name);
	}
	
	public function getPlural() {
		return (!empty($this->plural) ? $this->plural : $this->name);
	}

	
	public function count($condition='',$params=array()) {		
		$condition->addCondition("LOWER(" . $this->tableAlias . ".isArchive) <> '1'");
		
		return parent::count($condition,$params);
	}
	
	public function beforeFind() {
		parent::beforeFind();

		$criteria = $this->dbCriteria;
		if(!self::$Archived) $criteria->addCondition("LOWER(" . $this->tableAlias . ".isArchive) <> '1'");
		$this->dbCriteria = $criteria;
	}
	
	public function getParametersModel($cityId=City::DEFAULT_CITY) {
		return DoctorSpecialtyParameters::model()->findByAttributes(['doctorSpecialtyId'=>$this->id, 'cityId'=>$cityId]);
	}
	public function getParameter($name,$cityId=City::DEFAULT_CITY) {
		$parametersModel = $this->getParametersModel($cityId);
		if(is_object($parametersModel) && $parametersModel->hasAttribute($name)) {
			return $parametersModel->{$name};
		}
		if($this->hasAttribute($name) && $cityId === City::DEFAULT_CITY) {
			return $this->{$name};
		}
		return NULL;
	}
	public function setParameter($name,$value,$cityId=City::DEFAULT_CITY) {
		$parametersModel = $this->getParametersModel($cityId);
		if(!is_object($parametersModel)) {
			$parametersModel = new DoctorSpecialtyParameters; 
			$parametersModel->cityId = $cityId;
			$parametersModel->doctorSpecialtyId = $this->id;
		}
		if(is_object($parametersModel) && $parametersModel->hasAttribute($name)) {
			//echo " setParameter(".$name.",".$value.",".$cityId.");";
			if($this->hasAttribute($name) && $cityId === City::DEFAULT_CITY) {
				$this->{$name} = $value;
			}
			$parametersModel->{$name} = $value;
			return $parametersModel->save();
		}
	}

	public function getSeoCombinationArr() {
		if($this->seoCombinationArray === NULL && !Yii::app()->params["samozapis"]) {
			$this->seoCombinationArray = CJSON::decode($this->seoCombination);
			if($this->seoCombinationArray === NULL) {
				$this->seoCombinationArray = [];
			}
		}
		return $this->seoCombinationArray;
	}
	
	public function setSeoCombinationArr($value) {
		$this->seoCombinationArray = $value;
	}
	
	public function beforeSave() {
		if (parent::beforeSave()) {
			if(trim($this->companyActiviteId) == "") { $this->companyActiviteId = null; }
			if(is_array($this->seoCombinationArray) && !Yii::app()->params["samozapis"]) {
				$this->seoCombination = CJSON::encode($this->seoCombinationArray);
			}
			return true;
		} else {
			return false;
		}
	}
	
	public function behaviors() {
		return array(
			'linkUrlBehavior' => array(
				'class' => 'application.components.behaviors.LinkUrlBehavior',
			),
		);
	}

	// Альтернативные названия специальностей
	public static $specialitiesAndDuplicates = [
			"Акушер-гинеколог" => ["Акушер- гинеколог","АКУШ.-ГИНЕК.(ЖК39 пр.Просвещения,99)","АКУШЕР-ГИНЕК.ЖК N10 Кондратьевски25","АКУШЕР-ГИНЕКОЛ.","Акушерка смотрового кабинета","Акушерство и гинекология","врач-акушер-гинеколог","Врач-акушер-гинеколог (деж)"],
			"Аллерголог" => ["врач аллерголог","Аллергология","Аллергология и иммунология"],
			"Аллерголог-иммунолог" => ["Аллергология и иммунология","АЛЛЕРГОЛОГ/ИММ.","АЛЛЕРГОЛОГ/ИММУНОЛОГ","Врач-аллерголог-иммунолог"],
			"Врач общей практики" => ["Врач общей практики (семейный врач)","ВРАЧ ОБЩ.ПРАКТ","ВР.ОБЩ.ПРАКТИКИ","ВРАЧ ОБЩ. ПРАКТ.","ВРАЧ ОБЩ.ПРАКТ.(пр.Просвещения,99)","ВРАЧ ОБЩЕЙ ПР.","Общая врачебная практика","Общая практика"],
			"Врач УЗИ" => ["Врач-узи","Врач-узист","УЗИ","Врач ультразвуковой диагностики","Ультразвуковая диагностика"],
			"Гастроэнтеролог" => ["ГАСТРОЭНТЕР.ГП №123 Моравский д.5","Врач гастроэнтеролог","Врач-гастроэнтеролог","ГАСТРОЭНТЕРОЛОГ УЧЕБА ДО23.04.15","Гастроэнтерология"],
			"Гематолог" => ["врач гематолог","врач-гематолог","Гематология"],
			"Гериатр" => ["Гериатрия","ГЕРИАТР только для поликлиник №109,№5"],
			"Гинеколог" => ["Гинеколог ( ул. Шахматова д.12 к.4)","гин Уханова-отпуск, Котунова-б/л","Гинекология"],
			"Детский стоматолог" => ["ДЕТСКАЯ СТОМАТОЛОГИЯ","ДЕТСК.СТОМАТОЛОГ"],
			"Детский эндокринолог" => ["Врач-детский эндокринолог"],
			"Детский хирург" => ["Детский хирург каб. 339","Врач-детский хирург"],
			"Детский кардиолог" => ["Детский кардиолог каб. 212"],
			"Дерматолог" => ["ДЕРМАТОЛОГИЯ","ДЕРМАТОЛОГ (С 18 ЛЕТ)"],
			"Дерматовенеролог" => ["ДЕРМАТОВЕНЕРОЛОГ(платный прием)","Дерматовенерология","врач дермато-венеролог","Врач-дерматовенеролог"],
			"Инфекционист" => ["Врач-инфекционист"],
			"Кардиолог" => ["врач-кардиолог","врач кардиолог","Кардиология"],
			"Колопроктолог" => ["КОЛОПРОКТОЛОГ (Пискарёвский,12)","Колопроктология (проктолог)"],
			"Лабораторная диагностика" => ["Лаб. диагностика","Клиническая лабораторная диагностика"],
			"Логопед" => ["врач логопед","врач-логопед","Логопедия"],
			"Маммолог" => ["МАММОЛОГ (ЖК39 пр.Просвещения,99)"],
			"Невролог" => ["врач невролог","врач-невролог","Неврология","НЕВРОПАТОЛОГ","Врач-невролог ДС","НЕВРОПАТОЛОГкаф"],
			"Нефролог" => ["врач нефролог","врач-нефролог","нефрология","НЕФРОЛОГИ"],
			"Неонатолог" => ["Врач-неонатолог"],
			"Онколог-маммолог" => ["врач онколог-маммолог", "ОНКОЛОГ МАММОЛОГ"],
			"Онколог" => ["врач онколог","врач-онколог","Онкология","ОНКОЛОГ (Комсомола ,14)"],
			"Отоларинголог (ЛОР)" => ["Оториноларинголог","Оториноларингология","врач отоларинголог","врач-отоларинголог","ОТОЛАРИНГОЛОГ(запись для жит.Рыбацкое)","ЛОР (отоларинголог)","ОТОЛАРИНГОЛОГ каб. 138","ТОЛЬКО ДП30 лор"],
			"Ортопед" => ["врач ортопед","врач-ортопед","ортопедия","ОРТОПЕД каб. 238"],
			"Офтальмолог (окулист)" => ["врач офтальмолог","Врач-офтальмолог","Офтальмолог","Офтальмология","ОФТАЛЬМОЛОГ каб. 335","Окулист (офтальмолог)","окулист","врач окулист","врач-окулист"],
			"Психолог" => ["Врач психолог","Врач-психолог","психология"],
			"Педиатр" => ["Врач педиатр","Врач-педиатр","педиатрия"],
			"Педиатр участковый" => ["Врач-педиатр участковый","ПЕДИАТР УЧАСТ."],
			"Психотерапевт" => ["врач психотерапевт","врач-психотерапевт","ПСИХОТЕРАПИЯ"],
			"Психолог" => ["врач психолог","врач-психолог","Медицинский психолог"],
			"Пульмонолог" => ["Врач пульмонолог","Врач-пульмонолог","Пульмонология","ПУЛЬМОНОЛОГ только дпо 32","ПУЛЬМОНОЛОГ(Писк.12)","Пульмонология отп до 9 апр"],
			"Ревматолог" => ["врач ревматолог","врач-ревматолог","Ревматология"],
			"Рентгенолог" => ["Рентгенография","врач рентгенолог","врач-рентгенолог"],
			"Сердечно-сосудистый хирург" => ["C/C ХИРУРГ","СОСУДИСТЫЙ  ХИРУРГ"],
			"Стоматолог-пародонтолог" => ["Стоматолог-парадонтолог"],
			"Стоматолог-терапевт" => ["врач стоматолог-терапевт","Врач-стоматолог-терапевт","Стоматология терапевтическая","СТОМАТОЛОГ ТЕРАПЕВТ","СТОМ.-ТЕРАП"],
			"Стоматолог-хирург" => ["врач стоматолог-хирург","СТОМАТОЛОГ ХИРУРГ","СТОМ,-ХИРУРГ","СТОМАТОЛОГ ХИРУРГ ВЗРОСЛЫЙ","Стоматология хирургическая"],
			"Терапевт" => ["врач терапевт","врач-терапевт","ТЕРАПЕВТ медпроф.","ТЕРАПЕВТ ПОДР.","ТЕРАПЕВТ 3 ОТД.","ТЕРАПЕВТ 4 ОТД.","ТЕРАПЕВТЫ 2-го ОТДЕЛЕНИЯ","ТЕРАПЕВТЫ 1-го ОТДЕЛЕНИЯ","ТЕРАПЕВТ 1 ТО"],
			"Травматолог-ортопед" => ["Врач травматолог-ортопед","Врач-травматолог-ортопед","Врач-травматолог-ортопед ТП","ортопед-травматолог","ТРАВМ/ОРТОПЕД","ТРАВМАТОЛ/ОРТОП","ТРАВМАТОЛОГ - ОРТОПЕД","Травматолог-ортопед (амб.)","Травматология и ортопедия"],
			"Терапевт участковый" => ["Врач терапевт участковый","Врач-терапевт участковый","ТЕРАПЕВТ УЧАСТ."],
			"Уролог" => ["врач уролог","врач-уролог","Урология"],
			"Физиотерапевт" => ["Врач физиотерапевт","Врач-физиотерапевт","физиотерапия"],
			"Фтизиатр" => ["врач Фтизиатр","врач-Фтизиатр","Фтизиатрия"],
			"Хирург" => ["врач хирург","врач-хирург","Хирургия","ХИРУРГ-общ.хир."],
			"Эндокринолог" => ["врач эндокринолог","врач-эндокринолог","Эндокринология"],
	];
}
