<?php

/**
 * This is the model class for table "companySalesInfo".
 *
 * The followings are the available columns in table 'companySalesInfo':
 * @property integer $id
 * @property string $companyId
 * @property string $lprName
 * @property string $lprPosition
 * @property string $lprPhone
 * @property string $poType
 * @property string $managerComment
 * @property string $salesCategory
 *
 * The followings are the available model relations:
 * @property Company $company
 */
class CompanySalesInfo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanySalesInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companySalesInfo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('companyId', 'required'),
			array('companyId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('companyId, lprName, lprPosition, lprPhone, poType, managerComment, salesCategory, email, website', 'safe'),
			array('id, companyId, lprName, lprPosition, lprPhone, poType, managerComment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'companyId' => 'Company',
			'lprName' => 'ЛПР',
			'lprPosition' => 'Должность ЛПР',
			'lprPhone' => 'Телефон ЛПР',
			'poType' => 'Используемое ПО',
			'managerComment' => 'Комментарий',
			'salesCategory' => 'Категория клиента'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('lprName',$this->lprName,true);
		$criteria->compare('lprPosition',$this->lprPosition,true);
		$criteria->compare('lprPhone',$this->lprPhone,true);
		$criteria->compare('poType',$this->poType,true);
		$criteria->compare('managerComment',$this->managerComment,true);
		$criteria->compare('salesCategory',$this->salesCategory,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}