<?php

/**
 * This is the model class for table "service".
 *
 * The followings are the available columns in table 'service':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $fullName
 * @property string $serviceSectionId
 * @property string $description
 * @property integer $forChildren
 * @property integer $forAdult
 * @property string $companyActiviteId
 * @property string $seoCombination
 * @property integer $empComissionAsAppointment
 *
 * The followings are the available model relations:
 * @property Action[] $actions
 * @property AddressServices[] $addressServices
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property CompanyServices[] $companyServices
 * @property DoctorServices[] $doctorServices
 * @property PriceNomenclature[] $priceNomenclatures
 * @property ServiceSection $serviceSection
 * @property ClassifierUnit $classifierUnit
 * @property CompanyActivite $companyActivite
 */
class Service extends ActiveRecord
{
	public $seoCombinationArray = NULL;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Service the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'service';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			['name','unique'],
			array('name, fullName, companyActiviteId, serviceSectionId, description', 'required'),
			array('link','unique'),
			array('link', 'length', 'min' => 9, 'max'=>150),
			array(
	            'link',
	            'match', 'not' => true, 'pattern' => '/[^a-z0-9_-]/',
	            'message' => 'Ссылка должна содержать только цифры и латинские буквы',
        	),
			array('forChildren, forAdult, empComissionAsAppointment', 'numerical', 'integerOnly'=>true),
			array('id, serviceSectionId, companyActiviteId', 'length', 'max'=>36),
			array('name, fullName', 'length', 'max'=>250),
            array('description', 'length', 'max'=>750),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, fullName, serviceSectionId, description, forChildren, forAdult, companyActiviteId, empComissionAsAppointment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'actions' => array(self::HAS_MANY, 'Action', 'serviceId'),
			'addressServices' => array(self::HAS_MANY, 'AddressServices', 'serviceId'),
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'serviceId'),
			'companyServices' => array(self::HAS_MANY, 'CompanyServices', 'serviceId'),
			'doctorServices' => array(self::HAS_MANY, 'DoctorServices', 'cassifierServiceId'),
			'priceNomenclatures' => array(self::HAS_MANY, 'PriceNomenclature', 'serviceId'),
			'serviceSection' => array(self::BELONGS_TO, 'ServiceSection', 'serviceSectionId'),
			'companyActivite' => array(self::BELONGS_TO, 'CompanyActivite', 'companyActiviteId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название услуги',
			'link' => 'Ссылка (только латиница, без пробелов)',
			'linkUrl' => 'ЧПУ урл',
			'fullName' => 'Полное название',
			'serviceSectionId' => 'Категория',
			'description' => 'Описание',
			'forChildren' => 'Услуга для детей',
			'forAdult' => 'Консультация является консультацией',
			'companyActiviteId' => 'Профиль деятельности',
			'seoCombination' => 'Сео-тексты для комбинаций район/метро + специальность врача',
			'empComissionAsAppointment' => 'Считается как первичный прием',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('fullName',$this->fullName,true);
		$criteria->compare('serviceSectionId',$this->serviceSectionId,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('forChildren',$this->forChildren);
		$criteria->compare('forAdult',$this->forAdult);
		$criteria->compare('companyActiviteId',$this->companyActiviteId,true);
		$criteria->compare('empComissionAsAppointment', $this->empComissionAsAppointment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function behaviors() {
		return array(
				'linkUrlBehavior' => array(
						'class' => 'application.components.behaviors.LinkUrlBehavior',
				)
		);
	}
	
	public function getParametersModel($cityId=City::DEFAULT_CITY) {
		return ServiceParameters::model()->findByAttributes(['serviceId'=>$this->id, 'cityId'=>$cityId]);
	}
	public function getParameter($name,$cityId=City::DEFAULT_CITY) {
		$parametersModel = $this->getParametersModel($cityId);
		if(is_object($parametersModel) && $parametersModel->hasAttribute($name)) {
			return $parametersModel->{$name};
		}
		if($this->hasAttribute($name) && $cityId === City::DEFAULT_CITY) {
			return $this->{$name};
		}
		return NULL;
	}
	public function setParameter($name,$value,$cityId=City::DEFAULT_CITY) {
		$parametersModel = $this->getParametersModel($cityId);
		if(!is_object($parametersModel)) {
			$parametersModel = new ServiceParameters; 
			$parametersModel->cityId = $cityId;
			$parametersModel->serviceId = $this->id;
		}
		if(is_object($parametersModel) && $parametersModel->hasAttribute($name)) {
			//echo " setParameter(".$name.",".$value.",".$cityId.");";
			if($this->hasAttribute($name) && $cityId === City::DEFAULT_CITY) {
				$this->{$name} = $value;
			}
			$parametersModel->{$name} = $value;
			return $parametersModel->save();
		}
	}

	public function getSeoCombinationArr() {
		if($this->seoCombinationArray === NULL) {
			$this->seoCombinationArray = CJSON::decode($this->seoCombination);
			if($this->seoCombinationArray === NULL) {
				$this->seoCombinationArray = [];
			}
		}
		return $this->seoCombinationArray;
	}
	
	public function setSeoCombinationArr($value) {
		$this->seoCombinationArray = $value;
	}
	
	public function beforeValidate() {
		return parent::beforeValidate();
	}
	
	public function beforeSave() {
		if (parent::beforeSave()) {
			if(is_array($this->seoCombinationArray)) {
				$this->seoCombination = CJSON::encode($this->seoCombinationArray);
			}
			return true;
		} else {
			return false;
		}
	}
}