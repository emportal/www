<?php

/**
 * This is the model class for table "sex".
 *
 * The followings are the available columns in table 'sex':
 * @property string $id
 * @property integer $order
 * @property string $name
 *
 * The followings are the available model relations:
 * @property ContactPersonCompany[] $contactPersonCompanies
 * @property Doctor[] $doctors
 * @property PhysicalPerson[] $physicalPeople
 */
class Sex extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sex the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sex';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>36),
			array('name', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, order, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactPersonCompanies' => array(self::HAS_MANY, 'ContactPersonCompany', 'sexId'),
			'doctors' => array(self::HAS_MANY, 'Doctor', 'sexId'),
			'physicalPeople' => array(self::HAS_MANY, 'PhysicalPerson', 'sexId'),
			'users'			=> array(self::HAS_MANY,'User','sexId')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order' => 'Order',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function scopes() {
		return [
			'male' => [
				'condition' => "LOWER(name) = 'мужской'",
			],
			'female' => [
				'condition' => "LOWER(name) = 'женский'",
			]
		];
	}
	
	public static function getDefaultValue()
	{
		return 'ae11c45d855a991340c5f59edcb404da'; //Нет данных
	}
	
	public static function apiIdentifier($id) {
		return ($id == "81160944c16a392048918e487f6c328a" ? 1 : ($id == "b4a45cdfcb1a97ee4ba9baf3c3013f69" ? 0 : ""));
	}
}