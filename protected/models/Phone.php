<?php

/**
 * This is the model class for table "phone".
 *
 * The followings are the available columns in table 'phone':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $ownerId
 * @property integer $general
 * @property string $addressId
 * @property string $countryCode
 * @property string $cityCode
 * @property string $number
 * @property string $typeId
 *
 * The followings are the available model relations:
 * @property PhoneType $type
 * @property PhoneCompanyUnit[] $phoneCompanyUnits
 */
class Phone extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Phone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'phone';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, countryCode, cityCode, number, link', 'required'),
			array('name, link', 'unique'),
			array('general', 'numerical', 'integerOnly'=>true),
			array('id, addressId, typeId', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			array('countryCode', 'length', 'max'=>3),
			array('cityCode', 'length', 'max'=>10),
			array('number', 'length', 'max'=>15),
			//array('number, cityCode, countryCode', 'numerical', 'integerOnly'=>true),
			array('ownerId', 'safe'),
			//array('name, countryCode, cityCode, number', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, ownerId, general, addressId, countryCode, cityCode, number, typeId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => [self::BELONGS_TO,'Address','addressId'],
			'type' => [self::BELONGS_TO, 'PhoneType', 'typeId'],
			'phoneCompanyUnits' => [self::HAS_MANY, 'PhoneCompanyUnit', 'phoneId'],
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Номер телефона',
			'link' => 'Link',
			'ownerId' => 'Owner',
			'general' => 'General',
			'addressId' => 'Address',
			'countryCode' => 'Country Code',
			'cityCode' => 'City Code',
			'number' => 'Number',
			'typeId' => 'Type',
		);
	}

	public function beforeSave() {
		if (parent::beforeSave()) {
			if($this->addressId == "") { $this->addressId = null; }
			$this->link = str_pad(intval(microtime(true)*1000), 9, '0', STR_PAD_LEFT);
			if($this->typeId == "") { $this->typeId = null; }
			$this->name = $this->countryCode . " " . $this->cityCode . " " . $this->number;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('ownerId',$this->ownerId,true);
		$criteria->compare('general',$this->general);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('countryCode',$this->countryCode,true);
		$criteria->compare('cityCode',$this->cityCode,true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('typeId',$this->typeId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}