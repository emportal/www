<?php

/**
 * This is the model class for table "producerBrand".
 *
 * The followings are the available columns in table 'producerBrand':
 * @property string $id
 * @property string $producerId
 * @property string $brandId
 * @property string $nomenclatureGroupId
 * @property string $refId
 *
 * The followings are the available model relations:
 * @property Ref $ref
 * @property Brand $brand
 * @property NomenclatureGroup $nomenclatureGroup
 * @property Producer $producer
 */
class ProducerBrand extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProducerBrand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producerBrand';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, producerId, brandId, nomenclatureGroupId, refId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, producerId, brandId, nomenclatureGroupId, refId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ref' => array(self::BELONGS_TO, 'Ref', 'refId'),
			'brand' => array(self::BELONGS_TO, 'Brand', 'brandId'),
			'nomenclatureGroup' => array(self::BELONGS_TO, 'NomenclatureGroup', 'nomenclatureGroupId'),
			'producer' => array(self::BELONGS_TO, 'Producer', 'producerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'producerId' => 'Producer',
			'brandId' => 'Brand',
			'nomenclatureGroupId' => 'Nomenclature Group',
			'refId' => 'Ref',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('producerId',$this->producerId,true);
		$criteria->compare('brandId',$this->brandId,true);
		$criteria->compare('nomenclatureGroupId',$this->nomenclatureGroupId,true);
		$criteria->compare('refId',$this->refId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}