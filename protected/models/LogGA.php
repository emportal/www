<?php

/**
 * This is the model class for table "logGA".
 *
 * The followings are the available columns in table 'logGA':
 * @property integer $id
 * @property string $appointmentToDoctorsId
 * @property string $date
 * @property string $time
 * @property string $timestamp
 * @property string $PaymentTransactionId
 * @property string $clientId
 * @property string $userId
 * @property string $PaymentValue
 * @property string $GArequest
 */
class LogGA extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logGA';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('appointmentToDoctorsId, date, time, timestamp, PaymentTransactionId, clientId, userId, PaymentValue, GArequest', 'required'),
			array('appointmentToDoctorsId, timestamp, PaymentTransactionId', 'length', 'max'=>50),
			array('clientId, userId', 'length', 'max'=>128),
			array('PaymentValue', 'length', 'max'=>20),
			array('GArequest', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, appointmentToDoctorsId, date, time, timestamp, PaymentTransactionId, clientId, userId, PaymentValue, GArequest', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'appointmentToDoctorsId' => 'Appointment To Doctors',
			'date' => 'date',
			'time' => 'Time',
			'timestamp' => 'Timestamp',
			'PaymentTransactionId' => 'Payment Transaction',
			'clientId' => 'Client',
			'userId' => 'User',
			'PaymentValue' => 'Payment Value',
			'GArequest' => 'Garequest',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('appointmentToDoctorsId',$this->appointmentToDoctorsId,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('PaymentTransactionId',$this->PaymentTransactionId,true);
		$criteria->compare('clientId',$this->clientId,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('PaymentValue',$this->PaymentValue,true);
		$criteria->compare('GArequest',$this->GArequest,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogGA the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
