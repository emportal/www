<?php

/**
 * This is the model class for table "newsReference".
 *
 * The followings are the available columns in table 'newsReference':
 * @property string $id
 * @property string $newsId
 * @property string $entityId
 * @property integer $type
 *
 * The followings are the available model relations:
 * @property News $news
 */
class NewsReference extends ActiveRecord
{
	const TYPE_COMPANY = 0;
	const TYPE_ADDRESS = 1;
	const TYPE_DOCTOR = 2;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NewsReference the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'newsReference';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('newsId, entityId, type', 'required'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('id, newsId, entityId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, newsId, entityId, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'news' => array(self::BELONGS_TO, 'News', 'newsId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'newsId' => 'News',
			'entityId' => 'Entity',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('newsId',$this->newsId,true);
		$criteria->compare('entityId',$this->entityId,true);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function findByReferenceType($referenceType,$attributes=null) {
		$criteria = new CDbCriteria();
		$criteria->compare('type',$referenceType);
		if(is_array($attributes)) {
			foreach ($attributes as $key=>$value) {
				$criteria->compare($key,$value);
			}
		}
		return $this->findAll($criteria);
	}
}