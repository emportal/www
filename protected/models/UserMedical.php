<?php

/**
 * This is the model class for table "userMedical".
 *
 * The followings are the available columns in table 'userMedical':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $password
 * @property string $email
 * @property string $addressId
 * @property string $companyId
 * @property integer $status
 * @property string $recoveryKey
 * @property integer $feedZoon
 * @property integer $loginSkype
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Address $address
 */
class UserMedical extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserMedical the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userMedical';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, feedZoon', 'numerical', 'integerOnly'=>true),
			array('id, addressId, companyId', 'length', 'max'=>36),
			array('phoneForAlert', 'length', 'max'=>15),
			array('name, link, password, email', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, password, email, addressId, companyId, phoneForAlert, status, agreement, agreementNew, createDate, commonRegistry, feedZoon', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'password' => 'Password',
			'email' => 'Email',
			'addressId' => 'Address',
			'companyId' => 'Company',
			'phoneForAlert' => 'Phone for alert',
			'status' => 'Status',
			'commonRegistry' => 'commonRegistry',
			'agreement' => 'Agreement',
			'agreementNew' => 'AgreementNew',
			'createDate' => 'CreateDate',
			'recoveryKey' => 'Ключь для смены пароля',
			'feedZoon' => 'Выгружать в zoon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('phoneForAlert',$this->companyId,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('agreement',$this->Agreement,true);
		$criteria->compare('agreementNew',$this->AgreementNew,true);
		$criteria->compare('createDate',$this->CreateDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function setNewPassword($password) {
		$this->password = strtolower(md5($password));
	}
	
	public function validatePassword($password) {
		/*if(strlen($this->password) > 20) {
			return strtolower(md5($password)) === strtolower($this->password);
		} else {
			return $password === $this->password;
		}*/
		return strtolower(md5($password)) === strtolower($this->password);
	}
	
	public function generateNewRecoveryKey() {
		$this->recoveryKey = RandomKey::generate(32);
	}
}