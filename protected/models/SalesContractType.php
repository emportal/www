<?php

/**
 * This is the model class for table "salesContractType".
 *
 * The followings are the available columns in table 'salesContractType':
 * @property integer $id
 * @property integer $price
 * @property integer $isFixed
 * @property integer $doctorsLimit
 * @property string $name
 * @property string $description
 *
 * The followings are the available model relations:
 * @property SalesContract[] $salesContracts
 * @property TariffOptionType[] $tariffOptionTypes
 */
class SalesContractType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SalesContractType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesContractType';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description', 'required'),
			array('price, isFixed, doctorsLimit', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, price, isFixed, doctorsLimit, description, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'salesContracts' => array(self::HAS_MANY, 'SalesContract', 'salesContractTypeId'),
			'tariffOptionTypes' => array(self::MANY_MANY, 'TariffOptionType', 'TariffOptionTypeOfSalesContractType(salesContractTypeId, tariffOptionTypeId)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'price' => 'Price',
			'isFixed' => 'Is Fixed',
			'doctorsLimit' => 'Doctors Limit',
			'name' => 'Name',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('price',$this->price);
		$criteria->compare('isFixed',$this->isFixed);
		$criteria->compare('doctorsLimit',$this->doctorsLimit);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function hasTariffOption($tariffOptionTypeId) {
		foreach ($this->tariffOptionTypes as $type) {
			if($type->id === $tariffOptionTypeId ){
				return true;
				break;
			}
		}
		return false;
	}
}