<?php

/**
 * This is the model class for table "user1C".
 *
 * The followings are the available columns in table 'user1C':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $password
 * @property string $companyId
 * @property string $telefon
 * @property string $emailSignature
 *
 * The followings are the available model relations:
 * @property ContactInformation[] $contactInformations
 * @property Company $company
 */
class User1C extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User1C the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user1C';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, companyId', 'length', 'max'=>36),
			array('name, link, password, telefon, emailSignature', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, password, companyId, telefon, emailSignature', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactInformations' => array(self::HAS_MANY, 'ContactInformation', 'user1CId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'password' => 'Password',
			'companyId' => 'Company',
			'telefon' => 'Telefon',
			'emailSignature' => 'Email Signature',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('telefon',$this->telefon,true);
		$criteria->compare('emailSignature',$this->emailSignature,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}