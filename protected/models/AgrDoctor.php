<?php

/**
 * This is the model class for table "doctor".
 *
 * The followings are the available columns in table 'doctor':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $surName
 * @property string $firstName
 * @property string $fatherName
 * @property string $birthday
 * @property string $sexId
 * @property string $experience
 * @property string $experienceDate
 * @property integer $experiencePeriod
 * @property string $staffUnitId
 * @property integer $onHouse
 * @property string $currentPlaceOfWorkId
 * @property integer $rating
 * @property string $scientificTitleId
 * @property integer $hasPhoto
 *
 * The followings are the available model relations:
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property Comment[] $comments
 * @property ScientificTitle $scientificTitle
 * @property PlaceOfWork $currentPlaceOfWork
 * @property Sex $sex
 * @property StaffUnit $staffUnit
 * @property DoctorEducation[] $doctorEducations
 * @property DoctorScientificDegree[] $doctorScientificDegrees
 * @property ScientificDegree[] $scientificDegrees
 * @property DoctorServices[] $doctorServices
 * @property Phone[] $phones
 * @property PlaceOfWork[] $placeOfWorks
 * @property Ref[] $refs
 * @property SpecialtyOfDoctor[] $specialtyOfDoctors
 * @property WorkingHoursOfDoctors[] $workingHoursOfDoctors
 * @property DoctorSpecialty[] $specialties
 */
class AgrDoctor extends AgrActiveRecord {

	public $doctorSpecialty;
	protected $_photo;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrDoctor the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'agr_doctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('surName','isAlreadyInClinic', 'on' => 'insert'),
			array('description','filter','filter'=>function($v){ return strip_tags($v);}),
			array('description','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('description','length', 'max' => 5000),
			array('surName, firstName, sexId', 'required'),
			array('experiencePeriod, onHouse', 'numerical', 'integerOnly' => true),
			array('rating,ratingSystem,ratingUsers', 'numerical'),
			array('rating,ratingSystem,ratingUsers', 'default', 'value' => 0),
			array('id, sexId, staffUnitId, currentPlaceOfWorkId, scientificTitleId', 'length', 'max' => 36),
			array('name, link, surName, firstName, fatherName, experience', 'length', 'max' => 150),
			array('experience', 'numerical', 'integerOnly' => true, 'min' => 1930, 'tooSmall' => 'Неверная дата'),
			array('birthday, experienceDate, hasPhoto', 'safe'),
			array(
				'photo',
				'file',
				'types' => 'jpg, gif, png',
				'maxSize' => 1024 * 1024 * 5, // 5MB
				'allowEmpty' => 'true',
				'tooLarge' => 'The file was larger than 5MB. Please upload a smaller file.',
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, surName, firstName, fatherName, birthday, sexId, experience, experienceDate, experiencePeriod, staffUnitId, onHouse, currentPlaceOfWork, rating, scientificTitleId, hasPhoto', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appointmentCount' => array(self::STAT, 'AppointmentToDoctors', 'doctorId', 'condition' => 't.statusId=' . AppointmentToDoctors::VISITED),
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'doctorId'),
			'comments' => array(self::HAS_MANY, 'Comment', 'addressId', 'scopes' => 'moderated'),
			'commentsAll' => array(self::HAS_MANY, 'Comment', 'addressId'),
			//'currentPlaceOfWork' => array(self::BELONGS_TO, 'Company', 'currentPlaceOfWorkId'),
			'currentPlaceOfWork' => array(self::HAS_ONE, 'AgrPlaceOfWork', 'doctorId', 'scopes' => 'current'),
			'sex' => array(self::BELONGS_TO, 'Sex', 'sexId'),
			'staffUnit' => array(self::BELONGS_TO, 'StaffUnit', 'staffUnitId'),
			'doctorEducations' => array(self::HAS_MANY, 'AgrDoctorEducation', 'doctorId', 'with' => 'typeOfEducation', 'order' => 'typeOfEducation.order'),
			'doctorScientificDegrees' => array(self::HAS_MANY, 'DoctorScientificDegree', 'doctorId'),
			'scientificTitle' => array(self::BELONGS_TO, 'ScientificTitle', 'scientificTitleId'),
			'scientificDegrees' => array(self::MANY_MANY, 'ScientificDegree', 'agr_doctorScientificDegree(doctorId,scientificDegreeId)'),
			'doctorServices' => array(self::HAS_MANY, 'DoctorServices', 'doctorId'),
			'phones' => array(self::HAS_MANY, 'Phone', 'ownerId'),
			'placeOfWorks' => array(self::HAS_MANY, 'PlaceOfWork', 'doctorId'),
			//'address'					 => array(self::HAS_ONE, 'Address', 'addressId', 'through' => 'currentPlaceOfWork'),
			'refs' => array(self::HAS_MANY, 'Ref', 'ownerId'),
			'specialtyOfDoctors' => array(self::HAS_MANY, 'AgrSpecialtyOfDoctor', 'doctorId'),
			'specialties' => array(self::HAS_MANY, 'DoctorSpecialty', 'doctorSpecialtyId', 'through' => 'specialtyOfDoctors'),
			'workingHoursOfDoctors' => array(self::HAS_MANY, 'WorkingHoursOfDoctors', 'doctorId'),
				//'scientificDegreesCount'	 => array(self::STAT, 'ScientificDegree', 'doctorScientificDegree(doctorId,scientificDegreeId)'),
				//'cityDistrict'				 => array(self::HAS_ONE, 'CityDistrict', 'cityDistrictId', 'through'	 => 'address'),
				//'nearestMetroStations'		 => array(self::HAS_MANY, 'NearestMetroStation', array('id'=>'addressId'), 'through'	 => 'address'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'ФИО',
			'link' => 'Link',
			'surName' => 'Фамилия',
			'firstName' => 'Имя',
			'fatherName' => 'Отчество',
			'birthday' => 'Дата рождения',
			'sexId' => 'Пол',
			'description' => 'Текст',
			'experience' => 'Стаж работы',
			'experienceDate' => 'Стаж работы',
			'experiencePeriod' => 'Experience Period',
			'staffUnitId' => 'Staff Unit',
			'onHouse' => 'Выезд врача на дом',
			'currentPlaceOfWorkId' => 'Current Place Of Work',
			'rating' => 'Rating',
			'scientificTitle' => 'Ученое звание',
			'scientificTitleId' => 'Ученое звание',
			'scientificDegrees' => 'Ученая степень',
			'hasPhoto' => 'Наличие фотографии',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($clinic) {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->with = array(
			'sex',
			'currentPlaceOfWork',
			'currentPlaceOfWork.address',
			'specialties' => array(
				'select' => 'specialties.name',
				'together' => 'true'
			),
		);
		$criteria->group = 't.name';
		$criteria->condition = "`t`.`CurrentPlaceOfWorkId` IS NOT NULL";
		$criteria->compare('`address`.`id`', $clinic->id);

		$criteria->compare('`t`.`id`', $this->id, true);
		$criteria->compare('`t`.`name`', $this->name, true);
// 		$criteria->compare('link', $this->link, true);
		$criteria->compare('`t`.`surName`', $this->surName, true);
		$criteria->compare('`t`.`firstName`', $this->firstName, true);
		$criteria->compare('`t`.`fatherName`', $this->fatherName, true);
		$criteria->compare('`t`.`birthday`', $this->birthday, true);
		$criteria->compare('`sex`.`name`', $this->sexId, true);
		$criteria->compare('`t`.`experience`', $this->experience, true);
		$criteria->compare('`t`.`experienceDate`', $this->experienceDate, true);
		$criteria->compare('`t`.`experiencePeriod`', $this->experiencePeriod);
		$criteria->compare('`t`.`staffUnitId`', $this->staffUnitId, true);
		$criteria->compare('`t`.`onHouse`', $this->onHouse);
		$criteria->compare('`t`.`currentPlaceOfWorkId`', $this->currentPlaceOfWorkId, true);
		$criteria->compare('`t`.`rating`', $this->rating);
		$criteria->compare('`t`.`scientificTitleId`', $this->scientificTitleId, true);
		$criteria->compare('`specialties`.`name`', $this->doctorSpecialty->name, true);



		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
			'sort' => array(
				'attributes' => array(
					'specialties.name', '*'
				),
				'defaultOrder' => 't.name ASC',
			),
		));
	}

	function yearTextArg($year) {
		$year = abs($year);
		$t1 = $year % 10;
		$t2 = $year % 100;
		return ($t1 == 1 && $t2 != 11 ? "год" : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? "года" : "лет"));
	}

	function getExperience() {
		$year = (intval($this->experience) > 1900 ) ? (date("Y") - intval($this->experience)) : intval($this->experience);
		return ($year > 0) ? $year . ' ' . $this->yearTextArg($year) : 0;
	}

	public function getIsFavorite() {
		if (Yii::app()->user->isGuest)
			return false;

		return in_array($this->id, Yii::app()->user->model->favoritesArray);
	}

	public function getFio() {
		return implode('.', array_map(function ($var) {
							return mb_substr($var, 0, 1);
						}, array(
					$this->surName,
					$this->firstName,
					$this->fatherName
								)
				)) . '.';
	}

	public function getShortName() {
		return sprintf('%s %s.%s.', $this->surName, mb_substr($this->firstName, 0, 1), mb_substr($this->fatherName, 0, 1));
	}

	public function setSpecialtyOfDoctors($data) {
		if (!is_array($data)) {
			$data = $data ? array($data) : array();
		}

		foreach ($data as $key => &$specialty) {
			if (is_object($specialty))
				continue;

			if (is_array($specialty)) {
				if (!$object = AgrSpecialtyOfDoctor::model()->findByPk($specialty['id'])) {
					$object = new AgrSpecialtyOfDoctor();
					$object->doctorId = $this->id;
				}
				$object->attributes = $specialty;
				$object->id = ActiveRecord::create_guid(array(
							$object->doctorId,
							$object->doctorSpecialtyId,
							$object->doctorCategoryId)
				);

				if (!$object->save()) {
					unset($data[$key]);
					continue;
				}
				$specialty = $object;
			} else {
				$specialty = AgrSpecialtyOfDoctor::model()->findByPk($specialty);
			}
		}

		$this->specialtyOfDoctors = $data;
		$this->save();
	}

	public function setDoctorEducations($data) {
		if (!is_array($data))
			$data = $data ? array($data) : array();

		foreach ($data as $key => &$education) {
			if (is_object($education))
				continue;

			if (is_array($education)) {
				if (!$object = AgrDoctorEducation::model()->findByPk($education['id'])) {
					$object = new AgrDoctorEducation();
					$object->doctorId = $this->id;
				}
				$object->attributes = $education;
				$object->id = ActiveRecord::create_guid(array(
							$object->doctorId,
							$object->typeOfEducationId,
							$object->medicalSchoolId,
							$object->yearOfStady
				));

				if (!$object->save()) {
					unset($data[$key]);
					continue;
				}
				$education = $object;
			} else {
				$education = AgrSpecialtyOfDoctor::model()->findByPk($education);
			}
		}
		$this->doctorEducations = $data;
		$this->save();
		$models = AgrDoctorEducation::model()->findAll("doctorId IS NULL");
		foreach ($models as $md) {
			$md->delete();
		}
		#AgrDoctorEducation::model()->deleteAll("doctorId IS NULL");
	}

	public function setCurrentPlaceOfWork(Address $address) {

		if ($this->currentPlaceOfWork->addressId === $address->id)
			return;

		$model = new AgrPlaceOfWork();

		$criteria = new CDbCriteria();
		$criteria->compare('doctorId', $this->id);

		AgrPlaceOfWork::model()->updateAll(array('current' => 0), $criteria);

		$model->address = $address;
		$model->doctor = $this;
		$model->company = $address->company;
		$model->current = 1;
		$model->dateStart = Yii::app()->dateFormatter->formatDateTime(time());

		$model->save();

		if (!empty($model->errors))
			Yii::log('errors: ' . print_r($model->errors, true), CLogger::LEVEL_ERROR, 'adminClinic.experts');

		$this->currentPlaceOfWorkId = $address->company->id;
		$this->save();
	}

	public function getUploadPath() {

		$path = Yii::getPathOfAlias('uploads.doctor') . '/' . $this->link;

		if (!is_dir($path))
			mkdir($path, 0775, true);
		return $path;
	}

	public function getUploadUrl() {

		return '/uploads/doctor/' . $this->link;
	}

	public function getPhotoPath() {
		if ( is_file("{$this->uploadPath}/photo.jpg") ) {
			return "{$this->uploadPath}/photo.jpg";
		}
		elseif ( is_file("{$this->uploadPath}/photo.png") ) {
			return "{$this->uploadPath}/photo.png";
		}
		return false;
	}

	public function getPhoto() {

		if (!isset($this->_photo))
			$this->_photo = is_file($this->photoPath) ? "photo.jpg?z=" . rand() : false;

		return $this->_photo;
	}

	public function setPhoto($value) {
		$this->_photo = $value;
	}

	public function getPhotoUrl() {
		return $this->photo ? "{$this->uploadUrl}/$this->photo" : false;
	}

	public function delete() {
		
		return parent:: delete();
	}

	//Первоначальное обращение
	public function getInspectPrice() {

		$id = Service::model()->find("link='first'")->id;
		//echo $id;
		$model = DoctorServices::model()->find("doctorId=:dId AND cassifierServiceId=:sId", array(
			'dId' => $this->id,
			'sId' => $id
		));
		return $model->price;
		//return $this->doctorServices->find('cassifierService='.$id)->price;
	}

	public function getExperienceNum() {
		if ((int) $this->experience)
			return (int) date('Y') - (int) $this->experience;
		return '';
	}
	
	public function isAlreadyInClinic($attr) {
		if($this->firstName && $this->surName && $this->fatherName) {
			$criteria = new CDbCriteria();
			$criteria->select = 't.id,t.firstName,t.surName,t.fatherName';
			$criteria->with = [
				'placeOfWorks' => [
					'together' => true,
					'select' => false,
					'with' => [
						'company'=> [
							'select' => false,
							'together' => true,
						]
					]
				]
			];		
			$criteria->compare('company.id',Yii::app()->user->model->companyId);
			$criteria->compare('t.firstName',$this->firstName);
			$criteria->compare('t.surName',$this->surName);
			$criteria->compare('t.fatherName',$this->fatherName);
			$agrModel = AgrDoctor::model()->find($criteria);		
			$model = Doctor::model()->find($criteria);			
			if($agrModel || $model) {			
				$this->addError($attr,'Текущий врач добавлен вами в другом адресе, воспользуйтесь функцией импорта');
			}
		}
	}
}
