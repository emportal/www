<?php

/**
 * This is the model class for table "logVarious".
 *
 * The followings are the available columns in table 'logVarious':
 * @property integer $id
 * @property string $type
 * @property string $value
 * @property string $createdDate
 */
class LogVarious extends CActiveRecord
{
	const TYPE_SMS = 'sms';
	const TYPE_USER_BALANCE = 'user_balance';
    const TYPE_PROMOCODE = 'promocode';
    const TYPE_DEBT_CLINIC_DEACTIVATION = 'debt_clinic_deactivation';
    const TYPE_REFERRAL_APPOINTMENT = 'referralAppointment';
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogVarious the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logVarious';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, value', 'required'),
			array('type', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, value, createdDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'value' => 'Value',
			'createdDate' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('createdDate',$this->createdDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord)
				$this->createdDate = new CDbExpression('NOW()');
			return true;
		}
	}
}