<?php

/**
 * This is the model class for table "agr_company".
 *
 * The followings are the available columns in table 'agr_company':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $nameRUS
 * @property string $nameENG
 * @property string $description
 * @property string $categoryId
 * @property string $companyTypeId
 * @property integer $isPaid
 * @property integer $isFree
 * @property integer $IsOMS
 * @property integer $IsDMS
 * @property integer $doctorsOnHouse
 * @property integer $dateOpening
 * @property integer $averageCheck
 * @property string $addressId
 * @property string $INN
 * @property string $KPP
 * @property string $OGRN
 * @property string $OKPO
 * @property integer $ratingSystem
 * @property integer $ratingUser
 * @property integer $ratingSite
 */
class AgrCompany extends AgrActiveRecord
{
	
	private $_logo;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agr_company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isPaid, isFree, IsOMS, IsDMS, doctorsOnHouse, dateOpening, averageCheck', 'numerical', 'integerOnly' => true),
			array('id, categoryId, companyTypeId, addressId', 'length', 'max' => 36),
			array('name, link, nameRUS, nameENG, INN, KPP, OGRN, OKPO', 'length', 'max' => 150),
			//array('description', 'safe'),
			array('description','filter','filter'=>function($v){ return strip_tags($v);}),
			array('description','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
				/*
			array(
					'dateOpening', 'numerical',
				    'integerOnly'=>true,
				    'min'=>1700,
				    'max'=>date("Y"),
				    'tooSmall'=>'Значение слишком мало',
				    'tooBig'=>'Значение слишком велико'),*/
			array(
				'logo',
				'file',
				'types'		 => 'jpg,jpeg, gif, png',
				'maxSize'	 => 1024 * 1024 * 5, // 5MB
				'allowEmpty' => 'true',
				'tooLarge'	 => 'Файл более 5MБ. Пожалуйста, загрузите файл меньшего размера.',
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, nameRUS, nameENG, description, categoryId, companyTypeId, isPaid, isFree, IsOMS, IsDMS, doctorsOnHouse, dateOpening, employeeId, averageCheck, addressesId, INN, KPP, OGRN, OKPO, rating', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'actions'					 => array(self::HAS_MANY, 'Action', 'companyId'),
			'addresses'					 => array(self::HAS_MANY, 'Address', 'ownerId'),
			'contactPersonCompany'		 => array(self::HAS_MANY, 'ContactPersonCompany', 'companyId'),
			'address'					 => array(self::BELONGS_TO, 'Address', 'addressId'),
			'category'					 => array(self::BELONGS_TO, 'Category', 'categoryId'),
			'contracts'					 => array(self::HAS_MANY, 'CompanyContract', 'companyId'),
			'companyType'				 => array(self::BELONGS_TO, 'CompanyType', 'companyTypeId'),
			//'employee'					 => array(self::BELONGS_TO, 'Employee', 'employeeId'),
			'companyActivites'			 => array(self::MANY_MANY, 'CompanyActivite', 'companyActivites(addressId,companyActivitesId)', 'order'=>'name'),
			'companyContracts'			 => array(self::HAS_ONE, 'CompanyContract', 'companyId'),
			'companyEquipments'			 => array(self::HAS_MANY, 'CompanyEquipment', 'companyId'),
			'companyLicenses'			 => array(self::HAS_MANY, 'CompanyLicense', 'companyId'),
			'companyNomenclatureGroups'	 => array(self::HAS_MANY, 'CompanyNomenclatureGroup', 'companyId'),
			'companyServices'			 => array(self::HAS_MANY, 'CompanyServices', 'companyId'),
			'companySoftwares'			 => array(self::HAS_MANY, 'CompanySoftware', 'companyId'),
			'companyUnits'				 => array(self::HAS_MANY, 'CompanyUnit', 'companyId'),
			'contactInformations'		 => array(self::HAS_MANY, 'ContactInformation', 'companyId'),
			'contactPersonCompanies'	 => array(self::HAS_MANY, 'ContactPersonCompany', 'companyId'),
			'doctors'					 => array(self::HAS_MANY, 'Doctor', 'currentPlaceOfWorkId'),
			'emails'					 => array(self::HAS_MANY, 'Email', 'ownerId'),
			'phones'					 => array(self::HAS_MANY, 'Phone', 'ownerId'),
			'placeOfWorks'				 => array(self::HAS_MANY, 'PlaceOfWork', 'companyId'),
			'priceNomenclatures'		 => array(self::HAS_MANY, 'PriceNomenclature', 'companyId'),
			'refs'						 => array(self::HAS_MANY, 'Ref', 'ownerId'),
			'softwares'					 => array(self::HAS_MANY, 'Software', 'companyId'),
			'user1Cs'					 => array(self::HAS_MANY, 'User1C', 'companyId'),
			'userMedicals'				 => array(self::HAS_MANY, 'UserMedical', 'companyId'),
			'userPatronages'			 => array(self::HAS_MANY, 'UserPatronage', 'companyId'),
			'userShops'					 => array(self::HAS_MANY, 'UserShops', 'companyId'),
			'userSuppliers'				 => array(self::HAS_MANY, 'UserSuppliers', 'companyId'),
			'userTourisms'				 => array(self::HAS_MANY, 'UserTourism', 'companyId'),
			'workingHoursOfDoctors'		 => array(self::HAS_MANY, 'WorkingHoursOfDoctors', 'companyId'),
			'legalEntities'				 => array(self::BELONGS_TO, 'LegalEntities', 'legalEntitiesId'),
			'agr_companyCount'			 => array(self::STAT,'AgrCompany','id'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'					 => 'ID',
			'name'					 => 'Название компании',
			'link'					 => 'Link',
			'nameRUS'				 => 'Name Rus',
			'nameENG'				 => 'Name Eng',
			'description'			 => 'Описание',
			'categoryId'			 => 'Category',
			'companyTypeId'			 => 'Company Type',
			'isPaid'				 => 'Is Paid',
			'isFree'				 => 'Is Free',
			'IsOMS'					 => 'Is Oms',
			'IsDMS'					 => 'Is Dms',
			'doctorsOnHouse'		 => 'Doctors On House',
			'dateOpening'			 => 'Год основания',
			//'contractsId'			 => 'Contracts',
			'employeeId'			 => 'Employee',
			'averageCheck'			 => 'Average Check',
			'addressesId'			 => 'Address',
			//'contactPersonCompanyId' => 'Contact Person Company',
			'INN'					 => 'Inn',
			'KPP'					 => 'Kpp',
			'OGRN'					 => 'Ogrn',
			'OKPO'					 => 'Okpo',
			//'rating'				 => 'Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('nameRUS', $this->nameRUS, true);
		$criteria->compare('nameENG', $this->nameENG, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('categoryId', $this->categoryId, true);
		$criteria->compare('companyTypeId', $this->companyTypeId, true);
		$criteria->compare('isPaid', $this->isPaid);
		$criteria->compare('isFree', $this->isFree);
		$criteria->compare('IsOMS', $this->IsOMS);
		$criteria->compare('IsDMS', $this->IsDMS);
		$criteria->compare('doctorsOnHouse', $this->doctorsOnHouse);
		$criteria->compare('dateOpening', $this->dateOpening);
		//$criteria->compare('contractsId', $this->contractsId, true);
		$criteria->compare('employeeId', $this->employeeId, true);
		$criteria->compare('averageCheck', $this->averageCheck);
		$criteria->compare('addressesId', $this->addressesId, true);
		//$criteria->compare('contactPersonCompanyId', $this->contactPersonCompanyId, true);
		$criteria->compare('INN', $this->INN, true);
		$criteria->compare('KPP', $this->KPP, true);
		$criteria->compare('OGRN', $this->OGRN, true);
		$criteria->compare('OKPO', $this->OKPO, true);
		//$criteria->compare('rating', $this->rating);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function scopes() {
		return array(
			'clinic' => array(
				'condition' => "`t`.`categoryId` = 'a3969945e0f59fa84e0e7740c2e8cc87'"
			),
			'insurance' => array(
				'condition' => "`t`.`categoryId` = 'adf65996fa1f52014e98585a4a40422d'"
			),
		);
	}

	public function getUploadPath() {

		$path = Yii::getPathOfAlias('uploads.company') . '/' . $this->link;

		if ( !is_dir($path) )
			mkdir($path, 0775, true);
		return $path;
	}
	
	public function getUploadUrl() {

		return Yii::app()->baseUrl . '/uploads/company/' . $this->link;
	}

	public function getLogoPath() {
		return "{$this->uploadPath}/logo.png";
	}

	public function getLogo() {		
		if ( !isset($this->_logo) )
			$this->_logo = is_file($this->logoPath) ? "logo.png" : false;
		if($this->_logo == false)
			$this->_logo = is_file(str_replace('.png','.jpg',$this->logoPath)) ? "logo.jpg" : false;
		return $this->_logo;
	}

	public function setLogo($value) {
		$this->_logo = $value;
	}

	public function getLogoUrl() {

		return $this->logo ? "{$this->uploadUrl}/$this->logo" : false;
	}
}