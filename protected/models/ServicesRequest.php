<?php

/**
 * This is the model class for table "servicesRequest".
 *
 * The followings are the available columns in table 'servicesRequest':
 * @property string $id
 * @property string $name
 * @property string $addressId
 * @property integer $status
 * @property string $serviceSectionId
 * @property string $description
 * @property string $companyActiviteId
 * @property string $createdDate
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property CompanyActivite $companyActivite
 * @property ServiceSection $serviceSection
 */
class ServicesRequest extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ServicesRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'serviceRequest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, price, serviceSectionId, description', 'required'),
            array('status, price', 'numerical', 'integerOnly'=>true),
			array('id, addressId, serviceSectionId, companyActiviteId', 'length', 'max'=>36),
			array('name', 'length', 'max'=>150),
			array('reasonForRejectionId','default','value'=>'832eb63019fa65964954d97af153c2b8'),
			array('chekStatusesId','default','value'=>'b72fa510f3199cfd4a82fc7140c3668c'),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, addressId, status, serviceSectionId, description, companyActiviteId', 'safe', 'on'=>'search'),
			array('createdDate', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'companyActivite' => array(self::BELONGS_TO, 'CompanyActivite', 'companyActiviteId'),
			'serviceSection' => array(self::BELONGS_TO, 'ServiceSection', 'serviceSectionId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название услуги',
			'addressId' => 'Address',
			'status' => 'Status',
			'serviceSectionId' => 'Раздел',
			'description' => 'Описание',
			'companyActiviteId' => 'Профиль деятельности',
			'price' => 'Цена',
			'createdDate' => 'Дата создания',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('serviceSectionId',$this->serviceSectionId,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('companyActiviteId',$this->companyActiviteId,true);
		$criteria->compare('createdDate',$this->createdDate);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}	
	
	public function beforeSave() {
		if(parent::beforeSave()) {
			if($this->isNewRecord) {
				$this->checkDate = new CDbExpression('NOW()');
			}
			return true;
		}
		
	}
}