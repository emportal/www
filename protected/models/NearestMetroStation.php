<?php

/**
 * This is the model class for table "nearestMetroStation".
 *
 * The followings are the available columns in table 'nearestMetroStation':
 * @property string $id
 * @property string $addressId
 * @property string $metroStationId
 * @property integer $distance
 *
 * The followings are the available model relations:
 * @property MetroStation $metroStation
 * @property Address $address
 */
class NearestMetroStation extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NearestMetroStation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nearestMetroStation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('distance', 'numerical', 'integerOnly'=>true),
			array('id, addressId, metroStationId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, addressId, metroStationId, distance', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'metroStation' => array(self::BELONGS_TO, 'MetroStation', 'metroStationId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'addressId' => 'Address',
			'metroStationId' => 'Metro Station',
			'distance' => 'Distance',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('metroStationId',$this->metroStationId,true);
		$criteria->compare('distance',$this->distance);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}