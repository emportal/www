<?php

/**
 * This is the model class for table "legalEntity".
 *
 * The followings are the available columns in table 'legalEntity':
 * @property integer $id
 * @property string $name
 * @property string $factAddress
 * @property string $INN
 * @property string $KPP
 * @property string $OGRN
 * @property string $OKPO
 */
class LegalEntity extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LegalEntity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'legalEntity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, factAddress, INN, KPP, OGRN, OKPO', 'required'),
			array('INN', 'length', 'max'=>12),
			array('KPP, OKPO', 'length', 'max'=>10),
			array('OGRN', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, factAddress, INN, KPP, OGRN, OKPO', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'company'			=> array(self::BELONGS_TO,'Company','mainLegalEntityId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'factAddress' => 'Fact Address',
			'INN' => 'Inn',
			'KPP' => 'Kpp',
			'OGRN' => 'Ogrn',
			'OKPO' => 'Okpo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('factAddress',$this->factAddress,true);
		$criteria->compare('INN',$this->INN,true);
		$criteria->compare('KPP',$this->KPP,true);
		$criteria->compare('OGRN',$this->OGRN,true);
		$criteria->compare('OKPO',$this->OKPO,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}