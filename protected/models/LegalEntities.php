<?php

/**
 * This is the model class for table "legalEntities".
 *
 * The followings are the available columns in table 'legalEntities':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $INN
 * @property string $KPP
 * @property string $OGRN
 * @property string $OKPO
 * @property string $fullName
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 */
class LegalEntities extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LegalEntities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'legalEntities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'length', 'max'=>36),
			array('name, link, INN, KPP, OGRN, OKPO, fullName', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, INN, KPP, OGRN, OKPO, fullName', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'legalEntitiesId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'INN' => 'Inn',
			'KPP' => 'Kpp',
			'OGRN' => 'Ogrn',
			'OKPO' => 'Okpo',
			'fullName' => 'Full Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('INN',$this->INN,true);
		$criteria->compare('KPP',$this->KPP,true);
		$criteria->compare('OGRN',$this->OGRN,true);
		$criteria->compare('OKPO',$this->OKPO,true);
		$criteria->compare('fullName',$this->fullName,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}