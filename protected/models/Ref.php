<?php

/**
 * This is the model class for table "ref".
 *
 * The followings are the available columns in table 'ref':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $ownerId
 * @property string $refValue
 * @property string $typeId
 * @property integer $forCompany
 * @property integer $forProducers
 * @property integer $forDoctors
 *
 * The followings are the available model relations:
 * @property Brand[] $brands
 * @property ProducerBrand[] $producerBrands
 * @property ReferenceType $type
 * @property Company|Doctor|Producer $owner
 *
 */
class Ref extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ref the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ref';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ownerId, typeId, refValue', 'required'),
			array('forCompany, forProducers, forDoctors', 'numerical', 'integerOnly'=>true),
			array('id, ownerId typeId', 'length', 'max'=>36),
			array('name, link, refValue', 'length', 'max'=>150),
			array('refValue', 'url', 'validateIDN'=>true, 'defaultScheme' => 'http', 'validSchemes'=>array('http', 'https')),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, companyId, doctorId, producerId, stateHealthAgencyId, insuranceCompanyId, refValue, typeId, forCompany, forProducers, forDoctors', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => [self::BELONGS_TO,'Address','ownerId'],
			//'company' => [self::BELONGS_TO,'Company','ownerId'],
			'brands' => array(self::HAS_MANY, 'Brand', 'refId'),
			'producerBrands' => array(self::HAS_MANY, 'ProducerBrand', 'refId'),
			'type' => array(self::BELONGS_TO, 'ReferenceType', 'typeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'companyId' => 'Company',
			'doctorId' => 'Doctor',
			'producerId' => 'Producer',
			'stateHealthAgencyId' => 'State Health Agency',
			'insuranceCompanyId' => 'Insurance Company',
			'refValue' => 'Адрес сайта',
			'typeId' => 'Type',
			'forCompany' => 'For Company',
			'forProducers' => 'For Producers',
			'forDoctors' => 'For Doctors',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('producerId',$this->producerId,true);
		$criteria->compare('stateHealthAgencyId',$this->stateHealthAgencyId,true);
		$criteria->compare('insuranceCompanyId',$this->insuranceCompanyId,true);
		$criteria->compare('refValue',$this->refValue,true);
		$criteria->compare('typeId',$this->typeId,true);
		$criteria->compare('forCompany',$this->forCompany);
		$criteria->compare('forProducers',$this->forProducers);
		$criteria->compare('forDoctors',$this->forDoctors);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}