<?php

/**
 * This is the model class for table "shortSpecialtyOfDoctor".
 *
 * The followings are the available columns in table 'shortSpecialtyOfDoctor':
 * @property string $id
 * @property string $cityId
 * @property string $name
 * @property string $link
 * @property string $linkUrl
 * @property string $Parameters
 * @property integer $count
 * @property integer $samozapis
 */
class ShortSpecialtyOfDoctor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ShortSpecialtyOfDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shortSpecialtyOfDoctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, link, linkUrl', 'required'),
			array('count, samozapis', 'numerical', 'integerOnly'=>true),
			array('id, cityId', 'length', 'max'=>36),
			array('link', 'length', 'max'=>25),
			array('name, linkUrl', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cityId, name, link, linkUrl, Parameters, count, samozapis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'doctorSpecialties' => [self::HAS_MANY, 'DoctorSpecialty', 'shortSpecialtyId']
		);
	}
	public function getMetro() {
		
	}
	public function getDistricts() {
		
	}


	/**
	 * @param $arrData
	 * @return mixed
     */
	public static function getGeoSpecialty($arrData){

		$arrData = array_diff($arrData, array(''));
		$regexp = implode("|", $arrData);
		$criteria = new CDbCriteria;
		$criteria->select = 'name, linkUrl, samozapis';

		$criteria->order = '`t`.`name` ASC';
		$criteria->distinct = true;
		$criteria->params = array(':Parameters' => $regexp);
		$criteria->addCondition(array(
			"LOWER(`t`.`Parameters`) REGEXP :Parameters",
		));
		if(Yii::app()->params['samozapis']){
			$criteria->addCondition(array(
				"t.samozapis = 1",
			));
		}else{
			$criteria->addCondition(array(
				"t.samozapis = 0",
			));
		}
		$data = self::model()->findAll($criteria);
		return $data;
	}

	/**
	 * @param $cityId
	 * @return mixed
     */
	public static function getCitySpecialty($cityId){


		$criteria = new CDbCriteria;
		$criteria->select = 'name, linkUrl, samozapis';

		$criteria->order = '`t`.`name` ASC';
		$criteria->distinct = true;

		$criteria->params = array(':cityId' => $cityId);
		$criteria->addCondition(array(
			"cityId = :cityId",
		));
		if(Yii::app()->params['samozapis']){
			$criteria->addCondition(array(
				"t.samozapis = 1",
			));
		}else{
			$criteria->addCondition(array(
				"t.samozapis = 0",
			));
		}
		$data = self::model()->findAll($criteria);
		return $data;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cityId' => 'City',
			'name' => 'Name',
			'link' => 'Link',
			'linkUrl' => 'Link Url',
			'Parameters' => 'Parameters',
			'count' => 'Count',
			'samozapis' => 'Samozapis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('cityId',$this->cityId,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('linkUrl',$this->linkUrl,true);
		$criteria->compare('Parameters',$this->Parameters,true);
		$criteria->compare('count',$this->count);
		$criteria->compare('samozapis',$this->samozapis);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function getSpecialty($doctorsList) {
		$term = "(" . join(',', $doctorsList) . ")";

		$command = Yii::app()->db->createCommand()
		->select("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId id, doctorSpecialty.name, doctorSpecialty.linkUrl, company.linkUrl companyLinkUrl, COUNT(DISTINCT t.id) count, if(MAX(doctorServices.free)>0, 0, MIN(doctorServices.price)) AS minPrice, COUNT(appointmentToDoctors.id) AS appointments")
		->from("doctor t")
		->join("placeOfWork placeOfWorks" , "placeOfWorks.doctorId=t.id")
		->join("address address" , "(placeOfWorks.addressId=address.id) AND (address.isActive = '1')")
		->join("userMedical userMedicals" , "(userMedicals.addressId=address.id) AND (userMedicals.agreementNew='1')")
		->join("company company" , "address.ownerId=company.id")
		->join("specialtyOfDoctor specialtyOfDoctors" , "specialtyOfDoctors.doctorId=t.id")
		->join("doctorSpecialty doctorSpecialty" , "specialtyOfDoctors.doctorSpecialtyId=doctorSpecialty.id")
		->leftJoin("doctorServices doctorServices" , "doctorServices.doctorId=t.id AND (doctorServices.price > 0 OR doctorServices.free=1) AND doctorServices.cassifierServiceId='54869cc8-4a4e-4f88-a20f-cb5a5a7bb33f'")
		->leftJoin("appointmentToDoctors appointmentToDoctors" , "appointmentToDoctors.appType !='' AND appointmentToDoctors.appType !='own' AND appointmentToDoctors.doctorId=t.id AND appointmentToDoctors.statusId != 7 AND appointmentToDoctors.createdDate >= DATE_SUB(NOW(), INTERVAL 1 MONTH)")
		->where("t.id IN $term")
		->group("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId");
		$specialty = $command->queryAll();
		
		return $specialty;
	}
	
	public static function getNearestMetro($doctorsList) {
		$term = "(" . join(',', $doctorsList) . ")";
		
		$command = Yii::app()->db->createCommand()
		->select("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId id, metroStations.linkUrl")
		->from("doctor t")
		->join("placeOfWork placeOfWorks" , "placeOfWorks.doctorId=t.id")
		->join("address address" , "(placeOfWorks.addressId=address.id) AND (address.isActive = '1')")
		->join("userMedical userMedicals" , "(userMedicals.addressId=address.id) AND (userMedicals.agreementNew='1')")
		->join("nearestMetroStation metroStations_metroStations" , "address.id=metroStations_metroStations.addressId")
		->join("metroStation metroStations" , "metroStations.id=metroStations_metroStations.metroStationId")
		->join("specialtyOfDoctor specialtyOfDoctors" , "specialtyOfDoctors.doctorId=t.id")
		->where("t.id IN $term")
		->group("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId, metroStations.linkUrl");
		$nearestMetro = $command->queryAll();
		return $nearestMetro;
	}
	
	public static function getNearestDistrict($doctorsList) {
		$term = "(" . join(',', $doctorsList) . ")";
		
		$command = Yii::app()->db->createCommand()
		->select("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId id, cityDistrict.linkUrl")
		->from("doctor t")
		->join("placeOfWork placeOfWorks" , "(placeOfWorks.doctorId=t.id)")
		->join("address address" , "(placeOfWorks.addressId=address.id) AND (address.isActive = '1')")
		->join("userMedical userMedicals" , "(userMedicals.addressId=address.id) AND (userMedicals.agreementNew='1')")
		->join("cityDistrict cityDistrict", "cityDistrict.id = address.cityDistrictId")
		->join("specialtyOfDoctor specialtyOfDoctors" , "specialtyOfDoctors.doctorId=t.id")
		->where("t.id IN $term")
		->group("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId, cityDistrict.linkUrl");
		$nearestDistrict = $command->queryAll();
		return $nearestDistrict;
	}

	public static function getCompaniesResult($doctorsList) {
		$term = "(" . join(',', $doctorsList) . ")";
		
		$command = Yii::app()->db->createCommand()
		->select("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId id, company.id companyId, company.name companyName, company.linkUrl companyLinkUrl")
		->from("doctor t")
		->join("placeOfWork placeOfWorks" , "(placeOfWorks.doctorId=t.id)")
		->join("address address" , "(placeOfWorks.addressId=address.id) AND (address.isActive = '1')")
		->join("userMedical userMedicals" , "(userMedicals.addressId=address.id) AND (userMedicals.agreementNew='1')")
		->join("company company" , "address.ownerId=company.id")
		->join("specialtyOfDoctor specialtyOfDoctors" , "(specialtyOfDoctors.doctorId=t.id)")
		->where("t.id IN $term")
		->group("address.cityId, address.samozapis, specialtyOfDoctors.doctorSpecialtyId, company.id");
		$companiesResult = $command->queryAll();
		return $companiesResult;
	}
	
	public static function UpdateSelf($attributes=[], $log=TRUE) {
		$term = "";
		foreach ($attributes as $attribute=>$value) {
			$term .= "address.$attribute=".Yii::app()->db->quoteValue($value);
			$term .= " AND ";
		}
		// Выбрать данные для заполнения ShortSpecialtyOfDoctor
		$command = Yii::app()->db->createCommand()
		->select("t.id")
		->from("doctor t")
		->leftJoin("placeOfWork placeOfWorks" , "(placeOfWorks.doctorId=t.id)")
		->leftJoin("address address" , "(placeOfWorks.addressId=address.id) AND ((LOWER(address.name) <> 'нет данных') AND (address.isActive = '1'))")
		->leftJoin("userMedical userMedicals" , "(userMedicals.addressId=address.id) AND (LOWER(userMedicals.name) <> 'нет данных')")
		->leftJoin("company company" , "(address.ownerId=company.id) AND ((LOWER(company.name) <> 'нет данных') AND (company.removalFlag != '1'))")
		->join("companyContract companyContracts" , "(companyContracts.companyId=company.id) AND (LOWER(companyContracts.name) <> 'нет данных')")
		->where("$term(((((userMedicals.agreementNew='1') AND (placeOfWorks.id IS NOT NULL)))) AND (t.deleted <> '1'))")
		;

		$doctorsList = [];
		$doctorsListQueryResult = $command->queryAll();
		foreach ($doctorsListQueryResult as $doctor) {
			$doctorsList[$doctor['id']] = Yii::app()->db->quoteValue($doctor['id']);
		}
		
		$specialty = ShortSpecialtyOfDoctor::getSpecialty($doctorsList);
		$nearestMetro = ShortSpecialtyOfDoctor::getNearestMetro($doctorsList);
		$nearestDistrict = ShortSpecialtyOfDoctor::getNearestDistrict($doctorsList);
		$companiesResult = ShortSpecialtyOfDoctor::getCompaniesResult($doctorsList);
		
		$Parameters = [];
		foreach($nearestMetro as $value) {
			$Parameters[$value['cityId']][$value['samozapis']][$value['id']]["Metro"][] = 'metro-'.$value['linkUrl'];
		}
		foreach($nearestDistrict as $value) {
			$Parameters[$value['cityId']][$value['samozapis']][$value['id']]["District"][] = 'raion-'.$value['linkUrl'];
		}
		foreach($companiesResult as $value) {
			$Parameters[$value['cityId']][$value['samozapis']][$value['id']]["Companies"][] = $value['companyLinkUrl'];
		}
		
		try
		{
			$transaction=Yii::app()->db->beginTransaction();
		
			// удалить всё из ShortSpecialtyOfDoctor
			if(!empty($attributes))
				ShortSpecialtyOfDoctor::model()->deleteAllByAttributes($attributes);
			else
				ShortSpecialtyOfDoctor::model()->deleteAll();
			// удалить всё из ShortCompaniesOfDoctor
			if(!empty($attributes))
				ShortCompaniesOfDoctor::model()->deleteAllByAttributes($attributes);
			else
				ShortCompaniesOfDoctor::model()->deleteAll();
		
			$companies = [];
			foreach($companiesResult as $value) {
				if(!empty($companies[$value['companyId']][$value['cityId']][$value['samozapis']])) continue;
				$obj = new ShortCompaniesOfDoctor;
				$obj->companyId = $value['companyId'];
				$obj->cityId = $value['cityId'];
				$obj->companyName = $value['companyName'];
				$obj->companyLinkUrl = $value['companyLinkUrl'];
				$obj->samozapis = $value['samozapis'];
				if($obj->save()) {
					$companies[$value['companyId']][$value['cityId']][$value['samozapis']] = $value['companyId'];
				} else {
					if($log) echo $obj->companyName.": ERROR! <br>";
					if($log) echo " - " . MyTools::errorsToString($obj->getErrors()) . "<br>";
				}
			}
			
			// Заполнить данные в ShortSpecialtyOfDoctor
			if($log) echo "<table>".PHP_EOL;
			foreach($specialty as $key=>$value) {
		
				$obj = new ShortSpecialtyOfDoctor;
				$obj->link = $key;
				$obj->id = $value['id'];
				$obj->cityId = $value['cityId'];
				$obj->name = $value['name'];
				$obj->linkUrl = $value['linkUrl'];
				$obj->Parameters = json_encode($Parameters[$value['cityId']][$value['samozapis']][$value['id']]);
				$obj->count = intval($value['count']);
				$obj->samozapis = $value['samozapis'];
				$obj->minPrice = $value['minPrice'];
				$obj->appointments = $value['appointments'];
		
				$string = "<td>".$obj->id."</td><td>".$obj->name."</td><td>".$obj->link."</td><td>".$obj->linkUrl."</td><td>".$value['count']."</td><td>".$value['minPrice']."</td><td>".$value['appointments']."</td>";
				if($obj->save()) {
					$string .= "<td>OK</td>";
					// Записать идентификаторы
					$setIdInDoctorSpecialty = Yii::app()->db->createCommand();
					$setIdInDoctorSpecialty->update("doctorSpecialty", ["shortSpecialtyId" => $value['id']], "id=".Yii::app()->db->quoteValue($value['id']));
					$setIdInDoctorSpecialty->execute();
				}
				else $string .= "<td>ERROR!</td>";
				if($log) echo "<tr>".$string."</tr>".PHP_EOL;
			}
			if($log) echo "</table>".PHP_EOL;
		
			$transaction->commit();
			if($log) echo "[ Транзакция выполнена ]";
		}
		catch(Exception $e)
		{
			$transaction->rollback();
		}
	}
}