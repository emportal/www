<?php

/**
 * This is the model class for table "zRight".
 *
 * The followings are the available columns in table 'zRight':
 * @property string $name
 */
class RightType extends ActiveRecord
{
	//пояснения см. в таблице zRight
	const ACCESS_NEW_ADMIN = 1;
	const ACCESS_NEW_ADMIN_CLINIC = 2;
	const ACCESS_ALL_MANAGERS_DATA = 3;
	const CHANGE_MANAGER_RELATIONS = 4;
	const ACCESS_CALL_CENTER_FUNCTIONALITY = 5;
	const IS_ADMIN_IN_ADDRESS = 6;
	const IS_OWNER_OF_ADDRESS = 7;
	const CAN_RECEIVE_SELF_REGISTERED_COMPANIES = 8;
	const CAN_UPLOAD_DOCUMENTS_FROM_1C = 9; //"Может загружать документы из 1С"
	
	const CAN_WORK_AS_SITE_EDITOR= 10; // Может работать в админке редактора сайта ЕМП
	const CAN_SEE_EMP_COMISION_NEW_ADMIN= 11; // Видит комиссию ЕМП в регистратуре (newAdmin)
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Album the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zRight';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rights' => array(self::HAS_MANY, 'sRights', 'zRightId'),
		);
	}
	
	public function behaviors()
	{
		return array(
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}