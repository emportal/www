<?php

/**
 * This is the model class for table "phoneVerification".
 *
 * The followings are the available columns in table 'phoneVerification':
 * @property integer $id
 * @property string $phone
 * @property string $code
 * @property int $timestamp
 * @property int $status
 * @property string $userId
 */

class PhoneVerification extends CActiveRecord
{
	const NOT_READY = 0; // не готов
	const READY_TO_REGISTER = 1; // проставляется, когда пользователь ввел код при регистраии
	const READY_TO_APPOINTMENT = 2; // проставляется, когда пользователю выслан код активации ( без разницы, для гостя или пользователя )
	const READY_TO_CHANGEPHONE = 3;	//// проставляется когда пользователю выслан код для смены телефона	
	const CHANGEPHONE = 4; // проставляется когда пользователь подтвердил смену телефона
	const APPOINTMENT = 5; // проставляется, когда пользователь подтвердил код для записи на прием ( без разницы, для гостя или пользователя )
	const PHONE_EXTEND_LIFETIME = 6; // Устанавливается при создании кода высланного по истечению 3-х месячного периода
	const CHANGE_PHONE_EXTEND_LIFETIME =7; // Устанавливается при создании кода высланного на новый телефон, по истечению 3-х месячного периода
	const BLOCKED_APPOINTMENT = 8; // Устанавливается при создании кода после блокировки записи на прием.
	const RECOVERY_BY_PHONE = 9; // Восстановление пароля по коду на мобильный телефон
	
	
	public $type; //переменная нужна для выбора типа действия при реактивации телефона, новый телефон или нет. Хранит название нажатой кнопки yes/no
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PhoneVerification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'phoneVerification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phone, code', 'required'),
			array('phone, code', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, phone, code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'phone' => 'Phone',
			'code' => 'Code',
			'timestamp'=>'timestamp'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}	
	
	public function beforeSave() {
		if(parent::beforeSave()) {
			$this->timestamp = time();
			
			return true;
		}
	}	
	public function scopes() {
		return array(
			'user'=> array(
				'condition'=>"`t`.`userId` = '" . Yii::app()->user->model->id . "'"
			),
			'oneTimePhone'=>array(
				'condition'=>"`t`.`status` = '" . self::READY_TO_APPOINTMENT . "' AND `t`.`timestamp` > '". (time()-Yii::app()->params['SendCodeDelay']) . "'"
			),
			'readyOneTimePhone'=>array(
				'condition'=>"`t`.`status` = '" . self::APPOINTMENT . "' AND `t`.`timestamp` > '". (time()-Yii::app()->params['SendCodeDelay']) . "'"
			)
		);
	}
	/**
	 * removes expired phoneVerifications and clean session for appointment
	 */
	public static function cleanSessionPhone() {
		PhoneVerification::model()->deleteAll("timestamp < :waitTime",[
			':waitTime'=>time()-Yii::app()->params['SendCodeDelay']
		]);
		$model = PhoneVerification::model()->readyOneTimePhone()->findByAttributes([
			'phone' => Yii::app()->session['appointment_phone_set']
		]);
		if(!$model) {
			Yii::app()->session['appointment_phone_set'] = "";
		}
	}
}