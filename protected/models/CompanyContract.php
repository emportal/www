<?php

/**
 * This is the model class for table "companyContract".
 *
 * The followings are the available columns in table 'companyContract':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $companyId
 * @property string $contractNumber
 * @property string $periodOfValidity
 * @property string $employeeId
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 * @property Company $company
 * @property Employee $employee
 */
class CompanyContract extends ActiveRecord
{
	public $contract;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanyContract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companyContract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, companyId, employeeId', 'length', 'max'=>36),
			array('name, link, contractNumber', 'length', 'max'=>150),
			array('periodOfValidity, indexNum', 'safe'),
			['typeId', 'default', 'value' => '852b5020b64a5134413f6d12be1610e1'],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, companyId, contractNumber, periodOfValidity, employeeId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'contractsId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'employee' => array(self::BELONGS_TO, 'Employee', 'employeeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'companyId' => 'Company',
			'contractNumber' => 'Contract Number',
			'periodOfValidity' => 'Period Of Validity',
			'employeeId' => 'Employee',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('contractNumber',$this->contractNumber,true);
		$criteria->compare('periodOfValidity',$this->periodOfValidity,true);
		$criteria->compare('employeeId',$this->employeeId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}