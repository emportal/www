<?php

/**
 * This is the model class for table "pollOption".
 *
 * The followings are the available columns in table 'pollOption':
 * @property string $id
 * @property string $pollId
 * @property string $pollOption
 *
 * The followings are the available model relations:
 * @property PollAnswer[] $pollAnswers
 * @property Poll $poll
 */
class PollOption extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PollOption the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pollOption';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, pollId, pollOption', 'required'),
			array('id, pollId', 'length', 'max'=>36),
			array('pollOption', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, pollId, pollOption', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pollAnswers' => array(self::HAS_MANY, 'PollAnswer', 'pollOptionId'),
			'poll' => array(self::BELONGS_TO, 'Poll', 'pollId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pollId' => 'Poll',
			'pollOption' => 'Poll Option',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('pollId',$this->pollId,true);
		$criteria->compare('pollOption',$this->pollOption,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}