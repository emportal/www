<?php

/**
 * This is the model class for table "logSamozapis".
 *
 * The followings are the available columns in table 'logSamozapis':
 * @property integer $id
 * @property string $type
 * @property string $errorText
 * @property string $date
 * @property string $headers
 * @property integer $idDoctor
 */
class LogSamozapis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logSamozapis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, errorText, idDoctor', 'required'),
			array('type', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, errorText, date, headers, idDoctor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'errorText' => 'Error Text',
			'date' => 'Date',
			'headers' => 'Headers',
			'idDoctor' => 'Id Doctor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('errorText',$this->errorText,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('headers',$this->headers,true);
		$criteria->compare('idDoctor',$this->idDoctor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogSamozapis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function addLog($type,$error,$idDoctor) {
		$log = new LogSamozapis;
		$log->type = get_class($type);
		$log->errorText = json_encode($error->ErrorList);
		$log->date = date("Y-m-d H:i:s");
		if(is_object($type->client)) {
			$log->headers = $type->client->__getLastResponseHeaders().'\n'.$type->client->__getLastResponse();
		}
		else {
			$log->headers = 'None';
		}
		$log->idDoctor = $idDoctor;
		$log->save(); 
	}
}
