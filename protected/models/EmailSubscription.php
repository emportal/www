<?php

/**
 * This is the model class for table "emailSubscription".
 *
 * The followings are the available columns in table 'emailSubscription':
 * @property integer $id
 * @property integer $type
 * @property string $email
 * @property string $companyId
 * @property string $doctorId
 * @property string $cancelCode
 */
class EmailSubscription extends CActiveRecord
{
	
	public static $types = [
		1 => 'Новости ЕМП',
		2 => 'Новости клиники',
		3 => 'Уведомления о записи на прием',
		4 => 'Сообщить об открытии клиники',
		5 => 'Уведомления от ЕМП',
	];
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailSubscription the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emailSubscription';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, email', 'required'),
			array('email', 'email'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('companyId, doctorId, cancelCode', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, email, companyId, doctorId, cancelCode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'email' => 'Email',
			'companyId' => 'Company',
			'doctorId' => 'Doctor',
			'cancelCode' => 'Cancel Code',
		);
	}
	
	public function beforeSave() {
	
		parent::beforeSave();
		
		$guidString = '';
		if (function_exists('com_create_guid'))
		{
			$guidString = com_create_guid();
		} else {
			mt_srand((double)microtime()*10000); //для php 4.2.0 и выше
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);
			$guidString = chr(123)
				.substr($charid, 0, 8).$hyphen
				.substr($charid, 8, 4).$hyphen
				.substr($charid,12, 4).$hyphen
				.substr($charid,16, 4).$hyphen
				.substr($charid,20,12)
				.chr(125);
		}
		
		$this->cancelCode = (string)substr(md5($guidString), 0, 12);
		
		if ($this->type == 1 || $this->type == 3 || $this->type == 5)
		{
			//если это подписка на новости ЕМП или подиска на получение талонов,
			//надо убедиться, что такая подписка еще не существует
			$searchAttributes = [
				'type' => $this->type,
				'email' => $this->email,
			];
		}
		if ($this->type == 2 || $this->type == 4)
		{
			$searchAttributes = [
				'type' => $this->type,
				'email' => $this->email,
				'companyId' => $this->companyId
			];
		}
		
		$previouslySavedEmailSub = EmailSubscription::model()->findByAttributes($searchAttributes);
		return (!count($previouslySavedEmailSub)) ? true : false;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('cancelCode',$this->cancelCode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}