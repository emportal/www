<?php

/**
 * This is the model class for table "emailCode".
 *
 * The followings are the available columns in table 'emailCode':
 * @property integer $id
 * @property string $userId
 * @property string $value
 * @property integer $type
 * @property integer $date
 *
 * The followings are the available model relations:
 * @property User $user
 */
class EmailCode extends ActiveRecord {

	private static $listAlpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailCode the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'emailCode';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, value, date', 'required'),
			array('type, date', 'numerical', 'integerOnly' => true),
			array('userId', 'length', 'max' => 36),
			array('value', 'length', 'max' => 45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userId, value, type, date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id'	 => 'ID',
			'userId' => 'User',
			'value'	 => 'Value',
			'type'	 => 'Type',
			'date'	 => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('userId', $this->userId, true);
		$criteria->compare('value', $this->value, true);
		$criteria->compare('type', $this->type);
		$criteria->compare('date', $this->date);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function generateCode($numAlpha = 12) {
		return str_shuffle(
				substr(str_shuffle(self::$listAlpha), 0, $numAlpha)
		);
	}
}