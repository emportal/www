<?php

/**
 * This is the model class for table "cassifierService".
 *
 * The followings are the available columns in table 'cassifierService':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $fullName
 * @property string $medicalSectionId
 * @property string $classifierUnitId
 * @property string $description
 * @property integer $forChildren
 * @property integer $forAdult
 *
 * The followings are the available model relations:
 * @property Action[] $actions
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property ClassifierUnit $classifierUnit
 * @property MedicalSection $medicalSection
 * @property CompanyServices[] $companyServices
 * @property DoctorServices[] $doctorServices
 * @property PriceNomenclature[] $priceNomenclatures
 */
class CassifierService extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CassifierService the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cassifierService';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('forChildren, forAdult', 'numerical', 'integerOnly'=>true),
			array('id, medicalSectionId, classifierUnitId', 'length', 'max'=>36),
			array('name, link, fullName, description', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, fullName, medicalSectionId, classifierUnitId, description, forChildren, forAdult', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'actions' => array(self::HAS_MANY, 'Action', 'serviceId'),
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'ServicesGMUId'),
			'classifierUnit' => array(self::BELONGS_TO, 'ClassifierUnit', 'classifierUnitId'),
			'medicalSection' => array(self::BELONGS_TO, 'MedicalSection', 'medicalSectionId'),
			'companyServices' => array(self::HAS_MANY, 'CompanyServices', 'cassifierServiceId'),
			'doctorServices' => array(self::HAS_MANY, 'DoctorServices', 'cassifierServiceId'),
			'priceNomenclatures' => array(self::HAS_MANY, 'PriceNomenclature', 'cassifierServiceId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'fullName' => 'Full Name',
			'medicalSectionId' => 'Medical Section',
			'classifierUnitId' => 'Classifier Unit',
			'description' => 'Description',
			'forChildren' => 'For Children',
			'forAdult' => 'For Adult',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('fullName',$this->fullName,true);
		$criteria->compare('medicalSectionId',$this->medicalSectionId,true);
		$criteria->compare('classifierUnitId',$this->classifierUnitId,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('forChildren',$this->forChildren);
		$criteria->compare('forAdult',$this->forAdult);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}