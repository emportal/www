<?php

/**
 * This is the model class for table "doctorSpecialtyDisease".
 *
 * The followings are the available columns in table 'doctorSpecialtyDisease':
 * @property string $id
 * @property string $doctorSpecialtyId
 * @property string $diseaseGroupId
 *
 * The followings are the available model relations:
 * @property DiseaseGroup $diseaseGroup
 * @property DoctorSpecialty $doctorSpecialty
 */
class DoctorSpecialtyDisease extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DoctorSpecialtyDisease the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doctorSpecialtyDisease';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, doctorSpecialtyId, diseaseGroupId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, doctorSpecialtyId, diseaseGroupId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'diseaseGroup' => array(self::BELONGS_TO, 'DiseaseGroup', 'diseaseGroupId'),
			'doctorSpecialty' => array(self::BELONGS_TO, 'DoctorSpecialty', 'doctorSpecialtyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctorSpecialtyId' => 'Doctor Specialty',
			'diseaseGroupId' => 'Disease Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('doctorSpecialtyId',$this->doctorSpecialtyId,true);
		$criteria->compare('diseaseGroupId',$this->diseaseGroupId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}