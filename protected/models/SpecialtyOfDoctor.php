<?php

/**
 * This is the model class for table "specialtyOfDoctor".
 *
 * The followings are the available columns in table 'specialtyOfDoctor':
 * @property string $id
 * @property string $doctorId
 * @property string $doctorSpecialtyId
 * @property string $doctorCategoryId
 * @property string $experienceDate
 * @property integer $experiencePeriod
 *
 * The followings are the available model relations:
 * @property DoctorCategory $doctorCategory
 * @property Doctor $doctor
 * @property DoctorSpecialty $doctorSpecialty
 */
class SpecialtyOfDoctor extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SpecialtyOfDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'specialtyOfDoctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctorId', 'required'),
			array('experiencePeriod', 'numerical', 'integerOnly'=>true),
			array('id, doctorId, doctorSpecialtyId, doctorCategoryId', 'length', 'max'=>36),
			array('experienceDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, doctorId, doctorSpecialtyId, doctorCategoryId, experienceDate, experiencePeriod', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'doctorCategory' => array(self::BELONGS_TO, 'DoctorCategory', 'doctorCategoryId'),
			'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorId'),
			'doctorSpecialty' => array(self::BELONGS_TO, 'DoctorSpecialty', 'doctorSpecialtyId'),
			'shortSpecialtyOfDoctor' => array(self::HAS_ONE, 'ShortSpecialtyOfDoctor', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctorId' => 'Doctor',
			'doctorSpecialtyId' => 'Doctor Specialty',
			'doctorCategoryId' => 'Doctor Category',
			'experienceDate' => 'Experience Date',
			'experiencePeriod' => 'Experience Period',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('doctorSpecialtyId',$this->doctorSpecialtyId,true);
		$criteria->compare('doctorCategoryId',$this->doctorCategoryId,true);
		$criteria->compare('experienceDate',$this->experienceDate,true);
		$criteria->compare('experiencePeriod',$this->experiencePeriod);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}