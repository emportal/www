<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property integer $population
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property City[] $cities
 * @property MedicalSchool[] $medicalSchools
 * @property Nomenclature[] $nomenclatures
 * @property Producer[] $producers
 * @property ProducerOffice[] $producerOffices
 * @property Region[] $regions
 */
class Country extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('population', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, population', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'countryId'),
			'cities' => array(self::HAS_MANY, 'City', 'countryId'),
			'medicalSchools' => array(self::HAS_MANY, 'MedicalSchool', 'countryId'),
			'nomenclatures' => array(self::HAS_MANY, 'Nomenclature', 'classifierCountryId'),
			'producers' => array(self::HAS_MANY, 'Producer', 'countryId'),
			'producerOffices' => array(self::HAS_MANY, 'ProducerOffice', 'countryId'),
			'regions' => array(self::HAS_MANY, 'Region', 'countryId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'population' => 'Population',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('population',$this->population);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}