<?php

/**
 * This is the model class for table "relationShipStage".
 *
 * The followings are the available columns in table 'relationShipStage':
 * @property string $idRelationShipStage
 * @property string $name
 * @property string $link
 * @property integer $RelationShipStageEndStage
 */
class RelationShipStage extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RelationShipStage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'relationShipStage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RelationShipStageEndStage', 'numerical', 'integerOnly'=>true),
			array('idRelationShipStage', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idRelationShipStage, name, link, RelationShipStageEndStage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idRelationShipStage' => 'Id Relation Ship Stage',
			'name' => 'Name',
			'link' => 'Link',
			'RelationShipStageEndStage' => 'Relation Ship Stage End Stage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idRelationShipStage',$this->idRelationShipStage,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('RelationShipStageEndStage',$this->RelationShipStageEndStage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}