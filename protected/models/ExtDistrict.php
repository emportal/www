<?php

/**
 * This is the model class for table "extDistrict".
 *
 * The followings are the available columns in table 'extDistrict':
 * @property integer $extId
 * @property string $extName
 * @property integer $extOkato
 * @property string $cityDistrictId
 * @property integer $sysTypeId
 * @property string $lastUpdate
 *
 * The followings are the available model relations:
 * @property ExtSysType $sysType
 */
class ExtDistrict extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExtDistrict the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'extDistrict';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('extId, sysTypeId', 'required'),
			array('extId, extOkato, sysTypeId', 'numerical', 'integerOnly'=>true),
			array('extName', 'length', 'max'=>250),
			array('cityDistrictId', 'length', 'max'=>36),
			array('lastUpdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('extId, extName, extOkato, cityDistrictId, sysTypeId, lastUpdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sysType' => array(self::BELONGS_TO, 'ExtSysType', 'sysTypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'extId' => 'Ext',
			'extName' => 'Ext Name',
			'extOkato' => 'Ext Okato',
			'cityDistrictId' => 'City District',
			'sysTypeId' => 'Sys Type',
			'lastUpdate' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('extId',$this->extId);
		$criteria->compare('extName',$this->extName,true);
		$criteria->compare('extOkato',$this->extOkato);
		$criteria->compare('cityDistrictId',$this->cityDistrictId,true);
		$criteria->compare('sysTypeId',$this->sysTypeId);
		$criteria->compare('lastUpdate',$this->lastUpdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}