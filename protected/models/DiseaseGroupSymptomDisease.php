<?php

/**
 * This is the model class for table "diseaseGroupSymptomDisease".
 *
 * The followings are the available columns in table 'diseaseGroupSymptomDisease':
 * @property string $id
 * @property string $diseaseGroupId
 * @property string $symptomDiseaseId
 *
 * The followings are the available model relations:
 * @property SymptomDisease $symptomDisease
 * @property DiseaseGroup $diseaseGroup
 */
class DiseaseGroupSymptomDisease extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DiseaseGroupSymptomDisease the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'diseaseGroupSymptomDisease';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, diseaseGroupId, symptomDiseaseId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, diseaseGroupId, symptomDiseaseId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'symptomDisease' => array(self::BELONGS_TO, 'SymptomDisease', 'symptomDiseaseId'),
			'diseaseGroup' => array(self::BELONGS_TO, 'DiseaseGroup', 'diseaseGroupId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'diseaseGroupId' => 'Disease Group',
			'symptomDiseaseId' => 'Symptom Disease',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('diseaseGroupId',$this->diseaseGroupId,true);
		$criteria->compare('symptomDiseaseId',$this->symptomDiseaseId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}