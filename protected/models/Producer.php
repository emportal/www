<?php

/**
 * This is the model class for table "producer".
 *
 * The followings are the available columns in table 'producer':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $countryId
 * @property string $nameEng
 * @property string $descriptionription
 * @property integer $numberOfFactories
 * @property string $mainAddressId
 * @property integer $yearOfFoundation
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property Brand[] $brands
 * @property Email[] $emails
 * @property Phone[] $phones
 * @property Address $mainAddress
 * @property Country $country
 * @property ProducerBrand[] $producerBrands
 * @property ProducerNomenclatureGroup[] $producerNomenclatureGroups
 * @property ProducerOffice[] $producerOffices
 * @property Ref[] $refs
 */
class Producer extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Producer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numberOfFactories, yearOfFoundation', 'numerical', 'integerOnly'=>true),
			array('id, countryId, mainAddressId', 'length', 'max'=>36),
			array('name, link, nameEng, descriptionription', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, countryId, nameEng, descriptionription, numberOfFactories, mainAddressId, yearOfFoundation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'producerId'),
			'brands' => array(self::HAS_MANY, 'Brand', 'producerId'),
			'emails' => array(self::HAS_MANY, 'Email', 'producerId'),
			'phones' => array(self::HAS_MANY, 'Phone', 'producerId'),
			'mainAddress' => array(self::BELONGS_TO, 'Address', 'mainAddressId'),
			'country' => array(self::BELONGS_TO, 'Country', 'countryId'),
			'producerBrands' => array(self::HAS_MANY, 'ProducerBrand', 'producerId'),
			'producerNomenclatureGroups' => array(self::HAS_MANY, 'ProducerNomenclatureGroup', 'producerId'),
			'producerOffices' => array(self::HAS_MANY, 'ProducerOffice', 'producerId'),
			'refs' => array(self::HAS_MANY, 'Ref', 'producerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'countryId' => 'Country',
			'nameEng' => 'Name Eng',
			'descriptionription' => 'Descriptionription',
			'numberOfFactories' => 'Number Of Factories',
			'mainAddressId' => 'Main Address',
			'yearOfFoundation' => 'Year Of Foundation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('countryId',$this->countryId,true);
		$criteria->compare('nameEng',$this->nameEng,true);
		$criteria->compare('descriptionription',$this->descriptionription,true);
		$criteria->compare('numberOfFactories',$this->numberOfFactories);
		$criteria->compare('mainAddressId',$this->mainAddressId,true);
		$criteria->compare('yearOfFoundation',$this->yearOfFoundation);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}