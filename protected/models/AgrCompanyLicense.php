<?php

/**
 * This is the model class for table "agr_companyLicense".
 *
 * The followings are the available columns in table 'agr_companyLicense':
 * @property string $id
 * @property string $agr_companyId
 * @property string $number
 * @property string $kindOfActivity
 * @property string $authoritiesId
 * @property string $addressId
 */
class AgrCompanyLicense extends AgrActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrCompanyLicense the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agr_companyLicense';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, agr_companyId, authoritiesId, addressId', 'length', 'max'=>36),
			array('number', 'length', 'max'=>100),
			array('kindOfActivity', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, agr_companyId, number, kindOfActivity, authoritiesId, addressId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agr_companyId' => 'Agr Company',
			'number' => 'Number',
			'kindOfActivity' => 'Kind Of Activity',
			'authoritiesId' => 'Authorities',
			'addressId' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('agr_companyId',$this->agr_companyId,true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('kindOfActivity',$this->kindOfActivity,true);
		$criteria->compare('authoritiesId',$this->authoritiesId,true);
		$criteria->compare('addressId',$this->addressId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}