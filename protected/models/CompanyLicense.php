<?php

/**
 * This is the model class for table "companyLicense".
 *
 * The followings are the available columns in table 'companyLicense':
 * @property string $id
 * @property string $link
 * @property string $companyId
 * @property string $number
 * @property string $kindOfActivity
 * @property string $authoritiesId
 * @property string $addressId
 * @property string $image
 * @property-read string $imageUrl
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property Authorities $authorities
 * @property Company $company
 */
class CompanyLicense extends ActiveRecord
{

	private $_image;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanyLicense the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companyLicense';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, companyId, authoritiesId, addressId', 'length', 'max'=>36),
			array('link', 'length', 'max'=>9),
			array('number, kindOfActivity', 'length', 'max'=>150),
			//array('kindOfActivity','match','pattern'=>'/^([0-9]{2}\.[0-9]{2}\.[12]{1}[0-9]{3})+$/','allowEmpty'=>false),
			array(
				'image',
				'file',
				'types'		 => 'jpg, gif, png',
				'maxSize'	 => 1024 * 1024 * 5, // 5MB
				'allowEmpty' => 'true',
				'tooLarge'	 => 'The file was larger than 5MB. Please upload a smaller file.',
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, link, companyId, number, kindOfActivity, authoritiesId, addressId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'authorities' => array(self::BELONGS_TO, 'Authorities', 'authoritiesId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'link' => 'Link',
			'companyId' => 'Company',
			'number' => 'Number',
			'kindOfActivity' => 'Kind Of Activity',
			'authoritiesId' => 'Authorities',
			'addressId' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('kindOfActivity',$this->kindOfActivity,true);
		$criteria->compare('authoritiesId',$this->authoritiesId,true);
		$criteria->compare('addressId',$this->addressId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getUploadPath() {

		$path = Yii::getPathOfAlias('uploads.company') . '/' . $this->company->link . '/address/' . $this->address->link;

		if ( !is_dir($path) )
			mkdir($path, 0775, true);
		return $path;
	}

	public function getUploadUrl() {

		return Yii::app()->baseUrl.'/uploads/company'. '/' . $this->company->link . '/address/' . $this->address->link;
	}

	public function getImagePath() {
		return "{$this->uploadPath}/license_{$this->link}.jpg";
	}

	public function getImage() {

		if ( !isset($this->_image) )
			$this->_image = is_file($this->imagePath) ? "license_{$this->link}.jpg" : false;

		return $this->_image;
	}

	public function setImage($value) {
		$this->_image = $value;
	}

	public function getImageUrl() {

		return $this->image ? "{$this->uploadUrl}/{$this->image}" : false;
	}
}