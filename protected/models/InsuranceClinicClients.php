<?php

/**
 * This is the model class for table "insuranceClinicClients".
 *
 * The followings are the available columns in table 'insuranceClinicClients':
 * @property string $id
 * @property string $insuranceId
 * @property string $companyId
 */
class InsuranceClinicClients extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InsuranceClinicClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'insuranceClinicClients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('insuranceId, companyId', 'required'),
			array('id, insuranceId, companyId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, insuranceId, companyId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company'   =>[self::BELONGS_TO,'Company','companyId'],
			'insurance' =>[self::BELONGS_TO,'Company','insuranceId']
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'insuranceId' => 'Insurance',
			'companyId' => 'Company',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('insuranceId',$this->insuranceId,true);
		$criteria->compare('companyId',$this->companyId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}