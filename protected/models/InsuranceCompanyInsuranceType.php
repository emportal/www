<?php

/**
 * This is the model class for table "insuranceCompanyInsuranceType".
 *
 * The followings are the available columns in table 'insuranceCompanyInsuranceType':
 * @property string $id
 * @property string $insuranceCompanyId
 * @property string $insuranceTypeId
 *
 * The followings are the available model relations:
 * @property InsuranceType $insuranceType
 * @property InsuranceCompany $insuranceCompany
 */
class InsuranceCompanyInsuranceType extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InsuranceCompanyInsuranceType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'insuranceCompanyInsuranceType';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, insuranceCompanyId, insuranceTypeId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, insuranceCompanyId, insuranceTypeId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'insuranceType' => array(self::BELONGS_TO, 'InsuranceType', 'insuranceTypeId'),
			'insuranceCompany' => array(self::BELONGS_TO, 'InsuranceCompany', 'insuranceCompanyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'insuranceCompanyId' => 'Insurance Company',
			'insuranceTypeId' => 'Insurance Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('insuranceCompanyId',$this->insuranceCompanyId,true);
		$criteria->compare('insuranceTypeId',$this->insuranceTypeId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}