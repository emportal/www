<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $surName
 * @property string $firstName
 * @property string $fatherName
 * @property string $password
 * @property string $email
 * @property string $cityId
 *
 * The followings are the available model relations:
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property City $city
 */
class User extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, cityId', 'length', 'max'=>36),
			array('name, link, surName, firstName, fatherName, password, email', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, surName, firstName, fatherName, password, email, cityId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'UsersId'),
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
			'phoneVerification' => array(self::HAS_ONE,'PhoneVerification','userId'),
			'phoneVerificationCount' => array(self::STAT,'PhoneVerification','userId')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'surName' => 'Sur Name',
			'firstName' => 'First Name',
			'fatherName' => 'Father Name',
			'password' => 'Password',
			'email' => 'Email',
			'cityId' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('surName',$this->surName,true);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('fatherName',$this->fatherName,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('cityId',$this->cityId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}