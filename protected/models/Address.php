<?php

/**
 * This is the model class for table "address".
 *
 * The followings are the available columns in table 'address':
 * @property string $id
 * @property string $name
 * @property string $seoName
 * @property string $link
 * @property string $linkUrl
 * @property string $ownerId
 * @property string $countryId
 * @property string $postIndex
 * @property string $regionId
 * @property string $cityId
 * @property string $cityDistrictId
 * @property string $street
 * @property string $houseNumber
 * @property integer $longitude
 * @property integer $latitude
 * @property string $description
 * @property integer $lunchBreak
 * @property string $breakStart
 * @property string $breakFinish
 * @property integer $forCompany
 * @property integer $forProducers
 * @property-read bool $isFavorite
 * @property integer $isActive
 * @property float $rating
 * @property float $ratingSystem
 * @property float $ratingUser
 * @property float $ratingSite
 * @property integet $samozapis
 * @property string $legalName
 * @property string $legalAddress
 * @property string $legalINN
 *
 * The followings are the available model relations:
 * @property CityDistrict $cityDistrict
 * @property City $city
 * @property Country $country
 * @property Region $region
 * @property AddressCompanyUnit[] $addressCompanyUnits
 * @property AddressServices[] $addressServices
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property Bill[] $bills
 * @property Comment[] $comments
 * @property Company $company
 * @property Company[] $companies
 * @property CompanyLicense[] $companyLicenses
 * @property ContactInformation[] $contactInformations
 * @property ContactPersonCompany[] $contactPersonCompanies
 * @property Email[] $emails
 * @property InsuranceCompanyLicense[] $insuranceCompanyLicenses
 * @property NearestMetroStation[] $nearestMetroStations
 * @property-read MetroStation $nearestMetroStation
 * @property MetroStation[] $metroStations
 * @property Payment[] $payments
 * @property Phone[] $phones
 * @property PlaceOfWork[] $placeOfWorks
 * @property Producer[] $producers
 * @property ProducerOffice[] $producerOffices
 * @property UserDrugstore[] $userDrugstores
 * @property UserInsurance[] $userInsurances
 * @property UserMedical[] $userMedicals
 * @property UserPatronage[] $userPatronages
 * @property UserShops[] $userShops
 * @property UserSuppliers[] $userSuppliers
 * @property UserTourism[] $userTourisms
 * @property WorkingHour[] $workingHours
 * @property WorkingHoursOfDoctors[] $workingHoursOfDoctors
 * @property AgrAction	$agr_actionCount		
 * @property AgrEmail	$agr_emailCount			
 * @property AgrRef		$agr_refCount		
 * @property AgrPhone	$agr_phoneCount			
 * @property AgrDoctor	$agr_doctorCount
 * @property AgrAddress $agr_address	
 * @property AgrCompanyLicense	$agr_companyLicenseCount
 * @property SalesContract $salesContract
 */
class Address extends ActiveRecord {

	public static $showInactive = false;
	
	public $managerId;
	public $nameLike;
    private $debtDataCached;
	
	public static $arrGMU = array(
		'e909b718-2b35-11e2-b014-e840f2aca94f',
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f',
		'c06a39cb-f532-11e2-856f-e840f2aca94f',
		'93b817de-ee70-11e2-856f-e840f2aca94f',
		'c06a39ca-f532-11e2-856f-e840f2aca94f',
		'22075224-e934-11e2-856f-e840f2aca94f'
	);
	public $addressId;
	public $workingHoursStep = array('11111' => 'круглосуточно',
		'00:00' => '00:00', '00:30' => '00:30', '01:00' => '01:00', '01:30' => '01:30',
		'02:00' => '02:00', '02:30' => '02:30', '03:00' => '03:00', '03:30' => '03:30',
		'04:00' => '04:00', '04:30' => '04:30', '05:00' => '05:00', '05:30' => '05:30',
		'06:00' => '06:00', '06:30' => '06:30', '07:00' => '07:00', '07:30' => '07:30',
		'08:00' => '08:00', '08:30' => '08:30', '09:00' => '09:00', '09:30' => '09:30',
		'10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30',
		'12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30',
		'14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30',
		'16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30',
		'18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30',
		'20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00', '21:30' => '21:30',
		'22:00' => '22:00', '22:30' => '22:30', '23:00' => '23:00', '23:30' => '23:30',
	);
	public $breakHoursStep = array(
		'00:00' => '00:00', '00:30' => '00:30', '01:00' => '01:00', '01:30' => '01:30',
		'02:00' => '02:00', '02:30' => '02:30', '03:00' => '03:00', '03:30' => '03:30',
		'04:00' => '04:00', '04:30' => '04:30', '05:00' => '05:00', '05:30' => '05:30',
		'06:00' => '06:00', '06:30' => '06:30', '07:00' => '07:00', '07:30' => '07:30',
		'08:00' => '08:00', '08:30' => '08:30', '09:00' => '09:00', '09:30' => '09:30',
		'10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30',
		'12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30',
		'14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30',
		'16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30',
		'18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30',
		'20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00', '21:30' => '21:30',
		'22:00' => '22:00', '22:30' => '22:30', '23:00' => '23:00', '23:30' => '23:30',
	);
	
	private $cachedBills = [];

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Address the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lunchBreak, forCompany, forProducers,isActive', 'numerical', 'integerOnly' => true),
			array('longitude, latitude', 'numerical'),
			array('id, addressId, countryId, regionId, cityId, cityDistrictId', 'length', 'max' => 36),
			array('seoName, seoTitle, link, ownerId, postIndex, street, houseNumber', 'length', 'max' => 150),
			array('name', 'length', 'max' => 200),
			array('legalName, legalAddress', 'length', 'max' => 250),
			array('legalINN', 'length', 'max' => 20),
			array('legalPostalAddressCity,legalPostalAddressStreet,legalPostalAddressHouse,legalPostalAddressCode', 'length', 'max' => 150),
			array('breakStart, breakFinish, samozapis, current', 'safe'),
			array('description', 'filter', 'filter' => function($v) { return strip_tags($v); }),
			array('description', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, seoName, seoTitle, link, ownerId, countryId, postIndex, regionId, cityId, cityDistrictId, street, houseNumber, longitude, latitude, description, lunchBreak, breakStart, breakFinish, forCompany, forProducers, samozapis, legalName, legalAddress, legalINN', 'safe', 'on' => 'search'),
			['cityId, street, houseNumber', 'required', 'on' => 'self_register'],
            ['isActive', 'safe', 'on' => 'changeActivityStatus'],
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'attributeAddress' => array(self::HAS_MANY, 'AttributeAddress', 'addressId'),
			'addressCompanyUnits' => array(self::HAS_MANY, 'AddressCompanyUnit', 'addressId'),
			'addressServices' => array(self::HAS_MANY, 'AddressServices', 'addressId'),
			'addressServicesName' => array(self::MANY_MANY, 'Service', 'addressServices(addressId,serviceId)'),
			'addressActivites' => array(self::MANY_MANY, 'CompanyActivite', 'addressServices(addressId,companyActivitesId)', 'order' => 'name', 'condition' => "addressActivites.id <> ''", 'group' => 'companyActivitesId'),
			'agrphones' => array(self::HAS_MANY, 'AgrPhone', 'addressId', 'order' => 'agrphones.name'),
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'addressId'),
			'bills' => array(self::HAS_MANY, 'Bill', 'addressId'),
			'cityDistrict' => array(self::BELONGS_TO, 'CityDistrict', 'cityDistrictId'),
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
			'country' => array(self::BELONGS_TO, 'Country', 'countryId'),
			'company' => array(self::BELONGS_TO, 'Company', 'ownerId', 'together' => true),
			'companies' => array(self::HAS_MANY, 'Company', 'addressesId'),
			'companyLicenses' => array(self::HAS_MANY, 'CompanyLicense', 'addressId'),
			'contactInformations' => array(self::HAS_MANY, 'ContactInformation', 'addressId'),
			'contactPersonCompanies' => array(self::HAS_MANY, 'ContactPersonCompany', 'addressId'),
			'companyActivites' => array(self::HAS_MANY, 'CompanyActivites', 'addressId'),
			'companyActivitesGroupByName' => array(self::MANY_MANY, 'CompanyActivite', 'companyActivites(addressId,companyActivitesId)', 'order' => 'name'),
			'emails' => array(self::HAS_MANY, 'Email', 'addressId'),
			'insuranceCompanyLicenses' => array(self::HAS_MANY, 'InsuranceCompanyLicense', 'addressId'),
			'nearestMetroStations' => array(self::HAS_MANY, 'NearestMetroStation', 'addressId', 'order' => 'distance'),
			'metroStations' => array(self::MANY_MANY, 'MetroStation', 'nearestMetroStation(addressId,metroStationId)', 'order' => '`metroStations_metroStations`.`distance`'),
			'payments' => array(self::HAS_MANY, 'Payment', 'addressId', 'order' => 'createDate'),
			'phones' => array(self::HAS_MANY, 'Phone', 'addressId', 'order' => 'phones.name'),
			'placeOfWorks' => array(self::HAS_MANY, 'PlaceOfWork', 'addressId'),
			'producers' => array(self::HAS_MANY, 'Producer', 'mainAddressId'),
			'producerOffices' => array(self::HAS_MANY, 'ProducerOffice', 'addressId'),
			'refs' => array(self::HAS_MANY, 'Ref', 'ownerId'),
			'region' => array(self::BELONGS_TO, 'Region', 'regionId'),
			'userDrugstores' => array(self::HAS_MANY, 'UserDrugstore', 'addressId'),
			'userInsurances' => array(self::HAS_MANY, 'UserInsurance', 'addressId'),
			'userMedicals' => array(self::HAS_ONE, 'UserMedical', 'addressId'),
			'userPatronages' => array(self::HAS_MANY, 'UserPatronage', 'addressId'),
			'userShops' => array(self::HAS_MANY, 'UserShops', 'addressId'),
			'userSuppliers' => array(self::HAS_MANY, 'UserSuppliers', 'addressId'),
			'userTourisms' => array(self::HAS_MANY, 'UserTourism', 'addressId'),
			'_workingHours' => array(self::HAS_ONE, 'WorkingHour', 'addressId'),
			'workingHoursOfDoctors' => array(self::HAS_MANY, 'WorkingHoursOfDoctors', 'addressId'),
			/* 'comments'			 => array(self::HAS_MANY, 'Comment', 'addressId', 'scopes' => 'moderated'),
			  'commentsAll'			 => array(self::HAS_MANY, 'Comment', 'addressId'), */
			'comments' => array(self::HAS_MANY, 'RaitingCompanyUsers', 'addressId', 'scopes' => 'moderated'),
			#'comments_experts'			 => array(self::HAS_MANY, 'RaitingDoctorUsers', 'addressId', 'scopes' => 'moderated'),
			'commentsAll' => array(self::HAS_MANY, 'RaitingCompanyUsers', 'addressId'),
			'doctors' => array(self::HAS_MANY, 'Doctor', 'doctorId', 'through' => 'placeOfWorks'),
			//Счетчики данных внутри agr_таблиц
			'agr_actionCount' => array(self::STAT, 'AgrAction', 'addressId'),
			'agr_emailCount' => array(self::STAT, 'AgrEmail', 'addressId'),
			'agr_address' => array(self::STAT, 'AgrAddress', 'id'),
			'agr_refCount' => array(self::STAT, 'AgrRef', 'ownerId'),
			'agr_phoneCount' => array(self::STAT, 'AgrPhone', 'addressId'),
			'agr_doctorCount' => array(self::STAT, 'AgrDoctor', 'agr_placeOfWork(addressId,doctorId)'),
			'agr_companyLicenseCount' => array(self::STAT, 'AgrCompanyLicense', 'addressId'),
			'salesContract' => array(self::HAS_ONE, 'SalesContract', 'addressId'),
            //'extAddress' => array(self::HAS_ONE, 'ExtAddress', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Адрес',
			'seoName' => 'seoName',
			'seoTitle' => 'seoTitle',
			'link' => 'Link',
			'ownerId' => 'Owner',
			'countryId' => 'Country',
			'postIndex' => 'Post Index',
			'regionId' => 'Region',
			'cityId' => 'City',
			'cityDistrictId' => 'City District',
			'street' => 'Street',
			'houseNumber' => 'House Number',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
			'description' => 'Description',
			'lunchBreak' => 'Lunch Break',
			'breakStart' => 'Break Start',
			'breakFinish' => 'Break Finish',
			'forCompany' => 'For Company',
			'forProducers' => 'For Producers',
			'samozapis' => 'Самозапись',
			'legalName' => 'Юридическое название',
			'legalAddress' => 'Юридический адрес',
			'legalINN' => 'ИНН юрлица',
			'legalPostalAddressCity' => 'Город',
			'legalPostalAddressStreet' => 'Почтовый адрес',
			'legalPostalAddressHouse' => 'Дом',
			'legalPostalAddressCode' => 'Индекс',
            'legalKPP' => 'КПП',
            'legalOGRN' => 'ОГРН',
		);
	}
	public function attributeDescription() {
		return array(
			'legalName' => 'Полное наименование юридического лица, включая форму юрлица',
			'legalAddress' => 'Полный юридический адрес, включая почтовый индекс и город',
			'legalINN' => 'Индивидуальный номер налогоплательщика',
			'legalPostalAddressStreet' => 'Почтовый адрес, на который будут приходить закрывающие документы. Если не указан, будет использоваться юридический адрес.',
            'legalKPP' => 'Код причины постановки на учет (9 символов)',
            'legalOGRN' => 'Основной государственный регистрационный номер (13 символов)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
		
		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('seoName', $this->seoName);
		$criteria->compare('seoTitle', $this->seoName);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('ownerId', $this->ownerId, true);
		$criteria->compare('countryId', $this->countryId, true);
		$criteria->compare('postIndex', $this->postIndex, true);
		$criteria->compare('regionId', $this->regionId, true);
		$criteria->compare('cityId', $this->cityId, true);
		$criteria->compare('cityDistrictId', $this->cityDistrictId, true);
		$criteria->compare('street', $this->street, true);
		$criteria->compare('houseNumber', $this->houseNumber, true);
		$criteria->compare('longitude', $this->longitude);
		$criteria->compare('latitude', $this->latitude);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('lunchBreak', $this->lunchBreak);
		$criteria->compare('breakStart', $this->breakStart, true);
		$criteria->compare('breakFinish', $this->breakFinish, true);
		$criteria->compare('forCompany', $this->forCompany);
		$criteria->compare('forProducers', $this->forProducers);
		$criteria->compare('samozapis', $this->samozapis);
		$criteria->compare('legalName', $this->legalName);
		$criteria->compare('legalAddress', $this->legalAddress);
		$criteria->compare('legalINN', $this->legalINN);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function scopes() {
		return [
			'active' => [
				'condition' => $this->tableAlias . ".isActive = '1'"
			]
		];
	}

	public function beforeFind() {
		parent::beforeFind();

		if(Address::$showInactive === false) {
			$criteria = $this->dbCriteria;
			$criteria->addCondition($this->tableAlias . ".isActive = '1'");
			$this->dbCriteria = $criteria;
		}
	}
	
	public function afterSave()
	{
		if (parent::afterSave())
		{
			if ($this->isNewRecord)
			{
				//надо ставить дефолтные часы приема
				$workingHour = new WorkingHour();
				$workingHour->attributes = ['addressId' => $this->id];
				$workingHour->setHoursForAllDays();
				$workingHour->save();
			}

			if($this->samozapis == 0 && $this->canGetAppointments()) {
				$this->addService(Service::model()->findByLink('zapnp')->id);
			}

			$placeOfWork = PlaceOfWork::model()->findAll("addressId = :addressId", ['addressId' => $this->id]);
			foreach ($placeOfWork as $model) {
				Yii::app()->cache->delete(md5('timeBlock'.$model->addressId.$model->doctorId));
			}	

			return true;
		} else {
			return false;
		}		
	}
	
	public function getNearestMetroStation() {
		return reset($this->metroStations);
	}

	public function getWorkingHours() {

		if (empty($this->_workingHours)) {
			$this->_workingHours = new WorkingHour();
			$this->_workingHours->addressId = $this->id;
			$this->_workingHours->id = ActiveRecord::create_guid(array($this->id));
			/*
			  $this->_workingHours = array(
			  new WorkingHour(),
			  new WorkingHour(),
			  new WorkingHour(),
			  new WorkingHour(),
			  new WorkingHour(),
			  new WorkingHour(),
			  new WorkingHour(),
			  );


			  $weekdays = Weekdays::model()->findAll(array('index' => 'order'));

			  foreach ($this->_workingHours as $key => &$val) {
			  $val->address	 = $this;
			  $val->weekday	 = $weekdays[$key + 1];
			  $val->save();
			  }
			 */
		}

		return $this->_workingHours;
	}

	/**
	 *
	 * @param array $value
	 * @return void
	 */
	public function setWorkingHours($value) {
		$this->getWorkingHours();

		$this->_workingHours->attributes = $value;

		$this->_workingHours->save();
		/*
		  if ( !is_array($value) )
		  return;
		  foreach ($this->_workingHours as $key => $row) {
		  $row->attributes = $value[$key];
		  $row->save();
		  }
		  //$this->_workingHours = $value; */
	}

	public function getName()
    {
		return CHtml::encode($this->company->name . ' на ' . $this->street . ', ' . $this->houseNumber);
	}
	
	public function getRelativeLinkTag()
    {
		return '<a href="' . $this->getPagesUrls(true, true) . '" target="_blank">' . $this->getName() . '</a>';
	}
	
	public function getLinkName()
    {
		return '<a href="' . $this->linkHref . '" target="_blank" >' . $this->getName() . '</a>';
	}
	

	public function getPostalData() {
    	$postalCode = $this->postIndex ? $this->postIndex : NULL;
    	$cityName = $this->city->name;
    	$street = $this->street;
    	$houseNumber = $this->houseNumber ? $this->houseNumber : NULL;
		if (!empty($this->legalPostalAddressStreet)) {
		   	$postalCode = $this->legalPostalAddressCode;
		   	$cityName = $this->legalPostalAddressCity;
			$street = $this->legalPostalAddressStreet;
    		$houseNumber = $this->legalPostalAddressHouse;
		}
		return [
				'postalCode' => $postalCode,
				'cityName' => $cityName,
				'street' => $street,
				'houseNumber' => $houseNumber,
		];
	}
    /**
     * Получение адреса офиса для отправки закрывающих документов
     * 
	 * @param boolean $onlyActualAddress true чтобы получить почтовый адрес фактического месторасположения
	 * 
     * @return string полный почтовый адрес
     */
    public function getPostalAddress($onlyActualAddress = false)
    {
    	$postIndex = $this->postIndex ? $this->postIndex : NULL;
    	$cityName = $this->city->name;
    	$street = $this->street;
    	$houseNumber = $this->houseNumber ? $this->houseNumber : NULL;
		if (!$onlyActualAddress) {
			if (!empty($this->legalPostalAddressStreet)) {
		    	$postIndex = $this->legalPostalAddressCode;
		    	$cityName = $this->legalPostalAddressCity;
				$street = $this->legalPostalAddressStreet;
    			$houseNumber = $this->legalPostalAddressHouse;
			}
			elseif (preg_match("/[0-9]{6}/", $this->legalAddress)) {
				$street = $this->legalAddress;
			}
		}
        $chunks = [];
        $chunks[] = $postIndex;
        $chunks[] = $cityName;
        $chunks[] = $street;
        $chunks[] = $houseNumber;
        $result = implode(', ', array_filter($chunks));
        return $result;
    }
    
    /* public function getPostalData()
    {
        $data = [];
        $data['postIndex'] = $this->postIndex ? $this->postIndex : NULL;
        $data['postIndex'] = $this->postIndex ? $this->postIndex : NULL;
        $data['cityName'] = 'г. ' . $this->city->name;
        $data['street'] = $this->street;
        $data['houseNumber'] = $this->houseNumber ? $this->houseNumber : NULL;
        $data['flat'] = NULL;
    } */
	
	public function getLinkHref()
    {
		return $this->getPagesUrls(true);
	}

	public function defaultScope()
    {
		return array(
			'with' => array(
			),
		);
	}

	public function getIsFavorite()
    {
		if (Yii::app()->user->isGuest)
			return false;

		return in_array($this->id, Yii::app()->user->model->favoritesArray);
	}

	public function setPhones($data) {
		#var_dump($data);die();
		if (!is_array($data)) {
			$data = $data ? array($data) : array();
		}
		$moderate = array();
		$active = array();
		$allPhones = CHtml::listData($this->agrphones, 'id', 'id');

		foreach ($data as $ph) {
			if (!$ph['number'])
				continue;
			$ph['name'] = $ph['countryCode'] . ' ' . $ph['cityCode'] . ' ' . $ph['number'];
			$moderate[$ph['name']] = "+-" . $ph['name'];
		}

		$pre_active = CHtml::listData($this->phones, 'name', 'name');
		if (!$pre_active) {
			$pre_active = array();
		}
		#var_dump($moderate,"<br>");
		array_walk($moderate, function(&$value) {
			$value = preg_replace("/(\+|-)/", "", $value);
		});
		array_walk($pre_active, function(&$value) {
			$value = preg_replace("/(\+|-)/", "", $value);
		});
		foreach ($pre_active as $act) {
			$active[$act] = $act;
		}

		$diff = array_diff($moderate, $active);
		$diff1 = array_diff($active, $moderate);

		//var_dump($diff,$diff1,$allPhones,"<br><br>",$active,"<br><br>",$pre_active,"<br><br>",$moderate);die();
		if ($diff || $diff1 || (count($pre_active) != count($moderate))) {

			foreach ($data as $key => $phone) {
				if (is_object($phone)) {
					continue;
				}

				if (is_array($phone)) {

					if (!$object = AgrPhone::model()->findByAttributes(array('ownerId' => $this->company->id, 'link' => $phone['link']))) {
						$object = new AgrPhone();
						$object->ownerId = $this->company->id;
						$object->addressId = $this->id;
					}

					$phone['name'] = $phone['countryCode'] . ' ' . $phone['cityCode'] . ' ' . $phone['number'];

					$object->attributes = $phone;

					$object->link = md5($phone['name']);
					$object->id = ActiveRecord::create_guid($object->attributes);
					$object->syncThis = false;


					if (!$object->save()) {

						/*  var_dump($phone);
						  var_dump($object->getErrors()); */

						unset($data[$key]);
						continue;
					} else {

						unset($allPhones[$object->id]);
					}

					$phone = $object;
				} else {
					//$phone = Phone::model()->findByPk($phone);
				}
			}

			if (!$data) {
				$object = new AgrPhone();
				$object->ownerId = $this->id;
				$object->addressId = $this->id;
				$object->name = "remove";
				$object->save(false);
			}

			if (count($allPhones) && is_array($allPhones)) {

				$criteria = new CDbCriteria;
				$criteria->condition = "id IN ('" . implode("','", $allPhones) . "')";
				AgrPhone::model()->deleteAll($criteria);
			}
		} else {
			AgrPhone::model()->deleteAll("addressId=:id", array(
				':id' => $this->id
			));
		}


		/* var_dump($allPhones);
		  die(); */
		/* У нас нет первичного ключа, поэтому автоматом не будет сохраняться */
		//$this->phones = $data;
		//$this->save();
	}

	public function setCompanyLicenses($data) {
		if (!is_array($data))
			$data = $data ? array($data) : array();

		$allData = CHtml::listData($this->companyLicenses, 'id', 'id');

		foreach ($data as $key => &$license) {
			if (is_object($license) || $key < 0) {
				continue;
			}

			if (is_array($license)) {
				if (!$object = CompanyLicense::model()->findByAttributes(array('addressId' => $this->id, 'link' => $license['link']))) {
					$object = new CompanyLicense();
					$object->addressId = $this->id;
				}
				$object->attributes = $license;
				$object->company = $this->company;

				if (!$object->save()) {
					unset($data[$key]);
					continue;
				} else {
					unset($allData[$object->id]);
				}
				$license = $object;
			} else {
				$license = CompanyLicense::model()->findByPk($license);
			}
		}

		if (count($allData) && is_array($allData)) {

			$criteria = new CDbCriteria;
			$criteria->condition = "id IN ('" . implode("','", $allData) . "')";

			CompanyLicense::model()->deleteAll($criteria);
		}

		$this->companyLicenses = $data;
		$this->save();
	}

	public function behaviors() {
		return array(
			'galleryBehavior' => array(
				'class' => 'gallery.GalleryBehavior',
				'idAttribute' => 'link',
				'versions' => array(
					'small' => array(
						'centeredpreview' => array(98, 98),
					),
					'medium' => array(
						'resize' => array(800, null),
					)
				),
				'name' => true,
				'description' => true,
			),
			'linkUrlBehavior' => array(
				'class' => 'application.components.behaviors.LinkUrlBehavior',				
			)
		);
	}

	public function getUserMedical() {
		return UserMedical::model()->find('addressId=:addressId', array(
					'addressId' => $this->id
		));
	}

	//Метод возвращает статус - есть ли измененные поля у адреса
	public function getStatus() {
		$agr_actionsCount = $this->agr_actionCount;
		$agr_refCount = $this->agr_refCount;
		$agr_emailCount = $this->agr_emailCount;
		$agr_phoneCount = $this->agr_phoneCount;
		$agr_doctorCount = $this->agr_doctorCount;
		$agr_companyCount = $this->company->agr_companyCount;
		$agr_companyLicenseCount = $this->agr_companyLicenseCount;
		$agr_address = $this->agr_address;
		if ($agr_actionsCount || $agr_refCount || $agr_emailCount || $agr_phoneCount || $agr_doctorCount || ($agr_companyCount && $this->id === $this->company->addressId) || $agr_address) {
			return 1;
		}
		return 0;
	}

	public function getIsHospital() {
		$criteria = new CDbCriteria();
		$criteria->with = [
			'valueAttribute' => [
				'scopes' => 'isHospital',
			]
		];
		$criteria->compare('t.addressId', $this->id);

		$value = AttributeAddress::model()->find($criteria);
		return !empty($value);
	}

	public function setIsHospital($value) {
		$isHospitalId = ValueAttribute::model()->isHospital()->find()->id;

		if ($value) {
			$criteria = new CDbCriteria();
			$criteria->compare('t.addressId', $this->id);
			$criteria->compare('t.valueAttributeId', $isHospitalId);
			$isHospital = AttributeAddress::model()->find($criteria);
			if (empty($isHospital)) {
				$isHospital = new AttributeAddress();
				$isHospital->addressId = $this->id;
				$isHospital->valueAttributeId = $isHospitalId;
				$isHospital->save();
			}
		} else {
			AttributeAddress::model()->deleteAllByAttributes([
				'addressId' => $this->id,
				'valueAttributeId' => $isHospitalId
			]);
		}
	}

	public function saveMetroList($list) {
		NearestMetroStation::model()->DeleteAllByAttributes([
			'addressId' => $this->id
		]);
		if ($list) {
			foreach ($list as $row) {
				$metro = new NearestMetroStation();
				$metro->addressId = $this->id;
				$metro->metroStationId = $row['value'];
				$metro->distance = intval($row['distance']);
				$metro->save();
			}
		}
	}

	public function getRoundedRatingSystem() {
		return (round($this->ratingSystem * 100) - round($this->ratingSystem * 100) % 10) / 100;
	}

	public function getRoundedRatingUser() {
		return (round($this->ratingUser * 100) - round($this->ratingUser * 100) % 10) / 100;
	}

	public function getRoundedRatingSite() {
		return (round($this->ratingSite * 100) - round($this->ratingSite * 100) % 10) / 100;
	}
	
	public function addresssDataProviderForAdminPanel() {
	
		$criteria = new CDbCriteria;
		
		$criteria->with = [
			'company' => [
				'together' => true,
				'sort' => '(`companyContracts`.`id` IS NOT NULL) AS contract,contractNumber,name'
			],
			'userMedicals' => [
				'together' => true,	
			],
		];
		
		$criteria->order = 'company.name ASC';
		//$criteria->params = array('companyName' => $userId); //userMedicals.agreementNew
		
		$criteria->addCondition('cityId  = \'534bd8b8-e0d4-11e1-89b3-e840f2aca94f\''); //город
		$criteria->addCondition('userMedicals.agreementNew = 1');
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 10,
			)
		));
	}
	
	
	public function dataProviderForNewAdminPanel($options = null) {
	
		$criteria = new CDbCriteria;
		
		$criteria->with = [
			'company' => [
				'together' => true,
				'sort' => '(`companyContracts`.`id` IS NOT NULL) AS contract,contractNumber,name',
				'with' => [
					'managerRelations' => [
						'together' => true,
						'select' => '(`managerRelations`.`userId` != 1) AS id,userId'
					],
				]
			],
			'userMedicals' => [
				'together' => true,	
			],
		];
		
		$criteria->compare('ownerId', $this->ownerId, true);
		
		if (!empty($this->nameLike))
			$criteria->addCondition('company.name LIKE "%'.$this->nameLike.'%"');
		if (isset($options['samozapis']))
			$criteria->compare('t.samozapis', $options['samozapis'], false);
			
		if (!empty($this->managerId))
		{
			if ($this->managerId == -1) {				
				$criteria->addCondition('managerRelations.userId IS NULL');
			} elseif ($this->managerId == -2) {				
				$criteria->addCondition('managerRelations.userId IS NOT NULL');
			} else {
				$criteria->addCondition('managerRelations.userId LIKE "'.$this->managerId.'"');
			}	
		}
		
		if (!empty($options['region']))
		{
			$cityId = City::model()->getCityIdBySubdomain($options['region']);
			$criteria->compare('t.cityId', $cityId, false);
		} elseif (!empty($options['appointmentPeriod'])) {
			//все города
		} else {
			//все города
		}
		
		if ($options['appointmentPeriod'])// && $options['appointmentPeriod']['start'] && $options['appointmentPeriod']['end'])
		{
			$criteria->with = array_merge($criteria->with, [
				'appointmentToDoctors' => [
					'together' => true,
					'select' => false,
					'with' => [
						'company' => [
							'alias' => 'appOwner'
						]
					]
				]
			]);
			$criteriaAppointmentPeriod = new CDbCriteria();
			$criteriaAppointmentPeriod->addCondition("appointmentToDoctors.statusId != " . Yii::app()->db->quoteValue(AppointmentToDoctors::DISABLED));
			$criteriaAppointmentPeriod->addCondition("appointmentToDoctors.plannedTime LIKE " . Yii::app()->db->quoteValue( $options['appointmentPeriod'] . "%"));
		}
		
		if ($options['includeFixedContracts'])// && $options['appointmentPeriod']['start'] && $options['appointmentPeriod']['end'])
		{
			$criteria->with = array_merge($criteria->with, [
				'salesContract' => [
					//'together' => true,
					//'select' => false,
					'with' => [
						'salesContractType' => [
							//'alias' => 'appOwner'
						]
					]
				]
			]);
			$criteriaIncludeFixedContracts = new CDbCriteria();
			$criteriaIncludeFixedContracts->addCondition("salesContractType.isFixed = 1");
		}
		if (isset($criteriaAppointmentPeriod) && isset($criteriaIncludeFixedContracts))
		{
			$criteriaAppointmentPeriod->mergeWith($criteriaIncludeFixedContracts, 'OR');
			$criteria->mergeWith($criteriaAppointmentPeriod, 'AND');
		} elseif (isset($criteriaAppointmentPeriod)) {
			$criteria->mergeWith($criteriaAppointmentPeriod, 'AND');
		} elseif (isset($criteriaIncludeFixedContracts)) {
			$criteria->mergeWith($criteriaIncludeFixedContracts, 'AND');
		}
		
		$criteria->order = 'company.name ASC';
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false
		));
	}
	
	public static function getExtAddress($addressId)
    {
		$extAddress = ExtAddress::model()->findByAttributes(['addressId' => $addressId]);
		return $extAddress;
	}
	
	public function getTotalSale($dataProvider)
	{
		$totalsale = 0;
		$salesContractType = $this->salesContract->salesContractType;
		if ($salesContractType->isFixed) {
			$totalsale = $salesContractType->price;
		} else {
			foreach($dataProvider->getData() as $data) {
				$totalsale += $data->empComission;
			}
		}
		return (int)$totalsale;
	}
	
	/* получить имя без города и почтового индекса */
	public function getShortName() {
		return $this->street . ', ' . $this->houseNumber;
	}
	
	public function getDayNames() {
		return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
	}
	
	public function getClosestWorkingTime() {
		//date_default_timezone_set('Europe/Moscow');
		$timeLag = 0; //4 * 60 * 60;
		$currentTime = time();
		$closestWorkingTime = $currentTime;
		$currentDayName = date('l');
		$workingHour = $this->_workingHours;
		$workingHours = [ ];
		//echo 'current time = ' . gmdate('Y-m-d H:i:s', $currentTime) . ', today is ' . $currentDayName;
		//echo 'workingHour = ' . print_r($workingHour->attributes);
		
		foreach($this->dayNames as $dayName)
		{
			$hoursStart = $workingHour->{$dayName . 'Start'};
			$hoursFinish = $workingHour->{$dayName . 'Finish'};
			
			if ($hoursStart == '11111')
				$hoursStart = '00:00';
			if ($hoursFinish == '11111')
				$hoursFinish = '23:59';
			
			if ($currentDayName == $dayName)
			{
				$cursorDayTimeStartStr = date('Y-m-d ' . $hoursStart, (strtotime('this ' . $dayName)));
				$cursorDayTimeFinishStr = date('Y-m-d ' . $hoursFinish, (strtotime('this ' . $dayName)));
				$cursorDayTimeStart = strtotime($cursorDayTimeStartStr);
				$cursorDayTimeFinish = strtotime($cursorDayTimeFinishStr);
				
				if (($currentTime >= $cursorDayTimeStart) && ($currentTime <= $cursorDayTimeFinish))
				{
					$closestWorkingTime = $currentTime;
					break;
				}
			}
			
			$cursorDayTimeStartStr = date('Y-m-d ' . $hoursStart, (strtotime('next ' . $dayName))); //rm //rf wtf
			$cursorDayTimeFinishStr = date('Y-m-d ' . $hoursFinish, (strtotime('next ' . $dayName)));
			
			$cursorDayTimeStart = strtotime($cursorDayTimeStartStr);
			$cursorDayTimeFinish = strtotime($cursorDayTimeFinishStr);

			if ($currentTime <= $cursorDayTimeStart)
			{
				$diff = $cursorDayTimeStart - $currentTime;
				if (!isset($workingHours['minDiff']) || $diff < $workingHours['minDiff'])
				{
					$workingHours['minDiff'] = $diff;
					$workingHours['optDay'] = [
						'dayName' => $dayName,
						'cursorDayTimeStartStr' => $cursorDayTimeStartStr,
						'cursorDayTimeFinishStr' => $cursorDayTimeFinishStr,
					];
					$closestWorkingTime = $cursorDayTimeStart;
				}
			}
			
			//echo '<br>';
			//echo '<br>' . $dayName . 'Start = ' . $hoursStart . '; cursorDayTimeStartStr = ' . $cursorDayTimeStartStr;
			//echo '<br>' . $dayName . 'Finish = ' . $hoursFinish . '; cursorDayTimeFinishStr = ' . $cursorDayTimeFinishStr;
		}
		
		$closestWorkingTime += $timeLag; //rf
		//echo '<br>closestWorkingTime = ' . gmdate('Y-m-d H:i:s', $closestWorkingTime);
		return $closestWorkingTime;
	}
	
	public function getClosestReceptionTime() {
		
		$receptionInterval = 30 * 60;
		$closestReceptionTime = ceil($this->closestWorkingTime/$receptionInterval);
		$closestReceptionTime *= $receptionInterval;
		return $closestReceptionTime;
	}
	
	public function getInfoForAppointmentReports($appModel, $periodName = null, $customOptions = null) {
		$dtOptions = [
			'manStatus' => 'r',
			'addressId' => $this->id,
			'pageSize' => '10000',
			'showServices' => 'withVisits' //записи на услуги + записи на прием
		];
		if ($customOptions)
			$dtOptions = array_merge($dtOptions, $customOptions);
		
		$dataProvider = $appModel->dataProviderForNewAdminPanel($dtOptions);
		$totalSale = $this->getTotalSale($dataProvider);
		$endLine = 'Сумма к оплате за ' . $periodName . ': ' . $totalSale . ' руб.';
		
		if ($relation = $this->company->managerRelations)
		{
			$endLine .=  ' По вопросам сверки вы можете обращаться к вашему менеджеру ';
			$endLine .= $relation->user->name . ' ';
			$endLine .= $relation->user->email . ' ';
		}
		$specification = $this->company->name . ', ' . $this->shortName;
		if ($periodName)
			$specification .= ', ' . $periodName;
		return [
			'dataProvider' => $dataProvider,
			'endLine' => $endLine,
			'specification' => $specification,
			'totalSale' => $totalSale,
		];
	}
	
	public function sendAppReportsViaEmail($mailList, $dateInterval = null)
	{
        if (!$dateInterval)
			$dateInterval = MyTools::getDateInterval($this->intervalNameForPaymentInCurrentMonth);
		/* form mail */
    	$manager = $this->company->managerRelations->user;
		$mail = new YiiMailer();
		$mail->setFrom($manager->email, 'Emportal.ru');
		foreach($mailList as $mailAddress)
		{
			$mail->AddAddress($mailAddress);
		}
		$mail->setData(['clinic' => $this]);
		$mail->setView('app_report');
		$mail->Subject = 'ЕМП: сверка записей на прием (' . $this->company->name . ', '. $this->shortName .')';
		/* add Attachment */
		$file_report = FileMaker::createHTMLAppointmentReport($this, $dateInterval, true, "pdf", false);
		$mail->AddStringAttachment($file_report[1], $file_report[0]);
		/* render view & send */
		$mail->render();
		$mail->Send();
	}

	public function sendAppBillsViaEmail($mailList, $dateInterval = null)
	{
		if (!$dateInterval)
			$dateInterval = MyTools::getDateInterval($this->intervalNameForPaymentInCurrentMonth);
		/* form mail */
    	$manager = $this->company->managerRelations->user;
		$mail = new YiiMailer();
		$mail->setFrom($manager->email, 'Emportal.ru');
		foreach($mailList as $mailAddress)
		{
			$mail->AddAddress($mailAddress);
		}
		$mail->setData(['clinic' => $this]);
		$mail->setView('app_bill');
		$mail->Subject = 'ЕМП: счёт на оплату (' . $this->company->name . ', '. $this->shortName .')';
		/* add Attachment */

		$file_bill = FileMaker::createHTMLBill($this, $dateInterval, true, "pdf");
		$file_report = FileMaker::createHTMLAppointmentReport($this, $dateInterval, true, "pdf");
		
		$mail->AddStringAttachment($file_bill[1], $file_bill[0]);
		$mail->AddStringAttachment($file_report[1], $file_report[0]);
		$mail->render();
		$mail->Send();
	}
	
	public function sendReminderToPayViaEmail($mailList, $reportMonths)
	{
    	$manager = $this->company->managerRelations->user;
		$mail = new YiiMailer();
		$mail->setFrom($manager->email, 'Emportal.ru');
		
		foreach($mailList as $mailAddress)
			$mail->AddAddress($mailAddress);

		if(!is_array($reportMonths)) {
			$reportMonths = [$reportMonths];
		}
		$allPeriods = [];
		foreach ($reportMonths as $reportMonth) {
			$allPeriods[] = MyTools::convertReportMonthToPeriodName($reportMonth);
		}
		$periodName = strtolower(join($allPeriods, ", "));
		$mail->setData([
			'dataModel' => (object)['periodName' => $periodName, 'allPeriods' => $allPeriods,],
			'clinic' => $this,
		]);
		$mail->setView('reminder_to_pay');
		$mail->Subject = 'ЕМП: напоминание о необходимости оплаты счета за ' . $periodName . ' (' . $this->company->name . ', '. $this->shortName .')';
		
		$mail->render();
		$mail->Send();
	}
    
    public function sendDeactivationNotificationViaEmail($mailList)
    {
    	$manager = $this->company->managerRelations->user;
		$mail = new YiiMailer();
		$mail->setFrom($manager->email, 'Emportal.ru');
		
		foreach($mailList as $mailAddress)
			$mail->AddAddress($mailAddress);
		
		//$periodName = MyTools::convertReportMonthToPeriodName($reportMonth);
		$mail->setData([
			//'dataModel' => (object)['periodName' => $periodName],
			'clinic' => $this,
		]);
		$mail->setView('deactivation_notification');
		$mail->Subject = 'ЕМП: уведомление о приостановке обслуживания в связи с непогашенной задолженностью';
		
		$mail->render();
		$mail->Send();
    }
    
    public function sendClaimsViaEmail($mailList)
    {
    	$manager = $this->company->managerRelations->user;
		$mail = new YiiMailer();
		$mail->setFrom($manager->email, 'Emportal.ru');
		foreach($mailList as $mailAddress)
			$mail->addAddress($mailAddress);
		$mail->setData([
			'clinic' => $this,
            'manager' => $manager,
		]);
		$mail->setView('claim_notification');
		$mail->Subject = 'Претензия (от emportal.ru)';
		$file_claim = FileMaker::createClaim($this, true, "pdf");
		$mail->addStringAttachment($file_claim[1], $file_claim[0]);
		
		$mail->render();
		$mail->send();
    }
    
    public function sendCustomEmail($mailList, $view, $subject)
    {
        if (empty($mailList))
            return false;
        
		$mail = new YiiMailer();
		$mail->setFrom('robot@emportal.ru', 'Emportal.ru');
		
		foreach($mailList as $mailAddress)
			$mail->AddAddress($mailAddress);
        
		$mail->setView($view);
		$mail->Subject = $subject;
		
		$mail->render();
		$mail->Send();
    }
	
	public function getMailListForAppReports()
	{
		$mailList = [];
		//if ($this->userMedical->email)
		//	$mailList[] = $this->userMedical->email;
		//$lprEmails = explode(' ', $this->company->companySalesInfo->email);
        $lprEmails = preg_split('/( |,|;|\r|\n|\r\n)/', $this->company->companySalesInfo->email);
		foreach($lprEmails as $lprEmail)
		{
			if (filter_var($lprEmail, FILTER_VALIDATE_EMAIL) && !in_array($lprEmail, $mailList))
				$mailList[] = $lprEmail;
		}
		return $mailList;
	}
	
	public function getMailListForReminderToPay()
	{
		$mailList = [];
		if ($this->userMedical->email && filter_var($this->userMedical->email, FILTER_VALIDATE_EMAIL))
			$mailList[] = $this->userMedical->email;
		$lprEmails = explode(' ', $this->company->companySalesInfo->email);
		foreach($lprEmails as $lprEmail)
		{
			if (filter_var($lprEmail, FILTER_VALIDATE_EMAIL) && !in_array($lprEmail, $mailList))
				$mailList[] = $lprEmail;
		}
		//return ['a.f.dyachkov@gmail.com']; //пока дебажим
		return $mailList;
	}
	
	public function getPagesUrls($returnSingleURL = false, $onlyRelativeURI = false)
	{
		$pagesUrls = [];
		$hosts = [
			Yii::app()->params['host'], // ? 'local.emportal.ru' : 'emportal.ru',
		];
		//$devHosts = [
		//	'dev1.emportal.ru',
		//	'dev2.emportal.ru',
		//	'dev3.emportal.ru',
		//	'dev4.emportal.ru',
		//	'local.emportal.ru',
		//];
		//if ($returnSingleURL)
		//	$hosts = [$hosts[0]]; //array_merge($hosts, $devHosts);
		
		$possbileSubdomains = [];
		$subdomain = $this->city->subdomain;
		if ($this->samozapis == 1)
		{
			$possbileSubdomains[] = Yii::app()->params['regions'][$subdomain]['samozapisSubdomain'];
		} else {
			$possbileSubdomains[] = ($subdomain == 'spb') ? '' : $subdomain;
		}
		
		$pageUrlWithoutHost = '/klinika/' . $this->company->linkUrl . '/' . $this->linkUrl;
		if ($onlyRelativeURI)
		{
			$pagesUrls = [$pageUrlWithoutHost];
		} else {
			foreach($hosts as $host)
			{
				foreach($possbileSubdomains as $possbileSubdomain)
				{
					if (!empty($possbileSubdomain))
						$possbileSubdomain .= '.';
					
					$possibleUrlWithoutProtocol = $possbileSubdomain . $host . $pageUrlWithoutHost;
					
                    if (Yii::app()->params['devMode'] && $_SERVER['HTTP_HOST'] = 'local.emportal.ru')
						$pagesUrls[] = 'http://' . $possibleUrlWithoutProtocol;
                    
					$pagesUrls[] = 'https://' . $possibleUrlWithoutProtocol;
				}
			}
		}
		
		return ($returnSingleURL) ? $pagesUrls[0] : $pagesUrls;
	}
	
	/**
	* Получить связанный с адресом счет за определенный период
    * следует предусмотреть сценарий, когда клиника на абонентской плате
    * пытается получить счет за период, когда она еще не была создана/активна
    * 
    * @param string $reportMonth отчетный период в формате date('Y-m')
    * 
    * @return object Bill экземпляр счета за отчетный период
	*/
	public function getBill($reportMonth)
	{
		if (array_key_exists($reportMonth, $this->cachedBills))
			return $this->cachedBills[$reportMonth];
		
		$searchAttributes = [
			'addressId' => $this->id,
			'reportMonth' => $reportMonth,
		];
		
		$bill = Bill::model()->findByAttributes($searchAttributes);
		if (!$bill)
		{
			$bill = new Bill();
			$bill->attributes = $searchAttributes;
			if(!$this->isActive || !is_object($this->company) || $this->company->removalFlag) {
				return $bill;
			}
			$bill->save();
		}
		$timeDiff = (time() - strtotime($bill->lastUpdate));
		$shouldUpdate = ($timeDiff > 3600*2);
		if (!$bill->isFinal && $shouldUpdate)
			$bill->updateFigures();
		
		$this->cachedBills[$reportMonth] = $bill;
		
		return $bill;
	}

	public function addService($serviceId, $price=0, $free=0, $isActual=1)
	{
		$searchAttributes = [
				'addressId' => $this->id,
				'serviceId' => $serviceId,
		];
	
		$existingAddressService = AddressServices::model()->findByAttributes($searchAttributes);
		if ($existingAddressService) {
			return ['status'=>'EXIST', 'text'=>'exist', 'addressServices'=>$existingAddressService];
		}
	
		$service = Service::model()->findByPk($serviceId);
		if (!$service) {
			return ['status'=>'ERROR', 'text'=>'no such service!'];
		}
	
		$attributes = [
				'addressId' => $this->id,
				'serviceId' => $service->id,
				'price' => $price,
				'companyActivitesId' => $service->companyActiviteId,
				'free' => $free,
				'confirmedDate' => date('Y-m-d H:i:s', time()),
				'isActual' => $isActual,
		];
	
		$newAddressService = new AddressServices;
		$newAddressService->attributes = $attributes;
		if ($newAddressService->save()) {
			return ['status'=>'OK', 'text'=>'saved', 'addressServices'=>$newAddressService];
		} else {
			return ['status'=>'ERROR', 'text'=>'error while saving service'];
		}
	}
	
	public function getIntervalNameForPaymentInCurrentMonth()
	{
		return ($this->salesContract->salesContractType->isFixed == 1) ? 'currentMonth' : 'lastMonth';
	}
	
	public function canGetAppointments()
	{
		return !($this->userMedicals->agreementNew != 1 || !$this->isActive);
	}
	
	public function getDoctorsLimit() {
		if(isset($this->salesContract) AND isset($this->salesContract->salesContractType) AND isset($this->salesContract->salesContractType->doctorsLimit))
			return intval($this->salesContract->salesContractType->doctorsLimit);
		else return 0;
	}
	
	public function allDoctorsCount() {
		$result = 0;
		$agrDoctorCount = $this->agr_doctorCount;
		$doctorCount = count($this->placeOfWorks);
		if(is_int($agrDoctorCount)) { $result += $agrDoctorCount; }
		if(is_int($doctorCount)) { $result += $doctorCount; }
		return $result;
	}
	
	public function checkLimitNumberOfDoctors() {
		$doctorsLimit = $this->getDoctorsLimit();
		if($doctorsLimit > 0) {
			$doctorsCount = $this->allDoctorsCount();
			return $doctorsCount >= $doctorsLimit;
		} else {
			return false;
		}
	}
	
	public function getPricePerVisitor()
	{
		$salesContract = $this->salesContract;
		if (!$salesContract)
			return 0;
		if ($salesContract->salesContractType->isFixed == 1)
			return 0;
		else
			return $salesContract->salesContractType->price;
	}
    
    /**
     * Метод подсчитывает сумму счетов, которые клиника должна была оплатить, но не оплатила.
     * 
     * @return int сумма долга в рублях на текущий момент
     */
    public function getDebtData()
    {
        if (!isset($this->debtDataCached)) {
            $this->debtDataCached = ['debtSum' => 0, 'notPayedBills' => []];
            $criteria = new CDbCriteria();
            $criteria->compare('t.addressId', $this->id, false);
            $bills = Bill::model()->findAll($criteria);
            foreach($bills as $bill) {
                if ($bill->isDebt) {
                    $this->debtDataCached['debtSum'] += $bill->sum;
                    $this->debtDataCached['notPayedBills'][] = ['reportMonth' => $bill->reportMonth];
                }
            }
        }
        return $this->debtDataCached;
    }
    
    public function getDebtSum()
    {
        return $this->debtData['debtSum'];
    }
    
    public function getBalance() {
    	$balance = 0;
    	$payments = $this->payments;
    	$bills = $this->bills;
    	$details = [];
    	foreach ($payments as $payment) {
    		if($payment->statusId == Payment::STATUS_APPROVED && $payment->sum > 0) {
    			$createTime = strtotime($payment->createDate);
    			while(isset($details[$createTime])) { $createTime++; }
    			$details[$createTime] = (object)[
    					'date' => $payment->createDate,
    					'sum' => floatval($payment->sum),
    					'description' => "Зачисление средств на счёт",
    					'type' => 'payment',
    			];
    			$balance += floatval($payment->sum);
    		}
    	}
    	foreach ($bills as $bill) {
    		if($bill->getIsValid() && $bill->sum > 0 && !$bill->isPrepaid) {
    			$createTime = strtotime("+1 month",strtotime($bill->reportMonth."-01"));
    			$createDate = date("Y-m-d",$createTime);
    			while(isset($details[$createTime])) { $createTime++; }
				$reportMonthTimestamp = strtotime($bill->reportMonth."-01");
    			$details[$createTime] = (object)[
    					'date' => $createDate,
    					'sum' => - floatval($bill->sum),
    					'description' => "Оплата за услуги по привлечению клиента за ".MyTools::getRussianMonthsNames()[date('m',$reportMonthTimestamp)-1] . date(' Y',$reportMonthTimestamp),
    					'type' => 'bill',
    			];
    			$balance -= floatval($bill->sum);
    		}
    	}
    	ksort($details);
    	$model = new stdClass();
    	$model->balance = $balance;
    	$model->details = $details;
    	return $model;
    }

	public function newsReference()
    {
		return NewsReference::model()->findByReferenceType(NewsReference::TYPE_ADDRESS,['entityId'=>$this->id]);
	}
    
    public static function getDebtDataForAllAddresses($skipFixedContracts = false)
    {
        $addressInActivityStatus = Address::$showInactive;
        $debtData = ['addresses', 'debtSums', 'ovrDebt' => 0];
		Address::$showInactive = true;
        
		$maxReportMonth = date('Y-m', strtotime(date('Y-m') . '+ 1month'));
		$criteria = new CDbCriteria();
        $criteria->addCondition("UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(t.reportMonth, '-01'), '%Y-%m-%d')) <= UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(:maxReportMonth, '-01'), '%Y-%m-%d'))");
        $criteria->addCondition("t.sum > 0");
        $criteria->params = [':maxReportMonth' => $maxReportMonth];
        
		$criteria->compare('t.paymentStatusId', 0, false);
        $criteria->group = 't.addressId';
		$criteria->with = [
			'address' => [
				'together' => 'true',
                //'condition' => 'address.id = "476136b6-5fe2-11e2-b019-e840f2aca94f"',
			]
		];
        if ($skipFixedContracts) {
            $criteria->with = array_replace_recursive($criteria->with, [
                'salesContractType' => [
                    'select' => 'isFixed',
                    'condition' => 'salesContractType.isFixed != 1',
                ]
            ]);
        }
        
        $bills = Bill::model()->findAll($criteria);
        foreach($bills as $bill) {
            if ($bill->address->debtSum > 0) {
                $debtData['addresses'][] = $bill->address;
                $debtData['debtSums'][] = $bill->address->debtSum;
                $debtData['ovrDebt'] += $bill->address->debtSum;
            }
        }
        Address::$showInactive = $addressInActivityStatus;
        return $debtData;
    }
    
    public function notAllRequiredFieldsAreFilled() {
    	return (	trim(strval($this->userMedicals->email)) === ''
    			OR	trim(strval($this->userMedicals->phoneForAlert)) === ''
    			OR	trim(strval($this->legalINN)) === ''
    			OR	trim(strval($this->legalName)) === ''
    			OR	trim(strval($this->legalAddress)) === ''
    			OR	trim(strval($this->legalPostalAddressStreet)) === '');
    }

    public function updateRating() {
    	$company = $this->company;
    	/* Обработка рейтинга врачей адреса  */
    	$doctorCriteria = new CDbCriteria();
    	$doctorCriteria->compare('t.deleted', 0);
    	$doctorCriteria->compare('currentPlaceOfWork.addressId', $this->id);
    	$doctorCriteria->with = array(
    			'currentPlaceOfWork'=>array(
    					'together' => true,
    			),
    			'specialtyOfDoctors' => array(
    					'together' => true,
    					'with' => array(
    							'doctorCategory' => array(
    									'together' => true,
    							),
    					),
    			),
    			'scientificDegrees' => array(
    					'together' => true,
    			),
    			'scientificTitle' => array(
    					'together' => true,
    			),
    	);
    	$doctors = Doctor::model()->findAll($doctorCriteria);
    	foreach ($doctors as $doctor) {
    		$doctor->updateRating();
    	}
		/* Обработка рейтинга клиники */
		/* 1. Обработка экспертной оценки ( автоматическая часть ) */
		//получаем отношение высш / все врачи 
		$countDoctors = count($this->doctors);
		$countHighestCategory = 0;
		$doctorsSum = 0;
		foreach ($this->doctors as $d) {
			foreach ($d->specialtyOfDoctors as $sp) {
				if ($sp->doctorCategory->name == "Высшая категория") {
					$countHighestCategory++;
					break;
				}
			}
			//Среднее арифметическое рейтингов всех врачей клиники 
			$doctorsSum+=$d->rating;
		}
		if($countDoctors!=0) {
			$doctorsAverageRating = number_format($doctorsSum / $countDoctors, 2);   #inAdd
		} else {
			$doctorsAverageRating = 0 ;
		}
		if($countDoctors!=0) {
			$highToAll = number_format($countHighestCategory / $countDoctors * 5, 2);   #inAdd
		} else {
			$highToAll = 0 ;
		}
		$OMSmark = 0;
		if ($company->IsOMS) {
			$OMSmark = 5;  #inAdd
		}
		$DMSmark = 0;
		if ($company->IsDMS) {
			$DMSmark = 5; #inAdd
		}
		if (count($company->actions_active)) { #inAdd
			$ACTIONSmark = 5;
		} else {
			$ACTIONSmark = 0;
		}
		if ($company->companyContracts->attributes) {
			$appointmentOnlineMARK = 5; #inAdd
		} else {
			$appointmentOnlineMARK = 0;
		}
		/* Считаем оценку за наполнение информации клиникой */
		/*
		  пункты:
		 * 1. Описание компании
		 * 2. Год создания
		 * 3. Наличие лицензий
		 * 4. Наличие фотографии компании/лого
		 * 5. Наличие сайта
		 * 6. Наличие контактов
		 * 7. Наличие почты
		 * 8. Заполнены ли часы работы
		 * 9. Наличие услуг
		 * 10. Наличие фотографий
		 */
		$count = 10;
		$mysum = 0;

		if ($company->description)
			$mysum++;#inAdd_inner 1		
		if ($company->dateOpening)
			$mysum++;#inAdd_inner 2		
		if ($company->companyLicenses)
			$mysum++;#inAdd_inner 3		
		if ($company->logo)
			$mysum++;#inAdd_inner 4
		if ($this->refs[0]->attributes)
			$mysum++;#inAdd_inner 5
		if (count($this->emails))
			$mysum++;#inAdd_inner 6
		if (count($this->phones))
			$mysum++;#inAdd_inner 7
		if ($this->workingHours)
			$mysum++;#inAdd_inner 8
		if (count($this->addressServices) > 1)
			$mysum++;

		//Есть ли фото в галерее
		$sql = Yii::app()->db->createCommand();
		$sql->text = "SELECT COUNT(*) FROM `gallery` INNER JOIN `gallery_photo` ON `gallery`.`id` = `gallery_photo`.`gallery_id` WHERE `gallery`.`owner` = 'gallery" . $this->link . "'";
		$data = $sql->queryScalar();				
		if ($data) {					
			$mysum++;
		}
		$fullnessMark = ($mysum / $count) * 5;
		/* Считаем автоматическую оценку */
		$markClinicAutomatic = number_format(($fullnessMark + $appointmentOnlineMARK + $ACTIONSmark + $DMSmark + $OMSmark + $highToAll + $doctorsAverageRating) / 7, 2);
		#echo "<br>====<br> clinicAutomaticMark: " . $markClinicAutomatic;
		/* 2. Получаем оценки пациентов */
		$i = 0;
		$sum = 0.0;
		$rating = RaitingCompanyUsers::model()->findAll('addressId = :addrId  AND status = 1', array(
			"addrId" => $this->id
		));

		$isRecomend = 0;
		$countRecomend = 0;
		foreach ($rating as $rate) {
			foreach ($rate->raitingCompanyValues as $val) {
				$sum+=$val->value;
				$i++;
			}
			$isRecomend+=(int) $rate->isRecomend;
			$countRecomend++;
		}
		#echo "<br>".$isRecomend. " ".$countRecomend/2;
		if ($isRecomend > $countRecomend / 2) {
			$isRecomend = 1;
		} else {
			$isRecomend = 0;
		}
		if($i) {
			$markUsersCompanySummary = number_format($sum / $i, 2);
		} else {
			$markUsersCompanySummary = 0;
		}
                
		#echo "<br>markUsersCompanySummary: " . $markUsersCompanySummary . "<br>";
		/* 3. Получаем внешние данные для оценки от оператора */
				
		$markExpert = RaitingCompanyMenegers::model()->find("addressId = :aId", array(
			'aId' => $this->id
		));
		$markExpertSum = 0;
		$markExpertCount = 0;
		if($markExpert) {
			foreach ($markExpert->ratingCompanyManagerValues as $mark) {
				$markExpertSum+=$mark->value;
				$markExpertCount++;
			}
			$markExpertAverage = $markExpertSum / $markExpertCount;
		} else {
			$markExpertAverage = 0;
		}
				
		/* =. Складываем три оценки и получаем среднее */
		#echo "markExpertAverage: " . $markExpertAverage . "<br>";
		#$averageAddress =  $markClinicAutomatic + $markExpertAverage; 
		$count = 0;
		$averageAddress = 0;
		foreach(array($markUsersCompanySummary,$markClinicAutomatic,$markExpertAverage) as $row) {
			if(!empty($row)) {
				$count++;
				$averageAddress += $row;
			}
		}
		$addressSummary = number_format(($averageAddress) / $count, 2);
		unset($count);
		if(!empty($this->doctors)) {
			$addressSummary+=0.5;
		}
		
		//$maxCountedRating = max(array($addressSummary, $markClinicAutomatic, $markExpertAverage));
		//if ($doctorsAverageRating > $maxCountedRating) $maxCountedRating = $doctorsAverageRating;
		$maxCountedRating = $doctorsAverageRating;
		
		$this->rating = $maxCountedRating; //$addressSummary;
		$this->ratingUser = $maxCountedRating; //$markUsersCompanySummary;
		$this->ratingSystem = $maxCountedRating; //; // автоматическая оценка
		$this->ratingSite = $maxCountedRating; //; // оценка от оператора
		$this->ratingUpdateTime = time();
		$this->syncThis = false;
		$result = $this->save();
		$this->syncThis = true;
		return $result;
    }
    
    public function getDisableRatingParameter() {
    	return @Yii::app()->params["regions"][$this->city->subdomain][($this->samozapis ? "samozapis_" : "").'disableRating'];
    }
    public function getDisableReviewsParameter() {
    	return @Yii::app()->params["regions"][$this->city->subdomain][($this->samozapis ? "samozapis_" : "").'disableReviews'];
    }
    
    public function getLegalFields() {
        return [
			'legalINN',
            'legalKPP',
            'legalOGRN',
            'legalName',
			'legalAddress',
			'legalPostalAddressCity',
			'legalPostalAddressStreet',
			'legalPostalAddressHouse',
			'legalPostalAddressCode',
        ];
    }
    
    // Получение списка тарифных опций компании
    public function getAllTariffOption($active=true) {
    	$criteria = new CDbCriteria;
    	$criteria->compare("t.addressId",$this->id);
    	if($active) $criteria->addCondition("t.dateEnd IS NULL");
    	return TariffOption::model()->findAll($criteria);
    }
    // Добавление тарифной опции компании
    public function addTariffOption($tariffOptionTypeId) {
    	$criteria = new CDbCriteria;
    	$criteria->compare("t.addressId",$this->id);
    	$criteria->compare("t.tariffOptionTypeId",$tariffOptionTypeId);
    	$criteria->addCondition("t.dateEnd IS NULL");
    	if(TariffOption::model()->exists($criteria)) {
    		return false;
    	} else {
    		$tariffOption = new TariffOption;
    		$tariffOption->addressId = $this->id;
    		$tariffOption->tariffOptionTypeId = $tariffOptionTypeId;
    		return $tariffOption->save();
    	}
    }
    // Отключение тарифной опции компании
    public function disableTariffOption($tariffOptionTypeId) {
    	$criteria = new CDbCriteria;
    	$criteria->compare("t.addressId",$this->id);
    	$criteria->compare("t.tariffOptionTypeId",$tariffOptionTypeId);
    	$criteria->addCondition("t.dateEnd IS NULL");
    	$options = TariffOption::model()->findAll($criteria);
    	foreach ($options as $option) {
    		$option->dateEnd = date("Y-m-d H:i:s",time());
    		$option->update();
    	}
    	return true;
    }
    // Наличие тарифной опции компании
    public function hasTariffOption($tariffOptionTypeId) {
    	$criteria = new CDbCriteria;
    	$criteria->compare("t.tariffOptionTypeId",$tariffOptionTypeId);
    	$criteria->addCondition("t.dateEnd IS NULL");
    	return TariffOption::model()->exists($criteria);
    }

    public function getBreakStart() {
    	if(!preg_match("/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/", $this->breakStart)) {
    		return false;
    	}
    	if($this->breakStart == "00:00") {
    		return false;
    	}
    	return $this->breakStart;
    }

    public function getBreakFinish() {
    	if(!preg_match("/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/", $this->breakFinish)) {
    		return false;
    	}
    	if($this->breakFinish == "00:00") {
    		return false;
    	}
    	return $this->breakFinish;
    }
}
