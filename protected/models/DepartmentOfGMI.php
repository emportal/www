<?php

/**
 * This is the model class for table "departmentOfGMI".
 *
 * The followings are the available columns in table 'departmentOfGMI':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $stateHealthAgencyId
 * @property string $typeDepartmentOfGMI
 *
 * The followings are the available model relations:
 * @property TypeDepartmentsOfGMI $typeDepartmentOfGMI0
 * @property StateHealthAgency $stateHealthAgency
 */
class DepartmentOfGMI extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DepartmentOfGMI the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'departmentOfGMI';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, stateHealthAgencyId, typeDepartmentOfGMI', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, stateHealthAgencyId, typeDepartmentOfGMI', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'typeDepartmentOfGMI0' => array(self::BELONGS_TO, 'TypeDepartmentsOfGMI', 'typeDepartmentOfGMI'),
			'stateHealthAgency' => array(self::BELONGS_TO, 'StateHealthAgency', 'stateHealthAgencyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'stateHealthAgencyId' => 'State Health Agency',
			'typeDepartmentOfGMI' => 'Type Department Of Gmi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('stateHealthAgencyId',$this->stateHealthAgencyId,true);
		$criteria->compare('typeDepartmentOfGMI',$this->typeDepartmentOfGMI,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}