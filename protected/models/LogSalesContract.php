<?php

/**
 * This is the model class for table "logSalesContract".
 *
 * The followings are the available columns in table 'logSalesContract':
 * @property integer $id
 * @property string $addressId
 * @property integer $oldSalesContractTypeId
 * @property integer $newSalesContractTypeId
 * @property string $userId
 * @property string $datetime
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property SalesContractType $oldSalesContractType
 * @property SalesContractType $newSalesContractType
 * @property User $user
 */
class LogSalesContract extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogSalesContract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logSalesContract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('oldSalesContractTypeId, newSalesContractTypeId', 'numerical', 'integerOnly'=>true),
			array('addressId, userId', 'length', 'max'=>36),
			array('addressId, oldSalesContractTypeId, newSalesContractTypeId, datetime, userId', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, addressId, oldSalesContractTypeId, newSalesContractTypeId, datetime, userId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'oldSalesContractType' => array(self::BELONGS_TO, 'SalesContractType', 'oldSalesContractTypeId'),
			'newSalesContractType' => array(self::BELONGS_TO, 'SalesContractType', 'newSalesContractTypeId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'addressId' => 'Address',
			'oldSalesContractTypeId' => 'Old Sales Contract Type',
			'newSalesContractTypeId' => 'New Sales Contract Type',
			'userId' => 'User',
			'datetime' => 'Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userId',$this->addressId);
		$criteria->compare('oldSalesContractTypeId',$this->oldSalesContractTypeId);
		$criteria->compare('newSalesContractTypeId',$this->newSalesContractTypeId);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('datetime',$this->datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}