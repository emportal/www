<?php

/**
 * This is the model class for table "cityDistrict".
 *
 * The followings are the available columns in table 'cityDistrict':
 * @property string $id
 * @property string $name
 * @property string $locative
 * @property string $link
 * @property string $cityId
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property City $city
 * @property NearbyDistrict[] $nearbyDistrict
 */
class CityDistrict extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CityDistrict the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cityDistrict';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, cityId', 'length', 'max'=>36),
			array('name, locative, link', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, locative, link, cityId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'cityDistrictId'),
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
			'nearbyDistrict' => array(self::HAS_MANY, 'NearbyDistrict', 'cityDistrictId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'locative' => 'Местный падеж',
			'link' => 'Link',
			'linkUrl' => 'ЧПУ урл',
			'cityId' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('cityId',$this->cityId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function behaviors() {
		return array(
			'linkUrlBehavior' => array(
				'class' => 'application.components.behaviors.LinkUrlBehavior',
			)
		);
	}
	
	/**
	 �������� ������ �������, �� ������� ���� �������� �����������
	 ����� �� ��������� - �����
	*/
	public static function getActualDistricts($cityId = '534bd8b8-e0d4-11e1-89b3-e840f2aca94f')
	{
		$actualDistrictList = [];
		$actualSpecialties = ShortSpecialtyOfDoctor::model()->findAll();
		
		if(is_array($actualSpecialties)) {
			foreach($actualSpecialties as $actualSpecialty) {
				
				$parameters = json_decode($actualSpecialty->Parameters);
				$specialtyDistrictList = $parameters->District;
				//if (!$specialtyDistrictList) continue;
				if(is_array($specialtyDistrictList)) {
					foreach($specialtyDistrictList as $district) {
						
						if ( !in_array($district, $actualDistrictList) AND $district) {
							
							$actualDistrictList[] = substr($district, 6);
						}
					}
				}
			}
		}
		
		$criteria = new CDbCriteria();
		$cityCriteria = new CDbCriteria();
		$cityCriteria->compare('cityId', $cityId);
		foreach($actualDistrictList as $actualDistrict) {
			
			$criteria->compare('linkUrl', $actualDistrict, true, 'OR');
		}
		$criteria->mergeWith($cityCriteria);
		$districts = self::model()->findAll($criteria);
		return $districts;
	}

	public function getLocative() {
		return (!empty($this->locative) ? ($this->locative.' районе') : ('районе '.$this->name));
	}

	public function getNearbyDistrict(){

		$criteria = new CDbCriteria();
		$criteria->select = 't.cityNearbyDistrictId,CityDistrict.name as name';
		$criteria->join = 'LEFT JOIN CityDistrict ON CityDistrict.id = `t`.cityNearbyDistrictId';
		$criteria->compare('`t`.cityDistrictId', $this->attributes['id']);
		$districts = NearbyDistrict::model()->findAll($criteria);
		return $districts;
	}
}