<?php

/**
 * This is the model class for table "comment".
 *
 * The followings are the available columns in table 'comment':
 * @property integer $id
 * @property string $content
 * @property integer $rating
 * @property string $userId
 * @property string $addressId
 * @property string $doctorId
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Doctor $doctor
 * @property User $user
 * @property Company $company
 */
class Comment extends ActiveRecord
{
	static $ratingList = array(
		1 => 1,
		2 => 2,
		3 => 3,
		4 => 4,
		5 => 5,
	);
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Comments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content, rating, userId, addressId', 'required', 'on'=>'add'),
			array('rating, status', 'numerical', 'integerOnly'=>true),
			array('userId, addressId, doctorId', 'length', 'max'=>36),
			array('answer', 'length', 'min'=>5),
			array('answer', 'required', 'on' => 'answer'),
			array('status', 'safe', 'on' => 'answer'),
			array('date', 'safe', 'on' => 'add'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, content, rating, userId, addressId, doctorId, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorId'),
			'user' => array(self::BELONGS_TO, 'User', 'usersId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => 'Content',
			'rating' => 'Rating',
			'userId' => 'User',
			'addressId' => 'Address',
			'doctorId' => 'Doctor',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function scopes() {
		return array(
			'moderated' => array(
				'condition' => 'status = 1'
			),
		);
	}
}