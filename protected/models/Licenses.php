<?php

/**
 * This is the model class for table "Licenses".
 *
 * The followings are the available columns in table 'Licenses':
 * @property string $idLicenses
 * @property string $Owner_CompaniesId
 * @property string $LicensesNumber
 * @property string $LicensesDate
 * @property string $LicensesKindOfActivity
 * @property string $LicensingAuthoritiesId
 * @property string $AddressesId
 *
 * The followings are the available model relations:
 * @property Addresses $addresses
 * @property Companies $ownerCompanies
 * @property LicensingАuthorities $licensingАuthorities
 */
class Licenses extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Licenses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Licenses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idLicenses, LicensesNumber, LicensesKindOfActivity', 'length', 'max'=>150),
			array('Owner_CompaniesId, LicensingAuthoritiesId, AddressesId', 'length', 'max'=>36),
			array('LicensesDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idLicenses, Owner_CompaniesId, LicensesNumber, LicensesDate, LicensesKindOfActivity, LicensingAuthoritiesId, AddressesId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::BELONGS_TO, 'Addresses', 'AddressesId'),
			'ownerCompanies' => array(self::BELONGS_TO, 'Companies', 'Owner_CompaniesId'),
			'licensingАuthorities' => array(self::BELONGS_TO, 'LicensingАuthorities', 'LicensingAuthoritiesId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLicenses' => 'Id Licenses',
			'Owner_CompaniesId' => 'Owner Companies',
			'LicensesNumber' => 'Licenses Number',
			'LicensesDate' => 'Licenses Date',
			'LicensesKindOfActivity' => 'Licenses Kind Of Activity',
			'LicensingAuthoritiesId' => 'LicensingАuthorities',
			'AddressesId' => 'Addresses',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLicenses',$this->idLicenses,true);
		$criteria->compare('Owner_CompaniesId',$this->Owner_CompaniesId,true);
		$criteria->compare('LicensesNumber',$this->LicensesNumber,true);
		$criteria->compare('LicensesDate',$this->LicensesDate,true);
		$criteria->compare('LicensesKindOfActivity',$this->LicensesKindOfActivity,true);
		$criteria->compare('LicensingAuthoritiesId',$this->LicensingAuthoritiesId,true);
		$criteria->compare('AddressesId',$this->AddressesId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}