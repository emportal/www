<?php

/**
 * This is the model class for table "stateHealthAgency".
 *
 * The followings are the available columns in table 'stateHealthAgency':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $legalName
 * @property integer $сost
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property DepartmentOfGMI[] $departmentOfGMIs
 * @property Phone[] $phones
 * @property Ref[] $refs
 * @property StateHealthAgencyServices[] $stateHealthAgencyServices
 */
class StateHealthAgency extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StateHealthAgency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stateHealthAgency';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('сost', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>36),
			array('name, link, legalName, description', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, legalName, сost, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'stateHealthAgencyId'),
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'StateHealthAgenciesId'),
			'departmentOfGMIs' => array(self::HAS_MANY, 'DepartmentOfGMI', 'stateHealthAgencyId'),
			'phones' => array(self::HAS_MANY, 'Phone', 'stateHealthAgencyId'),
			'refs' => array(self::HAS_MANY, 'Ref', 'stateHealthAgencyId'),
			'stateHealthAgencyServices' => array(self::HAS_MANY, 'StateHealthAgencyServices', 'stateHealthAgencyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'legalName' => 'Legal Name',
			'сost' => 'сost',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('legalName',$this->legalName,true);
		$criteria->compare('сost',$this->сost);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}