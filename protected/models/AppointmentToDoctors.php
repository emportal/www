<?php

/**
 * This is the model class for table "appointmentToDoctors".
 *
 * The followings are the available columns in table 'appointmentToDoctors':
 * @property string $id
 * @property string $link
 * @property string $doctorId
 * @property string $companyId
 * @property string $userId
 * @property string $createdDate
 * @property string $plannedTime
 * @property string $serviceId
 * @property string $addressId
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property integer $statusId
 * @property string $autoLoginKey
 * @property string $extAppointmentId
 * @property integer $sysTypeId
 * @property integer $managerStatusId
 * 		Статус, проставляемый менеджером (см. MANAGER_STATUS_ID_TYPES)
 * @property string $price
 * @property string $comission
 * @property integer $salesContractTypeId
 * @property integer $prePaid
 * @property string $refererId
 * @property string $serviceName
 *
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property AddressServices $addressService
 * @property User $user
 * @property Address $address
 * @property Doctor $doctor
 * @property SalesContractType $salesContractType
 * @property Comment[] $comments
 */
class AppointmentToDoctors extends ActiveRecord {
	
	public static $showOwn = false;
	
	public $managerId;
	public $companyNameLike;
	public $extPatId; // Идентификатор пациента - значение из внешней МИС
    public $patientId; // Идентификатор пациента - значение user.id, см. dataProviderForNewAdminPanel
	
	//dataProviderForNewAdminPanel values
	public $apt_status;
	public $time_period;
	public $visit_time_period;
	public $app_type;
	public $price;
	public $img;

	const APP_TYPE_MOBILE = 'mobile';
	const APP_TYPE_SITE_REGISTRED = 'site';
	const APP_TYPE_VK_APP = 'vk_app';
	const APP_TYPE_SITE_GUEST = 'guest';
	const APP_TYPE_FROM_GIS = '2gis_widget';
    const APP_TYPE_FROM_MEDSOVET = 'medsovet';
	const APP_TYPE_SAMOZAPIS = 'Samozapis';
	const APP_TYPE_FROM_STOM_FIRMS = 'stom_firms';
	const APP_TYPE_OWN = 'own'; //собственные заявки клиник
	const APP_TYPE_EXTERNAL = 'ext'; //если загружено из внешних ресурсов путем интеграции
	const APP_TYPE_PHONE_EMPORTAL = 'phone_emportal';
	const APP_TYPE_PHONE_ZOON = 'phone_zoon';
	const APP_TYPE_PHONE_DOCTOR_PITER = 'phone_doctor_piter';
	const APP_TYPE_PHONE_MEDPORTAL = 'phone_medportal';
	const APP_TYPE_PHONE_MEDKOMPAS = 'phone_medkompas';
	const APP_TYPE_FROM_APP_FORM = 'appForm';
    const APP_TYPE_FROM_API = 'api';
	const APP_TYPE_PHONE_MEDIA = 'phone_media';
	const APP_TYPE_PHONE_SPRAVKUS = 'phone_spravkus';
	const APP_TYPE_PHONE_WARHOLA = 'phone_warhola';
    const APP_TYPE_PHONE_MEDSOVET = 'phone_medsovet';
	const VISIT_TYPE_CLINIC = 'clinic';
	const VISIT_TYPE_DOCTOR = 'doctor';
	const defaultUserId = 'f39d756c-7035-47fc-961c-aec9d0e72ca9';
	const SEND_TO_REGISTER = 0;
	const ACCEPTED_BY_REGISTER = 1;
	const DECLINED_BY_REGISTER = 2;
	const DECLINED_BY_PATIENT = 3;
	const VISITED = 4;
	const SEEN = 5;
	const EXPIRED = 6;
	const DISABLED = 7;
	const EMP_COMISSION = 0.15;

	public static $STATUSES = array(
		self::SEND_TO_REGISTER => 'Введена в систему',
		self::SEEN => 'Просмотрено',
		self::ACCEPTED_BY_REGISTER => 'Подтверждено регистратурой',
		self::DECLINED_BY_REGISTER => 'Отменено регистратурой',
		self::DECLINED_BY_PATIENT => 'Отменено пациентом',
		self::EXPIRED => 'Просрочено',
		self::VISITED => 'Визит состоялся',
	);
	public static $STATUSES_APP_TYPE = array(
		self::APP_TYPE_PHONE_EMPORTAL => 'Звонок с emportal.ru',
		self::APP_TYPE_PHONE_ZOON => 'Звонок с zoon.ru',
		self::APP_TYPE_PHONE_MEDIA => 'Звонок с media',
		self::APP_TYPE_PHONE_SPRAVKUS => 'Звонок с spravkus.ru',
		self::APP_TYPE_PHONE_WARHOLA => 'Звонок с warhola',
		self::APP_TYPE_PHONE_DOCTOR_PITER => 'Звонок с Doctorpiter',
		self::APP_TYPE_PHONE_MEDPORTAL => 'Звонок с Medportal',
		self::APP_TYPE_PHONE_MEDKOMPAS => 'Звонок с Medkompas',
		self::APP_TYPE_SITE_REGISTRED => 'Сайт',
		self::APP_TYPE_SITE_GUEST => 'Сайт (гость)',
		self::APP_TYPE_MOBILE => 'Приложение',
		self::APP_TYPE_FROM_GIS => '2 ГИС',
		self::APP_TYPE_FROM_STOM_FIRMS => 'Stom-firms.ru',
		self::APP_TYPE_OWN => 'Собственный пациент клиники',
		self::APP_TYPE_EXTERNAL => 'Загружено из внешних ресурсов',
		self::APP_TYPE_SAMOZAPIS => 'Самозапись',
		self::APP_TYPE_FROM_APP_FORM => 'Виджет записи',
        self::APP_TYPE_FROM_API => 'API',
        self::APP_TYPE_FROM_MEDSOVET => 'medsovet.info (сайт)',
        self::APP_TYPE_PHONE_MEDSOVET => 'Звонок с medsovet.info',
		self::APP_TYPE_OWN => 'Собственный пациент клиники',
		self::APP_TYPE_VK_APP => 'Приложение сообщества вконтакте',
		'partner_7062' => 'medportal',
		'partner_6132' => 'ПульсПлюс',
		'partner_7940' => 'Warhola', //zabolevaniyakozhi.ru
		'partner_8449' => 'Welltory',
	);
	public static $STATUSES_CANCELED = array(3);
	public static $CHANGEABLE_APP_TYPES = [
		self::APP_TYPE_SITE_REGISTRED,
		self::APP_TYPE_SITE_GUEST,
		self::APP_TYPE_PHONE_EMPORTAL,
		self::APP_TYPE_PHONE_ZOON,
		self::APP_TYPE_PHONE_MEDIA,
		//self::APP_TYPE_PHONE_WARHOLA,
		self::APP_TYPE_PHONE_MEDSOVET,
		self::APP_TYPE_PHONE_DOCTOR_PITER,
		self::APP_TYPE_PHONE_MEDPORTAL,
		self::APP_TYPE_PHONE_MEDKOMPAS,
	];
	public static $HIDDEN_APP_TYPES = [
		self::APP_TYPE_PHONE_SPRAVKUS,
		self::APP_TYPE_FROM_GIS,
		self::APP_TYPE_FROM_STOM_FIRMS,
	];  
	
	//статус заявки, выставляемый менеджером после звонка пациенту или клинике
	public static $MANAGER_STATUS_ID_TYPES = [
		0 => 'Не обработано',
		1 => 'Подтверждено',
		2 => 'Отклонено',
		3 => 'Пациент дошел',
		4 => 'Пациент не дошел',
		5 => 'Указать, дошел ли пациент',
	];
	
	//статус заявки, выставляемый представителем клиники в модуле adminClinic при сверке записей
	public static $CLINIC_STATUS_ID_TYPES = [
		'mayPay' => '0', //не указали явно, будут ли платить
		'willNotPay' => '1', //не хотят платить
		'willPay' => '2', //хотят платить
	];
	
	public static function getStatusesAppType() {
		$result = Yii::app()->cache->get('StatusesAppType');
		if(!is_array($result)) {
			$command = Yii::app()->db->createCommand()
			->select("appType")
			->from("appointmentToDoctors t")
			->where("t.appType IS NOT NULL AND t.appType != ''")
			->group("t.appType");
			$appTypes = $command->queryAll();
			foreach ($appTypes as $item) {
				$result[$item['appType']] = $item['appType'];
			}
			$result = array_merge($result,SELF::$STATUSES_APP_TYPE);
			asort($result);
			Yii::app()->cache->set('StatusesAppType',$result,10800);
		}
		return $result;
	}

	public static function getStatusesAppTypeForDate($time_period) {

		$lower_border = explode('-', $time_period)[0];
		$upper_border = explode('-', $time_period)[1];
		$upper_border = $upper_border ? $upper_border : $lower_border;
		
		$lower_border = strtotime($lower_border) + 21*60*60;
		$upper_border = strtotime($upper_border) + 21*60*60;
		
		$lower_border = gmdate("Y-m-d 00:00:00", $lower_border);
		$upper_border = gmdate("Y-m-d 23:59:59", $upper_border);

		$command = Yii::app()->db->createCommand()
		->select("appType")
		->from("appointmentToDoctors t")
		->where("t.appType IS NOT NULL AND t.appType != '' AND createdDate >= '" . $lower_border . "' AND createdDate <= '" . $upper_border . "'")
		->group("t.appType");
		$appTypes = $command->queryAll();
		foreach ($appTypes as $item) {
			$result[$item['appType']] = $item['appType'];
		}
		foreach (SELF::$STATUSES_APP_TYPE as $key => $value) {
			if($result[$key]) {
				$result[$key] = $value;
			}
		}
		if(is_array($result)) {
			asort($result);
		}
		else {
			$result = [];
		}

		return $result;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AppointmentToDoctors the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'appointmentToDoctors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('companyId, addressId', 'required'),
			array('addressId', 'required'),
			array('serviceId', 'required', 'on' => 'visit'),
			array('name', 'filter', 'filter' => function($v){
				$hasSurname = strrpos($v," ");
				if ($this->scenario == 'fromAppForm' OR $this->scenario == 'visit_to_service')
					$hasSurname = true;
				return $result = ( $hasSurname && strlen($v)>3 ) ? $v : false;
			}),
			array('name','required','on' => 'visit_to_doctor,visit','message' => 'Фамилия и Имя пациента обязательны'),
			array('userId', 'default', 'value' => self::defaultUserId),
			array('email', 'email'),
			array('phone', 'checkPhoneActivatedForGuest', 'on' => 'visit,visit_to_doctor'),
			array('id, doctorId, companyId, userId, refererId, serviceId, addressId, statusId', 'length', 'max' => 36),
			array('link, createdDate, name, plannedTime, extAppointmentId, extPatId, extAppointmentId, sysTypeId, price, comission, salesContractTypeId, serviceName', 'safe'),
			array('phone', 'numerical', 'integerOnly' => true, 'message' => 'Поле Телефон может содержать только цифры'),
			array('plannedTime', 'filter', 'filter' => function($v){
				$result = $v;
				if(@Yii::app()->params["regions"][$this->address->city->subdomain]['disableAppointmentCalendar']) {
					if (empty($result)) {
						$result = date('Y-m-d H:i:s',$this->doctor->getClosestReceptionTime());
					}
				}
				return $result;
			}),
			array('plannedTime', 'required', 'on' => 'visit,visit_to_doctor', 'message' => 'Необходимо выбрать дату в календаре.'),
			array('plannedTime', 'date', 'format' => 'yyyy-MM-dd HH:mm:ss', 'on' => 'visit,visit_to_doctor', 'message' => 'Неправильный формат даты.'),
			array('plannedTime', 'isBusyDate', 'on' => 'visit,visit_to_doctor'),
			//array('appType', 'unsafe'),
			array('appType', 'safe', 'on' => 'changeAppType, fromAppForm'),
			array('managerComment', 'safe', 'on' => 'changeManagerComment'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('managerStatusId', 'safe', 'on' => 'changeManagerStatus, fromAppForm'),
			array('statusId, sourcePageUrl', 'safe', 'on' => 'fromAppForm'),
			array('id, doctorId, companyId, userId, plannedTime, serviceId, refererId, addressId, name, email, phone, statusId, extAppointmentId, sysTypeId, sourcePageUrl, visitType, serviceName', 'safe', 'on' => 'search, visit_to_service'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'addressService' => array(self::BELONGS_TO, 'AddressServices', 'serviceId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorId'),
			'salesContractType' => array(self::BELONGS_TO, 'SalesContractType', 'salesContractTypeId'),
			'comments' => array(self::HAS_MANY, 'Comment', 'appointmentToDoctorsId'),
			'referer' => array(self::BELONGS_TO, 'User', 'refererId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'linkId' => 'Link',
			'doctorId' => 'Врач',
			'companyId' => 'Company',
			'userId' => 'User',
			'refererId' => 'Referer',
			'createdDate' => 'Дата',
			'plannedTime' => 'Дата и время приема',
			'serviceId' => 'Услуга',
			'addressId' => 'Клиника',
			'statusId' => 'Статус',
			'statusText' => 'Статус',
			'name' => 'ФИО',
			'phone' => 'Телефон',
			'email' => 'E-mail',
			'autoLoginKey' => 'Ключ автоматической авторизации',
			'extAppointmentId' => 'Идентификатор талона во внешней МИС',
			'sysTypeId' => 'Идентификатор МИС',
			'price' => 'Стоимость посещения (в рублях)',
			'comission' => 'comission',
			'salesContractTypeId' => 'Идентификатор тарифа',
			'serviceName' => 'Название услуги',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('doctorId', $this->doctorId, true);
		$criteria->compare('companyId', $this->companyId, true);
		$criteria->compare('userId', $this->userId, true);
		$criteria->compare('plannedTime', $this->plannedTime, true);
		$criteria->compare('serviceId', $this->serviceId, true);
		$criteria->compare('addressId', $this->addressId, true);
		$criteria->compare('extAppointmentId', $this->extAppointmentId, false);
		$criteria->compare('sysTypeId', $this->sysTypeId, false);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function beforeFind() {
		parent::beforeFind();

		if(AppointmentToDoctors::$showOwn === false) {
			$criteria = $this->dbCriteria;
			$criteria->addCondition($this->tableAlias . ".appType != 'own'");
			$this->dbCriteria = $criteria;
		}
	}

	public function beforeSave() {
		if (parent::beforeSave()) {
			
			if ($this->createdDate == '')
				$this->createdDate = new CDbExpression('NOW()');
			
			if ($this->statusId != self::DISABLED) {
				if ($this->isNewRecord) {
					
					if ($this->address->salesContract)
						$this->salesContractTypeId = $this->address->salesContract->salesContractTypeId;
					
					//подсчет стоимости приема/услуги на момент создания заявки
					if(!empty($this->serviceId)) {
						$addressService = $this->addressService;
						if ($addressService) {
							$this->price = $addressService->free ? 0 : $addressService->price;
							$this->serviceName = $addressService->service->name;
						} else {
							$this->price = null;
						}
					} else {
						$doctor = $this->doctor;
						if ($doctor) {
							$this->price = $doctor->getInspectFree() ? 0 : ($doctor->getInspectPrice()>0) ? $doctor->getInspectPrice() : null;
						} else {
							$this->price = null;
						}
					}
					$this->comission = $this->getEmpComission(false);
					
					if($this->scenario === 'visit_to_netrika' || $this->scenario === 'visit_to_emias') {
						$this->appType = self::APP_TYPE_SAMOZAPIS;
					} else {
						if (isset($_REQUEST['JSON'])) {
							$this->appType = self::APP_TYPE_MOBILE;
						} elseif (Yii::app()->request->getParam('isWidget') AND Yii::app()->session['widgetReferrer'] == 'stom-firms') {
							$this->appType = self::APP_TYPE_FROM_STOM_FIRMS;
						} elseif (Yii::app()->request->getParam('isWidget')) {
							$this->appType = self::APP_TYPE_FROM_GIS;
						} elseif ($this->scenario == 'fromAppForm' AND $this->attributes['appType'] == self::APP_TYPE_FROM_APP_FORM) {
							$this->appType = self::APP_TYPE_FROM_APP_FORM;
						} elseif ($this->scenario == 'fromAppForm' AND $this->attributes['appType'] != self::APP_TYPE_FROM_APP_FORM) {
							$this->appType = ($this->appType == '') ? self::APP_TYPE_PHONE_EMPORTAL : $this->appType;
						} elseif ($this->appType == self::APP_TYPE_OWN) {
							$this->appType = self::APP_TYPE_OWN;
						} elseif (Yii::app()->request->getParam('vk')) {
							$this->appType = self::APP_TYPE_VK_APP;
						} elseif (Yii::app()->user->isGuest) {
							$this->appType = self::APP_TYPE_SITE_GUEST;
						} else {
							$this->appType = self::APP_TYPE_SITE_REGISTRED;
						}
						if (isset(Yii::app()->request->cookies['partner_refererId'])) {
							$partner_key = 'partner_'.trim(Yii::app()->db->quoteValue(Yii::app()->request->cookies['partner_refererId']),"'");
							$this->appType = $partner_key;
						}
					}
					$this->autoLoginKey = RandomKey::generate(12);
					
					if(!empty($this->userId)) {
						if(!empty($this->user->refererId)) {
							$this->refererId = $this->user->refererId;
						}
						if(($this->address->userMedicals->agreementLoyaltyProgram == 1)
								&&	($this->price > 0)
								&&	($this->user->balance > 0)
								&&	Yii::app()->params['regions'][$this->address->city->subdomain]['loyaltyProgram']) {
							$this->prePaid = ($this->user->balance <= $this->price) ? $this->user->balance : $this->price;
							Yii::import('application.controllers.UserController');
							UserController::incrementBalance($this->user, -$this->prePaid);
						}
					}
				} else {
					if($this->statusId === AppointmentToDoctors::DECLINED_BY_REGISTER) {
						$appointment = AppointmentToDoctors::model()->findByAttributes(["id" => $this->id]);
						if((!empty($appointment))
						&& ($appointment->statusId !== AppointmentToDoctors::DECLINED_BY_REGISTER)
						&& ($appointment->prePaid > 0)) {
							Yii::import('application.controllers.UserController');
							if(UserController::incrementBalance($this->user, $appointment->prePaid)) {
								$this->prePaid = null;
							}
						}
					}
				}
			}

			return true;
		}
	}

	public function afterSave() {
		if (parent::afterSave()) {
			if ($this->user) {
				if ($this->user->appointmentToDoctorsCount % 3 == 0) {
					$this->user->isBlockedAppointment = User::APPOINT_DISALLOW;
					$this->user->save();
				}
			}
			Yii::app()->cache->delete(md5('timeBlock'.$this->addressId.$this->doctorId));

			$amoCRM = new AmoCRM();
			$amoCRM->setLead($this);
			return true;
		}
	}

	public function defaultScope() {
		return array(
			'with' => array(
				'company',
				'doctor',
				'address'
			),
		);
	}

	public function setPlannedTime($value) {
		$this->plannedTime = date("Y-m-d H:i:s", strtotime($value));
	}

	public function getRegistryPlannedTime() {
		if ((int) $this->plannedTime) {
			return Yii::app()->dateFormatter->formatDateTime($this->plannedTime, "medium", "short");
		} else {
			return "Дата не указана";
		}
	}

	public function getStatusText() {
		return self::$STATUSES[$this->statusId];
	}

	public function isCanceled() {
		return in_array($this->statusId, self::$STATUSES_CANCELED);
	}

	public function isCommentPosted() {
		return Yii::app()->db->createCommand("SELECT COUNT(*) FROM raitingCompanyUsers WHERE appointmentToDoctorsId='$this->id'")->queryScalar();
	}

	public function isCommentDoctorPosted() {
		return Yii::app()->db->createCommand("SELECT COUNT(*) FROM raitingDoctorUsers WHERE appointmentToDoctorsId='$this->id'")->queryScalar();
	}

	public function getStatusAction() {
		$result = [];
		if ($this->plannedTime > date("Y-m-d H:i:s")) {
			if ($this->isCanceled()) {
				$result[] = CHtml::link("Повторить запись", array("/clinic/visit/",
					"link" => $this->address->link, 
					//"AppointmentToDoctors[serviceId]" => $this->service->link,
					"AppointmentToDoctors[doctorId]" => $this->doctor->link
				));
			} else {
				$result[] = CHtml::link("Отменить запись", array("/user/cancelVisit", "link" => $this->link));
			}
		} elseif ($this->isCommentPosted() && ($this->isCommentDoctorPosted() || !$this->doctorId)) {
			$result[] = CHtml::encode("Отзыв оставлен");
		} elseif ((int)$this->statusId === (int)self::VISITED) {
			//if(!$this->isCommentPosted()) {
				$result[] = CHtml::link("Оставить отзыв о клинике", array("/user/comment", "link" => $this->link));
			//}
			//if($this->isCommentDoctorPosted() || !$this->doctorId) {
				$result[] = CHtml::link("Оставить отзыв о докторе", array("/user/commentDoctor", "link" => $this->link));
			//}
		}
		if((int)$this->statusId === (int)self::ACCEPTED_BY_REGISTER/* || (int)$this->managerStatusId === (int)self::ACCEPTED_BY_REGISTER*/) {
			$result[] = CHtml::link("Посмотреть талон", array("/user/voucher", "link" => $this->link), ["target" => "_blank"]);
            $result[] = CHtml::link("Распечатать талон", array("/user/voucher", "link" => $this->link, "type" => "print"), ["target" => "_blank"]);
            //$result[] = CHtml::link("Скачать талон", array("/user/voucher", "link" => $this->link, "type" => "download"));
		}
		if(empty($result)) $result[] = CHtml::encode("-");
		return implode("<br>", $result);
	}

	public function getStatusActionJSON() {

		if ($this->plannedTime > date("Y-m-d H:i:s")) {
			if ($this->isCanceled()) {
				return [
					'type' => 'repeatAppointment',
					'text' => "Повторить запись",
					'url' => Yii::app()->createAbsoluteUrl("/clinic/visit/", array(
						"companyLink" => $this->company->link,
						"linkUrl" => $this->address->link,
						//"AppointmentToDoctors[serviceId]" => $this->service->link,
						"AppointmentToDoctors[doctorId]" => $this->doctor->link,
						'JSON'=>1
					)),
					'data' => [
						"companyLink" => $this->company->link,
						"linkUrl" => $this->address->link,
						//"AppointmentToDoctors[serviceId]" => $this->service->link,
						"AppointmentToDoctors[doctorId]" => $this->doctor->link,
					]
				];
			} else {
				return [
					'type' => 'declineAppointment',
					'text' => "Отменить запись",
					'url' => Yii::app()->createAbsoluteUrl("/user/cancelVisit", array(
						"link" => $this->link,
						'delete'=>1,
						'JSON'=>1
					))
				];
				;
			}
		} /* elseif ($this->isCommentPosted() && ($this->isCommentDoctorPosted() || !$this->doctorId)) {
		  return CHtml::encode("Отзыв оставлен");
		  } elseif ((int) $this->statusId === (int) self::VISITED) {
		  return
		  ($this->isCommentPosted() ? "" : CHtml::link("Оставить отзыв о клинике", array("/user/comment", "link" => $this->link))."<br>")
		  . ($this->isCommentDoctorPosted() || !$this->doctorId  ? "" : CHtml::link("Оставить отзыв о докторе", array("/user/commentDoctor", "link" => $this->link)));

		  } */ else {
			return [
				'text' => "Нет действия",
				'url' => null
			];
		}
	}

	public function getHidePhoneEmail() {
		if ($this->statusId == self::SEND_TO_REGISTER) {
			return CHtml::decode('<span class="pseudo_a no-decoration" data="' . $this->phone . '<br/>' . $this->email . '">**********</span>');
		} else {
			return CHtml::decode($this->phone . "<br/>" . $this->emailWithBr);
		}
	}

	public function getEmailWithBr() {
		if (strlen($this->email) > 30) {
			return str_replace("@", "@<br/>", $this->email);
		} else {
			return $this->email;
		}
	}

	public function getActions() {
		switch ($this->statusId) {
			case self::SEND_TO_REGISTER:
				return '<div id="seen_' . $this->link . '" class="btn-small btn-small-blue" data-link="' . $this->link . '">Просмотреть</div>';
				break;
			case self::SEEN:
				return '<div id="accept_' . $this->link . '" class="btn-small btn-small-green" data-link="' . $this->link . '">Подтвердить</div>' .
						'<div id="decline_' . $this->link . '" class="btn-small btn-small-red" data-link="' . $this->link . '">Отклонить</div>'.
						'<div id="time_' . $this->link . '" class="btn-small btn-small-blue" data-doctor="'.$this->doctor->link.'" data-address="'.$this->address->link.'" data-link="' . $this->link . '">Изменить время приема</div>';
				break;
			case self::ACCEPTED_BY_REGISTER:
				//return '<div id="decline_'.$this->link.'" class="btn-red admin_button" data-link="'.$this->link.'">Отменить</div>';
				return ''; //Нет действий
				break;
			case self::DECLINED_BY_REGISTER:
				//return '<div id="accept_'.$this->link.'" class="btn-green admin_button" data-link="'.$this->link.'">Подтвердить</div>';
				return ''; //Нет действий
				break;
			case self::DECLINED_BY_PATIENT:
				return ''; //Нет действий
				break;
			case self::VISITED:
				return ''; //Нет действий
				break;
			case self::EXPIRED:
				return ''; //Нет действий
				break;
		}
	}

	public function sendMailToUser($type) {
		if ($this->email || ($this->user && $this->user->email)) {
			$mail = new YiiMailer();
			$mail->setFrom('robot@emportal.ru', 'Emportal.ru');

			if (Yii::app()->params['devMode']) {
				$mail->AddAddress(Yii::app()->params['devEmail']);
			} else {
				if ($this->user && $this->user->email)
				$mail->AddAddress($this->user->email);
				else
					$mail->AddAddress($this->email);
			}

			$mail->setData(array(
				'model' => $this,
				'user' => Yii::app()->user->model
			));

			switch ($type) {
				case self::ACCEPTED_BY_REGISTER:
					$mail->setView('appointment_accept_by_register');
					$mail->Subject = 'Подтверждение записи на прием';
					break;
				case self::DECLINED_BY_REGISTER:
					$mail->setView('appointment_decline_by_register');
					$mail->Subject = 'Отмена записи на прием';
					break;
				case self::SEND_TO_REGISTER:
					$mail->setView('appointment_send_to_register');
					$mail->Subject = 'Cоздана новая запись на прием';
					break;
			}

			$mail->render();
			$mail->Send();
		}
	}

	public function sendMailToClinic($type) {
		$mail = new YiiMailer();
		$mail->setFrom('robot@emportal.ru', 'Emportal.ru');

		if(Yii::app()->params['devMode'])
			$mail->AddAddress(Yii::app()->params['devEmail']);
		else
			$mail->AddAddress($this->address->userMedical->email);

		$mail->setData(array(
			'model' => $this
		));

		switch ($type) {
			case 'new_record':
				$mail->setView('new_appointment_to_register');
				$mail->Subject = 'Новая запись на ' . $this->visitTypeText;
				break;
			case self::ACCEPTED_BY_REGISTER:
				$mail->setView('appointment_accept_by_register_clinic');
				$mail->Subject = 'Подтверждение записи на ' . $this->visitTypeText;
				break;
			case self::DECLINED_BY_REGISTER:
				$mail->setView('appointment_decline_by_register_clinic');
				$mail->Subject = 'Отмена записи на ' . $this->visitTypeText;
				break;
		}

		$mail->render();
		$mail->Send();
	}

	public function sendSmsToUser($type) {
        
        if (!$this->canSendSmsToPatients())
            return true;
        
		$sms = new SmsGate();
		$sms->phone = $this->phone;
		$type = strval($type);
		
		switch ($type) {
			case 'plannedTimeChanged': 
				/*
				$sms->setMessage('Время записи в клинику :clinicName '.($this->doctor->name ? 'к специалисту :doctorName': '').' изменено: :plannedTime',[
					':clinicName' => $this->company->name,
					':doctorName' => $this->doctor->shortname,
					':plannedTime' => date("d.m.Y H:i", strtotime($this->plannedTime))
				]);					
				$sms->send();
				*/
				break;
			
			case self::SEND_TO_REGISTER: /* Статус заявки: :stat. */
				$sms->setMessage('Ваша заявка на ' . $this->visitTypeText . ' отправлена в клинику. Дождитесь звонка или СМС от клиники с подтверждением времени приема. emportal.ru', array(
					#':stat' => AppointmentToDoctors::$STATUSES[$this->statusId],
					':date' => strtotime($this->plannedTime) != false ? date("d.m.Y H:i", strtotime($this->plannedTime)) : 'нет даты'
				));
				$sms->send();
				break;
			
			case self::ACCEPTED_BY_REGISTER: /* Статус заявки: :stat. */
				/*
				$sms->setMessage('Ваша запись на ' . $this->visitTypeText . ' подтверждена. :doctor:date:address. emportal.ru', array(
					#':stat' => AppointmentToDoctors::$STATUSES[$this->statusId],
					':date' => strtotime($this->plannedTime) != false ? ' Дата: '.date("H:i d.m.Y", strtotime($this->plannedTime)) . '.' : '',
					':doctor' => (isset($this->doctor) ? ' Врач: '.$this->doctor->surName : '')." ".(!empty($this->doctor->firstName) ? mb_substr($this->doctor->firstName, 0, 1, 'UTF-8').'.' : '' ).(!empty($this->doctor->fatherName) ? mb_substr($this->doctor->fatherName, 0, 1, 'UTF-8').'.' : ''),
					':address' => (isset($this->address) ? ' Адрес: ' . $this->address->street.', '.$this->address->houseNumber . '.' : '')
				));
				$sms->send();
				*/
				break;
			
			case self::DECLINED_BY_REGISTER: /* Статус заявки: :stat. */
				/*
				$sms->setMessage('Заявка на ' . $this->visitTypeText . ' отклонена. Дата отмененного приема: :date.', array(
					#':stat' => AppointmentToDoctors::$STATUSES[$this->statusId],
					':date' => strtotime($this->plannedTime) != false ? date("d.m.Y H:i", strtotime($this->plannedTime)) : 'нет даты'
				));					
				$sms->send();
				*/
				break;			
		}
	}

	public function sendSmsToGuest($type) {
        
        if (!$this->canSendSmsToPatients())
            return true;
        
		$sms = new SmsGate();
		$sms->phone = $this->phone;
		switch ($type) {
			case self::SEND_TO_REGISTER:
				$sms->setMessage('Ваша заявка на ' . $this->visitTypeText . ' отправлена в клинику. Дождитесь звонка или СМС от клиники с подтверждением времени приема. emportal.ru', array(
					':date' => strtotime($this->plannedTime) != false ? date("d.m.Y H:i", strtotime($this->plannedTime)) : 'нет даты'
				));
					$sms->send();
				break;
			
			case self::ACCEPTED_BY_REGISTER:
				/*
				$sms->setMessage('Ваша запись на ' . $this->visitTypeText . ' подтверждена. :doctor:date:address. emportal.ru', array(
					#':stat' => AppointmentToDoctors::$STATUSES[$this->statusId],
					':date' => strtotime($this->plannedTime) != false ? ' Дата: '.date("H:i d.m.Y", strtotime($this->plannedTime)) . '.' : '',
					':doctor' => (isset($this->doctor) ? ' Врач: '.$this->doctor->surName : '')." ".(!empty($this->doctor->firstName) ? mb_substr($this->doctor->firstName, 0, 1, 'UTF-8').'.' : '' ).(!empty($this->doctor->fatherName) ? mb_substr($this->doctor->fatherName, 0, 1, 'UTF-8').'.' : ''),
					':address' => (isset($this->address) ? ' Адрес: ' . $this->address->street.', '.$this->address->houseNumber . '.' : '')
				));
				$sms->send();
				*/
				break;
		}
	}

	public function sendSmsToClinic($type) {
		$phone = $this->address->userMedicals->phoneForAlert;

		if ($phone) {
			switch ($type) {
				case 'new_record':
					$sms = new SmsGate();
					$sms->phone = $phone;
					$sms->setMessage('Новая заявка на ' . $this->visitTypeText . '. Дата приема: :date.', array(/* Статус заявки: :stat. Дата приема: :date. */
						':stat' => AppointmentToDoctors::$STATUSES[$this->statusId],
						':date' => strtotime($this->plannedTime) != false ? date("d.m.Y H:i", strtotime($this->plannedTime)) : 'нет даты'
					));

					$sms->send();
					break;
			}
		}
	}

	//Уведомление штатного оператора ЕМП при записи в клинику
	public function notifyOperator($type = 'new') {
		
		switch($type) {
			case 'new': 
				/* sms оператору ЕМП */
				//$sms = new SmsGate();
				//$sms->phone = Yii::app()->params['operatorPhone'];
				//$sms->setMessage('Новая заявка на ' . $this->visitTypeText . ' в клинику. Время отправки: ' . date("H:i:s"));
				//$sms->send();
				

				/* письмо на почту оператору ЕМП */
				$mail = new YiiMailer();
				$mail->setView('report_appointment_without_contract');
				$mail->setData(array(
					'model' => $this
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "#" . date('d.m.Y H:i', strtotime($this->createdDate)) . " " . $this->company->name;
				if(Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['devEmail']);
				else
					$mail->AddAddress(Yii::app()->params['operatorEmail']);
				$mail->Send();
				
				
				/* Письмо для отчета и контроля работы оператора */
				$mail = new YiiMailer();
				$mail->setView('report_appointment_without_contract');
				$mail->setData(array(
					'model' => $this
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "#" . date('d.m.Y H:i', strtotime($this->createdDate)) . " " . $this->company->name;
				if(!Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['reportEmail']);
				$mail->Send();
				
				
				/* если у данной клиники есть планшет с приложением, отсылаем на него push уведомление */
				
				if ($this->address->userMedicals->tabletAppRegistrationId) {
					
					$notificationMessage = [
						'ClinicId' => $this->address->userMedicals->name, //rm //TODO: подсасывать сюда id планшета //пока работает только для одного планшета (DONE)
						'Title' => 'Новая заявка (emportal.ru)',
						'Message' => $this->company->name . ': у вас новая заявка на прием',
					];
					
					$ns = new PushNotificationSender();
					$result = $ns->sendNotification($notificationMessage);				
				}
				break;
			case 'accept': 
				//$sms = new SmsGate();
				//$sms->phone = Yii::app()->params['operatorPhone'];
				//$sms->setMessage('Заявка на прием в клинику '.$this->company->name.' принята.');
				//$sms->send();

				/* письмо на почту оператору */
				$mail = new YiiMailer();
				$mail->setView('report_appointment_without_contract_accept');
				$mail->setData(array(
					'model' => $this
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "Заявка на прием подтверждена. " . $this->company->name;
				if(Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['devEmail']);
				else
					$mail->AddAddress(Yii::app()->params['operatorEmail']);
				$mail->Send();


				/* Письмо для отчета и контроля работы оператора */
				$mail = new YiiMailer();
				$mail->setView('report_appointment_without_contract_accept');
				$mail->setData(array(
					'model' => $this
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "Заявка на прием подтверждена. " . $this->company->name;
				if(!Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['reportEmail']);
				$mail->Send();
				break;
			case 'decline':
				//$sms = new SmsGate();
				//$sms->phone = Yii::app()->params['operatorPhone'];
				//$sms->setMessage('Заявка на прием в клинику '.$this->company->name.' отклонена.');
				//$sms->send();

				/* письмо на почту оператору */
				$mail = new YiiMailer();
				$mail->setView('report_appointment_without_contract_decline');
				$mail->setData(array(
					'model' => $this
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "#" . date('d.m.Y H:i', strtotime($this->createdDate)) . " " . $this->company->name;
				if(Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['devEmail']);
				else
					$mail->AddAddress(Yii::app()->params['operatorEmail']);
				$mail->Send();


				/* Письмо для отчета и контроля работы оператора */
				$mail = new YiiMailer();
				$mail->setView('report_appointment_without_contract_decline');
				$mail->setData(array(
					'model' => $this
				));
				$mail->render();
				$mail->From = 'robot@emportal.ru';
				$mail->FromName = Yii::app()->name;
				$mail->Subject = "#" . date('d.m.Y H:i', strtotime($this->createdDate)) . " " . $this->company->name;
				if(!Yii::app()->params['devMode'])
					$mail->AddAddress(Yii::app()->params['reportEmail']);
				$mail->Send();
				break;
		}
		
	}

	/* validation rule */

	public function checkPhoneActivatedForGuest($attribute) {
		if (Yii::app()->user->isGuest) {
			$value = '+'.MyRegExp::phoneRegExp($this->phone);
			$pv = PhoneVerification::model()->find("phone=:phone AND status=:stat1", array(
				"phone" => $value,
				"stat1" => PhoneVerification::APPOINTMENT,
					//"stat2"=>  PhoneVerification::READY_TO_APPOINTMENT
			));
			if (!$pv) {
				$this->addError($attribute, 'Телефон не активирован');
			}
		}
	}

	public function isBusyDate($attribute) {
		$criteria = new CDbCriteria();
		$criteria->compare("t.addressId", $this->addressId);
		$criteria->compare("t.plannedTime", $this->plannedTime);
		$criteria->addCondition("t.id <> '{$this->id}'");
		$criteria->addCondition("t.statusId <> " . AppointmentToDoctors::DECLINED_BY_PATIENT);
		if (!empty($this->doctorId)) {
			$criteria->compare("t.doctorId", $this->doctorId);
		}
		$model = AppointmentToDoctors::model()->find($criteria);
		if (!empty($model)) {
			$this->addError($attribute, "Выбранная вами дата занята");
		}
	}
	
	
	public function dataProviderForNewAdminPanel($options = []) {

		if (!$options['ignoreRights'] && !empty(Yii::app()->user) && !empty(Yii::app()->user->model) && !Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA'))
			$this->managerId = Yii::app()->user->id;
		
		$criteria = new CDbCriteria;
		$criteria->addCondition("statusId != '".AppointmentToDoctors::DISABLED."'"); //не показываем записи, которые являются просто отключенным временем приема
		$criteria->with = [
			'company' => [
				'together' => true,
			],
			'company.managerRelations' => [
				'together' => true,
			]
		];
		
		if (!empty($this->companyNameLike))
		{
			$criteria->addCondition('company.name LIKE "%'.trim(Yii::app()->db->quoteValue($this->companyNameLike),"'").'%"');
		}
		
		if (!empty($this->managerId))
		{		
			if ($this->managerId != -1)
			{				
				$criteria->addCondition('managerRelations.userId IN (SELECT userId FROM `managerRelations` GROUP BY `companyId` HAVING userId LIKE "'.$this->managerId.'")');
			} else {				
				$criteria->addCondition('managerRelations.userId IS NULL');
			}			
		}
		
		if (!empty($options['region']))
		{
			$criteria->addCondition("company.denormCitySubdomain = '" . $options['region'] . "'");
		}
		
		if (!empty($this->time_period))
		{
			$lower_border = explode('-', $this->time_period)[0];
			$upper_border = explode('-', $this->time_period)[1];
			$upper_border = $upper_border ? $upper_border : $lower_border;
			
			$lower_border = strtotime($lower_border) + 21*60*60;
			$upper_border = strtotime($upper_border) + 21*60*60;
			
			$lower_border = gmdate("Y-m-d 00:00:00", $lower_border);
			$upper_border = gmdate("Y-m-d 23:59:59", $upper_border);
			
			$criteria->addCondition("createdDate >= '" . $lower_border . "'");
			$criteria->addCondition("createdDate <= '" . $upper_border . "'");
		}
		if (!empty($this->visit_time_period))
		{
			$lower_border = explode('-', $this->visit_time_period)[0];
			$upper_border = explode('-', $this->visit_time_period)[1];
			
			$lower_border = strtotime($lower_border) + 21*60*60;
			$upper_border = strtotime($upper_border) + 21*60*60;
			
			$lower_border = gmdate("Y-m-d 00:00:00", $lower_border);
			$upper_border = gmdate("Y-m-d 23:59:59", $upper_border);
			
			$criteria->addCondition("plannedTime >= '" . $lower_border . "'");
			$criteria->addCondition("plannedTime <= '" . $upper_border . "'");
		}
		if (!empty($this->app_type))
		{
			$criteria->compare('appType', $this->app_type, false);
			if($this->app_type == self::APP_TYPE_OWN) {
				self::$showOwn = true;
			}
		}
		//if (!empty($this->visitType))
		//{
		//	$criteria->compare("visitType", $this->visitType);
		//}
		//if (!empty($options['excludedAppType'])) //rf?
		//{
		//	if (is_array($options['excludedAppType']))
		//	{
		//		foreach($options['excludedAppType'] as $exAppType)
		//		{
		//			$criteria->addCondition('appType != "' . $exAppType . '"');
		//		}
		//	} else {
		//		$criteria->addCondition('appType != "' . $options['excludedAppType'] . '"');
		//	}
		//	//$criteria->params[ ':username' ] = $name;
		//}
		if (!empty($options['companyId']))
		{
			$criteria->compare('company.id', $options['companyId'], false);
		}
		if (!empty($options['manStatus']))
		{
			if ($options['manStatus'] == 'r')
			{
				//показать все записи, за которые хотим брать деньги:
				//не обработанные, подтвержденные, дошедшие и со статусом "Указать, дошел ли пациент"
				$criteria->addCondition('(t.managerStatusId = "0" OR t.managerStatusId = "1" OR t.managerStatusId = "3" OR t.managerStatusId = "5" OR t.adminClinicStatusId = "' . AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willPay'] . '")');
				//не из самозаписи, не из виджета записи и не заведенные самой клиникой в ЛКК
				$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_SAMOZAPIS . '"');
				$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_FROM_APP_FORM . '"');
				$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_VK_APP . '"');
				$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_OWN . '"');
				//также исключаем те записи, которые клиника сама отклонила
				$criteria->addCondition('adminClinicStatusId != "' . AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willNotPay'] . '"');
			} elseif ($options['manStatus'] == 'd') {
				//показать все отклоненные (оператором ЕМП или клиникой) записи
				$criteria->addCondition('(t.managerStatusId = "2" OR t.managerStatusId = "4" OR t.adminClinicStatusId = "' . AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willNotPay'] . '")');
				$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_FROM_APP_FORM . '"');
				$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_OWN . '"');
				$criteria->addCondition('appType != "' . AppointmentToDoctors::APP_TYPE_VK_APP . '"');
				$criteria->addCondition('adminClinicStatusId != "' . AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willPay'] . '"');
			} else {
				$criteria->compare("managerStatusId", $options['manStatus']);
			}
		}
		if (!empty($options['addressId']))
		{
			$criteria->compare('address.id', $options['addressId'], false);
		}
		if (!empty($options['userId']))
		{
			$criteria->compare('t.userId', $options['userId'], false);
		}
		if (!empty($options['groupByAddressId']))
		{
			$criteria->group = 't.addressId';
		}
		if ($options['showServices'] === 'withVisits')
		{
			/* показываем и услуги, и заявки на запись на прием
			*/
		} elseif ($options['showServices'] === true)
		{
			$criteria->addCondition('t.serviceId IS NOT NULL');
		} else {
			$criteria->addCondition('t.serviceId IS NULL');
		}
        
        if ($this->patientId) {
            $criteria->with = array_replace_recursive($criteria->with, [
                'user' => [
                    'select' => 'user.id',
                ]
            ]);
            $criteria->compare('user.id', $this->patientId, false);
        }
		switch ($options['has_prePaid']) {
			case '1':
				$criteria->addCondition("t.prePaid > 0");
				break;
			case '0':
				$criteria->addCondition("t.prePaid = 0 OR t.prePaid = '' OR t.prePaid IS NULL");
				break;
			default:
				break;
		}
		
		
		$criteria->compare("statusId", $this->apt_status);
		
		$settings = [
			'criteria' => $criteria,
			'sort' => [
				'defaultOrder'=>'createdDate DESC',
			],
		];
		$pageSize = (key_exists('pageSize', $options)) ? $options['pageSize'] : 25;
		if($pageSize === false) {
			$settings['pagination'] = false;
		} else {
			$settings['pagination'] = array(
				'pageSize' => $pageSize,
			);
		}
		
		if (isset($options['limit']) && $options['limit'])
		{
			$criteria->limit = 1;
			$settings['pagination'] = false;
		}
		
		return new CActiveDataProvider($this, $settings);		
	}
	
	//может ли оператор call-центра ЕМП сменить источник заявки (appType)
	public function hasChangeableApptype() {
		$result = in_array($this->appType, self::$CHANGEABLE_APP_TYPES);
		return $result;
	}
	
	public function getManagerStatusText() {
		return self::$MANAGER_STATUS_ID_TYPES[$this->managerStatusId];
	}
	
	public function getServiceTypeText() {
		return $this->serviceId ? 'Запись на услугу' : 'Запись на прием';
	}
	
	public function getVisitTypeText($extended = false, $periodName = '') {
		$addedText = ($extended && $this->serviceId) ? ' "' . $this->addressService->service->name . '"' : '';
		$visitName = ($extended && !$this->serviceId && !!$periodName) ? ' Услуги привлечения клиента на сайте www.emportal.ru' . $periodName : 'Запись на прием';
		return $this->serviceId ? 'Запись на услугу' . $addedText : $visitName;
	}
	
	public function getVisitTypeSubject() {
		return $this->serviceId ? $this->serviceName : $this->doctor->name;
	}
	
	public function getEmpComission($pricePerVisitor = null) {
		if($this->comission !== NULL AND is_numeric($this->comission)) {
			return $this->comission;
		}
		$comission = (int)$pricePerVisitor;
		if ($this->addressService->service->empComissionAsAppointment == 0 && $this->serviceId != null) {
			$comission = round($this->price * self::EMP_COMISSION);

			$salesContractType = $this->salesContractType;
			if($salesContractType) {
				$minComission = $salesContractType->minPriceService;
			} else {
				$minComission = $this->address->salesContract->salesContractType->minPriceService;
			}
			if($comission < $minComission) {
				$comission = $minComission;
			}
		}
		elseif (!$pricePerVisitor)
		{
			//$salesContractType = $this->address->salesContract->salesContractType;
			$salesContractType = $this->salesContractType;
			//тариф по умолчанию
			if (!$salesContractType) {
				if ($this->address->salesContract) { //если у адреса сейчас есть действующий тариф, подставляем хотя бы его
					$comission = (int)$this->address->pricePerVisitor;
				}
				else { 
					$comission = (int)600;
				}
			}
			elseif ($salesContractType->isFixed) {
				$comission = (int)0;
			}
			else {
				$pricePerVisitor = $salesContractType->price;
				$comission = (int)$pricePerVisitor;
			}
		}
		$result = $comission - intval($this->prePaid);
		$this->comission = ($result < 0) ? 0 : $result;
		return $this->comission;
	}
	
	public function getEmpComissionStr($pricePerVisitor = null) {
		return $this->getEmpComission($pricePerVisitor) . ' руб.';
	}
	
	public function getTotalPrice() {
		
        //TODO: оставить только возврат $this->price
        if ($this->price)
            return (int) $this->price;
        
        if ($this->serviceId != null)
		{
			return $this->addressService->price;
		} else {
			$doctor = $this->doctor;
			if ($doctor && $doctor->inspectPrice)
			{
				return $doctor->inspectFree ? 0 : $doctor->inspectPrice;
			}
		}
		return false;
	}
	
	public function getTotalPriceStr() {
		if ($this->totalPrice !== false)
		{
			return $this->totalPrice . ' руб.';
		} else {
			return 'нет данных';
		}
	}
	
	//будет ли включена данная запись в счет для клиники
	public function getWillBePayedFor() {
		return (
			(
				in_array($this->managerStatusId, ['0', '1', '3', '5']) 
			OR 
				($this->adminClinicStatusId == AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willPay'])
			)
			AND 
			(!in_array($this->appType, [self::APP_TYPE_SAMOZAPIS, self::APP_TYPE_FROM_APP_FORM, self::APP_TYPE_OWN,self::APP_TYPE_VK_APP]))
			AND 
			($this->adminClinicStatusId != AppointmentToDoctors::$CLINIC_STATUS_ID_TYPES['willNotPay'])
		) ? true : false;
	}
    
    public function canSendSmsToPatients() {
        return ($this->company && ($this->company->linkUrl == 'alytermed')) ? false : true;
    }
    
    public function getVoucherNumber() {
    	return (substr($this->id, strlen($this->id)-2, 2)) . (is_numeric($this->link) ? floatval($this->link) : $this->link);
    }

    public function sendDataGA() {
    	$url = 'http://www.google-analytics.com/collect';
    	$post = [
    		'v' => '1',
       		'tid' => 'UA-50143164-1',
    		'cid' => $this->clientIdGA,
    		'uid' => $this->userId,
    		'cd1' => $this->clientIdGA,
    		'cd2' => $this->userId,
    		't' => 'event',
    		'ec' => 'user',
    		'ea' => 'appointmentToDoctor',
    		'el' => time(),
    		'dl' => 'server',
    		'ds' => 'server', 		
    	];

    	$GAUrl = $url.'?';
    	foreach ($post as $key => $value) {
    		$GAUrl .= $key.'='.$value.'&';
    	}
    	$GAUrl = substr($GAUrl,0,-1);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$GAUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		//curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$result = curl_exec($ch);
		curl_close($ch);

    	$log = new LogGA();
    	$attributes = [
    		'appointmentToDoctorsId' => $this->id,
    		'date' => date("Y-m-d"),
    		'time' => date("H:i:s"),
    		'timestamp' => time(),
    		'PaymentTransactionId' => time(),
    		'clientId' => $this->clientIdGA,
    		'userId' => $this->userId,
    		'PaymentValue' => $this->comission,
    		'GArequest' => $GAUrl,
    	];
    	$log->attributes = $attributes;
    	$log->save();
    }
}
