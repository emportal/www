<?php

/**
 * This is the model class for table "contactPersonCompany".
 *
 * The followings are the available columns in table 'contactPersonCompany':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $companyId
 * @property string $position
 * @property string $phonesString
 * @property string $emailString
 * @property string $icqString
 * @property string $phonesStringNoCharacters
 * @property string $surName
 * @property string $firstName
 * @property string $fatherName
 * @property string $birthday
 * @property string $sexId
 * @property string $addressId
 * @property string $nameForSign
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 * @property ContactInformation[] $contactInformations
 * @property Address $address
 * @property Company $company
 * @property Sex $sex
 * @property ContactPersonsCompaniesEmails[] $contactPersonsCompaniesEmails
 * @property ContactPersonsCompaniesICQs[] $contactPersonsCompaniesICQs
 * @property ContactPersonsCompaniesPhones[] $contactPersonsCompaniesPhones
 * @property Email[] $emails
 * @property Phone[] $phones
 */
class ContactPersonCompany extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ContactPersonCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contactPersonCompany';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, companyId, sexId, addressId', 'length', 'max'=>36),
			array('name, link, position, phonesString, emailString, icqString, phonesStringNoCharacters, surName, firstName, fatherName, nameForSign', 'length', 'max'=>150),
			array('birthday', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, companyId, position, phonesString, emailString, icqString, phonesStringNoCharacters, surName, firstName, fatherName, birthday, sexId, addressId, nameForSign', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'contactPersonCompanyId'),
			'contactInformations' => array(self::HAS_MANY, 'ContactInformation', 'contactPersonCompanyId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'sex' => array(self::BELONGS_TO, 'Sex', 'sexId'),
			'contactPersonsCompaniesEmails' => array(self::HAS_MANY, 'ContactPersonsCompaniesEmails', 'contactPersonCompanyId'),
			'contactPersonsCompaniesICQs' => array(self::HAS_MANY, 'ContactPersonsCompaniesICQs', 'contactPersonCompanyId'),
			'contactPersonsCompaniesPhones' => array(self::HAS_MANY, 'ContactPersonsCompaniesPhones', 'contactPersonCompanyId'),
			'emails' => array(self::HAS_MANY, 'Email', 'contactPersonCompanyId'),
			'phones' => array(self::HAS_MANY, 'Phone', 'contactPersonCompanyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'companyId' => 'Company',
			'position' => 'Position',
			'phonesString' => 'Phones String',
			'emailString' => 'Email String',
			'icqString' => 'Icq String',
			'phonesStringNoCharacters' => 'Phones String No Characters',
			'surName' => 'Sur Name',
			'firstName' => 'First Name',
			'fatherName' => 'Father Name',
			'birthday' => 'Birthday',
			'sexId' => 'Sex',
			'addressId' => 'Address',
			'nameForSign' => 'Name For Sign',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('phonesString',$this->phonesString,true);
		$criteria->compare('emailString',$this->emailString,true);
		$criteria->compare('icqString',$this->icqString,true);
		$criteria->compare('phonesStringNoCharacters',$this->phonesStringNoCharacters,true);
		$criteria->compare('surName',$this->surName,true);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('fatherName',$this->fatherName,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('sexId',$this->sexId,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('nameForSign',$this->nameForSign,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}