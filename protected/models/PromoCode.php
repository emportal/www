<?php
/*
CREATE TABLE `promoCode` (
	`id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
	`name` CHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`start` TIMESTAMP NULL DEFAULT NULL,
	`end` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
*/
/**
 * This is the model class for table "promocode".
 *
 * The followings are the available columns in table 'promocode':
 * @property string $id
 * @property string $name
 * @property string $start
 * @property string $end
 */
class PromoCode extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PromoCode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promoCode';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>50),
			array('start, end', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, start, end', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'start' => 'Start',
			'end' => 'End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('start',$this->start,true);
		$criteria->compare('end',$this->end,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave()
	{
		parent::beforeSave();
		if(empty($this->start) || $this->start == '0000-00-00 00:00:00') $this->start = NULL;
		if(empty($this->end) || $this->end == '0000-00-00 00:00:00') $this->end = NULL;
		return true;
	}

	public static function getAvailablePromoCodes() {
		$result = [];
		$criteria=new CDbCriteria;
		$criteria->addCondition('t.start <= :now OR t.start IS NULL OR t.start = ""');
		$criteria->addCondition('t.end >= :now OR t.end IS NULL OR t.end = ""');
		$criteria->params = [
				':now'=>date('Y-m-d H:i:s'),
		];
		$model = self::model()->findAll($criteria);
		foreach ($model as $promoCode) {
			$result[] = $promoCode->name;
		}
		return $result;
	}
}