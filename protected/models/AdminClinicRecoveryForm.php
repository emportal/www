<?php

class AdminClinicRecoveryForm extends CFormModel
{
	public $newPassword;
	public $repeatPassword;
	
	public function rules()
	{
		return array(
			array('newPassword,repeatPassword', 'required'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'newPassword'=>'Новый пароль',
			'repeatPassword'=>'Повтор пароля',
		);
	}
	
	public function validate($attributes = NULL, $clearErrors = true)
	{
		parent::validate($attributes, $clearErrors);
		if($this->newPassword === $this->repeatPassword) {
			if(mb_strlen($this->newPassword) >= 8 && preg_match('/[A-Za-zА-Яа-я]+[0-9]+/', $this->newPassword)) {
				return true;
			} else {
				$this->addError("newPassword", "Пароль должен быть длинной не менее 8 символов с минимумом одной буквой и одной цифрой");
				$this->addError("repeatPassword", "");
			}
		} else {
			$this->addError("newPassword", "");
			$this->addError("repeatPassword", "Пароль не совпадает");
		}
		return false;
	}
}
