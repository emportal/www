<?php

/**
 * This is the model class for table "contactManager".
 *
 * The followings are the available columns in table 'contactManager':
 * @property integer $id
 * @property string $userMedicalId
 * @property string $subject
 * @property string $msgbody
 * @property string $createDate
 *
 * The followings are the available model relations:
 * @property UserMedical $userMedical
 */
class ContactManager extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ContactManager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contactManager';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userMedicalId, subject, msgbody', 'required'),
			array('userMedicalId', 'length', 'max'=>36),
			array('subject', 'length', 'max'=>120),
			array('createDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userMedicalId, subject, msgbody, createDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userMedical' => array(self::BELONGS_TO, 'UserMedical', 'userMedicalId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userMedicalId' => 'User Medical',
			'subject' => 'Subject',
			'msgbody' => 'Msgbody',
			'createDate' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userMedicalId',$this->userMedicalId,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('msgbody',$this->msgbody,true);
		$criteria->compare('createDate',$this->createDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}