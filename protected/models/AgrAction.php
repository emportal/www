<?php

/**
 * This is the model class for table "agr_action".
 *
 * The followings are the available columns in table 'agr_action':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $companyId
 * @property string $description
 * @property string $shortDescription
 * @property string $start
 * @property string $end
 * @property integer $completed
 * @property integer $publish
 * @property string $employeeId
 * @property string $serviceId
 * @property integer $priceOld
 * @property integer $priceNew
 * @property integer $discount
 */
class AgrAction extends AgrActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agr_action';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unlimited, completed, publish, priceOld, priceNew, discount, type', 'numerical', 'integerOnly' => true, 'message'=>'Поле {attribute} должно быть целым числом'),
			array('name,present','length','max'=>40,'tooLong'=>"Длина акции должна быть меньше 40"),
			array('discount', 'required','on'=>'Discount'),
			array('name','required','message'=>'Заполните название акции'),
			array('companyId, companyActivitesId', 'required'),
			array('id, companyId, addressId, employeeId, serviceId, companyActivitesId', 'length', 'max' => 36),
			//array('serviceId', 'length', 'min' => 36),
			array('link', 'length', 'max' => 150),
			array('type','default','value'=>1),
			//array('name,present', 'length', 'max' => 150),
			array('start, end, description', 'safe'),
			array('priceNew','compare',"compareAttribute"=>"priceOld","operator"=>"<=", "on"=>"Discount","message"=>"Новая цена должна быть ниже старой"),
			array('priceNew','numerical','integerOnly'=>true, 'min'=>1,"on"=>"Discount","tooSmall"=>"Если цена будет равна нулю - выберите тип бесплатно"),
			array('priceNew,priceOld','required',"on"=>"Discount"),
			array('start,end','notNull'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, companyId, description, start, end, unlimited, completed, publish, employeeId, serviceId, priceOld, priceNew, discount', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'service'	 => array(self::BELONGS_TO, 'Service', 'serviceId'),
			'company'	 => array(self::BELONGS_TO, 'Company', 'companyId'),
			'address'	 => array(self::BELONGS_TO, 'Address', 'addressId'),
			'employee'	 => array(self::BELONGS_TO, 'Employee', 'employeeId'),
			'companyActivite'	 => array(self::BELONGS_TO, 'CompanyActivite', 'companyActivitesId'),
				
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'			 => 'ID',
			'name'			 => 'Наименование акции',
			'link'			 => 'Link',
			'companyActivitesId' => 'Профиль деятельности',
			'companyId'		 => 'Company',
			'description'	 => 'Описание',
			'start'			 => 'Start',
			'end'			 => 'End',
			'unlimited'		 => 'Unlimited',
			'completed'		 => 'Completed',
			'publish'		 => 'Publish',
			'employeeId'	 => 'Employee',
			'serviceId'		 => 'Service',
			'priceOld'		 => 'Стоимость до',
			'priceNew'		 => 'Стоимость по скидке',
			'discount'		 => 'Скидка',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('shortDescription',$this->shortDescription,true);
		$criteria->compare('start',$this->start,true);
		$criteria->compare('end',$this->end,true);
		$criteria->compare('completed',$this->completed);
		$criteria->compare('publish',$this->publish);
		$criteria->compare('employeeId',$this->employeeId,true);
		$criteria->compare('serviceId',$this->serviceId,true);
		$criteria->compare('priceOld',$this->priceOld);
		$criteria->compare('priceNew',$this->priceNew);
		$criteria->compare('discount',$this->discount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function notNull($attribute) {
		if(!$this->unlimited && empty($this->$attribute)) {			
			$this->addError($attribute,'Необходимо заполнить поле '.$this->getAttributeLabel($attribute));
		}
	}
}