<?php

/**
 * This is the model class for table "physicalPerson".
 *
 * The followings are the available columns in table 'physicalPerson':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $surName
 * @property string $firstName
 * @property string $fatherName
 * @property string $birthday
 * @property string $taxpayerId
 * @property string $codeTax
 * @property string $insuranceNumber
 * @property string $birthPlace
 * @property string $sexId
 * @property string $nameForSign
 *
 * The followings are the available model relations:
 * @property ContactInformation[] $contactInformations
 * @property Employee[] $employees
 * @property Sex $sex
 */
class PhysicalPerson extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PhysicalPerson the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'physicalPerson';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, sexId', 'length', 'max'=>36),
			array('name, link, surName, firstName, fatherName, taxpayerId, codeTax, insuranceNumber, birthPlace, nameForSign', 'length', 'max'=>150),
			array('birthday', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, surName, firstName, fatherName, birthday, taxpayerId, codeTax, insuranceNumber, birthPlace, sexId, nameForSign', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactInformations' => array(self::HAS_MANY, 'ContactInformation', 'physicalPersonId'),
			'employees' => array(self::HAS_MANY, 'Employee', 'physicalPersonId'),
			'sex' => array(self::BELONGS_TO, 'Sex', 'sexId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'surName' => 'Sur Name',
			'firstName' => 'First Name',
			'fatherName' => 'Father Name',
			'birthday' => 'Birthday',
			'taxpayerId' => 'Taxpayer',
			'codeTax' => 'Code Tax',
			'insuranceNumber' => 'Insurance Number',
			'birthPlace' => 'Birth Place',
			'sexId' => 'Sex',
			'nameForSign' => 'Name For Sign',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('surName',$this->surName,true);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('fatherName',$this->fatherName,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('taxpayerId',$this->taxpayerId,true);
		$criteria->compare('codeTax',$this->codeTax,true);
		$criteria->compare('insuranceNumber',$this->insuranceNumber,true);
		$criteria->compare('birthPlace',$this->birthPlace,true);
		$criteria->compare('sexId',$this->sexId,true);
		$criteria->compare('nameForSign',$this->nameForSign,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}