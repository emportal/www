<?php

/**
 * This is the model class for table "contactPersonsCompaniesICQs".
 *
 * The followings are the available columns in table 'contactPersonsCompaniesICQs':
 * @property string $id
 * @property string $contactPersonCompanyId
 * @property string $ICQ
 *
 * The followings are the available model relations:
 * @property ContactPersonCompany $contactPersonCompany
 */
class ContactPersonsCompaniesICQs extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ContactPersonsCompaniesICQs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contactPersonsCompaniesICQs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, contactPersonCompanyId', 'length', 'max'=>36),
			array('ICQ', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contactPersonCompanyId, ICQ', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactPersonCompany' => array(self::BELONGS_TO, 'ContactPersonCompany', 'contactPersonCompanyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contactPersonCompanyId' => 'Contact Person Company',
			'ICQ' => 'Icq',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('contactPersonCompanyId',$this->contactPersonCompanyId,true);
		$criteria->compare('ICQ',$this->ICQ,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}