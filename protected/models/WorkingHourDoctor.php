<?php

/**
 * This is the model class for table "workingHourDoctor".
 *
 * The followings are the available columns in table 'workingHourDoctor':
 * @property string $id
 * @property string $addressId
 * @property string $doctorId
 * @property string $MondayStart
 * @property string $MondayFinish
 * @property string $TuesdayStart
 * @property string $TuesdayFinish
 * @property string $WednesdayStart
 * @property string $WednesdayFinish
 * @property string $ThursdayStart
 * @property string $ThursdayFinish
 * @property string $FridayStart
 * @property string $FridayFinish
 * @property string $SaturdayStart
 * @property string $SaturdayFinish
 * @property string $SundayStart
 * @property string $SundayFinish
 */
class WorkingHourDoctor extends CActiveRecord
{
	const DEFAULT_TIME_START = "08:00";
	const DEFAULT_TIME_FINISH = "22:00";

	public $workingHoursStep = array('' => 'как у клиники', '11111' => 'круглосуточно',
		'00:00' => '00:00', '00:30' => '00:30', '01:00' => '01:00', '01:30' => '01:30',
		'02:00' => '02:00', '02:30' => '02:30', '03:00' => '03:00', '03:30' => '03:30',
		'04:00' => '04:00', '04:30' => '04:30', '05:00' => '05:00', '05:30' => '05:30',
		'06:00' => '06:00', '06:30' => '06:30', '07:00' => '07:00', '07:30' => '07:30',
		'08:00' => '08:00', '08:30' => '08:30', '09:00' => '09:00', '09:30' => '09:30',
		'10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30',
		'12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30',
		'14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30',
		'16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30',
		'18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30',
		'20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00', '21:30' => '21:30',
		'22:00' => '22:00', '22:30' => '22:30', '23:00' => '23:00', '23:30' => '23:30',
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'workingHourDoctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, addressId, doctorId', 'length', 'max'=>36),
			array('MondayStart, MondayFinish, TuesdayStart, TuesdayFinish, WednesdayStart, WednesdayFinish, ThursdayStart, ThursdayFinish, FridayStart, FridayFinish, SaturdayStart, SaturdayFinish, SundayStart, SundayFinish', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, addressId, doctorId, MondayStart, MondayFinish, TuesdayStart, TuesdayFinish, WednesdayStart, WednesdayFinish, ThursdayStart, ThursdayFinish, FridayStart, FridayFinish, SaturdayStart, SaturdayFinish, SundayStart, SundayFinish', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'addressId' => 'Address',
			'doctorId' => 'Doctor',
			'MondayStart' => 'Понедельник',
			'MondayFinish' => 'Пн',
			'TuesdayStart' => 'Вторник',
			'TuesdayFinish' => 'Вт',
			'WednesdayStart' => 'Среда',
			'WednesdayFinish' => 'Ср',
			'ThursdayStart' => 'Четверг',
			'ThursdayFinish' => 'Чт',
			'FridayStart' => 'Пятница',
			'FridayFinish' => 'Пт',
			'SaturdayStart' => 'Суббота',
			'SaturdayFinish' => 'Сб',
			'SundayStart' => 'Воскресенье',
			'SundayFinish' => 'Вс',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('MondayStart',$this->MondayStart,true);
		$criteria->compare('MondayFinish',$this->MondayFinish,true);
		$criteria->compare('TuesdayStart',$this->TuesdayStart,true);
		$criteria->compare('TuesdayFinish',$this->TuesdayFinish,true);
		$criteria->compare('WednesdayStart',$this->WednesdayStart,true);
		$criteria->compare('WednesdayFinish',$this->WednesdayFinish,true);
		$criteria->compare('ThursdayStart',$this->ThursdayStart,true);
		$criteria->compare('ThursdayFinish',$this->ThursdayFinish,true);
		$criteria->compare('FridayStart',$this->FridayStart,true);
		$criteria->compare('FridayFinish',$this->FridayFinish,true);
		$criteria->compare('SaturdayStart',$this->SaturdayStart,true);
		$criteria->compare('SaturdayFinish',$this->SaturdayFinish,true);
		$criteria->compare('SundayStart',$this->SundayStart,true);
		$criteria->compare('SundayFinish',$this->SundayFinish,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkingHourDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getTimeStart($date) {
		$time = $this->getAttribute(date("l", strtotime($date)) . "Start");
		if($time == "11111") {
			$time = "00:00";
		}
		return $time;
	}

	public function getTimeFinish($date) {
		$time = $this->getAttribute(date("l", strtotime($date)) . "Finish");
		if($time == "11111") {
			$time = "23:59";
		}
		return $time;		
	}

	public function afterSave() {
		if (parent::afterSave()) {
			Yii::app()->cache->delete(md5('timeBlock'.$this->addressId.$this->doctorId));
			return true;
		}
	}

}
