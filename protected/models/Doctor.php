<?php

/**
 * This is the model class for table "doctor".
 *
 * The followings are the available columns in table 'doctor':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $linkUrl
 * @property string $surName
 * @property string $firstName
 * @property string $fatherName
 * @property string $birthday
 * @property string $sexId
 * @property string $experience
 * @property string $experienceDate
 * @property integer $experiencePeriod
 * @property string $staffUnitId
 * @property integer $onHouse
 * @property string $currentPlaceOfWorkId
 * @property integer $rating
 * @property string $scientificTitleId
 * @property integer $hasPhoto
 * @property integer $countRecords
 *
 * The followings are the available model relations:
 * @property AppointmentToDoctors[] $appointmentToDoctors
 * @property Comment[] $comments
 * @property ScientificTitle $scientificTitle
 * @property PlaceOfWork $currentPlaceOfWork
 * @property Sex $sex
 * @property StaffUnit $staffUnit
 * @property DoctorEducation[] $doctorEducations
 * @property DoctorEducation[] $doctorEducationsCourseCount
 * @property DoctorScientificDegree[] $doctorScientificDegrees
 * @property ScientificDegree[] $scientificDegrees
 * @property DoctorServices[] $doctorServices
 * @property Phone[] $phones
 * @property PlaceOfWork[] $placeOfWorks
 * @property Ref[] $refs
 * @property SpecialtyOfDoctor[] $specialtyOfDoctors
 * @property WorkingHoursOfDoctors[] $workingHoursOfDoctors
 * @property DoctorSpecialty[] $specialties
 */
class Doctor extends ActiveRecord {

	const CONTEXT_NONE = 0;
	const CONTEXT_ACTIVATED = 1;
	const CONTEXT_DISABLED = 2;
	const CONTEXT_RECREATE = 3;
	const CONTEXT_CREATE = 4;

	public $doctorSpecialty;
	protected $_photo;
	protected $_mobilePhoto;
	
	public static $notUsedMediSale = array(
			"proshkova-tatyyana-vladimirovna",
			"filippova-tatyyana-vladimirovna-1",
			"mihaylov-ivan-viktorovich",
			"belova-roksana-akbarovna",
			"lisukova-elena-vladimirovna",
			"kulikova-svetlana-yuryevna",
			"tafrishyyan-nelya-ramisovna",
			"konykova-svetlana-vladimirovna",
			"grigoryeva-yuliya-vitalyevn",
			"grigoryeva-yuliya-vitalyevn",
			"gavrilova-veronika-mayevna",
			"gladkaya-ekaterina-olegovna",
			"muradyan-aleksandr-rafikovich",
			"naumenko-nikolay-nikolaevich",
			"kuzymina-irina-vitalyevna",
			"churina-irina-gennadyevna",
			"vihrov-anatoliy-petrovich_1",
			"lopatina-elizaveta-aleksandrovna",
			"filippova-olyga-andreevna",
			"hramcova-sofiya-viktorovna",
			"andreeva-irina-lyvovna",
			"batyukov-nikolay-mihaylovich",
			"kochetkova-olyga-alekseevna",
			"apackaya-ilona-valeryevna",
			"kozicyna-viktoriya-valeryevna",
			"krotova-natalyya-vasilyevna",
			"volkova-yuliya-valeryevna",
			"vinogradov-sergey-yuryevich",
			"galkin-dmitriy-galkin-dmitriy-vladimirov",
			"filonenko-sergey-aleksandrovich",
			"gusarina-elena-ivanovna",
			"plughnikova-mariya-mariusovna",
			"polyschikova-irina-valeryevna",
			"gelyshteyn-konstantin-borisovich",
			"hoschevskaya-irina-anatolyevna",
			"tertychnaya-irina-vleryevna",
			"minabutdinova-marina-evgenyevna",
			"marutyan-liliya-armenovna",
			"nikolaeva-anna-anatolyevna",
			"golydshteyn-elena-vladimirovna_1",
			"kozicyna-svetlana-ivanovna",
			"pozdyakova-yuliya-sergeevna",
			"spicyna-marina-sergeevna",
			"ashour-ahmed-zuhdi_2",
			"fedorov-oleg-valerievich",
			"kudryashova-svetlana-mihaylovna",
			"lebedeva-elena-borisovna",
			"glazyrina-natalyya-sergeevna",
			"kalitinova-elena-valeryevna",
			"borisov-dmitriy-aleksandrovich",
			"kobiashvili-mziya-ivanovna",
			"hazova-elena-leonidovna",
			"ryghova-irina-gennadyevna",
			"yanchenko-tatyyana-mihaylovna",
			"shapiro-evgeniya-germanovna",
			"golydshteyn-elena-vladimirovna_1",
			"parshina-natalyya-mihaylovna",
			"hoschevskaya-irina-anatolyevna",
			"avericheva-elina-borisovna",
			"dobychkina-anghelika-grigoryevna",
	);
	
	public static $filterDeleted = true;
	//public $scientificDegrees;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Doctor the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	
	public function behaviors() {
		return array(
			'linkUrlBehavior' => array(
				'class' => 'application.components.behaviors.LinkUrlBehavior',				
			)
		);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'doctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description', 'filter', 'filter' => function($v) { return strip_tags($v); }),
			array('description', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
			array('description', 'length', 'max' => 5000),
			array('surName, firstName, sexId', 'required'),
			array('experiencePeriod, onHouse', 'numerical', 'integerOnly' => true),
			array('rating,ratingSystem,ratingUsers', 'numerical'),
			array('rating,ratingSystem,ratingUsers', 'default', 'value' => 0),
			array('id, sexId, staffUnitId, currentPlaceOfWorkId, scientificTitleId', 'length', 'max' => 36),
			array('name, link, surName, firstName, fatherName, experience', 'length', 'max' => 150),
			array('experience', 'numerical', 'integerOnly' => true, 'min' => 1930, 'tooSmall' => 'Неверная дата'),
			array('name, link, surName, firstName, fatherName, birthday, sexId, experience, experienceDate, experiencePeriod, staffUnitId, onHouse, currentPlaceOfWork, scientificTitleId, hasPhoto', 'safe'),
			array(
				'photo',
				'file',
				'types' => 'jpg, gif, png',
				'maxSize' => 1024 * 1024 * 5, // 5MB
				'allowEmpty' => 'true',
				'tooLarge' => 'The file was larger than 5MB. Please upload a smaller file.',
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, surName, firstName, fatherName, birthday, sexId, experience, experienceDate, experiencePeriod, staffUnitId, onHouse, currentPlaceOfWork, rating, scientificTitleId, hasPhoto', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appointmentCount' => array(self::STAT, 'AppointmentToDoctors', 'doctorId', 'condition' => 't.statusId=' . AppointmentToDoctors::VISITED),
			'appointmentToDoctors' => array(self::HAS_MANY, 'AppointmentToDoctors', 'doctorId'),
			'comments' => array(self::HAS_MANY, 'RaitingDoctorUsers', 'doctorsId', 'scopes' => 'moderated'),
			'commentsAll' => array(self::HAS_MANY, 'Comment', 'addressId'),
			//'currentPlaceOfWork' => array(self::BELONGS_TO, 'Company', 'currentPlaceOfWorkId'),
			'currentPlaceOfWork' => array(self::HAS_ONE, 'PlaceOfWork', 'doctorId', 'scopes' => 'current'),
			'importPlaceOfWork' => array(self::HAS_ONE, 'PlaceOfWork', 'doctorId', 'scopes' => 'import'),
			'sex' => array(self::BELONGS_TO, 'Sex', 'sexId'),
			'staffUnit' => array(self::BELONGS_TO, 'StaffUnit', 'staffUnitId'),
			'doctorEducations' => array(self::HAS_MANY, 'DoctorEducation', 'doctorId', 'with' => 'typeOfEducation', 'order' => 'typeOfEducation.order'),
			#'doctorEducationsCourseCount'=> array(self::STAT,'DoctorEducation','doctorId','with'=>'typeOfEducation','condition'=>'typeOfEducation.name = "Повышение квалификации"'),
			'doctorScientificDegrees' => array(self::HAS_MANY, 'DoctorScientificDegree', 'doctorId'),
			'scientificTitle' => array(self::BELONGS_TO, 'ScientificTitle', 'scientificTitleId'),
			'scientificDegrees' => array(self::MANY_MANY, 'ScientificDegree', 'doctorScientificDegree(doctorId,scientificDegreeId)'),
			'doctorServices' => array(self::HAS_MANY, 'DoctorServices', 'doctorId'),
			'phones' => array(self::HAS_MANY, 'Phone', 'ownerId'),
			'placeOfWorks' => array(self::HAS_MANY, 'PlaceOfWork', 'doctorId'),
			//'address'					 => array(self::HAS_ONE, 'Address', 'addressId', 'through' => 'currentPlaceOfWork'),
			'refs' => array(self::HAS_MANY, 'Ref', 'ownerId'),
			'specialtyOfDoctors' => array(self::HAS_MANY, 'SpecialtyOfDoctor', 'doctorId'),
			'specialties' => array(self::HAS_MANY, 'DoctorSpecialty', 'doctorSpecialtyId', 'through' => 'specialtyOfDoctors'),
			'workingHoursOfDoctors' => array(self::HAS_MANY, 'WorkingHoursOfDoctors', 'doctorId'),
			'reviews' => array(self::HAS_MANY, 'Review', 'doctorId'),
			'publishedReviews' => array(self::HAS_MANY, 'Review', 'doctorId', 'condition' => 'publishedReviews.statusId=2'),
			'publishedReviewsCount' => array(self::STAT, 'Review', 'doctorId', 'condition' => 't.statusId=2'),
            'extDoctor' => array(self::HAS_ONE, 'ExtDoctor', 'doctorId'),
            '_workingHours' => array(self::HAS_ONE, 'WorkingHourDoctor', 'doctorId'),
			//'scientificDegreesCount'	 => array(self::STAT, 'ScientificDegree', 'doctorScientificDegree(doctorId,scientificDegreeId)'),
			//'cityDistrict'				 => array(self::HAS_ONE, 'CityDistrict', 'cityDistrictId', 'through'	 => 'address'),
			//'nearestMetroStations'		 => array(self::HAS_MANY, 'NearestMetroStation', array('id'=>'addressId'), 'through'	 => 'address'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'ФИО',
			'description' => 'Текст',
			'link' => 'Link',
			'surName' => 'Фамилия',
			'firstName' => 'Имя',
			'fatherName' => 'Отчество',
			'birthday' => 'Дата рождения',
			'sexId' => 'Пол',
			'experience' => 'Стаж работы',
			'experienceDate' => 'Стаж работы',
			'experiencePeriod' => 'Experience Period',
			'staffUnitId' => 'Staff Unit',
			'onHouse' => 'Выезд врача на дом',
			'currentPlaceOfWorkId' => 'Current Place Of Work',
			'rating' => 'Rating',
			'scientificTitle' => 'Ученое звание',
			'scientificTitleId' => 'Ученое звание',
			'scientificDegrees' => 'Ученая степень',
			'hasPhoto' => 'Наличие фотографии',
			'countRecords' => 'Количество записей',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($clinic) {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->with = array(
			'sex',
			'currentPlaceOfWork',
			'currentPlaceOfWork.address',
			'specialties' => array(
				'select' => 'specialties.name',
				'together' => 'true'
			),
		);

		$criteria->group = 't.id';
		$criteria->compare('`address`.`id`', $clinic->id);

		$criteria->compare('`t`.`id`', $this->id, true);
		$criteria->compare('`t`.`name`', $this->name, true);
// 		$criteria->compare('link', $this->link, true);
		$criteria->compare('`t`.`surName`', $this->surName, true);
		$criteria->compare('`t`.`firstName`', $this->firstName, true);
		$criteria->compare('`t`.`fatherName`', $this->fatherName, true);
		$criteria->compare('`t`.`birthday`', $this->birthday, true);
		$criteria->compare('`sex`.`name`', $this->sexId, true);
		$criteria->compare('`t`.`experience`', $this->experience, true);
		$criteria->compare('`t`.`experienceDate`', $this->experienceDate, true);
		$criteria->compare('`t`.`experiencePeriod`', $this->experiencePeriod);
		$criteria->compare('`t`.`staffUnitId`', $this->staffUnitId, true);
		$criteria->compare('`t`.`onHouse`', $this->onHouse);
		$criteria->compare('`t`.`currentPlaceOfWorkId`', $this->currentPlaceOfWorkId, true);
		$criteria->compare('`t`.`rating`', $this->rating);
		$criteria->compare('`t`.`scientificTitleId`', $this->scientificTitleId, true);
		$criteria->compare('`specialties`.`name`', $this->doctorSpecialty->name, true);



		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 30,
			),
			'sort' => array(
				'attributes' => array(
					'specialties.name', '*'
				),
				'defaultOrder' => 't.name ASC',
			),
		));
	}
	public function count($condition='',$params=array()) {		
		$condition->addCondition($this->tableAlias.".deleted <> '1'");
		
		return parent::count($condition,$params);
	}
	
	public function beforeSave() {
		if (parent::beforeSave()) {
			if (!$this->name || $this->name == '')
				$this->name = preg_replace("/[\s\s]+/iu", " ", implode(' ', array($this->surName, $this->firstName, $this->fatherName)));
			
			return true;
		} else {
			return false;
		}
	}

	public function beforeFind() {
		parent::beforeFind();
		if(self::$filterDeleted) {
			$criteria = $this->dbCriteria;
			$criteria->addCondition($this->tableAlias . ".deleted <> 1");
			$this->dbCriteria = $criteria;
		}
	}

	public function getWorkingHours() {

		if (empty($this->_workingHours)) {
			$this->_workingHours = new WorkingHourDoctor();
			$this->_workingHours->doctorId = $this->id;
			$this->_workingHours->id = ActiveRecord::create_guid(array($this->id));
		}

		return $this->_workingHours;
	}

	public function setWorkingHours($value) {
		$this->getWorkingHours();

		$this->_workingHours->attributes = $value;

		$this->_workingHours->save();
	}

	function yearTextArg($year) {
		$year = abs($year);
		$t1 = $year % 10;
		$t2 = $year % 100;
		return ($t1 == 1 && $t2 != 11 ? "год" : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? "года" : "лет"));
	}

	function getExperience() {
		$year = (intval($this->experience) > 1900 ) ? (date("Y") - intval($this->experience)) : intval($this->experience);
		return ($year > 0) ? $year . ' ' . $this->yearTextArg($year) : 0;
	}

	public function getIsFavorite() {
		if (Yii::app()->user->isGuest)
			return false;

		return in_array($this->id, Yii::app()->user->model->favoritesArray);
	}

	public function getFio() {
		return implode('.', array_map(function ($var) {
							return mb_substr($var, 0, 1);
						}, array(
					$this->surName,
					$this->firstName,
					$this->fatherName
								)
				)) . '.';
	}

	public function getShortName() {
		return sprintf('%s %s.%s.', $this->surName, mb_substr($this->firstName, 0, 1), mb_substr($this->fatherName, 0, 1));
	}

	public function setSpecialtyOfDoctors($data)
	{
		if (!is_array($data)) {
			$data = $data ? array($data) : array();
		}
		
		$allSpecialty = SpecialtyOfDoctor::model()->findAllByAttributes(["doctorId" => $this->id]);
		foreach ($allSpecialty as $value) {
			foreach ($data as $key => $specialty) {
				if($specialty['id'] == $value->id) continue(2);
			}
			$value->delete();
		}
		foreach ($data as $key => &$specialty) {
			if (is_object($specialty))
				continue;

			if (is_array($specialty)) {

				if (!$object = SpecialtyOfDoctor::model()->findByPk($specialty['id'])) {
					$object = new SpecialtyOfDoctor();
					$object->doctorId = $this->id;
				}

				$object->attributes = $specialty;
				$object->id = ActiveRecord::create_guid(array(
							$object->doctorId,
							$object->doctorSpecialtyId,
							$object->doctorCategoryId)
				);

				if (!$object->save()) {
					echo " -- ";
					MyTools::showErrors($object->getErrors());
					unset($data[$key]);
					continue;
				}
				$specialty = $object;
			} else {
				$specialty = SpecialtyOfDoctor::model()->findByPk($specialty);
			}
		}

		$this->specialtyOfDoctors = $data;
		$this->save();
		SpecialtyOfDoctor::model()->deleteAll("doctorId IS NULL");
	}

	public function setDoctorEducations($data)
	{
		if (!is_array($data)) {
			$data = $data ? array($data) : array();
		}
		
		$allEducation = DoctorEducation::model()->findAllByAttributes(["doctorId" => $this->id]);
		foreach ($allEducation as $value) {
			foreach ($data as $key => $education) {
				if($education['id'] == $value->id) continue(2);
			}
			$value->delete();
		}
		foreach ($data as $key => &$education) {
			if (is_object($education))
				continue;

			if (is_array($education)) {
				if (!$object = DoctorEducation::model()->findByPk($education['id'])) {
					$object = new DoctorEducation();
					$object->doctorId = $this->id;
				}
				$object->attributes = $education;
				/* $object->id = ActiveRecord::create_guid(array(
				  $object->doctorId,
				  $object->typeOfEducationId,
				  $object->medicalSchoolId,
				  $object->yearOfStady
				  ));
				  unset($object->id); */
				if (!$object->save()) {
					unset($data[$key]);
					continue;
				}
				$education = $object;
			} else {
				$education = SpecialtyOfDoctor::model()->findByPk($education);
			}
		}

		$this->doctorEducations = $data;
		$this->save();
		$models = DoctorEducation::model()->findAll("doctorId IS NULL");
		foreach ($models as $md) {
			$md->delete();
		}
	}

	public function setCurrentPlaceOfWork(Address $address) {

		if ($this->currentPlaceOfWork->addressId === $address->id)
			return;

		$model = new PlaceOfWork();

		$criteria = new CDbCriteria();
		$criteria->compare('doctorId', $this->id);

		PlaceOfWork::model()->updateAll(array('current' => 0), $criteria);

		$model->address = $address;
		$model->doctor = $this;
		$model->company = $address->company;
		$model->current = 1;
		$model->dateStart = date('Y-m-d H:i:s');

		$model->save();

		if (!empty($model->errors))
			Yii::log('errors: ' . print_r($model->errors, true), CLogger::LEVEL_ERROR, 'adminClinic.experts');

		$this->currentPlaceOfWorkId = $address->company->id;
		$this->save();
	}

	public function getUploadPath() {

		$path = Yii::getPathOfAlias('uploads.doctor') . DIRECTORY_SEPARATOR . $this->link;

		if (!is_dir($path))
			mkdir($path, 0775, true);
		return $path;
	}

	public function getUploadUrl() {

		return '/uploads/doctor/' . $this->link;
	}

	public function getPhotoPath() {
		if ( is_file("{$this->uploadPath}/photo.jpg") ) {
			return "{$this->uploadPath}/photo.jpg";
		}
		elseif ( is_file("{$this->uploadPath}/photo.png") ) {
			return "{$this->uploadPath}/photo.png";
		}
		return false;
	}

	public function getPhoto() {

		if (!isset($this->_photo)) {
			$photoPath = $this->photoPath;
			if(is_file($photoPath)) {
				$this->_photo = mb_substr($photoPath, mb_strripos($photoPath, 'photo.'));
			} else {
				$this->_photo = false;
			}
		}

		return $this->_photo;
	}

	public function setPhoto($value) {
		$this->_photo = $value;
	}

	public function getDoctorPhotoUrl() {
		$url = $this->originalPhotoUrl;
		if(empty($url)) {
			$url = $this->photo ? "{$this->uploadUrl}/{$this->photo}" : null;
			if(!empty($url)) {
				$this->originalPhotoUrl = $url;
				$this->update();
			}
		}
		return $url;
	}

	public function getPhotoUrl() {
		return $this->photo ? "/site/image?src={$this->getDoctorPhotoUrl()}" ."?z=" . rand() : false;
	}

	public function getMobilePhotoPath() {
		return "{$this->uploadPath}/mobile.png";
	}

	public function getMobilePhoto() {

		if (!$this->_mobilePhoto) {
			$this->_mobilePhoto = is_file($this->mobilePhotoPath) ? "mobile.png?z=" . rand() : false;
		}

		return $this->_mobilePhoto;
	}

	public function getMobilePhotoUrl() {
		return $this->mobilePhoto ? "{$this->uploadUrl}/$this->mobilePhoto" : false;
	}

	public function delete() {
		if (is_file($this->photoPath)) {
			unlink($this->photoPath);
		}
		$this->deleted = 1;
		return $this->save() ? 1 : 0;

		//return parent:: delete();
	}

	//Первоначальное обращение
	public function getInspectPrice() {
		foreach ($this->doctorServices as $d) {
			if ($d->cassifierService->link == "first") {
				return $d->price;
			}
		}
	}

	//Первоначальное обращение статус
	public function getInspectFree() {
		foreach ($this->doctorServices as $d) {
			if ($d->cassifierService->link == "first") {
				return $d->free;
			}
		}
	}

	public function getExperienceNum() {
		if ((int) $this->experience)
			return (int) date('Y') - (int) $this->experience;
		return '';
	}

	public function getRoundedRating() {
		return (round($this->rating * 100) - round($this->rating * 100) % 10) / 100;
	}

	public function getRoundedRatingSystem() {
		return (round($this->ratingSystem * 100) - round($this->ratingSystem * 100) % 10) / 100;
	}

	public function getRoundedRatingUser() {
		return (round($this->ratingUsers * 100) - round($this->ratingUsers * 100) % 10) / 100;
	}

	public function getBoldedRoundedRating() {
		$data = explode(".", $this->roundedRating);
		return '<span>' . $data[0] . '</span>.' . (!empty($data[1]) ? $data[1] : 0);
	}
	
	public function getCompany() {
		$company = $this->placeOfWorks[0]->company;
		return $company;
	}
	
	//Получить список страниц, на которых выводится врач
	public function getPagesUrls($addressId = null, $onlyRelativeURI = false)
	{
		$pagesUrls = [];
		$placeOfWorks = $this->placeOfWorks;
		$hosts = [
			'emportal.ru',
		];
		$devHosts = [
			'dev1.emportal.ru',
			'dev2.emportal.ru',
			'dev3.emportal.ru',
			'dev4.emportal.ru',
			'local.emportal.ru',
		];
		if (Yii::app()->params['devMode'])
			$hosts = array_merge($hosts, $devHosts);
		
		foreach($placeOfWorks as $placeOfWork)
		{
			$address = $placeOfWork->address;
			if ($addressId AND $addressId != $address->id)
				continue;
			
			$possbileSubdomains = [];
			$subdomain = $address->company->denormCitySubdomain;
			if ($address->samozapis == 1)
			{
				$possbileSubdomains[] =  Yii::app()->params['regions'][$subdomain]['samozapisSubdomain'];
			} else {
				$possbileSubdomains[] = ($subdomain == 'spb') ? '' : $subdomain;
			}
			$pageUrlWithoutHost = '/klinika/' . $placeOfWork->company->linkUrl . '/' . $placeOfWork->address->linkUrl . '/' . $this->linkUrl;
			if ($onlyRelativeURI)
			{
				$pagesUrls = [$pageUrlWithoutHost];
			} else {
				foreach($hosts as $host)
				{
					foreach($possbileSubdomains as $possbileSubdomain)
					{
						if (!empty($possbileSubdomain))
							$possbileSubdomain .= '.';
						
						$possibleUrlWithoutProtocol = $possbileSubdomain . $host . $pageUrlWithoutHost;
						
						$pagesUrls[] = 'https://' . $possibleUrlWithoutProtocol;
						
						if (Yii::app()->params['devMode'])
							$pagesUrls[] = 'http://' . $possibleUrlWithoutProtocol;
					}
				}
			}
		}
		return $pagesUrls;
	}
	
	//Проверка, что к врач доступен (что есть теорет. возможность записаться, но без проверки на наличие конкр. свободного времени)
	public function getIsAvaialble()
	{
		if ($this->deleted == 1)
			return false;
		if (!$this->placeOfWorks[0] OR !$this->placeOfWorks[0]->address->isActive)
			return false;
		if (!$this->company OR count($this->company->shortCompanyOfDoctor) != 1)
			return false;
		
		return true;
	}
	
	public function getClosestReceptionTime() {
		$result = false;
		$model = new WorkingHour();
		$secondsInOneDay = 24 * 60 * 60;
		$timestamp = time() + 0.5 * 60 * 60;
		for ($i = 0; $i < 7; $i++) {
			$timestamp += $i * $secondsInOneDay;
			$NOW = date("Y-m-d", $timestamp);
			$workingTime = $model->getDate($this->currentPlaceOfWork->address->link, $NOW, $this->link);
			if(is_array($workingTime) AND isset($workingTime["time"])) {
				foreach ($workingTime["time"] as $time) {
					if($time["status"] == "allow") {
						$result = strtotime($NOW . " " . $time["text"]);
						break 2;
					}
				}
			}
		}
		return $result;
	}

	public function newsReference() {
		return NewsReference::model()->findByReferenceType(NewsReference::TYPE_DOCTOR,['entityId'=>$this->id]);
	}
	
	public static function getIsLoyaltyProgramParticipantCriteria($criteria) {
		$criteria->with['doctorServices']['together'] = true;
		$criteria->with['placeOfWorks']['with']['address']['with']['userMedicals']['together'] = true;
		$criteria->compare('userMedicals.agreementLoyaltyProgram', 1);
		$criteria->addCondition('doctorServices.price > 0');
		return $criteria;
	}
	
	public function getIsLoyaltyProgramParticipant() {
		$criteria = self::getIsLoyaltyProgramParticipantCriteria(new CDbCriteria());
		$criteria->compare('t.id',$this->id);
		$doctor = Doctor::model()->find($criteria);
		return (is_object($doctor));
	}

	public function updateRating() {
		//Категория врача
		$markCategory = 0;
		foreach ($this->specialtyOfDoctors as $sp) {
			switch ($sp->doctorCategory->name) {
				case "Первая категория": $val = 4;
				break;
				case "Вторая категория": $val = 3;
				break;
				case "Высшая категория": $val = 5;
				break;
				case "Без категории": $val = 0;
				break;
			}
			if ($val > $markCategory) {
				$markCategory = $val;
			}
		}
		//Баллы за ученую степень
		$degree = $this->scientificDegrees[0]->name;
		#echo $degree;
		switch ($degree) {
			case "Доктор медицинских наук": $markDegree = 5;
			break;
			case "Кандидат медицинских наук": $markDegree = 3;
			break;
			default: $markDegree = 1;
			break;
		}
		
		//Баллы за ученое звание
		$title = $this->scientificTitle->name;
		switch ($title) {
			case "Профессор": $markTitle = 5;
			break;
			case "Доцент": $markTitle = 3;
			break;
			default: $markTitle = 1;
			break;
		}
		
		//Баллы за повышение квалификации
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'typeOfEducation'
		);
		$criteria->compare('t.doctorId', $this->id);
		$criteria->compare('typeOfEducation.name', 'Повышение квалификации');
		$edCount = DoctorEducation::model()->count($criteria);
		switch ($edCount) {
			case 0: $markCourse = 0;
			break;
			case 1: $markCourse = 1;
			break;
			case 2: $markCourse = 2;
			break;
			case 3: $markCourse = 3;
			break;
			case 4: $markCourse = 4;
			break;
			default : $markCourse = 5;
			break;
		}
		
		//Стаж
		$experience = $this->ExperienceNum * 1;
		if ($experience < 0) {
			$experience = 0;
		}
		$markExperience = $experience;
		if ($markExperience > 0) {
			$markExperience = 20;
		}
			
		//Получаем общее по автоматической части
		//$markInformSummary = (float) ($markExperience + $markCourse + $markTitle + $markDegree + $markCategory) / 5;
		$markInformSummary = 3.5 + ( ( $markExperience ) / 40 + ( $markTitle ) / 25 + ( $markCategory + $markDegree + $markCourse ) / 25 );
		//if (file_get_contents( 'http://emportal.ru/uploads/doctor/'.$doctor->link.'/photo.jpg' ))
		if ($this->hasPhoto)
			$markInformSummary += 0.5;
		if ($markInformSummary > 5) {
			$markInformSummary = 5;
		}
		
		/* 2.Получаем оценки пациентов */
		$reviews = Review::model()->findAllByAttributes([
				'doctorId' => $this->id,
				'statusId' => Review::STATUS_PUBLISHED,
		]);
		$i = 0;
		$sum = 0.0;
		foreach ($reviews as $review) {
			$sum += 5*$review->ratingDoctor;
			$i++;
		}
		$markUsersSummary = 0;
		if ($sum != 0 && $i != 0) {
			$markUsersSummary = number_format($sum/$i, 2);
		}
		
		/* =. Получаем общую оценку */
		if ($markUsersSummary > 0) {
			//формула (А), https://emp.bitrix24.ru/workgroups/group/1/tasks/task/view/2478/?MID=14476#com14476
			$doctorSummary = 0.6*$markInformSummary + 0.4*$markUsersSummary;
		} else {
			//формула (B), https://emp.bitrix24.ru/workgroups/group/1/tasks/task/view/2478/?MID=14476#com14476
			$doctorSummary = 0.95*$markInformSummary;
		}
		$doctorSummary = number_format($doctorSummary, 2);
		$this->rating = $doctorSummary;
		$this->ratingSystem = $markInformSummary;
		$this->ratingUsers = $markUsersSummary;
    	$this->syncThis = false;
    	$result = $this->save();
    	$this->syncThis = true;
		return $result;
	}

	public static function updateCountAppointment(){
		Yii::app()->db->createCommand()->update( '{{doctor}}', array (
			'countRecords' => 0,
		) );

		$command = Yii::app()->db->createCommand()
			->select("COUNT(atd.doctorId) as c, atd.doctorId")
			->from("appointmentToDoctors as atd")
			->where("atd.appType !='' AND atd.appType !='own' AND atd.statusId != 7")
			->group("atd.doctorId");

		$doctorsCountRec = $command->queryAll();
		foreach ($doctorsCountRec as $key => $val){
			if(empty($val['doctorId'])) continue;

			Yii::app()->db->createCommand()->update( '{{doctor}}', array (
				'countRecords' => $val['c'],
			), 'id="'.$val['doctorId'].'"' );
		}
	}
}
