<?php

/**
 * This is the model class for table "stateHealthAgencyServices".
 *
 * The followings are the available columns in table 'stateHealthAgencyServices':
 * @property string $id
 * @property string $stateHealthAgencyId
 * @property string $servicesGMUId
 *
 * The followings are the available model relations:
 * @property ServiceGMU $servicesGMU
 * @property StateHealthAgency $stateHealthAgency
 */
class StateHealthAgencyServices extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StateHealthAgencyServices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stateHealthAgencyServices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, stateHealthAgencyId, servicesGMUId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, stateHealthAgencyId, servicesGMUId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'servicesGMU' => array(self::BELONGS_TO, 'ServiceGMU', 'servicesGMUId'),
			'stateHealthAgency' => array(self::BELONGS_TO, 'StateHealthAgency', 'stateHealthAgencyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'stateHealthAgencyId' => 'State Health Agency',
			'servicesGMUId' => 'Services Gmuid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('stateHealthAgencyId',$this->stateHealthAgencyId,true);
		$criteria->compare('servicesGMUId',$this->servicesGMUId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}