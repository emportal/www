<?php

/**
 * This is the model class for table "diseaseOfOrgan".
 *
 * The followings are the available columns in table 'diseaseOfOrgan':
 * @property string $id
 * @property string $diseaseId
 * @property string $organId
 *
 * The followings are the available model relations:
 * @property Organ $organ
 * @property Disease $disease
 */
class DiseaseOfOrgan extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DiseaseOfOrgan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'diseaseOfOrgan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, diseaseId, organId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, diseaseId, organId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'organ' => array(self::BELONGS_TO, 'Organ', 'organId'),
			'disease' => array(self::BELONGS_TO, 'Disease', 'diseaseId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'diseaseId' => 'Disease',
			'organId' => 'Organ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('diseaseId',$this->diseaseId,true);
		$criteria->compare('organId',$this->organId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}