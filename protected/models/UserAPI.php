<?php

/**
 * This is the model class for table "userAPI".
 *
 * The followings are the available columns in table 'userAPI':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $accessKey
 * @property integer $isActive
 */
class UserAPI extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserAPI the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userAPI';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, accessKey', 'required'),
			array('isActive', 'numerical', 'integerOnly'=>true),
			array('name, accessKey', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, accessKey, isActive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::MANY_MANY, 'Address', 'userAPIAddresses(userAPIId, addressId)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'accessKey' => 'Access Key',
			'isActive' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('accessKey',$this->accessKey,true);
		$criteria->compare('isActive',$this->isActive);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//public function getRelatedAddresses(){
	//	$searchAttributes = ['userAPIId' => $this->id];
	//	$relations = UserAPIAddress::model()->findAllByAttributes($searchAttributes);
	//	$relatedAddresses = [];
	//	foreach($relations as $relation)
	//		$relatedAddresses[] = $relation->address;
	//	return $relatedAddresses;
	//}
	
	public function getAddressLinksAssociations()
	{
		$addressLinksAssociations = [];
		$addresses = $this->addresses;
		//var_dump($this->addresses);
		//exit();
		foreach($addresses as $address)
			$addressLinksAssociations[$address->link] = $address->id;
		return $addressLinksAssociations;
	}
}