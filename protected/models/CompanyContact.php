<?php

/**
 * This is the model class for table "companyContact".
 *
 * The followings are the available columns in table 'companyContact':
 * @property integer $id
 * @property string $companyId
 * @property string $companyName
 * @property string $addressId
 * @property string $addressName
 * @property string $managerId
 * @property integer $contactTypeId
 * @property string $createdTime
 * @property string $plannedTime
 * @property integer $resultTypeId
 * @property integer $comment
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Address $address
 * @property User $manager
 */
class CompanyContact extends CActiveRecord
{
	
	const RESULT_NOT_SET_YET = 0; //нет данных / контакт пока не состоялся
	const RESULT_LPR_FOUND = 1; //выявлено ЛПР (лицо-принимающее-решения)
	const RESULT_MEETING_ASSIGNED = 2; //назначена встреча для демонстрации презентационных материалов
	const RESULT_DEMO_EMAIL_SENT = 3; //демонстрация отправлена по емаил
	const RESULT_CALL_LATER = 4; //позвонить позже
	const RESULT_MEETING_FAIL = 5; //получен отказ после демонстрации "в живую"
	const RESULT_PHONE_FAIL = 6; //получен отказ по телефону
	const RESULT_MEETING_COMPLETED = 7; //произведена демонстрация "в живую"
	const RESULT_AGREEMENT_IS_BEING_PREPARED = 8; //договор передан на согласование
	const RESULT_AGREEMENT_SIGNED = 9; //договор подписан
	
	const TYPE_MEETING = 1; //договор подписан
	const TYPE_PHONE_CALL = 2; //договор подписан
	const TYPE_EMAIL = 3; //договор подписан
	
	//default values
	public $managerId = NULL;
	public $companyId = NULL;
	public $addressId = NULL;
	
	//dataProviderForNewAdminPanel values
	public $companyNameLike;
	public $result_status;
	public $time_period;
	public $task_status;
	
	public static $RESULT_TYPES = array(
		self::RESULT_NOT_SET_YET => 'нет данных',
		self::RESULT_LPR_FOUND => 'выявлено ЛПР',
		self::RESULT_MEETING_ASSIGNED => 'назначена встреча для демонстрации презентационных материалов',
		self::RESULT_DEMO_EMAIL_SENT => 'демонстрация отправлена по емаил',
		self::RESULT_CALL_LATER => 'позвонить позже',
		self::RESULT_MEETING_FAIL => 'получен отказ после демонстрации',
		self::RESULT_PHONE_FAIL => 'получен отказ по телефону',
		self::RESULT_MEETING_COMPLETED => 'произведена демонстрация',
		self::RESULT_AGREEMENT_IS_BEING_PREPARED => 'договор передан на согласование',
		self::RESULT_AGREEMENT_SIGNED => 'договор подписан',
	);
	
	public static $TYPES = array(
		self::TYPE_PHONE_CALL => 'Телефонный звонок',
		self::TYPE_MEETING => 'Встреча',
		self::TYPE_EMAIL => 'Email письмо',
	);
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanyContact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companyContact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('plannedTime, contactTypeId, resultTypeId', 'required'),
			array('contactTypeId, resultTypeId', 'numerical', 'integerOnly'=>true),
			array('companyId, addressId, managerId', 'length', 'max'=>36),
			array('id, companyId, companyName, addressId, addressName, managerId, contactTypeId, createdTime, plannedTime, resultTypeId, comment, personName, personPhone, authorId, taskMessage', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, companyId, companyName, addressId, addressName, managerId, contactTypeId, createdTime, plannedTime, resultTypeId, comment, authorId, taskMessage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'manager' => array(self::BELONGS_TO, 'User', 'managerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'companyId' => 'Клиника',
			'companyName' => 'Company Name',
			'addressId' => 'Адрес',
			'addressName' => 'Address Name',
			'managerId' => 'Ответственный',
			'contactTypeId' => 'Тип контакта',
			'createdTime' => 'Создан',
			'plannedTime' => 'Время проведения',
			'resultTypeId' => 'Результат',
			'comment' => 'Описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('companyName',$this->companyName,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('addressName',$this->addressName,true);
		$criteria->compare('managerId',$this->managerId,true);
		$criteria->compare('contactTypeId',$this->contactTypeId);
		$criteria->compare('createdTime',$this->createdTime,true);
		$criteria->compare('plannedTime',$this->plannedTime,true);
		$criteria->compare('resultTypeId',$this->resultTypeId);
		$criteria->compare('comment',$this->comment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function dataProviderForNewAdminPanel($options = [])
	{
		if (empty(Yii::app()->request->getParam('company_name')) AND !Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA'))
		{
			$this->managerId = Yii::app()->user->id; //показываем только клиники сотрудника
		}
		
		$criteria = new CDbCriteria;
		$criteria->with = [
			'company',
		];
		
		if (!empty($this->companyNameLike))
		{
			$criteria_c1 = new CDbCriteria;
			$criteria_c1->compare('company.name', $this->companyNameLike, true);
			
			$criteria_c2 = new CDbCriteria;
			$criteria_c2->compare('companyName', $this->companyNameLike, true);
			
			$criteria_c1->mergeWith($criteria_c2, 'OR');
			$criteria->mergeWith($criteria_c1, 'AND');
		}
		
		if (!empty($this->managerId))
		{
			if ($this->managerId != -1)
			{
				$criteria->compare('managerId', $this->managerId, true);
			} else {
				$criteria->compare('managerId', NULL, true);
			}
		}
		
		if (!empty($this->companyId))
		{
			$criteria->compare('company.id', $this->companyId, true);
		}
		
		if (!empty($this->time_period))
		{
			$lower_border = explode('-', $this->time_period)[0];
			$upper_border = explode('-', $this->time_period)[1];
			
			$lower_border = strtotime($lower_border);
			$upper_border = strtotime($upper_border);
			
			$lower_border = date("Y-m-d H:" . date('i') . ":s", $lower_border);
			$upper_border = date("Y-m-d H:" . date('i') . ":s", $upper_border);
			
			$criteria->addCondition("plannedTime >= '" . $lower_border . "'");
			$criteria->addCondition("plannedTime <= '" . $upper_border . "'");
		}
		
		if (!empty($this->task_status))
		{
			if ($this->task_status == 1)
			{
				$criteria->addCondition("taskMessage > ''");
			} else {
				//$criteria->addCondition("plannedTime <= '" . $upper_border . "'");
			}
		}
		
		$criteria->compare('resultTypeId', $this->resultTypeId, true);
		
		if (!empty($options['region']))
		{
			$criteria->addCondition("company.denormCitySubdomain = '" . $options['region'] . "'");
		}
		
		$sort = new CSort;
		$sort = [
			'defaultOrder' => 'plannedTime DESC',
			'attributes' => [
				'plannedTime',
				//'taskMessage'
			]
		];
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => [
				'pageSize' => 30
			],
			'sort' => $sort,
		));
	}
	
	
	public function getNewContactString()
	{
		$newContactString = '';
		$newContactParams = [];
		$newContactParams['companyId'] = $this->companyId;
		$newContactParams['companyName'] = Company::model()->findByPk($this->companyId)->name;	
		$newContactParams['contactTypeId'] = $this->contactTypeId;
		$newContactParams['personName'] = $this->personName;
		$newContactParams['personPhone'] = $this->personPhone;
		$newContactParams['managerId'] = $this->managerId;
		foreach($newContactParams as $key => $value) $newContactString .='&options[' . $key . ']=' . $value;
		
		return $newContactString;
	}
	
	
	
	
}