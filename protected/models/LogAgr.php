<?php

/**
 * This is the model class for table "agrLog".
 *
 * The followings are the available columns in table 'agrLog':
 * @property string $id
 * @property string $companyId
 * @property string $addressId
 * @property string $action
 * @property string $table
 * @property string $changes
 * @property string $datePublications
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property Company $company
 */
class LogAgr extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogAgr the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logAgr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id', 'required'),
			array('id, companyId, addressId', 'length', 'max'=>50),
			array('action, table', 'length', 'max'=>150),
			array('changes, datePublications', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, companyId, addressId, action, table, changes, datePublications', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'companyId' => 'Company',
			'addressId' => 'Address',
			'action' => 'Action',
			'table' => 'Table',
			'changes' => 'Changes',
			'datePublications' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('table',$this->table,true);
		$criteria->compare('changes',$this->changes,true);
		$criteria->compare('datePublications',$this->datePublications,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}