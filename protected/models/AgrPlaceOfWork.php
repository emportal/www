<?php

/**
 * This is the model class for table "agr_placeOfWork".
 *
 * The followings are the available columns in table 'agr_placeOfWork':
 * @property string $id
 * @property string $agr_doctorId
 * @property string $dateStart
 * @property string $companyId
 * @property string $positionId
 * @property integer $current
 * @property string $addressId
 */
class AgrPlaceOfWork extends AgrActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrPlaceOfWork the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agr_placeOfWork';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('current', 'numerical', 'integerOnly'=>true),
			array('id, doctorId, companyId, positionId, addressId', 'length', 'max'=>36),
			array('dateStart', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, doctorId, dateStart, companyId, positionId, current, addressId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
			'doctor' => array(self::BELONGS_TO, 'AgrDoctor', 'doctorId'),
			'position' => array(self::BELONGS_TO, 'Position', 'positionId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctorId' => 'Doctor',
			'dateStart' => 'Date Start',
			'companyId' => 'Company',
			'positionId' => 'Position',
			'current' => 'Current',
			'addressId' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('doctorId',$this->agr_doctorId,true);
		$criteria->compare('dateStart',$this->dateStart,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('positionId',$this->positionId,true);
		$criteria->compare('current',$this->current);
		$criteria->compare('addressId',$this->addressId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function scopes() {
		return array(
			'current'=>array(
				'condition'=>'current=1'
			),
		);
	}
}