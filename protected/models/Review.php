<?php

/**
 * This is the model class for table "review".
*
* The followings are the available columns in table 'review':
* @property integer $id
* @property string $companyId
* @property string $addressId
* @property string $appointmentToDoctorsId
* @property string $doctorId
* @property string $userId
* @property string $userName
* @property string $userPhone
* @property string $reviewText
* @property string $answerText
* @property string $createDate
* @property integer $statusId
* @property double $ratingDoctor
* @property double $ratingClinic
* @property string $confirmCode
*
* The followings are the available model relations:
* @property Address $address
* @property AppointmentToDoctors $appointmentToDoctors
* @property Company $company
* @property Doctor $doctor
* @property User $user
*/
class Review extends CActiveRecord
{
	const STATUS_NOT_PUBLISHED_PHONE_NOT_APPROVED = '0';
	const STATUS_NOT_PUBLISHED_PHONE_APPROVED = '1';
	const STATUS_PUBLISHED = '2';
	const STATUS_NOT_PUBLISHED_REVIEW_DECLINED = '3';
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Review the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		        array('createDate','default',
		              'value'=>new CDbExpression('NOW()'),
		              'setOnEmpty'=>false,'on'=>'insert'),
				array('companyId, addressId, userName', 'required'),
				array('statusId', 'numerical', 'integerOnly'=>true),
				array('ratingDoctor, ratingClinic', 'numerical'),
				array('companyId, addressId, appointmentToDoctorsId, doctorId, userId', 'length', 'max'=>36),
				array('userPhone', 'length', 'max'=>150),
				array('confirmCode', 'length', 'max'=>12),
				array('reviewText, answerText, createDate', 'safe'),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('id, companyId, addressId, appointmentToDoctorsId, doctorId, userId, userName, userPhone, reviewText, answerText, createDate, statusId, ratingDoctor, ratingClinic, confirmCode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
				'appointmentToDoctors' => array(self::BELONGS_TO, 'AppointmentToDoctors', 'appointmentToDoctorsId'),
				'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorId'),
				'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
				'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'companyId' => 'Company',
				'addressId' => 'Address',
				'appointmentToDoctorsId' => 'Appointment To Doctors',
				'doctorId' => 'Doctor',
				'userId' => 'User',
				'userName' => 'User Name',
				'userPhone' => 'User Phone',
				'reviewText' => 'Review Text',
				'answerText' => 'Answer Text',
				'createDate' => 'Create Date',
				'statusId' => 'Status',
				'ratingDoctor' => 'Rating Doctor',
				'ratingClinic' => 'Rating Clinic',
				'confirmCode' => 'Confirm Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('addressId',$this->addressId,true);
		$criteria->compare('appointmentToDoctorsId',$this->appointmentToDoctorsId,true);
		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('userName',$this->userName,true);
		$criteria->compare('userPhone',$this->userPhone,true);
		$criteria->compare('reviewText',$this->reviewText,true);
		$criteria->compare('answerText',$this->answerText,true);
		$criteria->compare('createDate',$this->createDate,true);
		$criteria->compare('statusId',$this->statusId);
		$criteria->compare('ratingDoctor',$this->ratingDoctor);
		$criteria->compare('ratingClinic',$this->ratingClinic);
		$criteria->compare('confirmCode',$this->confirmCode,true);

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}
	
	public function beforeSave() {
		parent::beforeSave();
		if(empty($this->appointmentToDoctorsId)) {
			$this->appointmentToDoctorsId = NULL;
		}
		if(empty($this->doctorId)) {
			$this->doctorId = NULL;
		}
		if(empty($this->userId)) {
			$this->userId = NULL;
		}
		if(empty($this->userId)) {
			$this->userId = NULL;
		}
		if(empty($this->addressId)) {
			$this->addressId = NULL;
		}
		if(empty($this->companyId)) {
			$this->companyId = NULL;
		}
		return true;
	}
	
	public function getUserAvatarSrc() {
		return ($user = $this->user) ? $user->avatarSrc : '/images/newLayouts/icon_user_default.jpg';
	}
}