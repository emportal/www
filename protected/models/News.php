<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $title
 * @property string $datePublications
 * @property string $Text
 * @property string $preview
 * @property integer $publish
 * @property string $tagDescription
 * @property string $lastReferenceUpdate
 * @property integer $views
 * @property integer $hosts
 *
 * @property NewsTags[] $tags
 * 
 * @method News published() Опубликованные новости
 */
class News extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title, Text, preview, typeNewsId,datePublications', 'required'),
			array('publish, views, hosts', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>36),
			array('name, link, tagDescription', 'length', 'max'=>150),
			array('datePublications, title, Text, preview, Date, authorId', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, title, datePublications, Text, preview, publish, tagDescription, doctorListTitle, clinicListTitle, shareButtonsTitle', 'safe', 'on'=>'search'),
			array('doctorListTitle, doctorListQuery, clinicListTitle, clinicListQuery, shareButtonsTitle, allTagsString', 'safe'),
			array('mainImage', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clinic'	=> array(self::BELONGS_TO, 'Address', 'clinicId'),
			'type'		=> array(self::BELONGS_TO, 'TypeNews', 'typeNewsId'),
			'tags'		=> array(self::HAS_MANY, 'NewsTags', 'newsId'),
			'author'	=> array(self::BELONGS_TO, 'User', 'authorId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'link' => 'Ссылка',
			'title' => 'Заголовок',
			'datePublications' => 'Дата публикации',
			'myDate'=> 'Дата публикации',
			'Date' => 'Дата создания',
			'Text' => 'Текст',
			'preview' => 'Анонс',
			'typeNewsId' => 'Категория',
			'publish' => 'Опубликовано',
			'tagDescription' => 'Тэг description',
			'doctorListTitle' => 'Заголовок к списку врачей',
			'doctorListQuery' => 'Ссылка на поисковую выдачу врачей',
			'clinicListTitle' => 'Заголовок к списку клиник',
			'clinicListQuery' => 'Ссылка на поисковую выдачу клиник',
			'shareButtonsTitle' => 'Заголовок к кнопкам "поделиться"',
			'allTagsString' => 'Теги (через пробел)',
			'fileField' => 'Изображение',
			'mainImage' => 'Превью-картинка',
			'authorId' => 'Автор',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('datePublications',$this->datePublications,true);		
		$criteria->compare('Date',$this->Date,true);
		$criteria->compare('Text',$this->Text,true);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('publish',$this->publish);
		$criteria->compare('tagDescription',$this->tagDescription);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function scopes() {
		return [
			'published' => 	[
				'condition' => ' publish = 1 AND datePublications < NOW()'
			],
			'recently'=>[
				'order'=>'datePublications DESC',
				'limit'=>4,
			],
			'random'=>[
				'order' => 'RAND()',
				'limit'=>1,
			],
			'clinic' => [
				'condition' => "t.typeNewsId = '9030997956c6a2c74f89a39683a30d'"
			]
		];
	}
	
	public function getMyDate() {
		return date('d.m.Y',strtotime($this->datePublications));
	}
	
	public function getTypeNews() {
		return CHtml::listData(TypeNews::model()->findAll(),'id','name');		
	}

	public function getAutorList($users) {
		return array_merge([''=>''],CHtml::listData($users,'id','name'));		
	}
	
	public function getEditedText() {
		$arr=explode("\n", $this->Text);
		foreach($arr as $a) {
			$a = "<p>".$a."</p>";
			$text.=$a;
		}
		return $text;
	}
	
	public function getFormattedText() {
		$output_text = str_replace('<a', '<a rel="nofollow"', $this->Text);
		return $output_text;
	}
	
	public function getNewsReference($newsReferenceType=null) {
		$criteria = new CDbCriteria();
		if($newsReferenceType !== null) {
			$criteria->compare('type',$newsReferenceType);
		}
		$criteria->compare('newsId',$this->id);
		NewsReference::model()->findAll($criteria);
	}
	public function addNewsReference($entitys,$newsReferenceType) {
		if(!is_array($entitys)) { $entitys = [$entitys]; }
		foreach ($entitys as $entityId) {
			$newsReference = new NewsReference();
			$newsReference->newsId = $this->id;
			$newsReference->entityId = $entityId;
			$newsReference->type = $newsReferenceType;
			$newsReference->save();
		}
	}
	public function removeNewsReference($newsReferenceType=null) {
		$criteria = new CDbCriteria();
		if($newsReferenceType !== null) {
			$criteria->compare('type',$newsReferenceType);
		}
		$criteria->compare('newsId',$this->id);
		NewsReference::model()->deleteAll($criteria);
	}
	
	public function getMainImage() {
		if(!empty($this->mainImage)) {
			return $this->mainImage;
		}
		try { // Добавляет метатег для репоста на фейсбук и ВК
			$pattern = "/<img.+?src=[\"'](.+?)[\"'].*?>/";
			if(preg_match ($pattern, $this->Text, $imgsrc)) {
				return $imgsrc[1];
			}
		} catch (Exception $e) { }
		return null; 
	}
	
	/*
	public function getTitleText() {
		$text=$this->title;
		$arr=explode(" ",$text);
		for($i=0;$i<count($arr);$i++) {
			if(mb_strlen($mystr.$arr[$i])<40)
				$mystr.=$arr[$i]." ";
		}		
		$mystr=trim($mystr," .");
		return $mystr."...";
	}
	
	public function getPreviewText() {
		$text=$this->preview;
		$arr=explode(" ",$text);
		for($i=0;$i<count($arr);$i++) {
			if(mb_strlen($mystr.$arr[$i])<200)
				$mystr.=$arr[$i]." ";
		}		
		$mystr=trim($mystr," .");
		return $mystr."...";
	}*/
	
	public static function getLastNews($limit = 0, $offset=0, $excludeId=0){
		$filter = array(
			'limit' => $limit,
			'offset' => $offset,
			'order' => 'Date DESC'
		);
		if($excludeId) {
			$filter['condition']= " id != '{$excludeId}' ";
		}
		return News::model()->published()->findAll($filter);
	}

	public function getSimilarNews($excludeId = []) {
		$excludeId = array_merge($excludeId,[$this->id]);
		$excludeId = array_map(function ($a) { return "'".$a."'";}, $excludeId);

		foreach ($this->tags as $tag) {
			$tags[] = "'".$tag->name."'";
		}

		if(!count($tags)) {
			return [];
		}

		$criteria = new CDbCriteria();
		$criteria->with = array(
			'tags' => [
				'together' => true,
			]
		);
		$criteria->together = true;
		$criteria->condition = "tags.name in (".implode(',',$tags).") AND tags.newsId NOT IN (".implode(',',$excludeId).") AND t.datePublications <= NOW() AND t.publish = 1";
		$criteria->group = 'tags.newsId';
		$criteria->order = 'COUNT(tags.newsId) DESC, t.datePublications DESC';
		$criteria->limit = 4;
		$news = News::model()->findAll($criteria);
		return $news;
	}

	public function getBeforeNews() {
		$criteria = new CDbCriteria();
		$criteria->limit = 1;
		$criteria->order = 'datePublications ASC';
		$criteria->compare("datePublications", ">$this->datePublications");
		$criteria->compare("datePublications", "<=NOW()");
		$criteria->compare("publish", 1);
		$news = News::model()->findAll($criteria);
		return $news[0];
	}

	public function getAfterNews() {
		$criteria = new CDbCriteria();
		$criteria->limit = 1;
		$criteria->order = 'datePublications DESC';
		$criteria->compare("datePublications", "<$this->datePublications");
		$criteria->compare("publish", 1);
		$news = News::model()->findAll($criteria);
		return $news[0];
	}

	public function beforeSave() {
		if (parent::beforeSave()) {
			return true;
		}
	}
	public function afterSave() {
		if (parent::afterSave()) {
			if(is_string($this->tagsString)) {
				$tagsArray = explode(' ', $this->tagsString);
				foreach ($tagsArray as $key=>$name) {
					$tagsArray[$key] = trim($name);
					if(empty($tagsArray[$key])) {
						unset($tagsArray[$key]);
					}
				}
				$tags = NewsTags::model()->findAll('newsId=:newsId',[':newsId' => $this->id]);
				foreach ($tags as $tag) {
					if(!in_array($tag->name, $tagsArray)) {
						$tag->delete();
					} else {
						if(($key = array_search($tag->name, $tagsArray)) !== false) {
						    unset($tagsArray[$key]);
						}
					}
				}
				foreach ($tagsArray as $key=>$name) {
					if(is_string($name)) {
						$tag = new NewsTags;
						$tag->newsId = $this->id;
						$tag->name = $name;
						$tag->save();
						unset($tag);
					}
				}
			}
			return true;
		}
	}
	
	public $tagsString = null;
	public function getAllTagsString() {
		if(!is_string($this->tagsString)) {
			$tagsArray = [];
			foreach ($this->tags as $tag) {
				$tagsArray[] = $tag->name;
			}
			$this->tagsString = implode(' ', $tagsArray);
		}
		return $this->tagsString;
	}
	public function setAllTagsString($string) {
		$this->tagsString = $string;
	}
	
	public function getUrl($absolute=false,$blogDomen=false) {
		$url = "/".$this->link.".html";
		if(!$blogDomen) $url = "/news" . $url;
		if($absolute) $url = ((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . "://" . ($blogDomen ? "blog.emportal.ru" : $_SERVER['SERVER_NAME']) . $url;
		return $url;
	}

	public function beforeDelete() {
		NewsTags::model()->deleteAllByAttributes(['newsId' => $this->id]);
		return parent::beforeDelete();
	}
	
	public function getPopularNews($num = 3, $idNew = NULL){
		$filter = array(
			'condition' => 'id <>\''.$idNew.'\'',
			'limit' => $num,
			'order' => 'views DESC'
		);
		return News::model()->findAll($filter);
	}
}