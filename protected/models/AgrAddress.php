<?php

/**
 * This is the model class for table "agr_address".
 *
 * The followings are the available columns in table 'agr_address':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $ownerId
 * @property string $countryId
 * @property string $postIndex
 * @property string $regionId
 * @property string $cityId
 * @property string $cityDistrictId
 * @property string $street
 * @property string $houseNumber
 * @property double $longitude
 * @property double $latitude
 * @property string $description
 * @property integer $lunchBreak
 * @property string $breakStart
 * @property string $breakFinish
 * @property integer $forCompany
 * @property integer $forProducers
 * @property integer $notTheExactCoordinates
 * @property string $notTheExactCoordinatesPo
 * @property integer $priority
 */
class AgrAddress extends AgrActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agr_address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, link', 'required'),
			array('lunchBreak, forCompany, forProducers, notTheExactCoordinates, priority', 'numerical', 'integerOnly'=>true),
			array('longitude, latitude', 'numerical'),
			array('id, countryId, regionId, cityId, cityDistrictId', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			array('postIndex, houseNumber', 'length', 'max'=>20),
			array('street', 'length', 'max'=>50),
			array('notTheExactCoordinatesPo', 'length', 'max'=>200),
			array('ownerId, description, breakStart, breakFinish', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, ownerId, countryId, postIndex, regionId, cityId, cityDistrictId, street, houseNumber, longitude, latitude, description, lunchBreak, breakStart, breakFinish, forCompany, forProducers, notTheExactCoordinates, notTheExactCoordinatesPo, priority', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'ownerId' => 'Owner',
			'countryId' => 'Country',
			'postIndex' => 'Post Index',
			'regionId' => 'Region',
			'cityId' => 'City',
			'cityDistrictId' => 'City District',
			'street' => 'Street',
			'houseNumber' => 'House Number',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
			'description' => 'Description',
			'lunchBreak' => 'Lunch Break',
			'breakStart' => 'Break Start',
			'breakFinish' => 'Break Finish',
			'forCompany' => 'For Company',
			'forProducers' => 'For Producers',
			'notTheExactCoordinates' => 'Not The Exact Coordinates',
			'notTheExactCoordinatesPo' => 'Not The Exact Coordinates Po',
			'priority' => 'Priority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('ownerId',$this->ownerId,true);
		$criteria->compare('countryId',$this->countryId,true);
		$criteria->compare('postIndex',$this->postIndex,true);
		$criteria->compare('regionId',$this->regionId,true);
		$criteria->compare('cityId',$this->cityId,true);
		$criteria->compare('cityDistrictId',$this->cityDistrictId,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('houseNumber',$this->houseNumber,true);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('lunchBreak',$this->lunchBreak);
		$criteria->compare('breakStart',$this->breakStart,true);
		$criteria->compare('breakFinish',$this->breakFinish,true);
		$criteria->compare('forCompany',$this->forCompany);
		$criteria->compare('forProducers',$this->forProducers);
		$criteria->compare('notTheExactCoordinates',$this->notTheExactCoordinates);
		$criteria->compare('notTheExactCoordinatesPo',$this->notTheExactCoordinatesPo,true);
		$criteria->compare('priority',$this->priority);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}