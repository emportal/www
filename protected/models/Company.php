<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $linkUrl
 * @property string $nameRUS
 * @property string $nameENG
 * @property string $description
 * @property string $categoryId
 * @property string $companyTypeId
 * @property integer $isPaid
 * @property integer $isFree
 * @property integer $IsOMS
 * @property integer $IsDMS
 * @property integer $doctorsOnHouse
 * @property integer $dateOpening
 * @property string $contractsId disabled
 * @property string $employeeId
 * @property integer $averageCheck
 * @property string $addressesId
 * @property string $contactPersonCompanyId
 * @property string $INN
 * @property string $KPP
 * @property string $OGRN
 * @property string $OKPO
 * @property integer $rating
 * @property string $legalEntitiesId
 * @property string $logo
 * @property string $logoImg
 * @property integer $removalFlag
 * @property integer $reviewsCount
 *
 * The followings are the available model relations:
 * @property Action[] $actions
 * @property Address[] $addresses
 * @property Comment[] $comments
 * @property ContactPersonCompany $contactPersonCompany
 * @property Address $address
 * @property Category $category
 * @property CompanyContract $contracts
 * @property CompanyType $companyType
 * @property Employee $employee
 * @property CompanyActivites[] $companyActivites
 * @property CompanyContract[] $companyContracts
 * @property CompanyEquipment[] $companyEquipments
 * @property CompanyLicense[] $companyLicenses
 * @property CompanyNomenclatureGroup[] $companyNomenclatureGroups
 * @property CompanyServices[] $companyServices
 * @property CompanySoftware[] $companySoftwares
 * @property CompanyUnit[] $companyUnits
 * @property ContactInformation[] $contactInformations
 * @property ContactPersonCompany[] $contactPersonCompanies
 * @property Doctor[] $doctors
 * @property Email[] $emails
 * @property Phone[] $phones
 * @property PlaceOfWork[] $placeOfWorks
 * @property PriceNomenclature[] $priceNomenclatures
 * @property Ref[] $refs
 * @property Software[] $softwares
 * @property User1C[] $user1Cs
 * @property UserDrugstore[] $userDrugstores
 * @property UserInsurance[] $userInsurances
 * @property UserMedical[] $userMedicals
 * @property UserPatronage[] $userPatronages
 * @property UserShops[] $userShops
 * @property UserSuppliers[] $userSuppliers
 * @property UserTourism[] $userTourisms
 * @property WorkingHoursOfDoctors[] $workingHoursOfDoctors
 * @property LegalEntities $legalEntities
 * @property Reviews[] $reviews
 * @property Rewiews $publishedReviewsCount
 */
class Company extends ActiveRecord {

	public static $showRemoved = false;

	private $_logo;	
	public $oneOffer;	//$this->pCityId
	public $pCityId;
	public $LastCreateDate;
	public $managerId;
	public $nameLike;
	public $companyTypeArtificial;
	
	public $filterSamozapis;
	
	//default values
	public $mainLegalEntityId = NULL;
	
	public static $CompanyTypesNotOnline = array(
		'c2148457-29d6-11e2-b014-e840f2aca94f', // Выездная медицинская помощь
		'd16b98ad-349a-11e2-b014-e840f2aca94f', // Санаторий
		'6e0c0de5-f06f-11e2-856f-e840f2aca94f', // Диспансер
		'71d418b8-6072-11e2-b019-e840f2aca94f', // Дом престарелых, интернат
		'a950904d-86fc-11e2-856f-e840f2aca94f'  // Тур. фирмы и тур. операторы с оздоровительными турами
	);
	
	public function behaviors() {
		return [
			'linkUrlBehavior' => array(
				'class' => 'application.components.behaviors.LinkUrlBehavior',				
			)
		];
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isPaid, isFree, IsOMS, IsDMS, doctorsOnHouse, dateOpening, averageCheck, mainLegalEntityId, reviewsCount', 'numerical', 'integerOnly' => true),
			array('id, categoryId, companyTypeId, addressId', 'length', 'max' => 36),
			array('link, INN, KPP, OGRN, OKPO', 'length', 'max' => 150),
			array('name, nameRUS, nameENG', 'length', 'max' => 200),
			['LastCreateDate, mainLegalEntityId, denormCitySubdomain', 'safe'],
			//array('description', 'safe'),
			array('description','filter','filter'=>function($v){ return strip_tags($v);}),
			array('description','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
				/*
			array(
					'dateOpening', 'numerical',
				    'integerOnly'=>true,
				    'min'=>1700,
				    'max'=>date("Y"),
				    'tooSmall'=>'Значение слишком мало',
				    'tooBig'=>'Значение слишком велико'),*/
			array(
				'logo',
				'file',
				'types'		 => 'jpg,jpeg, gif, png',
				'maxSize'	 => 1024 * 1024 * 5, // 5MB
				'allowEmpty' => 'true',
				'tooLarge'	 => 'Файл более 5MБ. Пожалуйста, загрузите файл меньшего размера.',
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, nameRUS, nameENG, description, categoryId, companyTypeId, isPaid, isFree, IsOMS, IsDMS, doctorsOnHouse, dateOpening, employeeId, averageCheck, addressesId, INN, KPP, OGRN, OKPO, rating, removalFlag, denormCitySubdomain', 'safe', 'on' => 'search'),
			['name, categoryId', 'required', 'on' => 'self_register'],
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'appointmentCount'			=> array(self::STAT, 'address', 'appointmentToDoctors(companyId,addressId)','condition' => '`appointmentToDoctors`.`statusId` = '.AppointmentToDoctors::VISITED),
			'attribute'				    =>	array(self::HAS_MANY, 'Attribute','companyId'),
			'actions'					=> array(self::HAS_MANY, 'Action', 'companyId'),
			'actions_active'			=> array(self::HAS_MANY, 'Action', 'companyId','scopes'=>array('published','active')),
			'addresses'					=> array(self::HAS_MANY, 'Address', 'ownerId'),					
			'contactPersonCompany'		=> array(self::HAS_MANY, 'ContactPersonCompany', 'companyId'),
			'address'					=> array(self::BELONGS_TO, 'Address', 'addressId'),
			'category'					=> array(self::BELONGS_TO, 'Category', 'categoryId'),
			'contracts'					=> array(self::HAS_MANY, 'CompanyContract', 'companyId'),
			'companyType'				=> array(self::BELONGS_TO, 'CompanyType', 'companyTypeId'),
			//'employee'				 => array(self::BELONGS_TO, 'Employee', 'employeeId'),
			'companyActivites'			=> array(self::MANY_MANY, 'CompanyActivite', 'companyActivites(addressId,companyActivitesId)', 'order'=>'name'),
			'companyContracts'			=> array(self::HAS_ONE, 'CompanyContract', 'companyId'),
			'companyEquipments'			=> array(self::HAS_MANY, 'CompanyEquipment', 'companyId'),
			'companyLicenses'			=> array(self::HAS_MANY, 'CompanyLicense', 'companyId'),
			'companyNomenclatureGroups'	=> array(self::HAS_MANY, 'CompanyNomenclatureGroup', 'companyId'),
			'companySalesInfo'			=> array(self::HAS_ONE, 'CompanySalesInfo', 'companyId'),
			'companyServices'			=> array(self::HAS_MANY, 'CompanyServices', 'companyId'),
			'companySoftwares'			=> array(self::HAS_MANY, 'CompanySoftware', 'companyId'),
			'companyUnits'				=> array(self::HAS_MANY, 'CompanyUnit', 'companyId'),
			'contactInformations'		=> array(self::HAS_MANY, 'ContactInformation', 'companyId'),
			'contactPersonCompanies'	=> array(self::HAS_MANY, 'ContactPersonCompany', 'companyId'),
			'doctors'					=> array(self::HAS_MANY, 'Doctor', 'currentPlaceOfWorkId'),
			'emails'					=> array(self::HAS_MANY, 'Email', 'ownerId'),
			'phones'					=> array(self::HAS_MANY, 'Phone', 'ownerId'),
			'placeOfWorks'				=> array(self::HAS_MANY, 'PlaceOfWork', 'companyId'),
			'priceNomenclatures'		=> array(self::HAS_MANY, 'PriceNomenclature', 'companyId'),
			'refs'						=> array(self::HAS_MANY, 'Ref', 'ownerId'),
			'softwares'					=> array(self::HAS_MANY, 'Software', 'companyId'),
			'user1Cs'					=> array(self::HAS_MANY, 'User1C', 'companyId'),
			'userMedicals'				=> array(self::HAS_MANY, 'UserMedical', 'companyId'),
			'userPatronages'			=> array(self::HAS_MANY, 'UserPatronage', 'companyId'),
			'userShops'					=> array(self::HAS_MANY, 'UserShops', 'companyId'),
			'userSuppliers'				=> array(self::HAS_MANY, 'UserSuppliers', 'companyId'),
			'userTourisms'				=> array(self::HAS_MANY, 'UserTourism', 'companyId'),
			'workingHoursOfDoctors'		=> array(self::HAS_MANY, 'WorkingHoursOfDoctors', 'companyId'),
			'legalEntities'				=> array(self::BELONGS_TO, 'LegalEntities', 'legalEntitiesId'),
			'agr_companyCount'			=> array(self::STAT,'AgrCompany','id'),
			'managerRelations'			=> array(self::HAS_ONE,'ManagerRelation','companyId'),
			'companyContacts'			=> array(self::HAS_MANY,'CompanyContact','companyId'),
			'shortCompanyOfDoctor'		=> array(self::HAS_ONE,'ShortCompaniesOfDoctor','companyId'),
            'extCompany' 				=> array(self::HAS_ONE, 'ExtCompany', 'companyId'),
            'reviews'      				=> array(self::HAS_MANY, 'Review', 'companyId'),
            'publishedReviewsCount' => array(self::STAT, 'Review', 'companyId', 'condition' => 't.statusId=2'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id'					 => 'ID',
			'name'					 => 'Название компании',
			'link'					 => 'Link',
			'nameRUS'				 => 'Name Rus',
			'nameENG'				 => 'Name Eng',
			'description'			 => 'Описание',
			'categoryId'			 => 'Category',
			'companyTypeId'			 => 'Company Type',
			'isPaid'				 => 'Is Paid',
			'isFree'				 => 'Is Free',
			'IsOMS'					 => 'Is Oms',
			'IsDMS'					 => 'Is Dms',
			'doctorsOnHouse'		 => 'Doctors On House',
			'dateOpening'			 => 'Год основания',
			//'contractsId'			 => 'Contracts',
			'employeeId'			 => 'Employee',
			'averageCheck'			 => 'Average Check',
			'addressesId'			 => 'Address',
			//'contactPersonCompanyId' => 'Contact Person Company',
			'INN'					 => 'Inn',
			'KPP'					 => 'Kpp',
			'OGRN'					 => 'Ogrn',
			'OKPO'					 => 'Okpo',
			//'rating'				 => 'Rating',
			'removalFlag'			 => 'removalFlag',
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('nameRUS', $this->nameRUS, true);
		$criteria->compare('nameENG', $this->nameENG, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('categoryId', $this->categoryId, true);
		$criteria->compare('companyTypeId', $this->companyTypeId, true);
		$criteria->compare('isPaid', $this->isPaid);
		$criteria->compare('isFree', $this->isFree);
		$criteria->compare('IsOMS', $this->IsOMS);
		$criteria->compare('IsDMS', $this->IsDMS);
		$criteria->compare('doctorsOnHouse', $this->doctorsOnHouse);
		$criteria->compare('dateOpening', $this->dateOpening);
		//$criteria->compare('contractsId', $this->contractsId, true);
		#$criteria->compare('employeeId', $this->employeeId, true);
		$criteria->compare('averageCheck', $this->averageCheck);
		#$criteria->compare('addressesId', $this->addressesId, true);
		//$criteria->compare('contactPersonCompanyId', $this->contactPersonCompanyId, true);
		$criteria->compare('INN', $this->INN, true);
		$criteria->compare('KPP', $this->KPP, true);
		$criteria->compare('OGRN', $this->OGRN, true);
		$criteria->compare('OKPO', $this->OKPO, true);
		//$criteria->compare('rating', $this->rating);
		$criteria->compare('removalFlag', $this->removalFlag, true);
		$criteria->compare('reviewsCount', $this->reviewsCount, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,			
		));
	}
	
	public function companyDataProviderForAdminPanel() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$this->oneOffer = Yii::app()->request->getParam(get_class($this))['oneOffer'];
		$this->filterSamozapis = Yii::app()->request->getParam(get_class($this))['filterSamozapis'];
		
		$criteria = new CDbCriteria;
		$criteria->with = [
			'companyType',
			'companyContracts' => [
				'together' => true,
				'select' => '(`companyContracts`.`id` IS NOT NULL) AS contract,contractNumber,name'
			],		
			'userMedicals' => [
				'together' => true,	
			],
			/*'managerRelations' => [
				'together' => true,
				'select' => '(`managerRelations`.`userId` != 1) AS id,userId'
			],*/
			'shortCompanyOfDoctor' => [
				'together' => true,
			],
			'category'
		];		
		if($this->oneOffer > 0) {
			/*$criteria->with = array_merge($criteria->with,['userMedicals' => [
				'together' => true,				
			]]);*/
		}
		if($this->oneOffer == 1) {
			#$criteria->compare('userMedicals.agreementNew',1);
			$criteria->addCondition('userMedicals.companyId IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MIN(agreementNew) = 1)');
		} elseif($this->oneOffer == 2) {
			$criteria->addCondition('userMedicals.companyId IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MAX(agreementNew) = 0)');
			#$criteria->compare('userMedicals.agreementNew',0);
		}
		
		if ($this->filterSamozapis == 'samozapis')
		{
			$cCondition = 'shortCompanyOfDoctor.samozapis = 1';
			$criteria->addCondition($cCondition);
		}
		if ($this->filterSamozapis == 'no-samozapis')
		{
			$cCondition = 'shortCompanyOfDoctor.samozapis = 0';
			$criteria->addCondition($cCondition);
		}
		
		if (!empty(Yii::app()->session['selectedRegion']))
		{
			$criteria->compare('t.denormCitySubdomain', Yii::app()->session['selectedRegion'], false);
		}
		
		$criteria->group = 't.id';		
		$criteria->select = ['*',"IF(MIN(userMedicals.agreementNew) > 0,MAX(userMedicals.createDate),'') as LastCreateDate"];		
		
		if(!isset($_REQUEST[get_class($this).'_sort'])) {
			$criteria->order = 'Contract DESC, t.name ASC';
		} else {
			if($_REQUEST[get_class($this).'_sort'] == 'LastCreateDate' || $_REQUEST[get_class($this).'_sort'] == 'LastCreateDate.desc') {
				
			}
			elseif($_REQUEST[get_class($this).'_sort'] == 'reviewsCount' || $_REQUEST[get_class($this).'_sort'] == 'reviewsCount.desc') {
				
			} else {
				$criteria->order = 'Contract DESC, t.name ASC';
			}
		}
		$criteria->compare('id', $this->id, true);
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('LastCreateDate', $this->LastCreateDate, true);
		$criteria->compare('nameRUS', $this->nameRUS, true);
		$criteria->compare('nameENG', $this->nameENG, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('categoryId', $this->categoryId, true);
		$criteria->compare('companyTypeId', $this->companyTypeId, true);
		$criteria->compare('isPaid', $this->isPaid);
		$criteria->compare('isFree', $this->isFree);
		$criteria->compare('IsOMS', $this->IsOMS);
		$criteria->compare('IsDMS', $this->IsDMS);
		$criteria->compare('doctorsOnHouse', $this->doctorsOnHouse);
		$criteria->compare('dateOpening', $this->dateOpening);
		//$criteria->compare('contractsId', $this->contractsId, true);
		#$criteria->compare('employeeId', $this->employeeId, true);
		$criteria->compare('averageCheck', $this->averageCheck);
		#$criteria->compare('addressesId', $this->addressesId, true);
		//$criteria->compare('contactPersonCompanyId', $this->contactPersonCompanyId, true);
		$criteria->compare('INN', $this->INN, true);
		$criteria->compare('KPP', $this->KPP, true);
		$criteria->compare('OGRN', $this->OGRN, true);
		$criteria->compare('OKPO', $this->OKPO, true);
		//$criteria->compare('rating', $this->rating);
		$criteria->compare('removalFlag', $this->removalFlag, true);
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50,
			),			
			'sort' => [	
				'attributes' => [
					'LastCreateDate' => [
						'ASC' => 'LastCreateDate ASC',
						'DESC' => 'LastCreateDate DESC',
					],
					'reviewsCount' => [
						'ASC' => 'reviewsCount ASC',
						'DESC' => 'reviewsCount DESC',
					]
				]				
			]
		));
	}
	

	public function beforeFind() {
		parent::beforeFind();
	
		if(Company::$showRemoved === false) {
			$criteria = $this->dbCriteria;
			$criteria->addCondition($this->tableAlias . ".removalFlag != '1'");
			$this->dbCriteria = $criteria;
		}
	}

	
	
	public function companyDataProviderForNewAdminPanel($options = null)
    {
		$this->oneOffer = Yii::app()->request->getParam(get_class($this))['oneOffer'];
		
		$criteria = new CDbCriteria;
		$criteria->with = [
			'companyType',
			'companyContracts' => [
				'together' => true,
				'select' => '(`companyContracts`.`id` IS NOT NULL) AS contract,contractNumber,name'
			],
			'userMedicals' => [
				'together' => true,
			],
			'addresses' => [
				'together' => true,
			],
			'managerRelations' => [
				'together' => true,
				'select' => '(`managerRelations`.`userId` != 1) AS id,userId'
			],
			'category',
			'companySalesInfo' => [
				'together' => true,
			],
			'shortCompanyOfDoctor' => [
				'together' => true,
			]
		];
		$criteria->group = 't.id';
		
		if (empty($this->filterSamozapis))
			$this->filterSamozapis = Yii::app()->request->getParam(get_class($this))['filterSamozapis'];
		if ($options['oneOffer'])
			$this->oneOffer = $options['oneOffer'];
		if (!Yii::app()->user->model->hasRight('ACCESS_ALL_MANAGERS_DATA') AND !$options['allowSearchOnAllManagers'])
			$this->managerId = Yii::app()->user->id;
		if (!empty($this->nameLike))
			$criteria->compare('t.name', $this->nameLike, true);
		if (!empty($options['region']) AND $options['region'] !== 'all')
			$criteria->addCondition("t.denormCitySubdomain = '" . $options['region'] . "'");
		
		
		if (!empty($this->managerId)) {
			if ($this->managerId == -1) {				
				$criteria->addCondition('managerRelations.userId IS NULL');
			} elseif ($this->managerId == -2) {				
				$criteria->addCondition('managerRelations.userId IS NOT NULL');
			} else {
				$criteria->addCondition('managerRelations.userId LIKE "'.$this->managerId.'"');
			}	
		}
		
		switch ($this->companyTypeArtificial) {
			case 'all':
				Address::$showInactive = true;
				break;
			case 'inactive':
				$this->oneOffer = 2;
				Address::$showInactive = true;
				break;
			case 'inactive_only':
				$this->oneOffer = 3;
				Address::$showInactive = true;
				break;
			case 'herd_only':
				$this->oneOffer = 4;
				Address::$showInactive = true;
				break;
			case 'self_registered':
				$this->oneOffer = 5;
				Address::$showInactive = true;
				$criteria->compare('t.nameENG', 'isViaSelfRegister', false);
				$criteria->addCondition('addresses.isActive = 0');
				break;
			case 'activePlusInactiveWithContact':
				Address::$showInactive = true;
				$criteria->addCondition('(userMedicals.companyId IS NOT NULL AND userMedicals.companyId NOT IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MAX(agreementNew) = 0)) OR t.id IN (SELECT DISTINCT companyId id  FROM `'.CompanyContact::model()->tableName().'`)');
				break;
			case 'inactiveWithoutContact':
				Address::$showInactive = true;
				$criteria->addCondition('(userMedicals.companyId IS NULL OR userMedicals.companyId IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MAX(agreementNew) = 0)) AND t.id NOT IN (SELECT DISTINCT companyId id  FROM `'.CompanyContact::model()->tableName().'`)');
				break;
			default:
				$this->oneOffer = 1;
				break;
		}
		//if (Address::$showInactive == false)
		//{
			if ($this->filterSamozapis == 'samozapis')
			{
				$cCondition = 'shortCompanyOfDoctor.samozapis = 1';
				$criteria->addCondition($cCondition);
			}
			
			if ($this->filterSamozapis == 'no-samozapis')
			{
				//$cCondition = 't.id NOT IN (SELECT companyId FROM `shortCompaniesOfDoctor` GROUP BY `companyId` HAVING MIN(samozapis) = 1)';
				$cCondition = 'addresses.samozapis = 0';
				$criteria->addCondition($cCondition);
			}
		//}
		
		$criteria->scopes = 'clinic'; //только клиники
		
		if($this->oneOffer == 1) {
			//$criteria->addCondition('userMedicals.companyId IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MAX(agreementNew) = 1)');
			$criteria->compare('userMedicals.agreementNew', '1', false);
		} elseif($this->oneOffer == 2) {
			$criteria->addCondition('(userMedicals.companyId IS NULL OR userMedicals.companyId IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MAX(agreementNew) = 0))');
		} elseif($this->oneOffer == 3) {
			$criteria->addCondition('userMedicals.companyId IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MAX(agreementNew) = 0)');
			$criteria->addCondition('companyContracts.companyId IN (SELECT companyId FROM `companyContract` GROUP BY `companyId`)');
		} elseif($this->oneOffer == 4) {
			$criteria->addCondition('(userMedicals.companyId IS NULL OR userMedicals.companyId IN (SELECT companyId FROM `userMedical` GROUP BY `companyId` HAVING MAX(agreementNew) = 0))');
			$criteria->addCondition('companyContracts.companyId IS NULL');
		}
		
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
		$clinics = [];
		foreach ($dataProvider->getData() as $row) {
			$clinics[$row->id] = $row;
		}
		$criteria = new CDbCriteria;
		$criteria->with = [
				'userMedicals' => [
						'together' => true,
				],
				'companySalesInfo' => [
						'together' => true,
				],
		];
		$criteria->addInCondition('t.id', array_keys($clinics));
		
		if (!$options['pageSize']) {
			$pageSize = 25;
		} else {
			$pageSize = $options['pageSize'];
		}
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => $pageSize,
			),			
			'sort' => [
				'defaultOrder'=> 'userMedicals.createDate DESC',
				'attributes' => [
					'LastCreateDate' => [
						'ASC' => 'LastCreateDate ASC',
						'DESC' => 'LastCreateDate DESC',
					],
					'companySalesInfo.managerComment' => [
						'ASC' => 'companySalesInfo.managerComment ASC',
						'DESC' => 'companySalesInfo.managerComment DESC',
					],
					'companySalesInfo.poType' => [
						'ASC' => 'companySalesInfo.poType ASC',
						'DESC' => 'companySalesInfo.poType DESC',
					],
					'userMedicals.createDate' => [
						'ASC' => 'userMedicals.createDate ASC',
						'DESC' => 'userMedicals.createDate DESC',
					],
				]
			]
		));
	}
	
	
	
	public function scopes() {
		return array(
			'clinic' => array(
				'condition' => "`t`.`categoryId` = 'a3969945e0f59fa84e0e7740c2e8cc87' OR `t`.`categoryId` = '8f7f0e92e866321d4ac5f2a5378a4668'"
			),
			'clinicByAddress' => array(
				'condition' => "`company`.`categoryId` = 'a3969945e0f59fa84e0e7740c2e8cc87' OR `company`.`categoryId` = '8f7f0e92e866321d4ac5f2a5378a4668'"
			),
			'insurance' => array(
				'condition' => "`t`.`categoryId` = 'adf65996fa1f52014e98585a4a40422d'"
			),
			'tourism'=>array(
				'condition' => "`t`.`companyTypeId` = 'a950904d-86fc-11e2-856f-e840f2aca94f'"
			),
			'beauty'=>array(
				'condition' => "`t`.`companyTypeId` = 'f40ea630-6070-11e2-b019-e840f2aca94f'"
			)
		);
	}
	
	/*public function getLastCreateDate() {
		return $this->_LastCreateDate;
	}
	public function setLastCreateDate($value) {
		var_dump(1);
		$this->_LastCreateDate = $value;
	}*/
        
    public function getAddressCount($activeOnly = 0) {
        
        $options;
        if ($activeOnly === 1) $options['activeOnly'] = 1;
        $addressCount = count($this->getCityAddresses($options));            
        return $addressCount;
    }
        
    public function getAppointmentsCount($options = null) {
                
		$search = new Search();
		$criteria = new CDbCriteria();
		$criteria->compare('companyId', $this->id);
		
		if ($options['apt_status']) $criteria->compare('statusId', $options['apt_status']);
		if ($options['addressId']) $criteria->compare('t.addressId', $options['addressId']);
		//if ($options['apt_status']) $criteria->compare('statusId',$options['apt_status']);
		
		$appointmentsCount = AppointmentToDoctors::model()->count($criteria);
		return $appointmentsCount;
    }
	
	public function getOneOfferNotAccepted()  {
		$userMedicals = UserMedical::model()->findAll('companyId = :cId',[
			':cId' => $this->id
		]);
		if($userMedicals) {
			foreach($userMedicals as $um) {
				if(!$um->agreementNew) {
					return 0;
				}
			}
			return 1;
		} else {
			return 0;
		}
	}
	
	/*public function getLastCreateDate() {	
		$criteria = new CDbCriteria();
		$criteria->order = 't.createDate DESC';
		$criteria->compare('t.companyId',$this->id);
		$result = UserMedical::model()->find($criteria);		
		return $result->agreementNew ? $result->createDate : '';
	}*/
	public function getUploadPath() {

		$path = Yii::getPathOfAlias('uploads.company') . '/' . $this->link;

		if ( !is_dir($path) )
			mkdir($path, 0775, true);
		return $path;
	}
	
	public function getUploadUrl() {

		return Yii::app()->baseUrl . '/uploads/company/' . $this->link;
	}

	public function getLogoPath() {
		return "{$this->uploadPath}/logo.png";
	}

	public function getLogo() {
		if ( !isset($this->_logo) )
			$this->_logo = is_file($this->logoPath) ? "logo.png" : false;
		if($this->_logo == false)
			$this->_logo = is_file(str_replace('.png','.jpg',$this->logoPath)) ? "logo.jpg" : false;
		return $this->_logo;
	}

	public function setLogo($value) {
		$this->_logo = $value;
	}

	public function getLogoUrl() {

		return $this->logo ? "{$this->uploadUrl}/$this->logo" : false;
	}
	/**
	 * 
	 * @return float rating
	 */
	public function getRoundedRating() {
		$result = (round($this->rating * 100) - round($this->rating * 100)%10)/100;
		return $result;
	}
	
	/**
	 * 
	 * @return type html
	 */
	public function getBoldedRoundedRating() {
		$data = explode(".",$this->roundedRating);
		return '<span>'.$data[0].'</span>.'.(!empty($data[1])?$data[1]:0);
	}
	
	public function getSeoName() {
		/* Режем слово Клиника, иначе будет дубль */	
		if(mb_strpos($this->name,"поликлиника",0,"UTF8") !== FALSE) {			
			return $this->name;			
		}				
		
		$name = preg_replace("/(клиника)/iu","",$this->name);		
		return 'Клиника '. trim($name); 		
	}
	
	public function getSeoNameBeauty() {
		/* Режем слово Клиника, иначе будет дубль */	
		$name = preg_replace("/(салон|красоты)/iu","",$this->name);		
		return trim($name); 		
	}
	
	public function getSeoNameInsurance() {
		/* Режем слово Клиника, иначе будет дубль */	
		#$name = preg_replace("/(страховая|компания)/iu","",$this->name);		
		return trim($this->name); 		
	}
	
	public function getSeoNameTourism() {
		/* Режем слово Клиника, иначе будет дубль */	
		#$name = preg_replace("/(страховая|компания)/iu","",$this->name);		
		return trim($this->name); 		
	}	
	
	public function getCityAddresses($options = null) {
		$search = new Search();
		$criteria = new CDbCriteria();
		$criteria->compare('cityId',$search->cityId);
		$criteria->compare('ownerId',$this->id);
        Address::$showInactive = ($options['activeOnly'] === 1) ? false : true;
		$addresses = Address::model()->findAll($criteria);
		return $addresses;
	}
	
	public function getCardCompletness() {
		
		$completness = 0;
		if ($this->description) $completness += 10;
		if (count($this->addresses)>0) $completness += 10;
		if (count($this->doctors)>0) $completness += 10;
		
		return $completness;
	}
	
	public function getDoctorsViewCount() {
		
		$stats = [];
		foreach ($this->doctors as $doc) {
		
			$stats = array_merge($stats, LogDoctor::model()->findAllByAttributes(['doctorId' => $doc->id]));
			
			/*foreach($stats as $stat)
			{					
				$strDate = date('d.m.y', (strtotime($stat->datetime)));
				$logDoctors[$strDate]++;
			}*/
		}
		
		return $stats;
	}
	
	public static function ActiveClinicsCriteria($args=NULL) {
		$criteria = new CDbCriteria();
		$criteria->compare('t.removalFlag', 0);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->compare('addresses.samozapis', 0);
		$criteria->compare('addresses.isActive', 1);
		$criteria->with = [
				'companyContracts' => [
						'select' => false,
						'together' => true,
						'joinType' => 'INNER JOIN'
				],
				'companyType' => [
						'select' => false,
						'together' => true
				],
				'addresses' => [
						'select' => false,
						'together' => true,
						'with' => [
								'userMedicals' => [
										'select' => false,
										'joinType' => 'INNER JOIN',
										'together' => true
								],
						],
				],
		];
		return $criteria;
	}

	public static function ActiveClinicAddressesCriteria($args=NULL) {
		$criteria = new CDbCriteria();
		$criteria->compare('company.removalFlag', 0);
		$criteria->compare('userMedicals.agreementNew', 1);
		$criteria->compare('t.samozapis', 0);
		$criteria->compare('t.isActive', 1);
		$criteria->with = [
				'userMedicals' => [
						'select' => false,
						'joinType' => 'INNER JOIN',
						'together' => true
				],
				'company' => [
					'with' => [
						'companyContracts' => [
								'select' => false,
								'together' => true,
								'joinType' => 'INNER JOIN'
						],
						'companyType' => [
								'select' => false,
								'together' => true
						],
					],
				],
		];
		return $criteria;
	}
	
	public function hasAppointments($time_period)
	{
		$appointmentModel = AppointmentToDoctors::model();
		$appointmentModel->time_period = $time_period;
		$dtOptions = [
			'manStatus' => 'r', //дошедшие + меди + не обработанные
			//'limit' => 1,
			'companyId' => $this->id,
		];
		$dataProvider = $appointmentModel->dataProviderForNewAdminPanel($dtOptions);
		return count($dataProvider->getData());// ? true : false;		
	}
	
	public function newsReference() {
		return NewsReference::model()->findByReferenceType(NewsReference::TYPE_COMPANY,['entityId'=>$this->id]);
	}
	
	public function updateRating() {
		$addressesRating = [];
		foreach ($this->addresses as $address) {
			$address->updateRating();
			if(floatval($address->rating) != floatval(0.0)) {
				$addressesRating[] = $address->rating;
			}
		}
		$sum = (float)0;
		if(count($addressesRating) > 0) {
			foreach($addressesRating as $r) {
				$sum+=$r;
			}
			$sum = number_format($sum / count($addressesRating),2);
			$criteria = new CDbCriteria();
			$criteria->compare("t.companyId", $this->id);
			$criteria->compare("t.statusId", Review::STATUS_PUBLISHED);
			$criteria->addCondition("t.ratingClinic IS NOT NULL");
			$reviews = Review::model()->findAll($criteria);
			if(count($reviews) > 0) {
				$sumRatingOfReviews = 0.0;
				foreach ($reviews as $review) {
					$sumRatingOfReviews += floatval($review->ratingClinic)*5;
				}
				$ratingOfReviews = $sumRatingOfReviews/count($reviews);
				$sum = ($sum + $ratingOfReviews)/2;
			} else {
				$sum *= 0.95;
			}
		}
		if($sum < 3.5) $sum = 3.5;
		$this->rating = $sum;
		$result = $this->update();
		return $result;
	}
	
	public function updateReviewsCount() {
		$count = Review::model()->count("t.companyId=:companyId AND t.statusId=:statusId",[":companyId"=>$this->id,":statusId"=>Review::STATUS_PUBLISHED]);
		$this->reviewsCount = $count;
		$result = $this->update();
		return $result;
	}

	public function getMailListForAppReports()
	{
		$mailList = [];
		//if ($this->userMedical->email)
		//	$mailList[] = $this->userMedical->email;
		//$lprEmails = explode(' ', $this->company->companySalesInfo->email);
        $lprEmails = preg_split('/( |,|;|\r|\n|\r\n)/', $this->companySalesInfo->email);
		foreach($lprEmails as $lprEmail)
		{
			if (filter_var($lprEmail, FILTER_VALIDATE_EMAIL) && !in_array($lprEmail, $mailList))
				$mailList[] = $lprEmail;
		}
		return $mailList;
	}
}