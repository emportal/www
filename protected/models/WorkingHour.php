<?php

/**
 * This is the model class for table "workingHour".
 *
 * The followings are the available columns in table 'workingHour':
 * @property string $id
 * @property string $addressId
 * @property string $MondayStart
 * @property string $MondayFinish
 * @property string $TuesdayStart
 * @property string $TuesdayFinish
 * @property string $WednesdayStart
 * @property string $WednesdayFinish
 * @property string $ThursdayStart
 * @property string $ThursdayFinish
 * @property string $FridayStart
 * @property string $FridayFinish
 * @property string $SaturdayStart
 * @property string $SaturdayFinish
 * @property string $SundayStart
 * @property string $SundayFinish
 *
 * The followings are the available model relations:
 * @property Address $address
 */
class WorkingHour extends ActiveRecord {
	const DEFAULT_TIME_START = "08:00";
	const DEFAULT_TIME_FINISH = "22:00";
	const DAY_TIME = 86400;
	const MONTH_TIME = 2592000;
	const STEP_TIME = 1800;

	public $allStart;
	public $allFinish;
	public $allCheck;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WorkingHour the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'workingHour';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, addressId', 'length', 'max' => 36),
			array('addressId', 'unique'),
			array('MondayStart, MondayFinish, TuesdayStart, TuesdayFinish, WednesdayStart, WednesdayFinish, ThursdayStart, ThursdayFinish, FridayStart, FridayFinish, SaturdayStart, SaturdayFinish, SundayStart, SundayFinish', 'length', 'max' => 5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('allStart,allFinish,allCheck','safe'),
			array('id, addressId, MondayStart, MondayFinish, TuesdayStart, TuesdayFinish, WednesdayStart, WednesdayFinish, ThursdayStart, ThursdayFinish, FridayStart, FridayFinish, SaturdayStart, SaturdayFinish, SundayStart, SundayFinish', 'safe', 'on' => 'search'),
			array('MondayStart', 'compareMonday'),
			array('TuesdayStart', 'compareTuesday'),
			array('WednesdayStart', 'compareWednesday'),
			array('ThursdayStart', 'compareThursday'),
			array('FridayStart', 'compareFriday'),
			array('SaturdayStart', 'compareSaturday'),
			array('SundayStart', 'compareSunday')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'addressId' => 'Address',
			'MondayStart' => 'Понедельник',
			'MondayFinish' => 'Пн',
			'TuesdayStart' => 'Вторник',
			'TuesdayFinish' => 'Вт',
			'WednesdayStart' => 'Среда',
			'WednesdayFinish' => 'Ср',
			'ThursdayStart' => 'Четверг',
			'ThursdayFinish' => 'Чт',
			'FridayStart' => 'Пятница',
			'FridayFinish' => 'Пт',
			'SaturdayStart' => 'Суббота',
			'SaturdayFinish' => 'Сб',
			'SundayStart' => 'Воскресенье',
			'SundayFinish' => 'Вс',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('addressId', $this->addressId, true);
		$criteria->compare('MondayStart', $this->MondayStart, true);
		$criteria->compare('MondayFinish', $this->MondayFinish, true);
		$criteria->compare('TuesdayStart', $this->TuesdayStart, true);
		$criteria->compare('TuesdayFinish', $this->TuesdayFinish, true);
		$criteria->compare('WednesdayStart', $this->WednesdayStart, true);
		$criteria->compare('WednesdayFinish', $this->WednesdayFinish, true);
		$criteria->compare('ThursdayStart', $this->ThursdayStart, true);
		$criteria->compare('ThursdayFinish', $this->ThursdayFinish, true);
		$criteria->compare('FridayStart', $this->FridayStart, true);
		$criteria->compare('FridayFinish', $this->FridayFinish, true);
		$criteria->compare('SaturdayStart', $this->SaturdayStart, true);
		$criteria->compare('SaturdayFinish', $this->SaturdayFinish, true);
		$criteria->compare('SundayStart', $this->SundayStart, true);
		$criteria->compare('SundayFinish', $this->SundayFinish, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
    
	public function afterSave() {
		if (parent::afterSave()) {
			$placeOfWork = PlaceOfWork::model()->findAll("addressId = :addressId", ['addressId' => $this->addressId]);
			foreach ($placeOfWork as $model) {
				Yii::app()->cache->delete(md5('timeBlock'.$model->addressId.$model->doctorId));
			}
				
			return true;
		}
	}
    public static function getPossbileEmportalTimeRange()
    {
        $time = [];
        $tStart = strtotime('00:00');
        $tEnd = strtotime('23:59');
        $tNow = $tStart;
        
        while($tNow <= $tEnd) {
            $keyValue = date('H:i', $tNow);
            $time[$keyValue] = $keyValue;
            $tNow = strtotime('+30 minutes', $tNow);
        }
        return $time;
    }
    
	public static function getDate($link, $date, $doctor = null, $backend = false, $idPat = NULL)
    {
		$clinic = Address::model()->findByLink($link);
		if(empty($clinic)) {
			return ['date' => date("d.m.Y", strtotime($date)), "msg" => "Ошибка: Клиника не найдена!"];
		}
		$idClinic = $clinic->id;
		
		if (!empty($doctor)) {
			$doctorModel = Doctor::model()->findByAttributes(["link" => $doctor]);

			//проверка на врача из раздела самозаписи (получаем расписание из нетрики для таких врачей)
			if($doctorModel) {
				$extDoctor = ExtDoctor::checkDoctor($doctorModel->id, $clinic->id);
				if($extDoctor) {
					$loader = $extDoctor->sysType->getLoader();
					$time = [];
					$msg = "";
					$status = "OK";
					try {
						if(is_array($idPat) || is_object($idPat) || trim(strval($idPat)) !== "") {
							$visitStart = date("Y-m-d", strtotime($date))."T00:00:00";
							$visitEnd = date("Y-m-d", strtotime($date))."T23:59:59";
							$referralId = @Yii::app()->request->cookies['OMSForm_referralId'];
							$avaibleAppointments = $loader->getAvaibleAppointments($extDoctor->extId, $extDoctor->extAddressId, $idPat, $visitStart, $visitEnd, $referralId);
							if($avaibleAppointments->GetAvaibleAppointmentsResult->Success) {
								$listAppointments = $avaibleAppointments->GetAvaibleAppointmentsResult->ListAppointments->Appointment;
								if(is_object($listAppointments)) $listAppointments = [$listAppointments];
								if(count($listAppointments) > 0) {
									foreach ($listAppointments as $appointment) {
										$dateVisitStart = new DateTime($appointment->VisitStart);
										$time[] = [
											'id' => $appointment->IdAppointment,
											'text' => $dateVisitStart->format('H:i'),
											'status' => isset($appointment->status) ? $appointment->status : 'allow',
											'VisitStart' => $appointment->VisitStart,
											'VisitEnd' => $appointment->VisitEnd,
										];
									}
								} else {
									$msg = "На этот день талонов нет.";
								}
							} else {
								$status = "ERROR";

								$error = false;
								$checkResult = &$avaibleAppointments->GetAvaibleAppointmentsResult;
								if(isset($checkResult->ErrorList)) {
									if(is_array($checkResult->ErrorList) && isset($checkResult->ErrorList[0])) {
										$error = $checkResult->ErrorList[0];
									}
									elseif (is_object($checkResult->ErrorList) && isset($checkResult->ErrorList->Error)) {
										$error = $checkResult->ErrorList->Error;
									}
								}
								if($error && isset($error->IdError) && isset($error->ErrorDescription)) {
									$msg = MyTools::createFromTemplate("Ошибка! %/Error/IdError%: %Error/ErrorDescription%. Для успешной записи следует явиться в закрепленное за вами медицинское учреждение и зарегистрироваться в очном порядке.",
											["%/Error/IdError%" => $error->IdError, "%Error/ErrorDescription%" => $error->ErrorDescription]);
								} else {
									$msg = $checkResult->ErrorList->Error->ErrorDescription;
								}

								LogSamozapis::addLog($loader,$checkResult,$doctorModel->id);
							}
						} else {
							//$status = "ERROR";
							$msg = "Ошибка: Пациент не задан. Для успешной записи следует явиться в закрепленное за вами медицинское учреждение и зарегистрироваться в очном порядке.";
						}
					} catch (Exception $e) {
						$status = "ERROR";
						$msg = "Ошибка ".$e->getMessage();
					}
					if($status == "ERROR") {
						if(empty($msg)) $msg = "Ошибка!";
						MyTools::ErrorReport(date("Y-m-d H:i:s", strtotime("NOW"))." > Ошибка при получении талонов из Netrika: ".$msg);
					}
					$timeBlock = array(
						'date' => date("d.m.Y", strtotime($date)),
						'time' => $time,
						'msg' => $msg,
					);
					return $timeBlock;
				}
			}
		}

		$timeBlock = Yii::app()->cache->get(md5('timeBlock'.$idClinic.$doctorModel->id));
		if($timeBlock['block'][$date]) {
			return [
					'date' => date("d.m.Y", strtotime($date)),
					'time' => $timeBlock['block'][$date],
					'msg' => $timeBlock['msg'][$date],
				];			
		}

		//формируем время от и до
		$doctorTimeWork = [];
		$doctorTimeWorkMsg = [];
		$timeNowUnix = time();
		$timeMonthUnix = $timeNowUnix + self::MONTH_TIME;
		$timeDateUnix = strtotime($date);
		if($timeDateUnix > $timeMonthUnix) {
			$timeMonthUnix = $timeDateUnix + self::DAY_TIME;
		}

		//время обеда
		$breakStart = $clinic->getBreakStart();
		$breakFinish = $clinic->getBreakFinish();

		//получем все записи к врачу за определенный период
		if($doctorModel) {
			$sql = "addressId=:addrId AND doctorId=:doctorId AND (plannedTime >= :date_start AND plannedTime <= :date_end)";
			$where = ["doctorId"=>$doctorModel->id];
		}
		else {
			$sql = "addressId=:addrId AND doctorId IS NULL AND (plannedTime >= :date_start AND plannedTime <= :date_end)";
			$where = [];
		}

		$appointments = Yii::app()->db->createCommand()
							->setFetchMode(PDO::FETCH_KEY_PAIR)
							->select("plannedTime, statusId")
							->from(AppointmentToDoctors::model()->tableName())
							->where($sql,array_merge([
								"addrId" => $idClinic,
								"date_start"=>date("Y-m-d 00:00:00",$timeNowUnix), 
								"date_end" => date("Y-m-d 23:59:59",$timeMonthUnix),
							],$where))->order('plannedTime')->queryAll();

		//формируем массив рабочего времени
		for($timeDay = $timeNowUnix; $timeDay <= $timeMonthUnix; $timeDay += self::DAY_TIME) {
			$dateStep = date('Y-m-d',$timeDay);

			if($doctorModel) {
				$timeStartDoctor = $doctorModel->workingHours->getTimeStart($dateStep);
				$timeFinishDoctor = $doctorModel->workingHours->getTimeFinish($dateStep);
			}

			$timeStart = strtotime($dateStep.' '.($timeStartDoctor ? $timeStartDoctor : $clinic->workingHours->getTimeStart($dateStep)));
			$timeFinish = strtotime($dateStep.' '.($timeFinishDoctor ? $timeFinishDoctor : $clinic->workingHours->getTimeFinish($dateStep)));

			$breakStartTime = $breakStart ? strtotime($dateStep.' '.$breakStart): false;
			$breakFinishTime = $breakFinish ? strtotime($dateStep.' '.$breakFinish): false;

			$doctorTimeWork[$dateStep] = [];
			if($timeStart === $timeFinish) {
				$doctorTimeWorkMsg[$dateStep] = 'Выходной';
			}
			else {
				for($time = $timeStart; $time < $timeFinish; $time += self::STEP_TIME) {

					if($breakStartTime && $breakFinishTime && ($time >= $breakStartTime && $time < $breakFinishTime)) {
						continue;
					}

					$t = date("H:i:00",$time);
					$appointment = $appointments[$dateStep.' '.$t];
					if(isset($appointment)) {
						if($appointment == AppointmentToDoctors::DISABLED || ($appointment != AppointmentToDoctors::DECLINED_BY_PATIENT && $appointment != AppointmentToDoctors::DECLINED_BY_REGISTER)) {
							continue;
						}
					}
					$t = date("H:i",$time);
					$doctorTimeWork[$dateStep][$t] = [
						'text' => $t,
						'status' => $time < $timeNowUnix + 60 * 60 ? 'hidden' : 'allow',
					];
				}
				$doctorTimeWorkMsg[$dateStep] = count($doctorTimeWork[$dateStep]) ? 'Рабочий' : 'Выходной';
			}
		}

		Yii::app()->cache->set(md5('timeBlock'.$idClinic.$doctorModel->id), ['msg' => $doctorTimeWorkMsg, 'block' => $doctorTimeWork]);

		return [
				'date' => date("d.m.Y", strtotime($date)),
				'time' => $doctorTimeWork[$date],
				'msg' => $doctorTimeWorkMsg[$date]
			];
	}

	public function getTimeStart($date) {
		$time = $this->getAttribute(date("l", strtotime($date)) . "Start");
		if (!$time) {
			$time = self::DEFAULT_TIME_START;
		}
		if($time == "11111") {
			$time = "00:00";
		}
		return $time;
	}

	public function getTimeFinish($date) {
		$time = $this->getAttribute(date("l", strtotime($date)) . "Finish");
		if (!$time) {
			$time = self::DEFAULT_TIME_FINISH;
		}
		if($time == "11111") {
			$time = "23:59";
		}
		return $time;		
	}

	/**
	 * 
	 * @param string $link идентификатор адреса
	 * @param string $date дата первого дня, во время которого был вызван запрос
	 * @param string $type тип forward/back - в какую сторону возвращать сдвиг
	 * @param string $frontend если false то отображает все типы блокировок разными цветами ( отключенные синим, записанные через сайт красным и пр.) изначально показывает все красным
	 * @return html
	 */
	public static function getDatesForCal($link, $date = null, $type = null, $doctorLink = null,$backend = false) {


		$clinic = Address::model()->findByLink($link);
		$idClinic = $clinic->id;


		if (($clinic->breakStart != "00:00" && $clinic->breakFinish != "00:00") || ($clinic->breakStart != "0000*" && $clinic->breakFinish != "0000-")) {
			$brStart = ((int)explode(":", $clinic->breakStart)[0] * 60) + (int)explode(":", $clinic->breakStart)[1];
			$brFinish = ((int)explode(":", $clinic->breakFinish)[0] * 60);
			$breakTimeBlock = array();
			if ($brStart < $brFinish) {
				for ($i = $brStart; $i < $brFinish; $i+=30) {
					$m = intval($i%60);
					$h = intval($i/60);
					if($m < 10) $m = "0".$m;
					if($h < 10) $h = "0".$h;
					$breakTimeBlock[$h . ":" . $m] = $h . ":" . $m;
				}
			}
		}
		$i = 1;
		$max = $i + 3;
		for ($i; $i < $max; $i++) {
			switch ($type) {
				case 'forward':
					$offset = "+ " . ($i + 2);
					break;
				case 'back':
					$offset = "- " . ($i);
					break;
				case 'doctor':
					//Используем доктор, когда меняем доктора или убираем, чтобы не возвращаться к изначальному положению
					$offset = "+" . ($i - 1);
					break;
				default:
					$offset = "+ " . $i;
					break;
			}
			$time = array();
			if(!empty($clinic->workingHours)) {
				$timeStart = $clinic->workingHours->getAttribute(date("l", strtotime($date . "$offset days ")) . "Start");
				$timeFinish = $clinic->workingHours->getAttribute(date("l", strtotime($date . "$offset days ")) . "Finish");
			}
			if (!$timeStart && !$timeFinish) {
				$timeStart = self::DEFAULT_TIME_START;
				$timeFinish = self::DEFAULT_TIME_FINISH;
			}

			if ($timeStart && $timeFinish) {
				//круглосуточно
				if (time() - 60 * 60 * 24 > strtotime($date . "$offset days")) {
					$timeBlock[] = array(
						'date' => date("d.m.Y", strtotime($date . "$offset days")),
						'time' => array(),
						'msg' => ""
					);
					continue;
				}
				if (($timeStart == "11111" || $timeFinish == '11111') || ($timeStart == "00:00" && $timeFinish == "23:59")) {
					$time = array(
							'00:00' => '00:00', '00:30' => '00:30', '01:00' => '01:00', '01:30' => '01:30',
							'02:00' => '02:00', '02:30' => '02:30', '03:00' => '03:00', '03:30' => '03:30',
							'04:00' => '04:00', '04:30' => '04:30', '05:00' => '05:00', '05:30' => '05:30',
							'06:00' => '06:00', '06:30' => '06:30', '07:00' => '07:00', '07:30' => '07:30',
							'08:00' => '08:00', '08:30' => '08:30', '09:00' => '09:00', '09:30' => '09:30',
							'10:00' => '10:00', '10:30' => '10:30', '11:00' => '11:00', '11:30' => '11:30',
							'12:00' => '12:00', '12:30' => '12:30', '13:00' => '13:00', '13:30' => '13:30',
							'14:00' => '14:00', '14:30' => '14:30', '15:00' => '15:00', '15:30' => '15:30',
							'16:00' => '16:00', '16:30' => '16:30', '17:00' => '17:00', '17:30' => '17:30',
							'18:00' => '18:00', '18:30' => '18:30', '19:00' => '19:00', '19:30' => '19:30',
							'20:00' => '20:00', '20:30' => '20:30', '21:00' => '21:00', '21:30' => '21:30',
							'22:00' => '22:00', '22:30' => '22:30', '23:00' => '23:00', '23:30' => '23:30',
					);

					$freeTime = array();
					$unixTime = strtotime($date . "$offset days");
					foreach ($time as $t) {						
						$status = "allow";
						$arr = explode(":", $t);
						$mktime = mktime($arr[0], $arr[1], 0, date("m", $unixTime), date("d", $unixTime), date("Y", $unixTime));
						$dt = date("Y-m-d H:i:s", $mktime);
						//Если указан доктор, убираем номерки, только с записями к нему
						if ($doctorLink) {
							$disabled = Yii::app()->db->createCommand()
									->select("COUNT(*)")
									->from(AppointmentToDoctors::model()->tableName())
									->where("addressId=:addrId AND plannedTime = :dt AND statusId=:stat1 AND doctorId=:dId", array(
										'addrId' => $idClinic,
										'dt' => $dt,
										'stat1' => AppointmentToDoctors::DISABLED,
										'dId' => Doctor::model()->findByLink($doctorLink)->id
									))->queryScalar();
							
							$isHide = Yii::app()->db->createCommand()
											->select("COUNT(*)")
											->from(AppointmentToDoctors::model()->tableName())
											->where("addressId=:addrId AND plannedTime = :dt AND statusId<>:stat1 AND statusId<>:stat2 AND doctorId=:dId", array(
												'addrId' => $idClinic,
												'dt' => $dt,
												'stat1' => AppointmentToDoctors::DECLINED_BY_PATIENT,
												'stat2' => AppointmentToDoctors::DECLINED_BY_REGISTER,
												'dId' => Doctor::model()->findByLink($doctorLink)->id
											))->queryScalar();


							//Если не выбран доктор, то убираем номерки с записями без докторов
						} else {
							$isHide = Yii::app()->db->createCommand()
											->select("COUNT(*)")
											->from(AppointmentToDoctors::model()->tableName())
											->where("addressId=:addrId AND plannedTime = :dt AND statusId<>:stat1 AND statusId<>:stat2 AND doctorId IS NULL", array(
												'addrId' => $idClinic,
												'dt' => $dt,
												'stat1' => AppointmentToDoctors::DECLINED_BY_PATIENT,
												'stat2' => AppointmentToDoctors::DECLINED_BY_REGISTER
											))->queryScalar();
						}

						if(!empty($isHide)) {
							$status = "busy";
						}
						/* Если статус disabled*/
						if(!empty($disabled) && $backend) {
							$status = 'disabled';
						}
						if ($mktime < time() + 60 * 60) {
							$status = "hidden";
						}

						$freeTime[] = array(
							'text' => $t,
							'status' => $status
						);
					}

					$timeBlock[] = array(
						'date' => date("d.m.Y", strtotime($date . "$offset days")),
						'time' => $freeTime,
						'msg' => "Круглосуточно"
					);

					continue;
				}

				//выходной
				if ($timeStart === $timeFinish) {
					$timeBlock[] = array(
						'date' => date("d.m.Y", strtotime($date . "$offset days")),
						'time' => array(),
						'msg' => "Выходной"
					);
					continue;
				}

				//working day
				$var = explode(":", $timeStart);
				$start_hour = (int) $var[0];
				$start_min = (int) $var[1];

				$var = explode(":", $timeFinish);
				$finish_hour = (int) $var[0];
				$finish_min = (int) $var[1];

				$unixTime = strtotime($date . "$offset days");
				while ($start_hour != $finish_hour || $start_min != $finish_min) {
					$status = "allow";
					$mktime = mktime($start_hour, $start_min, 0, date("m", $unixTime), date("d", $unixTime), date("Y", $unixTime));
					$dt = date("Y-m-d H:i:s", $mktime);
					//Если указан доктор, убираем номерки, только с записями к нему
					if ($doctorLink) {						
						$disabled = Yii::app()->db->createCommand()
									->select("COUNT(*)")
									->from(AppointmentToDoctors::model()->tableName())
									->where("addressId=:addrId AND plannedTime = :dt AND statusId=:stat1 AND doctorId=:dId", array(
										'addrId' => $idClinic,
										'dt' => $dt,
										'stat1' => AppointmentToDoctors::DISABLED,
										'dId' => Doctor::model()->findByLink($doctorLink)->id
									))->queryScalar();
						
						$isHide = Yii::app()->db->createCommand()
										->select("COUNT(*)")
										->from(AppointmentToDoctors::model()->tableName())
										->where("addressId=:addrId AND plannedTime = :dt AND statusId<>:stat1 AND statusId<>:stat2 AND doctorId=:dId", array(
											'addrId' => $idClinic,
											'dt' => $dt,
											'stat1' => AppointmentToDoctors::DECLINED_BY_PATIENT,
											'stat2' => AppointmentToDoctors::DECLINED_BY_REGISTER,
											'dId' => Doctor::model()->findByLink($doctorLink)->id
										))->queryScalar();

						//Если не выбран доктор, то убираем номерки с записями без докторов
					} else {
						$isHide = Yii::app()->db->createCommand()
										->select("COUNT(*)")
										->from(AppointmentToDoctors::model()->tableName())
										->where("addressId=:addrId AND plannedTime = :dt AND statusId<>:stat1 AND statusId<>:stat2 AND doctorId IS NULL", array(
											'addrId' => $idClinic,
											'dt' => $dt,
											'stat1' => AppointmentToDoctors::DECLINED_BY_PATIENT,
											'stat2' => AppointmentToDoctors::DECLINED_BY_REGISTER
										))->queryScalar();
					}
					
					if(!empty($isHide)) {
						$status = "busy";
					}
					/* Если статус disabled*/
					if(!empty($disabled) && $backend) {
						$status = 'disabled';
					}
					if ($mktime < time() + 60 * 60) {
						$status = "hidden";
					}
					$time[] = array(
						'text' => str_replace(" ", "0", sprintf("%2d:%2d", $start_hour, $start_min)),
						'status' => $status,
					);

					if ($start_min == 30) {
						$start_hour++;
						$start_min = 0;
					} else {
						$start_min = 30;
					}

					if ($start_hour == 24) {
						$start_hour = 0;
					}
				}

				//рабочий день
				$timeBlock[] = array(
					'date' => date("d.m.Y", strtotime($date . "$offset days")),
					'time' => $time,
					'msg' => $msg
				);

				//Проверяем занятые часы приема
				foreach ($timeBlock[$i - 1]['time'] as $k => $time) {
					/* $a_time=explode(':',$time);
					  $a_date=explode('.',$timeBlock[$i-1]['date']); */
					//$date=date("Y-m-d h:i:s",  mktime($a_time[0], $a_time[1], 0, $a_date[1], $a_date[0], date('Y')));
					//$timeBlock[$i-1]['date']
				}
			}
		}
		if ($type === "back") {
			$timeBlock = array_reverse($timeBlock);
		}
		/* Убираем под запись блоки на период обеденного перерыва */
		if ($timeBlock) {
			foreach ($timeBlock as &$block) {
				foreach ($block['time'] as $key => $time) {
					foreach ($breakTimeBlock as $brTime) {
						if ($time['text'] == $brTime) {
							unset($block['time'][$key]);
						}
					}
				}
			}
		}
		#var_dump($breakTimeBlock);
		#var_dump($timeBlock);
		/* echo "<pre>";
		  var_dump($timeBlock);
		  echo "</pre>"; */
		return $timeBlock;
	}

	public function compareMonday($attribute, $params) {
		if ($this->MondayStart == '11111')
			return true;
		if (($this->MondayStart == $this->MondayFinish) && $this->MondayStart == '00:00')
			return true;

		$start = (int) str_replace(":", "", $this->MondayStart);
		$finish = (int) str_replace(":", "", $this->MondayFinish);
		if ($start >= $finish) {
			$this->addError($attribute, 'Время начала должно быть больше время окончания');
		}
	}

	public function compareTuesday($attribute, $params) {
		if ($this->TuesdayStart == '11111')
			return true;
		if (($this->TuesdayStart == $this->TuesdayFinish) && $this->TuesdayStart == '00:00')
			return true;

		$start = (int) str_replace(":", "", $this->TuesdayStart);
		$finish = (int) str_replace(":", "", $this->TuesdayFinish);
		if ($start >= $finish) {
			$this->addError($attribute, 'Время начала должно быть больше время окончания');
		}
	}

	public function compareWednesday($attribute, $params) {
		if ($this->WednesdayStart == '11111')
			return true;
		if (($this->WednesdayStart == $this->WednesdayFinish) && $this->WednesdayStart == '00:00')
			return true;

		$start = (int) str_replace(":", "", $this->WednesdayStart);
		$finish = (int) str_replace(":", "", $this->WednesdayFinish);
		if ($start >= $finish) {
			$this->addError($attribute, 'Время начала должно быть больше время окончания');
		}
	}

	public function compareThursday($attribute, $params) {
		if ($this->ThursdayStart == '11111')
			return true;
		if (($this->ThursdayStart == $this->ThursdayFinish) && $this->ThursdayStart == '00:00')
			return true;

		$start = (int) str_replace(":", "", $this->ThursdayStart);
		$finish = (int) str_replace(":", "", $this->ThursdayFinish);
		if ($start >= $finish) {
			$this->addError($attribute, 'Время начала должно быть больше время окончания');
		}
	}

	public function compareFriday($attribute, $params) {
		if ($this->FridayStart == '11111')
			return true;
		if (($this->FridayStart == $this->FridayFinish) && $this->FridayStart == '00:00')
			return true;

		$start = (int) str_replace(":", "", $this->FridayStart);
		$finish = (int) str_replace(":", "", $this->FridayFinish);
		if ($start >= $finish) {
			$this->addError($attribute, 'Время начала должно быть больше время окончания');
		}
	}

	public function compareSaturday($attribute, $params) {
		if ($this->SaturdayStart == '11111')
			return true;
		if (($this->SaturdayStart == $this->SaturdayFinish) && $this->SaturdayStart == '00:00')
			return true;

		$start = (int) str_replace(":", "", $this->SaturdayStart);
		$finish = (int) str_replace(":", "", $this->SaturdayFinish);
		if ($start >= $finish) {
			$this->addError($attribute, 'Время начала должно быть больше время окончания');
		}
	}

	public function compareSunday($attribute, $params) {
		if ($this->SundayStart == '11111')
			return true;
		if (($this->SundayStart == $this->SundayFinish) && $this->SundayStart == '00:00')
			return true;

		$start = (int) str_replace(":", "", $this->SundayStart);
		$finish = (int) str_replace(":", "", $this->SundayFinish);
		if ($start >= $finish) {
			$this->addError($attribute, 'Время начала должно быть больше время окончания');
		}
	}

	/**
	 * Проверка являются ли все часы работы компании круглосуточными
	 */
	public function getIsWorkingHoursAllTheSame() {
		$arr = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');

		$fullday = true;
		for ($i = 0; $i < count($arr); $i++) {
			$day = $arr[$i];
			if (($this->{$day . Start} == '11111' || $this->{$day . Finish} == '11111') ||
					($this->{$day . Start} == '00:00' && $this->{$day . Finish} == '23:59')) {
				continue;
			} else {
				$fullday = false;
				break;
			}
		}
		if ($fullday)
			return 'fullday';

		$relax = true;
		for ($i = 1; $i < count($arr); $i++) {
			$day = $arr[$i];
			$prevDay = $arr[$i - 1];
			if ($this->{$day . Start} == $this->{$prevDay . Start} && $this->{$day . Finish} == $this->{$prevDay . Finish} &&
					$this->{$day . Start} == "00:00" && $this->{$day . Finish} == "00:00") {
				continue;
			} else {
				$relax = false;
				break;
			}
		}

		if ($relax)
			return 'relax';

		$work = true;
		for ($i = 1; $i < count($arr); $i++) {
			$day = $arr[$i];
			$prevDay = $arr[$i - 1];
			if ($this->{$day . Start} == $this->{$prevDay . Start} && $this->{$day . Finish} == $this->{$prevDay . Finish} && ($this->{$day . Start} != '' || $this->{$day . Finish} != '')) {
				continue;
			} else {
				$work = false;
				break;
			}
		}

		if ($work) {
			return 'work';
		}
	}

	/* Проверка являются ли все будни одинаковыми */

	public function getIsWeekdaysTheSame() {
		$arr = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');

		$fullday = true;
		for ($i = 0; $i < count($arr); $i++) {
			$day = $arr[$i];
			if (($this->{$day . Start} == '11111' || $this->{$day . Finish} == '11111') ||
					($this->{$day . Start} == '00:00' && $this->{$day . Finish} == '23:59')) {
				continue;
			} else {
				$fullday = false;
				break;
			}
		}
		if ($fullday)
			return 'fullday';

		$relax = true;
		for ($i = 1; $i < count($arr); $i++) {
			$day = $arr[$i];
			$prevDay = $arr[$i - 1];
			if ($this->{$day . Start} == $this->{$prevDay . Start} && $this->{$day . Finish} == $this->{$prevDay . Finish} &&
					$this->{$day . Start} == "00:00" && $this->{$day . Finish} == "00:00") {
				continue;
			} else {
				$relax = false;
				break;
			}
		}

		if ($relax)
			return 'relax';

		$work = true;
		for ($i = 1; $i < count($arr); $i++) {
			$day = $arr[$i];
			$prevDay = $arr[$i - 1];
			if ($this->{$day . Start} == $this->{$prevDay . Start} && $this->{$day . Finish} == $this->{$prevDay . Finish} && ($this->{$day . Start} != '' || $this->{$day . Finish} != '')) {
				continue;
			} else {
				$work = false;
				break;
			}
		}

		if ($work)
			return 'work';
	}

	/* Проверяем и склеиваем если выходные с одинаковыми часами работы */

	public function getIsWeekendTheSame() {
		$arr = array('Saturday', 'Sunday');

		$fullday = true;
		for ($i = 0; $i < count($arr); $i++) {
			$day = $arr[$i];
			if (($this->{$day . Start} == '11111' || $this->{$day . Finish} == '11111') ||
					($this->{$day . Start} == '00:00' && $this->{$day . Finish} == '23:59')) {
				continue;
			} else {
				$fullday = false;
				break;
			}
		}
		if ($fullday)
			return 'fullday';

		$relax = true;
		for ($i = 1; $i < count($arr); $i++) {
			$day = $arr[$i];
			$prevDay = $arr[$i - 1];
			if ($this->{$day . Start} == $this->{$prevDay . Start} && $this->{$day . Finish} == $this->{$prevDay . Finish} &&
					$this->{$day . Start} == "00:00" && $this->{$day . Finish} == "00:00") {
				continue;
			} else {
				$relax = false;
				break;
			}
		}

		if ($relax)
			return 'relax';

		$work = true;
		for ($i = 1; $i < count($arr); $i++) {
			$day = $arr[$i];
			$prevDay = $arr[$i - 1];
			if ($this->{$day . Start} == $this->{$prevDay . Start} && $this->{$day . Finish} == $this->{$prevDay . Finish} && ($this->{$day . Start} != '' || $this->{$day . Finish} != '')) {
				continue;
			} else {
				$work = false;
				break;
			}
		}

		if ($work)
			return 'work';
	}
	
	public function getIsEmpty() {
		foreach(array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday', 'Sunday') as $day) {
			if(!empty($this->{$day . Start}) || !empty($this->{$day . Finish})) {
				return true;
			}
		}
		return false;
	}
	
	public function setHoursForAllDays($timeStart = '10:00', $timeFinish = '18:00')
	{
		$days = MyTools::getEnglishDayNames();
		foreach($days as $day)
		{
			$this->{$day . Start} = $timeStart;
			$this->{$day . Finish} = $timeFinish;
		}
		return true;
	}
	
	public function getWeekDayTime($weekDay) {
		$result = ["start"=>null, "finish"=>null];
		$timeStart = $this->getAttribute($weekDay . "Start");
		if(empty($timeStart)) $timeStart = self::DEFAULT_TIME_START;
		elseif($timeStart === "11111") $timeStart = "00:00";
		$result["start"] = array_product(explode(":", $timeStart));
		$timeFinish = $this->getAttribute($weekDay . "Finish");
		if(empty($timeFinish)) $timeFinish = self::DEFAULT_TIME_START;
		elseif($timeFinish === "11111") $timeFinish = "23:59";
		$result["finish"] = array_product(explode(":", $timeFinish));
		return $result;
	}
	
	public static function convertReceptionsToApiFormat($workingHour, $date, $addressLink)
	{
		$possibleTimeQuants = [];
		foreach($workingHour['time'] as $index => $time) {
			if ($time['status'] === 'allow')
				$possibleTimeQuants[] = [
					'datetime' => $date . ' ' . $time['text'],
					//'time' => $time['text'],
					'addressId' => $addressLink,
				];
		}
		return $possibleTimeQuants;
	}
	
	public function getWeeklyHour() {
		$daysOfWeekMask = [];
		$timetable = [];
		if ($this->isWorkingHoursAllTheSame) {
			switch ($this->isWorkingHoursAllTheSame) {
				case 'fullday':
					$timetable["Пн - Вс"] = "Круглосуточно";
					$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, ["00:00 - 24:00" => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true, 5=>true, 6=>true]]);
					break;
				case 'work':
					$Hours = $this->MondayStart . " - " . $this->MondayFinish;
					$timetable["Пн - Вс"] = $Hours;
					$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, [$Hours => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true, 5=>true, 6=>true]]);
					break;
				case 'relax':
					$timetable["Пн - Вс"] = "Выходной";
					break;
			}
		} else
		{
			if ($this->isWeekdaysTheSame) {
				switch ($this->isWeekdaysTheSame) {
					case 'fullday':
						$timetable["Пн - Пт"] = "Круглосуточно";
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, ["00:00 - 24:00" => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true]]);
						break;
					case 'work':
						$Hours = $this->MondayStart . " - " . $this->MondayFinish;
						$timetable["Пн - Пт"] = $Hours;
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, [$Hours => [0=>true, 1=>true, 2=>true, 3=>true, 4=>true]]);
						break;
					case 'relax':
						$timetable["Пн - Пт"] = "Выходной";
						break;
				}
			}
			if ($this->isWeekendTheSame) {
				switch ($this->isWeekendTheSame) {
					case 'fullday':
						$timetable["Сб - Вс"] = "Круглосуточно";
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, ["00:00 - 24:00" => [5=>true, 6=>true]]);
						break;
					case 'work':
						$Hours = $this->SaturdayStart . " - " . $this->SaturdayFinish;
						$timetable["Сб - Вс"] = $Hours;
						$daysOfWeekMask = array_merge_recursive($daysOfWeekMask, [$Hours => [5=>true, 6=>true]]);
						break;
					case 'relax':
						$timetable["Сб - Вс"] = "Выходной";
						break;
				}
			}
			foreach (array('Пн'=>'Monday', 'Вт'=>'Tuesday', 'Ср'=>'Wednesday', 'Чт'=>'Thursday', 'Пт'=>'Friday', 'Сб'=>'Saturday', 'Вс'=>'Sunday') as $key => $day) {
				if(isset($timetable["Пн - Вс"])) break;
				if(isset($timetable["Пн - Пт"]) && in_array($day, ['Monday','Tuesday','Wednesday','Thursday','Friday'])) continue;
				if(isset($timetable["Сб - Вс"]) && in_array($day, ['Saturday','Sunday'])) continue;
				if (!empty($this->{$day . Start})) {
					if ($this->{$day . Start} == '00:00' && $this->{$day . Finish} == '00:00')
						$timetable[$key] = "Выходной";
					else {
						if (($this->{$day . Start} == '11111' || $this->{$day . Finish} == '11111') || ($this->{$day . Start} == '00:00' && $this->{$day . Finish} == '23:59')) {
							$timetable[$key] = "Круглосуточно";
							$Hours = "00:00 - 24:00";
						}
						else {
							$Hours = $this->{$day . Start} . " - " . $this->{$day . Finish};
							$timetable[$key] = $Hours;
						}
						switch ($key) {
							case 'Пн':
								$daysOfWeekMask[$Hours][0] = true;
								break;
							case 'Вт':
								$daysOfWeekMask[$Hours][1] = true;
								break;
							case 'Ср':
								$daysOfWeekMask[$Hours][2] = true;
								break;
							case 'Чт':
								$daysOfWeekMask[$Hours][3] = true;
								break;
							case 'Пт':
								$daysOfWeekMask[$Hours][4] = true;
								break;
							case 'Сб':
								$daysOfWeekMask[$Hours][5] = true;
								break;
							case 'Вс':
								$daysOfWeekMask[$Hours][6] = true;
								break;
						}
					}
				}
			}
		}
			
		if(count($daysOfWeekMask) == 0) {
			$daysOfWeekMask["08:00 - 22:00"] = [0 => true, 1 => true, 2 => true, 3 => true, 4 => true, 5 => true, 6 => true];
			$timetable["Пн - Вс"] = "08:00 - 22:00";
		}

		return ["timetable" => $timetable, "daysOfWeekMask" => $daysOfWeekMask];
	}
}
