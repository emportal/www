<?php

/**
 * This is the model class for table "unit".
 *
 * The followings are the available columns in table 'unit':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $nomenclatureId
 * @property string $classifierUnitId
 * @property integer $coefficient
 * @property integer $weight
 * @property integer $volume
 * @property string $barcode
 * @property string $specification
 *
 * The followings are the available model relations:
 * @property Nomenclature[] $nomenclatures
 * @property ClassifierUnit $classifierUnit
 * @property Nomenclature $nomenclature
 */
class Unit extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Unit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('coefficient, weight, volume', 'numerical', 'integerOnly'=>true),
			array('id, nomenclatureId, classifierUnitId', 'length', 'max'=>36),
			array('name, link, barcode, specification', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, nomenclatureId, classifierUnitId, coefficient, weight, volume, barcode, specification', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nomenclatures' => array(self::HAS_MANY, 'Nomenclature', 'mainUnitId'),
			'classifierUnit' => array(self::BELONGS_TO, 'ClassifierUnit', 'classifierUnitId'),
			'nomenclature' => array(self::BELONGS_TO, 'Nomenclature', 'nomenclatureId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'nomenclatureId' => 'Nomenclature',
			'classifierUnitId' => 'Classifier Unit',
			'coefficient' => 'Coefficient',
			'weight' => 'Weight',
			'volume' => 'Volume',
			'barcode' => 'Barcode',
			'specification' => 'Specification',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('nomenclatureId',$this->nomenclatureId,true);
		$criteria->compare('classifierUnitId',$this->classifierUnitId,true);
		$criteria->compare('coefficient',$this->coefficient);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('volume',$this->volume);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('specification',$this->specification,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}