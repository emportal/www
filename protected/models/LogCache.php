<?php

/**
 * This is the model class for table "logCache".
 *
 * The followings are the available columns in table 'logCache':
 * @property integer $id
 * @property string $dateDMY
 * @property string $companyId
 * @property string $chartLabels
 * @property string $chartData
 */
class LogCache extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogCache the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logCache';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dateDMY, companyId, chartLabels, chartData', 'required'),
			array('dateDMY', 'length', 'max'=>8),
			array('companyId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dateDMY, companyId, chartLabels, chartData', 'safe', 'on'=>'search'),
			array('id, dateDMY, companyId, chartLabels, chartData', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dateDMY' => 'Date Dmy',
			'companyId' => 'Company',
			'chartLabels' => 'Chart Labels',
			'chartData' => 'Chart Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dateDMY',$this->dateDMY,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('chartLabels',$this->chartLabels,true);
		$criteria->compare('chartData',$this->chartData,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}