<?php

/**
 * This is the model class for table "raitingCompanyMenegers".
 *
 * The followings are the available columns in table 'raitingCompanyMenegers':
 * @property string $id
 * @property string $user1CId
 * @property string $companyId
 * @property string $addressId
 * 
 * @property RaitingCompanyMenegersValue $ratingCompanyManagerValues
 */
class RaitingCompanyMenegers extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RaitingCompanyMenegers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'raitingCompanyMenegers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, user1CId, companyId, addressId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user1CId, companyId, addressId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ratingCompanyManagerValues' => array(self::HAS_MANY,'RaitingCompanyMenegersValue','raitingCompanyMenegersId')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user1CId' => 'User1 Cid',
			'companyId' => 'Company',
			'addressId' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user1CId',$this->user1CId,true);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('addressId',$this->addressId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}