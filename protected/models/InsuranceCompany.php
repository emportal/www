<?php

/**
 * This is the model class for table "insuranceCompany".
 *
 * The followings are the available columns in table 'insuranceCompany':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $description
 * @property integer $isOMS
 * @property integer $isDMS
 *
 * The followings are the available model relations:
 * @property InsuranceCompanyInsuranceType[] $insuranceCompanyInsuranceTypes
 * @property InsuranceCompanyLicense[] $insuranceCompanyLicenses
 */
class InsuranceCompany extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InsuranceCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'insuranceCompany';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isOMS, isDMS', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>36),
			array('name, link, description', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, description, isOMS, isDMS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'insuranceCompanyInsuranceTypes' => array(self::HAS_MANY, 'InsuranceCompanyInsuranceType', 'insuranceCompanyId'),
			'insuranceCompanyLicenses' => array(self::HAS_MANY, 'InsuranceCompanyLicense', 'insuranceCompanyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'description' => 'Description',
			'isOMS' => 'Is Oms',
			'isDMS' => 'Is Dms',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('isOMS',$this->isOMS);
		$criteria->compare('isDMS',$this->isDMS);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}