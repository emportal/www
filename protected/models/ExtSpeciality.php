<?php

/**
 * This is the model class for table "extSpeciality".
 *
 * The followings are the available columns in table 'extSpeciality':
 * @property string $extId
 * @property integer $extAddressId
 * @property string $extName
 * @property integer $sysTypeId
 * @property string $lastUpdate
 * @property string $specialtyId
 *
 * The followings are the available model relations:
 * @property ExtDoctor[] $extDoctors
 * @property ExtDoctor[] $extDoctors1
 * @property ExtDoctor[] $extDoctors2
 * @property ExtAddress $extAddress
 * @property ExtAddress $sysType
 */
class ExtSpeciality extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExtSpeciality the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'extSpeciality';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('extId, extAddressId, extName, sysTypeId', 'required'),
			array('extAddressId, sysTypeId', 'numerical', 'integerOnly'=>true),
			array('extId', 'length', 'max'=>50),
			//array('specialtyId', 'length', 'max'=>36),
			array('extId, extAddressId, extName, sysTypeId, lastUpdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('extId, extAddressId, extName, sysTypeId, lastUpdate, specialtyId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'extDoctors' => array(self::HAS_MANY, 'ExtDoctor', 'extAddressId'),
			'extDoctors1' => array(self::HAS_MANY, 'ExtDoctor', 'sysTypeId'),
			'extDoctors2' => array(self::HAS_MANY, 'ExtDoctor', 'extSpecialityId'),
			'extAddress' => array(self::BELONGS_TO, 'ExtAddress', 'extAddressId'),
			'sysType' => array(self::BELONGS_TO, 'ExtAddress', 'sysTypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'extId' => 'Ext',
			'extAddressId' => 'Ext Address',
			'extName' => 'Ext Name',
			'sysTypeId' => 'Sys Type',
			'lastUpdate' => 'Last Update',
			'specialtyId' => 'Specialty',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('extId',$this->extId,true);
		$criteria->compare('extAddressId',$this->extAddressId);
		$criteria->compare('extName',$this->extName,true);
		$criteria->compare('sysTypeId',$this->sysTypeId);
		$criteria->compare('lastUpdate',$this->lastUpdate,true);
		$criteria->compare('specialtyId',$this->specialtyId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}