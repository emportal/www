<?php

/**
 * This is the model class for table "shortCompaniesOfDoctor".
 *
 * The followings are the available columns in table 'shortCompaniesOfDoctor':
 * @property string $companyId
 * @property string $cityId
 * @property string $companyName
 * @property string $companyLinkUrl
 * @property integer $samozapis
 *
 * The followings are the available model relations:
 * @property Company $company
 */
class ShortCompaniesOfDoctor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ShortCompaniesOfDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shortCompaniesOfDoctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('companyId, cityId', 'required'),
			array('samozapis', 'numerical', 'integerOnly'=>true),
			array('companyId, cityId', 'length', 'max'=>36),
			array('companyName, companyLinkUrl', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('companyId, cityId, companyName, companyLinkUrl, samozapis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'companyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'companyId' => 'Company',
			'cityId' => 'City',
			'companyName' => 'Company Name',
			'companyLinkUrl' => 'Company Link Url',
			'samozapis' => 'Samozapis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('cityId',$this->cityId,true);
		$criteria->compare('companyName',$this->companyName,true);
		$criteria->compare('companyLinkUrl',$this->companyLinkUrl,true);
		$criteria->compare('samozapis',$this->samozapis);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getCompanyList() {
		$companyListCriteria = new CDbCriteria;
		$companyListCriteria->select = "t.companyLinkUrl, t.companyName"; //denormCitySubdomain
		$companyListCriteria->order = "t.companyName";
		$companyListCriteria->compare("samozapis",(Yii::app()->params["samozapis"]) ? 1 : 0);
		$companyListCriteria->compare("cityId",City::model()->selectedCityId);
		$companyList = CHtml::listData(ShortCompaniesOfDoctor::model()->findAll($companyListCriteria), 'companyLinkUrl', 'companyName');
		return $companyList;
	}
}