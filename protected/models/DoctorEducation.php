<?php

/**
 * This is the model class for table "doctorEducation".
 *
 * The followings are the available columns in table 'doctorEducation':
 * @property string $id
 * @property string $doctorId
 * @property string $medicalSchoolId
 * @property string $typeOfEducationId
 * @property integer $yearOfStady
 *
 * The followings are the available model relations:
 * @property TypeOfEducation $typeOfEducation
 * @property Doctor $doctor
 * @property MedicalSchool $medicalSchool
 */
class DoctorEducation extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DoctorEducation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'doctorEducation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctorId', 'required'),
			array('yearOfStady', 'numerical', 'integerOnly'=>true),
			array('id, doctorId, medicalSchoolId, typeOfEducationId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, doctorId, medicalSchoolId, typeOfEducationId, yearOfStady', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'typeOfEducation' => array(self::BELONGS_TO, 'TypeOfEducation', 'typeOfEducationId'),
			'doctor' => array(self::BELONGS_TO, 'Doctor', 'doctorId'),
			'medicalSchool' => array(self::BELONGS_TO, 'MedicalSchool', 'medicalSchoolId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctorId' => 'Doctor',
			'medicalSchoolId' => 'Medical School',
			'typeOfEducationId' => 'Type Of Education',
			'yearOfStady' => 'Year Of Stady',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('medicalSchoolId',$this->medicalSchoolId,true);
		$criteria->compare('typeOfEducationId',$this->typeOfEducationId,true);
		$criteria->compare('yearOfStady',$this->yearOfStady);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}