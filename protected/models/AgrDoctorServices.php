<?php

/**
 * This is the model class for table "agr_doctorServices".
 *
 * The followings are the available columns in table 'agr_doctorServices':
 * @property string $id
 * @property string $agr_doctorId
 * @property string $cassifierServiceId
 * @property integer $price
 */
class AgrDoctorServices extends AgrActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgrDoctorServices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agr_doctorServices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('price,free', 'numerical', 'integerOnly'=>true),
			array('id, doctorId, cassifierServiceId', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, doctorId, cassifierServiceId, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cassifierService' => array(self::BELONGS_TO, 'Service', 'cassifierServiceId'),
			'doctor' => array(self::BELONGS_TO, 'AgrDoctor', 'doctorId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agr_doctorId' => 'Agr Doctor',
			'cassifierServiceId' => 'Cassifier Service',
			'price' => 'Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('agr_doctorId',$this->agr_doctorId,true);
		$criteria->compare('cassifierServiceId',$this->cassifierServiceId,true);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}