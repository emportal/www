<?php

/**
 * This is the model class for table "contactInformation".
 *
 * The followings are the available columns in table 'contactInformation':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $ownerId
 * @property string $OwnerName
 * @property string $contactInformationTypeId
 * @property string $kinfOfcontactInfoId
 * @property string $presentation
 * @property string $cityCode
 * @property string $countryCode
 * @property integer $forAdress
 * @property string $addressesId
 *
 * The followings are the available model relations:
 * @property Address $addresses
 * @property ContactInformationType $contactInformationType
 * @property KindOfcontactInfo $kinfOfcontactInfo
 */
class ContactInformation extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ContactInformation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contactInformation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('forAdress', 'numerical', 'integerOnly'=>true),
			array('id, contactInformationTypeId, kinfOfcontactInfoId, addressesId', 'length', 'max'=>36),
			array('name, link, ownerId, OwnerName, presentation, cityCode, countryCode', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, ownerId, OwnerName, contactInformationTypeId, kinfOfcontactInfoId, presentation, cityCode, countryCode, forAdress, addressesId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::BELONGS_TO, 'Address', 'addressesId'),
			'contactInformationType' => array(self::BELONGS_TO, 'ContactInformationType', 'contactInformationTypeId'),
			'kinfOfcontactInfo' => array(self::BELONGS_TO, 'KindOfcontactInfo', 'kinfOfcontactInfoId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'ownerId' => 'Owner',
			'OwnerName' => 'Owner Name',
			'contactInformationTypeId' => 'Contact Information Type',
			'kinfOfcontactInfoId' => 'Kinf Ofcontact Info',
			'presentation' => 'Presentation',
			'cityCode' => 'City Code',
			'countryCode' => 'Country Code',
			'forAdress' => 'For Adress',
			'addressesId' => 'Addresses',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('ownerId',$this->ownerId,true);
		$criteria->compare('OwnerName',$this->OwnerName,true);
		$criteria->compare('contactInformationTypeId',$this->contactInformationTypeId,true);
		$criteria->compare('kinfOfcontactInfoId',$this->kinfOfcontactInfoId,true);
		$criteria->compare('presentation',$this->presentation,true);
		$criteria->compare('cityCode',$this->cityCode,true);
		$criteria->compare('countryCode',$this->countryCode,true);
		$criteria->compare('forAdress',$this->forAdress);
		$criteria->compare('addressesId',$this->addressesId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}