<?php

/**
 * This is the model class for table "companyType".
 *
 * The followings are the available columns in table 'companyType':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $categoryId
 * @property integer $isPharmacy
 * @property integer $isMedical
 * @property integer $isInsurance
 * @property integer $isSupplier
 * @property integer $isLab
 * @property integer $isDiagnostics
 * @property integer $existDoctors
 * @property integer $existLiicenses
 * @property integer $existPayService
 * @property integer $existOMSDMS
 * @property integer $existNomenclatureGroup
 * @property integer $existSoftWare
 * @property integer $existCompanyActivites
 * @property integer $existEquipment
 * @property integer $existPrice
 * @property integer $existServices
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 * @property Category $category
 */
class CompanyType extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanyType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companyType';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isPharmacy, isMedical, isInsurance, isSupplier, isLab, isDiagnostics, existDoctors, existLiicenses, existPayService, existOMSDMS, existNomenclatureGroup, existSoftWare, existCompanyActivites, existEquipment, existPrice, existServices', 'numerical', 'integerOnly'=>true),
			array('id, categoryId', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, categoryId, isPharmacy, isMedical, isInsurance, isSupplier, isLab, isDiagnostics, existDoctors, existLiicenses, existPayService, existOMSDMS, existNomenclatureGroup, existSoftWare, existCompanyActivites, existEquipment, existPrice, existServices', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'companyTypeId'),
			'category' => array(self::BELONGS_TO, 'Category', 'categoryId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'categoryId' => 'Category',
			'isPharmacy' => 'Is Pharmacy',
			'isMedical' => 'Is Medical',
			'isInsurance' => 'Is Insurance',
			'isSupplier' => 'Is Supplier',
			'isLab' => 'Is Lab',
			'isDiagnostics' => 'Is Diagnostics',
			'existDoctors' => 'Exist Doctors',
			'existLiicenses' => 'Exist Liicenses',
			'existPayService' => 'Exist Pay Service',
			'existOMSDMS' => 'Exist Omsdms',
			'existNomenclatureGroup' => 'Exist Nomenclature Group',
			'existSoftWare' => 'Exist Soft Ware',
			'existCompanyActivites' => 'Exist Company Activites',
			'existEquipment' => 'Exist Equipment',
			'existPrice' => 'Exist Price',
			'existServices' => 'Exist Services',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('categoryId',$this->categoryId,true);
		$criteria->compare('isPharmacy',$this->isPharmacy);
		$criteria->compare('isMedical',$this->isMedical);
		$criteria->compare('isInsurance',$this->isInsurance);
		$criteria->compare('isSupplier',$this->isSupplier);
		$criteria->compare('isLab',$this->isLab);
		$criteria->compare('isDiagnostics',$this->isDiagnostics);
		$criteria->compare('existDoctors',$this->existDoctors);
		$criteria->compare('existLiicenses',$this->existLiicenses);
		$criteria->compare('existPayService',$this->existPayService);
		$criteria->compare('existOMSDMS',$this->existOMSDMS);
		$criteria->compare('existNomenclatureGroup',$this->existNomenclatureGroup);
		$criteria->compare('existSoftWare',$this->existSoftWare);
		$criteria->compare('existCompanyActivites',$this->existCompanyActivites);
		$criteria->compare('existEquipment',$this->existEquipment);
		$criteria->compare('existPrice',$this->existPrice);
		$criteria->compare('existServices',$this->existServices);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}