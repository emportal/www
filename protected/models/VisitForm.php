<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class VisitForm extends CFormModel
{
	public $clinicLink;
	public $doctorLink;
	public $serviceLink;
	public $name;
	public $phone;
	public $email;



	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('clinicLink', 'required'),
			array('serviceLink', 'required', 'on'=>'clinic'),
			array('serviceLink, doctorLink', 'required', 'on'=>'doctor'),
			array('name', 'default', 'setOnEmpty'=>true,'value'=>  Yii::app()->user->model->name),
			array('phone', 'default', 'setOnEmpty'=>true,'value'=> Yii::app()->user->model->name),
			array('email', 'default', 'setOnEmpty'=>true,'value'=> Yii::app()->user->model->name),

		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Запомнить',
			'username'=>'Логин',
			'password'=>'Пароль',
		);
	}
}
