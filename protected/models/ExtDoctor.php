<?php

/**
 * This is the model class for table "extDoctor".
 *
 * The followings are the available columns in table 'extDoctor':
 * @property string $doctorId
 * @property string $extId
 * @property string $extSpecialityId
 * @property integer $extAddressId
 * @property string $extFio
 * @property integer $sysTypeId
 * @property string $lastUpdate
 * @property integer $deleted
 * @property string $parameters
 *
 * The followings are the available model relations:
 * @property ExtSpeciality $extAddress
 * @property ExtSpeciality $sysType
 * @property ExtSpeciality $extSpeciality
 */
class ExtDoctor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExtDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'extDoctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('extId, extAddressId, extFio, sysTypeId', 'required'),
			array('extAddressId, sysTypeId, deleted', 'numerical', 'integerOnly'=>true),
			array('doctorId', 'length', 'max'=>36),
			array('extId, extSpecialityId', 'length', 'max'=>50),
			array('extFio', 'length', 'max'=>64),
			array('lastUpdate, parameters', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('doctorId, extId, extSpecialityId, extAddressId, extFio, sysTypeId, lastUpdate, deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'extAddress' => array(self::BELONGS_TO, 'ExtAddress', 'extAddressId'),
			'sysType' => array(self::BELONGS_TO, 'ExtSysType', 'sysTypeId'),
			'extSpeciality' => array(self::BELONGS_TO, 'ExtSpeciality', 'extSpecialityId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'doctorId' => 'Doctor',
			'extId' => 'Ext',
			'extSpecialityId' => 'Ext Speciality',
			'extAddressId' => 'Ext Address',
			'extFio' => 'Ext Fio',
			'sysTypeId' => 'Sys Type',
			'lastUpdate' => 'Last Update',
			'deleted' => 'Deleted',
			'parameters' => 'Parameters',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('doctorId',$this->doctorId,true);
		$criteria->compare('extId',$this->extId,true);
		$criteria->compare('extSpecialityId',$this->extSpecialityId,true);
		$criteria->compare('extAddressId',$this->extAddressId);
		$criteria->compare('extFio',$this->extFio,true);
		$criteria->compare('sysTypeId',$this->sysTypeId);
		$criteria->compare('lastUpdate',$this->lastUpdate,true);
		$criteria->compare('deleted',$this->deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getParameters() {
		return json_decode($this->parameters);
	}

	public static function checkDoctor($doctorId, $clinicId, $sysTypeId=NULL) {
		$criteria = new CDbCriteria();
		$criteria->compare("t.doctorId",$doctorId);
		if($sysTypeId !== NULL) {
			$criteria->compare("t.sysTypeId",$sysTypeId);
		}
		$criteria->with = [
				'extAddress' => [
						'together' => true,
						'select' => false,
						'joinType' => 'INNER JOIN',
						'condition' => 'extAddress.addressId = '.Yii::app()->db->quoteValue($clinicId),
				]
		];
		$extDoctor = ExtDoctor::model()->find($criteria);
		return $extDoctor;
	}
}