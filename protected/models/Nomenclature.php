<?php

/**
 * This is the model class for table "nomenclature".
 *
 * The followings are the available columns in table 'nomenclature':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $groupId
 * @property string $mainUnitId
 * @property string $baseUnitId
 * @property string $baseUnitClassifierId
 * @property string $classifierCountryId
 * @property integer $severalUnit
 * @property string $description
 * @property integer $isImport
 * @property integer $minResidue
 * @property string $fullName
 *
 * The followings are the available model relations:
 * @property Country $classifierCountry
 * @property ClassifierUnit $baseUnitClassifier
 * @property NomenclatureGroup $group
 * @property Unit $mainUnit
 * @property ProducerNomenclatureGroup[] $producerNomenclatureGroups
 * @property Unit[] $units
 */
class Nomenclature extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Nomenclature the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nomenclature';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('severalUnit, isImport, minResidue', 'numerical', 'integerOnly'=>true),
			array('id, groupId, mainUnitId, baseUnitId, baseUnitClassifierId, classifierCountryId', 'length', 'max'=>36),
			array('name, link, description, fullName', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, groupId, mainUnitId, baseUnitId, baseUnitClassifierId, classifierCountryId, severalUnit, description, isImport, minResidue, fullName', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'classifierCountry' => array(self::BELONGS_TO, 'Country', 'classifierCountryId'),
			'baseUnitClassifier' => array(self::BELONGS_TO, 'ClassifierUnit', 'baseUnitClassifierId'),
			'group' => array(self::BELONGS_TO, 'NomenclatureGroup', 'groupId'),
			'mainUnit' => array(self::BELONGS_TO, 'Unit', 'mainUnitId'),
			'producerNomenclatureGroups' => array(self::HAS_MANY, 'ProducerNomenclatureGroup', 'nomenclatureId'),
			'units' => array(self::HAS_MANY, 'Unit', 'nomenclatureId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'groupId' => 'Group',
			'mainUnitId' => 'Main Unit',
			'baseUnitId' => 'Base Unit',
			'baseUnitClassifierId' => 'Base Unit Classifier',
			'classifierCountryId' => 'Classifier Country',
			'severalUnit' => 'Several Unit',
			'description' => 'Description',
			'isImport' => 'Is Import',
			'minResidue' => 'Min Residue',
			'fullName' => 'Full Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('groupId',$this->groupId,true);
		$criteria->compare('mainUnitId',$this->mainUnitId,true);
		$criteria->compare('baseUnitId',$this->baseUnitId,true);
		$criteria->compare('baseUnitClassifierId',$this->baseUnitClassifierId,true);
		$criteria->compare('classifierCountryId',$this->classifierCountryId,true);
		$criteria->compare('severalUnit',$this->severalUnit);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('isImport',$this->isImport);
		$criteria->compare('minResidue',$this->minResidue);
		$criteria->compare('fullName',$this->fullName,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}