<?php

/**
 * OMSForm class.
 * OMSForm is the data structure for keeping
 * user info for OMS personal data validation
 */
class OMSForm extends CFormModel
{
	public $surName;
	public $firstName;
	public $fatherName;
	public $birthday;
	public $extAddressId;
	public $patientId;
	public $omsNumber;
	public $omsSeries;
	public $referralId;
	public $exception;
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('surName, firstName, birthday', 'required'),
			array('surName, firstName, birthday, omsNumber', 'required', 'on'=>'emias'),
			array('surName, firstName, fatherName, birthday, extAddressId', 'safe'),
			array('exception', 'checkOMSData'),
		);
	}
	
	public function init() {
		if(empty($this->scenario)) {
			if(City::model()->getSelectedCityId() === City::MOSKVA) {
				$this->scenario = 'emias';
			}
		}
	}
	
	public function initOMSForm() {
		$cookies = &Yii::app()->request->cookies;
		
		$this->surName = trim(strval($this->surName));
		$this->firstName = trim(strval($this->firstName));
		$this->fatherName = trim(strval($this->fatherName));
		$this->omsNumber = trim(strval($this->omsNumber));
		$this->omsSeries = trim(strval($this->omsSeries));
		$this->birthday = trim(strval($this->birthday));
		$this->referralId = trim(strval(@$cookies['OMSForm_referralId']));
		
		$FIO = MyTools::FIOExplode(Yii::app()->user->model->name);
		if($this->surName === "") {
			if(Yii::app()->user->isGuest || $cookies['OMSForm_change']) {
				if (isset($cookies['OMSForm_surName'])) {
					$this->surName = trim(strval($cookies['OMSForm_surName']->value));
				}
			} else {
				$this->surName = trim(strval(@$FIO['surName']));
			}
		}
		if($this->firstName === "") {
			if(Yii::app()->user->isGuest || $cookies['OMSForm_change']) {
				if (isset($cookies['OMSForm_firstName'])) {
					$this->firstName = trim(strval($cookies['OMSForm_firstName']->value));
				}
			} else {
				$this->firstName = trim(strval(@$FIO['firstName']));
			}
		}
		if($this->fatherName === "") {
			if(Yii::app()->user->isGuest || $cookies['OMSForm_change']) {
				if (isset($cookies['OMSForm_fatherName'])) {
					$this->fatherName = trim(strval($cookies['OMSForm_fatherName']->value));
				}
			} else {
				$this->fatherName = trim(strval(@$FIO['fatherName']));
			}
		}
		if($this->birthday === "") {
			try {
				if(Yii::app()->user->isGuest || $cookies['OMSForm_change']) {
					$birthday_fromCookies = trim(strval($cookies['OMSForm_birthday']->value));
					if($birthday_fromCookies !== '') {
						$this->birthday = (new DateTime($birthday_fromCookies))->format( 'd-m-Y' );
					}
				} else{
					if(trim(strval(Yii::app()->user->model->birthday)) !== "") {
						$this->birthday = (new DateTime(Yii::app()->user->model->birthday))->format( 'd-m-Y' );
					}
				}
			} catch (Exception $e) { }
		}
		if($this->omsSeries === "") {
			if(Yii::app()->user->isGuest || $cookies['OMSForm_change']) {
				if(isset($cookies['OMSForm_omsSeries'])) {
					$this->omsSeries = trim(strval($cookies['OMSForm_omsSeries']->value));
				}
			} else {
				$this->omsSeries = trim(strval(Yii::app()->user->model->OMSSeries));
			}
		}
		if($this->omsNumber === "") {
			if(Yii::app()->user->isGuest || $cookies['OMSForm_change']) {
				if(isset($cookies['OMSForm_omsNumber'])) {
					$this->omsNumber = trim(strval($cookies['OMSForm_omsNumber']->value));
				}
			} else {
				$this->omsNumber = trim(strval(Yii::app()->user->model->OMSNumber));
			}
		}
		
		$this->saveOMSForm();
	}
	
	public function saveOMSForm() {
		$cookies = &Yii::app()->request->cookies;

		if($cookies['OMSForm_surName'] !== $this->surName) {
			$cookies['OMSForm_surName'] = new CHttpCookie('OMSForm_surName', $this->surName, ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
		if($cookies['OMSForm_firstName'] !== $this->firstName) {
			$cookies['OMSForm_firstName'] = new CHttpCookie('OMSForm_firstName', $this->firstName, ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
		if($cookies['OMSForm_fatherName'] !== $this->fatherName) {
			$cookies['OMSForm_fatherName'] = new CHttpCookie('OMSForm_fatherName', $this->fatherName, ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
		try {
			if($cookies['OMSForm_birthday'] !== $this->birthday) {
				$cookies['OMSForm_birthday'] = new CHttpCookie('OMSForm_birthday', (new DateTime($this->birthday))->format( 'd.m.Y' ), ['path'=>'/', 'domain'=>'.emportal.ru']);
			}
		} catch (Exception $e) { }
		if($cookies['OMSForm_omsNumber'] !== $this->omsNumber) {
			$cookies['OMSForm_omsNumber'] = new CHttpCookie('OMSForm_omsNumber', $this->omsNumber, ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
		if($cookies['OMSForm_omsSeries'] !== $this->omsSeries) {
			$cookies['OMSForm_omsSeries'] = new CHttpCookie('OMSForm_omsSeries', $this->omsSeries, ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
		if($cookies['OMSForm_change']) {
			$cookies['OMSForm_change'] = new CHttpCookie('OMSForm_change', false, ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
		if(!Yii::app()->user->isGuest) {
			$update = false;
			$name = $this->surName . " " . $this->firstName . " " . $this->fatherName;
			if(Yii::app()->user->model->name !== $name) {
				Yii::app()->user->model->name = $name;
				$update = true;
			}
			try {
				if((new DateTime(Yii::app()->user->model->birthday))->format( 'd.m.Y' ) !== $this->birthday) {
					Yii::app()->user->model->birthday = (new DateTime($this->birthday))->format( 'Y-m-d' );
					$update = true;
				}
			} catch (Exception $e) {
				Yii::app()->user->model->birthday = "";
				$update = true;
			}
			if(Yii::app()->user->model->OMSNumber !== $this->omsNumber) {
				Yii::app()->user->model->OMSNumber = $this->omsNumber;
				$update = true;
			}
			if(Yii::app()->user->model->OMSSeries !== $this->omsSeries) {
				Yii::app()->user->model->OMSSeries = $this->omsSeries;
				$update = true;
			}
			if($update) {
				Yii::app()->user->model->update();
			}
		}
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'surName'=>'Фамилия',
			'firstName'=>'Имя',
			'fatherName'=>'Отчество',
			'birthday'=>'Дата рождения',
			'patientId' => 'id пациента из ИС Нетрика',
			'omsNumber' => 'Номер полиса ОМС',
			'omsSeries' => 'Серия полиса ОМС',
		);
	}
	
	
	public function checkOMSData($attribute)
	{
		if(City::model()->getSelectedCityId() === City::MOSKVA) {
			
		} else {
			if(trim(strval($this->extAddressId)) === "") {
				$message = 'Необходимо заполнить поле extAddressId.';
				$this->addError($attribute, $message);
				return false;
			}
				
			if (!$this->surName OR !$this->firstName OR !$this->fatherName) {
				$this->addError($attribute, 'Не указаны обязательные данные (ФИО, дата рождения)');
				return false;
			}
			
			$loader = new NetrikaLoader();
			$birthday_arr = explode(".", $this->birthday);
			if(count($birthday_arr) < 3) {
				$birthday_arr = explode("-", $this->birthday);
			}
			if(count($birthday_arr) === 3) {
				if(mb_strlen($birthday_arr[0]) < 4) {
					$birthday_arr = array_reverse($birthday_arr);
				}
			}
			$birthday_str = implode("-", $birthday_arr)  . 'T00:00:00';
			$pat = [
				'Birthday' => $birthday_str, 
				'Surname' => $this->surName, 
				'Name' => $this->firstName, 
				'SecondName' => $this->fatherName
			];
			//$pat = ['Birthday' => '1972-11-29T00:00:00', 'Surname' => 'Король', 'Name' => 'Андрей', 'SecondName' => 'Максимович']; //rm test
			$checkResult = $loader->checkPatient($this->extAddressId, $pat); /*$this->extAddressId*/
			$patientId = (isset($checkResult->Success) && $checkResult->Success) ? $checkResult->IdPat : false;
			
			if (!$patientId) {
				$error = false;
				if(isset($checkResult->ErrorList)) {
					if(is_array($checkResult->ErrorList) && isset($checkResult->ErrorList[0])) {
						$error = $checkResult->ErrorList[0];
					}
					elseif (is_object($checkResult->ErrorList) && isset($checkResult->ErrorList->Error)) {
						$error = $checkResult->ErrorList->Error;
					}
				}
				if($error && isset($error->IdError) && isset($error->ErrorDescription)) {
					$message = MyTools::createFromTemplate("Ошибка %/Error/IdError%: %Error/ErrorDescription%. Для успешной записи следует явиться в закрепленное за вами медицинское учреждение и зарегистрироваться в очном порядке.",
							["%/Error/IdError%" => $error->IdError, "%Error/ErrorDescription%" => $error->ErrorDescription]);
					$this->addError($attribute, $message.$birthday_str);
				} else {
					$message = 'Запись в данную клинику невозможна. Рекомендуется проверить корректность данных пациента.';
					$this->addError($attribute, $message.$birthday_str);
				}
				return false;
			} else {
				$this->patientId = $patientId;
				return true;
			}
		}
	}

}
