<?php

/**
 * This is the model class for table "extCompany".
 *
 * The followings are the available columns in table 'extCompany':
 * @property integer $extId
 * @property integer $sysTypeId
 * @property string $companyId
 * @property string $lastUpdate
 * @property string $name
 *
 * The followings are the available model relations:
 * @property ExtAddress[] $extAddresses
 * @property ExtAddress[] $extAddresses1
 * @property ExtSysType $sysType
 */
class ExtCompany extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExtCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'extCompany';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('extId, sysTypeId', 'required'),
			array('extId, sysTypeId', 'numerical', 'integerOnly'=>true),
			array('companyId', 'length', 'max'=>36),
			array('name', 'length', 'max'=>500),
			array('lastUpdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('extId, sysTypeId, companyId, lastUpdate, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'extAddresses' => array(self::HAS_MANY, 'ExtAddress', 'extCompanyId'),
			'extAddresses1' => array(self::HAS_MANY, 'ExtAddress', 'sysTypeId'),
			'sysType' => array(self::BELONGS_TO, 'ExtSysType', 'sysTypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'extId' => 'Ext',
			'sysTypeId' => 'Sys Type',
			'companyId' => 'Company',
			'lastUpdate' => 'Last Update',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('extId',$this->extId);
		$criteria->compare('sysTypeId',$this->sysTypeId);
		$criteria->compare('companyId',$this->companyId,true);
		$criteria->compare('lastUpdate',$this->lastUpdate,true);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}