<?php
/*
ALTER TABLE `payment`
	ADD COLUMN `billId` INT NULL DEFAULT NULL AFTER `addressId`,
	ADD CONSTRAINT `payment_billId` FOREIGN KEY (`billId`) REFERENCES `bill` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
	
ALTER TABLE `payment`
	CHANGE COLUMN `createDate` `createDate` TIMESTAMP NULL DEFAULT NULL AFTER `id`;

ALTER TABLE `payment`
	ADD COLUMN `addressId` CHAR(36) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `createDate`,
	ADD CONSTRAINT `payment_addressId` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
	
ALTER TABLE `payment`
	ADD COLUMN `statusId` INT(11) NULL DEFAULT '0' AFTER `logId`;
	
ALTER TABLE `payment`
	ADD COLUMN `sourceId` CHAR(36) NULL DEFAULT NULL COMMENT 'Источник платежа' COLLATE 'utf8_unicode_ci' AFTER `statusId`;
 */
/**
 * This is the model class for table "payment".
 *
 * The followings are the available columns in table 'payment':
 * @property string $id
 * @property string $createDate
 * @property string $addressId
 * @property integer $billId
 * @property string $sum
 * @property string $purpose
 * @property string $logId
 * @property integer $statusId
 * @property string $sourceId
 */
class Payment extends CActiveRecord
{
	const STATUS_NOT_PROCESSED = 0;
	const STATUS_REJECTED = -1;
	const STATUS_APPROVED = 1;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Payment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('createDate, addressId, sum, purpose', 'required'),
			array('addressId, sourceId', 'length', 'max'=>36),
			array('sum, logId, billId, statusId', 'length', 'max'=>11),
			array('purpose', 'length', 'max'=>180),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, addressId, billId, sum, purpose, statusId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'log1CPayment' => array(self::BELONGS_TO, 'Log1CPayment', 'logId'),
            'bill' => array(self::BELONGS_TO, 'Bill', 'billId'),
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'createDate' => 'Дата',
			'addressId' => 'Клиника',
			'billId' => 'Счёт',
			'sum' => 'Сумма',
			'purpose' => 'Назначение',
			'logId' => 'Log',
		);
	}

	public function beforeSave() {
		if (parent::beforeSave()) {
			if($this->isNewRecord) {
				if(empty($this->createDate)) {
					$this->createDate = date("Y-m-d H:i:s", time());
				}
			}
			return true;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($attr=[])
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = [
			'address' => [
				'together' => true,
				'with' => [
					'company' => [
						'together' => true,
						'with' => [
							'managerRelations' => [
								'together' => true,
								'select' => '(`managerRelations`.`userId` != 1) AS id,userId'
							],
						]
					],
				],
			],
		];

		if(!empty($this->id)) $criteria->compare('id',$this->id);
		if(!empty($this->createDate)) $criteria->compare('createDate',$this->createDate,true);
		if(!empty($this->sum)) $criteria->compare('sum',$this->sum);
		if(!empty($this->purpose)) $criteria->compare('purpose',$this->purpose,true);
		if(!empty($this->logId)) $criteria->compare('logId',$this->logId);
		
		if (!empty($attr['managerId']))
		{
			if ($attr['managerId'] == -1) {				
				$criteria->addCondition('managerRelations.userId IS NULL');
			} elseif ($attr['managerId'] == -2) {				
				$criteria->addCondition('managerRelations.userId IS NOT NULL');
			} else {
				$criteria->compare('managerRelations.userId',$attr['managerId']);
			}	
		}
		if (!empty($attr['companyName']))
		{
			$criteria->compare('company.name',$attr['companyName'],true);
		}
		if (!empty($attr['reportMonth']))
		{
			$criteria->compare('date_format(t.createDate,"%Y-%m")',$attr['reportMonth']);
		}
		$allowedStatuses = [];
		if (!empty($attr['showRejects']))
		{
			$allowedStatuses[] = -1;
		}
		if (!isset($attr['showApproved']) OR !empty($attr['showApproved']))
		{
			$allowedStatuses[] = 1;
		}
		if (!isset($attr['showNotProcessed']) OR !empty($attr['showNotProcessed']))
		{
			$allowedStatuses[] = 0;
		}
		$criteria->addInCondition('statusId',$allowedStatuses);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => [
				'defaultOrder'=>'t.createDate DESC',
			],
			'pagination' => [
				'pageSize' => '100'
			],
		));
	}
    
    /**
	 * Создать модель данных из объекта Log1CPayment
	 */
    public function setAttributesFromLog(Log1CPayment $log)
    {
		$this->createDate = date("Y-m-d", strtotime($log->date));
		$this->logId = $log->id;
		$this->sum = (double) $log->sum;
		$this->purpose = $log->paymentPurpose;
		$this->sourceId = "1C";
		
        foreach ($this->rules() as $rule) {
        	if(isset($rule['max']) && isset($rule[0])) {
        		if(isset($this->{$rule[0]})) {
        			$this->{$rule[0]} = mb_substr($this->{$rule[0]}, 0, intval($rule['max']));
        		}
        	}
        }
        
        //return $this->validate();
    }
    
    static public function upload1CFile($fileHandle) {
    	Address::$showInactive = true;
    	Company::$showRemoved = true;
    	$responce = ['exists'=>0,'new'=>0];
    	$currentDocument = [];
    	while($line = fgets($fileHandle)) {
    		$line = mb_convert_encoding($line, "UTF-8", "WINDOWS-1251");
    		$lineData = explode('=', $line);
    		$currentDocument[ $lineData[0] ] = $lineData[1];
    		if (preg_match('/КонецДокумента/', $line)) {
    			$log = new Log1CPayment();
    			$log->setAttributesFromArray($currentDocument);
    			$log->rawData = CJSON::encode($currentDocument);
    			if($log->findExists())
    			{
    				$responce['exists'] += 1;
    			} else {
	    			if ($log->save()) {
	    				$responce['new'] += 1;
	    				$payment = new Payment();
	    				$payment->setAttributesFromLog($log);
	    				$address = NULL;
	    				$bill = NULL;
	    				if(trim($log->receiver1Name)==='ООО "ПИК"' || trim($log->receiverINN)==="7804495268") {
	    					preg_match_all("/\d+/", $log->paymentPurpose, $matches);
	    					foreach ($matches[0] as $value) {
	    						$billNum = intval($value);
	    						if($billNum > 2016 && $billNum < 10000) {
	    							$bill = Bill::model()->find("id=:billNum AND sum=:billSum", [
	    									":billNum"=>$billNum,
	    									":billSum"=>$log->sum,
	    							]);
	    							if(is_object($bill))
	    								break;
	    						}
	    					}
	    					if(is_object($bill)) {
		    					$payment->billId = $bill->id;
		    					$address = $bill->address;
	    					}

	    					if(is_object($address)) {
	    						$payment->addressId = $address->id;
		    					if(!Payment::model()->exists('billId=:billId',[':billId'=>$bill->id])) {
	    							$payment->statusId = Payment::STATUS_APPROVED;
		    					}
	    					}
	    				} else {
	    					$payment->statusId = Payment::STATUS_REJECTED;
	    				}
	    				if(!$payment->save()) {
	    					
	    				}
	    			};
    			}
    			$currentDocument = [];
    		}
    	}
    	return $responce;
    }
}