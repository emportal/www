<?php

/**
 * This is the model class for table "insuranceCompanyLicense".
 *
 * The followings are the available columns in table 'insuranceCompanyLicense':
 * @property string $id
 * @property string $insuranceCompanyId
 * @property string $number
 * @property string $kindOfActivity
 * @property string $licensingАuthorityId
 * @property string $addressId
 *
 * The followings are the available model relations:
 * @property Address $address
 * @property Authorities $licensingАuthority
 * @property InsuranceCompany $insuranceCompany
 */
class InsuranceCompanyLicense extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InsuranceCompanyLicense the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'insuranceCompanyLicense';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, insuranceCompanyId, licensingАuthorityId, addressId', 'length', 'max'=>36),
			array('number, kindOfActivity', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, insuranceCompanyId, number, kindOfActivity, licensingАuthorityId, addressId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'addressId'),
			'licensingАuthority' => array(self::BELONGS_TO, 'Authorities', 'licensingАuthorityId'),
			'insuranceCompany' => array(self::BELONGS_TO, 'InsuranceCompany', 'insuranceCompanyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'insuranceCompanyId' => 'Insurance Company',
			'number' => 'Number',
			'kindOfActivity' => 'Kind Of Activity',
			'licensingАuthorityId' => 'LicensingАuthority',
			'addressId' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id, true);
		$criteria->compare('insuranceCompanyId',$this->insuranceCompanyId, true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('kindOfActivity',$this->kindOfActivity, true);
		$criteria->compare('licensingАuthorityId',$this->licensingAuthorityId, true);//У Фарида тут русский символ "А" в базе
		$criteria->compare('addressId',$this->addressId, true);
	
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}