<?php

/**
 * This is the model class for table "diseaseGroup".
 *
 * The followings are the available columns in table 'diseaseGroup':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $description
 * @property string $tagDescription
 *
 * The followings are the available model relations:
 * @property Disease[] $diseases
 * @property DiseaseGroupOrgan[] $diseaseGroupOrgans
 * @property DiseaseGroupSymptomDisease[] $diseaseGroupSymptomDiseases
 * @property DoctorSpecialtyDisease[] $doctorSpecialtyDiseases
 * @property EquipmentDisease[] $equipmentDiseases
 */
class DiseaseGroup extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DiseaseGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'diseaseGroup';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			array('description','length', 'max'=>15000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'diseases' => array(self::HAS_MANY, 'Disease', 'diseaseGroupId'),
			'diseaseGroupOrgans' => array(self::HAS_MANY, 'DiseaseGroupOrgan', 'diseaseGroupId'),
			'diseaseGroupSymptomDiseases' => array(self::HAS_MANY, 'DiseaseGroupSymptomDisease', 'diseaseGroupId'),
			'doctorSpecialtyDiseases' => array(self::HAS_MANY, 'DoctorSpecialtyDisease', 'diseaseGroupId'),
			'equipmentDiseases' => array(self::HAS_MANY, 'EquipmentDisease', 'diseaseGroupId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'linkUrl' => 'ЧПУ урл',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Genitive name
	 * Родительный падеж 
	 */
	public function getGenName() {
		return (!empty($this->genitive) ? $this->genitive : $this->name);
	}
	

	public function beforeDelete() {
		DiseaseGroupOrgan::model()->deleteAllByAttributes(['diseaseGroupId' => $this->id]);
		DiseaseGroupSymptomDisease::model()->deleteAllByAttributes(['diseaseGroupId' => $this->id]);
		DoctorSpecialtyDisease::model()->deleteAllByAttributes(['diseaseGroupId' => $this->id]);
		EquipmentDisease::model()->deleteAllByAttributes(['diseaseGroupId' => $this->id]);
		return parent::beforeDelete();
	}
}