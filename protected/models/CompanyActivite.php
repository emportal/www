<?php

/**
 * This is the model class for table "companyActivite".
 *
 * The followings are the available columns in table 'companyActivite':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $description
 * @property string $tagTitle
 * @property string $tagDescription
 * @property string $seoCombination
 *
 * The followings are the available model relations:
 * @property CompanyActivites[] $companyActivites
 * @property DoctorSpecialty[] $doctorSpecialties
 * @property Service[] $services
 */
class CompanyActivite extends ActiveRecord
{
	public $seoCombinationArray = NULL;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanyActivite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companyActivite';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'length', 'max'=>36),
			array('name, link', 'length', 'max'=>150),
			array('description','length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, description, tagTitle, tagDescription, seoCombination', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companyActivites'		=> array(self::HAS_MANY, 'CompanyActivites', 'companyActivitesId'),
			'doctorSpecialties'		=> array(self::HAS_MANY, 'DoctorSpecialty', 'companyActiviteId'),
			'services'				=> array(self::HAS_MANY, 'Service', 'companyActiviteId'),
			'addressServices'		=> array(self::HAS_MANY, 'AddressServices','companyActivitesId')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'linkUrl' => 'ЧПУ урл',
			'description' => 'Description',
			'tagTitle' => '<title>',
			'tagDescription' => '<description>',
			'seoCombination' => 'Сео-тексты для комбинаций район/метро + специальность врача',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('tagTitle',$this->tagTitle);
		$criteria->compare('tagDescription',$this->tagDescription);
		$criteria->compare('seoCombination', $this->seoCombination);
		
		$criteria->order = '`t`.`name`';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 *  Genitive name 
	 *  Родительный падеж
	 */	
	public function getGenName() {
		return mb_strtolower((!empty($this->genitive) ? $this->genitive : $this->name),"UTF8");
	}
	
	public function getParametersModel($cityId=City::DEFAULT_CITY) {
		return CompanyActiviteParameters::model()->findByAttributes(['companyActiviteId'=>$this->id, 'cityId'=>$cityId]);
	}
	public function getParameter($name,$cityId=City::DEFAULT_CITY) {
		$parametersModel = $this->getParametersModel($cityId);
		if(is_object($parametersModel) && $parametersModel->hasAttribute($name)) {
			return $parametersModel->{$name};
		}
		if($this->hasAttribute($name) && $cityId === City::DEFAULT_CITY) {
			return $this->{$name};
		}
		return NULL;
	}
	public function setParameter($name,$value,$cityId=City::DEFAULT_CITY) {
		$parametersModel = $this->getParametersModel($cityId);
		if(!is_object($parametersModel)) {
			$parametersModel = new CompanyActiviteParameters; 
			$parametersModel->cityId = $cityId;
			$parametersModel->companyActiviteId = $this->id;
		}
		if(is_object($parametersModel) && $parametersModel->hasAttribute($name)) {
			//echo " setParameter(".$name.",".$value.",".$cityId.");";
			if($this->hasAttribute($name) && $cityId === City::DEFAULT_CITY) {
				$this->{$name} = $value;
			}
			$parametersModel->{$name} = $value;
			return $parametersModel->save();
		}
	}

	public function getSeoCombinationArr() {
		if($this->seoCombinationArray === NULL) {
			$this->seoCombinationArray = CJSON::decode($this->seoCombination);
			if($this->seoCombinationArray === NULL) {
				$this->seoCombinationArray = [];
			}
		}
		return $this->seoCombinationArray;
	}

	public function setSeoCombinationArr($value) {
		$this->seoCombinationArray = $value;
	}

	public function beforeSave() {
		if (parent::beforeSave()) {
			if(is_array($this->seoCombinationArray)) {
				$this->seoCombination = CJSON::encode($this->seoCombinationArray);
			}
			return true;
		} else {
			return false;
		}
	}
}