<?php

/**
 * This is the model class for table "medicalSchool".
 *
 * The followings are the available columns in table 'medicalSchool':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $fullName
 * @property string $extId
 * @property string $extParentId
 * @property string $cityId
 * @property string $countryId
 *
 * The followings are the available model relations:
 * @property DoctorEducation[] $doctorEducations
 * @property Country $country
 * @property City $city
 */
class MedicalSchool extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MedicalSchool the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'medicalSchool';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id,', 'length', 'max'=>36),
			array('name, link, fullName, extId, extParentId', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, link, fullName, extId, extParentId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'doctorEducations' => array(self::HAS_MANY, 'DoctorEducation', 'medicalSchoolId'),
			'country' => array(self::BELONGS_TO, 'Country', 'countryId'),
			'city' => array(self::BELONGS_TO, 'City', 'cityId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'link' => 'Link',
			'fullName' => 'Full Name',
			'extId' => 'Ext',
			'extParentId' => 'Ext Parent',
			'cityId' => 'City',
			'countryId' => 'Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('fullName',$this->fullName,true);
		$criteria->compare('extId',$this->extId,true);
		$criteria->compare('extParentId',$this->extParentId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}