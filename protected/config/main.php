<?php

// This is the main Web application configuration. Any writable
// application properties can be configured here.
$cfg = array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'Единый медицинский портал',
	'timeZone' => 'Europe/Moscow',
	'params' => array(
        'host' => 'emportal.ru',
		'baseUrl'  => 'http://emportal.ru', /* production */
		'adminEmail' => 'info@emportal.ru',
		'itEmail' => 'a.f.dyachkov@gmail.com',
        'techEmail' => 'dima90-dima@mail.ru',
		'operatorPhone' => '+79217572538',
		'operatorEmail' => 'empregister@yandex.ru',
		'reportEmail' => 'info@emportal.ru',
		'operatorEmail' => 'empregister@yandex.ru',
		/*'operatorPhone' => '+79213434502',
		'reportEmail' => 'dima90-dima@mail.ru',*/
		'remarkAppstoreEmail'	=>	'dima90-dima@mail.ru',  // mail for feedBack
		'phoneActivationLifetime' => 62208000, //3600 * 24 * 720
		'SendCodeDelay' => 60 * 10,
		'devMode' => @$_GET['devMode'],
		'nbsMode' => @$_GET['nbsMode'],
		'samozapis' => (count(array_intersect(['samozapis','samozapis-moskva'], explode('.', @$_SERVER['HTTP_HOST'])))>0 || @$_GET['samozapis']),
		'lastDayForAppCollation' => 5, //номер последнего дня месяца, когда клиника еще может отменить/подтвердить заявки предыдущего месяца (appointment.adminClinicStatusId)
		'empLoyaltyPageLink' => '<a href="/loyalty.html" target="_blank" title="Программа лояльности для пациентов ЕМП" class="loyaltyLink">',
		'api' => [
			'kassa' => [
				'shopId' => '41742',
				'scid' => '33148', //тестовый scid: 526110, боевой scid: 33148
				'shopStr' => 'emPD0zYs44f01ls',
			]
		],
		'regions' => [
			'moskva' => [
				'name' => 'Москва',
				'subdomain' => 'moskva',
				'link' => '2',
				'samozapis' => true,
				'samozapisSubdomain' => 'samozapis-moskva',
				'samozapis_disableRating' => true,
				'samozapis_disableReviews' => true,
				'samozapis_disableMainpageReviews' => true,
				'samozapis_disableMainpageTopdocs' => true,
				'empRegistryPhone' => '<span class="ya-phone-moskva">+7 (499) 350-11-29</span>',
                //'prioritizeEmpRegistryPhone' => true,
				//'empRegistryPhone' => '<span class="ya-phone-moskva">8 800 333 46 45</span>',
				'zoonPrice' => '200',
				'loyaltyProgram' => true,
			],
			'spb' => [
				'name' => 'Санкт-Петербург',
				'subdomain' => 'spb',
				'link' => '1',
				'default' => '',
				'samozapis' => true,
				'samozapisSubdomain' => 'samozapis',
				'empRegistryPhone' => '<span class="ya-phone">+7 (812) 313-21-29</span>', //для я.метрики //'+7 (812) 313-21-29',
				'empRegistryPhoneForPartner' => [ //телефоны регистратуры ЕМП для переадресации с партнерок по каждому региону
					'media' => '+7 (812) 725-00-36'
				],
				'zoonPrice' => '75',
				//'selfRegisterScenario' => true,
				'loyaltyProgram' => true,
			],
			'nizhny-novgorod' => [
				'name' => 'Нижний Новгород',
				'subdomain' => 'nizhny-novgorod',
				'link' => '5',
				'samozapis' => false,
				'empRegistryPhone' => '<span class="ya-phone-nn">+7 (831) 219-92-08</span>',
				'zoonPrice' => '50',
			],
			'novosibirsk' => [
				'name' => 'Новосибирск',
				'subdomain' => 'novosibirsk',
				'link' => '6',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
				'zoonPrice' => '50',
			],
			'ekaterinburg' => [
				'name' => 'Екатеринбург',
				'subdomain' => 'ekaterinburg',
				'link' => '3',
				'samozapis' => false,
				'disableAppointmentCalendar' => true,
				'empRegistryPhone' => '+7 (343) 357-93-43',
                'zoonPrice' => '50',
			],
			'kazan' => [
				'name' => 'Казань',
				'subdomain' => 'kazan',
				'link' => '4',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'samara' => [
				'name' => 'Самара',
				'subdomain' => 'samara',
				'link' => '7',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'chelyabinsk' => [
				'name' => 'Челябинск',
				'subdomain' => 'chelyabinsk',
				'link' => '8',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'omsk' => [
				'name' => 'Омск',
				'subdomain' => 'omsk',
				'link' => '109',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'rostov-na-donu' => [
				'name' => 'Ростов-на-Дону',
				'subdomain' => 'rostov-na-donu',
				'link' => '203',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'ufa' => [
				'name' => 'Уфа',
				'subdomain' => 'ufa',
				'link' => '30537',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'krasnoyarsk' => [
				'name' => 'Красноярск',
				'subdomain' => 'krasnoyarsk',
				'link' => '206',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'perm' => [
				'name' => 'Пермь',
				'subdomain' => 'perm',
				'link' => '105',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'volgograd' => [
				'name' => 'Волгоград',
				'subdomain' => 'volgograd',
				'link' => '35',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
                'zoonPrice' => '50',
			],
			'voronezh' => [
				'name' => 'Воронеж',
				'subdomain' => 'voronezh',
				'link' => '205',
				'samozapis' => false,
				'empRegistryPhone' => '+7 (473) 200-61-29',
				'selfRegisterScenario' => true,
                'zoonPrice' => '50',
			],
			'irkutsk' => [
				'name' => 'Иркутск',
				'subdomain' => 'irkutsk',
				'link' => '37',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
				'zoonPrice' => '50',
			],
			// new
			'saratov' => [
				'name' => 'Саратов',
				'subdomain' => 'saratov',
				'link' => '209',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
			],
			'tyumen' => [
				'name' => 'Тюмень',
				'subdomain' => 'tyumen',
				'link' => '212',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
			],
			'vladivostok' => [
				'name' => 'Владивосток',
				'subdomain' => 'vladivostok',
				'link' => '204',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
			],
			'yaroslavl' => [
				'name' => 'Ярославль',
				'subdomain' => 'yaroslavl',
				'link' => '60',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
			],
			/*
			'ulyanovsk' => [
				'name' => 'Ульяновск',
				'subdomain' => 'ulyanovsk',
				'link' => '1854',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
			],
			'izhevsk' => [
				'name' => 'Ижевск',
				'subdomain' => 'izhevsk',
				'link' => '200',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
			],
			'krasnodar' => [
				'name' => 'Краснодар',
				'subdomain' => 'krasnodar',
				'link' => '36',
				'samozapis' => false,
				'empRegistryPhone' => '8 800 333 46 45',
			],
			*/
		],
		//'selectedRegion' => (Yii::app()->session['selectedRegion']) ? Yii::app()->session['selectedRegion'] : 'spb'
	),
	'preload' => array('log'),
	'aliases' => array(
		'shared' => 'webroot.shared',
		'bootstrap' => 'application.extensions.bootstrap',
		'uploads' => 'webroot.uploads'
	),
	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.components.lib.*',
		'application.extensions.debugtoolbar.*',
		'ext.YiiMailer.YiiMailer',
		'ext.giix-components.*',
		'ext.ESelect2.*',
		'ext.imperavi-redactor-widget.*',
		'ext.JuiDateTimePicker.*',
		'application.extensions.image.*',
		'application.commands.*',
		
	    'ext.eoauth.*',
	    'ext.eoauth.lib.*',
	    'ext.lightopenid.*',
	    'ext.eauth.services.*',
	),
	'language' => 'ru',
	'defaultController' => 'site',
	'modules' => array(
// uncomment the following to enable the Gii tool

		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => '123456',
			'generatorPaths' => array(
				'ext.giix-core', // giix generators
			),
		// If removed, Gii defaults to localhost only. Edit carefully to taste.
//'ipFilters'	 => array('127.0.0.1', '*', '37.77.130.170'),
		),
		'cal' => array(
			'debug' => true // For first run only!
		),
		'admin',
		'adminClinic',
		'adminInsurance', // ЛК страховых
		'adminServices',
		'adminAll',
		'newAdmin',
		#'new_adminClinic',
		'gallery'
	),
	'controllerMap' => array(
		'min' => array(
			'class' => 'application.extensions.minScript.controllers.ExtMinScriptController',
		),
	),
	// application components
	'components' => array(
		'ePdf' => array(
			'class'         => 'ext.yii-pdf.EYiiPdf',
			'params'        => array(
				'mpdf'     => array(
					'librarySourcePath' => 'application.vendors.mpdf.*',
					'constants'         => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder.
					/*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
					 'mode'              => '', //  This parameter specifies the mode of the new document.
						'format'            => 'A4', // format A4, A5, ...
						'default_font_size' => 0, // Sets the default document font size in points (pt)
						'default_font'      => '', // Sets the default font-family for the new document.
						'mgl'               => 15, // margin_left. Sets the page margins for the new document.
						'mgr'               => 15, // margin_right
						'mgt'               => 16, // margin_top
						'mgb'               => 16, // margin_bottom
						'mgh'               => 9, // margin_header
						'mgf'               => 9, // margin_footer
						'orientation'       => 'P', // landscape or portrait orientation
					)*/
				),
				'HTML2PDF' => array(
					'librarySourcePath' => 'application.vendors.html2pdf.*',
					'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
					/*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
					 'orientation' => 'P', // landscape or portrait orientation
						'format'      => 'A4', // format A4, A5, ...
						'language'    => 'en', // language: fr, en, it ...
						'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
						'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
						'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
					)*/
				)
			),
		),
		'sxGeo' => array(
			'class' => 'ext.SxGeo.SxGeo',
		),
		'loid' => array(
	        'class' => 'ext.lightopenid.loid',
	    ),
	    'eauth' => array(
	        'class' => 'ext.eauth.EAuth',
	        'popup' => true, // Use the popup window instead of redirecting.
	        'services' => array( // You can change the providers and their classes.
	            'odnoklassniki' => array(
	                'class' => 'OdnoklassnikiOAuthService',
	                'client_id' => '1132428800',
	                'client_secret' => '8940813CD39C9028AA9F7E5D',
	                'client_public' => 'CBAPHQHEEBABABABA',
	            ),
	            'facebook' => array(
	                'class' => 'FacebookOAuthService',
	                'client_id' => '742878159166414',
	                'client_secret' => 'e1c091458621ca3e869274645fe71068',
	            ),
	        ),
	    ),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'urlManager' => array(
			//'class' => 'application.components.UrlManager',
			'urlFormat' => 'path',
			'showScriptName' => false,
			'appendParams' => false,
			'rules' => array(
				'/robots.txt' => 'site/robots',
				'/login' => 'site/login',
				'/logout' => 'site/logout',
				'/admin<url:.*>' => '/admin<url>',
				'/adminClinic<url:.*>' => '/adminClinic<url>',
				'/newAdmin<url:.*>' => '/newAdmin<url>',
				'/user<url:.*>' => '/user<url>',
				'<controller:(ajax)>/<method:.*>' => '<controller>/<method>',
				((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://blog.emportal.ru' => 'news/index',
				((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://blog.emportal.ru/authors.html' => 'news/authors',
				((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://blog.emportal.ru/<link:\w+>.html' => 'news/view',
				((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://blog.emportal.ru/<filter:.*>' => 'news/index',
				((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://blog.emportal.ru/<param1:.*>/<param2:.*>' => 'site/error404',
				'' => 'site/index',
				'<view:(raiting|rating|guide|about)>.html' => 'site/redirectToMain',
				'<view:\w+>.html' => 'site/page',
				//'<view:\w+>.html' => 'site/page',
				'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
				'<action:(getrating|login|logout|register|success|recoveryByPhone|recovery|activate|activatephone|confirmedVisit|blockedappointment|help)>' => 'site/<action>',
				'<controller:news>/<filter:\w+>' => '<controller>/index',
				'<action:poll>' => 'site/form',
				'<filter:article>' => 'news/index',	
				'<controller:(user|search|news)>' => '<controller>/index',
				'<controller:(beauty|tourism|insurance|actions)>/*' => 'site/redirectToMain', /* not used categories */
				'<controller:(handbook|search)>/<action:(doctorSpecialty)>' => 'site/redirectToMain', /* not used categories */
				
				'<controller:clinic>/<link:\w+>.html' => 'site/error404',
				'<controller:\w+>/<link:\w+>.html' => '<controller>/view',
				
				'http://<subdomain:\w+>.emportal.ru/sitemap.xml' => 'site/sitemap',
				'https://<subdomain:\w+>.emportal.ru/sitemap.xml' => 'site/sitemap',
				'/sitemap.xml' => 'site/sitemap',

				'http://<subdomain:\w+>.emportal.ru/reviews.xml' => 'site/reviews',
				'https://<subdomain:\w+>.emportal.ru/reviews.xml' => 'site/reviews',
				'/reviews.xml' => 'site/reviews',
				
				'mobileApps/MetroAndDistrictLists.JSON' => 'site/getMetroAndDistrictList',
				'doctor/similar' => 'doctor/similar',
				'clinic/similar' => 'clinic/similar',
				'appVk' => 'search/appVk',
				
				/* возможности для клиник */
				'vozmozhnosti_dlya_klinik' => 'site/redirectToMain',
				'vozmozhnosti_dlya_klinik/<page:.*>' => 'site/vozmozhnosti_dlya_klinik',
				
				/*START search BLOCK*/
				//redirects for old clinic urls
				'<action:(clinic)>/<metro:metro-[-\w]+>/<profile:profile-[-\w]+>' => 'site/RedirectToKlinikaUrl',
				'<action:(clinic)>/<district:raion-[-\w]+>/<profile:profile-[-\w]+>' => 'site/RedirectToKlinikaUrl',
				//new klinika seo url
				'<action:(klinika)>/<metro:metro-[-\w]+>/<profile:profile-[-\w]+>' => 'search/clinic',
				'<action:(klinika)>/<district:raion-[-\w]+>/<profile:profile-[-\w]+>' => 'search/clinic',
				
				'<action:(map)>/<metro:metro-[-\w]+>/<profile:profile-[-\w]+>' => 'search/<action>',
				'<action:(map)>/<district:raion-[-\w]+>/<profile:profile-[-\w]+>' => 'search/<action>',
				'<action:(doctor)>/<metro:metro-[-\w]+>/<specialty:specialty-[-\w]+>' => 'search/<action>',	
				'<action:(doctor)>/<district:raion-[-\w]+>/<specialty:specialty-[-\w]+>' => 'search/<action>',
				'<action:(diagnostics)>/<metro:metro-[-\w]+>/<service:service-[-\w]+>' => 'search/<action>',	
				'<action:(diagnostics)>/<district:raion-[-\w]+>/<service:service-[-\w]+>' => 'search/<action>',
				
				'<action:(clinic)>/<profile:profile-[-\w]+>' => 'site/RedirectToKlinikaUrl',
				'<action:(klinika)>/<profile:profile-[-\w]+>' => 'search/clinic',
				
				'<action:(map)>/<profile:profile-[-\w]+>' => 'search/<action>',
				'<action:(doctor)>/<specialty:specialty-[-\w]+>' => 'search/<action>',	
				'<action:(diagnostics)>/<service:service-[-\w]+>' => 'search/<action>',				
				
				'<action:(doctor|map|diagnostics)>' => 'search/<action>',
				'<action:(doctor|map|diagnostics)>/<metro:metro-[-\w]+>/' => 'search/<action>',			
				'<action:(doctor|map|diagnostics)>/<district:raion-[-\w]+>/' => 'search/<action>',					
				
				//redirects for old clinic urls
				'<action:(clinic)>' => 'site/RedirectToKlinikaUrl',
				'<action:(clinic)>/<metro:metro-[-\w]+>/>' => 'site/RedirectToKlinikaUrl',
				'<action:(clinic)>/<district:raion-[-\w]+>/>' => 'site/RedirectToKlinikaUrl',
				//new klinika seo url
				'<action:(klinika)>' => 'search/clinic',
				'<action:(klinika)>/<metro:metro-[-\w]+>' => 'search/clinic',
				'<action:(klinika)>/<district:raion-[-\w]+>' => 'search/clinic',
				
				'<controller:search>/<action:(beauty|tourism|insurance)>' => 'site/redirectToMain',
				/*END search BLOCK*/
				
				'<controller:doctor>/<linkUrl:[-\w]+>' => 'doctor/view',
				'<controller:doctor>/<linkUrl:[-\w]+>/visit' => 'doctor/visit',
				
				'<controller:doctor>/<link:\w+>.html' => 'doctor/visit',
				
				'<controller:clinic>/<action:(addresses|experts)>/<link:\w+>.html' => 'site/error404',
				'<controller:\w+>/<action:\w+>/<link:\w+>.html' => '<controller>/<action>',
				
				'<controller:(clinic)>/<companyLink>/<linkUrl>' => 'site/RedirectToKlinikaUrl',
				'<controller:(klinika)>/<companyLink>/<linkUrl>' => 'clinic/view',
				'clinic/<companyLink>/<linkUrl>/<action:(experts|visit|map|comments)>' => 'site/RedirectToKlinikaUrl', #show experts
				'klinika/<companyLink>/<linkUrl>/<action:(experts|visit|map|comments)>' => 'clinic/<action>', #show experts
				
				'clinic/<companyLinkUrl>/<addressLinkUrl>/<linkUrl>' => 'site/RedirectToKlinikaUrl',
				'klinika/<companyLinkUrl>/<addressLinkUrl>/<linkUrl>' => 'doctor/view',
				
				'<module:(adminClinicTablet)>/<controller:\w+>/<action:\w+>' => 'adminClinic/<controller>/<action>', #for tablet app
				
				/* Widget Controller (for 2GIS)  */
				'<controller:(widget)>/<companyLink>/<linkUrl>' => '<controller>/view',	
				'widget/<companyLinkUrl>/<addressLinkUrl>/<linkUrl>' => 'widget/viewdoc',										
				
				/* Clinic Controller  */
				//old clinic seo url
				'<controller:(clinic)>/<companyLink>' => 'clinic/addresses',
				//
				'<controller:(klinika)>/<action:(experts)>' => 'clinic/experts',
				'<controller:(klinika)>/<companyLink>/<linkUrl>/<action:\w+>' => 'clinic/<action>',
				'<controller:(klinika)>/<companyLink>' => 'clinic/addresses',
				'<controller:(handbook)>/<action:\w+>/<linkUrl>' => '<controller>/<action>',
				'<controller:(handbook)>/<action:(doctorSpecialty|disease)>' => 'search/<action>',
				'<action:(companyActivity)>' => 'search/<action>',
				
				/* API */
				'api/kassa/<type>' => 'site/kassa',
				'api/v1/<action>' => 'api/<action>',
				'api/v1/<action>/<id>' => 'api/<action>',
				'api/v1/<action>/<id>/<relatedModels>' => 'api/<action>',
				
				'<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',	
				
				/* XML for partners */
				'site/GisList.xml' => 'site/xmlCreate',
				'site/yandexCompanies.xml' => 'site/YandexCompanies',
			),
		),
		'session' => array(
            'class' => 'CDbHttpSession',
            'connectionID' => 'db',
            'cookieParams' => array('domain' => '.emportal.ru', 'lifetime' => 0),
            'timeout' => 10800,
            'sessionName' => 'session',
        ),
		'user' => array(
			// enable cookie-based authentication
			'class' => 'WebUser',
			'allowAutoLogin' => true,
			'identityCookie' => array('domain' => '.emportal.ru'),
		),
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=emportal',
			'emulatePrepare' => true,
			'username' => 'emportal',
			'password' => '9WLn39x1nUyZ9pGK',
			'charset' => 'utf8',
			'tablePrefix' => '',
			'enableParamLogging' => true,
			'enableProfiling' => true,
		),
		'search' => array(
			'class' => 'application.components.DGSphinxSearch.DGSphinxSearch',
			'server' => '127.0.0.1',
			'port' => 9312,
			'maxQueryTime' => 3000,
			'enableProfiling' => 0,
			'enableResultTrace' => 0,
			'fieldWeights' => array(
				'name' => 10000,
				'keywords' => 100,
			),
		),
		'assetManager' => array(
			'linkAssets' => 'true',
		//'forceCopy'		 => true,
		),
		'image' => array(
			'class' => 'application.extensions.image.CImageComponent',
			// GD or ImageMagick
			'driver' => 'GD',
			// ImageMagick setup path
			'params' => array('directory' => '/opt/local/bin'),
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning, watch, info',
					'categories' => 'application.*',
				),
				array(
					'class' => 'MyEmailLogRoute',
					'levels' => 'error',
					'emails' => 'dima90-dima@mail.ru',
					'subject' => 'Error at Emportal - ' . @$_SERVER['HTTP_HOST'],
					'filter' => 'CLogFilter',
					'sentFrom' => 'error@emportal.ru',
				),
				array(
					'class' => 'MyEmailLogRoute',
					'levels' => 'error',
					'emails' => 'alexander.d@emportal.ru',
					'subject' => 'Error at Emportal - ' . @$_SERVER['HTTP_HOST'],
					'filter' => 'CLogFilter',
					'sentFrom' => 'error@emportal.ru',
				),
			/* 	'web'=>array(
			  'class'=>'CWebLogRoute',
			  'levels'=>'error, warning',
			  'categories'=>'system.db.*',
			  'showInFireBug'=>false //true/falsefirebug only - turn off otherwise
			  ),
			  'file'=>array(
			  'class'=>'CFileLogRoute',
			  'levels'=>'error, warning, watch',
			  'categories'=>'system.*',
			  ),

			  'profile'=>array(
			  'class' => 'CProfileLogRoute',
			  'report'=>'summary',
			  ),
			 */
			/*
			  array(
			  'class'=>'CProfileLogRoute',
			  'levels'=>'profile',
			  'report'=>'summary',
			  'enabled'=>true,
			  )
			 */
			),
		),
// 		'log'			 => array(
// 			'class'	 => 'CLogRouter',
// 			'routes' => array(
// 				array(
// 					'class'	 => 'CFileLogRoute',
// 					'levels' => 'error, warning',
// 				),
// 				// uncomment the following to show log messages on web pages
// 				//array(
// 				//	'class' => 'CWebLogRoute',
// 				//),
// 				array(
// 					'class'		 => 'XWebDebugRouter',
// 					//'config' => 'alignRight, opaque, runInDebug, fixedPos',
// 					//'config'	 => 'alignRight, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
// 					'config'	 => 'alignRight, opaque, fixedPos, collapsed, yamlStyle',
// 					'levels'	 => 'error, warning, trace, profile, info',
// 					'allowedIPs' => array('127.0.0.1', '::1', '192.168.88.5', '37.77.130.170'),
// 				),
// 				// standard log route
// 				array(
// 					'class'	 => 'ext.firephp.SFirePHPLogRoute',
// 					'levels' => 'error, warning, info, trace',
// 				),
// 				// profile log route
// 				array(
// 					'class'	 => 'ext.firephp.SFirePHPProfileLogRoute',
// 					'report' => 'summary' // or "callstack"
// 				),
// 			),
// 		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.Bootstrap',
		),
		'clientScript' => array(
			//'class'=>'application.extensions.minScript.components.ExtMinScript',
			'scriptMap' => array(
				'jquery.js' => '/shared/js/jquery.min.js',
				'jquery.min.js' => '/shared/js/jquery.min.js',
				'jquery-migrate-1.1.0.js' => '/shared/js/jquery-migrate-1.1.0.js',
				'jquery-migrate-1.1.0.min.js' => '/shared/js/jquery-migrate-1.1.0.min.js',
				'jquery-ui.js' => '/shared/js/jquery-ui-1.9.2.js',
				'jquery-ui.min.js' => '/shared/js/jquery-ui-1.9.2.min.js',
			//	'jquery.jcarousel.js'=>'/shared/js/jquery.jcarousel.js',
			),
			'coreScriptPosition' => CClientScript::POS_END,
			'packages' => array(
				'main' => array(
					'basePath' => 'shared',
					'js' => array('js/scripts.js?v=0229'),
					'css' => array('css/main.css?v=0228'),
					'depends' => array(
						'jquery',
						'cookie',
						'modernizr',
						'cusel',
						'checkbox',
						'infieldlabel',
						'fancybox',
						'rateit',
						'datetimepicker',
					//	'appForm',
						'modalWindow',
						'emailBlock'
					)
				),
				'migrate' => array(		
					'basePath' => 'shared',
					'js' => array('js/' . (YII_DEBUG ? 'jquery-migrate-1.1.0.js' : 'jquery-migrate-1.1.0.min.js')),
					'depends' => array('jquery'),
				),
				'jquery.ui' => array(
					'js' => array('js/' . (YII_DEBUG ? 'jquery-ui.js' : 'jquery-ui.min.js')),
					'depends' => array('jquery'),
				),
				'rateit' => array(
					'basePath' => 'shared',
					'js' => array('js/' . (YII_DEBUG ? 'jquery.rateit.js' : 'jquery.rateit.min.js')),
					'depends' => array('jquery'),
				),
				'jquery.mousewheel' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.mousewheel.js'),
					'depends' => array('jquery')
				),
				'modernizr' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.modernizr-2.6.2.min.js'),
					'depends' => array('jquery')
				),
				'jquery.mask' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.mask.min.js'),
					'depends' => array('jquery')
				),
				'jquery.maskedinput' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.maskedinput.min.js'),
					'depends' => array('jquery')
				),
				'jquery.tablesorter' => [
					'basePath' => 'shared',
					'js' => array('js/jquery.tablesorter.min.js'),
					'depends' => array('jquery')
				],
				'cusel' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.cusel.min-2.5.js'),
					'depends' => array('migrate', 'jquery.mousewheel')
				),
				'checkbox' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.checkbox.js?v=0133'),
					'depends' => array('jquery')
				),
				'focus' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.focus.js'),
					'depends' => array('jquery')
				),
				'infieldlabel' => array(
					'basePath' => 'shared',
					'js' => array('js/jquery.infieldlabel.js'),
					'depends' => array('jquery')
				),
				'fancybox' => array(
					'basePath' => 'shared',
					'js' => array('js/fancybox/' . (YII_DEBUG ? 'jquery.fancybox-1.3.4.js' : 'jquery.fancybox-1.3.4.pack.js'),
						'js/fancybox/jquery.easing-1.3.pack.js',
					),
					'css' => array('js/fancybox/jquery.fancybox-1.3.4.css'),
					'depends' => array('jquery', 'jquery.mousewheel')
				),
				'datetimepicker' => [
					'basePath' => 'shared',
					'js' => array('js/xdsoftpicker/jquery.datetimepicker.js'),
					'css' => array('js/xdsoftpicker/jquery.datetimepicker.css'),
					'depends' => array('jquery')
				],
				'yandex' => array(
					'basePath' => 'shared',
					'js' => array('js/yandex_map_custom.js?v=0094')
				),
				'eldar_css' => [
					'basePath' => 'shared',
					'css' => ['css/eldar_css.css?v=0095']
				],
				'modalWindow' => [
					'basePath' => 'shared.bundles.modalWindow',
					'js' => ['modalWindow.js'],
					'depends' => ['jquery']
				],
				'appForm' => array(
					'basePath' => 'shared.bundles.appForm',
					'js' => array('appForm.js?v=1'),
					'css' => array('appForm.css?v=1'),
					//'depends' => array('jquery'),
				),
				'emailBlock' => [
					'basePath' => 'shared.bundles.emailBlock',
					'js' => ['emailBlock.js'],
					'depends' => ['jquery']
				],
			),
		),
		'cache'=>array( 
			'class'=>'system.caching.CDbCache'
		)
	),
);

//if(@$_SERVER['HTTP_HOST'] == 'emportal.ru') {
//	$cfg['components']['log']['routes'][] = array(
//		'class' => 'MyEmailLogRoute',
//		'levels' => 'error',
//		'emails' => 'dima90-dima@mail.ru',
//		'subject' => 'Error at Emportal - ' . $_SERVER['HTTP_HOST'],
//		'filter' => 'CLogFilter',
//		'sentFrom' => 'error@emportal.ru',
//	);
//}
if(@$_SERVER['HTTP_HOST'] == 'dev1.emportal.ru') {
	$cfg['params']['lastDayForAppCollation'] =  '1';
}

return $cfg;
?>