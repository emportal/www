<?php
$main_cfg = (require dirname(__FILE__) . '/main.php');

$cfg = array(
	// У вас этот путь может отличаться. Можно подсмотреть в config/main.php.
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',	
	'params'   => $main_cfg['params'],
	'name'	 => 'Единый медицинский портал',
	'timeZone' => 'Europe/Moscow',
	'preload' => array('log'),
	'aliases' => array(
		'shared' => 'webroot.shared',
		'bootstrap' => 'application.extensions.bootstrap',
		'uploads' => 'webroot.uploads'
	),
	'import' => array(
		'application.components.*',
		'application.components.lib.*',
		'application.models.*',
		'application.extensions.YiiMailer.*'
	),
	'modules'			 => array(
		'gallery'
	),
	'behaviors'=>array(
		'viewRenderer'=>'application.components.behaviors.CAViewRendererBehavior',
	),
	// Копирование yiic.php и console.php было сделано ради
	// перенаправления журнала для cron в отдельные файлы:
	'components' => array(
		'ePdf' => array(
			'class'         => 'application.extensions.yii-pdf.EYiiPdf',
			'params'        => array(
				'mpdf'     => array(
					'librarySourcePath' => 'application.vendors.mpdf.*',
					'constants'         => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder.
					/*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
					 'mode'              => '', //  This parameter specifies the mode of the new document.
						'format'            => 'A4', // format A4, A5, ...
						'default_font_size' => 0, // Sets the default document font size in points (pt)
						'default_font'      => '', // Sets the default font-family for the new document.
						'mgl'               => 15, // margin_left. Sets the page margins for the new document.
						'mgr'               => 15, // margin_right
						'mgt'               => 16, // margin_top
						'mgb'               => 16, // margin_bottom
						'mgh'               => 9, // margin_header
						'mgf'               => 9, // margin_footer
						'orientation'       => 'P', // landscape or portrait orientation
					)*/
				),
				'HTML2PDF' => array(
					'librarySourcePath' => 'application.vendors.html2pdf.*',
					'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
					/*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
					 'orientation' => 'P', // landscape or portrait orientation
						'format'      => 'A4', // format A4, A5, ...
						'language'    => 'en', // language: fr, en, it ...
						'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
						'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
						'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
					)*/
				)
			),
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'logFile' => 'cron.log',
					'levels' => 'error, warning',
				),
				array(
					'class' => 'CFileLogRoute',
					'logFile' => 'cron_trace.log',
					'levels' => 'trace',
				),
				array(
					'class' => 'CFileLogRoute',
					'logFile' => 'updateMainAddress.log',
					'levels' => 'log',
					'categories' => 'updateMainAddress',
				),
			),
		),
		// Соединение с СУБД
		'db'			 => array(
			'connectionString'	 => 'mysql:host=localhost;dbname=emportal',
			'emulatePrepare'	 => true,
			'username'			 => 'emportal',
			'password'			 => '9WLn39x1nUyZ9pGK',
			/*'username'			 => 'root',
			'password'			 => '260294o',*/	
			'charset'			 => 'utf8',
			'tablePrefix'		 => '',
			'enableParamLogging' => true,
			'enableProfiling'	 => true,
		),
		'cache'=>array( 
			'class'=>'system.caching.CDbCache'
		),
	),
);

return $cfg;