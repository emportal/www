<?php

return array_replace_recursive(
	require dirname(__FILE__) . '/main.php', array(
		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'params' => array(
            'host' => 'dev1.emportal.ru',
			'devMode' => true,
			'devEmail' => 'roman.z@emportal.ru',
			'devPhone' => '+79516509085' //null //'+79516509085'
		),
		'components' => array(
			'log' => array(
				'class' => 'CLogRouter',
				'routes' => array(
					'file' => array(
						'class' => 'CFileLogRoute',
						'levels' => 'error, warning, watch, info',
						'categories' => 'application.*',
					),
					'devmail' => array(
						'class' => 'MyEmailLogRoute',
						'levels' => 'error',
						'emails' => 'roman.z@emportal.ru',
						'subject' => 'Error at dev.emportal.ru - ' . $_SERVER['HTTP_HOST'],
						'filter' => 'CLogFilter',
						'sentFrom' => 'error@emportal.ru',
					),
					//'profile' => array(
					//	'class' => 'CProfileLogRoute',
					//	'report' => 'summary',
					//),
					//'web'=>array(
					//	'class'=>'CWebLogRoute',
					//	'levels'=>'error, warning',
					//	'categories'=>'system.db.*',
					//	'showInFireBug'=>false //true/falsefirebug only - turn off otherwise
					//),
				),
			),
		),
	));