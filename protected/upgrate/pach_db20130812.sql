ALTER TABLE `action` ADD `unlimited` TINYINT( 1 ) NOT NULL DEFAULT '1';
ALTER TABLE `action` ADD `companyActivitesId` CHAR( 36 ) NOT NULL;
ALTER TABLE `addressServices` ADD `companyActivitesId` CHAR( 36 ) NOT NULL;

ALTER TABLE `action` ADD `addressId` CHAR( 36 ) NOT NULL AFTER `companyId`,
ADD INDEX `action_address` ( `addressId` ),
ADD INDEX `action_companyActivites` ( `companyActivitesId` ),
ADD CONSTRAINT `action_address` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `action_companyActivites` FOREIGN KEY (`companyActivitesId`) REFERENCES `companyActivite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `addressServices` ADD `companyActivitesId` CHAR( 36 ) NOT NULL AFTER `serviceId`,
ADD INDEX `addressServices_companyActivitesId` ( `companyActivitesId` ),
ADD CONSTRAINT `addressServices_companyActivitesId` FOREIGN KEY (`companyActivitesId`) REFERENCES `companyActivites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

;
ALTER TABLE `companyLicense` ADD `link` char(9) COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `doctor` CHANGE `birthday` `birthday` DATE NULL DEFAULT NULL COMMENT 'ДеньРождения';
ALTER TABLE `doctor` CHANGE `experienceDate` `experienceDate` DATE NULL DEFAULT NULL COMMENT 'ОпытРаботы_Дата';

--
-- Структура таблицы `emailCode`
--

CREATE TABLE IF NOT EXISTS `emailCode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `date` int(13) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_emailCode_userUserId1` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Структура таблицы `userMedical`
--

DROP TABLE IF EXISTS `userMedical`;
CREATE TABLE `userMedical` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'id',
  `name` char(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Наименование',
  `link` char(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'link',
  `password` char(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Пароль',
  `email` char(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ЭлектроннаяПочта',
  `addressId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Адрес',
  `companyId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Контрагент',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  PRIMARY KEY (`id`),
  KEY `userMedical_address` (`addressId`),
  KEY `userMedical_company` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ПользователиПортала_Медучреждения';

--
-- Дамп данных таблицы `userMedical`
--

INSERT INTO `userMedical` (`id`, `name`, `link`, `password`, `email`, `addressId`, `companyId`, `status`) VALUES
('2c8dee37-a433-11e2-b7ff-000c292b591f', 'test', NULL, 'test', NULL, '15b7b542-edd0-11e1-b127-e840f2aca94f', '15b7b540-edd0-11e1-b127-e840f2aca94f', 1);

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'id',
  `name` char(25) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Наименование',
  `link` char(25) COLLATE utf8_unicode_ci NOT NULL COMMENT 'link',
  `date` datetime DEFAULT NULL COMMENT 'ДатаКомментария',
  `content` text COLLATE utf8_unicode_ci COMMENT 'Содержимое',
  `rating` int(2) DEFAULT NULL COMMENT 'Рейтинг',
  `userId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ПользовательПортала',
  `doctorId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Врач',
  `status` int(2) DEFAULT NULL COMMENT 'Статус',
  `addressId` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `appointmentToDoctorsId` char(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `doctorId` (`doctorId`),
  KEY `FK_comment_addressAddressId1` (`addressId`),
  KEY `FK_comment_appointmentToDoctorsAppointmentToDoctorsId1` (`appointmentToDoctorsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='КомментарииПользователей';


CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `versions_data` text COLLATE utf8_unicode_ci NOT NULL,
  `name` tinyint(1) NOT NULL DEFAULT '1',
  `description` tinyint(1) NOT NULL DEFAULT '1',
  `owner` char(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`owner`),
  UNIQUE KEY `owner` (`owner`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `owner_2` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `gallery`
--

INSERT INTO `gallery` (`id`, `versions_data`, `name`, `description`, `owner`) VALUES
(0, 'a:2:{s:5:"small";a:1:{s:6:"resize";a:2:{i:0;i:200;i:1;N;}}s:6:"medium";a:1:{s:6:"resize";a:2:{i:0;i:800;i:1;N;}}}', 1, 1, ''),
(1, '', 1, 1, '000000005');

--
-- Структура таблицы `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  `file_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_gallery_photo_gallery1` (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=69 ;

--
-- Дамп данных таблицы `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `rank`, `name`, `description`, `file_name`) VALUES
(60, 1, 63, '', NULL, '20130902_164638.jpg'),
(61, 1, 60, '223', '23', '20130902_164715.jpg'),
(63, 1, 61, 'rrr', '', '20130902_164814.jpg'),
(64, 1, 64, '', NULL, '20130902_164745.jpg'),
(66, 1, 66, '', NULL, '20130730_201642.jpg'),
(67, 1, 67, '', NULL, '20130730_201756.jpg'),
(68, 1, 68, '', NULL, '20130730_201858.jpg');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `fk_gallery_photo_gallery1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
  --
-- Структура таблицы `favorite`
--

CREATE TABLE `favorite` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'id',
  `ownerId` text COLLATE utf8_unicode_ci COMMENT 'Владелец',
  `userId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ПользовательПортала',
  PRIMARY KEY (`id`),
  KEY `favorite_user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Избранное';

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `favorite_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
 
--
-- Структура таблицы `appointmentToDoctors`
--

CREATE TABLE `appointmentToDoctors` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `link` char(9) COLLATE utf8_unicode_ci NOT NULL,
  `doctorId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Врач',
  `companyId` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'МедицинскоеУчреждение',
  `userId` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ПользовательПортала',
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `plannedTime` datetime DEFAULT NULL COMMENT 'ПланируемоеВремя',
  `serviceId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Услуга',
  `addressId` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Адрес',
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `statusId` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `appointmentToDoctors_doctor` (`doctorId`),
  KEY `appointmentToDoctors_address` (`addressId`),
  KEY `appointmentToDoctors_company` (`companyId`),
  KEY `appointmentToDoctors_user` (`userId`),
  KEY `appointmentToDoctors_service` (`serviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ЗаписиКВрачам';


CREATE TABLE `servicesRequest` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'id',
  `name` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `addressId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ссылка',
  `status` tinyint(1) DEFAULT NULL COMMENT 'Стоимость',
  `serviceSectionId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ГруппыУслуг',
  `description` text COLLATE utf8_unicode_ci COMMENT 'Описание',
  `companyActiviteId` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ПрофильДеятельности',
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `servicesRequest_address` (`addressId`),
  KEY `servicesRequest_companyActivite` (`companyActiviteId`),
  KEY `servicesRequest_serviceSection` (`serviceSectionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Услуги.Заявки на модерацию';

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `servicesRequest`
--
ALTER TABLE `servicesRequest`
  ADD CONSTRAINT `servicesRequest_address` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `servicesReques_companyActivite` FOREIGN KEY (`companyActiviteId`) REFERENCES `companyActivite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `servicesReques_serviceSection` FOREIGN KEY (`serviceSectionId`) REFERENCES `serviceSection` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE TABLE `syncTables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tableId` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `table` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `action` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `done` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=0 ;


