CREATE TABLE IF NOT EXISTS `phoneVerification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` char(50) NOT NULL,
  `code` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE `user` ADD `phoneActivationStatus` INT NOT NULL AFTER `telefon` ,
ADD `phoneActivationDate` DATETIME NULL AFTER `phoneActivationStatus` ;

ALTER TABLE `phoneVerification` ADD `status` INT( 11 ) NOT NULL AFTER `code` ,
ADD `timestamp` INT( 10 ) NOT NULL AFTER `status` ;