<?php

class m161110_090122_add_field_skype_to_userMedical extends CDbMigration
{
	

	public function safeUp()
	{
		Yii::app()->db->createCommand("ALTER TABLE userMedical ADD loginSkype VARCHAR(250)")->execute();
	}
	public function down()
	{
		echo "m161110_090122_add_field_skype_to_userMedical does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}