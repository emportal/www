<?php
/**
 * Wrapper for ivaynberg jQuery select2 (https://github.com/ivaynberg/select2)
 * 
 * @author Anggiajuang Patria <anggiaj@gmail.com>
 * @link http://git.io/Mg_a-w
 * @license http://www.opensource.org/licenses/apache2.0.php
 */
class ESelect2 extends CInputWidget
{

    /**
     * @var array select2 options
     */
    public $options = array();

    /**
     * @var array CHtml::dropDownList $data param
     */
    public $data = array();

    /**
     * @var string html element selector
     */
    public $selector;

    /**
     * @var array javascript event handlers
     */
    public $events = array();

	/**
	 * @var boolean should the items of a multiselect list be sortable using jQuery UI
	 */
	public $sortable = false;
    
    protected $defaultOptions = array();

    public function init()
    {
        $this->defaultOptions = array(
            'formatNoMatches'=> 'js:function () { return "Совпадений не найдено"; }',
			'formatInputTooShort'=> 'js:function (input, min) { var n = min - input.length; return "Пожалуйста, введите еще " + n + " символ" + (n == 1 ? "" : ((n > 1)&&(n < 5) ? "а" : "ов")); }',
			'formatInputTooLong'=> 'js:function (input, max) { var n = input.length - max; return "Пожалуйста, введите на " + n + " символ" + (n == 1 ? "" : ((n > 1)&&(n < 5)? "а" : "ов")) + " меньше"; }',
			'formatSelectionTooBig'=> 'js:function (limit) { return "Вы можете выбрать не более " + limit + " элемент" + (limit == 1 ? "а" : "ов"); }',
			'formatLoadMore'=>'js:function (pageNumber) { return "Загрузка данных..."; }',
			'formatSearching'=> 'js:function () { return "Поиск..."; }'
        );
    }

    public function run()
    {
        if ($this->selector == null) {
			
            list($this->name, $this->id) = $this->resolveNameId();
            $this->selector = '#' . $this->id;

            if (isset($this->htmlOptions['placeholder']))
                $this->options['placeholder'] = $this->htmlOptions['placeholder'];

            if (!isset($this->htmlOptions['multiple'])) {
                $data = array();
                if (isset($this->options['placeholder']))
                    $data[''] = '';
                $this->data = $data + ($this->data ? $this->data : []);
            }			
            
            if ($this->hasModel())
            {				
                if (isset($this->options['ajax']))
                    echo CHtml::activeHiddenField($this->model, $this->attribute, $this->htmlOptions);
                else
                    echo CHtml::activeDropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);

            }
            elseif (!isset($this->options['ajax']))
            {
                $this->htmlOptions['id'] = $this->id;				
                echo CHtml::dropDownList($this->name, $this->value, $this->data, $this->htmlOptions);
            }
            else
            {
                echo CHtml::hiddenField($this->name, $this->value, $this->htmlOptions);
            }
        }

        $bu = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/');
        $cs = Yii::app()->clientScript;
		
        $cs->registerCssFile($bu . '/select2.css');

        if (YII_DEBUG)
            $cs->registerScriptFile($bu . '/select2.js', CClientScript::POS_END);
        else
            $cs->registerScriptFile($bu . '/select2.min.js', CClientScript::POS_END);

		if ($this->sortable) {
			$cs->registerCoreScript('jquery.ui');
		}
		
        $options = CJavaScript::encode(CMap::mergeArray($this->defaultOptions, $this->options));
        ob_start();
        echo "jQuery('{$this->selector}').select2({$options})";
        foreach ($this->events as $event => $handler)
            echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";
		echo ';';
		if ($this->sortable) {
			echo <<<JavaScript
jQuery('{$this->selector}').select2("container").find("ul.select2-choices").sortable({
	containment: 'parent',
	start: function() { jQuery('{$this->selector}').select2("onSortStart"); },
	update: function() { jQuery('{$this->selector}').select2("onSortEnd"); }
});
JavaScript;
		}

        $cs->registerScript(__CLASS__ . '#' . $this->id, ob_get_clean());
        
    }

}
