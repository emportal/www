<?php
/**
 * YiiMailer class - wrapper for PHPMailer
 * Yii extension for sending emails using views and layouts
 * Copyright (c) 2013 YiiMailer
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *
 * @package YiiMailer
 * @author Vernes Šiljegović
 * @copyright  Copyright (c) 2013 YiiMailer
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version 1.1, 2013-02-02
 */



/**
 * Include the the PHPMailer class.
 */
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'PHPMailer'.DIRECTORY_SEPARATOR.'class.phpmailer.php');

/**
 * Define the name of the default config file
 */
define('DEFAULT_CONFIG_FILE','mail.php');

class YiiMailer extends PHPMailer {

	private $viewPath='application.views.mail';

	private $layoutPath='application.views.mail.layouts';

	private $baseDirPath='webroot.images.mail';

	public $layout;

	private $view;

	private $data;

	public $CharSet='UTF-8';

	public $AltBody='';

	public $FromEmail = 'robot@emportal.ru';

	/**
	 * Set and configure initial parameters
	 * @param string $layout Layout file
	 */
	public function __construct($view='', $data=array(), $layout='', $configFile=DEFAULT_CONFIG_FILE)
	{
		if($configFile !== NULL) {
			//initialize config
			$config=require(Yii::getPathOfAlias('application.config').DIRECTORY_SEPARATOR.$configFile);
			$this->setConfig($config);
		}
		//set view
		$this->setView($view);
		//set data
		$this->setData($data);
		//set layout
		$this->setLayout($layout);
	}

	/**
	 * Configure parameters
	 * @param array $config Config parameters
	 * @throws CException
	 */
	private function setConfig($config)
	{
		if(!is_array($config))
			throw new CException("Configuration options must be an array!");
		foreach($config as $key=>$val)
		{
			$this->$key=$val;
		}
	}

	/**
	 * Set the view to be used
	 * @param string $view View file
	 * @throws CException
	 */
	public function setView($view)
	{
		if($view!='')
		{
			if(!is_file($this->getViewFile($this->viewPath.'.'.$view)))
				throw new CException('View "'.$view.'" not found');
			$this->view=$view;
		}
	}

	/**
	 * Get currently used view
	 * @return string View filename
	 */
	public function getView()
	{
		return $this->view;
	}

	/**
	 * Send data to be used in mail body
	 * @param array $data Data array
	 */
	public function setData($data)
	{
		$this->data=$data;
	}

	/**
	 * Get current data array
	 * @return array Data array
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Set layout file to be used
	 * @param string $layout Layout filename
	 * @throws CException
	 */
	public function setLayout($layout)
	{
		if($layout!='')
		{
			if(!is_file($this->getViewFile($this->layoutPath.'.'.$layout)))
				throw new CException('Layout "'.$layout.'" not found!');
			$this->layout=$layout;
		}
	}

	/**
	 * Get current layout
	 * @return string Layout filename
	 */
	public function getLayout()
	{
		return $this->layout;
	}

	/**
	 * Set path for email views
	 * @param string $path Yii path
	 * @throws CException
	 */
	public function setViewPath($path)
	{
		if (!is_string($path) && !preg_match("/[a-z0-9\.]/i",$path))
			throw new CException('Path "'.$path.'" not valid!');
		$this->viewPath=$path;
	}

	/**
	 * Get path for email views
	 * @return string Yii path
	 */
	public function getViewPath()
	{
		return $this->viewPath;
	}

	/**
	 * Set path for email layouts
	 * @param string $path Yii path
	 * @throws CException
	 */
	public function setLayoutPath($path)
	{
		if (!is_string($path) && !preg_match("/[a-z0-9\.]/i",$path))
			throw new CException('Path "'.$path.'" not valid!');
		$this->layoutPath=$path;
	}

	/**
	 * Get path for email layouts
	 * @return string Yii path
	 */
	public function getLayoutPath()
	{
		return $this->layoutPath;
	}

	/**
	 * Set path for images to embed in email messages
	 * @param string $path Yii path
	 * @throws CException
	 */
	public function setBaseDirPath($path)
	{
		if (!is_string($path) && !preg_match("/[a-z0-9\.]/i",$path))
			throw new CException('Path "'.$path.'" not valid!');
		$this->baseDirPath=$path;
	}

	/**
	 * Get path for email images
	 * @return string Yii path
	 */
	public function getBaseDirPath()
	{
		return $this->baseDirPath;
	}

	/**
	 * Find the view file for the given view name
	 * @param string $viewName Name of the view
	 * @return string The file path or false if the file does not exist
	 */
	public function getViewFile($viewName)
	{
		//In web application, use existing method
		if(isset(Yii::app()->controller))
			return Yii::app()->controller->getViewFile($viewName);
		//resolve the view file
		
		if(empty($viewName))
			return false;

		$viewFile=Yii::getPathOfAlias($viewName);
		if(is_file($viewFile.'.php'))
			return Yii::app()->findLocalizedFile($viewFile.'.php');
		else
			return false;
	}

	/**
	 * Render the view file
	 * @param string $viewName Name of the view
	 * @param array $viewData Data for extraction
	 * @return string The rendered result
	 * @throws CException
	 */
	public function renderView($viewName,$viewData=null)
	{
		//resolve the file name
		if(($viewFile=$this->getViewFile($viewName))!==false)
		{
			//use controller instance if available or create dummy controller for console applications
			if(isset(Yii::app()->controller))
				$controller=Yii::app()->controller;
			else
				$controller=new CController(__CLASS__);

			//render and return the result
			return $controller->renderInternal($viewFile,$viewData,true);
		}
		else
		{
			//file name does not exist
			throw new CException('View "'.$viewName.'" does not exist!');
		}

	}

	/**
	 * Generates HTML email, with or without layout
	 */
	public function render()
	{
		//render body
		$body=$this->renderView($this->viewPath.'.'.$this->view, $this->data);
		if($this->layout)
		{
			//has layout
			$this->MsgHTMLWithLayout($body, Yii::getPathOfAlias($this->baseDirPath));
		}
		else
		{
			//no layout
			$this->MsgHTML($body, Yii::getPathOfAlias($this->baseDirPath));
		}
	}

	/**
	 * Render HTML email message with layout
	 * @param string $message Email message
	 * @param string $basedir Path for images to embed in message
	 */
	protected function MsgHTMLWithLayout($message, $basedir = '')
	{
		$this->MsgHTML($this->renderView($this->layoutPath.'.'.$this->layout, array('content'=>$message,'data'=>$this->data)), $basedir);
	}

  	public function CreateHeader() {
	    $result = '';

	    // Set the boundaries
	    $uniq_id = md5(uniqid(time()));
	    $this->boundary[1] = 'b1_' . $uniq_id;
	    $this->boundary[2] = 'b2_' . $uniq_id;
	    $this->boundary[3] = 'b3_' . $uniq_id;

	    if ($this->MessageDate == '') {
	      $result .= $this->HeaderLine('Date', self::RFCDate());
	    } else {
	      $result .= $this->HeaderLine('Date', $this->MessageDate);
	    }

	    if ($this->ReturnPath) {
	      $result .= $this->HeaderLine('Return-Path', trim($this->ReturnPath));
	    } elseif ($this->Sender == '') {
	      $result .= $this->HeaderLine('Return-Path', trim($this->FromEmail));
	    } else {
	      $result .= $this->HeaderLine('Return-Path', trim($this->Sender));
	    }

	    // To be created automatically by mail()
	    if($this->Mailer != 'mail') {
	      if ($this->SingleTo === true) {
	        foreach($this->to as $t) {
	          $this->SingleToArray[] = $this->AddrFormat($t);
	        }
	      } else {
	        if(count($this->to) > 0) {
	          $result .= $this->AddrAppend('To', $this->to);
	        } elseif (count($this->cc) == 0) {
	          $result .= $this->HeaderLine('To', 'undisclosed-recipients:;');
	        }
	      }
	    }

	    $from = array();
	    $from[0][0] = trim($this->FromEmail);
	    $from[0][1] = $this->FromName;
	    $result .= $this->AddrAppend('From', $from);

	    // sendmail and mail() extract Cc from the header before sending
	    if(count($this->cc) > 0) {
	      $result .= $this->AddrAppend('Cc', $this->cc);
	    }

	    // sendmail and mail() extract Bcc from the header before sending
	    if((($this->Mailer == 'sendmail') || ($this->Mailer == 'mail')) && (count($this->bcc) > 0)) {
	      $result .= $this->AddrAppend('Bcc', $this->bcc);
	    }

	    if(count($this->ReplyTo) > 0) {
	      $result .= $this->AddrAppend('Reply-To', $this->ReplyTo);
	    }

	    // mail() sets the subject itself
	    if($this->Mailer != 'mail') {
	      $result .= $this->HeaderLine('Subject', $this->EncodeHeader($this->SecureHeader($this->Subject)));
	    }

	    if($this->MessageID != '') {
	      $result .= $this->HeaderLine('Message-ID', $this->MessageID);
	    } else {
	      $result .= sprintf("Message-ID: <%s@%s>%s", $uniq_id, $this->ServerHostname(), $this->LE);
	    }
	    $result .= $this->HeaderLine('X-Priority', $this->Priority);
	    if ($this->XMailer == '') {
	        $result .= $this->HeaderLine('X-Mailer', 'PHPMailer '.$this->Version.' (http://code.google.com/a/apache-extras.org/p/phpmailer/)');
	    } else {
	      $myXmailer = trim($this->XMailer);
	      if ($myXmailer) {
	        $result .= $this->HeaderLine('X-Mailer', $myXmailer);
	      }
	    }

	    if($this->ConfirmReadingTo != '') {
	      $result .= $this->HeaderLine('Disposition-Notification-To', '<' . trim($this->ConfirmReadingTo) . '>');
	    }

	    // Add custom headers
	    for($index = 0; $index < count($this->CustomHeader); $index++) {
	      $result .= $this->HeaderLine(trim($this->CustomHeader[$index][0]), $this->EncodeHeader(trim($this->CustomHeader[$index][1])));
	    }
	    if (!$this->sign_key_file) {
	      $result .= $this->HeaderLine('MIME-Version', '1.0');
	      $result .= $this->GetMailMIME();
	    }

	    return $result;
	 }

  	public function SetFrom($address, $name = '', $auto = 1) {
	    $address = trim($address);
	    $name = trim(preg_replace('/[\r\n]+/', '', $name)); //Strip breaks and trim
	    if (!$this->ValidateAddress($address)) {
	      $this->SetError($this->Lang('invalid_address').': '. $address);
	      if ($this->exceptions) {
	        throw new phpmailerException($this->Lang('invalid_address').': '.$address);
	      }
	      if ($this->SMTPDebug) {
	        $this->edebug($this->Lang('invalid_address').': '.$address);
	      }
	      return false;
	    }
	    $this->FromEmail = $address;
	    $this->FromName = $name;
	    if ($auto) {
	      if (empty($this->ReplyTo)) {
	        $this->AddAnAddress('Reply-To', $address, $name);
	      }
	      // if (empty($this->Sender)) {
	      //   $this->Sender = $address;
	      // }
	    }
	    return true;
  	}


}