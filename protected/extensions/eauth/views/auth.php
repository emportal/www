<style>
<!--
.auth-service {
	margin: 0 0 10px 0;
    display: inline-block;
}
-->
</style>
<div class="auth-services">
  <div class="auth-services clear">
  	<div class="auth-service vkontakte" style="">
  		<?php echo CHtml::Link('<button class="auth_btn auth_vkontakte" value="Вконтакте"><span class="auth_icon"></span><span class="auth_label">Вконтакте</span></button>', SocialUserIdentity::vkLoginUrl(), ['class' => 'auth-link vkontakte', 'title' => 'Войти через Вконтакте']); ?>
  	</div>
	<?php
	$N = 0;
	foreach ($services as $name => $service) {
		$N++;
		$btnName = $name;
		if($name == 'facebook') $btnName = 'Facebook';
		if($name == 'odnoklassniki') $btnName = 'Одноклассники';
		echo '<div class="auth-service '.$service->id.'" style="'.(($N%2==1) ? 'margin-left: 7px;' : '').'">';
		$html = '<button class="auth_btn auth_'.$name.'" value="'.$btnName.'"><span class="auth_icon"></span><span class="auth_label">'.$btnName.'</span></button>';
		//$html .= '<span class="auth-title">'.$service->title.'</span>';
		if($name !== 'odnoklassniki' || ($name == 'odnoklassniki' && in_array($_SERVER['HTTP_HOST'], ['emportal.ru', 'dev1.emportal.ru', 'samozapis.emportal.ru', 'moskva.emportal.ru', 'nizhny-novgorod.emportal.ru', 'novosibirsk.emportal.ru', 'ekaterinburg.emportal.ru']))) {
			$html = CHtml::link($html, array($action, 'service' => $name), array(
				'class' => 'auth-link '.$service->id,
			));
		} else {
			$html = CHtml::link($html, 'https://emportal.ru/login?service='.$name, array(
				'class' => 'auth-link '.$service->id,
			));
		}
		echo $html;
		echo '</div>'.(($N%2==1)?'<br>':'') ;
	}
	?>
  </div>
</div>