<?php

/**
 * ELinkBehavior class file.
 *
 * @author neosonic <neosonic@inbox.ru>
 * @copyright Copyright &copy; 2013 NeoSonic
 */
class ELinkBehavior extends CActiveRecordBehavior {

	/**
	 * Finds model by link attribute
	 * @param $link
	 * @return CActiveRecord model
	 */
	public function findByLink($link) {

		if ( !in_array('link', $this->owner->tableSchema->columnNames) )
				throw new CDbException('Current model don`t have link column!');;

		$model = $this->owner->findByAttributes(array(
			'link' => $link,
		));
		return $model;
	}

}
