<?php

class AmoCRM {

	private $login = 'dima90-dima@mail.ru';
	private $secret_hash = 'aa84709e34de28d19b4408754535482d';
	private $url = 'https://emportal.amocrm.ru/private/api/';
	private $apiVersion = 'v2/';
	private $type = 'json';
	private $user = '1159959';

	public $customFields = [
		'contacts' => [
			'phone' => '1511776',
			'email' => '1511778',
		],
		'leads' => [
			'id' => '1522728',
			'description' => '1522956',
			'appType' => '1522958',
			'address' => '1522960',
			'region' => '1556616',
			'createdDate' => '1557936',
			'plannedTime' => '1557950',
		],
		'companies' => [
			'id' => '1534452',
			'phone' => '1511776',
			'email' => '1511778',
			'address' => '1511784',
			'addresses' => '1534522',
			'region' => '1558668',
		],
	];

	const VISITED = '142'; //хотим взять деньги
	const DECLINED = '143'; //заявка отменена
	const SEND_TO_REGISTER = '12864084'; //заявка введена в систему
	const SEEN = '12864087'; //заявка просмотрена

	public $empStatuses = [
		'0' => self::VISITED,
		'1' => self::DECLINED,
		'2' => self::VISITED,
	];

	public $empRegions = [
		'moskva' => ['leads' => '3691886', 'companies' => '3696554'],
		'spb' => ['leads' => '3691888', 'companies' => '3696556'],
		'nizhny-novgorod' => ['leads' => '3691890', 'companies' => '3696558'],
		'novosibirsk' => ['leads' => '3691892', 'companies' => '3696560'],
		'ekaterinburg' => ['leads' => '3691894', 'companies' => '3696562'],
		'kazan' => ['leads' => '3691896', 'companies' => '3696564'],
		'samara' => ['leads' => '3691898', 'companies' => '3696566'],
		'chelyabinsk' => ['leads' => '3691900', 'companies' => '3696568'],
		'omsk' => ['leads' => '3691902', 'companies' => '3696570'],
		'rostov-na-donu' => ['leads' => '3691904', 'companies' => '3696572'],
		'ufa' => ['leads' => '3691906', 'companies' => '3696574'],
		'krasnoyarsk' => ['leads' => '3691908', 'companies' => '3696576'],
		'perm' => ['leads' => '3691910', 'companies' => '3696578'],
		'volgograd' => ['leads' => '3691912', 'companies' => '3696580'],
		'voronezh' => ['leads' => '3691914', 'companies' => '3696582'],
		'irkutsk' => ['leads' => '3691916', 'companies' => '3696584'],
		'saratov' => ['leads' => '3691918', 'companies' => '3696586'],
		'tyumen' => ['leads' => '3691920', 'companies' => '3696588'],
		'vladivostok' => ['leads' => '3691922', 'companies' => '3696590'],
		'yaroslavl' => ['leads' => '3691924', 'companies' => '3696592'],
	];

	public $empAppType = [
		'api' => '3692254',
		'partner_7940' => '3692256',
		'partner_8449' => '3692258',
		'doctorpiter' => '3692260',
		'partner_7062' => '3692262',
		'medsovet' => '3692264',
		'appForm' => '3692266',
		'ext' => '3692268',
		'phone_emportal' => '3692270',
		'phone_media' => '3692272',
		'phone_medsovet' => '3692274',
		'phone_warhola' => '3692276',
		'phone_zoon' => '3692278',
		'mobile' => '3692280',
		'vk_app' => '3692282',
		'partner_6132' => '3692284',
		'site' => '3692286',
		'guest' => '3692288',
		'Samozapis' => '3692290',
		'own' => '3692292',
	];

	public function callServer($method, $params = [], $request = 'GET') {
		$this->auth();
		if($method == 'unsorted/list') {
			$link = 'https://emportal.amocrm.ru/api/'.$method.'?api_key='.$this->secret_hash.'&login='.$this->login;
		} else {
			$link = $this->url.$this->apiVersion.$this->type.'/'.$method;
		}
		$out = $this->callServerCURL($link,$params,$request);
		$response=json_decode($out,true);

		return $response;
	}

	public function auth() {
		$user=[
  			'USER_LOGIN' => $this->login,
  			'USER_HASH' => $this->secret_hash,
		];

		$link = $this->url.'auth.php?type='.$this->type;
		$this->callServerCURL($link,$user,'POST');
	}

	public function callServerCURL($link, $params = [], $request = 'GET') {
		$curl=curl_init(); 
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
		curl_setopt($curl,CURLOPT_URL,$link);
		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,$request);

		if($request == 'POST') {
			curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($params));
		}

		curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		curl_setopt($curl,CURLOPT_HEADER,false);
		curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/amoCookie.txt');
		curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/amoCookie.txt');
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
		$out=curl_exec($curl);
		$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
		curl_close($curl);

		$code=(int)$code;
		$errors=array(
			301=>'Moved permanently',
			400=>'Bad request',
			401=>'Unauthorized',
			403=>'Forbidden',
			404=>'Not found',
			500=>'Internal server error',
			502=>'Bad gateway',
			503=>'Service unavailable'
		);
		try {
			if($code!=200 && $code!=204) {
		    	throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
			}
		}
		catch(Exception $e) {
			//die('Ошибка: '.$e->getMessage().PHP_EOL.'Код ошибки: '.$e->getCode());
			return false;
		}

		return $out;
	}

	public function getLeadsList($query = '') {
		$leadsList = $this->callServer('leads/list?query='.$query);
		return $leadsList;
	}

	public function getLeadId($id) {
		$lead = $this->getLeadsList($id);
		if($lead && is_array($lead)) {
			$leadId = $lead['response']['leads'][0]['id'];
			if($leadId) {
				return $leadId;
			}
		}
		return false;
	}

	public function setLead($appointment) {
		if($appointment->statusId == AppointmentToDoctors::DISABLED) {
			return;
		}

		$leadId = $this->getLeadId($appointment->link);
		if($leadId) {

			$status = $this->empStatuses[$appointment->adminClinicStatusId];
			if(in_array($appointment->appType, [AppointmentToDoctors::APP_TYPE_SAMOZAPIS,AppointmentToDoctors::APP_TYPE_FROM_APP_FORM,AppointmentToDoctors::APP_TYPE_VK_APP,AppointmentToDoctors::APP_TYPE_OWN]) || $appointment->statusId == AppointmentToDoctors::DECLINED_BY_PATIENT) {
				$status = self::DECLINED;
			}

			$leads['request']['leads']['update'] = [
				[
					'id' => $leadId,
					'last_modified'=>time()+420,
					'status_id' => $status,
    				'custom_fields' => [
    					[
    						'id' => $this->customFields['leads']['appType'],
    						'values' => [
    							['value' => $this->empAppType[$appointment->appType]]
    						],
    					],
    				],
				]
			];
		}
		else {
			$doc = $appointment->doctor->name ? $appointment->doctor->name : $appointment->addressService->service->name;
			$description = "Время создания: ".$appointment->createdDate."\n";
			$description .= "Время визита: ".$appointment->plannedTime."\n";
			$description .= "Клиника, адрес, врач: ".$appointment->company->name.", ".$appointment->address->name.", ".$doc."\n";

			$price = $appointment->comission;
			$status = $this->empStatuses[$appointment->adminClinicStatusId];
			if(in_array($appointment->appType, [AppointmentToDoctors::APP_TYPE_SAMOZAPIS,AppointmentToDoctors::APP_TYPE_FROM_APP_FORM,AppointmentToDoctors::APP_TYPE_VK_APP,AppointmentToDoctors::APP_TYPE_OWN])) {
				$price = 0;
				$status = self::DECLINED;
			}

			if($appointment->statusId == AppointmentToDoctors::DECLINED_BY_PATIENT) {
				$status = self::DECLINED;
			}

			$leads['request']['leads']['add'] = [
				[
					'name' => 'Заявка на прием '.$appointment->link,
					'date_create'=>time(),
					'status_id' => $status,
					'price'=> $price,
    				'responsible_user_id'=> $this->user,
    				'custom_fields' => [
    					[
    						'id' => $this->customFields['leads']['id'],
    						'values' => [
    							['value' => $appointment->link]
    						],
    					],
    					[
    						'id' => $this->customFields['leads']['description'],
    						'values' => [
    							['value' => $description]
    						],
    					],
    					[
    						'id' => $this->customFields['leads']['appType'],
    						'values' => [
    							['value' => $this->empAppType[$appointment->appType]]
    						],
    					],
       					[
    						'id' => $this->customFields['leads']['address'],
    						'values' => [
    							['value' => $appointment->address->name]
    						],
    					],
       					[
    						'id' => $this->customFields['leads']['region'],
    						'values' => [
    							['value' => $this->empRegions[$appointment->company->denormCitySubdomain]['leads']]
    						],
    					],
       					[
    						'id' => $this->customFields['leads']['createdDate'],
    						'values' => [
    							['value' => $appointment->createdDate]
    						],
    					],
       					[
    						'id' => $this->customFields['leads']['plannedTime'],
    						'values' => [
    							['value' => $appointment->plannedTime]
    						],
    					],
    				],
				]
			];
		}

		//print_r($leads);
		$result = $this->callServer('leads/set',$leads,'POST');
		if(!$leadId) {
			$leadId = $result['response']['leads']['add'][0]['id'];
			$this->setContact($appointment,$leadId);
			$this->setCompany($appointment,$leadId);
		}


	}

	public function getContactsList($query = '') {
		$contactsList = $this->callServer('contacts/list?query='.$query);
		return $contactsList;
	}

	public function getContactId($phone) {
		$phone = preg_replace('/\s|\+|-|\(|\)/','', $phone);
		$contact = $this->getContactsList($phone);
		if($contact && is_array($contact)) {
			$contactId = $contact['response']['contacts'][0]['id'];
			if($contactId) {
				return $contactId;
			}
		}
		return false;
	}

	public function getContactLeads($phone) {
		$phone = preg_replace('/\s|\+|-|\(|\)/','', $phone);
		$contact = $this->getContactsList($phone);
		if($contact && is_array($contact)) {
			$contactLeads = $contact['response']['contacts'][0]['linked_leads_id'];
			if(is_array($contactLeads)) {
				return $contactLeads;
			}
		}
		return [];
	}

	public function setContact($appointment,$leadId = NULL) {
		$contactId = $this->getContactId($appointment->phone);
		if($contactId && $leadId) {
			$contacts['request']['contacts']['update'] = [
				[
					'id' => $contactId,
					'name' => $appointment->name,
					'linked_leads_id' => array_merge($this->getContactLeads($appointment->phone),[$leadId]),
					'last_modified' => time()+420,
					'company_name' => 'Пациент ЕМП',
				],
			];
		}
		else {
			$contacts['request']['contacts']['add'] = [
				[
					'name' => $appointment->name,
					'linked_leads_id' => [$leadId],
					'company_name' => 'Пациент ЕМП',
					'custom_fields' => [
						[
	    						'id' => $this->customFields['contacts']['phone'],
	    						'values' => [
	    							['value' => $appointment->phone, 'enum' => 'MOB']
	    						],						
						],
						[
	    						'id' => $this->customFields['contacts']['email'],
	    						'values' => [
	    							['value' => $appointment->email]
	    						],						
						]
					],
				],
			];
		}

		return $this->callServer('contacts/set',$contacts,'POST');
	}

	public function getCompanyList($query = '') {
		$companyList = $this->callServer('company/list?query='.$query);
		return $companyList;
	}

	public function getCompanyId($id) {
		$company = $this->getCompanyList($id);
		if($company && is_array($company)) {
			$companyId = $company['response']['contacts'][0]['id'];
			if($companyId) {
				return $companyId;
			}
		}
		return false;
	}

	public function getCompanyLeads($id) {
		$company = $this->getCompanyList($id);
		if($company && is_array($company)) {
			$companyLeads = $company['response']['contacts'][0]['linked_leads_id'];
			if(is_array($companyLeads)) {
				return $companyLeads;
			}
		}
		return [];
	}

	public function setCompany($appointment,$leadId = NULL) {
		$companyId = $this->getCompanyId($appointment->company->link);
		if($companyId && $leadId) {
			$contacts['request']['contacts']['update'] = [
				[
					'id' => $companyId,
					'linked_leads_id' => array_merge($this->getCompanyLeads($appointment->company->link),[$leadId]),
					'last_modified' => time()+420,
					'custom_fields' => [
       					[
    						'id' => $this->customFields['companies']['region'],
    						'values' => [
    							['value' => $this->empRegions[$appointment->company->denormCitySubdomain]['companies']]
    						],
    					],
					],
				],
			];
		}
		else {
			$addressInfo = '';
			foreach ($appointment->company->addresses as $value) {
				$addressInfo .= $value->name.', ';
				if (!empty($phones = $value->phones)) {
					foreach($phones as $phone) {
						$addressInfo .= CHtml::encode($phone->name) . ', ';
					}
				}
				$addressInfo .= $value->userMedicals->email."\n";
			}
			$contacts['request']['contacts']['add'] = [
				[
					'name' => $appointment->company->name,
					'linked_leads_id' => [$leadId],
					'custom_fields' => [
						[
	    						'id' => $this->customFields['companies']['id'],
	    						'values' => [
	    							['value' => $appointment->company->link]
	    						],						
						],
						[
	    						'id' => $this->customFields['companies']['address'],
	    						'values' => [
	    							['value' => $appointment->company->address->name]
	    						],						
						],
						[
	    						'id' => $this->customFields['companies']['addresses'],
	    						'values' => [
	    							['value' => $addressInfo]
	    						],						
						],
       					[
    						'id' => $this->customFields['companies']['region'],
    						'values' => [
    							['value' => $this->empRegions[$appointment->company->denormCitySubdomain]['companies']]
    						],
    					],
					],
				],
			];
		}
		$this->callServer('company/set',$contacts,'POST');
	}

	public function setRew($review) {
		$contactId = $this->getContactId($review->userPhone);
		if(!$contactId) {
			$appointment = (object)[
				"name" => $review->userName,
				"phone" => $review->userPhone,
				"email" => "",
			];
			$result = $this->setContact($appointment);
			$contactId = $result['response']['contacts']['add'][0]['id'];
		}

		$text =  "Добавил отзыв\n";
		$text .= $review->company->name.", ";
		$text .= $review->address->name." ";
		$text .= $review->doctor->name."\n";
		$text .= $review->reviewText;
		$notes['request']['notes']['add'] = [
			[
			    'element_id'=> $contactId,
			    'element_type'=> 1,
			    'note_type'=> 4,
			    'text'=> $text,
			    'responsible_user_id' => $this->user,				
			]
		];

		$this->callServer('notes/set',$notes,'POST');
	}

	public function getUnsortedList() {
		$unsortedList = $this->callServer('unsorted/list');
		return $unsortedList;
	}

	public function getAccountsInfo() {
		$accountsInfo = $this->callServer('accounts/current');
		return $accountsInfo;
	}

}