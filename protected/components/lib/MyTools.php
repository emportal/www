<?php

class MyTools extends CComponent {

	public static function sklon($text, $padeg) {
		$credentials = array('Username' => 'test',
			'Password' => 'test');

		$header = new SOAPHeader('http://morpher.ru/', 'Credentials', $credentials);

		$url = 'http://morpher.ru/WebService.asmx?WSDL';

		$client = new SoapClient($url);

		$client->__setSoapHeaders($header);

		$params = array('parameters' => array('s' => $text));

		$result = (array) $client->__soapCall('GetXml', $params);

		$singular = (array) $result['GetXmlResult'];

		return $singular[$padeg];
	}

	public static function switcher($text, $arrow = 0) {
		$str[0] = array(
			'й' => 'q', 'ц' => 'w', 'у' => 'e', 'к' => 'r', 'е' => 't', 'н' => 'y', 'г' => 'u', 'ш' => 'i', 'щ' => 'o',
			'з' => 'p', 'х' => '[', 'ъ' => ']', 'ф' => 'a', 'ы' => 's', 'в' => 'd', 'а' => 'f', 'п' => 'g', 'р' => 'h',
			'о' => 'j', 'л' => 'k', 'д' => 'l', 'ж' => ';', 'э' => '\'', 'я' => 'z', 'ч' => 'x', 'с' => 'c', 'м' => 'v',
			'и' => 'b', 'т' => 'n', 'ь' => 'm', 'б' => ',', 'ю' => '.', 'Й' => 'Q', 'Ц' => 'W', 'У' => 'E', 'К' => 'R',
			'Е' => 'T', 'Н' => 'Y', 'Г' => 'U', 'Ш' => 'I', 'Щ' => 'O', 'З' => 'P', 'Х' => '[', 'Ъ' => ']', 'Ф' => 'A',
			'Ы' => 'S', 'В' => 'D', 'А' => 'F', 'П' => 'G', 'Р' => 'H', 'О' => 'J', 'Л' => 'K', 'Д' => 'L', 'Ж' => ';',
			'Э' => '\'', '?' => 'Z', 'ч' => 'X', 'С' => 'C', 'М' => 'V', 'И' => 'B', 'Т' => 'N', 'Ь' => 'M', 'Б' => ',', 'Ю' => '.',);
		$str[1] = array('q' => 'й', 'w' => 'ц', 'e' => 'у', 'r' => 'к', 't' => 'е', 'y' => 'н', 'u' => 'г', 'i' => 'ш', 'o' => 'щ',
			'p' => 'з', '[' => 'х', ']' => 'ъ', 'a' => 'ф', 's' => 'ы', 'd' => 'в', 'f' => 'а', 'g' => 'п', 'h' => 'р', 'j' => 'о',
			'k' => 'л', 'l' => 'д', ';' => 'ж', '\'' => 'э', 'z' => 'я', 'x' => 'ч', 'c' => 'с', 'v' => 'м', 'b' => 'и', 'n' => 'т',
			'm' => 'ь', ',' => 'б', '.' => 'ю', 'Q' => 'Й', 'W' => 'Ц', 'E' => 'У', 'R' => 'К', 'T' => 'Е', 'Y' => 'Н', 'U' => 'Г',
			'I' => 'Ш', 'O' => 'Щ', 'P' => 'З', '[' => 'Х', ']' => 'Ъ', 'A' => 'Ф', 'S' => 'Ы', 'D' => 'В', 'F' => 'А', 'G' => 'П',
			'H' => 'Р', 'J' => 'О', 'K' => 'Л', 'L' => 'Д', ';' => 'Ж', '\'' => 'Э', 'Z' => '?', 'X' => 'ч', 'C' => 'С', 'V' => 'М',
			'B' => 'И', 'N' => 'Т', 'M' => 'Ь', ',' => 'Б', '.' => 'Ю',);
		return strtr($text, isset($str[$arrow]) ? $str[$arrow] : array_merge($str[0], $str[1]));
	}

	public static function translit($str, $direction = 0) {
		$rus = array('я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю',);
		$lat = array('ya', 'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu',);
		return $direction === 0 ? str_replace($rus, $lat, $str) : str_replace($lat, $rus, $str);
	}

	public static function translit2($str, $direction = 0) {
		$translit = array(  
            'а' => 'a',   'б' => 'b',   'в' => 'v',  
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',  
            'и' => 'i',   'й' => 'j',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',  
            'с' => 's',   'т' => 't',   'у' => 'u',  
            'ф' => 'f',   'х' => 'h',   'ц' => 'c', 
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',  
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',  
            'э' => 'eh',   'ю' => 'yu',  'я' => 'ya',           
            'А' => 'A',   'Б' => 'B',   'В' => 'V',  
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E', 
            'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z', 
            'И' => 'I',   'Й' => 'J',   'К' => 'K', 
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH', 
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'\'', 
            'Э' => 'EH',   'Ю' => 'YU',  'Я' => 'YA',
        );
		return $direction === 0 ? strtr($str, $translit) : strtr($str, array_flip($translit));
	}

	public static function translitAuto($str) {
		if(preg_match('/^[a-zA-Z- ]+$/',$str)) {
			return MyTools::translit2($str,1);
		}
		else {
			return MyTools::translit2($str);
		}
	}

	public static function makeLinkUrl($name) {
		return preg_replace('|[\s\.]+|', '-', preg_replace('|[^a-z0-9\s\.-]+|', '', mb_strtolower(self::translit(trim($name)), 'utf8')
				)
		);
	}

	/**
	 * Cuts all non-numeric characters
	 * @param string $str Raw phone string
	 */
	public static function setMessage($str) {
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/post_log_text.txt';
		$somecontent = "\n-------------------------------\n URL: " . $_SERVER['REQUEST_URI'] . " IP " . $_SERVER['REMOTE_ADDR'] . ' | ' . date("Y-m-d H:i:s") . " - REQUEST данные \n";
		$somecontent .= $str;
		// Вначале давайте убедимся, что файл существует и доступен для записи.
		if (is_writable($filename)) {

			// В нашем примере мы открываем $filename в режиме "записи в конец".
			// Таким образом, смещение установлено в конец файла и
			// наш $somecontent допишется в конец при использовании fwrite().
			if (!$handle = fopen($filename, 'a')) {
				echo "Не могу открыть файл ($filename)";
				exit;
			}

			// Записываем $somecontent в наш открытый файл.
			if (fwrite($handle, $somecontent) === FALSE) {
				echo "Не могу произвести запись в файл ($filename)";
				exit;
			}

			fclose($handle);
		} else {
			echo "Файл $filename недоступен для записи";
		}
	}

	/**
	 * Get real ip address
	 * @return string
	 */
	public static function getUserHostAddress() {
		if (!empty($_SERVER['HTTP_X_REAL_IP'])) {   //check ip from share internet
			$ip = $_SERVER['HTTP_X_REAL_IP'];
		} elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	public static function cleanSearchText($str) {
		$str = preg_replace("/[^\w\sA-Za-zА-Яа-я-\.]/iu", "", $str);
		$str = preg_replace("/[\s\s]+/iu", " ", $str);
		$str = trim($str);
		return $str;
	}

	public static function getMonth($str) {
		$str--;
		$data = [
			'Января','Февраля',	'Марта',
			'Апреля','Мая',	'Июня',	'Июля',
			'Августа','Сентября','Октября',
			'Ноября','Декабря',
		];
		return $data[$str];
	}

	public static function getShortMonth($str) {
		$str--;
		$data = [
			'янв','фев',	'мар',
			'апр','мая',	'июн',	'июл',
			'авг','сен','окт',
			'ноя','дек',
		];
		return $data[$str];
	}
	
	/* функция преобразует кириллическую строку в url-совместимый транслит (пробелы заменяются знаком нижнего подчеркивания и т.п.) */
	public static function convertRussianToTranslit($str) {
		$tr = array(
			"А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
			"Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
			"Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
			"О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
			"У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
			"Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
			"Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
			"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
			"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
			"ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", 
			" "=> "_", "."=> "", "/"=> "_", "-"=> "_"
		);
		$urlstr = strtr($str,$tr);	
		$urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);	
		return $urlstr;
	}
	
	public static function showErrors($errors) {
		echo self::errorsToString($errors);
	}
	public static function errorsToString($errors) {
		$result = "";
		if(!is_array($errors)) $result = $errors;
		foreach ($errors as $key=>$error) {
			$result .= " - ".$key.": ";
			foreach ($error as $er) {
				$result .= $er." | ";
			}
		}
		return $result;
	}
	
	/*
	 * Расстояние между двумя точками
	 * $fA, $lA - широта, долгота 1-й точки,
	 * $fB, $lB - широта, долгота 2-й точки
	 * Написано по мотивам http://gis-lab.info/qa/great-circles.html
	 * Михаил Кобзарев <kobzarev@inforos.ru>
	 *
	*/
	public static function calculateTheDistance ($fA, $lA, $fB, $lB) {
		$EARTH_RADIUS = 6372795;
		
		// перевести координаты в радианы
		$lat1 = $fA * M_PI / 180;
		$lat2 = $fB * M_PI / 180;
		$long1 = $lA * M_PI / 180;
		$long2 = $lB * M_PI / 180;
	
		// косинусы и синусы широт и разницы долгот
		$cl1 = cos($lat1);
		$cl2 = cos($lat2);
		$sl1 = sin($lat1);
		$sl2 = sin($lat2);
		$delta = $long2 - $long1;
		$cdelta = cos($delta);
		$sdelta = sin($delta);
	
		// вычисления длины большого круга
		$y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
		$x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;
	
		//
		$ad = atan2($y, $x);
		$dist = $ad * $EARTH_RADIUS;
	
		return $dist;
	}

	public static function getMetro($metroName,$cityName=NULL) {
		$APIlink = "http://geocode-maps.yandex.ru/1.x/?format=json&geocode=";
		if(!empty($cityName)) $APIlink .= $cityName." ";
		$APIlink .= "метро ".$metroName;
		$APIlink .= "&kind=metro";
		$response["APIlink"] = $APIlink;
		$json = file_get_contents($APIlink);
		
		if(!$json) {
			$response["status"] = "ERROR";
			$response["text"] = "Нет ответа";
		} else {
			$APIResponse = json_decode($json);
			if(!$APIResponse) {
				$response["status"] = "ERROR";
				$response["text"] = "Ответ не может быть преобразован";
			} else {
				$result = NULL;
				foreach ($APIResponse->response->GeoObjectCollection->featureMember as $member) {
					$point = explode(' ', $member->GeoObject->Point->pos);
					if(trim(mb_strtolower(str_replace("метро", "", $metroName))) === trim(mb_strtolower(str_replace("метро", "", $member->GeoObject->name)))) {
						$result = [
							"name" => $metroName,
							"longitude" => $point[0],
							"latitude" => $point[1],
						];
						break;
					}
				}
				if(!empty($result)) {
					$response["status"] = "OK";
					$response["text"] = "Найдено!";
					$response["result"] = $result;
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Не найдено!";
				}
			}
		}
		return $response;
	}
	public static function getNearestMetroStationFromYandex($longitude,$latitude,$cityId="534bd8b8-e0d4-11e1-89b3-e840f2aca94f",$maxDistance=2000) {
		$APIlink = "http://geocode-maps.yandex.ru/1.x/?format=json&geocode=$longitude,$latitude&kind=metro";
		$response["APIlink"] = $APIlink;
		$json = file_get_contents($APIlink);
	
		if(!$json) {
			$response["status"] = "ERROR";
			$response["text"] = "Нет ответа";
		} else {
			$APIResponse = json_decode($json);
			if(!$APIResponse) {
				$response["status"] = "ERROR";
				$response["text"] = "Ответ не может быть преобразован";
			} else {
				$list = [];
				$metroNamesList = [];
				$existWithMaxDistanceLimit = false;
				foreach ($APIResponse->response->GeoObjectCollection->featureMember as $member) {
					$point = explode(' ', $member->GeoObject->Point->pos);
					$distance = MyTools::calculateTheDistance($latitude, $longitude, $point[1], $point[0]);
					$metroName = trim(str_replace("метро", "", $member->GeoObject->name));
					if(!in_array($metroName, $metroNamesList)) { $metroNamesList[] = $metroName; }
					else continue;
					$metroModel = MetroStation::model()->find(["condition"=>'name = :name AND cityId=:cityId', "params"=>[':name'=>$metroName, 'cityId'=>$cityId]]);
					if($metroModel) {
						$list[] = [
								"value" => $metroModel->id,
								"distance" => $distance,
								"name" => $metroName,
								"longitude" => $point[0],
								"latitude" => $point[1],
						];
						if($distance <= $maxDistance) $existWithMaxDistanceLimit = true;
					}
				}
				foreach ($list as $index=>$metro) {
					if($existWithMaxDistanceLimit) {
						if($metro["distance"] > $maxDistance && $metro["distance"] < 50000) { unset($list[$index]); }
					} else {
						if($index > 0) { unset($list[$index]); }
					}
				}
				$list = array_values($list);
				if(count($list) > 0) {
					$text = "";
					foreach ($list as $index=>$metro) {
						if($index > 0) $text .= ", ";
						$text .= $metro["name"];
					}
					$response["status"] = "OK";
					$response["text"] = $text;
					$response["list"] = $list;
				} else {
					$response["status"] = "OK";
					$response["text"] = "Метро не найдено!";
				}
			}
		}
		return $response;
	}
	public static function getDistrictFromYandex($longitude,$latitude,$cityId="534bd8b8-e0d4-11e1-89b3-e840f2aca94f") {
		try {
			$APIlink = "http://geocode-maps.yandex.ru/1.x/?format=json&geocode=$longitude,$latitude&kind=district";
			$response["APIlink"] = $APIlink;
			$json = file_get_contents($APIlink);
			if(!$json) {
				$response["status"] = "ERROR";
				$response["text"] = "Нет ответа";
			} else {
				$APIResponse = json_decode($json);
				if(!$APIResponse) {
					$response["status"] = "ERROR";
					$response["text"] = "Ответ не может быть преобразован";
				} else {
					$result = NULL;
					foreach ($APIResponse->response->GeoObjectCollection->featureMember as $member) {
						if(isset($member->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->SubAdministrativeArea->Locality->DependentLocality)) {
							$dependentLocality = $member->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea->SubAdministrativeArea->Locality->DependentLocality;
							while ($dependentLocality !== NULL) {
								$districtName = $dependentLocality->DependentLocalityName;
								$districtName = trim(str_replace("район", "", $districtName));
								$cityDistrictModel = CityDistrict::model()->find(["condition"=>'name = :name AND cityId=:cityId', "params"=>[':name'=>$districtName, 'cityId'=>$cityId]]);
								if($cityDistrictModel) {
									$result["name"] = $districtName;
									$result["districtId"] = $cityDistrictModel->id;
									break;
								}
								if(isset($dependentLocality->DependentLocality)) {
									$dependentLocality = $dependentLocality->DependentLocality;
								} else {
									$dependentLocality = NULL;
								}
							}
						}
					}
					if(!empty($result)) {
						$response["status"] = "OK";
						$response["text"] = "Район определён!";
						$response["result"] = $result;
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Не найдено";
					}
				}
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "".$e->getMessage();
		}
		return $response;
	}
	public static function ErrorReport($message, $address=[]) {
		$mail = new YiiMailer();
		if(empty($address)) {
			if(isset(Yii::app()->params['devEmail'])) {
				$mail->AddAddress(Yii::app()->params['devEmail']);
			} elseif (isset(Yii::app()->params['remarkAppstoreEmail'])) {
				$mail->AddAddress(Yii::app()->params['remarkAppstoreEmail']);
			}
		} else {
			if(is_array($address)) {
				foreach ($address as $value) {
					$mail->AddAddress($value);
				}
			}
		}
		$mail->setView('error');
		$mail->setData(array(
			'message' => $message,
		));
		$mail->render();
		$mail->From = 'robot@emportal.ru';
		$mail->FromName = 'Робот';
		$mail->Subject = "Отчёт об ошибке:";
		return $mail->Send();
	}
	public static function FIOExplode($FIO) {
		$FIO = explode(" ", $FIO);
		if(is_array($FIO)) {
			$trimFIO = [];
			foreach ($FIO as $key=>$nm) {
				if(!empty(trim($nm))) $trimFIO[] = $nm;
			}
			foreach ($trimFIO as $key=>$nm) {
				if($key == 0) {
					if(count($trimFIO)==1) $data["firstName"] = $nm;
					else $data["surName"] = $nm;
				}
				elseif($key == 1) $data["firstName"] = $nm;
				else $data["fatherName"] .= " ".$nm;
			}
			return $data;
		} else {
			$data["firstName"] = $FIO;
			return $data;
		}
	}

	// Функция проверки корректности доменного имени
	public static function validateHOST($params) {
		$subdomains = array_reverse(explode('.', $_SERVER['HTTP_HOST']));
		if(file_exists(dirname(__FILE__) . '/../../../../local.txt')) return true;
		else {
			$N = 0;
			switch ($subdomains[2]) {
				case "dev1":
				case "dev2":
				case "dev3":
				case "dev4":
				case "dev5":
				case "dev6":
				case "dev7":
				case "dev8":
				case "dev9":
					if(strtolower(array_reverse(explode(DIRECTORY_SEPARATOR, dirname(__FILE__)))[3]) === "www") { return false; }
					else { $N++; }
				case "blog":
					if(isset($subdomains[3])) {
						return false;
					} else {
						return true;
					}
				default:
					if (!isset($subdomains[2+$N])) {
						return true;
					}
					foreach ($params["regions"] as $region) {
						if($subdomains[2+$N] == $region['subdomain']) {
							if(isset($subdomains[3+$N])) {
								if(($subdomains[3+$N] === $region['samozapisSubdomain'])) {
									return true;
								}
							} else {
								return true;
							}
						}
						elseif(!isset($subdomains[3+$N])) {
							if($subdomains[2+$N] == $region['samozapisSubdomain']) {
								return true;
							}
						}
					}
					break;
			}
		}
		return false;
	}
	
	public static function stringToArgs($inputArray) {
		/* Demo inputArray
		$inputArray = array(
				'name' => 'Name',
				'address' => '123 Street Rd',
				'products[0][product_id]' => 1,
				'products[0][price]' => 12.00,
				'products[0][name]' => 'Product Name',
				'products[0][product_qty]' => 1,
				'products[1][product_id]' => 2,
				'products[1][price]' => 3.00,
				'products[1][name]' => 'Product Name',
				'products[1][product_qty]' => 1,
				'systemNotes[0]' => 'Note 1',
				'systemNotes[1]' => 'Note 2'
		);
		*/
		$result = array();
		foreach ($inputArray as $key => $val) {
			$keyParts = preg_split('/[\[\]]+/', $key, -1, PREG_SPLIT_NO_EMPTY);
		
			$ref = &$result;
		
			while ($keyParts) {
				$part = array_shift($keyParts);
		
				if (!isset($ref[$part])) {
					$ref[$part] = array();
				}
		
				$ref = &$ref[$part];
			}
		
			$ref = $val;
		}
		return $result;
	}
	
	public static function createFromTemplate($template="",$rules=[]) {
		$result = $template;
		if(!empty($template) AND is_array($rules)) {
			foreach ($rules as $key=>$value) {
				$result = str_replace($key,(!empty($value) || is_numeric($value))
					? (is_array($value)) ? implode(", ", $value) : $value
					: "", $result);
			}
		}
		return $result;
	}
	
	public static function getDateInterval($type, $d = '-') {
		
		$timeint = [
			'prevmonth' => date('d.m.Y', strtotime('-1 month')),
			'prevweek' => date('d.m.Y', strtotime('-1 week')),
			'prevday' => date('d.m.Y', strtotime('-1 day')),
			'today' => gmdate('d.m.Y', time()),
			'nextweek' => date('d.m.Y', strtotime('+1 week')),
			'nextmonth' => date('d.m.Y', strtotime('+1 month')),
			'firstDayOfPreviousWeek' => date('d.m.Y', (strtotime('this week') - 7*24*60*60)),
			'lastDayOfPreviousWeek' => date('d.m.Y', (strtotime('this week') - 1*24*60*60)),
			'firstDayOfThisWeek' => gmdate('d.m.Y', strtotime('this week', time()) ),
			'firstDayOfThisMonth' => date('d.m.Y', strtotime('first day of this month')),
			'firstDayOfPreviousMonth' => date('d.m.Y', strtotime('first day of last month')),
			'lastDayOfPreviousMonth' => date('d.m.Y', strtotime('last day of last month')),
		];
		
		switch ($type) {
			case 'lastMonth':
				return $timeint['firstDayOfPreviousMonth'] . $d . $timeint['lastDayOfPreviousMonth'];
				break;
			case 'currentMonth':
				return $timeint['firstDayOfThisMonth'] . '-' . $timeint['today'];
				break;
			case 'pastWeek':
				return $timeint['firstDayOfPreviousWeek'] . '-' . $timeint['lastDayOfPreviousWeek'];
				break;
			case 'currentWeek':
				return $timeint['firstDayOfThisWeek'] . '-' . $timeint['today'];
				break;
			case 'yesterday':
				return $timeint['prevday'] . '-' . $timeint['prevday'];
				break;
			case 'today':
				return $timeint['today'] . '-' . $timeint['today'];
				break;
			default:
				if (preg_match("/[0-9]{4}-[0-9]{2}/i", $type))
				{
					$startDate = date('d.m.Y', strtotime($type . '-01'));
					$endDate = date('d.m.Y', strtotime($type . '+1 month -1 day'));
					return $startDate . '-' . $endDate;
				} else {
					return false;
				}
				break;
		}
	}
	
	public static function convertIntervalToReportMonth($intervalStr, $d = '-') {
		$interval = explode($d, $intervalStr);
		$reportMonth = [
			date('Y-m', strtotime($interval[0])),
			date('Y-m', strtotime($interval[1])),
		];
		return $reportMonth[0];
	}
	
	public static function convertReportMonthToPeriodName($reportMonth) {
		$reportMonthExploded = explode('-', $reportMonth);
		$readableMonth = self::getRussianMonthsNames()[$reportMonthExploded[1] - 1];
		$periodName = $readableMonth . ' ' . $reportMonthExploded[0];
		return $periodName;
	}
	
	public static function getRussianMonthsNames() {
		return [
			'Январь',
			'Февраль',
			'Март',
			'Апрель',
			'Май',
			'Июнь',
			'Июль',
			'Август',
			'Сентябрь',
			'Октябрь',
			'Ноябрь',
			'Декабрь',
		];
	}
	
	public static function getRussianDayNames() {
		return [
			'Понедельник',
			'Вторник',
			'Среда',
			'Четверг',
			'Пятница',
			'Суббота',
			'Воскресенье',
		];
	}
	
	public static function getEnglishDayNames() {
		return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday', 'Sunday'];
	}
	
	public static function url_origin($s, $full=true, $use_forwarded_host=false)
	{
		$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
		$sp = strtolower($s['SERVER_PROTOCOL']);
		$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
		$port = $s['SERVER_PORT'];
		$port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
		$host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
		$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
		return $protocol . '://' . $host . (($full)?$s['REQUEST_URI']:"");
	}

	public static function getKlinikaRedirectAddress($options = []) {
		$oldChunk = '/clinic';
		$newChunk = '/klinika';
		$path_info = rtrim(str_replace($oldChunk, $newChunk, $_SERVER['PHP_SELF']), "/");
		$url_origin = self::url_origin($_SERVER, false);
		$redirectAddress = $url_origin . $path_info;
		if ($options['addressLinkUrl'])
			$redirectAddress .= '/' . $options['addressLinkUrl'];
		$redirectAddress .= (!empty($_SERVER['QUERY_STRING']) ? ("?".$_SERVER['QUERY_STRING']) : "" );
		return $redirectAddress;
	}
	
	public static function createCountableForm($num, $form1, $form2, $form3) {
		$lastDigits = (int)substr($num, -2, 2);
		return (($lastDigits >= 5 && $lastDigits <= 20) || $lastDigits%10 >= 5 || $lastDigits%10 == 0) ? $form3 : ($lastDigits%10 == 1 ? $form1 : $form2);
	}
	
	public static function getBrowser()
	{
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$bname = 'Unknown';
		$platform = 'Unknown';
		$version= "";
	
		//First get the platform?
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		}
	
		// Next get the name of the useragent yes seperately and for good reason
		if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		}
		elseif(preg_match('/Firefox/i',$u_agent))
		{
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		}
		elseif(preg_match('/Chrome/i',$u_agent))
		{
			$bname = 'Google Chrome';
			$ub = "Chrome";
		}
		elseif(preg_match('/Safari/i',$u_agent))
		{
			$bname = 'Apple Safari';
			$ub = "Safari";
		}
		elseif(preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Opera';
			$ub = "Opera";
		}
		elseif(preg_match('/Netscape/i',$u_agent))
		{
			$bname = 'Netscape';
			$ub = "Netscape";
		}
	
		// finally get the correct version number
		$known = array('Version', $ub, 'other');
		$pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		if (!preg_match_all($pattern, $u_agent, $matches)) {
			// we have no matching number just continue
		}
	
		// see how many we have
		$i = count($matches['browser']);
		if ($i != 1) {
			//we will have two since we are not using 'other' argument yet
			//see if version is before or after the name
			if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
				$version= $matches['version'][0];
			}
			else {
				$version= $matches['version'][1];
			}
		}
		else {
			$version= $matches['version'][0];
		}
	
		// check if we have a number
		if ($version==null || $version=="") {$version="?";}
	
		return array(
				'userAgent' => $u_agent,
				'name'      => $bname,
				'version'   => $version,
				'platform'  => $platform,
				'pattern'    => $pattern
		);
	}
	public static function getClientIp() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	public static function ipCIDRCheck ($IP, $CIDR) {
		list ($net, $mask) = preg_split ("/[\/]/", $CIDR);
	
		$ip_net = ip2long ($net);
		$ip_mask = ~((1 << (32 - $mask)) - 1);
	
		$ip_ip = ip2long ($IP);
	
		$ip_ip_net = $ip_ip & $ip_mask;
	
		return ($ip_ip_net == $ip_net);
	}
	public static function getUserFromCookie() {
		$result = false;
		preg_match("/\"([a-zA-Z0-9\-])*\"/", $_COOKIE["user_"],$user_);
		if(isset($user_[0])) {
			$user_id = trim($user_[0], '"');
			$result = User::model()->findByPk($user_id);
		}
		return $result;
	}
	
	public static function BuildAcronym($string) {
		$acronim = "";
		try {
			$string=preg_replace("/( )/", "_", $string);
			preg_match_all('/\b(\w)\w*\W*/', $string, $subject);
			foreach ($subject[0] as $word) {
				$word = trim($word,'_');
				$leter = mb_substr($word, 0, 1);
				if(is_numeric($leter)) { $acronim .= $word; }
				else { $acronim .= mb_substr($word, 0, 1); }
			}
		} catch (Exception $e) {
			$acronim = $string;
		}
		return $acronim;
	}

	public static function mb_ucfirst($str, $enc = 'utf-8') {
		return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
	}
	public static function mb_lcfirst($str, $enc = 'utf-8') {
		return mb_strtolower(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
	}
	
	public static function desktop_version() {
		return (intval(Yii::app()->request->cookies["desktop_version"]->value) > 0);
	}
	public static function isMobile() {
		return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}
	
	public static function StartCommand($name) {
		$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
		$runner = new CConsoleCommandRunner();
		$runner->addCommands($commandPath);
		$runner->run(['yiic', $name]);
	}
	public static function deleteDir($dirPath, $withDir=true) {
		if (! is_dir($dirPath)) {
			throw new InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				if($withDir) self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}
	
	public static function GetSearchUrl($type,$data=[],$shortSearch=null,$isVkApp=null) {
		$locationSet = false;
		$locationValue = '';
		$metroCount = 0;
		$districtCount = 0;
		$needle = -1;
		$dNeedle = -1;
		
		$formData = $data;
		/* gets metro alias, metro,district counts */
		foreach ($formData as $key => $el) {
			if (strpos($el['value'], 'metro') !== FALSE) {
				$metroCount++;
				$needle = $key;
			}
		}
		foreach ($formData as $key => $el) {
			if (strpos($el['value'], 'raion') !== FALSE) {
				$districtCount++;
				$dNeedle = $key;
			}
		}
		
		if ($metroCount === 1 && $districtCount === 0) {
			$locationValue = $formData[$needle]['value'];
			unset($formData[$needle]);
		} elseif ($districtCount === 1 && $metroCount === 0) {
			$locationValue = $formData[$dNeedle]['value'];
			unset($formData[$dNeedle]);
		}
		
		/* gets secondary filter value   profil / specialty */
		foreach ($formData as $key => $el) {
			if (strpos($el['value'], 'profile-') !== FALSE || strpos($el['value'], 'specialty-') !== FALSE || strpos($el['value'], 'service-') !== FALSE) {
				$secondaryValue = $el['value'];
				unset($formData[$key]);
			}
		}
		$getStr='';
		foreach ($formData as $key => $el) {
			if(!empty($el['value'])) {
				if(empty($getStr)) {
					$getStr = '?';
				}
				$getStr.=$el['name'].'='.$el['value'].'&';
			}
		}
		if($shortSearch) {
			if($getStr) {
				$getStr.='shortSearch=1&';
			} else {
				$getStr.='?shortSearch=1';
			}
		}

		if($isVkApp) {
			if($getStr) {
				$getStr.='vk=1&';
			} else {
				$getStr.='?vk=1';
			}
		}
		$getStr = trim($getStr,'&');
		$url = '/'.$type.($locationValue ? '/'.$locationValue : '').($secondaryValue ? '/'.$secondaryValue : '').$getStr;
		return $url;
	}
}

?>
