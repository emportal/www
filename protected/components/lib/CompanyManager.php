<?php

class CompanyManager extends CComponent {

	public static function AddCompany($data) {
		$response = [];
		try {
			if ($data) {
				$model = new Company();
				$model->attributes = $data;
				if ($model->save()) {
					$response["status"] = "OK";
					$response["text"] = "Успех: Компания добавлена";
					$response["model"] = $model;
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function EditCompany($id, $data) {
		$response = [];
		try {
			if ($data) {
				$model = Company::model()->findByPk($id);
				$model->attributes = $data;
				if ($model->update()) {
					$response["status"] = "OK";
					$response["text"] = "Успех: Компания отредактирована";
					$response["model"] = $model;
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function DeleteCompany($id) {
		$response = [];
		try {
			if (isset($id)) {
				$model = Company::model()->findByPk($id);
				if($model) {
					$model->removalFlag = 1;
					if($model->update()) {
						$response["status"] = "OK";
						$response["text"] = "Успех: Компания удалена";
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
					}
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: клиника не найдена!";
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function AddAddress($companyid, $data) {
		$response = [];
		try {
			$company = Company::model()->findByPk($companyid);
			if ($data) {
				$model = new Address();
				$model->attributes = $data;
				$model->ownerId = $companyid;
				if ($model->save()) {
					$response["status"] = "OK";
					$response["text"] = "Успех: Адрес добавлен";
					$response["model"] = $model;
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function EditAddress($id, $data) {
		$response = [];
		try {
			if ($data) {
				Address::$showInactive = true;
				$model = Address::model()->findByPk($id);
				if($model) {
					$model->attributes = $data;
					if ($model->update()) {
						$response["status"] = "OK";
						$response["text"] = "Успех: Адрес отредактирован";
						$response["model"] = $model;
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
					}
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: Адрес не найден! ";
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function DeleteAddress($id) {
		$response = [];
		try {
			if (isset($id)) {
				$model = Address::model()->findByPk($id);
				if($model) {
					$model->isActive = 0;
					if($model->update()) {
						$response["status"] = "OK";
						$response["text"] = "Успех: адрес больше не активен!";
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
					}
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: адрес не найден!";
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}
	
	public static function CreateAccount($address, $data=false) {
		$response = [];
		try {
			if($address !== NULL) {
				if (!$model = $address->userMedicals) {
					$model = new UserMedical();
					$model->name = $address->link;
					$model->addressId = $address->id;
					$model->companyId = $address->company->id;
					$model->companyContractId = $address->company->companyContracts->id;
					$model->status = 0;
					$model->agreement = 0;
					if ($data) {
						$model->attributes = $data;
						if(isset($data["password"])) {
							$pass = $data["password"]; }
					}
					if(!isset($pass)) {
						$pass = User::generate_password(6, true); }
					$model->password = md5($pass);
					if($model->save()) {
						$response["status"] = "OK";
						$response["pass"] = $pass;
						$response["text"] = "Успех: Аккаунт создан";
						$response["model"] = $model;
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
					}
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: Адрес уже имеет аккаунт";
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Адрес не найден";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function AddDoctor($data) {
		$response = [];
		try {
			if ($data) {
				$model = new Doctor('insert');
				$model->attributes = $data;
				$Erorrs = array();
				
				if ($model->validate() && count($Erorrs) === 0) {
					$doctorCurrentLink = Yii::app()->db->createCommand()->select("max(`link`)")->from(Doctor::model()->tableName())->queryScalar();
					$argDoctorCurrentLink = Yii::app()->db->createCommand()->select("max(`link`)")->from(AgrDoctor::model()->tableName())->queryScalar();
					$currentLink = max($doctorCurrentLink, $argDoctorCurrentLink);
					$currentLink++;
					$model->link = str_pad($currentLink, 9, '0', STR_PAD_LEFT);
					
					if($model->save()) {
						if(isset($data["specialtyOfDoctors"])) $model->setSpecialtyOfDoctors($data['specialtyOfDoctors']);
						if(isset($data["doctorEducations"])) $model->setDoctorEducations($data['doctorEducations']);
						if(isset($data["address"])) $model->setCurrentPlaceOfWork($data["address"]);
						$response["status"] = "OK";
						$response["text"] = "Успех: Врач добавлен";
						$response["model"] = $model;
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
					}
				}
				else {
					$Erorrs = array_merge($Erorrs, $model->getErrors());
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function EditDoctor($id, $data) {
		$response = [];
		try {
			if ($data) {
				$DoctorFilterDeleted = Doctor::$filterDeleted;
				Doctor::$filterDeleted = false;
				$model = Doctor::model()->findByPk($id);
				if($model) {
					$model->attributes = $data;
					if(isset($data['deleted'])) {
						$model->deleted = $data['deleted'];
						if($model->deleted == 1) echo " deleted ";
					}
					if ($model->update()) {
						$response["status"] = "OK";
						$response["text"] = "Успех: Врач отредактирован";
						$response["model"] = $model;
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
					}
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: Врач не найден! ";
				}
				Doctor::$filterDeleted = $DoctorFilterDeleted;
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}

	public static function DeleteDoctor($id) {
		$response = [];
		try {
			if (isset($id)) {
				$model = Doctor::model()->findByPk($id);
				if($model) {
					$model->deleted = 1;
					if($model->update()) {
						$response["status"] = "OK";
						$response["text"] = "Успех: врач удалён!";
					} else {
						$response["status"] = "ERROR";
						$response["text"] = "Ошибка: <br>" . MyTools::errorsToString($model->getErrors());
					}
				} else {
					$response["status"] = "ERROR";
					$response["text"] = "Ошибка: врач не найден!";
				}
			} else {
				$response["status"] = "ERROR";
				$response["text"] = "Ошибка: Запрос не верный";
			}
		} catch (Exception $e) {
			$response["status"] = "ERROR";
			$response["text"] = "Ошибка: ".$e->getMessage();
		}
		return $response;
	}
}

?>
