<?php


class MyRegExp extends CComponent {

	/**
	 * Cuts all non-numeric characters
	 * @param string $str Raw phone string
	 */
	public static function phoneRegExp($str) {
		return $str = preg_replace('/[^\d|*\.]/',"", $str);
	}
	
	
}

?>
