<?php

class PushNotificationSender {
	
	protected $settings;
	
	public function __construct($settings = ['url' => 'http://emp-server.azurewebsites.net/api/push']) {$this->settings = $settings;}
	
	public function sendNotification($notificationMessage) {
		
		$url = $this->settings['url'];
		$parameterString = http_build_query($notificationMessage);
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameterString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
			'Content-Length: ' . strlen($parameterString))
		);                                                                                                                   
	
		$result = curl_exec($ch);
		return $result;
	}
}