<?php

class NetrikaLoader {
	
	static $PRESET = [
			"production" => [
					"urlServer" => 'https://api.gorzdrav.spb.ru/Service/HubService.svc?singlewsdl',
					"guid" => '2F2C7ACD-6D3E-42DE-B8D3-5BA2FCE735D5',
					"systype" => 3,
					"externalSystemId" => 'api.gorzdrav.spb.ru',
			],
			"demo" => [
					"urlServer" => 'http://212.116.96.50/Service/HubService.svc?wsdl',
					"guid" => '5E39D284-F532-4024-9750-E0F0A3B39A4E',
					"systype" => 2,
					"externalSystemId" => 'Demo',
			],
	];
	static $DEFAULT_PRESET_TYPE = "production";

	public $urlServer = NULL;
	public $guid = NULL; //идентификатор подключаемого к N3.Netrika http://api.netrika.ru/
	public $systype = NULL;
	public $externalSystemId = NULL;
	public $requestDelay = 0.2;
	public $client = NULL;
	
	function __construct($type = NULL) {
		if($type === NULL) $type = self::$DEFAULT_PRESET_TYPE;
		foreach (self::$PRESET[$type] as $key=>$value) {
			$this->{$key} = $value;
		}
	}
	
	public function callServer($functionName, $params) {
		try {
			if(!isset($params["guid"])) $params["guid"] = $this->guid;
			$client = new SoapClient($this->urlServer, ['trace' => TRUE]);
			$this->client = $client;
			$result = $client->__soapCall($functionName, [$params]);
		} catch (Exception $e) {
			return false;
		}
		return $result;
	}
	
	public function populateLPUList() { //наполнить список адресов extAddress
		
		$params = ['guid' => $this->guid];
		$response = $this->callServer('GetLPUList', $params);
		if(!$response) {
			echo "Ошибка при запросе к серверу";
			return false;
		}
		$listLPU = $response->GetLPUListResult->ListLPU->Clinic;
		if(is_array($listLPU)) {
			ExtAddress::model()->updateAll(["isActive"=>0], ["condition"=>"sysTypeId=".$this->systype]);
			echo "Список адресов: ".count($listLPU).PHP_EOL;
		} else {
			echo "Нет адресов! ".PHP_EOL;
			return false;
		}
		foreach($listLPU as $data) {
			echo $data->IdLPU ."::".$data->LPUFullName . PHP_EOL;
			$attributes = [
				'name' => $data->LPUFullName,
				'extId' => $data->IdLPU,
				'extDescription' => $data->Description,
				'extShortName' => $data->LPUShortName,
				'extLPUType' => $data->LPUType,
				'extDistrictId' => $data->District,
				'sysTypeId' => $this->systype,
				'isActive' => 1,
			];
			
			$extSysType = ExtSysType::model()->findByAttributes(['id' => $this->systype]);
			if(!$extSysType) {
				$extSysType = new ExtSysType;
				$extSysType->id = $this->systype;
				if($extSysType->save()) {
					echo " - Добавил внешнюю систему".PHP_EOL;
				} else {
					echo " - Ошибки при добавлении внешней системы: " . PHP_EOL . MyTools::errorsToString($extCompany->getErrors());
					continue;
				}
			}

			$extCompany = ExtCompany::model()->findByAttributes(['sysTypeId' => $this->systype, 'extId' => $data->IdLPU]);
			$extCompanyAttributes = ['name' => $data->LPUFullName, 'sysTypeId' => $this->systype, 'extId' => $data->IdLPU];
			if ($extCompany) {
				$extCompany->attributes = $extCompanyAttributes;
				if($extCompany->update()) {
					echo " - Обновил компанию!" . PHP_EOL;
					$attributes['extCompanyId'] = $extCompany->extId;
				} else {
					echo " - Ошибки при обновлении компании: " . PHP_EOL . MyTools::errorsToString($extCompany->getErrors());
					continue;
				}
			} else {
				$extCompany = new ExtCompany;
				$extCompany->attributes = $extCompanyAttributes;
				if($extCompany->save()) {
					echo " - Добавил компанию!" . PHP_EOL;
				} else {
					echo " - Ошибки при добавлении компании: " . PHP_EOL . MyTools::errorsToString($extCompany->getErrors());
					continue;
				}
			}
			
			$attributes['extCompanyId'] = $extCompany->extId;
			
			$existingExtAddress = ExtAddress::model()->findByAttributes(['extId' => $data->IdLPU, 'sysTypeId' => $this->systype]);
			
			if ($existingExtAddress) {
				$existingExtAddress->attributes = $attributes;
				if($existingExtAddress->update()) {
					echo " - Обновил адрес!" . PHP_EOL;
				} else {
					echo " - Ошибки при обновлении адреса: " . PHP_EOL . MyTools::errorsToString($existingExtAddress->getErrors());
					continue;
				}
			} else {
				$extAddress = new ExtAddress;
				$extAddress->attributes = $attributes;
				if($extAddress->save()) {
					echo " - Добавил адрес!" . PHP_EOL;
				} else {
					echo " - Ошибки при добавлении адреса: " . PHP_EOL . MyTools::errorsToString($extAddress->getErrors());
					continue;
				}
			}
		}
	}
	
	public function populateDistrictList() { //наполнить список районов extDistrict и сопоставить их с имеющимися районами из базы
		$params = ['guid' => $this->guid];
		$response = $this->callServer('GetDistrictList', $params);
		if(!$response) {
			echo "Ошибка при запросе к серверу";
			return false;
		}
		$districtList = $response->GetDistrictListResult->District;
		echo "Список районов: " . count($districtList) . PHP_EOL;
		foreach($districtList as $data) {
			echo PHP_EOL . $data->IdDistrict .": ".$data->DistrictName.": ";
			$cityDistrictId = NULL;
			$cityDistrict = NULL;

			if(!$cityDistrict) {
				$criteria = new CDbCriteria;
				$criteria->addCondition('name = :name AND cityId = :cityId');
				$criteria->params = array( ':name'=>$data->DistrictName, ':cityId'=>'534bd8b8-e0d4-11e1-89b3-e840f2aca94f' );
				$cityDistrict = CityDistrict::model()->find($criteria);
			}
			if(!$cityDistrict) {
				$criteria = new CDbCriteria;
				$criteria->addCondition('name LIKE :name AND cityId = :cityId');
				$criteria->params = array( ':name'=>$data->DistrictName."%", ':cityId'=>'534bd8b8-e0d4-11e1-89b3-e840f2aca94f' );
				$cityDistrict = CityDistrict::model()->find($criteria);
			}
			if(!$cityDistrict) {
				$criteria = new CDbCriteria;
				$criteria->addCondition('name LIKE :name AND cityId = :cityId');
				$criteria->params = array( ':name'=>"%".$data->DistrictName."%", ':cityId'=>'534bd8b8-e0d4-11e1-89b3-e840f2aca94f' );
				$cityDistrict = CityDistrict::model()->find($criteria);
			}
			
			if(!$cityDistrict) {
				echo " !!! Район не удалось сопоставить! :: ";
			} else {
				echo " == ".$cityDistrict->name . ":: ";
				$cityDistrictId = $cityDistrict->id;
			}
			
			$attributes = [
				'extId' => $data->IdDistrict, // Int
				'extName' => $data->DistrictName, // String
				'extOkato' => $data->Okato, // Int
				'cityDistrictId' => $cityDistrictId, // String cityDistrictId сопоставленный
				'sysTypeId' => $this->systype, // Идентификатор внешней системы
			];

			$existingExtDistrict = ExtDistrict::model()->findByAttributes([ 'extId' => $data->IdDistrict, 'sysTypeId' => $this->systype ]);

			if ($existingExtDistrict) {
				$existingExtDistrict->attributes = $attributes;
				if($existingExtDistrict->update()) {
					echo "Обновил район!";
				} else {
					echo "Ошибки при обновлении района: " . PHP_EOL . MyTools::errorsToString($existingExtDistrict->getErrors());
					continue;
				}
			} else {
				$extDistrict = new ExtDistrict;
				$extDistrict->attributes = $attributes;
				if($extDistrict->save()) {
					echo "Добавил район!";
				} else {
					echo "Ошибки при добавлении района: " . PHP_EOL . MyTools::errorsToString($extDistrict->getErrors());
					continue;
				}
			}
		}
	}
	
	public function populateSpecialityList() { //наполнить список доступных специальностей extSpecialty
		$extAddresses = ExtAddress::model()->findAll(['condition' => 'sysTypeId='.Yii::app()->db->quoteValue($this->systype), 'order'=>'extId ASC']);
		if(!is_array($extAddresses) || count($extAddresses) < 1) {
			echo "Нет адресов! " . PHP_EOL;
		}
		else {
			echo "адресов: ".count($extAddresses) . PHP_EOL;
			foreach($extAddresses as $extAddress) {
				echo $extAddress->extId ."::".$extAddress->name . PHP_EOL;
				sleep($this->requestDelay); // Таймаут перед очередным запросом запросом
				$params = [
					'guid' => $this->guid,
					'idLpu' => $extAddress->extId,
				];
					
				$response = $this->callServer('GetSpesialityList', $params);
				if(!$response && !isset($response->GetSpesialityListResult->ListSpesiality)) {
					echo "Ошибка при запросе к серверу";
					continue;
				}
				if(!$response->GetSpesialityListResult->Success) {
					print_r($response->GetSpesialityListResult);
					continue;
				}
				if(isset($response->GetSpesialityListResult->ListSpesiality->Spesiality)) {
					if(isset($response->GetSpesialityListResult->ListSpesiality->Spesiality->IdSpesiality)) {
						$listSpeciality = [$response->GetSpesialityListResult->ListSpesiality->Spesiality];
					} else {
						$listSpeciality = $response->GetSpesialityListResult->ListSpesiality->Spesiality;
					}
				}
					
				if(!isset($listSpeciality) || !is_array($listSpeciality)) {
					echo "Нет специальностей! " . PHP_EOL;
					continue;
				}
				foreach($listSpeciality as $data) {
					echo "-- ".$data->NameSpesiality."::";
					$attributes = [
						'extId' => $data->IdSpesiality,
						'extAddressId' => $extAddress->extId,
						'extName' => $data->NameSpesiality,
						'sysTypeId' => $this->systype,
					];
						
					$existingSpeciality = ExtSpeciality::model()->findByAttributes([
						'extId' => $data->IdSpesiality,
						'extAddressId' => $extAddress->extId,
						'sysTypeId' => $this->systype,
					]);
							
					if ($existingSpeciality) {
						$existingSpeciality->attributes = $attributes;
						if($existingSpeciality->update()) {
							echo "Обновил специальность!" . PHP_EOL;
						} else {
							echo "Ошибки при обновлении специальности: " . PHP_EOL . MyTools::errorsToString($existingSpeciality->getErrors());
							continue;
						}
					} else {
						$extSpeciality = new ExtSpeciality;
						$extSpeciality->attributes = $attributes;
						if($extSpeciality->save()) {
							echo "Добавил специальность!" . PHP_EOL;
						} else {
							echo "Ошибки при добавлении специальности: " . PHP_EOL . MyTools::errorsToString($extSpeciality->getErrors());
							continue;
						}
					}
				}
			}
		}
	}
	
	public function populateDoctorList() { //наполняем список врачей по каждому адресу
		try
		{
			$extAddresses = ExtAddress::model()->findAll([
				'condition' => 'sysTypeId='.Yii::app()->db->quoteValue($this->systype),
				'order' => 'extId ASC',
			]);
			$criteria = new CDbCriteria();
			$criteria->compare('sysTypeId', $this->systype);
			ExtDoctor::model()->updateAll(["lastUpdate"=>NULL], $criteria);
			if(!is_array($extAddresses) || count($extAddresses) < 1) {
				echo "Нет адресов! " . PHP_EOL;
			} else {
				foreach($extAddresses as $extAddress) {
					echo $extAddress->extId."::".$extAddress->name."::";
					$extSpecialities = ExtSpeciality::model()->findAllByAttributes(['sysTypeId' => $this->systype, 'extAddressId' => $extAddress->extId]);
					if(!is_array($extSpecialities) || COUNT($extSpecialities) < 1) {
						echo "Нет специальностей! " . PHP_EOL;
						continue;
					}
					foreach($extSpecialities as $extSpeciality) {
						echo $extSpeciality->extName .PHP_EOL;
						sleep($this->requestDelay); // Таймаут перед очередным запросом запросом
						$params = [
							'guid' => $this->guid,
							'idLpu' => $extAddress->extId,
							'idSpesiality' => $extSpeciality->extId
						];
						
						$response = $this->callServer('GetDoctorList', $params);
						if(!$response) {
							echo "-- Ошибка при запросе к серверу";
							return;
						}

						if(!$response->GetDoctorListResult->Success) {
							print_r($response->GetDoctorListResult);
							continue;
						}
						if(isset($response->GetDoctorListResult->Docs->Doctor)) {
							if(isset($response->GetDoctorListResult->Docs->Doctor->IdDoc)) {
								$listDoctor = [$response->GetDoctorListResult->Docs->Doctor];
							} else {
								$listDoctor = $response->GetDoctorListResult->Docs->Doctor;
							}
						}
						if(!isset($listDoctor) || !is_array($listDoctor)) {
							echo "Нет врачей! " . PHP_EOL;
							continue;
						}
						
						if(!is_array($listDoctor) || count($listDoctor) < 0) {
							echo "-- Нет врачей! ".PHP_EOL;
							continue;
						}
						foreach($listDoctor as $data) {
							echo "-- ". $data->Name ."::";
							$attributes = [
								'extId' => $data->IdDoc,
								'extSpecialityId' => $extSpeciality->extId,
								'extAddressId' => $extAddress->extId,
								'extFio' => $data->Name,
								'sysTypeId' => $this->systype,
								'deleted' => 0,
								'lastUpdate' => date("Y-m-d H:i:s",time()),
							];
							$existingDoctor = ExtDoctor::model()->findByAttributes(['extId' => $data->IdDoc, 'extAddressId' => $extAddress->extId, 'sysTypeId' => $this->systype]);
							
							if ($existingDoctor) {
								$existingDoctor->attributes = $attributes;
								if($existingDoctor->update()) {
									echo "Обновил врача!" .PHP_EOL;
								} else {
									echo "Ошибки при обновлении врача: ". PHP_EOL . MyTools::errorsToString($existingSpeciality->getErrors());
									continue;
								}
							} else {
								$extDoctor = new ExtDoctor;
								$extDoctor->attributes = $attributes;
								if($extDoctor->save()) {
									echo "Добавил врача!".PHP_EOL;
								}
								else {
									echo "Ошибки при добавлении врача: " . PHP_EOL . MyTools::errorsToString($extDoctor->getErrors());
									continue;
								}
							}
						}
					}
				}
			}
			ExtDoctor::model()->updateAll(["deleted"=>1], "lastUpdate IS NULL");
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function SendNotificationAboutAppointment($attr) {
		$params = [
			'idLpu' => 0, //integer
			'appointmentSource' => 'Интернет', //ЦТО, Инфомат, Регистратура, Врач_АПУ, Интернет, Прочее
			'guid' => $this->guid,
			'doctor' => [
					'IdDoc' => '', //string
					'Name' => '', //string
					'IdSpesiality' => '', //string
					'NameSpesiality' => '', //string
			],
			'patient' => [
					'Birthday' => '', //datetime 1989-12-11T00:00:00.000
					'IdPat' => '', //string
					'Name' => '', //string
					'Surname' => '', //string
			],
			'Аppointment' => [
					'IdAppointment' => '', //string
					'VisitStart' => '', //datetime 2015-03-25T10:00:00
					'VisitEnd' => '', //datetime 2015-03-25T10:30:00
			]
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
			
		$response = $this->callServer('SendNotificationAboutAppointment', $params);
		return $response;
	}
	
	public function getWorkingTime($extDoctor) {
		$params = [
				'guid' => $this->guid,
				'idDoc' => $extDoctor->extId,
				'idLpu' => $extDoctor->extAddressId,
				'visitStart' => strtotime ( 'now'),
				'visitEnd' => strtotime ( '+6 day'),
		];
			
		$response = $this->callServer('GetWorkingTime', $params);
		return $response;
	}
	
	public function checkPatient(
		$idLpu, $pat = [
		'AriaNumber' => null, 
		'Birthday' => null, 
		'CellPhone' => null, 
		'Document_N' => null, 
		'Document_S' => null, 
		'HomePhone' => null, 
		'IdPat' => null, 
		'Name' => null, 
		'Polis_N' => null, 
		'Polis_S' => null, 
		'SecondName' => null, 
		'Surname' => null, ]) {
		
		try {
			$params = [
				'guid' => $this->guid,
				'idLpu' => $idLpu,
				'pat' => $pat,
			];
				
			$response = $this->callServer('CheckPatient', $params);
			
			return $response->CheckPatientResult;
		} catch (Exception $e) {
			return false;
		}
	}

	public function checkDoctor($doctorId, $clinicId) {
		$criteria = new CDbCriteria();
		$criteria->compare("t.doctorId",$doctorId);
		$criteria->compare("t.sysTypeId",$this->systype);
		$criteria->with = [
				'extAddress' => [
						'together' => true,
						'select' => false,
						'joinType' => 'INNER JOIN',
						'condition' => 'extAddress.addressId = '.Yii::app()->db->quoteValue($clinicId),
				]
		];
		$extDoctor = ExtDoctor::model()->find($criteria);
		return $extDoctor;
	}
	
	public function getAvaibleAppointments($idDoc, $idLpu, $idPat, $visitStart, $visitEnd, $referralId=NULL) {
		$params = [
				'guid' => $this->guid,
				'idDoc' => $idDoc,
				'idLpu' => $idLpu,
				'idPat' => $idPat,
				'visitStart' => $visitStart,
				'visitEnd' => $visitEnd,
				'pat' => $pat,
		];
		
		$response = $this->callServer('GetAvaibleAppointments', $params);
		return $response;
	}
	public function setAppointment($idAppointment, $idLpu, $idPat, $doctorsReferal=NULL) {
		$params = [
				'guid' => $this->guid,
				'idAppointment' => $idAppointment,
				'idLpu' => $idLpu,
				'idPat' => $idPat,
				'doctorsReferal' => $doctorsReferal, // Номер направления
		];
		
		$response = $this->callServer('SetAppointment', $params);
		return $response;
	}
}
