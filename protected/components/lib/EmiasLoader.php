<?php

/*
INSERT INTO `emportal`.`extSysType` (`id`,`name`) VALUES (4,'MPGU');
ALTER TABLE `extAddress`
	CHANGE COLUMN `extCompanyId` `extCompanyId` BIGINT NOT NULL AFTER `name`,
	CHANGE COLUMN `extId` `extId` BIGINT NOT NULL COMMENT 'id адреса во внешней системе' AFTER `addressId`;
ALTER TABLE `extCompany`
	CHANGE COLUMN `extId` `extId` BIGINT NOT NULL FIRST;
ALTER TABLE `extDoctor`
	CHANGE COLUMN `extAddressId` `extAddressId` BIGINT NOT NULL AFTER `extSpecialityId`;
ALTER TABLE `company`
	CHANGE COLUMN `name` `name` CHAR(200) NOT NULL COMMENT 'Наименование' COLLATE 'utf8_unicode_ci' AFTER `id`,
	CHANGE COLUMN `linkUrl` `linkUrl` CHAR(200) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `rating`;
ALTER TABLE `address`
	CHANGE COLUMN `name` `name` CHAR(200) NOT NULL COMMENT 'Наименование' COLLATE 'utf8_unicode_ci' AFTER `id`,
	CHANGE COLUMN `seoName` `seoName` CHAR(200) NULL DEFAULT NULL COMMENT 'seoName' COLLATE 'utf8_unicode_ci' AFTER `name`,
	CHANGE COLUMN `linkUrl` `linkUrl` CHAR(200) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `isActive`;
ALTER TABLE `companyContract`
	CHANGE COLUMN `name` `name` CHAR(200) NOT NULL COMMENT 'Наименование' COLLATE 'utf8_unicode_ci' AFTER `id`;
	
/usr/bin/php /var/www/emportal/dev1/protected/yiic updatefromemias
	
 * http://stackoverflow.com/questions/21042162/php-soapclient-ssl-failed-to-load-external-entity-certificate-not-loaded
 * https://www.daniweb.com/programming/web-development/threads/476724/cannot-connect-to-host-via-soap
 * https://github.com/Evrika/Vidal/blob/master/web/soap.php
 * 
 * view-source:https://local.emportal.ru/site/emias?method=getAllLpusInfo&params[returnBranches]=1&params[externalSystemId]=MPGU&&params[lpuTypeCode]=71
 */
class EmiasLoader {

	static $PRESET = [
			"demo" => [
					"urlServer" => 'https://37.230.149.6:10004/emias-soap-service/PGUServicesInfo2_24?wsdl',
					"systype" => 4,
					"externalSystemId" => 'MPGU',
					"local_cert" => 'EmiasTestSSLClient.pem',
					"passphrase" => 'testSSLClient',
					"cityId" => '534bd8b9-e0d4-11e1-89b3-e840f2aca94f',
			],
			"production" => [
					"urlServer" => 'https://mosmedzdrav.ru:4058/emias-soap-service/PGUServicesInfo2_24?wsdl',
					"systype" => 4,
					"externalSystemId" => 'MEDKOMPAS',
					"local_cert" => 'MEDKOMPAS.pem',
					"passphrase" => 'Bie1ba4i',
					"cityId" => '534bd8b9-e0d4-11e1-89b3-e840f2aca94f',
			],
	];
	static $DEFAULT_PRESET_TYPE = "production";
	
	public $params = [];
	public $systype = NULL;
	public $local_cert = NULL;
	public $passphrase = NULL;
	public $externalSystemId = NULL;
	public $urlServer = NULL;
	public $requestDelay = 0.2;
	public $cityId = NULL;
	public $client = NULL;

	function __construct($type = NULL) {
		if($type === NULL) $type = self::$DEFAULT_PRESET_TYPE;
		foreach (self::$PRESET[$type] as $key=>$value) {
			$this->{$key} = $value;
		}
	}
	
	public function test() {
		$lpusInfo = $this->getLpusInfo(['omsNumber'=>'R25090000002789']);
		$response = $this->populateLPUList($lpusInfo->return, false, false);
		print_r($response);
	}
	
	public function callServer($functionName, $params) {
		static $sslOptions = array(
		    'ssl' => array(
		        'allow_self_signed' => true,
		        'verify_peer' => false,
		    ),
		);
		try {
			if(!is_object($this->client)) $this->client = @new SoapClient($this->urlServer ,array(
			    'local_cert' => dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->local_cert,
			    'passphrase'    => $this->passphrase,
			    'stream_context' => stream_context_create($sslOptions),
			    'trace' => true,
			    'exceptions' => true,
			    'cache_wsdl' => WSDL_CACHE_NONE,
			    'wsdl_cache_enabled' => false
			));
			if(is_object($this->client)) {
			   $result = $this->client->__soapCall($functionName, [$params]);
			   return $result;
			}
		} catch (Exception $e) {
			$errorMessage = "";
			switch ($e->getMessage()) {
				case "PATIENT_NOT_FOUND":
					$errorMessage = "Пациент не найден";
					break;
				default:
					$errorMessage = $e->getMessage();
					break;
			}
			$exceptionResult = (object)[
				"message" => $errorMessage,
			];
			return $exceptionResult;
		}
	}
	
	public function populateLPUList($list, $log=false, $geocoder=false) { //наполнить список адресов extAddress
		if($geocoder) {
			Yii::import('application.controllers.AjaxController');
			$ajaxController = new AjaxController("ajax");
		}
		$result = [];
		if(is_object($list)) $list = [$list];
		if(is_array($list)) {
			ExtAddress::model()->updateAll(["isActive"=>0], ["condition"=>"sysTypeId=".$this->systype]);
			if($log) if($log) echo "Список адресов: ".count($list).PHP_EOL;
		} else {
			if($log) if($log) echo "ERROR: Нет адресов! ".PHP_EOL;
			return $result;
		}		
		$extSysType = ExtSysType::model()->findByAttributes(['id' => $this->systype]);
		if(!$extSysType) {
			$extSysType = new ExtSysType;
			$extSysType->id = $this->systype;
			$extSysType->name = $this->externalSystemId;
			if($extSysType->save()) {
				if($log) if($log) echo " - Добавил внешнюю систему ".$extSysType->name.PHP_EOL;
			} else {
				if($log) if($log) echo " - Ошибки при добавлении внешней системы: " . PHP_EOL . MyTools::errorsToString($extCompany->getErrors()) . PHP_EOL;
				return $result;
			}
		}
		foreach($list as $LPU) {
			if($log) if($log) echo $LPU->id ."::".$LPU->lpuName . PHP_EOL;
			$attributes = [
				'name' => $LPU->lpuAddress,
				'extId' => $LPU->id,
				'extDescription' => $LPU->lpuType->name,
				'extShortName' => $LPU->lpuAddress,
				'extLPUType' => intval($LPU->lpuType->code),
				'extDistrictId' => null,
				'sysTypeId' => $this->systype,
				'isActive' => 1,
			];

			$extCompany = ExtCompany::model()->findByAttributes(['sysTypeId' => $this->systype, 'extId' => $LPU->id]);
			$extCompanyAttributes = ['name' => $LPU->lpuName, 'sysTypeId' => $this->systype, 'extId' => $LPU->id];
			if ($extCompany) {
				$extCompany->attributes = $extCompanyAttributes;
				try {
					if($extCompany->update()) {
						if($log) echo " - Обновил компанию!" . PHP_EOL;
						$attributes['extCompanyId'] = $extCompany->extId;
					} else {
						if($log) echo " - Ошибки при обновлении компании: " . PHP_EOL . MyTools::errorsToString($extCompany->getErrors()) . PHP_EOL;
					}
				} catch (Exception $e) {
					if($log) echo " - Ошибка обновления компании!" . PHP_EOL;
				}
			} else {
				$extCompany = new ExtCompany;
				$extCompany->attributes = $extCompanyAttributes;
				try {
					if($extCompany->save()) {
						if($log) echo " - Добавил компанию!" . PHP_EOL;
					} else {
						if($log) echo " - Ошибки при добавлении компании: " . PHP_EOL . MyTools::errorsToString($extCompany->getErrors()) . PHP_EOL;
						continue;
					}
				} catch (Exception $e) {
					if($log) echo " - Ошибка добавления компании!" . PHP_EOL;
					continue;
				}
			}
			
			$attributes['extCompanyId'] = $extCompany->extId;
			$extAddress = ExtAddress::model()->findByAttributes(['extId' => $LPU->id, 'sysTypeId' => $this->systype]);
			if (is_object($extAddress)) {
				$extAddress->attributes = $attributes;
				try {
					if($extAddress->update()) {
						if($log) echo " - Обновил адрес!" . PHP_EOL;
					} else {
						if($log) echo " - Ошибки при обновлении адреса: " . PHP_EOL . MyTools::errorsToString($extAddress->getErrors()) . PHP_EOL;
					}
				} catch (Exception $e) {
					if($log) echo " - Ошибка обновления адреса!" . PHP_EOL;
				}
			} else {
				$extAddress = new ExtAddress;
				$extAddress->attributes = $attributes;
				try {
					if($extAddress->save()) {
						if($log) echo " - Добавил адрес!" . PHP_EOL;
					} else {
						if($log) echo " - Ошибки при добавлении адреса: " . PHP_EOL . MyTools::errorsToString($extAddress->getErrors()) . PHP_EOL;
						continue;
					}
				} catch (Exception $e) {
					if($log) echo " - Ошибка добавления адреса!" . PHP_EOL;
				}
			}

			$extCompany = ExtCompany::model()->findByAttributes(['sysTypeId' => $this->systype, 'extId' => $LPU->id]);
			if(trim(strval($extCompany->companyId))==="") {
				$data = [ "name" => $extCompany->name, "nameRUS" => $extCompany->name, "categoryId" => "a3969945e0f59fa84e0e7740c2e8cc87", "companyTypeId" => "468a3543-0d50-11e2-a1cd-e840f2aca94f", "cityId" => $this->cityId, "IsOMS" => 1, "ratingSystem" => 3.5, "ratingUsers" => 3.5, "ratingSite" => 3.5, "rating" => 3.5];
				$responseArray[0] = CompanyManager::AddCompany($data);
				if($log) echo " > ".$responseArray[0]["status"] . ": " . $responseArray[0]["text"] . PHP_EOL;
				if($responseArray[0]["status"] == "OK") {
					$extCompany->companyId = $responseArray[0]["model"]["id"];
					if(!$extCompany->update()) {
						if($log) echo " > ERROR: при сохранении идентификатора компании! ".PHP_EOL;
						if($log) MyTools::showErrors($extCompany->getErrors());
						continue;
					}
					$modelContract = new CompanyContract();
					$modelContract->contractNumber = 'КО-1/' . (Yii::app()->db->createCommand('SELECT MAX(indexNum) FROM `' . CompanyContract::model()->tableName() . '`')->queryScalar() + 1);
					$modelContract->name = $extCompany->name;
					//$modelContract->companyTypeId = "d9c0f168-9bd2-11e2-856f-e840f2aca94f";
					//$modelContract->typeId = "852b5020b64a5134413f6d12be1610e1";
					$modelContract->employeeId = "b44192f5-a19d-415d-8992-40edffb48cee";
					$modelContract->contractTypeId = "d9c0f168-9bd2-11e2-856f-e840f2aca94f";
					$modelContract->companyId = $extCompany->companyId;
					$modelContract->periodOfValidity = "2999-01-11 00:00:00";
					$modelContract->indexNum = explode('/', $modelContract->contractNumber)[1];
					if(!$modelContract->save()) {
						if($log) echo " > ERROR:  при создании контракта компании! ".PHP_EOL;
						if($log) MyTools::showErrors($extCompany->getErrors());
						continue;
					}
				}
			}

			$extAddress = ExtAddress::model()->findByAttributes(['extId' => $LPU->id, 'sysTypeId' => $this->systype]);
			if(trim(strval($extAddress->addressId))==="") {
				if(!$extCompany) {
					if($log) echo " > ERROR: компании нет".PHP_EOL;
					continue;
				}
				$companyId = $extCompany->companyId;
				if(empty($companyId)) {
					if($log) echo " > ERROR: идентификатор компании не задан ".PHP_EOL;
					continue;
				}
				$company = Company::model()->find(["condition"=>"id=".Yii::app()->db->quoteValue($companyId)]);
					
				$data = [
						"name" => $extAddress->extShortName,
						"street" => $extAddress->name,
						"isActive" => $extAddress->isActive,
						"countryId" => "534bd8ae-e0d4-11e1-89b3-e840f2aca94f",
						"regionId" => "710c010c-32f9-11e2-b014-e840f2aca94f",
						"cityId" => $this->cityId,
						"ratingSystem" => 3.5,
						"ratingUsers" => 3.5,
						"ratingSite" => 3.5,
						"rating" => 3.5,
						"samozapis" => 1,
				];
				if(!empty($extAddress->extDistrictId)) {
					$extDistrict = ExtDistrict::model()->find(["condition"=>"sysTypeId=".Yii::app()->db->quoteValue($loader->systype)." AND extId=".Yii::app()->db->quoteValue($extAddress->extDistrictId)]);
					if($extDistrict) $data["cityDistrictId"] = $extDistrict->cityDistrictId;
				}
				$responseArray[0] = CompanyManager::AddAddress($companyId, $data);
				if($log) echo " > ".$responseArray[0]["status"] . ": " . $responseArray[0]["text"] . PHP_EOL;
				if($responseArray[0]["status"] == "OK") {
					$extAddress->addressId = $responseArray[0]["model"]["id"];
					if(!$extAddress->update()) {
						if($log) echo " > ERROR: при сохранении идентификатора адреса!! ".PHP_EOL;
						if($log) MyTools::showErrors($extAddress->getErrors());
						continue;
					}
					$extAddressModel = $responseArray[0]["model"];
					$extAddressModel->name = $extAddress->name;
					if(!$extAddressModel->update()) {
						if($log) echo " > ERROR: при сохранении идентификатора адреса! ".PHP_EOL;
						if($log) MyTools::showErrors($extAddressModel->getErrors());
						continue;
					}
					$responseArray[1] = CompanyManager::CreateAccount($responseArray[0]["model"], ["email" => "roman.z@emportal.ru", "password" => "6485", "phoneForAlert" => "", "agreement" => 1, "agreementNew" => 1, "createDate" => date('Y-m-d H:i:s'), "commonRegistry" => 1 ]);
					if($log) echo " > ".$responseArray[1]["status"] . ": " .$responseArray[1]["text"].PHP_EOL;
					if(!$company) {
						if($log) echo " > ERROR: при сохранении основного адреса компании!(Компания не найдена) ".PHP_EOL;
						continue;
					}
					$company->attributes = ["addressId"=>$extAddress->addressId];
					if(!$company->update()) {
						if($log) echo " > ERROR: при сохранении основного адреса компании! ".PHP_EOL;
						if($log) MyTools::showErrors($extAddress->getErrors());
						continue;
					}
					if($geocoder && ((trim(strval($extAddressModel->longitude))==="") OR (trim(strval($extAddressModel->latitude))===""))) {
						$result = $ajaxController->actionDefineGeocodesOfAddress($extAddressModel->linkUrl, 'php');
						if($log) echo " > " . $result['status'] . " > " . $result['text'] . PHP_EOL;
					}
				} else {
					continue;
				}
			} else {
				$data = [
						//"name" => $extAddress->extShortName,
						//"street" => $extAddress->name,
						"isActive" => $extAddress->isActive,
				];
				$responseArray[0] = CompanyManager::EditAddress($extAddress->addressId, $data);
				$extAddressModel = $responseArray[0]["model"];
				if($log) echo " > ".$responseArray[0]["status"] . ": " . $responseArray[0]["text"] . PHP_EOL;
				if($responseArray[0]["status"] !== "OK") {
					if($log) echo " > ERROR: при обновлении адреса компании! ".PHP_EOL;
				} else {
					if($log) echo " > Успешное обновление адреса компании!".PHP_EOL;
				}
				if($geocoder && ((trim(strval($extAddressModel->longitude))==="") OR (trim(strval($extAddressModel->latitude))===""))) {
					$result = $ajaxController->actionDefineGeocodesOfAddress($extAddressModel->linkUrl, 'php');
					if($log) echo " > " . $result['status'] . " > " . $result['text'] . PHP_EOL;
				}
			}
			
			$extAddress = ExtAddress::model()->findByAttributes(['extId' => $LPU->id, 'sysTypeId' => $this->systype]);
			
			if($geocoder && (trim(strval($extAddress->addressId))!=="")) {
				$extAddressModel = Address::model()->findByPk($extAddress->addressId);
				if(is_object($extAddressModel) AND (trim(strval($extAddressModel->cityDistrictId))==="") AND (trim(strval($extAddressModel->longitude))!=="") AND (trim(strval($extAddressModel->latitude))!=="")) {
					$district = MyTools::getDistrictFromYandex($extAddressModel->longitude, $extAddressModel->latitude, $extAddressModel->cityId);
					if($district["status"] === "OK") {
						$extAddressModel->cityDistrictId = $district["result"]["districtId"];
						if($extAddressModel->update()) {
							if($log) echo " > Успешное обновление района адреса! ".$district["result"]["name"].PHP_EOL;
						} else {
							if($log) echo " > ERROR: при обновлении района адреса!".PHP_EOL;
						}
					} else {
						if($log) echo " > ERROR: при обновлении района адреса:: ".$district["text"].PHP_EOL;
					}
				}
			}

			if(is_object($extAddress) AND (trim(strval($extAddress->addressId))!=="") AND isset($LPU->lpuWorkSchedule) AND isset($LPU->lpuWorkSchedule->weekDaySchedule)) {
				$workingHour = WorkingHour::model()->findByAttributes(['addressId'=>$extAddress->addressId]);
				if(is_object($workingHour)) {
					$workingHour->MondayStart = null;
					$workingHour->MondayFinish = null;
					$workingHour->TuesdayStart = null;
					$workingHour->TuesdayFinish = null;
					$workingHour->WednesdayStart = null;
					$workingHour->WednesdayFinish = null;
					$workingHour->ThursdayStart = null;
					$workingHour->ThursdayFinish = null;
					$workingHour->FridayStart = null;
					$workingHour->FridayFinish = null;
					$workingHour->SaturdayStart = null;
					$workingHour->SaturdayFinish = null;
					$workingHour->SundayStart = null;
					$workingHour->SundayFinish = null;
					foreach ($LPU->lpuWorkSchedule->weekDaySchedule as $schedule) {
						switch ($schedule->weekDay) {
							case 'Пн':
								$workingHour->MondayStart = $schedule->workItem->from;
								$workingHour->MondayFinish = $schedule->workItem->to;
								break;
							case 'Вт':
								$workingHour->TuesdayStart = $schedule->workItem->from;
								$workingHour->TuesdayFinish = $schedule->workItem->to;
								break;
							case 'Ср':
								$workingHour->WednesdayStart = $schedule->workItem->from;
								$workingHour->WednesdayFinish = $schedule->workItem->to;
								break;
							case 'Чт':
								$workingHour->ThursdayStart = $schedule->workItem->from;
								$workingHour->ThursdayFinish = $schedule->workItem->to;
								break;
							case 'Пт':
								$workingHour->FridayStart = $schedule->workItem->from;
								$workingHour->FridayFinish = $schedule->workItem->to;
								break;
							case 'Сб':
								$workingHour->SaturdayStart = $schedule->workItem->from;
								$workingHour->SaturdayFinish = $schedule->workItem->to;
								break;
							case 'Вс':
								$workingHour->SundayStart = $schedule->workItem->from;
								$workingHour->SundayFinish = $schedule->workItem->to;
								break;
						}
					}
					if(!$workingHour->update()) {
						if($log) echo " > ERROR: при обновлении расписания адреса компании! ".PHP_EOL;
						if($log) MyTools::showErrors($workingHour->getErrors());
					} else {
						if($log) echo " > Успешное обновление расписания адреса компании!" . PHP_EOL;
					}
				}
			}
			$result[$extCompany->companyId] = ['companyId'=>$extCompany->companyId, 'addresses'=>[$extAddress->addressId]];
		}
		return $result;
	}
	
	public function populateDoctorList($list, $attr, $log=false) { //наполняем список врачей по каждому адресу
		$result = [];
		if(is_object($list)) $list = [$list];
		if(!is_array($list)) {
			if($log) echo "Список пуст! " . PHP_EOL;
		} else
		foreach ($list as $data) {
			$extFio = $data->lastName . " " . $data->firstName . " " . $data->secondName;
			if($log) echo "-- ". $extFio ."::" . PHP_EOL;
			$attributes = [
				'extId' => $data->id,
				'extAddressId' => $data->lpuRoomsInfo->lpuRoomView->lpuId,
				'extSpecialityId' => @$attr['specialityId'],
				'extFio' => $extFio,
				'sysTypeId' => $this->systype,
				'deleted' => 0,
				'lastUpdate' => date("Y-m-d H:i:s",time()),
				'parameters' => json_encode(['receptionView' => $data->receptionTypes->receptionView]),
			];
			$extDoctor = ExtDoctor::model()->findByAttributes(['extId' => $attributes['extId'], 'extAddressId' => $attributes['extAddressId'], 'sysTypeId' => $this->systype]);
			
			if ($extDoctor) {
				if($extDoctor->extFio !== $attributes['extFio'] || $extDoctor->parameters !== $attributes['parameters']) {
					$extDoctor->attributes = $attributes;
					if($extDoctor->update()) {
						if($log) echo "Обновил врача!" .PHP_EOL;
					} else {
						if($log) echo "Ошибки при обновлении врача: ". PHP_EOL . MyTools::errorsToString($existingSpeciality->getErrors()) . PHP_EOL;
					}
					if($extDoctor->extFio !== $attributes['extFio'] && trim(strval($extDoctor->doctorId)) !== "") {
						$data = [
								"name" => $extDoctor->extFio,
						];
						if(is_numeric($extDoctor->deleted)) $data["deleted"] = $extDoctor->deleted;
						$data = array_merge($data, MyTools::FIOExplode($extDoctor->extFio));
						$response[0] = CompanyManager::EditDoctor($extDoctor->doctorId, $data);
						if($log) echo " > ".$response[0]["status"] . ": " . $response[0]["text"] . PHP_EOL;
					}
				}
			} else {
				$extDoctor = new ExtDoctor;
				$extDoctor->attributes = $attributes;
				if($extDoctor->save()) {
					if($log) echo "Добавил врача!".PHP_EOL;
				}
				else {
					if($log) {
						if($log) echo "Ошибки при добавлении врача: " . PHP_EOL . MyTools::errorsToString($extDoctor->getErrors()) . PHP_EOL;
					}
					continue;
				}
			}
			if(trim(strval($extDoctor->doctorId)) === "" || !Doctor::model()->exists("id=".Yii::app()->db->quoteValue($extDoctor->doctorId))) {
				$extAddress = ExtAddress::model()->find(["condition"=>"extId=".Yii::app()->db->quoteValue($extDoctor->extAddressId)." AND sysTypeId=".Yii::app()->db->quoteValue($extDoctor->sysTypeId)]);
				if(!$extAddress) {
					if($log) echo "Адрес ExtAddress не найден ".PHP_EOL;
					continue;
				}
				$address = Address::model()->find(["condition"=>"id=".Yii::app()->db->quoteValue($extAddress->addressId)]);
				if(!$address) {
					if($log) echo "Адрес Address не найден ".PHP_EOL;
					continue;
				}
				$extCompany = ExtCompany::model()->find(["condition"=>"extId=".Yii::app()->db->quoteValue($extAddress->extId)." AND sysTypeId=".Yii::app()->db->quoteValue($extAddress->sysTypeId)]);
				if(!$extCompany) {
					if($log) echo " ОШИБКА";
					continue;
				}
				$companyId = $extCompany->companyId;
				if(empty($companyId)) {
					if($log) echo "идентификатор компании не задан ".PHP_EOL;
					continue;
				}
			
				$data = [ "name" => $extDoctor->extFio, "sexId" => "ae11c45d855a991340c5f59edcb404da", "birthday" => "0000-00-00 00:00:00", "staffUnitId" => "b694644c-0334-11e2-930f-e840f2aca94f", "currentPlaceOfWorkId" => $companyId, "currentPosition" => "1bd00355-82c7-4c96-8a9a-c53028166ca3", "ratingSystem" => 3.5, "ratingUsers" => 3.5, "ratingSite" => 3.5, "rating" => 3.5 ];
				
				if(is_array($FIO = MyTools::FIOExplode($extDoctor->extFio))) $data = array_merge($data,$FIO);
				
				$data["address"] = $address;
				$response[0] = CompanyManager::AddDoctor($data);
				if($log) echo " > ".$response[0]["status"] . ": " . $response[0]["text"];
				if($response[0]["status"] == "OK") {
					if($log) echo " id: ".$response[0]["model"]["id"];
					$extDoctor->doctorId = $response[0]["model"]["id"];
					if(!$extDoctor->update()) {
						if($log) echo "Ошибка при сохранении идентификатора врача! " . PHP_EOL . MyTools::errorsToString($extDoctor->getErrors()) . PHP_EOL;
						continue;
					}
				}
			}
			if(trim(strval($extDoctor->doctorId)) !== "") {
				if(trim(strval($extDoctor->extSpecialityId)) === "" || $extDoctor->extSpecialityId !== @$attr['specialityId'] || !SpecialtyOfDoctor::model()->exists("doctorId = ".Yii::app()->db->quoteValue($extDoctor->doctorId))) {
					if(trim(strval($extDoctor->extSpecialityId)) !== "" AND $extDoctor->extSpecialityId !== @$attr['specialityId']) {
						SpecialtyOfDoctor::model()->deleteAll("doctorId = ".Yii::app()->db->quoteValue($extDoctor->doctorId));
					}
					$extCriteria = new CDbCriteria;
					$extCriteria->addCondition("specialtyId IS NOT NULL AND specialtyId != '' AND sysTypeId = ".$extDoctor->sysTypeId." AND extId=".Yii::app()->db->quoteValue(@$attr['specialityId'])." AND extAddressId=0");
					$extCriteria->group="extName";
					$allSpeciality = ExtSpeciality::model()->findAll($extCriteria);
					$specialities = [];
					foreach ($allSpeciality as $speciality) {
						$specialities[] = ['doctorId' => $extDoctor->doctorId,'doctorSpecialtyId' => $speciality->specialtyId,'doctorCategoryId' => "7395e4d6-89d8-4d54-893f-56532b8f7502"];
					}
					if(count($specialities)>0) {
						$doc = Doctor::model()->find(["condition"=>"id=".Yii::app()->db->quoteValue($extDoctor->doctorId)]);
						if($doc) {
							try {
								$doc->setSpecialtyOfDoctors($specialities);
								if($log) echo " > добавил ".count($specialities)." специальностей".PHP_EOL;
								$extDoctor->extSpecialityId = @$attr['specialityId'];
								$extDoctor->update();
							} catch (Exception $e) {
								if($log) echo PHP_EOL."Ошибка: ".$e->getMessage().PHP_EOL;
							}
						}
					}
				}
				$result[$extDoctor->doctorId] = ['doctorId'=>$extDoctor->doctorId, $extDoctor->extFio];
			}
		}
		ShortSpecialtyOfDoctor::UpdateSelf(['samozapis'=>1, 'cityId'=>$this->cityId], false);
		return $result;
	}
	
	public function populateSpecialityList($list, $log=false) { //наполнить список доступных специальностей extSpecialty
		$result = [];
		if(is_object($list)) $list = [$list];
		if(!is_array($list)) {
			if($log) echo "Список пуст! " . PHP_EOL;
		} else
		foreach($list as $data) {
			if($log) echo "-- ".$data->name."::".PHP_EOL;
			$keyAttributes = [
				'extId' => $data->code,
				'extAddressId' => 0,
				'sysTypeId' => $this->systype,
			];
			$extSpeciality = ExtSpeciality::model()->findByAttributes($keyAttributes);
			if (!is_object($extSpeciality)) {
				$attributes = array_merge($keyAttributes, ['extName' => trim(strval($data->name))]);
				$extSpeciality = new ExtSpeciality;
				$extSpeciality->attributes = array_merge($keyAttributes, ['extName' => trim(strval($data->name))]);
				if($extSpeciality->save()) {
					if($log) echo "Добавил специальность!" . PHP_EOL;
				} else {
					if($log) echo "Ошибки при добавлении специальности: " . PHP_EOL . MyTools::errorsToString($extSpeciality->getErrors()) . PHP_EOL;
					continue;
				}
			}
			
			if (strval($extSpeciality->specialtyId) === "" || !DoctorSpecialty::model()->exists("id=".Yii::app()->db->quoteValue($extSpeciality->specialtyId))) {
				if($extSpeciality->extName !== "") {
					$extName = $extSpeciality->extName;
					foreach (DoctorSpecialty::$specialitiesAndDuplicates as $specialityName=>$specialityArray) {
						foreach ($specialityArray as $duplicateName) {
							if($extSpeciality->extName == $duplicateName)  {
								$extName = $specialityName;
								if($log) echo $duplicateName;
								break 2;
							}
						}
					}
					DoctorSpecialty::$Archived = TRUE;
					$criteria = new CDbCriteria;
					$criteria->addCondition('name = :name','AND');
					$criteria->params = array( ':name'=>$extName );
					$doctorSpecialty = DoctorSpecialty::model()->find($criteria);
					if(empty($doctorSpecialty)) {
						$criteria = new CDbCriteria;
						$criteria->addCondition('linkUrl = :linkUrl','AND');
						$criteria->params = array( ':linkUrl'=>MyTools::makeLinkUrl($extName) );
						$doctorSpecialty = DoctorSpecialty::model()->find($criteria);
					}
					if(empty($doctorSpecialty)) {
						$doctorSpecialty = new DoctorSpecialty;
						$doctorSpecialty->name = $extName;
						$doctorSpecialty->isArchive = 1;
						$doctorSpecialty->linkUrl = MyTools::makeLinkUrl($extName);
						if($doctorSpecialty->save()) {
							if($log) echo " _NEW_ ";
						} else {
							$doctorSpecialty = NULL;
						}
					}
					if($doctorSpecialty) {
						ExtSpeciality::model()->updateAll(["specialtyId"=>$doctorSpecialty->id], ["condition"=>"extName=".Yii::app()->db->quoteValue($extSpeciality->extName)." AND "."sysTypeId=".Yii::app()->db->quoteValue($this->systype)]);
						if($log) echo " == ". $doctorSpecialty->name;
					} else {
						ExtSpeciality::model()->updateAll(["specialtyId"=>NULL], ["condition"=>"extName=".Yii::app()->db->quoteValue($extSpeciality->extName)." AND "."sysTypeId=".Yii::app()->db->quoteValue($this->systype)]);
						if($log) echo " == NULL";
					}
					$extSpeciality = ExtSpeciality::model()->findByAttributes($keyAttributes);
				}
			}
			if(is_object($extSpeciality) AND strval($extSpeciality->specialtyId) !== "") {
				$doctorSpecialty = DoctorSpecialty::model()->findByPk($extSpeciality->specialtyId);
				if(!empty($doctorSpecialty)) {
					$result[$extSpeciality->specialtyId] = ['linkUrl'=>$doctorSpecialty->linkUrl, 'name'=>$doctorSpecialty->name];
				}
				if(!ShortSpecialtyOfDoctor::model()->exists(
						"id=".Yii::app()->db->quoteValue($extSpeciality->specialtyId)." AND ".
						"cityId=".Yii::app()->db->quoteValue($this->cityId)." AND ".
						"samozapis=1")) {
					try {
						$shortSpecialtyOfDoctor = new ShortSpecialtyOfDoctor;
						$shortSpecialtyOfDoctor->id = $doctorSpecialty->id;
						$shortSpecialtyOfDoctor->name = $doctorSpecialty->name;
						$shortSpecialtyOfDoctor->linkUrl = $doctorSpecialty->linkUrl;
						$shortSpecialtyOfDoctor->link = $doctorSpecialty->link;
						$shortSpecialtyOfDoctor->samozapis = 1;
						$shortSpecialtyOfDoctor->cityId = $this->cityId;
						$shortSpecialtyOfDoctor->Parameters = "";
						if($shortSpecialtyOfDoctor->save()) {
							if(trim(strval($doctorSpecialty->shortSpecialtyId)) === "") {
								$doctorSpecialty->shortSpecialtyId = $shortSpecialtyOfDoctor->id;
								$doctorSpecialty->update();
							} 
							if($log) echo " -- Добавил shortSpecialtyOfDoctor!" . PHP_EOL;
						} else {
							if($log) echo " -- Ошибки при добавлении shortSpecialtyOfDoctor: " . PHP_EOL . MyTools::errorsToString($shortSpecialtyOfDoctor->getErrors()) . PHP_EOL;
						}
					} catch (Exception $e) { }
				}
			}
		}
		return $result;
	}
	
	public function getAllLpusInfo($attr=null) {
		$params = [
				'returnBranche' => 1,
				'externalSystemId' => $this->externalSystemId,
				'lpuTypeCode' => null,
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('getAllLpusInfo', $params);
		return $response;
	}
	
	public function getLpusInfo($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getLpusInfo&params[attr][omsNumber]=R25090000002789
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
				'omsNumber' => null, //Номер полиса ОМС (required)
				'omsSeries' => null, //Серия полиса ОМС
				'birthDate' => null, //Дата рождения пациента
				'lpuTypeCode' => null, //Код типа ЛПУ
				'lpuName' => null, //Наименование ЛПУ
				'baseLpuAddress' => null, //Адрес ЛПУ
				'serviceLpuAddress' => [ //Адрес обслуживания ЛПУ
						'area' => null, //Область
						'district' => null, //Район
						'street' => null, //Улица
						'house' => null, //Дом
						'building' => null, //Строение
						'construction' => null, //Здание
				],
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('getAllLpusInfo', $params);
		return $response;
	}
	
	public function getDoctorsInfo($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getDoctorsInfo&params[attr][omsNumber]=R25090000002789&params[attr][birthDate]=1983-08-17T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getDoctorsInfo&params[attr][omsSeries]=779999&params[attr][omsNumber]=9992511111&params[attr][birthDate]=1987-02-06T00:00:00&params[attr][specialityId]=788
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
				'omsNumber' => null, //Номер полиса ОМС (required)
				'omsSeries' => null, //Серия полиса ОМС
				'birthDate' => null, //Дата рождения пациента (required)
				'specialityId' => null, //Код специальности врача (required)
				'referralId' => null, //ИД направления при записи по направлению
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('getDoctorsInfo', $params);
		return $response;
	}
	
	public function getSpecialitiesInfo($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getSpecialitiesInfo&params[attr][omsNumber]=R25090000002789&params[attr][birthDate]=1983-08-17T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getSpecialitiesInfo&params[attr][omsSeries]=779999&params[attr][omsNumber]=9992511111&params[attr][birthDate]=1987-02-06T00:00:00
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
				'omsNumber' => null, //Номер полиса ОМС (required)
				'omsSeries' => null, //Серия полиса ОМС
				'birthDate' => null, //Дата рождения пациента (required)
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('getSpecialitiesInfo', $params);
		return $response;
	}
	
	public function getPatientInfo($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getPatientInfo&params[attr][omsNumber]=R25090000002789&params[attr][birthDate]=1983-08-17T00:00:00
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
				'omsNumber' => null, //Номер полиса ОМС (required)
				'omsSeries' => null, //Серия полиса ОМС
				'birthDate' => null, //Дата рождения пациента (required)
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('getPatientInfo', $params);
		return $response;
	}
	
	public function getAvailableResourceScheduleInfo($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getAvailableResourceScheduleInfo&params[attr][omsSeries]=779999&params[attr][omsNumber]=9992511111&params[attr][birthDate]=1987-02-06T00:00:00&params[attr][lpuId]=135&params[attr][availableResourceId]=10471719
		$params = [
				'lpuId' => null,//(required)
				'availableResourceId' => '',//(required)
				'externalSystemId' => $this->externalSystemId, //(required)
				'omsNumber' => null, //Номер полиса ОМС (required)
				'omsSeries' => null, //Серия полиса ОМС
				'birthDate' => null, //Дата рождения пациента (required)
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('getAvailableResourceScheduleInfo', $params);
		return $response;
	}
	
	public function getAvaibleAppointments($idDoc, $idLpu, $idPat, $visitStart, $visitEnd, $referralId=NULL) {
		$params = [
				'lpuId' => $idLpu,
				'availableResourceId' => $idDoc,
				'referralId' => $referralId,
		];
		$idPat = json_decode($idPat, true);
		if(is_array($idPat)) {
			$params = array_merge($params, $idPat);
		}
		$scheduleInfo = $this->getAvailableResourceScheduleInfo($params);
		$Success = false;
		$Appointment = [];
		$ErrorList = [];
		if(is_object($scheduleInfo->return)) {
			$schedules = NULL;
			if(is_object($scheduleInfo->return->schedules)) {
				$schedules = [ $scheduleInfo->return->schedules ];
			} else {
				$schedules = $scheduleInfo->return->schedules;
			}
			if(is_array($schedules)) {
				$Success = true;
				$visitStartDateTime = strtotime( date("Y-m-d", strtotime($visitStart)) );
				$visitEndDateTime = strtotime( date("Y-m-d", strtotime($visitEnd)) );
				foreach ($schedules as $schedule) {
					$scheduleDateTime = strtotime( date("Y-m-d", strtotime($schedule->date)) );
					if($visitStartDateTime <= $scheduleDateTime AND $scheduleDateTime <= $visitEndDateTime) {
						$timePeriods = $schedule->resourceSchedule->timePeriods;
						if(is_object($timePeriods)) {
							$timePeriods = [$timePeriods];
						}
						if(is_array($timePeriods)) {
							foreach ($timePeriods as $timePeriod) {
								$Appointment[] = (object)[
										'IdAppointment' => json_encode([
												'availableResourceId' => $scheduleInfo->return->availableResourceId,
												'startTime' => $timePeriod->startTime,
												'endTime' => $timePeriod->endTime,
												'receptionDate' => $schedule->date,
										]),
										'VisitStart' => $timePeriod->startTime,
										'VisitEnd' => $timePeriod->endTime,
										'status' => ($timePeriod->allowedAppointment) ? "allow" : "busy"
								];
							}
						}
					}
				}
			}
		}
		if(!$Success) {
			if(isset($scheduleInfo->message)) {
				$Error = (object)[
					'IdError' => '',
					'ErrorDescription' => strval($scheduleInfo->message),
				];
				$ErrorList[] = $Error;
			} elseif(isset($scheduleInfo->detail->exception)) {
				$Error = (object)[
					'IdError' => '',
					'ErrorDescription' => strval($scheduleInfo->detail->exception),
				];
				$ErrorList[] = $Error;
			} elseif(empty($ErrorList)) {
				$Error = (object)[
					'IdError' => '',
					'ErrorDescription' => 'Неизвестная ошибка',
				];
				$ErrorList[] = $Error;
			}
		}
		$response = (object)[
				'GetAvaibleAppointmentsResult' => (object)[
						'Success' => $Success,
						'ListAppointments' => (object)[
								'Appointment' => $Appointment,
						],
						'ErrorList' => $ErrorList,
				],
		];
		return $response;
	}
	
	public function createAppointment($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getAvailableResourceScheduleInfo&params[attr][omsSeries]=779999&params[attr][omsNumber]=9992511111&params[attr][birthDate]=1987-02-06T00:00:00&params[attr][lpuId]=135&params[attr][availableResourceId]=10471719
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('createAppointment', $params);
		return $response;
	}
	
	public function setAppointment($idAppointment, $idLpu, $idPat, $doctorsReferal=NULL) {
		$attr = [
				'lpuId' => $idLpu,
				'referralId' => $doctorsReferal,
		];
		$idPat = json_decode($idPat, true);
		if(is_array($idPat)) {
			$attr = array_merge($attr, $idPat);
		}
		$idAppointment = json_decode($idAppointment, true);
		if(is_array($idAppointment)) {
			$attr = array_merge($attr, $idAppointment);
		}

		$extDoctor = ExtDoctor::model()->findByAttributes(['extId'=>$idAppointment['availableResourceId'],'extAddressId'=>$idLpu,'sysTypeId'=>$this->systype]);
		$parameters = $extDoctor->getParameters();
		if(is_array($parameters->receptionView)) {
			foreach ($parameters->receptionView as $receptionView) {
				if($doctorsReferal === NULL) {
					if($receptionView->primarySign AND !$receptionView->homeSign) {
						$attr['receptionTypeCodeOrLdpTypeCode'] = $receptionView->code;
						break;
					}
				} else {
					$attr['receptionTypeCodeOrLdpTypeCode'] = $receptionView->code;
					break;
				}
			}
		}
		$appointment = $this->createAppointment($attr);
		
		$Success = false;
		$appointmentId = NULL;
		$ErrorList = (object)[];
		if(isset($appointment->return) && isset($appointment->return->appointmentId) && isset($attr['receptionTypeCodeOrLdpTypeCode'])) {
			$Success = true;
			$appointmentId = $appointment->return->appointmentId;
		} else {
			$Error = (object)[
				'IdError' => '',
				'ErrorDescription' => '',
			];
			if(isset($appointment->message)) {
				$Error->ErrorDescription = strval($appointment->message);
			} elseif(isset($appointment->detail)) {
				if(isset($appointment->detail->exception)) {
					$Error->ErrorDescription = $appointment->detail->exception;
				} elseif(isset($appointment->detail->faultstring)) {
					$Error->ErrorDescription = $appointment->detail->faultstring;
				}
			}
			$ErrorList->{"Error"} = $Error;
		}
		$response = (object)[
				'SetAppointmentResult' => (object)[
						'Success' => $Success,
						'AppointmentId' => $appointmentId,
						'ErrorList' => $ErrorList,
				],
		];
		return $response;
	}
	
	public function getAppointmentReceptionsByPatient($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getAppointmentReceptionsByPatient&params[attr][omsSeries]=779999&params[attr][omsNumber]=9992511111&params[attr][birthDate]=1987-02-06T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getAppointmentReceptionsByPatient&params[attr][omsNumber]=7700000061265000&params[attr][birthDate]=2000-10-26T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getAppointmentReceptionsByPatient&params[attr][omsNumber]=7700004124070292&params[attr][birthDate]=1992-02-07T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getAppointmentReceptionsByPatient&params[attr][omsNumber]=R25090000002789&params[attr][birthDate]=1983-08-17T00:00:00
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		foreach ($params as $key=>$value) { if(is_string($value)) $params[$key] = trim($value); }
		$response = $this->callServer('getAppointmentReceptionsByPatient', $params);
		return $response;
	}
	
	public function cancelAppointment($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=cancelAppointment&params[attr][omsSeries]=779999&params[attr][omsNumber]=9992511111&params[attr][birthDate]=1987-02-06T00:00:00&params[attr][appointmentId]=232941591
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=cancelAppointment&params[attr][omsNumber]=7700000061265000&params[attr][birthDate]=2000-10-26T00:00:00&params[attr][appointmentId]=232970284
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=cancelAppointment&params[attr][omsNumber]=7700004124070292&params[attr][birthDate]=1992-02-07T00:00:00&params[attr][appointmentId]=232970284
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=cancelAppointment&params[attr][omsNumber]=R25090000002789&params[attr][birthDate]=1983-08-17T00:00:00&params[attr][appointmentId]=232970284
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		$params = self::FormattingParams($params);
		$response = $this->callServer('cancelAppointment', $params);
		return $response;
	}
	
	public function getReferralsInfo($attr=[]) {
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getReferralsInfo&params[attr][omsNumber]=7700004130551182&params[attr][birthDate]=1982-11-05T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getReferralsInfo&params[attr][omsSeries]=779999&params[attr][omsNumber]=9992511111&params[attr][birthDate]=1987-02-06T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getReferralsInfo&params[attr][omsNumber]=7700000061265000&params[attr][birthDate]=2000-10-26T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getReferralsInfo&params[attr][omsNumber]=7700004124070292&params[attr][birthDate]=1992-02-07T00:00:00
		//view-source:http://samozapis-moskva.emportal.ru/site/emias?method=getReferralsInfo&params[attr][omsNumber]=R25090000002789&params[attr][birthDate]=1983-08-17T00:00:00
		$params = [
				'externalSystemId' => $this->externalSystemId, //(required)
		];
		if(is_array($attr)) $params = array_merge($params, $attr);
		$params = self::FormattingParams($params);
		$response = $this->callServer('getReferralsInfo', $params);
		return $response;
	}
	
	public function checkPatient(
		$idLpu, $pat = [
		'Birthday' => null,
		'Polis_N' => null, 
		'Polis_S' => null]) {
		
		try {
			$params = [
				'omsNumber' => $pat['Polis_N'],
				'omsSeries' => $pat['Polis_S'],
				'birthDate' => $pat['Birthday'],
			];
			$info = $this->getSpecialitiesInfo($params);
			
			$response = new stdClass();
			$response->Success = isset($info->return);
			$response->IdPat = isset($info->return) ? $params : null;
			return $response;
		} catch (Exception $e) {
			return false;
		}
		
	}
	
	public function SendNotificationAboutAppointment() {
		
	}
	
	public static function checkDoctor($attr=[]) {
		return [];
	}
	
	public static function FormattingParams($params) {
		foreach ($params as $key=>$value) {
			if(is_string($value)) $params[$key] = trim($value);
			if($params[$key] == "") unset($params[$key]);
			if(isset($params['birthDate'])) $params['birthDate'] = date('Y-m-d\TH:i:s',strtotime($params['birthDate']));
		}
		return $params;
	}
}
