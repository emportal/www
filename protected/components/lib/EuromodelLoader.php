<?php

class EuromodelLoader {
	const DAY_TIME		=	86400;
	const MONTH_TIME	=	2592000;
	
	private static $DEFAULT_URL = [
		'evromed-klinik' => 'http://intranet.euromed.ru/api/',
		'evromed-kids' => 'http://intranet.deti-euromed.ru/api/',
	];
	private static $DEFAULT_USERNAME = 'alfa';
	private static $DEFAULT_PASSWORD = 'JKJKLkxjkdjfkdke354edfd2';
	
	private $url;
	private $username;
	private $password;

	/*
	 *	Расшифровывает данные из csv формата в массив.
	 *	$input - входные данные, которые надо расшифровать;
	 *	$row_delimiter - разделитель строк;
	 *	$column_delimiter - разделитель столбцов;
	 *	$in_charset - кодировка входных данных;
	 *	$out_charset - кодировка выходных данных;
	 */
	private static function DataEncode($input, $row_delimiter="\n", $column_delimiter=";", $in_charset="WINDOWS-1251", $out_charset="UTF-8") {
		$data = [];
		$outCharsetInput = iconv($in_charset, $out_charset, $input);
		$rows = explode($row_delimiter, $outCharsetInput);
		foreach ($rows as $row) {
			if(!empty($row))
				$data[] = str_getcsv($row, $column_delimiter);
		}
		return $data;
	}

	/*
	 *	Объединяет ключи с соответствующими им значениями в новый массив.
	 *	$data - массив значений;
	 *	$fio - массив ключей;
	 */
	private static function CombineDataWithKeys($data,$keys) {
		$result = [];
		foreach ($data as $key=>$row) {
			if(!is_array($row)) { $row = [$row]; }
			if(count($keys) >= count($row)) {
				$row = array_pad($row, count($keys), null);
			} else {
				$different = count($row)-count($keys);
				for ($i=0; $i < $different; $i++) {
					$keys[] = count($keys) + 1;
				}
			}
			$result[$key] = array_combine($keys, $row);
		}
		return $result;
	}

	/*
	 *	Находит идентификатор сотрудника по его ФИО.
	 *	$staffList - массив сотрудников;
	 *	$fio - Фамилия Имя Отчество сотрудника;
	 */
	private static function FindEmployeeIdByFIO($staffList,$fio) {
		foreach ($staffList as $employee) {
			if(mb_strtolower(trim($employee["fio"])) === mb_strtolower(trim($fio))) {
				return $employee["id"];
			}
		}
	}

	function __construct($urlKey=null, $setUsername=null, $setPassword=null) {
		$this->url = isset($urlKey) ? self::$DEFAULT_URL[$urlKey] : self::$DEFAULT_URL['evromed-klinik'];
		$this->username = isset($setUsername) ? $setUsername : self::$DEFAULT_USERNAME;
		$this->password = isset($setPassword) ? $setPassword : self::$DEFAULT_PASSWORD;
	}

	/*
	 *	Вызывает функцию API сервера.
	 *	$functionName - название функции;
	 *	$getParams - GET параметры;
	 *	$postParams - POST параметры;
	 */
	public function callServer($functionName, $getParams=null, $postParams=null) {
		$result = ["data"=>null, "success"=>false, "messages"=>[]];
		try {
			$getParamsData = '';
			if(is_array($getParams)) {
				foreach($getParams as $key=>$value)
					$getParamsData .= $key.'='.$value.'&';
				$getParamsData = "?" . trim($getParamsData, '&');
			}
			$url = $this->url . $functionName . $getParamsData;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERPWD, $this->username.":".$this->password);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			if(is_array($postParams)) {
				$postParamsData = '';
				foreach($postParams as $key=>$value)
					$postParamsData .= $key.'='.$value.'&';
				$postParamsData = trim($postParamsData, '&');
				curl_setopt($ch, CURLOPT_POST, count($postParams));
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postParamsData);
			}
			$output = curl_exec($ch);
			$result["data"] = self::DataEncode($output);
			$result["success"] = true;
			curl_close($ch);
		} catch (Exception $e)
		{
			$result["messages"][] = "Ошибка! " . $e->getMessage();
		}
		return $result;
	}

	/*
	 *	Получает список сотрудников.
	 */
	public function getStaffList() {
		$keys = [
				"id", // id сотрудника;
				"fio", // фио сотрудника;
				"speciltyId", // id специализации;
				"speciltyName", // наименование специализации;
		];
		$result = $this->callServer("stafflist.php");
		if($result["success"]) {
			$result["data"] = self::CombineDataWithKeys($result["data"],$keys);
			$result["messages"][] = "Cписок сотрудников получен.";
		} else {
			$result["messages"][] = "Ну удалось получить список сотрудников.";
		}
		return $result;
	}

	/*
	 *	Cписок доступных сотрудников по дате дежурств.
	 *	$date_start - дата начала периода выборки (по умолчанию сегодня);
	 *	$date_end - дата конца периода выборки (по умолчанию +30 дней к дате начала периода);
	 */
	public function getStaff($date_start=null,$date_end=null) {
		$date_start = date("Y-m-d", ($date_start === null) ? time() : strtotime($date_start));
		$date_end = date("Y-m-d", ($date_end === null) ? (strtotime($date_start) + self::MONTH_TIME) : strtotime($date_end));
		$keys = [
				"date", // дата;
				"id", // id сотрудника;
				"fio", // фио сотрудника;
				"speciltyId", // id специализации;
				"speciltyName", // наименование специализации;
				"comment", // комментарий по времени;
				"status", // статус;
		];
		$result = $this->callServer("staff.php", ["d_start"=>$date_start, "d_end"=>$date_end]);
		if($result["success"]) {
			$result["messages"][] = "Список сотрудников по дате дежурств получен.";
			$data = self::CombineDataWithKeys($result["data"],$keys);
			foreach ($data as $key=>$visit) {
				$visitTime = self::CombineDataWithKeys([explode("-",$visit["comment"])],["visitstart","visitend"]);
				$data[$key]["comment"] = [
						"visitstart" => str_replace(".", ":", trim($visitTime[0]["visitstart"])),
						"visitend" => str_replace(".", ":", trim($visitTime[0]["visitend"])),
				];
				$data[$key]["status"] = trim($data[$key]["status"]);
			}
			$formattedData = [];
			foreach ($data as $employeeData) {
				$formattedData[$employeeData["date"]][$employeeData["id"]] = $employeeData;
			}
			$result["data"] = $formattedData;
		} else {
			$result["messages"][] = "Ну удалось получить список сотрудников по дате дежурств.";
		}
		return $result;
	}

	/*
	 *	Получает расписание сотрудника с учётом статуса дежурств.
	 *	$employeeArr - массив идентификаторов сотрудников;
	 *	$date_start - дата начала периода выборки (по умолчанию сегодня);
	 *	$date_end - дата конца периода выборки (по умолчанию +30 дней к дате начала периода);
	 */
	public function getTaskWithDutyStatus($employeeArr,$date_start=null,$date_end=null) {
		$result = ["data"=>null, "success"=>false, "messages"=>[]];
		$task = $this->getTask($employeeArr,$date_start,$date_end);
		$result["messages"] = array_merge($result["messages"],$task["messages"]);
		if($task["success"]) {
			$staff = $this->getStaff($date_start,$date_end);
			$result["messages"] = array_merge($result["messages"],$staff["messages"]);
			if($staff["success"]) {
				$taskData = [];
				foreach ($task["data"] as $taskKey=>$taskValue) {
					$stafValue = null;
					if(isset($staff["data"][$taskValue["date"]]) AND isset($staff["data"][$taskValue["date"]][$taskValue["employeeId"]])) {
						$stafValue = $staff["data"][$taskValue["date"]][$taskValue["employeeId"]];
						if(is_array($stafValue) AND isset($stafValue["status"])) {
							$taskValueVisitstart = explode(":",$taskValue["time"]["visitstart"]);
							if(isset($taskValueVisitstart[0]) AND isset($taskValueVisitstart[0]) AND is_numeric($taskValueVisitstart[0]) AND is_numeric($taskValueVisitstart[0])) {

								$taskValueVisitstartMinute = $taskValueVisitstart[0]*60 + $taskValueVisitstart[1];
								switch ($stafValue["status"]) {
									case "phone":
									case "on duty":
										$taskData[] = $taskValue;
										break;
									case "clinic":
										if(empty($stafValue["comment"]["visitstart"]) OR empty($stafValue["comment"]["visitend"])) {
											$taskData[] = $taskValue;
										} else {
											$stafValueVisitstart = explode(":",$stafValue["comment"]["visitstart"]);
											$stafValueVisitend = explode(":",$stafValue["comment"]["visitend"]);
											if(isset($stafValueVisitstart[0]) AND isset($stafValueVisitstart[0]) AND is_numeric($stafValueVisitstart[0]) AND is_numeric($stafValueVisitstart[0]) AND isset($stafValueVisitend[0]) AND isset($stafValueVisitend[0]) AND is_numeric($stafValueVisitend[0]) AND is_numeric($stafValueVisitend[0])) {
													$stafValueVisitstartMinute = $stafValueVisitstart[0]*60 + $stafValueVisitstart[1];
													$stafValueVisitendMinute = $stafValueVisitend[0]*60 + $stafValueVisitend[1];
													$taskValueVisitstart = explode(":",$taskValue["time"]["visitstart"]);
													if(isset($taskValueVisitstart[0]) AND isset($taskValueVisitstart[0]) AND is_numeric($taskValueVisitstart[0]) AND is_numeric($taskValueVisitstart[0])) {
														$taskValueVisitstartMinute = $taskValueVisitstart[0]*60 + $taskValueVisitstart[1];
														if($stafValueVisitstartMinute <= $taskValueVisitstartMinute AND $taskValueVisitstartMinute < $stafValueVisitendMinute) {
															if($taskData[count($taskData)-1]["time"]["visitstart"] !== $taskValue["time"]["visitstart"]) {
																$taskData[] = $taskValue;
															}
														}
													}
											} else {
												$taskData[] = $taskValue;
											}
										}
										break;
									default:
										$taskData[] = $taskValue;
										break;
								}
							}
						}
					}
				}
				$result["success"] = true;
				$result["data"] = $taskData;
			} else {
				$result["success"] = false;
			}
		} else {
			$result["success"] = false;
		}
		return $result;
	}
	
	/*
	 *	Получает расписание сотрудника.
	 *	$employeeArr - массив идентификаторов сотрудников;
	 *	$date_start - дата начала периода выборки (по умолчанию сегодня);
	 *	$date_end - дата конца периода выборки (по умолчанию +30 дней к дате начала периода);
	 */
	public function getTask($employeeArr,$date_start=null,$date_end=null) {
		$date_start = date("Y-m-d", ($date_start === null) ? time() : strtotime($date_start));
		$date_end = date("Y-m-d", ($date_end === null) ? (strtotime($date_start) + self::MONTH_TIME) : strtotime($date_end));
		$keys = [
				"date", // дата;
				"employeeId", // id сотрудника;
				"time", // время;
		];
		$getParams = ["d_start"=>$date_start, "d_end"=>$date_end, "st"=>implode(";",$employeeArr)];
		$result = $this->callServer("task.php",$getParams);
		if($result["success"]) {
			$result["data"] = self::CombineDataWithKeys($result["data"],$keys);
			foreach ($result["data"] as $key=>$visit) {
				$visitTime = self::CombineDataWithKeys([explode("-",$visit["time"])],["visitstart","visitend"]);
				$result["data"][$key]["time"] = [
						"visitstart" => str_replace(".", ":", trim($visitTime[0]["visitstart"])),
						"visitend" => str_replace(".", ":", trim($visitTime[0]["visitend"])),
				];
			}
			$result["messages"][] = "Расписание сотрудника получено.";
		} else {
			$result["messages"][] = "Ну удалось получить расписание сотрудника.";
		}
		return $result;
	}

	/*
	 *	Получает расписание сотрудника по его ФИО.
	 *	$fio - Фамилия Имя Отчество сотрудника;
	 *	$date_start - дата начала периода выборки;
	 *	$date_end - дата конца периода выборки;
	 */
	public function getTaskByFIO($fio,$date_start=null,$date_end=null,$withDutyStatus=true) {
		$result = ["data"=>null, "success"=>false, "messages"=>[]];
		$staffList = $this->getStaffList();
		$result["messages"] = array_merge($result["messages"],$staffList["messages"]);
		if($staffList["success"]) {
			$employeeId = self::FindEmployeeIdByFIO($staffList["data"], $fio);
			if($employeeId !== null) {
				$result["messages"][] = "Сотрудник найден.";
				$task = ($withDutyStatus == true)
							? $this->getTaskWithDutyStatus([ $employeeId ], $date_start, $date_end)
							: $this->getTask([ $employeeId ], $date_start, $date_end);
				$result["messages"] = array_merge($result["messages"],$task["messages"]);
				$result["success"] = $task["success"];
				$result["data"] = $task["data"];
			} else {
				$result["success"] = false;
				$result["messages"][] = "Сотрудник не найден.";
				$result["messages"][] = "Ну удалось получить расписание сотрудника.";
			}
		} else {
			$result["success"] = false;
		}
		return $result;
	}
	
	public function updateAppointmentsToDoctors() {

		$criteria = new CDbCriteria();
		$criteria->with = [
			'currentPlaceOfWork' => ['with' => ['company']],
		];
		$criteria->addInCondition('company.linkUrl', array_keys(self::$DEFAULT_URL));
		$doctors = Doctor::model()->findAll($criteria);
		foreach ($doctors as $doctor) {
			$this->url = self::$DEFAULT_URL[$doctor->currentPlaceOfWork->company->linkUrl];
			//echo $doctor->link."\n";
			$this->updateAppointmentsToDoctor($doctor);
		}
	}
	/*
	 *	Обновляет расписание сотрудника в системе emportal
	 *	$doctor - объект Сотрудник
	 *	$clinic - объект клиника
	 *	$date_start - дата начала периода выборки (по умолчанию сегодня);
	 *	$date_end - дата конца периода выборки (по умолчанию +30 дней к дате начала периода);
	 */
	public function updateAppointmentsToDoctor($doctor,$clinic=null,$date_start=null,$date_end=null) {
		$result = ["data"=>null, "success"=>false, "messages"=>[]];
		if(!is_object($doctor)) $doctor = Doctor::model()->findByLink($doctor);
		//if(!is_object($clinic)) $clinic = Address::model()->findByLink($clinic);
		// Если сотрудник не из клиники 'Евромед' на портале, то не обновляем его
		if(!in_array($doctor->currentPlaceOfWork->company->linkUrl, array_keys(self::$DEFAULT_URL))) {
			$result["messages"][] = "Сотрудник не из клиники 'Евромед'";
			$result["success"] = false;
		} else {
			$result["messages"][] = "Сотрудник из клиники 'Евромед'";
			if($date_start === null) $date_start = date("Y-m-d H:i:s", time());
			if($date_end === null) $date_end = date("Y-m-d 00:00:00", (strtotime($date_start) + self::MONTH_TIME));
			if(array_product(explode(":", date("H:i:s",strtotime($date_end)))) === 0) $date_end = date("Y-m-d H:i:s", (strtotime($date_end) + 86399));
			$date_start_minute = date("i", strtotime($date_start));
			if($date_start_minute > 30 OR ($date_start_minute > 0 AND $date_start_minute < 30)) $date_start = date("Y-m-d H:30:00",strtotime($date_start));
			// Получить из Euromed расписание сотрудника по его ФИО;
			$taskByFIO = $this->getTaskByFIO($doctor->name, $date_start, $date_end);
			$result["messages"] = array_merge($result["messages"], $taskByFIO["messages"]);
			if($taskByFIO["success"]) {
				// Создать массив с Датой/Временем начала визита, со значением, преобразованным в кратное 30 минутам
				$eurotime = [];
				foreach ($taskByFIO["data"] as $index=>$task) {
					$visitstart = explode(":", $task["time"]["visitstart"]);
					$visitstart[0] = intval($visitstart[0]);
					$visitstart[1] = intval($visitstart[1]);
					if($visitstart[1] === 0 OR $visitstart[1] === 30) $eurotime[$index] = $task["date"]." ".str_pad($visitstart[0],2,'0',STR_PAD_LEFT).":".str_pad($visitstart[1],2,'0',STR_PAD_LEFT).":00";
					elseif($visitstart[1] > 0 AND $visitstart[1] < 30) $eurotime[$index] = $task["date"]." ".str_pad($visitstart[0],2,'0',STR_PAD_LEFT).":00:00";
					else $eurotime[$index] = $task["date"]." ".str_pad($visitstart[0],2,'0',STR_PAD_LEFT).":30:00";
				}
				// Перебрать все возможные дыты/время визита за указанный период
				$time_end = strtotime($date_end);
				$time_start = strtotime($date_start);

				$appointments = Yii::app()->db->createCommand()
									->setFetchMode(PDO::FETCH_KEY_PAIR)
									->select("plannedTime, statusId")
									->from(AppointmentToDoctors::model()->tableName())
									->where("doctorId=:doctorId AND (plannedTime >= :date_start AND plannedTime <= :date_end)",[
										"doctorId"=>$doctor->id, 
										"date_start"=>$date_start, 
										"date_end" => $date_end
									])->order('plannedTime')->queryAll();


				for ($time = $time_start; $time <= $time_end; $time+=1800) {
					$datetime = date("Y-m-d H:i:00", $time);
					$freeAppointment = in_array($datetime, $eurotime);

					if(!$freeAppointment && !$appointments[$datetime]) {
						$newAppointment = new AppointmentToDoctors();
						$newAppointment->name = "DISABLED";
						$newAppointment->doctorId = $doctor->id;
						$newAppointment->companyId = $doctor->currentPlaceOfWork->companyId;
						$newAppointment->addressId = $doctor->currentPlaceOfWork->addressId;
						$newAppointment->createdDate = date("Y-m-d H:i:s", time());
						$newAppointment->plannedTime = $datetime;
						$newAppointment->statusId = AppointmentToDoctors::DISABLED;
						$newAppointment->save();						
					}

					if($freeAppointment && $appointments[$datetime]==AppointmentToDoctors::DISABLED) {
						Yii::app()->db->createCommand()->delete(AppointmentToDoctors::model()->tableName(), 'doctorId=:doctorId AND plannedTime = :plannedTime', ["doctorId"=>$doctor->id,"plannedTime"=>$datetime]);
					}

				}

				// for ($time = $time_start; $time <= $time_end; $time+=1800) {
				// 	$datetime = date("Y-m-d H:i:00", $time);
				// 	/*
				// 	if(is_object($clinic)) {
				// 		$weekDayTime = $clinic->workingHours->getWeekDayTime(date("l", $time));
				// 		$currentTime = array_product(explode(":", date("H:i", $time)));
				// 		if($currentTime < $weekDayTime["start"] OR $weekDayTime["finish"] < $currentTime) continue;
				// 	}
				// 	*/

				// 	$appointment = AppointmentToDoctors::model()->find("doctorId=:doctorId AND plannedTime=:plannedTime",["doctorId"=>$doctor->id, "plannedTime"=>$datetime]);
				// 	$existsAppointment = !empty($appointment);
				// 	$freeAppointment = in_array($datetime, $eurotime);
				// 	if($freeAppointment AND $existsAppointment AND strval($appointment->statusId)===strval(AppointmentToDoctors::DISABLED)) {
				// 		$status = "enable";
				// 	} else {
				// 		$status = $existsAppointment ? "exists" : ($freeAppointment ? "free" : "disable");
				// 	}
				// 	switch ($status) {
				// 		// Если время не занято в системе emportal и не свободно в системе Euromed, то отключить это время
				// 		case "disable":
				// 			$newAppointment = new AppointmentToDoctors();
				// 			$newAppointment->name = "DISABLED";
				// 			$newAppointment->doctorId = $doctor->id;
				// 			$newAppointment->companyId = $doctor->currentPlaceOfWork->companyId;
				// 			$newAppointment->addressId = $doctor->currentPlaceOfWork->addressId;
				// 			$newAppointment->createdDate = date("Y-m-d H:i:s", time());
				// 			$newAppointment->plannedTime = $datetime;
				// 			$newAppointment->statusId = AppointmentToDoctors::DISABLED;
				// 			if($newAppointment->save()) { $status = "DISABLED"; }
				// 			break;
				// 		// Если время отключено в системе emportal, но свободно в системе Euromed, то включить это время
				// 		case "enable":
				// 			if($appointment->delete()) { $status = "ENABLED"; }
				// 			break;
				// 	}
				// 	$result["data"][$datetime] = $status;
				// }
				$result["messages"][] = "Расписание сотрудника обновлено.";
				$result["success"] = true;
			} else {
				$result["messages"][] = "Не удалось обновить расписание сотрудника.";
				$result["success"] = false;
			}
		}
		return $result;
	}
}
