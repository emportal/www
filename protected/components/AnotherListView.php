<?php

Yii::import('zii.widgets.CListView');

class AnotherListView extends CListView {

	/**
	 * @var string the HTML tag name for the container of all data item display. Defaults to 'div'.
	 * @since 1.1.4
	 */
	public $itemsTagName = 'table';

	/**
	 * @var string the URL of the CSS file used by this list view. Defaults to null, meaning using the integrated
	 * CSS file. If this is set false, you are responsible to explicitly include the necessary CSS file in your page.
	 */
	public $cssFile = false;

	/**
	 * @var string the base script URL for all list view resources (e.g. javascript, CSS file, images).
	 * Defaults to null, meaning using the integrated list view resources (which are published as assets).
	 */
	public $baseScriptUrl	 = '/shared/js/anotherlistview';
	public $updateSelector	 = '#load-more';
	public $moreCssClass	 = '';
	public $moreText		 = '';
	
	public $ajaxAppend;
	public $ajaxUpdate  = 'load-more';

	public function renderMore() {
		
		if($this->enablePagination 
				&& $this->dataProvider->pagination->pageCount > 1 
				&& $this->dataProvider->pagination->currentPage+1 < $this->dataProvider->pagination->pageCount)

		echo CHtml::link(
				$this->moreText,
				$this->dataProvider->pagination->createPageUrl(
					$this->owner,
					$this->dataProvider->pagination->currentPage+1
					),
				array(
					'id'	 => 'load-more',
					'class'	 => $this->moreCssClass
				)
		);
	}

	/**
	 * Registers necessary client scripts.
	 * @
	 */
	public function registerClientScript()
	{
		$id=$this->getId();

		if($this->ajaxUpdate===false)
			$ajaxUpdate=array();
		else
			$ajaxUpdate=array_unique(preg_split('/\s*,\s*/',$this->ajaxUpdate,-1,PREG_SPLIT_NO_EMPTY));
		if($this->ajaxAppend===false)
			$ajaxAppend=array();
		else
			$ajaxAppend=array_unique(preg_split('/\s*,\s*/',$this->ajaxAppend,-1,PREG_SPLIT_NO_EMPTY));
		$options=array(
			'ajaxAppend'=>$ajaxAppend,
			'ajaxUpdate'=>$ajaxUpdate,
			'ajaxVar'=>$this->ajaxVar,
			'pagerClass'=>$this->pagerCssClass,
			'loadingClass'=>$this->loadingCssClass,
			'sorterClass'=>$this->sorterCssClass,
			'enableHistory'=>$this->enableHistory
		);
		if($this->ajaxUrl!==null)
			$options['url']=CHtml::normalizeUrl($this->ajaxUrl);
		if($this->updateSelector!==null)
			$options['updateSelector']=$this->updateSelector;
		foreach(array('beforeAjaxUpdate', 'afterAjaxUpdate', 'ajaxUpdateError') as $event)
		{
			if($this->$event!==null)
			{
				if($this->$event instanceof CJavaScriptExpression)
					$options[$event]=$this->$event;
				else
					$options[$event]=new CJavaScriptExpression($this->$event);
			}
		}

		$options=CJavaScript::encode($options);
		$cs=Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$cs->registerCoreScript('bbq');
		if($this->enableHistory)
			$cs->registerCoreScript('history');
		$cs->registerScriptFile($this->baseScriptUrl.'/jquery.yiilistview.js',CClientScript::POS_END);
		$cs->registerScript(__CLASS__.'#'.$id,"jQuery('#$id').yiiListView($options);");
	}
}