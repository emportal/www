function searchFormConstructor() {
    var settings = {
        'usedProtocol': 'http://',
        'domain': 'local.emportal.ru',
    };
	function ajaxCall(action, params, callback) {
		var nocache = new Date().getTime(),
			xmlhttp = new XMLHttpRequest(),
			ajaxQuery = '',	
			srcDir = settings.usedProtocol + settings.domain + "/" + "shared/shared/bundles/searchForm.js",
			paramsStr = '';
		
		for (k in params) {
			paramsStr += '&' + k + '=' + params[k];
		}
		
		ajaxQuery = "action=" + action + paramsStr + "&nocache=" + nocache;
			
		xmlhttp.open("GET", srcDir + "?" + ajaxQuery, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); //we have to use proper encoding	
		xmlhttp.send();
		
		xmlhttp.onreadystatechange = function() {
			
			if (xmlhttp.readyState == 4) {
				
				var response = xmlhttp.responseText;
				var data = JSON.parse(response);
				callback(data);
			}
		}
	}
}