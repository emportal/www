<?php

Yii::import('zii.widgets.CPortlet');

class SearchForm extends CPortlet
{
    private $params;
    
    public function __construct($params = [])
    {
        $this->params = array_replace_recursive([
            'filePath' => Yii::getPathOfAlias('application.components.searchForm'),
            'settings' => [
                'partner_refererId' => 0,
            	'default_specialty' => '',
            	'title' => 'Поиск врача',
                'scheme' => Yii::app()->request->isSecureConnection ? 'https' : 'http',
                'host' => $_SERVER['HTTP_HOST'],
            ],
            'styles' => [
                'width' => 180,
                'height' => 180,
            ]
        ], $params);
        parent::__construct();
    }
    
	public function run()
	{
        $imagePath = $this->params['settings']['scheme'] . '://'
            . $this->params['settings']['host']
            . Yii::app()->assetManager->publish($this->params['filePath'] . DIRECTORY_SEPARATOR . 'img');
        
        $this->params = array_replace_recursive($this->params, [
            'image' => [
                'magnifying_glass' => $imagePath . '/' . 'magnifying_glass.png',
                'selector_arrow' => $imagePath . '/' . 'selector_arrow.png',
            ]
        ]);
        
		$this->render('index', ['params' => $this->params]);
	}
}
?>