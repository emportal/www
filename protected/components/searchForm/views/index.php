<style>
form.empSearchForm {
    display: block !important;
    box-sizing: border-box !important;
    width: <?= $params['styles']['width'] ?>px;
    height: <?= $params['styles']['height'] ?>px;
    border: 1px solid lightgray !important;
    color: #8e8e8e !important;
    font: <?= round(14*$params['styles']['width']/180) ?>px Arial !important;
    padding: 0 7px !important;
    text-align: center !important;
}
form.empSearchForm>div:first-child {
    line-height: <?= $params['styles']['height']/4 ?>px !important;
}
form.empSearchForm>div:first-child img {
    vertical-align: text-bottom !important;
    width: <?= round(17*$params['styles']['width']/180) ?>px !important;
}
form.empSearchForm>div {
    -webkit-box-sizing: border-box !important;
    -moz-box-sizing: border-box !important;
    box-sizing: border-box !important;
    height: <?= $params['styles']['height']/4 ?>px !important;
    border: 0px solid !important;
    text-align: center !important;
}
form.empSearchForm input[type="submit"], form.empSearchForm select {
    height: 90% !important;
    text-align: center !important;
}
form.empSearchForm select {
    display: block !important;
    /* width: 166px; */
    width: 100% !important;
    /* height: 40px; */
    /* margin: 7px auto; */
    padding: 8px !important;
    color: #8e8e8e !important;
    font-size: <?= round(11*$params['styles']['width']/180) ?>px !important;
    background: url('<?= $params['image']['selector_arrow'] ?>') no-repeat 95% 55% #fff !important; /* 142 */
    appearance: none !important;
    -moz-appearance: none !important; /* Firefox */
    -webkit-appearance: none !important; /* Safari and Chrome */
    border: 1px solid #ddd !important;
}
form.empSearchForm input[type="submit"] {
    display: block !important;
    width: 112px !important;
    height: 38px !important;
    margin: auto !important;
    border: none !important;
    border-radius: 4px !important;
    color: #fff !important;
    font-size: 18px !important;
    background-color: #4cb8b4 !important;
    cursor: pointer !important;
}
form.empSearchForm>div.table {
    display: table !important;
    width: 100% !important;
}
form.empSearchForm>div.table>div {
    display: table-cell !important;
    width: 100% !important;
    vertical-align: middle !important;
    text-align: center !important;
    padding-bottom: 7px !important;
}
</style>
<form method="GET" action="#utm_source=partner1" class="empSearchForm">
	<div>
        <img src="<?= $params['image']['magnifying_glass'] ?>"></img>
        <?= $params['settings']['title'] ?>
    </div>
    <div>
        <select name="specialty">
            <option value="">Выберите направление</option>
        </select>
    </div>
    <div>
    <select name="metro">
        <option value="">Выберите метро</option>
    </select>
    </div>
    <div class="table">
        <div>
            <input type="hidden" name="partner_refererId">
            <input type="submit" value="Найти"></input>
        </div>
    </div>
</form>
<script>
    var searchForm = {
        'settings': {
            'partner_refererId': <?= $params['settings']['refererId'] ?>,
            'default_specialty': '<?= $params['settings']['default_specialty'] ?>',
            'scheme': '<?= $params['settings']['scheme'] . '://' ?>',
            'host': '<?= $params['settings']['host'] ?>',
            'form': document.getElementsByClassName("empSearchForm")[0],
        },
        'run': function() {
            this.settings.form.action = this.settings.scheme + this.settings.host + '/doctor';
            this.settings.form.querySelectorAll('input')[0].value = this.settings.partner_refererId;
            this.ajaxCall(this.settings.scheme + this.settings.host + '/site/getMetroAndDistrictList', {}, this.fillMetro);
            this.ajaxCall(this.settings.scheme + this.settings.host + '/site/getDoctorSpecialties', {}, this.fillSpecialty);
            <!-- this.settings.form.onsubmit = this.beforeSubmit(); -->
        },
        'ajaxCall': function(srcDir, params, callback) {
            
            var nocache = new Date().getTime(),
                xmlhttp = new XMLHttpRequest(),
                paramsStr = '',
                searchForm = this;
            
            for (k in params) {
                paramsStr += '&' + k + '=' + params[k];
            }
            xmlhttp.open("GET", srcDir, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4) {
                    var data = JSON.parse(xmlhttp.responseText);
                    if (typeof callback == 'function')
                        callback.apply(searchForm, [data]);
                }
            }
        },
        'fillMetro': function(data) {
            var optionsHTML = '<option value="">Выберите метро</option>';
            for (k in data.metroList) {
                optionsHTML += '<option value="metro-' + data.metroList[k]['linkUrl'] + '">' + data.metroList[k]['name'] + '</option>';
            }
            this.settings.form.querySelectorAll('select')[1].innerHTML = optionsHTML;
        },
        'fillSpecialty': function(data) {
            var optionsHTML = '<option value="">Выберите направление</option>';
            for (k in data) {
                optionsHTML += '<option value="' + k + '" ' + (this.settings.default_specialty == k ? 'selected' : '') + '>' + data[k] + '</option>';
            }
            this.settings.form.querySelectorAll('select')[0].innerHTML = optionsHTML;
        },
        /*'beforeSubmit': function(searchForm) {
            return true;
        }*/
        'getParam': function(param) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if (pair[0] == param) {
                    return pair[1];
                }
            }
        }
    };
    searchForm.run();
</script>