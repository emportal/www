<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class SocialUserIdentity extends CUserIdentity
{
	const TYPE_VK = 1;
	const TYPE_FB = 2;
	const TYPE_TWT = 3;
	const TYPE_OK = 4;
	
	
	const ERROR_ACCESS_TOKEN_NOT_FOUND=5;
	const ERROR_USER_NOT_FOUND=6;
	
	const VK_CLIENT_ID = '4430365'; #"4406916";
	const VK_CLIENT_SECRET = '1aAWOkCRfJjlRX2JifLQ'; #"yL7f0LhOs7e5Mx1pPrlX";
	
	public static $VK_SCOPES = [
		'email',
		'friends',
		'offline'
	];
	private $_id;

	public function authenticateFB($authIdentity)
	{
		if ($authIdentity->authenticate()) {
			$email = $authIdentity->getAttribute('email');
			$uid = $authIdentity->getAttribute('id');
			$tokenId = $authIdentity->getAttribute('id');
			
			$token = Token::model()->findByAttributes([
				'uid' => $uid,
				'type' => self::TYPE_FB
			]);
			if(!$token) {
				/* create token record */
				$token			= new Token();
				$token->uid		= $uid;
				$token->token	= $tokenId;
				$token->type	= self::TYPE_FB;						
			} else {
				$token->token = $tokenId;				
			}
			
			if($token->save()) {

			} else {
				MyTools::setMessage('Error create/update token:uid '.$uid);
			}
			/* if vk returns email -> find by email */
			if(!empty($email)) {				
				$user = User::model()->findByAttributes([
					'email'=>$email
				]);				
			}
			/* if notfound by email -> search by token */
			if(empty($user) && $token->userToken->user) {
				$user = $token->userToken->user;
				/* if not found in base by email but added after*/
				if($email && empty($user->email)) {
					$user->email = $email;
				}
			}
			
			
			/*If user already exists*/
			if(!$user) {
				if(!empty($authIdentity->gender)) {
					if($authIdentity->gender === 'male') {				
						$sex = Sex::model()->male()->find();					
					} else {					
						$sex = Sex::model()->female()->find();
					}
				}
				
				$user = new User();		
				$user->email = $email;
				$user->name = $authIdentity->name;						
				if(isset($sex)) $user->sexId = $sex->id;			
				$user->status = 1;
				$user->phoneActivationStatus = 1;					
				$user->phoneActivationDate = new CDbExpression("NOW()");
				
				if(!empty($authIdentity->birthday)) {
					$bdate = explode('/',$authIdentity->birthday);
					if(!empty($bdate) && count($bdate) == 3 ) {
						$user->birthday = $bdate[2].'-'.$bdate[0].'-'.$bdate[1];	
					}
				}
			}
				
			if($user->save(false)) {
				$userToken = UserToken::model()->findByAttributes([
					'tokenId' => $token->id,
					'userId' => $user->id
				]);
				if(!$userToken) {
					$userToken = new UserToken();
					$userToken->tokenId = $token->id;
					$userToken->userId = $user->id;
					$userToken->save();
				}
				
				$this->errorCode=self::ERROR_NONE;				
				$this->_id=$user->id;
			}
			
		}
		
		return !$this->errorCode;		
	}
	public function authenticateOK($authIdentity)
	{
		if ($authIdentity->authenticate()) {
			$email = null;
			$uid = $authIdentity->getAttribute('id');
			$tokenId = $authIdentity->getAttribute('id');
			
			$token = Token::model()->findByAttributes([
				'uid' => $uid,
				'type' => self::TYPE_OK,
			]);
			if(!$token) {
				/* create token record */
				$token			= new Token();
				$token->uid		= $uid;
				$token->token	= $tokenId;
				$token->type	= self::TYPE_OK;						
			} else {
				$token->token = $tokenId;			
			}
			
			if($token->save()) {

			} else {
				MyTools::setMessage('Error create/update token:uid '.$uid);
			}
			/* if vk returns email -> find by email */
			if(!empty($email)) {			
				$user = User::model()->findByAttributes([
					'email'=>$email
				]);				
			}
			/* if notfound by email -> search by token */
			if(empty($user) && $token->userToken->user) {
				$user = $token->userToken->user;
				/* if not found in base by email but added after*/
				if($email && empty($user->email)) {
					$user->email = $email;
				}
			}
			
			
			/*If user already exists*/
			if(!$user) {
				if(!empty($authIdentity->gender)) {
					if($authIdentity->gender === 'male') {				
						$sex = Sex::model()->male()->find();					
					} else {					
						$sex = Sex::model()->female()->find();
					}
				}
				
				$user = new User();		
				$user->email = $email;
				$user->name = $authIdentity->name;						
				if(isset($sex)) $user->sexId = $sex->id;			
				$user->status = 1;
				$user->phoneActivationStatus = 1;					
				$user->phoneActivationDate = new CDbExpression("NOW()");
				
				if(!empty($authIdentity->birthday)) {
					$user->birthday = $authIdentity->birthday;	
				}
			}
				
			if($user->save(false)) {
				$userToken = UserToken::model()->findByAttributes([
					'tokenId' => $token->id,
					'userId' => $user->id
				]);
				if(!$userToken) {
					$userToken = new UserToken();
					$userToken->tokenId = $token->id;
					$userToken->userId = $user->id;
					$userToken->save();
				}
				
				$this->errorCode=self::ERROR_NONE;				
				$this->_id=$user->id;
			}
			
		}
		
		return !$this->errorCode;		
	}

	/******/
	/* VK */
	/******/
	
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticateVK()
	{
		$url = self::vkGetTokenAccessUrl();
		
		$response = $this->vkRequestToApi($url);
		#$response=json_decode(file_get_contents($url));
		
		if($response != NULL && empty($response['error'])) {	
			
			$email = $response['email'];
			$uid = $response['user_id'];
			$tokenId = $response['access_token'];
			
			$token = Token::model()->findByAttributes([
				'uid' => $uid,
				'type' => self::TYPE_VK
			]);
			if(!$token) {
				/* create token record */
				$token			= new Token();
				$token->uid		= $uid;
				$token->token	= $tokenId;
				$token->type	= self::TYPE_VK;						
			} else {
				$token->token = $tokenId;				
			}
			
			if($token->save()) {

			} else {
				MyTools::setMessage('Error create/update token:uid '.$uid);
			}
			/* if vk returns email -> find by email */
			if(!empty($email)) {				
				$user = User::model()->findByAttributes([
					'email'=>$email
				]);				
			}
			/* if notfound by email -> search by token */
			if(empty($user) && $token->userToken->user) {
				$user = $token->userToken->user;
				/* if not found in base by email but added after*/
				if($email && empty($user->email)) {
					$user->email = $email;
				}
			}
			
			
			/*If user already exists*/
			if(!$user) {				
				$response = $vkUserData = $this->vkUsersGet([
					'user_ids' => $uid,
					'fields' => 'sex,bdate',
					'name_case' => 'nom'
				])['response'];
				
				if($response[0]['sex'] === 2) {				
					$sex = Sex::model()->male()->find();					
				} else {					
					$sex = Sex::model()->female()->find();
				}
				
				$user = new User();		
				$user->email = $email;
				$user->name = $response[0]['last_name']." ".$response[0]['first_name'];						
				$user->sexId = $sex->id;			
				$user->status = 1;
				$user->phoneActivationStatus = 1;					
				$user->phoneActivationDate = new CDbExpression("NOW()");
				
				$bdate = explode('.',$response[0]['bdate']);
				if(!empty($bdate) && count($bdate) == 3 ) {
					$user->birthday = $bdate[2].'-'.$bdate[1].'-'.$bdate[0];	
				}		
				
			}
				
			if($user->save(false)) {
				$userToken = UserToken::model()->findByAttributes([
					'tokenId' => $token->id,
					'userId' => $user->id
				]);
				if(!$userToken) {
					$userToken = new UserToken();
					$userToken->tokenId = $token->id;
					$userToken->userId = $user->id;
					$userToken->save();
				}
				
				$this->errorCode=self::ERROR_NONE;				
				$this->_id=$user->id;
			}
			
			
			//
			
			
			
			
			
			
			
			/*Сохраняем*/
			//$user=User::model()->find('LOWER(id_vk_user)=?',array($response->user_id));		
			
			/*if(!$response->access_token) {
				$this->errorCode=self::ERROR_ACCESS_TOKEN_NOT_FOUND;
				#не получен токен от VK
			} elseif($user===null) {
				#регистрируем нового пользователя
				$this->errorCode=self::ERROR_NONE;
				
				$user = new User();
				if($user->isNewRecord) {					
										
					$response_a=json_decode(file_get_contents($url),true);	
					
					//Регистрируем нового пользователя по user_id
					$user->access_token = $response->access_token;
					$user->name=$response_a['response']["0"]["first_name"];					
					$user->surname=$response_a['response']["0"]["last_name"];	
					$user->mail = '';
					$user->password = '';					
					$user->reg_time =new CDbExpression('NOW()');
					$user->active=1;			
					$user->id_vk_user = $response->user_id;
					$user->save();
					
				}				
				
			} else {
				echo 1;
				$this->errorCode=self::ERROR_NONE;				
				$this->_id=$user->id_user;
				
				
			}*/
		}
		
		return !$this->errorCode;		
	}
	public function vkRequestToApi($url) {
		$curl = curl_init();
		
		//уcтанавливаем урл, к которому обратимся
		curl_setopt($curl, CURLOPT_URL,$url);
		curl_setopt($curl, CURLOPT_PORT , 443);
		//включаем вывод заголовков
		curl_setopt($curl, CURLOPT_HEADER, 0);
		
		//передаем данные по методу post
		#curl_setopt($curl, CURLOPT_POST, 1);
		
		//теперь curl вернет нам ответ, а не выведет
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$res = curl_exec($curl);
		$data = json_decode($res, true);
		return $data;
	}
	
	public static function vkGetTokenAccessUrl() {
		$url = 'https://oauth.vk.com/access_token?'
			. 'client_id='.self::VK_CLIENT_ID.'&client_secret='.self::VK_CLIENT_SECRET.'&'
			. 'code='.Yii::app()->request->getQuery('code').'&redirect_uri='	. self::vkRedirectUrl();			
		return $url;
		
	}
	
	public static function vkScopes() {
		return implode(",",self::$VK_SCOPES);
	}
	
	public static function vkLoginUrl() {
		return 'https://oauth.vk.com/authorize?client_id='.self::VK_CLIENT_ID.'&scope='.self::vkScopes().'&redirect_uri='.self::vkRedirectUrl().'&response_type=code';
	}
	
	public static function vkRedirectUrl() {
		return "http://".$_SERVER['HTTP_HOST']."/login?type=".self::TYPE_VK;
	}
	/**
	 * $data - key pair array with parametres to users.get query
	 * @param array $data
	 */
	public function vkUsersGet($data) {
		if(!empty($data)) {
			$rows = [];
			foreach($data as $key=>$row) {
				$rows[] = $key."=".$row;
			}
			$url = 'https://api.vk.com/method/users.get?'.implode("&",$rows);
			
			return $this->vkRequestToApi($url);
		} else {
			return 0;
		}
	}
			
	public function getId()
    {
        return $this->_id;
    }
}