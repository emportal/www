<?php

Yii::import('zii.widgets.CPortlet');

class InfoBox extends CPortlet {

	const T_INFO = 'infoBox';
	const T_QUICKLINK = 'quickLink';

	/**
	 * @var string
	 */
	public $title = null;

	/**
	 *
	 * @var string|InfoBox::T_INFO|InfoBox::T_QUICKLINK
	 */
	public $type = false;

	public function init() {
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}

	public function run() {
		$this->renderContent();
		$content = ob_get_clean();
		echo $content;
	}

	protected function renderContent() {

		if ( $this->type )
			$this->render($this->type);
	}

}

?>