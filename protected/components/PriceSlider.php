<?php
Yii::import('zii.widgets.CPortlet');

class PriceSlider extends CPortlet
{
    public $id = null;
    public $type = 'doctor';
	public $formId = null;
	public $name = 'Search';
    public $inputMin = 'priceMin';
    public $inputMax = 'priceMax';

	public function init()
	{
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}
	public function run()
	{
		$this->renderContent();
		$content=ob_get_clean();
		echo $content;
	}

    protected function renderContent()
    {
    	$this->render('priceSlider',array('model'=>$this));
    }
}
?>