<?php

class LinkUrlBehavior extends CActiveRecordBehavior {
	
	public function beforeSave($event) {
		parent::beforeSave($event);
		if ( $this->owner->hasAttribute('linkUrl') && ($this->owner->isNewRecord || empty(trim($this->owner->linkUrl))) ) {
			$count = 0;
			$baseLinkUrl = $linkUrl = MyTools::makeLinkUrl($this->owner->name);
			$doctorFilterDeleted = Doctor::$filterDeleted;
			$companyShowRemoved = Company::$showRemoved;
			$addressShowInactive = Address::$showInactive;
			Doctor::$filterDeleted = false;
			Company::$showRemoved = true;
			Address::$showInactive = true;
			while(ActiveRecord::model(get_class($this->owner))->findByAttributes(['linkUrl' => $linkUrl])) {
				$count++;
				$linkUrl = $baseLinkUrl . '_' . $count;
			};
			Doctor::$filterDeleted = $doctorFilterDeleted;
			Company::$showRemoved = $companyShowRemoved;
			Address::$showInactive = $addressShowInactive;
			
			$this->owner->linkUrl = $linkUrl;
		}
	}	
	
}
