<?php
Yii::import('zii.widgets.CPortlet');

class UserRegion extends CPortlet
{
	public $regions;
	public $selectedRegion = null;	

	public function init()
	{
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}
	public function run()
	{
		$this->renderContent();
		$content = ob_get_clean();
		echo $content;
	}

    protected function renderContent()
    {
		$selectedRegion = Yii::app()->session['selectedRegion'];
		$defaultRegion = [];
		foreach($this->regions as $region)
		{
			if (array_key_exists('default', $region))
				$defaultRegion = $region;
		}
		
		if ($this->selectedRegion == null)
			$this->selectedRegion = $defaultRegion['subdomain'];
		
		$isSamozapis = Yii::app()->params['samozapis'];
		if ($isSamozapis)
		{
			$this->regions = [];
			foreach (Yii::app()->params['regions'] as $region) {
				if($region['samozapis'] || ($selectedRegion == $region['subdomain'] && strval($region['samozapisSubdomain']) !== "")) {
					$this->regions[] = $region;
				}
			}
		}
		
		$this->render('userRegion', array('regions' => $this->regions, 'selectedRegion' => $this->selectedRegion));
    }
	
	public static function getRegionPhones($forLinkHref = false)
	{
		$selectedRegion = Yii::app()->session['selectedRegion'];
		$regionRegistryPhone = Yii::app()->params['regions'][$selectedRegion]['empRegistryPhone'];
		$empRegistryWorks = (date('H') >= 9 AND date('H') <= 21);
		
		$empRegistryPhone = ($regionRegistryPhone && !Yii::app()->params['samozapis'] && $empRegistryWorks) ? $regionRegistryPhone : '';
		$empRegistryPhoneForLinkHref = str_replace([' ', '-', '(', ')'], '', $empRegistryPhone);
		$empRegistryPhoneForLinkHref = str_replace(['+7'], '8', strip_tags($empRegistryPhone));
		
		return [
			'empRegistryPhone' => $empRegistryPhone,
			'empRegistryPhoneForLinkHref' => $empRegistryPhoneForLinkHref
		];
	}
}
?>