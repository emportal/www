<?php

/**
 * Description of ActiveRecord
 *
 * @author neosonic
 */
class ActiveRecord extends CActiveRecord {

	/**
	 * Наборы атриббутов для извлечения
	 * Структура:
	 * <pre>
	 * array('имя набора' => array('имя атрибута', ...атрибуты...), ...наборы...)
	 * </pre>
	 * @var array
	 */	
	public $sets = array();
		
	/* По умолчанию включено отсеивание 'нет данных' но можно выключить */
	public static $filterEmptyData = true;
	/**
	 *
	 * @var boolean 
	 */
	public $syncThis=true;

	public function toArray($set = false, $depth = false) {

		$attributeNames = array();

		if ($set && !empty($this->sets)) {
			if (is_array($set))
				$attributeNames = $set;
			elseif (isset($this->sets[$set]))
				$attributeNames = $this->sets[$set];
			else
				$attributeNames = array($set);
		}

		if (empty($attributeNames))
			$attributes = $this->getAttributes();
		else
			$attributes = $this->getAttributes($attributeNames);

		if (!($depth && $depth > 0))
			return $attributes;

		foreach ($this->relations() as $name => $v) {
			$obj = $this->getRelated($name, false);

			if (is_array($obj)) {
				foreach ($obj as &$val)
					if ($val instanceof ActiveRecord)
						$val = $val->toArray(false, --$depth);
				unset($val);
			}
			elseif ($obj instanceof ActiveRecord)
				$obj = $obj->toArray(false, --$depth);

			$attributes[$name] = $obj;
		}

		return $attributes;
	}

	public function beforeSave() {
		
		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				if (!$this->id) {					
					$this->id = ActiveRecord::create_guid(array_merge($this->attributes, ['roundmicrotime' => round(microtime(true)*1000)]));
				}
			}
						
			
			// TODO пиздец какойто 
			if (isset($this->tableSchema->columns['link']) && !$this->link) {
				
				$currentLink = Yii::app()->db->createCommand()->
						select("max(`link`)")->
						from($this->tableName())->
						queryScalar();
				
				$currentLink = intval($currentLink);
				$currentLink++;
				
				$this->link = str_pad($currentLink, 9, '0', STR_PAD_LEFT);
			}

			return true;
		} else
			return false;
	}

	public function afterSave() {
		parent::afterSave();

		$table = $this->tableName();
		if ($table != 'SyncTables' && $this->id && $this->syncThis) {
			
			/*$cmd = Yii::app()->db->createCommand();

			$action = $this->isNewRecord ? 'insert' : 'update';

			$sql = "INSERT INTO `syncTables` (`tableId`, `table`, `action`,`done`,`timestamp`) VALUES ('{$this->id}', '{$table}', '{$action}',0,NOW());";

			$cmd->text = $sql;
			$cmd->execute();	*/		
		}

		return true;
	}

	public function afterDelete() {
		parent::afterDelete();

		$table = $this->tableName();
		if ($table != 'SyncTables' && $this->id && $this->syncThis) {

			/*$cmd = Yii::app()->db->createCommand();

			$action = 'delete';

			$sql = "INSERT INTO `syncTables` (`tableId`, `table`, `action`,`done`,`timestamp`) VALUES ('{$this->id}', '{$table}', '{$action}',0,NOW());";

			$cmd->text = $sql;
			$cmd->execute();*/
		}
	}

	/**
	 *
	 * @param string $link
	 * @return this
	 * @throws CDbException
	 */
	public function findByLink($link) {

		if (!in_array('link', $this->tableSchema->columnNames))
			throw new CDbException('Current model don`t have link column!');

		$model = $this->findByAttributes(array(
					'link' => $link,
		));
		if(!$model && in_array('linkUrl', $this->tableSchema->columnNames) && $link != null) {			
			$model = $this->findByAttributes(array(
					'linkUrl' => $link,
			));
		}
		return $model;
	}

	public function behaviors() {
		return array(
			'activerecord-relation' => array(
				'class' => 'ext.behaviors.activerecord-relation.EActiveRecordRelationBehavior',
			),
		);
	}

	public function beforeFind() {
		parent::beforeFind();
		#var_dump($this->tableSchema);
		if (in_array('name', $this->tableSchema->columnNames) && ActiveRecord::$filterEmptyData) {
			$criteria = $this->dbCriteria;
			$criteria->addCondition("LOWER(" . $this->tableAlias . ".name) <> 'нет данных'");
			$this->dbCriteria = $criteria;
		}
		/* $criteria = $this->dbCriteria;
		  if($this->tableSchema['name'])
		  $criteria-> */
	}
	/**
	 * 
	 * @param array $keys Массив содержащий ключевые поля, которые не могу повторяться
	 * @return string guid
	 */
	public static function create_guid($keys) {   //Генерация GUID 
		$glue = "_";		
		$str = join($glue, $keys);
		$hash = md5($str);
		$guid = 'EMPORTAL-'.substr($hash,  8,  4). 
         '-'.substr($hash, 12,  4). 
         '-'.substr($hash, 16,  4). 
         '-'.substr($hash, 20, 12); 
		 return $guid; 
	}

}

?>
