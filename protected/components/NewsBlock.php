<?php

class NewsBlock extends CWidget {

	/**
	 * @var string
	 */
	public $title = null;

	public function init() {
	}

	public function run() {		
		
		$news = News::model()->published()->recently()->findAll();		
		#$news = News::model()->random()->findAll();		
		if ( count($news) ) {
			$this->render('newsBlock', array('news'=>$news));
		}
	}

}

?>