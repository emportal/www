<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

	private $_id;

	const ERROR_NOT_ACTIVATED = 5;

	public function authenticate($md5 = false) {
		
		$this->username = trim($this->username);
		//findByAttributes(array("email" => $this->username));
		if($user = User::model()->find("email = :username", array(':username'=>$this->username))) {
			$user->md5 = $md5;
		}
		
		if(!$user ) {
			
			#$this->username=  preg_replace("/\+/", "", $this->username);
			#$this->username=  preg_replace("")
			#echo $this->username; die();
			$this->username="+".MyRegExp::phoneRegExp($this->username);
			if($user = User::model()->find("telefon = :username", array(':username'=>$this->username))) {				
				$user->md5 = $md5;
			}
		}
		if(is_null($this->password)) {
			$this->password = '';
		}
		if($user && is_null($user->password)) {
			$user->password = '';
		}
		#var_dump($user);
		if ( empty($user) )
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if ( $user->status == 0 )
			$this->errorCode = self::ERROR_NOT_ACTIVATED;
		else if ( !$user->validatePassword($this->password) )
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			$this->_id		 = $user->id;
			$this->errorCode = self::ERROR_NONE;
			
			/* set admin in session by email */
			$adminEmails = [
				#'klka1@live.ru', # Fedorov Dmitry
				#'fedorov.dmitry@edvancemedia.com',  # Fedorov Dmitry
				#'a.f.dyachkov@gmail.com',
				'roma.zaytsev@gmail.com',
			];
			if(in_array($user->email,$adminEmails)) {
				Yii::app()->session['isAdmin'] = 1;
			} else {
				Yii::app()->session['isAdmin'] = false;
			}
			if ($user->hasRight('ACCESS_ALL_MANAGERS_DATA'))
				Yii::app()->session['isAdmin'] = 1;
		}
		
		return !$this->errorCode;
	}

	public function getId() {
		return $this->_id;
	}

}