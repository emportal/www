<?php
Yii::import('zii.widgets.CPortlet');

class UserMenu extends CPortlet
{
	public $visible=false;

	protected function renderContent()
	{
		if($this->visible) {
			$this->render('userMenu');
		}
	}
}
?>