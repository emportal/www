<?php
Yii::import('zii.widgets.CBreadcrumbs');

/**
 * Description of Breadcrumbs
 *
 * @author NeoSonic
 */
class Breadcrumbs extends CBreadcrumbs {

	/**
	 * @var array the HTML attributes for the breadcrumbs container tag.
	 */
	public $htmlOptions=array('class'=>'crumbs');

	/**
	 * @var string the separator between links in the breadcrumbs. Defaults to ' &raquo; '.
	 */
	public $separator='<span> &gt; </span>';

	/**
	 * Renders the content of the portlet.
	 */
	public function run()
	{

		if($this->htmlOptions['class'] == 'crumbs') {
			$this->activeLinkTemplate = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{url}" itemprop="url"><span itemprop="title" class="link">{label}</span></a></span>';

			$this->inactiveLinkTemplate = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">{label}</span><a href="{url}" itemprop="url" style="display:none;">{label}</a></span>';
		}
		
		if(empty($this->links))
			return;

		$definedLinks = $this->links;

		echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";
		$links=array();
		if($this->homeLink===null)
			$definedLinks=array(Yii::t('zii','Home') => Yii::app()->homeUrl)+$definedLinks;
		elseif($this->homeLink!==false)
			$links[]=$this->homeLink;
		foreach($definedLinks as $label=>$url)
		{
			if(is_string($label) || is_array($url))
				$links[]=strtr($this->activeLinkTemplate,array(
					'{url}'=>CHtml::normalizeUrl($url),
					'{label}'=>$this->encodeLabel ? CHtml::encode($label) : $label,
				));
			else {
				$links[]=strtr($this->inactiveLinkTemplate,array(
					'{url}'=>Yii::app()->request->requestUri,
					'{label}'=>$this->encodeLabel ? CHtml::encode($url) : $url,
				));
			}
		}
		echo implode($this->separator,$links);
		echo CHtml::closeTag($this->tagName);
	}
}

?>
