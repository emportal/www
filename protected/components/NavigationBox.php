<?php
Yii::import('zii.widgets.CPortlet');

/**
 * @property Controller $controller
 */

class NavigationBox extends CPortlet
{
    public $title=null;

	public function init()
	{
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}
	public function run()
	{
		$this->renderContent();
		$content=ob_get_clean();
		echo $content;
	}

    protected function renderContent()
    {
	        $this->render('navigationBox');
    }
}
?>