<?php
/* Создаем объект класса
 * устанавливаем номера телефонов для рассылки,
 * устанавливаем тип сообщения,
 * устанавливаем сгенерированный секретный код,  
 * Пример
 * 
 *  */
/*
 		$sms=new SmsGate();	
		$sms->phone="+79046428629";
		$sms->message=SmsGate::VERIFICATION;
		$code=$sms->generateCode();		
		
		if($sms->send())
			echo $sms->result;
 */

class SmsGate extends CComponent {
	
	const NONE = 0;
	const VERIFICATION = 1;
	const REGISTER = 2;
	const PASSWORD = 3;
	const CHANGE_PHONE = 4;
	const USER_SEND_TO_REGISTER = 5;
	const CLINIC_SEND_TO_REGISTER = 10;
	
	/* sms-uslugi */
	
	private $url = 'https://lcab.sms-uslugi.ru/lcabApi/sendSms.php';
	private $log = 'leeza';
	private $pass = '7641830';
	
	
	/* jaysms */
	#private $url = "http://www.jaysms.com/smsgate/send.html";
	/**
	 * account login
	 * format 7**********
	 * @var string 
	 */
	#private $log = '79210968255';
	
	/**
	 * account password
	 * @var string 
	 */
	#private $pass = "ljrnjh2012";
	
	/**
	 * sender name, applied by smsgate server
	 * @var string 
	 */
	#private $sender = "emportal";
	
	/**
	 * Message Text
	 * @var string UTF-8 
	 */
	private $message = "";  
	private $phone = "";
	private $user_pass = "";
	private $code = "";
	public $errors = [];	
	public $type;
	public $request="";

	public $result;
	
	
	public function prepareMessage() {
		/*$arr=array(
			'_code_',
			'_pass_'
		);
		$arr_replace=array(
			$this->code,
			$this->user_pass
		);*/
		#$this->message=urlencode(str_replace($arr,$arr_replace,$this->message));	

		/* sms-uslugi */
		$this->message=urlencode($this->message);
		$this->phone=trim($this->phone,"+");
		$this->request="login=$this->log&password=$this->pass&txt=$this->message&to=$this->phone&source=emportal&onlydelivery=1";
		
		/* jaysms */	
		/*
		$this->message=urlencode($this->message);
		$this->phone=urlencode($this->phone);
		$this->request="log=$this->log&pass=$this->pass&sender=$this->sender&message=$this->message&phone=$this->phone";
		*/
		
	}
	
	public function send() {
		
		$no_error = 1;
		if(!$this->message) {
			$this->errors[]='No message';
			$no_error =0;
		}		
		if(!$this->phone) {
			$this->errors[]='No phone';
			$no_error =0;
		}
		
		if($no_error) {
			if(Yii::app()->params['devMode']) {
				if(empty(Yii::app()->params['devPhone'])) {
					MyTools::ErrorReport($this->message, ['roman.z@emportal.ru']);
					return true;
				}
				$this->setPhone(Yii::app()->params['devPhone']);
			}
			$this->prepareMessage();
			$this->sendSms();
			
			return true;
		}				
		return false;
	}
	public function test() {
		//$this->generateCode();
		//$this->user_pass=User::generate_password(6, true);
		//$this->setMessage(self::REGISTER);		
		$this->phone = "+79213434502";		
		$this->prepareMessage();		
		$this->sendSms();	
	}
	/**
	 * 
	 * @return string set in variable and returns code string
	 */
	public function generateCode() {
		$this->code = mt_rand(10000, 99999);
		return $this->code;
	}
	/**
	 * 
	 * @return string result code
	 */
	private function sendSms() {
		/* sms message logging */
		$parsedRequest = [];
		parse_str($this->request, $parsedRequest);
		
		$smsLog = new LogVarious();
		$smsLog->attributes = [
			'type' => 'sms',
			'value' => json_encode([
				'request' => $parsedRequest,
			], JSON_UNESCAPED_UNICODE),
		];
		$smsLog->save();
		
		/* sms-uslugi.ru */
		$this->result = CJSON::decode(file_get_contents($this->url."?".$this->request));
		
		/*
		 * jaysms
		 * 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$this->url);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->request);
		$this->result = curl_exec($ch);
		curl_close($ch);*/	
		
	}
	public function setMessage($text,$arr_replace = null) {
		if($arr_replace)
			$text=str_replace(array_keys($arr_replace),  array_values($arr_replace),$text);
		$this->message=$text;
	}
	/*
	public function setMessage($type = self::NONE) {
		$this->type = $type;
		switch($type) {
			case self::VERIFICATION:				
				$this->message=trim('
					Код подтверждения: _code_.
					');					
				break;
			case self::REGISTER:
				$this->message=trim('
					Код подтверждения: _code_. Пароль: _pass_
					');	
				break;
			case self::PASSWORD:
				$this->message=trim('
					Пароль для доступа в личный кабинет: _pass_
					');
			case self::USER_SEND_TO_REGISTER:
				$this->message=trim(' 
					Зарегистрирована заявка на прием
						');
				break;
			case self::CLINIC_SEND_TO_REGISTER:
				$this->message=trim(' 
					
						');
				break;
				break;
		}
	}*/
	public function getMessage() {
		return $this->message;
	}
	
	public function getUser_pass() {
		return $this->user_pass;
	}
	public function setUser_pass($value) {
		$this->user_pass=$value;
	}
	public function setPhone($value) {
		if(is_string($value)) {			
			$this->phone=$value;
		}
		$this->phone=trim($this->phone,", ");
	}
	
	public function getPhone() {
		return $this->phone;
	}
	public function setCode($value) {
		$this->code = $value;
	}
	public function getCode() {
		return $this->code;
	}
	
}
