<?php

class EmailBlock extends CWidget {

	public $options = [
		'blockId' => '',
		'emailAttributes' => [], /* data-type="2" data-companyUrl="<?= $model->company->linkUrl ?>" data-doctorUrl="<?= $model->linkUrl ?>" */
		'fixedEmail' => '', //используем, если надо сделать предустановленный емаил
		'readonlyEmail' => false, //чтобы поле емаила было неизменяемым
		'inputClasses' => ''
	];

	public function init() {
	}

	public function run()
	{
		$emailAttributes = [];
		$emailAttributesStr = '';
		foreach($this->options['emailAttributes'] as $key => $value)
		{
			$emailAttributes[] = $key . '="' . $value . '"';
		}
		$emailAttributesStr = implode(' ', $emailAttributes);
		
		if ($this->options['inputClasses'])
		{
			$this->options['inputClasses'] = ' ' . $this->options['inputClasses'];
		}
		
		$valuesToPass = [
			'blockId' => $this->options['blockId'], //trackClinicEmailBlock
			'emailAttributesStr' => $emailAttributesStr,
			'inputClasses' => $this->options['inputClasses'],
		];
		
		if (isset(Yii::app()->user->model->email))
		{
			$this->options['fixedEmail'] = Yii::app()->user->model->email;
			$valuesToPass['readonlyEmail'] = true;
		}
		
		if ($this->options['fixedEmail'])
		{
			$valuesToPass['fixedEmail'] = $this->options['fixedEmail'];
		}
		
		$this->render('emailBlock', $valuesToPass);
	}
}

?>