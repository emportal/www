<?php

/*
Функции, которые представляют вещественные числа словами
function FloatToText($R,$Precision)
Преобразуют вещественные (целые) числа в его текстовое представление с точностью до Precision <= 4 знаков после точки.

function CurrencyToText($sum,$price = "RUR")
Преобразует сумму в слова (как целую, так и дробную)

если параметр $price = USD – выводит со словами долларов и центов,
если $price = RUR - со словами рублей и копеек

function ArabicToRoman($num_int)
осуществляет перевод целого числа $num_int в римское представление числа

function RomanToArabic($Rim_num)
осуществляет перевод из римского в арабское представление числа

function AmountOfUnits($AUnit,$R,$Precision,$Options)
То же, что и FloatToText, но с учётом единицы измерения и опциями:
ntoNotReduceFrac: "сорок сотых" вместо "четырёх десятых".
ntoExplicitZero: "ноль целых"
ntoMinus, ntoPlus: "плюс", "минус".

function CountOfUnits($AUnit,$N,$Options)
То же для целых чисел. Все функции модуля реализованы через неё.
*/

//-----------------------------------------------------------------------
/* class as convert data */
class NumberAnaliz
{
    private $FFirstLevel = 0;
    private $FSecondLevel = 0;
    private $FThirdLevel = 0;
    
    var $UnitWord = array('Gender' => 'GENNEUTER', 'Base' => '', 'End1' => '', 'End2' => '', 'End5' => '');
    var $FNumber = 0;
    
    //********************************************
    /* constants of rules */
    public static $WD_EMPTY = array('Gender' => 'GENMASCULINE', 'Base' => '', 'End1' => '', 'End2' => '', 'End5' => '');
    public static $WD_THOUSEND = array('Gender' => 'GENFEMININE', 'Base' => 'тысяч', 'End1' => 'а', 'End2' => 'и', 'End5' => '');
    public static $WD_MILLION = array('Gender' => 'GENMASCULINE', 'Base' => 'миллион', 'End1' => '', 'End2' => 'а', 'End5' => 'ов');
    public static $WD_MILLIARD = array('Gender' => 'GENMASCULINE', 'Base' => 'миллиард', 'End1' => '', 'End2' => 'а', 'End5' => 'ов');
    
    public static $WD_INT = array('Gender' => 'GENFEMININE', 'Base' => 'цел', 'End1' => 'ая', 'End2' => 'ых', 'End5' => 'ых');
    public static $WD_Frac = array(1 => array('Gender' => 'GENFEMININE', 'Base' => 'десят', 'End1' => 'ая', 'End2' => 'ых', 'End5' => 'ых'), 2 => array('Gender' => 'GENFEMININE', 'Base' => 'coт', 'End1' => 'ая', 'End2' => 'ых', 'End5' => 'ых'), 3 => array('Gender' => 'GENFEMININE', 'Base' => 'тысячн', 'End1' => 'ая', 'End2' => 'ых', 'End5' => 'ых'), 4 => array('Gender' => 'GENFEMININE', 'Base' => 'десятитысячн', 'End1' => 'ая', 'End2' => 'ых', 'End5' => 'ых'));
    //********************************************
    /* Рубли, копейки */
    public static $WD_RUBLE = array('Gender' => 'GENMASCULINE', 'Base' => 'рубл', 'End1' => 'ь', 'End2' => 'я', 'End5' => 'ей');
    
    public static $WD_KOPECK = array('Gender' => 'GENFEMININE', 'Base' => 'копе', 'End1' => 'йка', 'End2' => 'йки', 'End5' => 'ек');
    //********************************************
    /* Доллары, центы */
    public static $WD_USD = array('Gender' => 'GENMASCULINE', 'Base' => 'доллар', 'End1' => '', 'End2' => 'а', 'End5' => 'ов');
    
    public static $WD_CENT = array('Gender' => 'GENMASCULINE', 'Base' => 'цент', 'End1' => '', 'End2' => 'а', 'End5' => 'ов');
    //********************************************
    /* секунды */
    public static $WD_SECOND = array('Gender' => 'GENFEMININE', 'Base' => 'секунд', 'End1' => 'а', 'End2' => 'ы', 'End5' => '');
    /* минуты */
    public static $WD_MINUTES = array('Gender' => 'GENFEMININE', 'Base' => 'минут', 'End1' => 'а', 'End2' => 'ы', 'End5' => '');
    /* часы */
    public static $WD_HOURS = array('Gender' => 'GENMASCULINE', 'Base' => 'час', 'End1' => '', 'End2' => 'а', 'End5' => 'ов');
    /* дни */
    public static $WD_DAYS = array('Gender' => 'GENMASCULINE', 'Base' => '', 'End1' => 'день', 'End2' => 'дня', 'End5' => 'дней');
    /* недели */
    public static $WD_WEEKS = array('Gender' => 'GENFEMININE', 'Base' => 'недел', 'End1' => 'я', 'End2' => 'и', 'End5' => 'ь');
    //месяцы
    public static $WD_MONTH = array('Gender' => 'GENMASCULINE', 'Base' => 'месяц', 'End1' => '', 'End2' => 'а', 'End5' => 'ев');
    //годы
    public static $WD_YEAR = array('Gender' => 'GENMASCULINE', 'Base' => '', 'End1' => 'год', 'End2' => 'года', 'End5' => 'лет');
    //********************************************
    
    public static $TenIn = array(1 => 10, 2 => 100, 3 => 1000, 4 => 10000);
    
    public static $MaxPrecision = 4; // до десятитысячных
    
    //-----------------------------------------------------------------------
    
    /* для работы с римскими числами */
    
    public static $Rim = array(1 => 'I', 2 => 'IV', 3 => 'V', 4 => 'IX', 5 => 'X', 6 => 'XL', 7 => 'L', 8 => 'XC', 9 => 'C', 10 => 'CD', 11 => 'D', 12 => 'CM', 13 => 'M');
    public static $Arab = array(1 => 1, 2 => 4, 3 => 5, 4 => 9, 5 => 10, 6 => 40, 7 => 50, 8 => 90, 9 => 100, 10 => 400, 11 => 500, 12 => 900, 13 => 1000);
    
    //-----------------------------------------------------------------------
    /* declaraited functions */
    public static function mod($a, $b)
    {
        return $a % $b;
    }
    public static function div($a, $b)
    {
        return floor($a / $b);
    }
    public static function Trunc($a)
    {
        return self::div($a, 1);
    }
    public static function Frac($a)
    {
        $out = explode('.', $a);
        return $out[1];
    }
    public static function FreeZero($a)
    {
        $s     = '';
        $s_len = strlen($a);
        while ($s_len >= 0) {
            if (substr($a, $s_len, 1) == 0) {
                $s_len--;
            } else {
                $s = substr($a, 0, $s_len + 1);
                break;
            }
        }
        return $s;
    }
    //-----------------------------------------------------------------------
    
    
    //-----------------------------------------------------------------------
    //$Options = (ntoExplicitZero, ntoMinus, ntoPlus, ntoDigits, ntoNotReduceFrac)
    public function CountOfUnits($AUnit = array('Gender' => 'GENNEUTER', 'Base' => '', 'End1' => '', 'End2' => '', 'End5' => ''), $N, $Options = array())
    {
        $result = '';
        if (($N == 0) and (!@in_array('ntoExplicitZero', $Options))) {
            return $result;
        }
        
        if (!@in_array('ntoDigits', $Options)) {
            if (($N < 0) and (@in_array('ntoMinus', $Options))) {
                $result = 'минус ';
            } else if (($N > 0) and (@in_array('ntoPlus', $Options))) {
                $result = 'плюс ';
            } else if ($N == 0) {
                return 'ноль ' . $AUnit['Base'] . $AUnit['End5'];
            }
        } //if
        else {
            if (($N < 0) and (@in_array('ntoMinus', $Options))) {
                $result = '-';
            } else if (($N > 0) and (@in_array('ntoPlus', $Options))) {
                $result = '+';
            }
        } //else
        
        $N = Abs($N);
        
        if (@in_array('ntoDigits', $Options)) {
            $this->SetNumber($N);
            $this->UnitWord = $AUnit;
            $result         = $N . " " . $this->UnitWordInRightForm();
        } //if
        else {
            $Mrd = self::mod(self::div($N, 1000000000), 1000);
            $Mil = self::mod(self::div($N, 1000000), 1000);
            $Th  = self::mod(self::div($N, 1000), 1000);
            $Un  = self::mod($N, 1000);
            
            $result .= $this->ConvertToText(self::$WD_MILLIARD, $Mrd) . $this->ConvertToText(self::$WD_MILLION, $Mil) . $this->ConvertToText(self::$WD_THOUSEND, $Th);
            
            if ($Un > 0) {
                $result .= $this->ConvertToText($AUnit, $Un);
            } else {
                $result .= $AUnit['Base'] . $AUnit['End5'];
            }
        } //else
        return $result;
    }
    //-----------------------------------------------------------------------
    //$Options = (ntoExplicitZero, ntoMinus, ntoPlus, ntoDigits, ntoNotReduceFrac)
    public function AmountOfUnits($AUnit = array('Gender' => 'GENNEUTER', 'Base' => '', 'End1' => '', 'End2' => '', 'End5' => ''), $R, $Precision, $Options = array())
    {
        // Количество цифр после запятой
        
        if ($Precision < 0) {
            $Precision = 0;
        }
        if ($Precision > self::$MaxPrecision) {
            $Precision = self::$MaxPrecision;
        }
        
        $result = '';
        if (($R > 0) and (@in_array('ntoPlus', $Options))) {
            $result = 'плюс ';
        }
        if (($R < 0) and (@in_array('ntoMinus', $Options))) {
            $result = 'минус ';
        }
        
        $R = abs($R);
        
        // Если Precision = 0, т.е. без дробной части, происходит округление в большую сторону
        
        if ($Precision > 0) {
            $n_int = self::Trunc($R);
        } else {
            $n_int = round($R);
        }
        
        // Дробная часть
        
        $n_Frac = round(($R - $n_int) * self::$TenIn[$Precision]);
        
        // Отбрасывание нулей в дробной части
        // опция ntoNotReduceFrac не работает при n_Frac = 0 (т.е. не будет "ноль сотых")
        
        if (!@in_array('ntoNotReduceFrac', $Options)) {
            while ((self::mod($n_Frac, 10) == 0) and ($Precision > 0)) {
                $n_Frac = self::div($n_Frac, 10);
                $Precision--;
            }
        } //if
        
        // Явная запись нуля
        if ($n_int == 0) {
            if ($n_Frac == 0) {
                // При отсутствии дробной части добавление «ноля» происходит вне зависимости от опции ntoExplicitZero
                // "Result +" отброшено, во избежание "минус ноль"
                // при очень маленькой дробной части за пределами точности
                return 'ноль ' . $AUnit['Base'] . $AUnit['End5'];
            } else if (@in_array('ntoExplicitZero', $Options)) {
                $result .= 'ноль целых ';
            }
        } //if
        
        if ($n_Frac == 0) {
            $result .= CountOfUnits($AUnit, $n_int, array());
        } // N единиц
        else {
            $result .= CountOfUnits(self::$WD_INT, $n_int, array());
        } // столько-то целых
        
        if ($n_Frac == 0) {
            return $result;
        }
        
        $result .= CountOfUnits(self::$WD_Frac[$Precision], $n_Frac, array());
        // N десятых, сотых...
        $result .= $AUnit['Base'] . $AUnit['End2'];
        
        return $result;
    }
    
    //-----------------------------------------------------------------------
    
    //перевод вещественного числа в строчное представление
    public function FloatToText($R, $Precision = 4)
    {
        try {
            return AmountOfUnits(self::$WD_EMPTY, $R, $Precision, array(
                'ntoExplicitZero',
                'ntoMinus'
            ));
        }
        catch (Exception $e) {
            return '';
        }
    }
    //-----------------------------------------------------------------------
    
    /* перевод денежной единицы в строковое представление
    $price = RUR – рубли и копейки
    $price = USD – доллары, центы
    
    $price = YEAR - года (только целое число)
    $price = MONTH - месяцы (только целое число)
    $price = WEEKS - недели (только целое число)
    $price = DAY - дни (только целое число)
    $price = HOURS - часы (только целое число)
    $price = MINUTES - минуты (только целое число)
    $price = SECOND - секунды (только целое число)
    
    $valueAndCurrencySeparately = true если надо вывести отдельно число прописью и единицу измерения прописью (работает только с целыми числами)
    */
    public function CurrencyToText($sum, $price = "RUR", $valueAndCurrencySeparately = false)
    {
        $sum = number_format($sum, 2, '.', '');
        
        $price  = trim($price);
        $RubSum = self::Trunc($sum);
        $KopSum = Round(self::Frac($sum));
        
        if ($KopSum > 100) {
            $KopSum = round($KopSum / 100, 2);
            $RubSum = $RubSum + self::Trunc($KopSum);
            $KopSum = self::FreeZero(Round(self::Frac($KopSum) * 100));
        }
        
        $sprice  = self::$WD_EMPTY;
        $sprice1 = self::$WD_EMPTY;
        switch ($price) {
            case 'USD':
                $sprice  = self::$WD_USD;
                $sprice1 = self::$WD_CENT;
                break;
            case 'SECOND':
                $sprice = self::$WD_SECOND;
                break;
            case 'MINUTES':
                $sprice = self::$WD_MINUTES;
                break;
            case 'HOURS':
                $sprice = self::$WD_HOURS;
                break;
            case 'DAY':
                $sprice = self::$WD_DAYS;
                break;
            case 'WEEKS':
                $sprice = self::$WD_WEEKS;
                break;
            case 'MONTH':
                $sprice = self::$WD_MONTH;
                break;
            case 'YEAR':
                $sprice = self::$WD_YEAR;
                break;
            case 'RUR':
                $sprice  = self::$WD_RUBLE;
                $sprice1 = self::$WD_KOPECK;
                break;
        }
        $result = self::CountOfUnits($sprice, $RubSum, array('ntoExplicitZero'));
        if (!$valueAndCurrencySeparately)
            $result .= ' ' . self::CountOfUnits($sprice1, $KopSum, array());
        $result = trim($result);
        if ($result != "") {
            $str       = strtoupper($result[0]);
            $result[0] = $str;
        }
        if ($valueAndCurrencySeparately) {
            $words = explode(' ', $result);
            $currency = array_pop($words);
            $result = [
                'valueText' => implode(' ', $words),
                'currencyText' => $currency,
            ];
        }
        return $result;
    }
    //-----------------------------------------------------------------------
    
    //перевод целого числа в римское представление
    public function ArabicToRoman($num_int)
    {
        $num_int = self::Trunc($num_int);
        $result  = '';
        $i       = 13;
        while ($num_int > 0) {
            while (self::$Arab[$i] > $num_int) {
                $i--;
            }
            $result .= self::$Rim[$i];
            $num_int = $num_int - self::$Arab[$i];
        }
        return $result;
    }
    
    //перевод римского числа в обычное целое
    function RomanToArabic($rim_num)
    {
        $result = 0;
        $i      = 13;
        $p      = 0;
        while ($p <= strlen($rim_num)) {
            while (substr($rim_num, $p, strlen(self::$Rim[$i])) != self::$Rim[$i]) {
                $i--;
                if ($i == 0) {
                    return $result;
                }
            }
            $result .= self::$Arab[$i];
            $p = $p + strlen(self::$Rim[$i]);
        }
        return $result;
    }
    //-----------------------------------------------------------------------
    
    function Levels($I)
    {
        switch ($I) {
            case 1:
                return $this->FFirstLevel;
            case 2:
                return $this->FSecondLevel;
            case 3:
                return $this->FThirdLevel;
        }
    }
    
    function SetNumber($AValue)
    {
        if ($this->FNumber != $AValue) {
            $this->FNumber      = $AValue;
            $this->FFirstLevel  = self::mod($this->FNumber, 10);
            $this->FSecondLevel = self::mod(self::div($this->FNumber, 10), 10);
            $this->FThirdLevel  = self::mod(self::div($this->FNumber, 100), 10);
            if ($this->FSecondLevel == 1) {
                $this->FFirstLevel  = $this->FFirstLevel + 10;
                $this->FSecondLevel = 0;
            }
        }
    }
    
    private function GetNumberInWord($N, $Level)
    {
        if ($Level == 1) {
            switch ($N) {
                case 0:
                    return '';
                case 1:
                    if ($this->Gender() == 'GENMASCULINE') {
                        return 'один';
                    } else if ($this->Gender() == 'GENFEMININE') {
                        return 'одна';
                    } else if ($this->Gender() == 'GENNEUTER') {
                        return 'одно';
                    }
                case 2:
                    if ($this->Gender() == 'GENMASCULINE') {
                        return 'два';
                    } else if ($this->Gender() == 'GENFEMININE') {
                        return 'две';
                    } else if ($this->Gender() == 'GENNEUTER') {
                        return 'два';
                    }
                case 3:
                    return 'три';
                case 4:
                    return 'четыре';
                case 5:
                    return 'пять';
                case 6:
                    return 'шесть';
                case 7:
                    return 'семь';
                case 8:
                    return 'восемь';
                case 9:
                    return 'девять';
                case 10:
                    return 'десять';
                case 11:
                    return 'одиннадцать';
                case 12:
                    return 'двенадцать';
                case 13:
                    return 'тринадцать';
                case 14:
                    return 'четырнадцать';
                case 15:
                    return 'пятнадцать';
                case 16:
                    return 'шестнадцать';
                case 17:
                    return 'семнадцать';
                case 18:
                    return 'восемнадцать';
                case 19:
                    return 'девятнадцать';
            } //switch
        } //level 1
        else if ($Level == 2) {
            switch ($N) {
                case 0:
                    return '';
                case 1:
                    return 'десять';
                case 2:
                    return 'двадцать';
                case 3:
                    return 'тридцать';
                case 4:
                    return 'сорок';
                case 5:
                    return 'пятьдесят';
                case 6:
                    return 'шестьдесят';
                case 7:
                    return 'семьдесят';
                case 8:
                    return 'восемьдесят';
                case 9:
                    return 'девяносто';
            }
        } //level 2
        else if ($Level == 3) {
            switch ($N) {
                case 0:
                    return '';
                case 1:
                    return 'сто';
                case 2:
                    return 'двести';
                case 3:
                    return 'триста';
                case 4:
                    return 'четыреста';
                case 5:
                    return 'пятьсот';
                case 6:
                    return 'шестьсот';
                case 7:
                    return 'семьсот';
                case 8:
                    return 'восемьсот';
                case 9:
                    return 'девятьсот';
            }
        } //level 3
    }
    
    function Gender()
    {
        return $this->UnitWord['Gender'];
    }
    
    function UnitWordInRightForm()
    {
        $result = $this->UnitWord['Base'];
        $slevel = $this->Levels(1);
        if ($slevel == 1) {
            return $result . $this->UnitWord['End1'];
        }
        if (($slevel == 0) or (($slevel >= 5) and ($slevel <= 19))) {
            return $result . $this->UnitWord['End5'];
        }
        if (($slevel >= 2) or ($slevel <= 4)) {
            return $result . $this->UnitWord['End2'];
        }
    }
    
    private function Convert()
    {
        if ($this->FNumber == 0) {
            return '';
        }
        $result = '';
        for ($i = 3; $i >= 1; $i--) {
            $s = $this->GetNumberInWord($this->Levels($i), $i);
            if ($s != "") {
                $result .= $s . ' ';
            }
        } //for
        return $result . $this->UnitWordInRightForm() . ' ';
    }
    
    function ConvertToText($AUnit = array('Gender' => 'GENNEUTER', 'Base' => '', 'End1' => '', 'End2' => '', 'End5' => ''), $ANumber)
    {
        $this->UnitWord = $AUnit;
        $this->SetNumber($ANumber);
        return $this->Convert();
    }
    
} //end of class number
//-----------------------------------------------------------------------

?>