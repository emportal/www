<?php
Yii::import('zii.widgets.CPortlet');

class UserLogin extends CPortlet
{
    public $id="userLoginForm";
    public $title=null;
    public $visible=false;
    public $onlyForm=false;

	public function init()
	{
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}
	public function run()
	{
		$this->renderContent();
		$content=ob_get_clean();
		echo $content;
	}

    protected function renderContent()
    {
    	if($this->visible) {
	        $form = new LoginForm;
//	        if(isset($_POST['LoginForm']))
//	        {
//	            $form->attributes=$_POST['LoginForm'];
//	            if(!$form->validate() && $form->login())
//	                $this->controller->redirect(array('login'));
//	        }
			if($this->onlyForm) {
	        	$this->render('userLoginForm',array('model'=>$form, 'id'=>$this->id));
			} else {
	        	$this->render('userLogin',array('model'=>$form, 'id'=>$this->id));
			}
    	}
    }
}
?>