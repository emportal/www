<?php

/**
 * Description of ActiveRecord
 *
 *
 */
class AgrActiveRecord extends ActiveRecord {
	
	
	public function beforeSave() {
		
		if (parent::beforeSave()) {
			$this->syncThis = false;
			
			return true;
		} else
			return false;
	}

	public function beforeDelete() {
		
		if(parent::beforeDelete()) {
			$this->syncThis = false;
			
			return true;
		} else
			return false;
	}

}

?>
