<?php

//Yii::import('zii.widgets.CWidget');

class NewCompany extends CWidget {
	
	public $params = [
		//'userModel' => $userModel,
		//'companyModel' => $companyModel,
		//'addressModel' => $addressModel,
	];

	public function init()
	{
		//ob_start();
		//ob_implicit_flush(false);
		//ob_clean();
	}

	public function run()
	{
		$userModel = (Yii::app()->user->isGuest) ? new User('register') : Yii::app()->user->model;
		$companyModel = new Company('self_register');
		$addressModel = new Address('self_register');
		
		$companyModel->attributes = Yii::app()->request->getParam(get_class($companyModel));
		$addressModel->attributes = Yii::app()->request->getParam(get_class($addressModel));
		
		//если новый юзер, привязываем атрибуты
		if ($userModel->isNewRecord)
		{
			$preDefinedUserData = [
				'sexId' => Sex::getDefaultValue(),
				'status' => 1,
				'phoneActivationStatus' => 1,
				'phoneActivationDate' => date('Y-m-d H:i:s'),// '2014-09-25 14:15:01',
			];
			$userModelData = (array)Yii::app()->request->getParam(get_class($userModel));
			$userModelData['telefon'] = LoginForm::convertPhoneStringToStandardForm($userModelData['telefon']);
			$userModel->attributes = array_merge($userModelData, $preDefinedUserData);
			$presavedPass = $userModelData['password'];
			
			//var_dump([
			//	'$userModel->attributes' => $userModel->attributes,
			//	'$userModel->validate()' => $userModel->validate(),
			//	'errors' => $userModel->getErrors(),
			//]);
		}
		
		if (!empty($_POST) && (!$userModel->isNewRecord OR $userModel->validate()) && $companyModel->validate() && $addressModel->validate())
		{
			//ob_start();
			$companyModel->nameRUS = $companyModel->name;
			$companyModel->nameENG = 'isViaSelfRegister';
			
			if ($companyModel->save())
			{
				//var_dump(['saved new company!']);
				
				if ($userModel->isNewRecord && $userModel->save())
					var_dump(['saved new user!']);
				
				$addressModel->ownerId = $companyModel->id;
				$addressModel->isActive = 0;
				$addressModel->name = $addressModel->street . ', ' . $addressModel->houseNumber;
				
				$companyModel->denormCitySubdomain = City::model()->findByPk($addressModel->cityId)->subdomain;
				$companyModel->addressId = $addressModel->id;
				$companyModel->update();
				
				if ($addressModel->save())
				{
					//var_dump(['saved new address!']);
					
					//создаем договор для адреса
					$companyContractModel = new CompanyContract();
					$indexNum = (Yii::app()->db->createCommand('SELECT MAX(indexNum) FROM `' . CompanyContract::model()->tableName() . '`')->queryScalar() + 1);
					$contractNumber = 'КО-1/' . $indexNum;
					$companyContractModel->attributes = [
						'contractNumber' => $contractNumber,
						'indexNum' => $indexNum,
						'name' => $companyModel->name,
						'periodOfValidity' => '2019' . date('-m-d H:i:s'),
						'companyId' => $companyModel->id,
					];
					if ($companyContractModel->save())
						var_dump(['saved new contract!']);
					
					//создаем аккаунт пользователя клиники
					$userMedicalModel = new UserMedical();
					$userMedicalModel->attributes = [
						'addressId' => $addressModel->id,
						'companyId' => $companyModel->id,
						'companyContractId' => $companyContractModel->id,
						'status' => '0',
						'agreement' => '1',
						'agreementService' => '1',
						'agreementNew' => '0',
						'name' => $addressModel->link,
						'createdDate' => new CDbExpression("NOW()"),
						'email' => $userModel->email,
						'phoneForAlert' => $userModel->telefon,
						//email ?
					];
					if ($userMedicalModel->save())
						var_dump(['saved new userMedical!']);
					//var_dump([
					//	'$userMedicalModel->attributes' => $userMedicalModel->attributes,
					//	'id' => $userMedicalModel->id,
					//	'errors' => $userMedicalModel->getErrors(),
					//]);
					
					//привязываем юзера как владельца ЛКК через добавление соотв. права
					$rightModel = new Right('self_register');
					$rightModel->attributes = [
						'userId' => $userModel->id,
						'zRightId' => RightType::IS_OWNER_OF_ADDRESS,
						'value' => $addressModel->id,
					];
					if ($rightModel->save())
						var_dump(['saved new rightModel!']);
					
					//надо также привязать к свежесозданной клинике тариф по умолчанию
					$salesContractModel = new SalesContract();
					$salesContractModel->attributes = [
						'addressId' => $addressModel->id,
						'salesContractTypeId' => SalesContractType::model()->findByAttributes(['isFixed'=>'1', 'price'=>'990'])->id,
					];
					if ($salesContractModel->save())
						var_dump(['saved new salesContractModel!']);
					
					//выбрать менеджера из тех, к кому можно привязать новую компанию
					$rights = Right::model()->findAllByAttributes([
						'zRightId' => RightType::CAN_RECEIVE_SELF_REGISTERED_COMPANIES
					]);
					$selectedManager = $rights[array_rand($rights)]->user;
					
					//и привязать этого менеджера к новой компании
					$managerRelationModel = new ManagerRelation();
					$managerRelationModel->attributes = [
						'userId' => $selectedManager->id,
						'companyId' => $companyModel->id,
					];
					if ($managerRelationModel->save())
						var_dump(['saved new managerRelationModel!']);
					
					//также надо завести соотв. служебную информацию об ЛПР для карточки клиники в модуле newAdmin
					$companySalesInfo = new CompanySalesInfo();
					$companySalesInfo->attributes = [
						'companyId' => $companyModel->id,
						'lprName' => $userModel->name,
						'lprPosition' => Yii::app()->request->getParam('lprPosition'),
						'lprPhone' => $userModel->telefon,
						'email' => $userModel->email,
						'website' => Yii::app()->request->getParam('website'),
					];
					if ($companySalesInfo->save())
						var_dump(['saved new companySalesInfo!']);
					
					if ($presavedPass)
					{
						$lf = new LoginForm();
						$lf->attributes = [
							'username' => $userModel->email,
							'password' => $presavedPass,
						];
						$lf->login();
					}
					Yii::app()->controller->redirect(array('/user/administrate')); //rf
				}
			}
		}
		
		$this->params = [
			'userModel' => $userModel,
			'companyModel' => $companyModel,
			'addressModel' => $addressModel,
		];
		
		$this->renderContent();
		//$content = ob_get_clean();
		//echo $content;
	}

	protected function renderContent()
	{
		$this->render('newCompany', $this->params);
	}

}

?>