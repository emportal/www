<?php
Yii::import('zii.widgets.CPortlet');

class UserCabinetMenu extends CPortlet
{
	public $visible=false;

	protected function renderContent()
	{
		if($this->visible) {
			$this->render('userCabinetMenu');
		}
	}
}
?>