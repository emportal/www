<?php

class NewsReferenceBlock extends CWidget {
	
	public $newsReferences = null;

	public function init() {
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}

	public function run() {
		$this->renderContent();
		$content=ob_get_clean();
		echo $content;
	}

    protected function renderContent()
    {
    	$this->render('newsReferenceBlock',array('newsReferences'=>$this->newsReferences));
    }

}

?>