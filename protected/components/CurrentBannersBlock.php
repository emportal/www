<?php

class CurrentBannersBlock extends CWidget
{

	/**
	 * @var string
	 */
	public $type = '';

	public function init()
	{
		
	}

	public function run()
	{
		$criteria = new CDbCriteria();
		$criteria->order = "RAND()";
		$criteria->limit = 1;

		$currentBanner = CurrentBanners::model()->active()->find($criteria);

		$count = count($currentBanner);
		#var_dump($currentPriority[0]->action->attributes);
		$imagePath = Yii::getPathOfAlias("webroot") . '/uploads/banners/' . $currentBanner->name;
		$imageUrl = '/uploads/banners/' . $currentBanner->name;
		if (!empty($count) && is_file($imagePath))
		{
			$this->render('currentBannersBlock', [
				'model' => $currentBanner,
				'imageUrl' => $imageUrl,
				'type' => $this->type
			]);
		}
	}

}

?>