<? /* @var $news News[] */?>
<div class="news_block">
	<div class="title-blog">СЕКРЕТЫ ЗДОРОВОГО ОБРАЗА ЖИЗНИ</div>
	<div class="news_block_content">
		<?php
		for($i = 0; $i <= 3; $i++) {
			if(!isset($news[$i])) break;
			?>
			<div class="blog-annonce" style=" <?=(($i+1)%2 == 0)?'margin-right: 0px':''?>">
				<a href="<?=$news[$i]->getUrl(true,true)?>" class="link-blog">
					<div class="img-bg" style="background: url('<?=$news[$i]->getMainImage()?>'); background-position: center; background-size: cover;"></div>
					<div class="background-opc"></div>
					<div class="title-current-annonce">
						<span><?=mb_strtoupper($news[$i]->title) ?></span>
					</div>

				</a>
			</div>
			<?php
		}
		?>
		<div style="clear: both"></div>
	</div>
	<?= CHtml::link('Читать больше', ((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http').'://blog.emportal.ru', array('class'=>'long-white-button button-blog')) ?>
</div>
