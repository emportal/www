<div class="log-in" id="<?= (empty($id)) ? 'userLoginForm' : $id ?>">
	<div class="auth">
		<div class="auth-head"><span class="icon_key"></span>Войти</div>
			<div class="auth-field">
				<div class="social-head">Войти с помощью:</div>
				<?php Yii::app()->eauth->renderWidget(); ?>			
			</div>
			<div style="margin: 15px 0px 7px 0px; height: 1px; background-color: rgb(211, 211, 211); text-align: center">
				<span style="background-color: white; position: relative; top: -9px;line-height: 10px; letter-spacing: 1px; padding: 0px 8px 0px 8px; color: rgb(153, 153, 153); font-size: 10px; font-weight: bold;">
					ИЛИ
				</span>
			</div>
		<?php
		$schema = Yii::app()->params['devMode'] ? '' : 'https';
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'login-form',
			'action' => Yii::app()->createAbsoluteUrl('site/login', [], $schema),
			'enableAjaxValidation' => true,
		));
		?>
		<fieldset>
			<div class="auth-field">
				<label>Телефон или e-mail</label>
				<div style="display: inline-block; width: 100%;margin: 3px 0 1px 0;">
					<?php echo $form->textField($model, 'username', array("class" => "loginForm-text", 'tabindex' => 1)); ?>	
				</div>			
			</div>
			<div class="auth-field">
				<label>Пароль</label>
				<div class="recovery inline">
					<a href="/recovery" title="Забыли пароль?">Забыли пароль?</a>
				</div>
				<div class="" style="display: inline-block; width: 100%;border: 1px solid rgb(220,220,220);border-radius: 3px;box-sizing: border-box;margin: 3px 0 1px 0;">
					<?php echo $form->passwordField($model, 'password', array("class" => "loginForm-text user_password_block", 'tabindex' => 2)); ?>	
					<div class="user_remember_block">
						<span class="niceCheck" id="showPassword">
							<input type="checkbox" value="1">
						</span>
						<label class="rememberLabel" for="showPassword">Показать</label>
					</div>
				</div>
			</div>
			<div class="remember-row">
				<div class="remember inline">
					<span class="niceCheck" value="1" tabindex="3" name="<?=get_class($model)?>[rememberMe]" id="<?= (empty($id)) ? 'userLoginForm' : $id ?>_LoginForm_rememberMe">
						<input type="checkbox">
					</span>
					<label class="rememberLabel" for="<?= (empty($id)) ? 'userLoginForm' : $id ?>_LoginForm_rememberMe">Запомнить</label>
				</div>
			</div>
			<div class="auth-field">
				<?php echo CHtml::submitButton('Войти', array("class" => "btn btn-green", 'tabindex' => 4)); ?>
			</div>
		</fieldset>
		<?php $this->endWidget(); ?>
		<div  class="show-in-responsive">
			<div style="margin: 15px 0px 7px 0px; height: 1px; background-color: rgb(211, 211, 211); text-align: center"></div>
			<div class="auth-field" style="padding: 10px 0px 15px 0px; text-align: center; font-size: 14px; font-weight: 500;">
				Регистрация нового пользователя
			</div>
			<div class="auth-field">
				<a class="btn btn-green" href="/register" style="width: 100%;padding: 10px;font-size: 18px;">
					Зарегистрироваться
				</a>
			</div>
		</div>
	</div><!-- /auth -->
</div>