<? /* @var $actions Action[] */ 
 Yii::app()->clientScript->registerScript('actions', '
	 $(".agent").click(function(e) {
		var href = $(this).attr("data-href");
		location.href=href;
		e.stopPropagation();
		return false;
	 });
	 $(".companyActivity").click(function(e) {
		var href = $(this).attr("data-href");
		location.href=href;
		e.stopPropagation();
		return false;
	 });
		 ');


?>

<div class="stock">
	<?= CHtml::link('смотреть все акции', array('/actions'), array('class'=>'all-link')) ?>
	<h2>Акции</h2>
	<ul class="stock-list">
		<?php if($currentPriority): ?>
			<?php foreach ($currentPriority as $act): $action = $act->action; ?>
			<li>
				<a href="<?= $this->owner->createUrl('/actions/view', array('link'=>$action->link))?>" class="stock-item">
					<div class="stock-item-inner">
						<i class="<?php if($action->type==="0") {
							echo 'ico-free';
						} else {
							echo 'ico-discount';
						}?>"></i>
						<div class="stock-item-title">
							<h3><?= $action->NameCut?></h3>
							<strong>
							<?php //if($action->unlimited):?>
								<?php if($action->priceOld && $action->priceNew):?>
								Старая цена <?php echo $action->priceOld?>,
								Цена по акции <?php echo $action->priceNew?>
								<?php elseif($action->type==="0"): ?>
								  Бесплатно
								<?php elseif($action->type==="2"): ?>
								  Подарок - <?=$action->present?>
								<?php else:?>
								  Скидка - <?=$action->discount?>%
								<?php endif;?>
							<?php /*else:?>
								СРОК АКЦИИ:
								<?= Yii::app()->dateFormatter->formatDateTime(strtotime($action->start), 'short', null) ?>
								-
								<?= Yii::app()->dateFormatter->formatDateTime(strtotime($action->end), 'short', null) ?>
							<?php/ endif;*/?>
							</strong><br/>
							<div class="action_links">												
								<div class="agent" data-href="<?=Yii::app()->createUrl('clinic/view',array('link'=>$action->address->link))?>"><?=$action->company->name?></div>							
								<div class="companyActivity" data-href="<?= Yii::app()->createUrl('handbook/CompanyActivites',array('link'=>$action->companyActivite->link))?>"><?=$action->companyActivite->name?></div>
							</div>						
						</div><!-- /stock-item-title -->
						<p></p>
					</div><!-- /stock-item-inner -->
				</a>
			</li>
			<? endforeach; ?>
		<?php endif; ?>
		<?php if($actions): ?>
			<?php foreach ($actions as $action): ?>
			<li>
				<a href="<?= $this->owner->createUrl('/actions/view', array('link'=>$action->link))?>" class="stock-item">
					<div class="stock-item-inner">
						<i class="<?php if($action->type==="0") {
							echo 'ico-free';
						} else {
							echo 'ico-discount';
						}?>"></i>
						<div class="stock-item-title">
							<h3><?= $action->NameCut?></h3>
							<strong>
							<?php //if($action->unlimited):?>
								<?php if($action->priceOld && $action->priceNew):?>
								Старая цена <?php echo $action->priceOld?>,
								Цена по акции <?php echo $action->priceNew?>
								<?php elseif($action->type==="0"): ?>
								  Бесплатно
								<?php elseif($action->type==="2"): ?>
								  Подарок - <?=$action->present?>
								<?php else:?>
								  Скидка - <?=$action->discount?>%
								<?php endif;?>
							<?php /*else:?>
								СРОК АКЦИИ:
								<?= Yii::app()->dateFormatter->formatDateTime(strtotime($action->start), 'short', null) ?>
								-
								<?= Yii::app()->dateFormatter->formatDateTime(strtotime($action->end), 'short', null) ?>
							<?php/ endif;*/?>
							</strong><br/>
							<div class="action_links">												
								<div class="agent" data-href="<?=Yii::app()->createUrl('clinic/view',array('link'=>$action->address->link))?>"><?=$action->company->name?></div>							
								<div class="companyActivity" data-href="<?= Yii::app()->createUrl('handbook/CompanyActivites',array('link'=>$action->companyActivite->link))?>"><?=$action->companyActivite->name?></div>
							</div>						
						</div><!-- /stock-item-title -->
						<p></p>
					</div><!-- /stock-item-inner -->
				</a>
			</li>
			<? endforeach; ?>
		<?php endif; ?>
	</ul><!-- /stock-list -->
</div><!-- /stock -->