<style>
<?php include($fullAppPath . 'appForm.css'); ?>
</style>
<?php include($fullAppPath . 'appForm.html'); ?>
<script>
empAppFormOptions = {
	'companyUrl': '<?= $companyLinkUrl ?>',
	'addressUrl': '<?= $addressLinkUrl ?>',
	<?php
	if (!AppForm::isLocalVersion())
		echo "'domain': 'emportal.ru',";
	?>
	'appType': '<?= $appType ?>',
};
<?php include($fullAppPath . 'appForm.js'); ?>
</script>