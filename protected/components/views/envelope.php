<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
</head>
<style>
@font-face{
	font-family:"PostIndexLight";
	src:url("/shared/font/postindexlightbold.ttf");
}
body {
	width: 229mm; /* c5 = 114*229 */
    height: 160mm;
	margin: auto;
    padding: 0;
	font: 24px Arial;
}
.envelope-table {
    width: 100%;
    height: 100%;
    border: none;
}
.envelope-table tr>td {
    width: 50%;
    border: none;
}
.v-align-bottom td {
    vertical-align: bottom;
}
.font-big {
    font-size: 100px;
    font-family: PostIndexLight;
}
.postalCode {
    display: table;
    border: 1px solid;
    width: 50%;
    height: auto;
}
.postalCode>div {
    display: table-cell;
    vertical-align: top;
    text-align: center;
}
.postalCode>div>div {
    font-size: 11px;
    font-style: italic;
}
.postalMark {
    float: right;
    width: 18.5mm;
    height: 26mm;
    margin: 10mm 20mm 0 0;
    border: 1px solid gray;
}
</style>
<body>
<table class="envelope-table">
	<tr>
        <td colspan="2">
            <img src="/images/newLayouts/LOGO-MAIN-DARK.png" style="float: left;">
        </td>
	</tr>
    <tr>
        <td>
            <div>
            	<?= $legalData->siteUrl . '<br>' .  $legalData->name . '<br>' .  $legalData->address ?>
            </div>
            <div>
	            <div class="postalCode" style="float: right; margin-top: 0.1cm;">
	            	<div>
	            		<div>Индекс места отправления</div>
	            		<?= $legalData->postalCode ?>
	            	</div>
	            </div>
            </div>
        </td>
        <td style="vertical-align: top;">
            <div class="postalMark"></div>
        </td>
	</tr>
    <tr class="v-align-bottom">
        <td class="font-big">$<?= $model->postalCode ?></td>
        <td style="padding-left: 5%;">
            Кому: <?= $model->toWho ?> <?=$model->toWho != $model->toWhereName ? '(компания '.$model->toWhereName.')' : '';?>
            <br>
            Куда: <?= $model->toWhere['street']. (!empty($model->toWhere['houseNumber']) ? ", ".$model->toWhere['houseNumber'] : "") ?>
            <br>
            <?= $model->toWhere['cityName'] ?>
            <div class="postalCode"><div><div>Индекс места назначения</div><?= $model->postalCode ?></div></div>
            <!-- <h2>Получатель</h2>
            <table>
                <tr>
                    <td>
                        Кому <span class="dotted-span"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Улица <span class="dotted-span"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        дом: <span class="dotted-span"></span>
                        корп.: <span class="dotted-span"></span>
                        стр.: <span class="dotted-span"></span>
                        кв.: <span class="dotted-span"></span>
                    </td>
                </tr>
                <tr>
                    <span class="dotted-span">full width</span>
                </tr>
                <tr>
                    <td>
                        Населенный пункт <span class="dotted-span"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Район <span class="dotted-span"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Республика, край, область <span class="dotted-span"></span>
                    </td>
                </tr>
            </table> -->
        </td>
	</tr>
</table>
</body>
</html>