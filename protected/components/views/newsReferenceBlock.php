<?php if(is_array($newsReferences) && count($newsReferences)>0): ?>
<div class="information" id="newsReference">
	<a href="javascript:void(0);" class="roll-more opened"><span class="information_header"><span class="icon-news-micro"></span>Новости и статьи</span></a>
	<div class="more">
		<?php foreach ($newsReferences as $index=>$newsReference): ?>
			<?php if($index === 2): ?>
			<div class="information">
				<a href="javascript:void(0);" class="roll-more hiding-link"><span>Ознакомиться со всеми новостями</span></a>
				<div class="more" style="display: none;">
			<?php endif; ?>
					<div class="newsReference">
						<div class="date-time"><?= RuDate::post($newsReference->news->datePublications) ?></div>
						<?= CHtml::link($newsReference->news->title, array('/news/view', 'link' => $newsReference->news->link), array('class'=>'article_title')) ?>
						<p class="article_text"><?= $newsReference->news->preview ?></p>
					</div>
			<?php if(($index+1) === count($newsReferences)): ?>
				</div>
			</div>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>
<?php endif; ?>