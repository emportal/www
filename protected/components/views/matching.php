<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Акт об оказании услуг/Акт сверки</title>
</head>
<style>
body {
	width: 460px;
	margin: auto;
	font-family: "Times New Roman";
	font-size: 12px;
}
p {
	min-height: 1em;
	text-indent: 25px;
	text-align: left;
	margin: 1px 0 1px 0;
}
.report-table {
	border-collapse: collapse;
	margin: 3px 0 3px 0;
}
.report-col {
	border: 1px solid black;
}
.report-row {
	border: 1px solid black;
	padding: 1px 3px 1px 2px;
}
</style>
<body>
<table style="border: 0px solid white;width: 100%;">
	<tr>
	<td style="width: 50%;"> </td>
	<td>
		<b>
				к Договору-оферте на оказание услуг на
				сайте «Единый медицинский портал»
		</b>
	</td>
	</tr>
</table>
<p> </p>
<p style="text-align: center;">
	<b>
			Акт об оказании услуг № <?= $model->number ?>
	<br>	по Договору-Оферте № <?= CHtml::encode($model->contractNumber) ?><?= !empty($model->contractCreateDate) ? " от " . date("d.m.Y",strtotime($model->contractCreateDate)) : "" ?>
	</b>
</p>
<p> </p>
<table style="border: 0px solid white;width: 100%;">
	<tbody>
		<tr>
			<td style="text-align: left;">
				г. Санкт-Петербург
			</td>
			<td style="text-align: right;">
				<?= $model->issueDate ?> г.
			</td>
		</tr>
	</tbody>
</table>
<p style="text-align: left;">
			Наименование клиники и ее адрес: <?= $model->client->name ?>
</p>
<p style="text-align: left;">
			Период оказания услуг: <?= $model->period ?>
</p>
<p>
			<?= $legalData->nameFull ?>, ИНН <?= $legalData->inn ?>, КПП
            <?= $legalData->kpp ?>, адрес <?= $legalData->postalCode . ', ' . $legalData->address ?>, 
			именуемое в дальнейшем «Исполнитель», в лице Генерального директора  <?= $legalData->ceoNameRoditelny ?>, действующего на основании Устава, составил,
	<br>	а <?= $model->client->legalData->nameFull ?>,
			ИНН <?= $model->client->legalData->inn ?>,
			адрес <?= $model->client->legalData->address ?>, 
			именуемое в дальнейшем «Клиент», в лице 
	<br>	__________________________________________________________________________________________________________________________ ,
	<br>	действующего на основании
	<br>	________________________________________________________________________________________________________________ , утвердил
	<br>	настоящий Акт о том, что Исполнитель надлежащим образом выполнил обязательства по
	<br>	Договору-оферте в соответствии с нижеприведенными данными:
</p>
<table class="report-table">
	<tbody>
		<tr class="report-col" style="font-size: 10px;">
			<td class="report-row">№</td>
			<td class="report-row">Фамилия Имя Отчество пациента</td>
			<td class="report-row">Телефон</td>
			<td class="report-row">Специалист/услуга</td>
			<td class="report-row">Дата и время визита</td>
			<td class="report-row">Стоимость приема/услуги, рублей</td>
			<td class="report-row">Сумма к оплате, рублей</td>
		</tr>
<?php
$index = 0;
foreach ($model->products as $product) :
$index++;
?>
		<tr class="report-col">
			<td class="report-row"><?= CHtml::encode($index) ?></td>
			<td class="report-row"><?= CHtml::encode($product->name) ?></td>
			<td class="report-row"><?= CHtml::encode($product->phone) ?></td>
			<td class="report-row"><?= CHtml::encode( (!$product->visitTypeSubject ? $product->serviceTypeText : $product->visitTypeSubject) ) ?></td>
			<td class="report-row"><?= date('d.m.Y H:i', strtotime($product->plannedTime)) ?></td>
			<td class="report-row"><?= $product->totalPrice ?></td>
			<td class="report-row"><?= $product->empComission ?></td>
		</tr>
<?php endforeach; ?>
		<tr>
			<td colspan=6 class="report-row" style="text-align: right;">Без налога (НДС):</td>
			<td class="report-row"> - </td>
		</tr>
		<tr>
			<td colspan=6 class="report-row" style="text-align: right;">Итого к оплате за период:</td>
			<td class="report-row"><?= $model->totalSale ?></td>
		</tr>
	</tbody>
</table>
<p>
			1. Обязательства исполнителя по договору за <?= MyTools::getRussianMonthsNames()[intval(explode("-", $model->reportMonth)[1])-1] ?> <?= explode("-", $model->reportMonth)[0] ?> года выполнены в объеме <?= $model->totalSaleText ?>. Услуги  оказаны надлежащим образом. Клиент по объему и качеству
			выполненных Исполнителем услуг претензий не имеет.
</p>
<p>
			2. Настоящий акт подписан в 2 (двух) подлинных экземплярах на русском языке по
			одному для каждой из сторон.
</p>
<table style="width: 100%; border: 0px solid white;">
	<tr>
		<td style="width: 50%; padding: 25px 0px 0px 25px; vertical-align: top;">
					От имени клиента
			<br>	__________________________________
			<br>	__________________________________
			<br>	
			<br>	МП
		</td>
		<td style="width: 50%; padding: 25px 0px 0px 0px; vertical-align: top;">
					От имени исполнителя
			<br>	Генеральный директор <?= $legalData->name ?>
			<br>	(<?= $legalData->ceoName ?>)
            <?php if ($model->useLegalSigns && $withFaksimile): ?>
			<br>	
                <img style="border:0px solid white; height: 22mm; margin: 5mm; vertical-align: top;" src="https://emportal.ru/images/scan-signature-hi-res.png">
                <img style="border:0px solid white; height: 41mm; margin: 5mm; float: right;" src="https://emportal.ru/images/scan-print-hi-res.png">
            <?php else: ?>
			<br>	<br><br><br><br>
            <?php endif; ?>
		</td>
	</tr>
</table>
</body>
</html>