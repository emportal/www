<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Претензия (от emportal.ru)</title>
</head>
<style>
body {
	width: 460px;
	margin: auto;
	font-family: Times New Roman;
	font-size: 12px;
}
h1,h2,h3 {
	text-align: center;
    margin: 5px 0;
}
p {
	min-height: 1em;
	text-indent: 25px;
	text-align: justify;
	margin: 10px 0;
}
.report-table {
	border-collapse: collapse;
	margin: 3px 0 3px 0;
}
.report-col {
	border: 1px solid black;
}
.report-row {
	border: 1px solid black;
	padding: 1px 3px 1px 2px;
}
</style>
<body>
<table style="border: 0px solid white;width: 100%;">
	<tr>
		<td style="width: 60%;"> </td>
		<td>
			<p>
				Генеральному директору
				<br><?= $model->legalName ?>
				<br>
				<br>
				<br>от Генерального директора
				<br>ООО «Первая информационная компания»
				<br>Милославского И.С.
			</p>
		</td>
	</tr>
</table>
	<h2>Претензия</h2>
	<h3>Уважаемый клиент!</h3>
<p>
	<?= $model->contractCreateDate ?> между <?= $model->legalName ?> (Клиент) и ООО «Первая информационная компания» (Исполнитель),
	путем прохождения Клиентом соответствующей регистрации на сайте «Единый медицинский портал» (https://emportal.ru),
	был заключен договор оферты №<?= $model->contractNumber ?> об оказании услуг по привлечению пациентов в клинику Клиента
	путем размещения информации о Клиенте на указанном сайте (далее – Договор).
</p>
<p>
	Согласно положениям Договора, Клиент обязуется добросовестно оплачивать предоставленные Исполнителем услуги в порядке
	и на условиях разделов 4 и 5 указанного Договора. Кроме того, в Договоре сторонами была согласована процедура сверки объема
	и приемки Клиентом оказанных Исполнителем услуг. 
</p>
<p>
	Однако, на дату написания настоящей Претензии (<?= $model->currentDate ?>) Вами не были предприняты действия
	по выполнению своих обязанностей по проведению оплаты оказанных услуг согласно Договора. 
</p>
<p>
	В связи с этим, убедительно просим Вас произвести указанные действия в срок до <?= $model->deadLine ?>
</p>
<p>
	Напоминаем Вам, что главой 7 Договора предусмотрены определенные меры ответственности Клиента
	в случае нарушения им своих договорных обязательств, а именно право Исполнителя на требование об уплате Клиентом пени
	в размере 0,1% от суммы задолженности за каждый календарный день просрочки.
</p>
<p>
	Также нарушение Клиентом условий Договора влечет за собой приостановление оказания Клиенту услуг,
	а в отдельных случаях – отказ Исполнителя от исполнения своих обязательств по Договору в одностороннем внесудебном порядке.
	При этом приостановление оказания услуг по вине Клиента не освобождает его от оплаты услуг, оказание которых предусмотрено Договором.
</p>
<p>
	<b>В связи с изложенным выше и во исполнение положений Договора №<?= $model->contractNumber ?> от <?= $model->contractCreateDate ?>.
	между <?= $model->legalName ?> (Клиент) и ООО «Первая информационная компания» (Исполнитель),</b>
	извещаем Вас о необходимости прекратить нарушения обязанности Клиента по оплате оказанных надлежащим образом
	и принятых Клиентом без каких-либо замечаний услуг Исполнителя и оплатить Исполнителю платеж
	в размере <?= $model->debtSum ?> (<?= (new NumberAnaliz)->CurrencyToText($model->debtSum, NULL) ?>) руб. 00 коп.
</p>
<p>
	В случае неисполнения Вами данного законного требования, Исполнитель, в соответствии с п. 11.4 Договора,
	оставляет за собой право на обращение за защитой своих интересов в суд.
</p>
<table style="border: 0px solid white;width: 100%;">
	<tr>
		<td style="width: 50%;vertical-align: bottom;">
			<p>
			</p>
		</td>
		<td style="width: 50%;vertical-align: bottom;">
			<p>
				<img style="border:0px solid white; margin-left:5mm; height: 22mm; vertical-align: top;" src="https://emportal.ru/images/scan-signature-hi-res.png">
			</p>
		</td>
	</tr>
	<tr>
		<td style="width: 50%; vertical-align: top;">
			<p>
				<b>Генеральный директор</b> 
			</p>
		</td>
		<td style="width: 50%;vertical-align: top;">
			<p>
				<b>____________/ Милославский И.С.</b>
			</p>
		</td>
	</tr>
	<tr>
		<td style="width: 50%;vertical-align: top;">
			<p> </p>
		</td>
		<td style="width: 50%;vertical-align: top;">
			<img style="border:0px solid white; height: 41mm; margin: 5mm; float: right;" src="https://emportal.ru/images/scan-print-hi-res.png">
		</td>
	</tr>
</table>
</body>
</html>