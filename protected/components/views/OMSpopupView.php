<?php Yii::app()->clientScript->registerPackage('jquery.maskedinput'); ?>
<style>
<!--
#OMSpopupView input.btn-green {
	font-size: 18px;
	width: 100%;
	min-height: 54px !important;
	margin: 10px auto 0px auto;
}

.OMSForm-text {
	box-sizing: border-box;
	float: none;
	height: 40px;
	width: 100%;
	display: -moz-inline-box;
	display: inline-block;
	word-spacing: normal;
	vertical-align: top;
	position: relative;
	font-size: 14px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	border: 1px solid rgb(220,220,220);
	background: white;
	padding: 0px 10px;
	color: rgb(51,51,51) !important;
}

.popup_icon_user {
	background: url(/images/newLayouts/icon_user.png?v=0001) no-repeat 0px 0px transparent;
	background-size: 32px;
	display: inline-block;
	vertical-align: bottom;
	width: 32px;
	height: 32px;
}
-->
</style>
<div style="display:none;">
	<div id="OMSpopupView">
		<form>
			<div class="auth-head"><span class="popup_icon_user"></span>ОМС данные</div>
			<div class="auth-field">
				<label>Фамилия</label>
				<div style="display: inline-block; width: 100%;margin: 3px 0 1px 0;">
					<input class="OMSForm-text" tabindex="1" name="OMSForm[surName]" id="OMSForm_surName" type="text">
				</div>
			</div>
			<div class="auth-field">
				<label>Имя</label>
				<div style="display: inline-block; width: 100%;margin: 3px 0 1px 0;">
					<input class="OMSForm-text" tabindex="2" name="OMSForm[firstName]" id="OMSForm_firstName" type="text">
				</div>
			</div>
			<div class="auth-field">
				<label>Отчество</label>
				<div style="display: inline-block; width: 100%;margin: 3px 0 1px 0;">
					<input class="OMSForm-text" tabindex="3" name="OMSForm[fatherName]" id="OMSForm_fatherName" type="text">
				</div>
			</div>
			<div class="auth-field">
				<label>Дата рождения</label>
				<div style="display: inline-block; width: 100%;margin: 3px 0 1px 0;">
					<input class="OMSForm-text" tabindex="4" name="OMSForm[birthday]" id="OMSForm_birthday" type="text">
				</div>
			</div>
			<div class="auth-field">
				<label>Серия ОМС Полиса</label>
				<div style="display: inline-block; width: 100%;margin: 3px 0 1px 0;">
					<input class="OMSForm-text" tabindex="5" name="OMSForm[OMSSeries]" id="OMSForm_omsSeries" type="text" placeholder="Для полисов старого образца">
				</div>
			</div>
			<div class="auth-field">
				<label>Номер ОМС Полиса</label>
				<div style="display: inline-block; width: 100%;margin: 3px 0 1px 0;">
					<input class="OMSForm-text" tabindex="6" name="OMSForm[OMSNumber]" id="OMSForm_omsNumber" type="text">
				</div>
			</div>
		</form>
		<div class="auth-field">
			<input class="btn btn-green" tabindex="7" type="submit" id="OMSForm_submit" value="Сохранить">
		</div>
	</div>
</div>
<script type="text/javascript">
	var OMSpopupView = $( "#OMSpopupView" ).clone();
	$( "#OMSpopupView" ).remove();
	function showOMSpopupView() {
		$.fancybox({
			id: "OMSpopup_fancyView",
			content: OMSpopupView,
		});
		OMSpopupView.find("#OMSForm_birthday").datetimepicker({
			changeMonth: true,
			changeYear: true,
			lang: "ru",
			timepicker: false,
			format: "d.m.Y",
			closeOnDateSelect: true,
			minDate: "1900/01/01",
			maxDate: "0",
			defaultDate:"1989/12/31",
			firstDay: 1,
		});
		OMSpopupView.find("#OMSForm_birthday").mask("99.99.9999", {placeholder: "_"});
		OMSpopupView.find("#OMSForm_birthday").attr("placeholder", "31.12.1989");
		OMSpopupView.find('form input').each(function() {
			var input = $(this);
			input.val($.cookie(input.attr('id')));
		});
		OMSpopupView.find("#OMSForm_birthday").val(OMSpopupView.find("#OMSForm_birthday").val().split('-').join('.'));
		var splitbirthday = OMSpopupView.find("#OMSForm_birthday").val().split(".");
		if(splitbirthday[0].length > 3) {
			OMSpopupView.find("#OMSForm_birthday").val(splitbirthday.reverse().join("."));
		}
		OMSpopupView.find("#OMSForm_submit").on("click", function() {
			OMSpopupView.find('form input').each(function() {
				var input = $(this);
				$.cookie(input.attr('id'), input.val(), { path: '/', domain:'.emportal.ru' });
				$("[id="+input.attr('id')+"]").val(input.val());
			});
			if($("#OMSpopupView #OMSForm_omsNumber").val().trim() !== "") {
				$.cookie('OMSpopup_isShowing', true, { path: '/', domain:'.emportal.ru' });
			}
			$.cookie('OMSForm_change', true, { path: '/', domain:'.emportal.ru' });
			parent.$.fancybox.close();
			window.location.search = "";
		});
		//$.removeCookie('OMSpopup_isShowing', { path: '/' });
	}
</script>