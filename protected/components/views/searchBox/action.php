<?php
/* @var $model Search */
Yii::app()->clientScript->registerScript('search-service', '
	$(document).ready(function() {
		var srv=$("#Action_serviceName");
		if($("#search-clinic-form_idCompanyActivite").val()) {
			srv.show();
		} else {
			srv.hide();
		}
	});
		      ');
$this->tab = isset($tabs[$this->tab]) ?
		$this->tab : key($tabs);
?>
<div class="search">
	<ul class="search-tabs">
		<?php foreach ($tabs as $key => $tab): ?>
			<li id="search-tab-label-<?= $key ?>" <?= $this->tab == $key ? 'class="current"' : '' ?>><?= $tab ?></li>
		<? endforeach; ?>
	</ul><!-- /search-tabs -->
	<div id="search-tab-1" class="search-box <?= $this->tab == Search::S_ACTION ? ' visible' : '' ?>">
		<?php
		/* @var $form CActiveForm */
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'get',
			'id' => 'search-action-form',
			'action' => '/actions',
			'enableAjaxValidation' => false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		));
		?>
		<fieldset>
			<!-- /search-row -->
			<div class="search-field w350">
				<?php
				echo $form->dropDownList($model, 'metroStationId', $MetroStationList, array('id' => $form->id . '_idMetroStation', 'empty' => 'Станция метро', 'class' => 'custom-select'));
				?>
				<?php
				echo $form->dropDownList($model, 'cityDistrictId', $CityDistrictList, array('id' => $form->id . '_idCityDistrict', 'empty' => 'Район города', 'class' => 'custom-select'));
				?>				
				<?php
				$companyActivityId = 'Action_serviceName';
				//echo $form->dropDownList($model, 'companyActiviteId', $CompanyActivitiesList, array('id' => $form->id . '_idCompanyActivite', 'empty' => 'Профиль деятельности', 'class' => 'custom-select'));
				echo $form->dropDownList($model, 'companyActiviteId', $CompanyActivitiesList, array('id' => $form->id . '_idCompanyActivite', 'empty'=>'Профиль деятельности','class' => 'custom-select', 'style'=>'width:292px;', 'onchange'=>'js:$( "#' . CHtml::activeId($model, 'serviceId') . ', #Action_serviceName" ).removeAttr("value");if(this.value) {jQuery( "#'.$companyActivityId.'" ).show();}else {jQuery( "#'.$companyActivityId.'" ).hide();}')); 
				echo $form->hiddenField($model, 'serviceId');
				echo $form->error($model, 'companyActivitesId');
				?>
				<?php							
				$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
					'model'			 => $model,
					'name'			 => 'Search[serviceName]',
					'id'			 => 'Action_serviceName',
					'value'			 => $model->serviceModel->name,
					'source'		 => 'js:function (request, response) {
						 $.ajax({
							type: "GET",
							url: "/ajax/ServiceByServiceActivite",
							dataType: "json",
							data: {
								"json": true,
								//"link": $( "#' . CHtml::activeId($model, 'companyActivitesId') . '" ).val(),
								"link": $("#search-clinic-form_idCompanyActivite").val(),									
								term: request.term
							},
							success: response
							});
					}',
					// additional javascript options for the autocomplete plugin
					'options'		 => array(
						'minLength'	 => '0',
						'change'	 => 'js:function( event, ui ) {
							if ( ui.item ) {
								$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( ui.item.id ).change();								
								return true;
							}
							else {							
								$(this).val( "" );							
								$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( "" ).change();							
								return false;
							}
							}'
					),
					'htmlOptions'	 => array(
						'class' => 'custom-text w450',
						'style' => 'width:337px;',						
					),					
				));
				?>

				<!--				<div class="search-action">
									<a href="#" title="Заказ лекарств" class="btn f-left"><i class="ico-order"></i>Заказ лекарств</a>
									<a href="#" title="Консультации врача" class="btn f-right"><i class="ico-skype"></i>Консультации врача</a>
								</div> /search-action -->
			</div><!-- /search-field -->

			<div class="search-field">
				<!--				<label>Стоимость первоначального обращения</label>
				
								<div class="search-cost">
									<select class="custom-select">
										<option value="1">от</option>
										<option value="2">от 2</option>
										<option value="3">от 3</option>
										<option value="4">от 4</option>
										<option value="5">от 5</option>
										<option value="6">от 6</option>
										<option value="7">от 7</option>
									</select>
									<select class="custom-select">
										<option value="1">до</option>
										<option value="2">до 2</option>
										<option value="3">до 3</option>
										<option value="4">до 4</option>
										<option value="5">до 5</option>
										<option value="6">до 6</option>
										<option value="7">до 7</option>
									</select>
				
								</div> /search-cost
								<div style="width: 263px;margin: 10px 0;padding: 0 7px;">
				<?php
				$this->widget('zii.widgets.jui.CJuiSliderInput', array(
					'name' => 'rate',
					'event' => 'change',
					'options' => array(
						'range' => true,
						'min' => 0,
						'max' => 12000,
						'step' => 500,
						'values' => array(0, 12000),
						'slide' => 'js:function(event, ui) {
							                        $("#Project_budgetMin").val(ui.values[0]);
							                        $("#Project_budgetMax").val(ui.values[1]);
							                 }',
				)));
				?>
								</div>-->
								<div class="search-time">
				<?php
				
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'model' => $model,
						'attribute' => 'start',
						'language' => Yii::app()->language,							
						'options' => array(						
							'dateFormat' => 'yy-mm-dd',							
						),
						'htmlOptions' => array(
							'id' => $form->id . '_start',
							'class' => 'custom-text colored-placeholder',	
							'placeholder'=>'Начало акции после'
						),
					)
				);
				?>
									<i class="ico-calendar"></i>
								</div> 

				<div class="search-action">
					<input type="submit" value="Найти" class="search-btn btn-blue">
					<div>
						<label><?php echo $form->checkBox($model, 'for_free'); ?> Бесплатно</label><br />									
					</div>
				</div><!-- /search-action -->
			</div><!-- /search-field -->
		</fieldset>
		<?php $this->endWidget(); ?>
	</div><!-- /search-box -->	
</div><!-- /search -->