<?php if($CompanyActivitiesList): ?>
	<div id="Search_companyActiviteId_table" class="specialtyTable addedStyle">
		<div class="f-left">
			<?php
			$count = count($CompanyActivitiesList);
			$n = 0;

			foreach ($CompanyActivitiesList as $key => $sp):  ?>		
				<div class="specialty" id="<?=$key?>" onclick="$('#Search_companyActiviteId').select2('val','<?=$key?>',true); return false;">
					<a href='#setCompanyActivite'><?=$sp?></a>
				</div>
				<?= ($n + 1) % intval(ceil($count/4)) ? '' : '</div><div class="f-left">' ?>
			<?php $n++; endforeach; ?>
		</div>
	</div>
<?php elseif($DoctorSpecialtyList): ?>
	<div id="Search_doctorSpecialtyId_table" class="specialtyTable addedStyle">
		<div class="f-left">
			<?php
			$count = count($DoctorSpecialtyList);
			$n = 0;

			foreach ($DoctorSpecialtyList as $key => $sp):  ?>		
				<div class="specialty" id="<?=$key?>" onclick="$('#Search_doctorSpecialtyId').select2('val','<?=$key?>',true); return false;">
					<a href='#setDoctorSpecialty'><?=$sp?></a>
				</div>
				<?= ($n + 1) % intval(ceil($count/4)) ? '' : '</div><div class="f-left">' ?>
			<?php $n++; endforeach; ?>
		</div>
	</div>
<?php endif; ?>

<div id="Search_metroStationId_table" class="specialtyTable addedStyle">
	<div class="f-left">
		<?php
		$count = count($CityDistrictList);
		$n = 0;
		?>
		<div class="dropDownTableHeader">
			<span>РАЙОНЫ</span>
		</div>
		<?php foreach ($CityDistrictList as $key => $sp):  ?>		
			<div class="specialty" id="<?=$key?>" onclick="if($(this).hasClass('highlighted')) select2RemoveValue('#Search_metroStationId','<?=$key?>'); else select2AddValue('#Search_metroStationId','<?=$key?>'); return false;">
				<a href='#setMetroStation' onclick="return false;"><?=$sp?></a>
			</div>
			<?php
			if(($n + 1) % intval(ceil($count/1))) {
				
			} else {
				echo '</div>';
				if(($n + 1) != $count) {
					echo '<div class="f-left">';
				}
			}
			?>
		<?php $n++; endforeach; ?>
	
	<?php
	if(isset($MetroStationList) AND is_array($MetroStationList) AND count($MetroStationList)===1 AND ($MetroStationList[key($MetroStationList)]==="Все метро" OR $MetroStationList[key($MetroStationList)]==="Нет")) {
		$MetroStationList = [];
	}
	$count = count($MetroStationList);
	if($count>0):
	$n = 0;
	?>
	<div class="f-left">
		<div class="dropDownTableHeader">
			<span>СТАНЦИИ МЕТРО</span>
		</div>
		<?php foreach ($MetroStationList as $key => $sp):  ?>		
			<div class="specialty" id="<?=$key?>" onclick="if($(this).hasClass('highlighted')) select2RemoveValue('#Search_metroStationId','<?=$key?>'); else select2AddValue('#Search_metroStationId','<?=$key?>'); return false;">
				<a href='#setMetroStation' onclick="return false;"><?=$sp?></a>
			</div>
			<?= ($n + 1) % intval(ceil($count/3)) ? '' : '</div><div class="f-left">' ?>
		<?php $n++; endforeach; ?>
	</div>
	<?php endif; ?>
</div>

<?php
Yii::app()->clientScript->registerScriptFile("/shared/js/_drop_table.js", CClientScript::POS_END);
?>