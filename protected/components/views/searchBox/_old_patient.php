<?php
/* @var $model Search */

Yii::app()->clientScript->registerScript('search-service', ''
		. '      document.getElementById(\'search-service-form_text\').oninput = function() {             
             var obj = $("#search-tab-3 .custom-select");
             if($("#search-service-form_text").val()) {
                obj.removeClass("classDisCusel");
            } else {
                obj.addClass("classDisCusel");
            }
        }    
		$(document).ready(function() {
		var srv=$("#Action_serviceName");
		if($("#search-clinic-form_idCompanyActivite").val()) {
			srv.show();
		} else {
			srv.hide();
		}
	});
	');
$this->tab = isset($tabs[$this->tab]) ?
		$this->tab : key($tabs);
?>
<div class="search ef-background">
	<!--
	<ul class="search-tabs">
		<?php foreach ($tabs as $key => $tab): ?>
			<li <?= $key == 'service' ? 'onclick="yaCounter18794458.reachGoal(\'enter_search_service\');"':'' ?> id="search-tab-label-<?= $key ?>" <?= $this->tab == $key ? 'class="current"' : '' ?>><?= $tab ?></li>
		<? endforeach; ?>
	</ul>
	-->
	<!-- /search-tabs -->
	<div id="search-tab-1" class="search-box <?= in_array($_SERVER['PATH_INFO'],$this->pushHistoryUrl) ? 'pushHistory' : '' ?> <?= $this->tab == Search::S_CLINIC ? ' visible' : '' ?>">
		<?php
		/* @var $form CActiveForm */
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'get',
			'id' => 'search-clinic-form',
			'action' => '/search/clinic',
			'enableAjaxValidation' => false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		));
		?>
		<fieldset>
			<div class="search-row">
				<?php				
				$this->widget('ESelect2',[							
						'name'=> CHtml::activeName($model, 'companyActiviteId'),						
						'data' =>  $CompanyActivitiesList,
						'options'=>[
							'placeholder'=>'Профиль деятельности',
							'allowClear'=>true,							
						],
						'htmlOptions' => [
							'style' => 'margin-right:20px;width:348px;'
						]
						
					]);
				#echo $form->dropDownList($model, 'doctorSpecialtyId', $DoctorSpecialtyList, array('id' => $form->id . '_idDoctorSpecialty', 'empty' => 'Специальность врача', 'class' => 'custom-select w348'));
				?>
				<?php
				#$form->dropDownList($model, 'metroStationId', array_merge($MetroStationList,$CityDistrictList), array('id' => $form->id . '_idMetroStation', 'empty' => 'Станция метро или район города', 'class' => 'custom-select w348'));
				?>
				<?php 
					$this->widget('ESelect2',[						
						'name'=> CHtml::activeName($model, 'metroStationId'),		
						#'id' => CHtml::activeId($model, 'metroStationId'),
						'data' => [ 
							'Станции метро' => $MetroStationList,
							'Районы города' => $CityDistrictList
						],
						'options'=>[
							'placeholder'=>'Станция метро или район города',
							'allowClear'=>true,							
						],
						'htmlOptions' => [
							'multiple' => 'multiple',
							'size' => 1,
							'style' => 'width:336px;'
						]
					]);
				?>
				 
				
				<?= $form->hiddenField($model, 'text'); ?>
				<?php /*CHtml::hiddenField('price', CHtml::encode(Yii::app()->request->getParam('price'))); */?>
				<input type="submit" value="Найти" class="search-btn btn-blue">
			</div>

			<div class="search-field">
				<!--				<label>Стоимость первоначального обращения</label>
				
								<div class="search-cost">
									<select class="custom-select">
										<option value="1">от</option>
										<option value="2">от 2</option>
										<option value="3">от 3</option>
										<option value="4">от 4</option>
										<option value="5">от 5</option>
										<option value="6">от 6</option>
										<option value="7">от 7</option>
									</select>
									<select class="custom-select">
										<option value="1">до</option>
										<option value="2">до 2</option>
										<option value="3">до 3</option>
										<option value="4">до 4</option>
										<option value="5">до 5</option>
										<option value="6">до 6</option>
										<option value="7">до 7</option>
									</select>
				
								</div> /search-cost
								<div style="width: 263px;margin: 10px 0;padding: 0 7px;">
				<?php
				$this->widget('zii.widgets.jui.CJuiSliderInput', array(
					'name' => 'rate',
					'event' => 'change',
					'options' => array(
						'range' => true,
						'min' => 0,
						'max' => 12000,
						'step' => 500,
						'values' => array(0, 12000),
						'slide' => 'js:function(event, ui) {
							                        $("#Project_budgetMin").val(ui.values[0]);
							                        $("#Project_budgetMax").val(ui.values[1]);
							                 }',
				)));
				?>
								</div>
								<div class="search-time">
				<?php
				$this->widget('JuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'visit_time',
					'language' => Yii::app()->language,
					'options' => array(
						'numberOfMonths' => 2,
						'minDate' => 1,
						'maxDate' => 30,
						'showMinute' => false,
					),
					'htmlOptions' => array(
						'id' => $form->id . '_visit_time',
						'class' => 'custom-text',
						'value' => 'Желаемые время и дата приема',
					),
						)
				);
				?>
									<i class="ico-calendar"></i>
								</div> /search-time -->
			
			</div><!-- /search-field -->
		</fieldset>
		<?php $this->endWidget(); ?>
	</div><!-- /search-box -->
	<div id="search-tab-2" class="search-box <?= in_array($_SERVER['PATH_INFO'],$this->pushHistoryUrl) ? 'pushHistory' : '' ?> <?= $this->tab == Search::S_DOCTOR ? ' visible' : '' ?>">
		<?php
		/* @var $form CActiveForm */
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'get',
			'id' => 'search-doctor-form',
			'action' => '/search/doctor',
			'enableAjaxValidation' => false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		));
		?>
		<fieldset>

			
			<div class="search-row">
				<?php				
				$this->widget('ESelect2',[						
						'name'=> CHtml::activeName($model, 'doctorSpecialtyId'),						
						'data' =>  $DoctorSpecialtyList,
						'options'=>[
							'placeholder'=>'Специальность врача',
							'allowClear'=>true,							
						],
						'htmlOptions' => [
							'style' => 'margin-right:20px;width:348px;'
						]
						
					]);
				#echo $form->dropDownList($model, 'doctorSpecialtyId', $DoctorSpecialtyList, array('id' => $form->id . '_idDoctorSpecialty', 'empty' => 'Специальность врача', 'class' => 'custom-select w348'));
				?>
				<?php
				#$form->dropDownList($model, 'metroStationId', array_merge($MetroStationList,$CityDistrictList), array('id' => $form->id . '_idMetroStation', 'empty' => 'Станция метро или район города', 'class' => 'custom-select w348'));
				?>
				<?php 
					$this->widget('ESelect2',[						
						'name'=> CHtml::activeName($model, 'metroStationId'),						
						#'id'=> CHtml::activeId($model, 'metroStationId').'1',
						'data' => [ 
							'Станции метро' => $MetroStationList,
							'Районы города' => $CityDistrictList
						],
						'options'=>[
							'placeholder'=>'Станция метро или район города',
							'allowClear'=>true,							
						],
						'htmlOptions' => [
							'multiple' => 'multiple',
							'size' => 1,
							'style' => 'width:336px;'
						]
					]);
				?>
				<?php
				echo $form->hiddenField($model, 'sexId');
				?>
				<?php
				echo $form->hiddenField($model, 'price');
				?>
				<?php echo $form->hiddenField($model, 'onHouse', array('class' => '')); 
				?>
				<?= $form->hiddenField($model, 'text'); ?>
				<?php /*CHtml::hiddenField('price', CHtml::encode(Yii::app()->request->getParam('price'))); */?>
				<input type="submit" value="Найти" class="search-btn btn-blue">
			</div><!-- /search-row -->			
			<!-- /search-field -->

			<div class="search-field">
				<!--				<label>Стоимость первоначального обращения</label>
				
								<div class="search-cost">
									<select class="custom-select">
										<option value="1">от</option>
										<option value="2">от 2</option>
										<option value="3">от 3</option>
										<option value="4">от 4</option>
										<option value="5">от 5</option>
										<option value="6">от 6</option>
										<option value="7">от 7</option>
									</select>
									<select class="custom-select">
										<option value="1">до</option>
										<option value="2">до 2</option>
										<option value="3">до 3</option>
										<option value="4">до 4</option>
										<option value="5">до 5</option>
										<option value="6">до 6</option>
										<option value="7">до 7</option>
									</select>
				
								</div> /search-cost
								<div style="width: 263px;margin: 10px 0;padding: 0 7px;">
				<?php
				$this->widget('zii.widgets.jui.CJuiSliderInput', array(
					'name' => 'rate',
					'event' => 'change',
					'options' => array(
						'range' => true,
						'min' => 0,
						'max' => 12000,
						'step' => 500,
						'values' => array(0, 12000),
						'slide' => 'js:function(event, ui) {
							                        $("#Project_budgetMin").val(ui.values[0]);
							                        $("#Project_budgetMax").val(ui.values[1]);
							                 }',
				)));
				?>
								</div>
								<div class="search-time">
				<?php
				$this->widget('JuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'visit_time',
					'language' => Yii::app()->language,
					'options' => array(
						'numberOfMonths' => 2,
						'minDate' => 1,
						'maxDate' => 30,
						'showMinute' => false,
					),
					'htmlOptions' => array(
						'id' => $form->id . '_visit_time',
						'class' => 'custom-text',
						'value' => 'Желаемые время и дата приема',
					),
						)
				);
				?>
									<i class="ico-calendar"></i>
								</div> /search-time -->
				
				<!-- /search-action -->
			</div><!-- /search-field -->
		</fieldset>
		<?php $this->endWidget(); ?>
	

				<!--				<div class="search-action">
									<a href="#" title="Заказ лекарств" class="btn f-left"><i class="ico-order"></i>Заказ лекарств</a>
									<a href="#" title="Консультации врача" class="btn f-right"><i class="ico-skype"></i>Консультации врача</a>
								</div> /search-action -->
			</div><!-- /search-field -->

			<div class="search-field">
				<!--				<label>Стоимость первоначального обращения</label>
				
								<div class="search-cost">
									<select class="custom-select">
										<option value="1">от</option>
										<option value="2">от 2</option>
										<option value="3">от 3</option>
										<option value="4">от 4</option>
										<option value="5">от 5</option>
										<option value="6">от 6</option>
										<option value="7">от 7</option>
									</select>
									<select class="custom-select">
										<option value="1">до</option>
										<option value="2">до 2</option>
										<option value="3">до 3</option>
										<option value="4">до 4</option>
										<option value="5">до 5</option>
										<option value="6">до 6</option>
										<option value="7">до 7</option>
									</select>
				
								</div> /search-cost
								<div style="width: 263px;margin: 10px 0;padding: 0 7px;">
				<?php
				$this->widget('zii.widgets.jui.CJuiSliderInput', array(
					'name' => 'rate',
					'event' => 'change',
					'options' => array(
						'range' => true,
						'min' => 0,
						'max' => 12000,
						'step' => 500,
						'values' => array(0, 12000),
						'slide' => 'js:function(event, ui) {
							                        $("#Project_budgetMin").val(ui.values[0]);
							                        $("#Project_budgetMax").val(ui.values[1]);
							                 }',
				)));
				?>
								</div>
								<div class="search-time">
				<?php
				$this->widget('JuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'visit_time',
					'language' => Yii::app()->language,
					'options' => array(
						'numberOfMonths' => 2,
						'minDate' => 1,
						'maxDate' => 30,
						'showMinute' => false,
					),
					'htmlOptions' => array(
						'id' => $form->id . '_visit_time',
						'class' => 'custom-text',
						'value' => 'Желаемые время и дата приема',
					),
						)
				);
				?>
									<i class="ico-calendar"></i>
								</div> /search-time -->						
	</div><!-- /search-box -->
	
	<div id="search-tab-3" class="search-box <?= in_array($_SERVER['PATH_INFO'],$this->pushHistoryUrl) ? 'pushHistory' : '' ?> <?= $this->tab == Search::S_SERVICE ? ' visible' : '' ?>">
		
		<?php
		/* @var $form CActiveForm */
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'get',
			'id' => 'search-service-form',
			'action' => '/search/service',
			'enableAjaxValidation' => false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		));
		?>
		<fieldset>

			<div class="search-row">
				<?php
				echo $form->textField($model, 'text', array(
					'id' => $form->id . '_text',
					'class' => 'custom-text w643',
					'placeholder' => 'Введите название услуги',
					'value' => $this->tab == 'service' ? CHtml::encode($_REQUEST['Search']['text']) : ''
				));
				?>							
				<?php echo $form->hiddenField($model, 'isStrictText', array('id' => $form->id . '_isStrictText',)); ?>
			</div><!-- /search-row -->
			<div class="search_action_words">
				<a href="#" id="search_service_all">Все услуги</a>
				<a href="/ajax/ServiceSectionByServiceActivite/000000069.html" class="fancybox3" id="diagnostic">Диагностика</a>
			</div>
			<div class="search-field w350">
				<?php
				echo $form->dropDownList($model, 'metroStationId', $MetroStationList, array('id' => $form->id . '_idMetroStation', 'disabled' => '', 'empty' => 'Станция метро', 'class' => 'custom-select'));
				?>
<?php
echo $form->dropDownList($model, 'cityDistrictId', $CityDistrictList, array('id' => $form->id . '_idCityDistrict', 'empty' => 'Район города', 'class' => 'custom-select'));
?>
<?php
//echo $form->dropDownList($model, 'doctorSpecialtyId', $DoctorSpecialtyList, array('id'	 => $form->id . '_idDoctorSpecialty', 'empty'	 => 'Специальность врача', 'class'	 => 'custom-select'));
?>

				<!--				<div class="search-action">
									<a href="#" title="Заказ лекарств" class="btn f-left"><i class="ico-order"></i>Заказ лекарств</a>
									<a href="#" title="Консультации врача" class="btn f-right"><i class="ico-skype"></i>Консультации врача</a>
								</div> /search-action -->
			</div><!-- /search-field -->

			<div class="search-field">
				<input type="submit" value="Найти" class="search-btn btn-blue">
				<div class="search-cost">
					<label>Стоимость услуги</label><br/>
					<select name="price" class="custom-select">
						<option <?= Yii::app()->request->getParam('price') == 1 ? 'selected="true"' : '' ?> value="1">от - до</option>
						<option <?= Yii::app()->request->getParam('price') == 2 ? 'selected="true"' : '' ?> value="2">от 0 до 500</option>
						<option <?= Yii::app()->request->getParam('price') == 3 ? 'selected="true"' : '' ?> value="3">от 500 до 1000</option>
						<option <?= Yii::app()->request->getParam('price') == 4 ? 'selected="true"' : '' ?> value="4">от 1000 до 2000</option>
						<option <?= Yii::app()->request->getParam('price') == 5 ? 'selected="true"' : '' ?> value="5">от 2000 до 3000</option>
						<option <?= Yii::app()->request->getParam('price') == 6 ? 'selected="true"' : '' ?> value="6">от 3000 до 5000</option>
						<option <?= Yii::app()->request->getParam('price') == 7 ? 'selected="true"' : '' ?> value="7">от 5000</option>
					</select>

				</div>
				<!--
				<div style="width: 263px;margin: 10px 0;padding: 0 7px;">
				<?php
				$this->widget('zii.widgets.jui.CJuiSliderInput', array(
					'name' => 'rate',
					'event' => 'change',
					'options' => array(
						'range' => true,
						'min' => 0,
						'max' => 12000,
						'step' => 500,
						'values' => array(0, 12000),
						'slide' => 'js:function(event, ui) {
							                        $("#Project_budgetMin").val(ui.values[0]);
							                        $("#Project_budgetMax").val(ui.values[1]);
							                 }',
				)));
				?>
				</div>
				<div class="search-time">
				<?php
				$this->widget('JuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'visit_time',
					'language' => Yii::app()->language,
					'options' => array(
						'numberOfMonths' => 2,
						'minDate' => 1,
						'maxDate' => 30,
						'showMinute' => false,
					),
					'htmlOptions' => array(
						'id' => $form->id . '_visit_time',
						'class' => 'custom-text',
						'value' => 'Желаемые время и дата приема',
					),
						)
				);
				?>
					<i class="ico-calendar"></i>
				</div> /search-time -->
				<div class="search-action service_btn">					
					
					<div  class="both"></div>
				</div><!-- /search-action -->
			</div><!-- /search-field -->
		</fieldset>
<?php $this->endWidget(); ?>
	</div><!-- /search-box -->
	<div id="search-tab-4" class="search-box <?= $this->tab == Search::S_ACTION ? ' visible' : '' ?>">
		<?php
		/* @var $form CActiveForm */
		$form = $this->beginWidget('CActiveForm', array(
			'method' => 'get',
			'id' => 'search-action-form',
			'action' => '/actions',
			'enableAjaxValidation' => false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		));
		?>
		<fieldset>
			<!-- /search-row -->
			<div class="search-field w350">
				<?php
				echo $form->dropDownList($model, 'metroStationId', $MetroStationList, array('id' => $form->id . '_idMetroStation', 'empty' => 'Станция метро', 'class' => 'custom-select'));
				?>
				<?php
				echo $form->dropDownList($model, 'cityDistrictId', $CityDistrictList, array('id' => $form->id . '_idCityDistrict', 'empty' => 'Район города', 'class' => 'custom-select'));
				?>				
				<?php
				$companyActivityId = 'Action_serviceName';
				//echo $form->dropDownList($model, 'companyActiviteId', $CompanyActivitiesList, array('id' => $form->id . '_idCompanyActivite', 'empty' => 'Профиль деятельности', 'class' => 'custom-select'));
				echo $form->dropDownList($model, 'companyActiviteId', $CompanyActivitiesList, array('id' => $form->id . '_idCompanyActivite', 'empty'=>'Профиль деятельности','class' => 'custom-select', 'style'=>'width:292px;', 'onchange'=>'js:$( "#' . CHtml::activeId($model, 'serviceId') . ', #Action_serviceName" ).removeAttr("value");if(this.value) {jQuery( "#'.$companyActivityId.'" ).show();}else {jQuery( "#'.$companyActivityId.'" ).hide();}')); 
				echo $form->hiddenField($model, 'serviceId');
				echo $form->error($model, 'companyActivitesId');
				?>
				<?php							
				$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
					'model'			 => $model,
					'name'			 => 'Search[serviceName]',
					'id'			 => 'Action_serviceName',
					'value'			 => $model->serviceModel->name,
					'source'		 => 'js:function (request, response) {
						 $.ajax({
							type: "GET",
							url: "/ajax/ServiceByServiceActivite",
							dataType: "json",
							data: {
								"json": true,
								//"link": $( "#' . CHtml::activeId($model, 'companyActivitesId') . '" ).val(),
								"link": $("#search-clinic-form_idCompanyActivite").val(),									
								term: request.term
							},
							success: response
							});
					}',
					// additional javascript options for the autocomplete plugin
					'options'		 => array(
						'minLength'	 => '0',
						'change'	 => 'js:function( event, ui ) {
							if ( ui.item ) {
								$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( ui.item.id ).change();								
								return true;
							}
							else {							
								$(this).val( "" );							
								$( "#' . CHtml::activeId($model, 'serviceId') . '" ).val( "" ).change();							
								return false;
							}
							}'
					),
					'htmlOptions'	 => array(
						'class' => 'custom-text w450',
						'style' => 'width:337px;',						
					),					
				));
				?>

				<!--				<div class="search-action">
									<a href="#" title="Заказ лекарств" class="btn f-left"><i class="ico-order"></i>Заказ лекарств</a>
									<a href="#" title="Консультации врача" class="btn f-right"><i class="ico-skype"></i>Консультации врача</a>
								</div> /search-action -->
			</div><!-- /search-field -->

			<div class="search-field">
				<!--				<label>Стоимость первоначального обращения</label>
				
								<div class="search-cost">
									<select class="custom-select">
										<option value="1">от</option>
										<option value="2">от 2</option>
										<option value="3">от 3</option>
										<option value="4">от 4</option>
										<option value="5">от 5</option>
										<option value="6">от 6</option>
										<option value="7">от 7</option>
									</select>
									<select class="custom-select">
										<option value="1">до</option>
										<option value="2">до 2</option>
										<option value="3">до 3</option>
										<option value="4">до 4</option>
										<option value="5">до 5</option>
										<option value="6">до 6</option>
										<option value="7">до 7</option>
									</select>
				
								</div> /search-cost
								<div style="width: 263px;margin: 10px 0;padding: 0 7px;">
				<?php
				$this->widget('zii.widgets.jui.CJuiSliderInput', array(
					'name' => 'rate',
					'event' => 'change',
					'options' => array(
						'range' => true,
						'min' => 0,
						'max' => 12000,
						'step' => 500,
						'values' => array(0, 12000),
						'slide' => 'js:function(event, ui) {
							                        $("#Project_budgetMin").val(ui.values[0]);
							                        $("#Project_budgetMax").val(ui.values[1]);
							                 }',
				)));
				?>
								</div>-->
								<div class="search-time">
				<?php
				
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'model' => $model,
						'attribute' => 'start',
						'language' => Yii::app()->language,							
						'options' => array(						
							'dateFormat' => 'yy-mm-dd',							
						),
						'htmlOptions' => array(
							'id' => $form->id . '_start',
							'class' => 'custom-text colored-placeholder',	
							'placeholder'=>'Начало акции после'
						),
					)
				);
				?>
									<i class="ico-calendar"></i>
								</div> 

				<div class="search-action">
					<input type="submit" value="Найти" class="search-btn btn-blue">
					<div>
						<label><?php echo $form->checkBox($model, 'for_free'); ?> Бесплатно</label><br />									
					</div>
				</div><!-- /search-action -->
			</div><!-- /search-field -->
		</fieldset>
		<?php $this->endWidget(); ?>
	</div><!-- /search-box -->	
</div><!-- /search -->