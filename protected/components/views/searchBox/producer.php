<?php
/* @var $model Search */

$last_search_tab = isset(Yii::app()->request->cookies['last_search_tab']) ?
		Yii::app()->request->cookies['last_search_tab']->value : '1';
?>
<div class="search-box visible">
	<div class="crumbs"><a href="#">Главная</a> > <span>Каталог поставщиков</span></div>
	<?php
	/* @var $form CActiveForm */
	$form			 = $this->beginWidget('CActiveForm', array(
		'id'					 => 'search-'.Search::S_INSURANCE.'-form',
		'action'				 => '/search/'.Search::S_INSURANCE,
		'enableAjaxValidation'	 => false,
		'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
			));
	?>
		<fieldset>
			<div class="search-row">
				<?php echo $form->textField($model, 'text', array('id'=>$form->id.'_text', 'class' => 'custom-text w643')); ?>
			</div><!-- /search-row -->
			<div class="search-field w350">
				<?php
				echo $form->dropDownList($model, 'idRegions', $model->regions, array('id'=>$form->id.'_idRegions', 'empty'	 => 'Регион', 'class'	 => 'custom-select'));
				?>
				<?php
				echo $form->dropDownList($model, 'idNomenclatureGroup', $model->nomenclatureGroups, array('id'=>$form->id.'_idNomenclatureGroup', 'empty'	 => 'Номенклатурная группа', 'class'	 => 'custom-select'));
				?>
				<select class="custom-select">
					<option value="1">Тип конрагента</option>
					<option value="2">Тип конрагента 2</option>
					<option value="3">Тип конрагента 3</option>
					<option value="4">Тип конрагента 4</option>
					<option value="5">Тип конрагента 5</option>
					<option value="6">Тип конрагента 6</option>
					<option value="7">Тип конрагента 7</option>
				</select>                                            
			</div><!-- /search-field -->
			<div class="search-field right">
				<?php
				echo $form->dropDownList($model, 'idCities', $model->cities, array('id'=>$form->id.'_idCities', 'empty'	 => 'Город', 'class'	 => 'custom-select'));
				?>                                           
				<?php
				echo $form->dropDownList($model, 'idBrands', $model->brands, array('id'=>$form->id.'_idBrands', 'empty'	 => 'Бренды', 'class'	 => 'custom-select'));
				?>
				<div class="search-action">
					<input type="submit" value="Найти" class="btn-blue">
				</div><!-- /search-action -->
			</div><!-- /search-field -->
		</fieldset>
	<?php $this->endWidget(); ?>
</div><!-- /search-box -->