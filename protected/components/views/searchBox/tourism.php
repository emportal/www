<?php
/* @var $model Search */

$last_search_tab = isset(Yii::app()->request->cookies['last_search_tab']) ?
		Yii::app()->request->cookies['last_search_tab']->value : '1';
?>
<div class="search-box visible">
	<div class="crumbs"><a href="/">Главная</a> > <span>Тур. фирмы и тур. операторы</span></div>
	<?php
	/* @var $form CActiveForm */
	$form			 = $this->beginWidget('CActiveForm', array(
		'method'				 => 'get',
		'id'					 => 'search-'.Search::S_TOURISM.'-form',
		'action'				 => '/search/'.Search::S_TOURISM,
		'enableAjaxValidation'	 => false,
		'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
			));
	?>
		<fieldset>
			<div class="search-row">
				<?php echo $form->textField($model, 'text', array('id'=>$form->id.'_text', 'class' => 'custom-text w643')); ?>
			</div><!-- /search-row -->
			<div class="search-field w350">

			<?php
				echo $form->dropDownList($model, 'metroStationId', $MetroStationList, array('id'	 => $form->id . '_idMetroStation', 'empty'	 => 'Станция метро', 'class'	 => 'custom-select'));
			?>
			
			</div><!-- /search-field -->
			<div class="search-field right">
				
				<div class="search-action">
					<input type="submit" value="Найти" class="btn-blue">
					<div>
					<!--	<label><?php echo $form->checkBox($model, 'IsOMS')?> Полис ОМС</label><br />-->
						<?php /*?><label><?php echo $form->checkBox($model, 'IsDMS')?> Полис ДМС</label><br /><?*/?>
					</div>
				</div><!-- /search-action -->
			</div><!-- /search-field -->
		</fieldset>
	<?php $this->endWidget(); ?>
</div><!-- /search-box -->