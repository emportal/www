<?php
/* @var $model Search */

$this->tab = isset($tabs[$this->tab]) ?
		$this->tab : key($tabs);
?>
<div class="search">
	<ul class="search-tabs">
		<?php foreach ($tabs as $key => $tab): ?>
			<li id="search-tab-label-<?= $key ?>" <?= $this->tab == $key ? 'class="current"' : '' ?>><?= $tab ?></li>
		<? endforeach; ?>
	</ul><!-- /search-tabs -->
	<div id="search-tab-1" class="search-box <?= $this->tab == Search::S_CLINIC ? ' visible' : '' ?>">
		<?php
		/* @var $form CActiveForm */
		$form	 = $this->beginWidget('CActiveForm', array(
			'method'				 => 'get',
			'id'					 => 'search-clinic-form',
			'action'				 => '/search/clinic',
			'enableAjaxValidation'	 => false,
			'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
				));
		?>
		<fieldset>
			<div class="search-row">
				<?php echo $form->textField($model, 'text', array('id'	 => $form->id . '_text', 'class'	 => 'custom-text w800')); ?>
				<input type="submit" value="Найти" class="btn-blue">
			</div><!-- /search-row -->
		</fieldset>
		<?php $this->endWidget(); ?>
	</div><!-- /search-box -->
	<div id="search-tab-2" class="search-box <?= $this->tab == Search::S_DOCTOR ? ' visible' : '' ?>">
		<?php
		/* @var $form CActiveForm */

		$form = $this->beginWidget('CActiveForm', array(
			'method'				 => 'get',
			'id'					 => 'search-doctor-form',
			'action'				 => '/search/doctor',
			'enableAjaxValidation'	 => false,
			'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
				));
		?>
		<fieldset>
			<div class="search-row">
				<?php echo $form->textField($model, 'text', array('id'	 => $form->id . '_text', 'class'	 => 'custom-text w800')); ?>
				<input type="submit" value="Найти" class="btn-blue">
			</div><!-- /search-row -->
		</fieldset>
		<?php $this->endWidget(); ?>
	</div><!-- /search-box -->
	<div id="search-tab-3" class="search-box <?= $this->tab == Search::S_SERVICE ? ' visible' : '' ?>">
		<?php
		/* @var $form CActiveForm */
		$form	 = $this->beginWidget('CActiveForm', array(
			'method'				 => 'get',
			'id'					 => 'search-service-form',
			'action'				 => '/search/service',
			'enableAjaxValidation'	 => false,
			'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
				));
		?>
		<fieldset>
			<div class="search-row">
				<?php echo $form->textField($model, 'text', array('id'	 => $form->id . '_text', 'class'	 => 'custom-text w800')); ?>
				<input type="submit" value="Найти" class="btn-blue">
			</div><!-- /search-row -->
		</fieldset>
		<?php $this->endWidget(); ?>
	</div><!-- /search-box -->
</div><!-- /search -->