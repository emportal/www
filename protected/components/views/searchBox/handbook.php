<?php
/* @var $this SearchBox */
/* @var $model Search */
/* @var $tabs array */

$this->tab = isset($tabs[$this->tab]) ?
		$this->tab : key($tabs);
?>
<div class="search">
	<ul class="search-tabs">
		<?php foreach ($tabs as $key => $tab): ?>
			<li id="search-tab-label-<?= $key ?>" <?= $this->tab == $key ? 'class="current"' : '' ?>><?= $tab ?></li>
		<? endforeach; ?>
	</ul><!-- /search-tabs -->
	<?php foreach ($tabs as $key => $tab): ?>
		<div id="search-tab-<?= $key ?>" class="search-box <?= $this->tab == $key ? 'visible' : '' ?>">
			<?php $url	 = $this->controller->createUrl('/search/' . $key) ?>
			<?php
			/* @var $form CActiveForm */
			$form	 = $this->beginWidget('CActiveForm',
							   array(
				'method'				 => 'get',
				'id'					 => 'search-' . $key . '-form',
				'action'				 => $url,
				'enableAjaxValidation'	 => false,
				'htmlOptions'			 => array('enctype' => 'multipart/form-data'),
					)
			);
			?>
			<fieldset>
				<div class="search-row">
					<?php echo $form->textField($model,
																					 'text',
																					 array('id'	 => $form->id . '_text', 'class'	 => 'custom-text w625')); ?>
					<input type="submit" value="Ок" class="btn-blue">
				</div>
			<?php echo $this->letterList($url, $model->chars,$key); ?>
			</fieldset>
	<?php $this->endWidget(); ?>
		</div><!-- /search-box -->
<? endforeach; ?>
</div><!-- /search -->