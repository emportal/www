<?php
/* @var $model Search */

Yii::app()->clientScript->registerScript('search-service', '');
$this->tab = isset($tabs[$this->tab]) ?
		$this->tab : key($tabs);
?>
<div class="SECTION SEARCH_BOX_HEAD">
	<div class="main_column">
		<a href="<?=Yii::app()->params['isBlogDomen']?"http://emportal.ru":""?>/" title="Единый Медицинский Портал"><img src="/images/newLayouts/LOGO-MAIN-mini<?=(!in_array(Yii::app()->session['selectedRegion'], ['spb','moskva', 'nizhny-novgorod', 'ekaterinburg', 'perm', 'voronezh'])) ? "-beta" : ""?>.png" alt="Единый Медицинский Портал" class="LOGO_MAIN-mini"></a>
		<?php $this->controller->widget('Breadcrumbs', array('links' => $this->controller->breadcrumbs, 'htmlOptions' => [ 'class' => 'mobilecrumbs', 'style' => 'display:none;' ] )); ?>
		<div id="search-tab-1" class="search-box <?= in_array($_SERVER['PATH_INFO'],$this->pushHistoryUrl) ? 'pushHistory' : '' ?> <?= $this->tab == Search::S_CLINIC ? ' visible' : '' ?>">
			<?php
			$form = $this->beginWidget('CActiveForm', array(
				'method' => 'get',
				'id' => 'search-clinic-form',
				'action' => '/clinic',
				'enableAjaxValidation' => false,
				'htmlOptions' => array('enctype' => 'multipart/form-data'),
			));
			?>
			<fieldset>
				<div class="search-row">
					<?php if (!Yii::app()->params['samozapis']) : ?>
					<h1 class="slogan">Запись на приём к врачу в частные клиники</h1>
					<?php else : ?>
					<h1 class="slogan">Самозапись на приём к врачу по полису ОМС в <?= City::model()->getSelectedCity()->locative ?></h1>
					<?php endif; ?>
					<?php if(!Yii::app()->params['isBlogDomen']): ?>
					<div class="your_city city-in-search in-page-search">
						<?php
						$this->widget('UserRegion', [
							'regions' => Yii::app()->params['regions'],
							'selectedRegion' => Yii::app()->session['selectedRegion']
						]);
						?>
					</div>
					<?php endif; ?>
					<?php								
						$this->widget('ESelect2',[							
							'name'=> CHtml::activeName($model, 'companyActiviteId'),						
							'data' => $CompanyActivitiesList,
							'value' => $model->companyActiviteId,
							'options'=>[
								'placeholder'=>'Профиль деятельности',
								'allowClear'=>true,							
							],
							'htmlOptions' => [
								'style' => '
									width: 242px;
									height: 40px;
									vertical-align: top;
									border-radius: 4px 0px 0px 4px;
									position: absolute;
								'
							]
							
						]);
					 
						$this->widget('ESelect2',[						
							'name'=> CHtml::activeName($model, 'metroStationId'),		
							#'id' => CHtml::activeId($model, 'metroStationId'),
							'value' => $model->metroStationId,
							'data' => [
								'Районы города' => $CityDistrictList,
								'Станции метро' => $MetroStationList
							],
							'options'=>[
								'placeholder'=>'Любое место',
								'allowClear'=>true,							
							],
							'htmlOptions' => [
								'multiple' => (Yii::app()->params["samozapis"]) ? NULL : 'multiple',
								'size' => 1,
								'style' => "
									width: 246px;
									height: 40px;
									vertical-align: top;
									margin-left: 242px;
									position: absolute;
								",
							]
						]);
					?>
					 
					<?= $form->hiddenField($model, 'companyTypeId'); ?>
					<?= $form->hiddenField($model, 'text'); ?>
					<?= $form->hiddenField($model, 'loyaltyProgram'); ?>
					<?= $form->hiddenField($model, 'offline'); ?>
					<button type="submit" value="Найти клинику" class="search-btn"><span>Найти клинику</span><span class="icon_search_mini"></span></button>
				</div>
			</fieldset>
			<?php
				$this->render('searchBox/_drop_table',
					array(
						"model"=>$model,
						"CompanyActivitiesList"=>$CompanyActivitiesList,
						"CityDistrictList"=>$CityDistrictList,
						"MetroStationList"=>$MetroStationList
					)
				);
			?>
			<?php
				if (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) {
					$this->render('searchBox/_oms-filter_block', array('model'=>$model, 'form'=>$form));
				}
			?>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>