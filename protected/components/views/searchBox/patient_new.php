<?php
/* @var $model Search */
//Yii::app()->clientScript->registerScript('search-service', '');
$this->tab = isset($tabs[$this->tab]) ? $this->tab : key($tabs);
?>
<style>
	input#s2id_autogen2.select2-input.select2-default {
		color: rgb(51,51,51) !important;  
	}
	#s2id_Search_metroStationId .select2-choices {
		border-left:0 !important; 
	}
	
<?php
 $samozapisSpb = Yii::app()->params["samozapis"] && ($cityId === City::SPB);
 $metroStationSelectStyle = "width:330px; height: 62px; vertical-align: top;".($samozapisSpb ? "" : " margin-left: 340px;")." position: absolute;";
 if ($samozapisSpb) :
	$metroStationSelectStyle = "width: 660px !important; height: 62px; left: 10px !important; vertical-align: top;".($samozapisSpb ? "" : " margin-left: 340px;")." position: absolute;";
	?>
	
	.search-box_newlayout #s2id_Search_metroStationId .select2-choices, .search-box_newlayout #s2id_Search_metroStationId2 .select2-choices {
		background: url(/images/newLayouts/icon_search_drop.png) 615px 25px no-repeat white!important;
	}	
	.searchBoxSpecialtyLabel {
		display: none !important;
	}	
	.searchBoxMetroLabel {
		margin-left: 20px !important;
	}	
	#s2id_Search_metroStationId .select2-choices {
		border-left:0 !important;
		border-radius: 6px 0 0 6px;
	}
	
<?php endif; ?>
</style>

<div class="SECTION FIRST_SCREEN">
	<div class="main_column main_first_screen_column">
		<a href="/" title="Единый Медицинский Портал">
			<img src="/images/newLayouts/LOGO-MAIN<?=(!in_array(Yii::app()->session['selectedRegion'], ['spb', 'moskva', 'nizhny-novgorod', 'ekaterinburg', 'perm', 'voronezh'])) ? "-beta" : ""?>.png" alt="Единый Медицинский Портал" class="LOGO_MAIN">
		</a>
		<?php
		$RegionPhones = UserRegion::getRegionPhones();
		if(!empty($RegionPhones['empRegistryPhone'])):
		?>
		<div class="phone-block">
			<div class="title-phone">Поможем найти врача</div>
			<div class="phone"><?=$RegionPhones['empRegistryPhone'] ?></div>
			<div class="call-back">или <a href="#" onClick="$('.button-widget-open').click()">закажите обратный звонок</a></div>
		</div>
		<?php endif; ?>
		<?php
		echo '<h1 class="slogan-newlayout">';
		echo Yii::app()->params["samozapis"] ? 'САМОЗАПИСЬ К ВРАЧУ В 3 КЛИКА' : 'ЗАПИШИТЕСЬ К ВРАЧУ В 3 КЛИКА';
		echo '</h1>';
		?>
		<div class="search-box_section border_radius_5px">
		
			<div class="search-box_newlayout transparentBg">
			
				<div class="searchBoxOmsSwitch">
					<div data-tab-number="1">
						Частные клиники
					</div>
					<?php 
                    if (Yii::app()->params['regions'][Yii::app()->session['selectedRegion']]['samozapis']
                    ||
                    (Yii::app()->session['selectedRegion'] === 'moskva' AND $_SERVER['HTTP_HOST'] === 'samozapis-moskva.emportal.ru')
                    ) : 
                    ?>
					<div data-tab-number="2">
						Государственные клиники
					</div>
					<?php endif; ?>
					<div data-tab-number="3">
						Диагностика
					</div>
					<div class="your_city city-in-search">
						<?php
						$this->widget('UserRegion', [
							'regions' => Yii::app()->params['regions'],
							'selectedRegion' => Yii::app()->session['selectedRegion']
						]);
						?>
					</div>
				</div>
			
				<div class="searchBoxTab" data-id="1">
					<div class="searchBoxSpecialtyLabel" style="display: none;"><?=($this->tabNumber == SearchBox::TAB_SERVICE) ? "УСЛУГИ ДИАГНОСТИКИ" : "СПЕЦИАЛЬНОСТЬ ВРАЧА"?></div>
					<div class="searchBoxMetroLabel" style="display: none;">МЕТРО ИЛИ РАЙОН</div>
					<div class="search ef-background patient" style="">
						<?php
							$form = $this->beginWidget('CActiveForm', array(
								'method' => 'get',
								'id' => 'search-doctor-form-new',
								'action' => (Yii::app()->params['isBlogDomen'] ? ((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://emportal.ru' : "")
									. $this->tabNumber==SearchBox::TAB_SERVICE ? '/diagnostics' : ($samozapisSpb ? '/klinika' : '/doctor'),
								'enableAjaxValidation' => false,
								'htmlOptions' => array('enctype' => 'multipart/form-data'),
							));
							?>
							<div>
								<div class="search-row">
									<?php
									if($this->tabNumber == SearchBox::TAB_SERVICE) {
										$this->widget('ESelect2',[
											'name' => CHtml::activeName($model, 'companyServiceId'),
											'data' => $CompanyServiceList,
											'value' => $model->companyServiceId,
											'options' => [
												'placeholder'=>'Услуги диагностики',
												'allowClear'=>true,
											],
											'htmlOptions' => [
												'style' => '
													margin-left: 10px;
													width: 330px;
													height: 62px;
													border-radius: 4px 0px 0px 4px;
													vertical-align: top;
													margin-left: 10px;
													position: absolute;',
												'class' => 'onload_visibility'
											],
										]);
									} else {
										$this->widget('ESelect2',[
											'name'=> CHtml::activeName($model, 'doctorSpecialtyId'),						
											'data' =>  $DoctorSpecialtyList,
											'value' => $model->doctorSpecialtyId,
											'options'=>[
												'placeholder'=>'Специальность врача',
												'allowClear'=>true,							
											],
											'htmlOptions' => [
												'style' => '
													margin-left: 10px;
													width: 330px;
													height: 62px;
													border-radius: 4px 0px 0px 4px;
													vertical-align: top;
													margin-left: 10px;
													position: absolute;',
												'class' => 'onload_visibility'
											]
										]);
									}
									?>
									<?php 
									$this->widget('ESelect2',[
											'name'=> CHtml::activeName($model, 'metroStationId'),
											//'id'=> CHtml::activeId($model, 'metroStationId').'1',
											'value' => $model->metroStationId,
											'data' => [ 
												'Районы города' => $CityDistrictList,
												'Станции метро' => $MetroStationList,
											],
											'options'=>[
													'placeholder'=>'Метро или район',
													'allowClear'=>true,
											],
											'htmlOptions' => [
												'multiple' => ($samozapisSpb ? NULL : 'multiple'),
												'size' => 1,
												'style' => $metroStationSelectStyle,
												'class' => 'onload_visibility '.($samozapisSpb ? "samozapis" : '')
											]
										]);
									
									?>
									<?php echo $form->hiddenField($model, 'sexId'); ?>
									<?php echo $form->hiddenField($model, 'price'); ?>
									<?php echo $form->hiddenField($model, 'onHouse', array('class' => '')); ?>
									<?= $form->hiddenField($model, 'text'); ?>
									<?= $form->hiddenField($model, 'loyaltyProgram'); ?>
									<?php $searchBtnName = ($this->tabNumber == SearchBox::TAB_SERVICE) ? "Найти услугу" : "Найти врача"; ?>
									<button type="submit" value="<?=$searchBtnName?>" class="search-btn"><span><?=$searchBtnName?> </span><span class="icon_search"></span></button>
								</div>
							</div>
						<?php $this->endWidget(); ?>
					</div>
				</div>
				<?php
					$this->render('searchBox/_drop_table',
						array(
							"DoctorSpecialtyList"=>$DoctorSpecialtyList,
							"CompanyServiceList"=>$CompanyServiceList,
							"CityDistrictList"=>$CityDistrictList,
							"MetroStationList"=>$MetroStationList,
						)
					);
				?>
				<?php
				/*
					if (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) {
						$this->render('searchBox/_oms-filter_block', array('model'=>$model, 'form'=>$form));
					}
				*/
				?>
			</div>
		</div>
		<!-- <div class="btn-instruction_container">
			<a class="btn-instruction" title="Инструкция по записи" href="#instruction">Инструкция по записи</a>
		</div> -->
		<?php if(Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']): ?>
		<div class="btn-instruction_container">
			<a class="btn-instruction" style="width: 300px !important;" title="Отменить запись" href="/doctor?cancelAppointment=1">Посмотреть или отменить Мои записи</a>
		</div>
		<?php endif; ?>
	</div>
<!--# block name="icons_bottom_stats" -->
<?= (new CController('context'))->renderPartial("//layouts/ssi_icons_bottom_stats", []); ?>
<!--# endblock -->
<!--# include virtual="/index.php?r=ajax/renderPartial&name=//layouts/ssi_icons_bottom_stats" stub="icons_bottom_stats" -->
                        
</div>

<script>	
	$(function() {
		var samozapisSubdomain = "<?=urlencode(Yii::app()->params['regions'][Yii::app()->session['selectedRegion']]['samozapisSubdomain'])?>";
		var selectedRegion = "<?=urlencode(Yii::app()->session['selectedRegion'])?>";
		function showTab(tabNum) {
			$("#select2-drop-mask").click();
			
			var tabs = $('.searchBoxOmsSwitch div');
			for(var i = 0; i < tabs.length; i++) {
				var tab = tabs.get(i);
				$(tab).removeClass('active');
			}
			$('.searchBoxOmsSwitch div[data-tab-number="'+tabNum+'"]').addClass('active');
		}
		
		var selectedTabNum = 1,
			$buttons = $('.searchBoxOmsSwitch div'),
			protocol = document.location.protocol + '//';
		var currentTabNum = '<?= $this->tabNumber ?>';
		
		$buttons.each(function() {
			
			var $button = $(this);
			$button.click(function() {
				selectedTabNum = $button.attr('data-tab-number');
				if(selectedTabNum != currentTabNum) {
					var domainsArr = document.domain.split('.');
					for(var i in domainsArr) {
						domainsArr[i] = domainsArr[i].toLowerCase();
						if(domainsArr[i] == samozapisSubdomain) domainsArr.splice(i,1);
						else if(domainsArr[i] == selectedRegion) domainsArr.splice(i,1);
					}
					switch(selectedTabNum) {
					case '1':
						if(selectedRegion != 'spb') domainsArr.unshift(selectedRegion);
						window.location.href = protocol + domainsArr.join('.');
						break;
					case '2':
						domainsArr.unshift(samozapisSubdomain);
						window.location.href = protocol + domainsArr.join('.');
						break;
					case '3':
						if(selectedRegion != 'spb') domainsArr.unshift(selectedRegion);
						window.location.href = protocol + domainsArr.join('.') + "/?diagnostics";
						break;
					default:
						console.log(selectedTabNum);
						break;
					}
				}
			});
		});
		
		showTab(currentTabNum);
	});
</script>
