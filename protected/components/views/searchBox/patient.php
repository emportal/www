<?php
/* @var $model Search */

	Yii::app()->clientScript->registerScript('search-service', '');
	$this->tab = isset($tabs[$this->tab]) ? $this->tab : key($tabs);
	$isVkApp = Yii::app()->request->getParam('vk');
?>
<style>
	input#s2id_autogen4.select2-input.select2-default {
		color: rgb(51,51,51) !important;  
	}
</style>
<div class="SECTION SEARCH_BOX_HEAD">
	<div class="main_column">
		<a href="<?=Yii::app()->params['isBlogDomen']?"http://emportal.ru":""?>/" title="Единый Медицинский Портал"><img src="/images/newLayouts/LOGO-MAIN-mini<?=(!in_array(Yii::app()->session['selectedRegion'], ['spb','moskva', 'nizhny-novgorod', 'ekaterinburg', 'perm', 'voronezh'])) ? "-beta" : ""?>.png?v=002" alt="Единый Медицинский Портал" class="LOGO_MAIN-mini"></a>
		<?php $this->controller->widget('Breadcrumbs', array(
				'homeLink'=> (Yii::app()->params['isBlogDomen'] ? CHtml::link('emportal.ru', "http://emportal.ru") : CHtml::link('Главная', "/")),
				'links' => $this->controller->breadcrumbs,
				'htmlOptions' => [ 'class' => $isVkApp ? 'crumbs' : 'mobilecrumbs', 'style' => 'display:none;' ],
			)
		); ?>
		<div id="search-tab-2" class="search-box">			
			<?php
			$form = $this->beginWidget('CActiveForm', array(
				'method' => 'get',
				'id' => 'search-doctor-form',
				'action' => (Yii::app()->params['isBlogDomen'] ? ((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://emportal.ru' : "")
									. '/doctor',
				'enableAjaxValidation' => false,
				'htmlOptions' => array('enctype' => 'multipart/form-data'),
			));
			?>
			<fieldset>
				<div class="search-row">
					<?php if (!Yii::app()->params['samozapis']) : ?>
						<p class="slogan">Запись на приём к врачу в частные клиники</p>
					<?php else : ?>
						<p class="slogan">Самозапись на приём к врачу по полису ОМС в <?= City::model()->getSelectedCity()->locative ?></p>
					<?php endif; ?>
					<?php if(!Yii::app()->params['isBlogDomen']): ?>
					<div class="your_city city-in-search in-page-search">
						<?php
							$this->widget('UserRegion', [
								'regions' => Yii::app()->params['regions'],
								'selectedRegion' => Yii::app()->session['selectedRegion']
							]);
						?>
					</div>
					<?php endif; ?>
					<?php						
						$this->widget('ESelect2',[
							'name' => CHtml::activeName($model, 'doctorSpecialtyId'),						
							'data' =>  $DoctorSpecialtyList,
							'value' => $model->doctorSpecialtyId,
							'options'=>[
								'placeholder'=>'Специальность врача',
								'allowClear'=>true,							
							],
							'htmlOptions' => [
								'style' => '
									width: 242px;
									height: 40px;
									vertical-align: top;
									border-radius: 4px 0px 0px 4px;
									position: absolute;'
							]
							
						]);
					?>
					<?php
						$this->widget('ESelect2',[						
							'name'=> CHtml::activeName($model, 'metroStationId'),
							'value' => $model->metroStationId,
							'data' => [
								'Районы города' => $CityDistrictList,
								'Станции метро' => $MetroStationList,
							],
							'options'=>[
									'placeholder'=>'Любое место',
									'allowClear'=>true,
							],
							'htmlOptions' => [
								'multiple' => (Yii::app()->params["samozapis"]) ? NULL : 'multiple',
								'size' => 1,
								'style' => "
									width: 246px;
									height: 40px;
									vertical-align: top;
									margin-left: 242px;
									position: absolute;
								",
							]
						]);
					?>
					<?= $form->hiddenField($model, 'addressLink'); ?>
					<?= $form->hiddenField($model, 'sexId'); ?>
					<?= $form->hiddenField($model, 'clinicLinkUrl'); ?>
					<?= $form->hiddenField($model, 'price'); ?>
					<div class="search-doctor-filter ajax_filter" style="display: none;">
					<?= $form->hiddenField($model, 'priceMin', array('class'=>'offscreen')); ?>
					<?= $form->hiddenField($model, 'priceMax', array('class'=>'offscreen')); ?>
					</div>
					<?= $form->hiddenField($model, 'onHouse'); ?>
					<?= $form->hiddenField($model, 'text'); ?>
					<?= $form->hiddenField($model, 'loyaltyProgram'); ?>
					<?= $form->hiddenField($model, 'offline'); ?>
					<?= $form->hiddenField($model, 'sort'); ?>
					<input type="hidden" id="Search_doctorSpecialty" name="Search[doctorSpecialty]">
					<button type="submit" value="Найти врача" class="search-btn"><span>Найти врача </span><span class="icon_search_mini"></span></button>
				</div>
			</fieldset>
			<?php
				$this->render('searchBox/_drop_table',
					array(
						"model"=>$model,
						"DoctorSpecialtyList"=>$DoctorSpecialtyList,
						"CityDistrictList"=>$CityDistrictList,
						"MetroStationList"=>$MetroStationList
					)
				);
			?>
			<?php
				if (Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) {
					$this->render('searchBox/_oms-filter_block', array('model'=>$model, 'form'=>$form));
				}
			?>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>