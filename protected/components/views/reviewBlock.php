				<div class="review_block <?= $options['reviewCssClass'] ?>">
					<p class="review_block_label"><span class="<?= $options['reviewIconCssClass'] ?>"></span><?= $options['reviewTitle'] ?></p>
					<div class="review_item">
						<a href="<?= $options['reviewLink'] ?>" class="review_title"><?= CHtml::encode($options['reviewName']) ?></a>
						<p class="review_text"><?= $options['reviewText'] ?></p>
						<div class="author_block">
							<img class="author_avatar" src="<?= $options['reviewUserAvatar'] ?>">
							<div class="author_sign">
								<div class="author_name"><?= CHtml::encode($options['review']['userName']) ?></div>
								<div class="date-time"><?= RuDate::post($options['review']['createDate']) ?></div>
							</div>
						</div>
					</div>
				</div>