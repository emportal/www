<div class="full-wisth-menu my-cabinet">
	<ul>
		<li><span class="u-name"><?=Yii::app()->user->model->attributes['name'] ?></span></li>
		<li><a href="/user/profile"><span class="ico-profile ico-el"></span>ПРОФИЛЬ</a></li>
		<li><a href="/user/registry"><span class="ico-registry ico-el"></span>РЕГИСТРАТУРА</a></li>
		<li><a href="/user/comments"><span class="ico-reviews ico-el"></span>МОИ ОТЗЫВЫ</a></li>
		<li><a href="/user/bonuses"><span class="ico-bonuses ico-el"></span>МОИ БОНУСЫ: <?=Yii::app()->user->model->attributes['balance']; ?> БАЛЛОВ</a></li>
		<?php
		foreach(Yii::app()->user->model->rights as $right):
			if ($right->zRightId == RightType::ACCESS_NEW_ADMIN):
			?>
				<li><a href="/newAdmin" style="padding-left: 2px;padding-right: 2px;"><span</span>Админка менеджера</a></li>
			<?php
			break;
			endif;
		endforeach;
		?>
		<?php
		foreach(Yii::app()->user->model->rights as $right):
			if ($right->zRightId == RightType::IS_ADMIN_IN_ADDRESS || $right->zRightId == RightType::IS_OWNER_OF_ADDRESS):
			?>
				<li><a href="/user/administrate" style="padding-left: 2px;padding-right: 2px;"><span></span>Моя клиника</a></li>
			<?php
			break;
			endif;
		endforeach;
		?>
		<li><a href="/logout?returnUrl=<?= urlencode(Yii::app()->request->url) ?>" class="exit-el">ВЫЙТИ</a></li>
		<li></li>
	</ul>
</div>