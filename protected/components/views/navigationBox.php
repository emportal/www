<? /* @var $this NavigationBox */ ?>
<div class="navigation">
	<div class="navigation-inner">
		<div class="navigation-center">
			<div class="navigation-box">
				<div class="navigation-image">
					<?= CHtml::image($this->controller->assetsImageUrl . '/pic-useful.png', 'Полезное') ?>
				</div><!-- /navigation-image -->
				<strong class="navigation-title"><span>ПОЛЕЗНОЕ</span></strong>
				<ul>
					<li><a href="<?= $this->controller->createUrl('/article') ?>" title="Статьи">Статьи</a></li>					
					<li><a href="<?= $this->controller->createUrl('/news/') ?>" title="Новости">Новости</a></li>												
					<li><a href="<?= $this->controller->createUrl('/search/doctor') ?>">Все доктора</a></li>
					<li><a href="<?= $this->controller->createUrl('/search/clinic') ?>">Все клиники</a></li>
					<li><a href="<?= $this->controller->createUrl('/guide.html') ?>" title="Новости">Гид по сайту</a></li>
					<!--<li><a href="<?= $this->controller->createUrl('/search/' . Search::S_BEAUTY) ?>" title="Салоны красоты">Салоны красоты</a></li>
					<li><a href="<?= $this->controller->createUrl('/search/' . Search::S_INSURANCE) ?>" title="Страховые компании">Страховые компании</a></li>				
					<li><a href="<?= $this->controller->createUrl('/search/' . Search::S_TOURISM) ?>" title="Тур. фирмы и тур. операторы с оздоровительными турами">Тур. фирмы и тур. операторы с оздоровительными турами</a></li>-->
				</ul>
				<!--				<ul>
									<li><a href="#" title="Аптеки">Аптеки</a></li>
									<li><a href="#" title="Неотложная помощь">Неотложная помощь</a></li>
									<li><a href="#" title="Страховые компании">Страховые компании</a></li>
									<li><a href="#" title="Вопросы онлайн">Вопросы онлайн</a></li>
									<li><a href="#" title="Каталог медучреждений">Каталог медучреждений</a></li>
									<li><a href="#" title="Юридические вопросы">Юридические вопросы</a></li>
									<li><a href="#" title="Статьи">Статьи</a></li>
								</ul>-->
			</div><!-- /navigation-box -->
			<div class="navigation-box border">
				<div class="navigation-image">
					<?= CHtml::image($this->controller->assetsImageUrl . '/pic-enc.png', 'Энциклопедии') ?>
				</div><!-- /navigation-image -->
				<strong class="navigation-title"><span>ЭНЦИКЛОПЕДИИ</span></strong>
				<ul>
					<li><a href="<?= $this->controller->createUrl('/handbook/' . Search::S_DISEASE) ?>" title="Болезни">Болезни</a></li>
					<!--<li><a href="<?= $this->controller->createUrl('/search/' . Search::S_MEDICAMENT) ?>" title="Лекарства">Лекарства</a></li>-->
					<li><a href="<?= $this->controller->createUrl('/handbook/' . Search::S_DOCTORSPECIALTY) ?>" title="Специальности врачей">Специальности врачей</a></li>
					<!--<li><a href="#" title="Виртуальный анатомический атлас">Виртуальный анатомический атлас</a></li>-->
					<li><a href="<?= $this->controller->createUrl('/handbook/' . Search::S_COMPANYACTIVITY) ?>" title="Профили деятельности клиник">Профили деятельности клиник</a></li>
				</ul>
			</div><!-- /navigation-box -->
			<div class="navigation-box">
				<div class="navigation-image">
					<?= CHtml::image($this->controller->assetsImageUrl . '/pic-about.png', 'О проекте') ?>
				</div><!-- /navigation-image -->
				<strong class="navigation-title"><span>О ПРОЕКТЕ</span></strong>
				<ul>					
					<li><a href="<?= $this->controller->createUrl('/about_us.html') ?>" title="О сайте">О нас</a></li>
					<li><a href="<?= $this->controller->createUrl('/team.html') ?>" title="Команда">Команда</a></li>	
					<!--<li><a href="<?= $this->controller->createUrl('site/page', array('view' => 'project')) ?>" title="О проекте">О проекте</a></li>-->					
					<li><a href="<?= $this->controller->createUrl('site/gallery') ?>" title="Фотогалерея">Фотогалерея</a></li>
					<li><a href="<?= $this->controller->createUrl('/rating.html') ?>" title="Рейтинг">Рейтинг на сайте</a></li>
					<!--<li><a href="<?= $this->controller->createUrl('/ads.html') ?>" title="Размещение рекламы">Размещение рекламы</a></li>-->
					<li><a href="<?= $this->controller->createUrl('site/page', array('view' => 'terms_of_use')) ?>" title="Юридическая информация">Юридическая информация</a></li>	
					<li><a href="<?= $this->controller->createUrl('site/feedbackApp') ?>" title="Страница поддержки приложения">Страница поддержки приложения</a></li>
									
					
					<!--<li><a href="<?= $this->controller->createUrl('site/page', array('view' => 'privacy_policy')) ?>" title="Политика конфиденциальности">Политика конфиденциальности</a></li>-->
				</ul>
			</div><!-- /navigation-box -->
		</div><!-- /navigation-center -->
	</div><!-- /navigation-inner -->

</div><!-- /navigation -->
<div class="foreign_links">
	
	<?php 
		if(strpos($_SERVER['REQUEST_URI'],"/user") === FALSE):
	?>
		<div class="share_buttons">				
			<script type="text/javascript" src="//yandex.st/share/share.js"	charset="utf-8"></script>
			<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter" ></div> 		
			<div class="text">Поделиться: </div>
		</div>
	<?php endif;?>
	<div class="both"></div>
</div>
<!--<div class="for-partners">
	<p>Мы заинтересованы в увеличении количества новых партнеров. Если вы являетесь представителем медицинского учреждения, салона красоты, страховой компании, туристической фирмы или туристического оператора с оздоровительными турами и хотите участвовать в нашем проекте - <?php echo CHtml::link('пожалуйста, заполните анкету', array('site/contact')); ?>.</p>
</div>-->