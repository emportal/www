<? /* @var $this UserCity */ ?>
	<span class="icon_pin"></span>
	<span class="sel-city">Ваш город
		<select id="citySelector" class="select-city onload_visibility">
			<?php
			foreach($regions as $region) :
				$selected = '';
				if ($selectedRegion == $region['subdomain'])
					$selected = ' selected';
			?>
			<option value="<?= CHtml::encode($region['subdomain']) ?>"<?= $selected ?>><?= CHtml::encode($region['name']) ?></option>
			<?php endforeach; ?>
		</select>
	</span>