<div class="f-left my-cabinet">
	<span class="name">
		<?= CHtml::encode(Yii::app()->user->shortname) ?>
	</span>
	<a href="/user">
		<span class="icon-cabinet"></span>
		<span class="label">Личный кабинет</span>
	</a>
</div>				
<div class="block f-left logout">
	<a href="/logout?returnUrl=<?= urlencode(Yii::app()->request->url) ?>" class="btn-blue">Выход</a>
</div>