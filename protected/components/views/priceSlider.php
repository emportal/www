<? /* @var $this PriceSlider */ ?>
			<style>
				.price_slider_container {
					height: 38px;
					width: 470px;
					background: white;
					border: 1px solid #dcdcdc;
					border-radius: 3px;
				}
				.noUi-horizontal .noUi-handle {
					width: 16px;
					height: 17px;
					left: -8px;
					top: -8px;
				}
				.noUi-handle:before {
					content: "";
					display: block;
					position: absolute;
					width: 1px;
					height: 11px;
					background: #E8E7E6;
					left: 14px;
					top: 0px;
				}
				.noUi-handle::after {
					left: 4px;
					top: 2px;
					width: 6px;
					height: 11px;
				}
				.price_slider {
					width: 335px;
					top: 18px;
					left: 60px;
					height: 2px;
					border: 0;
				}
				.price_left_value {
					position: relative;
					top: 9px;
					margin-left: 8px;
					text-align: left;
					width: 48px;
					float: left;
					font-size: 15px;
					color: #606060;
				}
				.price_right_value {
					position: relative;
					top: 9px;
					margin-right: 8px;
					text-align: right;
					width: 60px;
					float: right;
					font-size: 15px;
					color: #606060;
				}
				.noUi-connect {
					background: #04b0cf;
				}
				.noUi-background {
					  background: #dde0e0;
				}
				.price_slider:before {
				    content: "";
				    display: block;
				    position: absolute;
				    height: 12px;
				    width: 100%;
				    background: #f5f5f5;
				    left: 0px;
				    top: -5px;
				}
			</style>
			<?php
				$path = Yii::getPathOfAlias('shared') . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'noUiSlider' . DIRECTORY_SEPARATOR;
				$urlCSS = Yii::app()->assetManager->publish($path . 'jquery.nouislider.min.css');
				$urlJS = Yii::app()->assetManager->publish($path . 'jquery.nouislider.all.js');
				Yii::app()->clientScript->registerCssFile($urlCSS);
				Yii::app()->clientScript->registerScriptFile($urlJS, CClientScript::POS_END);
			?>
			<div class="price_slider_container none-in-responsive">
				<div id="<?= $model->id ?>" class="noUi-target noUi-ltr noUi-horizontal noUi-background price_slider"></div>
				<div id="<?= $model->id ?>_left_value" class="price_left_value"></div>
				<div id="<?= $model->id ?>_right_value" class="price_right_value"></div>
			</div>
			<div class="price_slider_container_mobile" style="display:none;">
				<input id="search-doctor-form_priceMin" class="custom-text" autocomplete="off" placeholder="От 0" value="<?= (isset(Yii::app()->request->getParam($model->name)[$model->inputMin]) && is_numeric(Yii::app()->request->getParam($model->name)[$model->inputMin])) ? intval(Yii::app()->request->getParam('Search')['priceMin']) : '' ?>" type="text" name="Doctor[priceMin]" style="
				    width: 45%;
				    float: left;
				">
				<input id="search-doctor-form_priceMax" class="custom-text" autocomplete="off" placeholder="До 5000" value="<?= (isset(Yii::app()->request->getParam($model->name)[$model->inputMax]) && is_numeric(Yii::app()->request->getParam($model->name)[$model->inputMax])) ? intval(Yii::app()->request->getParam('Search')['priceMax']) : '' ?>" type="text" name="Doctor[priceMax]" style="
				    width: 45%;
				    float: right;
				">
			</div>
			<script>
			$( document ).ready(function() {
				var min_price = 0;
				var max_price = 10000;
				var get_price_min = <?= (isset(Yii::app()->request->getParam($model->name)[$model->inputMin]) && is_numeric(Yii::app()->request->getParam($model->name)[$model->inputMin])) ? intval(Yii::app()->request->getParam('Search')['priceMin']) : 'min_price' ?>;
				var get_price_max = <?= (isset(Yii::app()->request->getParam($model->name)[$model->inputMax]) && is_numeric(Yii::app()->request->getParam($model->name)[$model->inputMax])) ? intval(Yii::app()->request->getParam('Search')['priceMax']) : 'max_price' ?>;

			    $("#<?= $model->id ?>").noUiSlider({
					start: [parseInt(get_price_min), parseInt(get_price_max)],
					step: 100,
					connect: true,
					range: {
						'min': min_price,
						'max': max_price
					}
				}, true);
				function setPriceValue(obj,type){
						var values = $(obj).val();
						var lval = parseInt(values[0]);
						var rval = parseInt(values[1]);
						if(type == "change") {
							<?php if(!$model->formId):?>
							 var form = $('body');
							<?php else: ?>
							 var form = $('#<?= $model->formId ?>').first();
							<?php endif; ?>	
							form.find("#<?= $model->name."_".$model->inputMin ?>").val(lval);
							if(rval >= max_price) {
								form.find("#<?= $model->name."_".$model->inputMax ?>").val(null);
							} else  {
								if(rval <= 0) {
									form.find("#<?= $model->name."_".$model->inputMax ?>").val("00");
								} else {
									form.find("#<?= $model->name."_".$model->inputMax ?>").val(rval);
								}
							}
							form.find("#<?= $model->name."_".$model->inputMax ?>").change();
							//refreshSearch(0, "#"+form.attr('id'), true, '<?= $model->type ?>');
						};
						$("#search-doctor-form_priceMin").val(lval);
						$("#search-doctor-form_priceMax").val(rval);
						if(lval == min_price) { $("#search-doctor-form_priceMin").val(null); }
						if(rval == max_price) { $("#search-doctor-form_priceMax").val(null); }
						if(rval >= max_price) rval = max_price+'+';
						$("#<?= $model->id ?>_left_value").text(lval);
						$("#<?= $model->id ?>_right_value").text(rval);
				};
				$("#<?= $model->id ?>").on({
					slide: function() { setPriceValue(this,"slide"); },
					set: function() { setPriceValue(this,"set"); },
					change: function() { setPriceValue(this,"change"); },
				});
				setPriceValue($("#<?= $model->id ?>")[0]);
			});
			</script>