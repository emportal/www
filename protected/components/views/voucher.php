<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Талон #<?= $model->number ?></title>
<style>
body {
	background: lightgray;
	font-size: 12px;
    font: 12px Tahoma;
    line-height: 1.4em;
}
.voucher-table {
}
.voucher-leftCol {
}
.voucher-col {
}
.voucher-row {
	/* padding: 12px 8px 6px 6px; */
    padding: 8px;
	vertical-align: top;
}
.voucher-row.title {
    font-size: 1.5em;
    line-height: 1.4em;
}
.voucher-left-cell {
    width: 110px;
    border-right: 1px solid gray;
    border-bottom: 1px solid gray;
}
</style>
</head>
<body>
<div style="margin:auto; width: 560px; border: 1px solid gray; border-radius: 20px; background: white; padding-bottom: 10px;">
	<table class="voucher-table" style="width: 100%; border-collapse: collapse;">
		<tbody>
			<tr class="voucher-col">
			<?php if($model->leftColumn): ?>
				<td class="voucher-row voucher-left-cell">
					<img src="/images/newLayouts/LOGO-VOUCHER.png" alt="Единый Медицинский Портал" style="width: 110px;">
					<div>
                        <a href="https://emportal.ru">https://emportal.ru</a>
                    </div>
				</td>
			<?php endif; ?>
				<td class="voucher-row title" style="border-bottom: 1px solid gray;">
					Талон #<?= $model->number ?>
                    <br><?= $model->name ?>
				</td>
			</tr>
			<tr class="voucher-col">
			<?php if($model->leftColumn): ?>
				<td class="voucher-row voucher-left-cell">
					<?php
					if($model->companyLogo) {
						echo "<img src='" . $model->companyLogo . "' style='max-width: 110px;'>";
					}
					?>
				</td>
			<?php endif; ?>
				<td class="voucher-row" style="border-bottom: 1px solid gray;">
					<div>Клиника: <?= $model->companyName ?></div>
					<div>Адрес: <?= $model->addressName ?></div>
				</td>
			</tr>
			<tr class="voucher-col">
			<?php if($model->leftColumn): ?>
				<td class="voucher-row voucher-left-cell">
					<?php
					if($model->doctorPhoto) {
						echo "<img src='" . $model->doctorPhoto . "' style='max-width: 110px;'>";
					}
					?>
				</td>
			<?php endif; ?>
				<td class="voucher-row" style="border-bottom: 1px solid gray;">
					<div>Время приёма: <?= $model->plannedTime ?></div>
					<div>
						<?php
						if($model->doctorName) {
							echo "Врач: " . $model->doctorName . (is_array($model->doctorSpecialty) ? ", " . implode(", ", $model->doctorSpecialty) : "");
						}
						?>
					</div>
					<div>-</div>
					<div>Стоимость: <?= $model->price ?> </div>
					<div><?= $model->prePaid ?></div>
					<div><b><?= $model->totalPrice ?></b></div>
				</td>
			</tr>
			<tr class="voucher-col">
			<?php if($model->leftColumn): ?>
				<td class="voucher-row voucher-left-cell"> </td>
			<?php endif; ?>
				<td class="voucher-row" style="border-bottom: 1px solid gray;">
					<div>ФИО пациента: <?= $model->userName ?></div>
					<div>Дата рождения: <?= $model->userBirthday ?></div>
					<div>Телефон пациента: <?= $model->userTelefon ?></div>
				</td>
			</tr>
			<tr class="voucher-col">
			<?php if($model->leftColumn): ?>
				<td class="voucher-row"> </td>
			<?php endif; ?>
				<td class="voucher-row">
				<?php if(!empty($appointment->prePaid)): ?>
					Примечание: <b>Обязательным условием</b>
					получения услуги по указанной цене с учётом
					указанной скидки является предъявление в
					клинике данного талона в распечатанном или
					электронном виде. Без предъявления талона
					скидка может быть не предоставлена.
				<?php endif; ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</body>
</html>