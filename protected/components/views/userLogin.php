<? /* @var $this UserLogin */ ?>
<div class="entrance">
	<a href="/adminClinic/default/login" class="for-clinics">
		<span class="icon_clinic"></span>
		<span>Вход для клиник</span>
	</a>
	<span>/</span>
	<span class="login">
		<a href="/login" onclick="return false;" class="login-btn">
			<span>Вход для пациентов</span>
		</a>
	</span>
</div>
<div class="register" style="display: none;">
	<a href="/register" class="register-btn">
		<span class="icon_register"></span><span class="label">Зарегистрироваться</span>
	</a>
</div>

<div class="assets" style="display: none;">
<?php $this->render('userLoginForm',array('model'=>$model, 'id'=>$id)); ?>
</div>
<div style="display: none;">
</div>