<div class="info">
	<div class="info-inner">
		<strong class="info-title">Быстрый переход</strong>
		<table class="info-table">
			<tr><td style="padding: 3px 0;"><a href="<?= $this->controller->createUrl('/search/'.Search::S_DISEASE)?>" title="Болезни">Болезни</a></td></tr>
			<!--<tr><td style="padding: 3px 0;"><a href="<?= $this->controller->createUrl('/search/'.Search::S_MEDICAMENT)?>" title="Лекарства">Лекарства</a></td></tr>-->
			<tr><td style="padding: 3px 0;"><a href="<?= $this->controller->createUrl('/search/'.Search::S_DOCTORSPECIALTY)?>" title="Специальности врачей">Специальности врачей</a></td></tr>
			<tr><td style="padding: 3px 0;"><a href="<?= $this->controller->createUrl('/search/'.Search::S_COMPANYACTIVITY)?>" title="Профили деятельности клиник">Профили деятельности клиник</a></td></tr>
		</table>
	</div><!-- /information-inner -->
</div><!-- /information -->