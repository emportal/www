<noindex>
<!-- email block -->
<div id="<?= $blockId ?>" class="">
	<input id="emailSubscriptionInput" class="emailSubInput<?= $inputClasses ?>" type="text" <?= ($readonlyEmail) ? 'readonly' : '' ?> value="<?= $fixedEmail ?>" placeholder="Ваш e-mail" <?= $emailAttributesStr ?>>
	<a id="emailSubscriptionSubmitButton" href="#" class="btn-green btn-compact" onclick="return false;">ок</a>
</div>
<script>
var emailBlock_<?= $blockId ?> = {}; //visible globally
$(function() {
	var options = {
		'id': '<?= $blockId ?>'
	};
	emailBlock_<?= $blockId ?> = new EmailBlockConstructor(options);
});
</script>
<!-- //email block -->
</noindex>