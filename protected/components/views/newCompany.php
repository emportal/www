<style>
#newCompany-form, #newCompany-form h1, #newCompany-form h3 {
	color: #fff;
	margin-top: 30px;
}
#newCompany-form .data-table {
	width: 100%;
}
#newCompany-form input[readonly="readonly"] {
	background-color: lightgray !important;
	border: 1px solid gray;
}
#newCompany-form input:not([type="submit"]), #newCompany-form select, #newCompany-form textarea {
	width: 350px;
}
#newCompany-form .data-table>div>div {
	border: none;
}
#newCompany-form div.errorSummary {
	background-color: #FF7070;
	padding: 10px;
	margin: -75px 50px 25px 0px;
}
.custom-text.error {
	border: 5px solid #FF7070 !important;
}
</style>
<span id="signup_form"></span>
<!-- <div class="SECTION">
	<div class="wrapper vpadding20">-->
		<?php
		$form = $this->beginWidget('CActiveForm',[
			'id' => 'newCompany-form',
			'enableAjaxValidation' => false,
			'action' => Yii::app()->getBaseUrl() . '#newCompany-form',
		]);
		?>
		<?php if ($form->errorSummary($userModel) || $form->errorSummary($companyModel) || $form->errorSummary($addressModel)) :
			echo '<div">';
			echo $form->errorSummary($userModel);
			echo $form->errorSummary($companyModel);
			echo $form->errorSummary($addressModel);
			echo '<div>';
		endif; 
		?>
		<h2 class="header">Регистрация клиники на портале</h2>
		<div class="vmargin20">
			<?php
			$readonly = ($userModel->isNewRecord) ? '' : 'readonly';
			?>
			<h3>Ваши данные</h3>
			<div class="data-table vmargin10">
				<div>
					<div class="w130">Имя</div>
					<div><?= $form->textField($userModel, 'name', ['readonly' => $readonly, 'class' => 'custom-text', 'placeholder' => 'Юлия Юрьевна Петрушкина']) ?></div>
				</div>
				<div>
					<div>Должность</div>
					<div><input type="text" name="lprPosition" class="custom-text" placeholder="Менеджер по маркетингу"></div>
				</div>
				<div>
					<div>Email</div>
					<div><?= $form->textField($userModel, 'email', ['readonly' => $readonly, 'class' => 'custom-text', 'placeholder' => 'address@clinic.ru']) ?></div>
				</div>
				<div>
					<div>Телефон</div>
					<div><?= $form->textField($userModel, 'telefon', ['readonly' => $readonly, 'class' => 'custom-text', 'id' => 'phoneInput', 'placeholder' => '+7 (999) 999-99-99']) ?></div>
				</div>
				<?php if (Yii::app()->user->isGuest) : ?> 
				<div>
					<div>Пароль</div>
					<div><?= $form->textField($userModel, 'password', ['class' => 'custom-text', 'placeholder' => 'Не менее 6 символов, буквы + цифры']) ?></div>
				</div>
				<?php endif; ?>
				<div>
					<div>Я согласен(-на) на <br>обработку личных данных</div>
					<div style="text-align: left;"><?= $form->checkBox($userModel, 'confirmation', array('class' => 'f-left wauto', 'style' => 'width: auto;')) ?></div>
				</div>
			</div>
			<h3>Компания</h3>
			<div class="data-table vmargin10">
				<div>
					<div class="w130">Название</div>
					<div><?= $form->textField($companyModel, 'name', ['placeholder' => 'Например, "Американская медицинская клиника"', 'class' => 'custom-text']) ?></div>
				</div>
				<div>
					<div>Тип</div>
					<div>
						<?php
						echo $form->dropDownList($companyModel, 'categoryId', CHtml::listData(Category::model()->findAll(['condition'=>'id="a3969945e0f59fa84e0e7740c2e8cc87"', 'order'=>'name ASC']),'id','name'), ['class' => 'displaynone']);
						$condition = [
							'эстетическая',
							'центр',
							'клиника',
							'стомат',
							'медицинский центр',
						];
						$conditionStr = 'name LIKE "%' . implode('%" OR name LIKE "%', $condition) . '%"';
						echo $form->dropDownList($companyModel,'companyTypeId',CHtml::listData(CompanyType::model()->findAll(['condition'=>$conditionStr,'order'=>'name ASC']),'id','name'), ['class' => 'custom-text']);
						?>
					</div>
				</div>
				<div>
					<div>Сайт (если есть)</div>
					<div><input type="text" name="website" class="custom-text" placeholder="clinic.ru"></div>
				</div>
			</div>
			<h3>Адрес</h3>
			<div class="data-table vmargin10">
				<div>
					<div class="w130">Город</div>
					<div>
						<?= $form->dropDownList(
							$addressModel,
							'cityId',
							CHtml::listData(
								City::model()->findAll([
									'select' => 'id, name',
									'order' => 'population DESC',
									'limit' => 150,
									//'condition' => 'countryId = \'534bd8ae-e0d4-11e1-89b3-e840f2aca94f\' AND subdomain IN ("voronezh")'
                                    //'condition' => 'countryId = \'534bd8ae-e0d4-11e1-89b3-e840f2aca94f\' AND subdomain IS NOT NULL'
                                    'condition' => 'subdomain IN ("' . implode('","', City::model()->selfRegisterCitySubdomains) . '")'
								]),
								'id',
								'name'
							),
							['class' => 'custom-text']
						); ?>
					</div>
				</div>
				<div>
					<div>Улица</div>
					<div><?= $form->textField($addressModel, 'street', ['class' => 'custom-text', 'placeholder' => 'пр. Художников']);?></div>
				</div>
				<div>
					<div>Номер дома</div>
					<div><?= $form->textField($addressModel, 'houseNumber', ['class' => 'custom-text', 'placeholder' => '28']);?></div>
				</div>
			</div>
			<!--<input type="submit" class="btn-green" value="Далее">-->
			<button id="newCompany-form-submit-button" class="landing_btn-start" style="margin: 40px 48px 0 0; float: right;" type="submit">Добавить клинику на Портал</button>
			<div id="newCompany-form-loading-indicator" class="displaynone">Идет обработка данных...</div>
			<script>
				$(function(){
					$('#newCompany-form').on('submit', function() {
						$('#newCompany-form-submit-button').html('Идет обработка данных формы...');
						$('#newCompany-form-submit-button').attr('disabled', 'true');
						//$('#newCompany-form-loading-indicator').show();
					});
				});
			</script>
			<?php $this->endWidget(); ?>
		</div>
	<!--</div>
</div> -->