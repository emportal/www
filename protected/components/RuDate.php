<?php

class RuDate extends CComponent {
	public static $weekArr = [
			0 => "Вс",
			1 => "Пн",
			2 => "Вт",
			3 => "Ср",
			4 => "Чт",
			5 => "Пт",
			6 => "Сб"
	];
	
	public static $month = [
	        1	=> 'Января',
	        2	=> 'Февраля',
	        3	=> 'Марта',
	        4	=> 'Апреля',
	        5	=> 'Мая',
	        6	=> 'Июня',
	        7	=> 'Июля',
	        8	=> 'Августа',
	        9	=> 'Сентября',
	        10	=> 'Октября',
	        11	=> 'Ноября',
	        12	=> 'Декабря'
    ];

	static function post($datetime, $withTime=true, $short=true) {		
		try {
	        $timestamp = strtotime($datetime);
			$result = date('d', $timestamp) .' '. self::$month[intval(date('m', $timestamp))];
			if(date('Y', $timestamp) !== date('Y') || !$short) $result .= date(' Y', $timestamp);
			if($withTime) $result .= date(' в H:i', $timestamp);
			return ltrim($result, '0');
		} catch (Exception $e) {
			return $datetime;
		}
	}

	static function dayOfWeek($datetime) {
		$timestamp = strtotime($datetime);
	    $published = date('d.m.Y', $timestamp);
	    $dateArr = explode('.', $published);
		return self::$weekArr[date('w', mktime(0, 0, 0, $dateArr[1], $dateArr[0], $dateArr[2]))];
	}
}

?>