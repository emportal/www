<?php

class FileMaker extends CComponent {
	
    public static function getLegalData()
    {
        return (object) [
            'inn' => '7804495268',
            'kpp' => '780401001',
            'name' => 'ООО “ПИК”',
            'nameFull' => 'Общество с ограниченной ответственностью «Первая информационная компания»',
            'bik' => '044030790',
            'bankName' => 'ПАО "Банк "Санкт-Петербург"',
            'accountNum1' => '30101810900000000790', //корреспондентский счет 
            'accountNum2' => '40702810490200001192', //расчетный счет 
            'address' => 'Свердловская наб, дом № 4, корпус А, г. Санкт-Петербург',
            'postalCode' => '195009',
            'phone' => '337-61-70',
            'ceoName' => 'Милославский И.С.',
            'ceoNameRoditelny' => 'Милославского И.С.',
            'siteUrl' => 'www.emportal.ru',
        ];
    }
    
	public static function createAppointmentReport($dataProvider, $endLine = null, $specification = null, $returnFile = false)
	{
		$csvHeader = [
			'Клиника',
			'Адрес',
			'Врач',
			'Время создания заявки',
			'Предварительное время приема',
			'Посетитель',
			'Телефон посетителя',
			'Статус заявки',
			'Отметка клиники',
		];
		$csvRows = [];
		
		$outstream = fopen("php://temp", 'r+');
		fputcsv($outstream, $csvHeader, ';');
		
		foreach($dataProvider->getData() as $data)
		{
			$row = [
				$data->company->name,
				$data->address->shortName,
				$data->doctor->name,
				$data->createdDate,
				$data->plannedTime,
				$data->name,
				$data->phone,
				$data->managerStatusText,
			];
			fputcsv($outstream, $row, ';');
		}
		if ($endLine)
		{
			fputcsv($outstream, [$endLine], ';');
		}
		rewind($outstream);
		
		$fileName = 'Сверка записей';
		if (MyTools::getDateInterval('lastMonth') == $model->visit_time_period)
			$fileName .= ' ' . MyTools::getRussianMonthsNames()[(date('n')-1)] . ' ' . date('Y');
			
		if ($specification)
			$fileName .= ' (' . $specification . ')';
		
		$fileName .= '.csv';
		
		if ($returnFile)
		{
			$file = [
				$fileName,
				mb_convert_encoding(stream_get_contents($outstream), 'CP1251'),
				'text/csv; charset=windows-1251',
				false
			];
			fclose($outstream);
			//$mail->AddAttachment($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			return $file;
		} else {
			Yii::app()->request->sendFile($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			fclose($outstream);
		}
	}
	
	public static function createHTMLAppointmentReport($clinic, $time_period, $returnFile = false, $fileType = "pdf", $useLegalSigns = true, $withFaksimile=true)
	{
		$ATDmodel = AppointmentToDoctors::model();
		$ATDmodel->visit_time_period = $time_period;
		$appReportsInfo = $clinic->getInfoForAppointmentReports($ATDmodel, $time_period);
		
		//$model = new stdClass();
		//сonvert array to object recursively
		$model = json_decode(json_encode([
			'client' => [
				'name' => '',
				'address' => '',
				'legalData' => [
					'nameFull' => '',
					'inn' => '',
					'address' => '',
				],
			]
		]));
		$model->period = $time_period;
        $model->issueDate = RuDate::post( explode('-', $time_period)[1], false, false );
		
		$model->client->name .= $clinic->company->name;
		$model->client->name .=  ', ' . $clinic->getPostalAddress(true);
		
		$model->client->legalData->nameFull .= " " . ((!!$clinic->legalName) ? $clinic->legalName : '_________________________________________________________________________');
		$model->client->legalData->inn .= " " . ((!!$clinic->legalINN) ? $clinic->legalINN : '_____________');
		$model->client->legalData->address .= " " . ((!!$clinic->legalAddress) ? $clinic->legalAddress : '______________________________________________________________________');
		
		$model->reportMonth = MyTools::convertIntervalToReportMonth($ATDmodel->visit_time_period);
		$model->number = $clinic->getBill($model->reportMonth)->id;
		$model->products = [];
		$model->contractNumber = $clinic->company->companyContracts->contractNumber;
		$model->contractCreateDate = $clinic->userMedicals->createDate;
		
		$model->totalSale = $appReportsInfo['totalSale'];
        $numberAnaliz = new NumberAnaliz();
        $numberAnalizOutput = $numberAnaliz->CurrencyToText($model->totalSale, "RUR", true);
        $model->totalSaleText= $appReportsInfo['totalSale'] . ' (' . $numberAnalizOutput['valueText'] . ') ' . $numberAnalizOutput['currencyText'];
        
        
		$data = $appReportsInfo['dataProvider']->getData();
		foreach ($data as $item) {
			$model->products[] = $item;
		}
		ksort($model->products);
		
        $model->useLegalSigns = $useLegalSigns;
        
		$ccc = new CController('context');
		$templatePath = Yii::getPathOfAlias('application.components.views.matching') . '.php';
		$htmlpage = $ccc->renderFile($templatePath, array('model' => $model, 'legalData' => static::getLegalData(), 'withFaksimile'=>$withFaksimile), true);
		
		switch (strtolower($fileType)) {
			case "pdf":
				$fileName = 'Сверка записей ' . $model->period . '.pdf';
				$html2pdf = Yii::app()->ePdf->mpdf();
				$html2pdf->WriteHTML($htmlpage);
				$page = $html2pdf->Output($fileName, EYiiPdf::OUTPUT_TO_STRING);
				$mimeType = 'application/pdf';
				break;
			default:
			case "html":
				$page = $htmlpage;
				$fileName = 'Сверка записей ' . $model->period . '.html';
				$mimeType = 'text/html; charset=utf8';
				break;
		}

		$outstream = fopen("php://temp", 'r+');
		fwrite($outstream,$page);
		rewind($outstream);
		$file = [
			$fileName,
			stream_get_contents($outstream),
			$mimeType,
			false
		];
		fclose($outstream);
		
		if ($returnFile) {
			return $file;
		} else {
			Yii::app()->request->sendFile($file[0], $file[1], $file[2], $file[3]);
		}
	}
	
	public static function createHTMLBill($clinic, $time_period, $returnFile=false, $fileType = "pdf")
	{
		$model = new stdClass();
		$model->client = "";
		$ATDmodel = AppointmentToDoctors::model();
		$ATDmodel->visit_time_period = $time_period;
		$model->period = $ATDmodel->visit_time_period;
		
		$appReportsInfo = $clinic->getInfoForAppointmentReports($ATDmodel, $model->period);
		
		//Используем ОГРН
		if(!empty($clinic->legalName)) {
			$model->client .= ((!empty($model->client))?", ":"") . " " . $clinic->legalName;
		} else {
			//если нет ОГРН, берем обычное название компании
			if(!empty($clinic->company->name)) {
				$model->client .= ((!empty($model->client))?", ":"") . $clinic->company->name;
			}
		}
		if(!empty($clinic->legalINN)) {
			$model->client .= ((!empty($model->client))?", ":"") . "ИНН " . $clinic->legalINN;
		}
		if(!empty($clinic->legalName) && !empty($clinic->legalAddress)) {
			//ставим юр. адрес только если есть и ОГРН
			$model->client .= ((!empty($model->client))?", ":"") . " " . $clinic->legalAddress;
		} else {
			if(!empty($clinic->shortName)) {
				$model->client .= ((!empty($model->client))?", ":"") . $clinic->shortName;
			}
		}
		
		$reportMonth = MyTools::convertIntervalToReportMonth($ATDmodel->visit_time_period);
		$periodName = MyTools::convertReportMonthToPeriodName($reportMonth);
		
		$model->number = $clinic->getBill($reportMonth)->id;
		$model->products = [];

		$criteria = new CDbCriteria();
		$criteria->compare('t.reportMonth',$reportMonth);
		$criteria->compare('t.addressId',$clinic->id);
		$bill = Bill::model()->find($criteria);
		if($bill) {
			$model->totalSale = $bill->sum;
			$model->isFixed = SalesContractType::model()->findByPk($bill->salesContractTypeId)->isFixed;
		} else {
			$model->totalSale = $appReportsInfo['totalSale'];
			$model->isFixed = $clinic->salesContract->salesContractType->isFixed;
		}
		if($model->isFixed) {
			$model->products[ "Услуги привлечения клиента на сайте www.emportal.ru за период " . $periodName][$model->totalSale] = [$model->totalSale];
		} else {
			$data = $appReportsInfo['dataProvider']->getData();
			foreach ($data as $item) {
				$model->products[$item->getVisitTypeText(true, ' за период ' . $periodName)][$item->empComission][] = $item;
			}
		}
		ksort($model->products);
		
		$ccc = new CController('context');
		$templatePath = Yii::getPathOfAlias('application.components.views.bill') . '.php';
		$htmlpage = $ccc->renderFile($templatePath, array('model' => $model, 'legalData' => static::getLegalData()), true);
		
		switch (strtolower($fileType)) {
			case "pdf":
				$fileName = 'Счёт ' . $model->period . '.pdf';
				$html2pdf = Yii::app()->ePdf->mpdf();
				$html2pdf->WriteHTML($htmlpage);
				$page = $html2pdf->Output($fileName, EYiiPdf::OUTPUT_TO_STRING);
				$mimeType = 'application/pdf';
				break;
			default:
			case "html":
				$page = $htmlpage;
				$fileName = 'Счёт ' . $model->period . '.html';
				$mimeType = 'text/html; charset=utf8';
				break;
		}

		$outstream = fopen("php://temp", 'r+');
		fwrite($outstream,$page);
		rewind($outstream);
		$file = [
			$fileName,
			stream_get_contents($outstream),
			$mimeType,
			false
		];
		fclose($outstream);
		
		if ($returnFile) {
			return $file;
		} else {
			Yii::app()->request->sendFile($file[0], $file[1], $file[2], $file[3]);
		}
	}
	
	public static function createBillsListReport($dataProvider, $reportMonth, $endLine = null, $specification = null, $returnFile = false)
	{
		$csvHeader = [
			'Компания',
			'Адрес',
			'Юридическое название',
			'ИНН',
			'Юр. адрес',
			'Почтовый адрес',
			'Договор',
			'Номер счета',
            'Сумма счета, руб.',
			'Статус оплаты',
		];
		
		$outstream = fopen("php://temp", 'r+');
		fputcsv($outstream, $csvHeader, ';');
		
		foreach($dataProvider->getData() as $data)
		{
			$bills = Bill::model()->findAllByAttributes(['addressId' => $data->id,'reportMonth' => $reportMonth]);
			if($reportMonth === "2015-07") {
				$bills = array_merge($bills, $bills = Bill::model()->findAllByAttributes(['addressId' => $data->id,'reportMonth' => "1970-01"]));
			}
			$bills_id = [];
            $bills_sums = [];
			$bills_paymentStatusId = [];
			foreach ($bills as $bill) {
				$bills_id[] = $bill->id;
                $bills_sums[] = $bill->sum;
                $bills_paymentStatusIdStr = Bill::$MANAGER_STATUS_ID_TYPES[intval($bill->paymentStatusId)];
                if ($bill->sumPayedOnline > 0) {
                    $criteria = new CDbCriteria();
                    $criteria->compare('type', 'aviso', false);
                    $criteria->addSearchCondition('requestParamsStr', '{"orderNumber":"b' . $bill->id . '",');
                    if ($logKassa = LogKassa::model()->find($criteria)) {
                        $paymentDate = CJSON::decode($logKassa->requestParamsStr)['paymentDatetime'];
                        $paymentDate = ' ' . date('Y-m-d H:i', strtotime($paymentDate));
                        //$paymentDate = ' ' . date('Y-m-d H:i', strtotime($logKassa->createdDate));
                    } else {
                        $paymentDate = '';
                    }
                    
                    $bills_paymentStatusIdStr .= ' (оплачено онлайн через яндекс.кассу ' . $bill->sumPayedOnline . ' руб.' . $paymentDate . ')';
                }
                $bills_paymentStatusId[] = $bills_paymentStatusIdStr;
			}
			
			$contract = $data->company->companyContracts ?
				"Договор №{$data->company->companyContracts->contractNumber} до ". Yii::app()->dateFormatter->formatDateTime($data->company->companyContracts->periodOfValidity) :
				'Нет данных о контрактах';
			$row = [
				$data->company->name,
				$data->shortName,
				$data->legalName,
				$data->legalINN,
				$data->legalAddress,
				$data->postalAddress,
				$contract,
				implode(", ", $bills_id),
                implode(", ", $bills_sums),
				implode(", ", $bills_paymentStatusId),
			];
			fputcsv($outstream, $row, ';');
		}
		if ($endLine)
		{
			fputcsv($outstream, [$endLine], ';');
		}
		rewind($outstream);
		
		$fileName = 'Список счетов';
		if (MyTools::getDateInterval('lastMonth') == $model->visit_time_period)
			$fileName .= ' ' . MyTools::getRussianMonthsNames()[(date('n')-1)] . ' ' . date('Y');
			
		if ($specification)
			$fileName .= ' (' . $specification . ')';
		
		$fileName .= '.csv';
		
		if ($returnFile)
		{
			$file = [
				$fileName,
				mb_convert_encoding(stream_get_contents($outstream), 'CP1251'),
				'text/csv; charset=windows-1251',
				false
			];
			fclose($outstream);
			//$mail->AddAttachment($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			return $file;
		} else {
			Yii::app()->request->sendFile($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			fclose($outstream);
		}
	}
	
	public static function createBillsList1CReport($dataProvider, $reportMonth, $endLine = null, $specification = null, $returnFile = false)
	{
		$csvHeader = [
			'Компания',
			'Адрес',
			'Юридическое название',
			'ИНН',
			'Юр. адрес',
			'Почтовый адрес',
			'Договор',
			'Номер счета',
			'Дата счета',
            'Сумма счета, руб.',
			'Статус оплаты',
			'Услуга',
			'Содержание',
			'Количество',
			'Цена (комиссия ЕМП)',
			'Цена итого (сумма комиссий ЕМП)',
			'Пациент',
			'Телефон',
			'Специалист',
			'ВремяВизита',
			'Стоимость в клинике (если указана)',
			'Общая сумма стоимости в клинике (если указано)',
		];

		$ATDmodel = AppointmentToDoctors::model();
		$ATDmodel->visit_time_period = MyTools::getDateInterval($reportMonth);
		
		$outstream = fopen("php://temp", 'r+');
		fputcsv($outstream, $csvHeader, ';');
		
		foreach($dataProvider->getData() as $data)
		{
			$bills = Bill::model()->findAllByAttributes(['addressId' => $data->id,'reportMonth' => $reportMonth]);
			if($reportMonth === "2015-07") {
				$bills = array_merge($bills, $bills = Bill::model()->findAllByAttributes(['addressId' => $data->id,'reportMonth' => "1970-01"]));
			}
			$bills_id = [];
            $bills_createdDate = [];
            $bills_sums = [];
			$bills_paymentStatusId = [];
			foreach ($bills as $bill) {
				$bills_id[] = $bill->id;
                $bills_createdDate[] = preg_split('/[ ]/', $bill->createdDate)[0];
                $bills_sums[] = $bill->sum;
                $bills_paymentStatusIdStr = Bill::$MANAGER_STATUS_ID_TYPES[intval($bill->paymentStatusId)];
                if ($bill->sumPayedOnline > 0) {
                    $criteria = new CDbCriteria();
                    $criteria->compare('type', 'aviso', false);
                    $criteria->addSearchCondition('requestParamsStr', '{"orderNumber":"b' . $bill->id . '",');
                    if ($logKassa = LogKassa::model()->find($criteria)) {
                        $paymentDate = CJSON::decode($logKassa->requestParamsStr)['paymentDatetime'];
                        $paymentDate = ' ' . date('Y-m-d H:i', strtotime($paymentDate));
                        //$paymentDate = ' ' . date('Y-m-d H:i', strtotime($logKassa->createdDate));
                    } else {
                        $paymentDate = '';
                    }
                    
                    $bills_paymentStatusIdStr .= ' (оплачено онлайн через яндекс.кассу ' . $bill->sumPayedOnline . ' руб.' . $paymentDate . ')';
                }
                $bills_paymentStatusId[] = $bills_paymentStatusIdStr;
			}
			
			$contract = $data->company->companyContracts ?
				"Договор №{$data->company->companyContracts->contractNumber} до ". Yii::app()->dateFormatter->formatDateTime($data->company->companyContracts->periodOfValidity) :
				'Нет данных о контрактах';

			$appReportsInfo = $data->getInfoForAppointmentReports($ATDmodel, $ATDmodel->visit_time_period);
			$dataAppReportsInfo = $appReportsInfo['dataProvider']->getData();
			foreach ($dataAppReportsInfo as $itemAppReports) {
				$row = [
					$data->company->name,
					$data->shortName,
					$data->legalName,
					$data->legalINN,
					$data->legalAddress,
					$data->postalAddress,
					$contract,
					implode(", ", $bills_id),
					implode(", ", $bills_createdDate),
					implode(", ", $bills_sums),
					implode(", ", $bills_paymentStatusId),
					$itemAppReports->getVisitTypeText(true, ''),//'Услуга',
					$itemAppReports->getVisitTypeSubject(),//'Содержание',
					1,//'Количество',
					number_format($itemAppReports->empComission, 2, '.', ''),//'Цена (комиссия ЕМП)',
					number_format($itemAppReports->empComission, 2, '.', ''),//'Цена итого (сумма комиссий ЕМП)',
					$itemAppReports->name,//'Пациент',
					$itemAppReports->phone,//'Телефон',
					$itemAppReports->doctor->name,//'Специалист',
					$itemAppReports->plannedTime,//'ВремяВизита',
					$itemAppReports->totalPrice,//'Стоимость в клинике (если указана)',
					$itemAppReports->totalPrice,//'Общая сумма стоимости в клинике (если указано)',
					
				];
				fputcsv($outstream, $row, ';');
			}
		}
		if ($endLine)
		{
			fputcsv($outstream, [$endLine], ';');
		}
		rewind($outstream);
		
		$fileName = 'Список счетов';
		if (MyTools::getDateInterval('lastMonth') == $model->visit_time_period)
			$fileName .= ' ' . MyTools::getRussianMonthsNames()[(date('n')-1)] . ' ' . date('Y');
			
		if ($specification)
			$fileName .= ' (' . $specification . ')';
		
		$fileName .= '.csv';
		
		if ($returnFile)
		{
			$file = [
				$fileName,
				mb_convert_encoding(stream_get_contents($outstream), 'CP1251'),
				'text/csv; charset=windows-1251',
				false
			];
			fclose($outstream);
			//$mail->AddAttachment($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			return $file;
		} else {
			Yii::app()->request->sendFile($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			fclose($outstream);
		}
	}
	
	public static function createContrAgentsListReport($dataProvider, $endLine = null, $specification = null, $returnFile = false)
	{
		$csvHeader = [
				'Название',
				'Адрес сайта',
				'E-mail ЛПР'
		];
		$outstream = fopen("php://temp", 'r+');
		fputcsv($outstream, $csvHeader, ';');
		
		foreach($dataProvider->getData() as $data)
		{
			$chunks = explode(' ', $data->companySalesInfo->email);
			$emailsString = '';
			foreach($chunks as $chunk){
				$validEmail = filter_var($chunk, FILTER_VALIDATE_EMAIL);
				if($validEmail)
					$emailsString .= $validEmail . ' ';
			}
			
			$row = [
					$data->name,
					$data->companySalesInfo->website,
					$emailsString,
			];
			fputcsv($outstream, $row, ';');
		}
		if ($endLine)
		{
			fputcsv($outstream, [$endLine], ';');
		}
		rewind($outstream);
		
		$fileName = 'Список контрагентов';
		
		if ($specification)
			$fileName .= ' (' . $specification . ')';
		
		$fileName .= '.csv';
		
		if ($returnFile)
		{
			$file = [
					$fileName,
					mb_convert_encoding(stream_get_contents($outstream), 'CP1251'),
					'text/csv; charset=windows-1251',
					false
			];
			fclose($outstream);
			return $file;
		} else {
			Yii::app()->request->sendFile($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			fclose($outstream);
		}
	}
	
	public static function createVoucher($appointment, $leftColumn=true, $returnFile=false, $fileType = "pdf")
	{
		$model = new stdClass();
		$model->leftColumn = $leftColumn;
		$model->number = $appointment->getVoucherNumber();
		$model->companyLogo = ($appointment->company->logo) ? $appointment->company->logoUrl : false;
		$model->companyName = $appointment->company->name;
		$model->addressName = trim(trim($appointment->address->name, ','));
		$model->doctorPhoto = (is_object($appointment->doctor) && $appointment->doctor->photo) ? $appointment->doctor->photoUrl : false;
		$model->plannedTime = date("H:i, d ", strtotime($appointment->plannedTime)) . MyTools::getShortMonth(date("m", strtotime($appointment->plannedTime))) . date(" Y г.", strtotime($appointment->plannedTime));
		$model->price = is_numeric($appointment->price) ? number_format($appointment->price, 2, ",", " ") . " руб." : "не указана";
		$model->prePaid = (is_numeric($appointment->price) && $appointment->prePaid > 0) ? ("Оплачено бонусами: " . number_format($appointment->prePaid, 2, ",", " ") . " руб.") : "";
		$model->totalPrice = is_numeric($appointment->price)
								?	"Итого к оплате: " . number_format($appointment->price - $appointment->prePaid, 2, ",", " ") . " руб."
								:	'';
		$model->userName = $appointment->user->name;
		$model->userBirthday = (!empty($appointment->user->birthday)) ? date("d.m.Y", strtotime($appointment->user->birthday)) : "не указана";
		$model->userTelefon = $appointment->user->telefon;
		$model->doctorName = (is_object($appointment->doctor)) ? $appointment->doctor->name : false;
		$specialties = [];
		if (is_object($appointment->doctor)) {
			foreach ($appointment->doctor->specialtyOfDoctors as $specialtyOfDoctor) {
				$specialties[] = ["id"=>$specialtyOfDoctor->doctorSpecialty->id, "name"=>$specialtyOfDoctor->doctorSpecialty->name];
			}
		}
		$model->doctorSpecialty = CHtml::listData($specialties, 'id', 'name');
        $model->name = $appointment->getVisitTypeText(true); // ? 'Талон записи на услугу' : 'Талон записи на прием';
				
		$ccc = new CController('context');
		$templatePath = Yii::getPathOfAlias('application.components.views.voucher') . '.php';
		$htmlpage = $ccc->renderFile($templatePath, array('model'=>$model, 'appointment'=>$appointment), true);
		
		switch (strtolower($fileType)) {
			case "pdf":
				$fileName = 'Талон.pdf';
				$html2pdf = Yii::app()->ePdf->mpdf();
				$html2pdf->WriteHTML($htmlpage);
				$page = $html2pdf->Output($fileName, EYiiPdf::OUTPUT_TO_STRING);
				$mimeType = 'application/pdf';
				break;
			default:
			case "html":
				$page = $htmlpage;
				$fileName = 'Талон.html';
				$mimeType = 'text/html; charset=utf8';
				break;
		}
		
		$outstream = fopen("php://temp", 'r+');
		fwrite($outstream,$page);
		rewind($outstream);
		$file = [
				$fileName,
				stream_get_contents($outstream),
				$mimeType,
				false
		];
		fclose($outstream);
		
		if ($returnFile) {
			return $file;
		} else {
			Yii::app()->request->sendFile($file[0], $file[1], $file[2], $file[3]);
		}
	}

	public static function createCSV($fileName, $data, $header, $endLine = null, $returnFile = false)
	{
		$outstream = fopen("php://temp", 'r+');
		fputcsv($outstream, $header, ';');
	
		foreach($data as $row)
		{
			$csvrow = [];
			foreach ($header as $key=>$value) {
				$csvrow[$key] = $row[$key];
			}
			fputcsv($outstream, $csvrow, ';');
		}
		if ($endLine)
		{
			fputcsv($outstream, [$endLine], ';');
		}
		rewind($outstream);
	
		$fileName .= '.csv';
	
		if ($returnFile)
		{
			$file = [
					$fileName,
					mb_convert_encoding(stream_get_contents($outstream), 'CP1251'),
					'text/csv; charset=windows-1251',
					false
			];
			fclose($outstream);
			return $file;
		} else {
			Yii::app()->request->sendFile($fileName, mb_convert_encoding(stream_get_contents($outstream), 'CP1251'), 'text/csv; charset=windows-1251', false);
			fclose($outstream);
		}
	}
    
    public static function createEnvelope(Address $address, $filetype = 'html', $print = false)
    {
        $model = new StdClass();
        $model->toWhere = $address->getPostalData();
        $model->toWhereName = $address->company->name;
        $model->toWho = ($address->legalName) ? $address->legalName : $address->company->name;
        $model->postalCode = $model->toWhere['postalCode'];
        if (empty($model->postalCode)) {
            $model->postalCode = "&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ";
        }
        
        $ccc = new CController('context');
		$templatePath = Yii::getPathOfAlias('application.components.views.envelope') . '.php';
		$htmlpage = $ccc->renderFile($templatePath, array('model' => $model, 'legalData' => static::getLegalData()), true);
        
        $fileName = 'Конверт для ' . $model->toWho . ', ' . implode(', ', array_filter($model->toWhere));
        switch($filetype) {
            case 'html':
                $fileName .= '.html';
                $outstream = fopen("php://temp", 'r+');
                fwrite($outstream, $htmlpage);
                rewind($outstream);
				$mimeType = 'text/html; charset=utf8';
                $file = [
                    $fileName,
                    stream_get_contents($outstream),
                    $mimeType,
                    false
                ];
                fclose($outstream);
                Yii::app()->request->sendFile($file[0], $file[1], $file[2], $file[3]);
                break;
            case 'pdf':
                $fileName .= '.pdf';
                $html2pdf = Yii::app()->ePdf->mpdf();
				$html2pdf->WriteHTML($htmlpage);
				$page = $html2pdf->Output($fileName, EYiiPdf::OUTPUT_TO_STRING);
				$mimeType = 'application/pdf';
                
                $outstream = fopen("php://temp", 'r+');
                fwrite($outstream, $page);
                rewind($outstream);
                
                $file = [
                    $fileName,
                    stream_get_contents($outstream),
                    $mimeType,
                    false
                ];
                fclose($outstream);
                Yii::app()->request->sendFile($file[0], $file[1], $file[2], $file[3]);
                break;
            default:
                echo $htmlpage;
                if ($print) {
                    echo '<script>window.print();</script>';
                }
                break;
        }
    }
    
	public static function createClaim($clinic, $returnFile=false, $fileType = "pdf")
	{
        $blankLine = '________________________';
		$model = new stdClass();
		$model->legalName = trim(strval($clinic->legalName)) === "" ? '«' . $clinic->company->name . '»' : $clinic->legalName;
		$model->lprName = trim(strval($clinic->company->companySalesInfo->lprName)) === "" ? $blankLine : $clinic->company->companySalesInfo->lprName;
        if ($model->lprName == $blankLine)
            return false;
		$model->contractCreateDate = trim(strval($clinic->userMedicals->createDate)) === "" ? "«___» __________201__г." : RuDate::post($clinic->userMedicals->createDate, false, false) . " г.";
		$model->contractNumber = trim(strval($clinic->company->companyContracts->contractNumber)) === "" ? $blankLine : $clinic->company->companyContracts->contractNumber;
		$model->currentDate = RuDate::post(date("Y-m-d"), false, false) . " г.";
		$model->deadLine = RuDate::post(date("Y-m-d", strtotime("+15 day")), false, false) . " г.";
		$model->debtSum =  trim(strval($clinic->debtSum)) === "" ? 0 : $clinic->debtSum;
		
		$ccc = new CController('context');
		$templatePath = Yii::getPathOfAlias('application.components.views.claims') . '.php';
		$htmlpage = $ccc->renderFile($templatePath, array('model' => $model), true);
		
		switch (strtolower($fileType)) {
			case "pdf":
				$fileName = 'Претензия от emportal.pdf';
				$html2pdf = Yii::app()->ePdf->mpdf();
				$html2pdf->WriteHTML($htmlpage);
				$page = $html2pdf->Output($fileName, EYiiPdf::OUTPUT_TO_STRING);
				$mimeType = 'application/pdf';
				break;
			default:
			case "html":
				$page = $htmlpage;
				$fileName = 'Претензия от emportal.html';
				$mimeType = 'text/html; charset=utf8';
				break;
		}

		$outstream = fopen("php://temp", 'r+');
		fwrite($outstream,$page);
		rewind($outstream);
		$file = [
			$fileName,
			stream_get_contents($outstream),
			$mimeType,
			false
		];
		fclose($outstream);
		
		if ($returnFile) {
			return $file;
		} else {
			Yii::app()->request->sendFile($file[0], $file[1], $file[2], $file[3]);
		}
	}


	public static function createInvoice($invoice, $download=false, $fileType = "pdf")
	{
		$clinic = $invoice->address;

		$model = new stdClass();
		$model->client = "";
		//Используем ОГРН
		if(!empty($clinic->legalName)) {
			$model->client .= ((!empty($model->client))?", ":"") . " " . $clinic->legalName;
		} else {
			//если нет ОГРН, берем обычное название компании
			if(!empty($clinic->company->name)) {
				$model->client .= ((!empty($model->client))?", ":"") . $clinic->company->name;
			}
		}
		if(!empty($clinic->legalINN)) {
			$model->client .= ((!empty($model->client))?", ":"") . "ИНН " . $clinic->legalINN;
		}
		if(!empty($clinic->legalName) && !empty($clinic->legalAddress)) {
			//ставим юр. адрес только если есть и ОГРН
			$model->client .= ((!empty($model->client))?", ":"") . " " . $clinic->legalAddress;
		} else {
			if(!empty($clinic->shortName)) {
				$model->client .= ((!empty($model->client))?", ":"") . $clinic->shortName;
			}
		}
		
		$model->number = $invoice->id ."-". ltrim($clinic->link,0);
		$model->products = [
				"Предоплата за информационные услуги для аккаунта №".ltrim($clinic->link,0) => [
						$invoice->sum => $invoice->sum,
				]
		];
		$model->totalSale = $invoice->sum;
		
		$ccc = new CController('context');
		$templatePath = Yii::getPathOfAlias('application.components.views.bill') . '.php';
		$htmlpage = $ccc->renderFile($templatePath, array('model' => $model, 'legalData' => static::getLegalData()), true);
		
		switch (strtolower($fileType)) {
			case "pdf":
				$fileName = 'Счёт' . '.pdf';
				$html2pdf = Yii::app()->ePdf->mpdf();
				$html2pdf->WriteHTML($htmlpage);
				$page = $html2pdf->Output($fileName, EYiiPdf::OUTPUT_TO_STRING);
				$mimeType = 'application/pdf';
				break;
			default:
			case "html":
				$page = $htmlpage;
				$fileName = 'Счёт' . '.html';
				$mimeType = 'text/html; charset=utf8';
				break;
		}

		$outstream = fopen("php://temp", 'r+');
		fwrite($outstream,$page);
		rewind($outstream);
		$file = [
			$fileName,
			stream_get_contents($outstream),
			$mimeType,
			false
		];
		fclose($outstream);
		
		if ($download) {
			Yii::app()->request->sendFile($file[0], $file[1], $file[2], $file[3]);
		} else {
			return $file;
		}
	}
}

?>