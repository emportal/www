<?php

/**
 *
 *
 * 
 */
class UpdateCounters extends CComponent {

	public static function updateAddressCounter($link) {
		$address = Address::model()->findByLink($link);
		
		if (@$address && !self::botDetect()) {
			$addressId = $address->id;
			$ip = MyTools::getUserHostAddress();

			/*$model = LogAddress::model()->findByAttributes([
				'ip' => $ip,
				'addressId' => $addressId
			]);*/
			
			$model = new LogAddress();
			$datetime = new CDbExpression("NOW()");
			$obj = new CHtmlPurifier();
			$model->addressId = $addressId;
			$model->ip = $ip;
			$model->datetime = $datetime;
			$model->date = $datetime;
			$model->referrer = $obj->purify($_SERVER['HTTP_REFERER']);
			$model->url = MyTools::url_origin($_SERVER);
			$model->userAgent = $obj->purify($_SERVER['HTTP_USER_AGENT']);  
			$address->views++;
			$address->save();
			$model->save();
		}
	}

	public static function updateDoctorCounter($link) {
		$doctor = Doctor::model()->findByLink($link);
		
		if ($doctor && !self::botDetect()) {
			$doctorId = $doctor->id;
			$ip = MyTools::getUserHostAddress();
		
			$model = new LogDoctor();
			$datetime = new CDbExpression("NOW()");
			$obj = new CHtmlPurifier();
			$model->doctorId = $doctorId;
			$model->ip = $ip;
			$model->datetime = $datetime;
			$model->date = $datetime;
			$model->referrer = $obj->purify($_SERVER['HTTP_REFERER']);
			$model->url = MyTools::url_origin($_SERVER);
			$model->userAgent = $obj->purify($_SERVER['HTTP_USER_AGENT']); 			
			$model->save();
			
		}
	}
	public static function updateClinicCounter($link) {
		$clinic = Company::model()->findByLink($link);
		
		if ($clinic && !self::botDetect()) {
			$clinicId = $clinic->id;
			$ip = MyTools::getUserHostAddress();

		
			
			$model = new LogClinic();
			$datetime = new CDbExpression("NOW()");
			$obj = new CHtmlPurifier();
			$model->companyId = $clinicId;
			$model->ip = $ip;
			$model->datetime = $datetime;
			$model->date = $datetime;
			$model->referrer = $obj->purify($_SERVER['HTTP_REFERER']);
			$model->url = MyTools::url_origin($_SERVER);
			$model->userAgent = $obj->purify($_SERVER['HTTP_USER_AGENT']);  
			$model->save();
			
		}
	}
	
	public static function updateNewsCounter($link) {
		$news = News::model()->findByLink($link);
		
		if (@$news && !self::botDetect()) {
			$ip = MyTools::getUserHostAddress();
			
			$model = new LogNews();
			$datetime = new CDbExpression("NOW()");
			$obj = new CHtmlPurifier();
			$model->newsId = $news->id;
			$model->ip = $ip;
			$model->datetime = $datetime;
			$model->date = $datetime;
			$model->referrer = $obj->purify($_SERVER['HTTP_REFERER']);
			$model->url = MyTools::url_origin($_SERVER);
			$model->userAgent = $obj->purify($_SERVER['HTTP_USER_AGENT']);
			$news->views++;
			if(!LogNews::model()->exists('newsId=:newsId AND ip=:ip',[':newsId'=>$model->newsId, ':ip'=>$model->ip])) {
				$news->hosts++;
			}
			$news->save();
			$model->save();
		}
	}
	

	public static function botDetect() {
		$USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
		$crawlers = array(
			'bot' => 'bot',
			'Google' => 'Google',
			'MSN' => 'msnbot',
			'Rambler' => 'Rambler',
			'Yahoo' => 'Yahoo',
			'AbachoBOT' => 'AbachoBOT',
			'accoona' => 'Accoona',
			'AcoiRobot' => 'AcoiRobot',
			'ASPSeek' => 'ASPSeek',
			'CrocCrawler' => 'CrocCrawler',
			'Dumbot' => 'Dumbot',
			'FAST-WebCrawler' => 'FAST-WebCrawler',
			'GeonaBot' => 'GeonaBot',
			'Gigabot' => 'Gigabot',
			'Lycos spider' => 'Lycos',
			'MSRBOT' => 'MSRBOT',
			'Altavista robot' => 'Scooter',
			'AltaVista robot' => 'Altavista',
			'ID-Search Bot' => 'IDBot',
			'eStyle Bot' => 'eStyle',
			'Scrubby robot' => 'Scrubby',
			'Facebook' => 'facebookexternalhit',
		);

		foreach ($crawlers as $crawler) {
			if (stripos($USER_AGENT, $crawler)) {
				return true;
			}
		}

		return false;
	}

}

?>
