<?php

/**
 *
 *
 * @author NeoSonic
 */
class Html extends CHtml {


	/**
	 * Extend base {@link CHtml::link} for adding default title based on $text
	 */
	public static function link($text, $url = '#', $htmlOptions = array()) {

		if ( !isset($htmlOptions['title']) )
			$htmlOptions['title'] = $text;

		return parent::link($text, $url, $htmlOptions);
	}

	public static function uploadedImage($src, $alt = '', $htmlOptions = array()) {
		
	}
}

?>
