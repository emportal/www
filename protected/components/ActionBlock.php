<?php

class ActionBlock extends CWidget {

	/**
	 * @var string
	 */
	public $title = null;

	public function init() {
	}

	public function run() {
		$criteria=new CDbCriteria();		
		$criteria->order = "RAND()";
		$criteria->with = [
			'action'=>[
				'together'=>true
			]
		];
		$currentPriority = CurrentPriorityAction::model()->active()->findAll($criteria);
		$count = count($currentPriority);
		#var_dump($currentPriority[0]->action->attributes);
		$actions = Action::model()->published()->active()->findAll(array('order'=>'RAND()','limit' => 2-$count));

		if ( count($actions ) || $count ) {
			$this->render('actionBlock', [
				'currentPriority'=>$currentPriority,'actions'=>$actions
			]);
		}

	}

}

?>