<?php
Yii::import('zii.widgets.CPortlet');

class AppForm extends CPortlet
{
	public $options = [
			'companyLinkUrl' => '',
			'addressLinkUrl' => ''
	];

	public function init()
	{
		//ob_start();
		//ob_implicit_flush(false);
		//ob_clean();
	}
	public function run()
	{
		$this->renderContent();
		//$content = ob_get_clean();
		//echo $content;
	}

    protected function renderContent()
    {
		$appFolder = 'shared' . DIRECTORY_SEPARATOR . 'bundles' . DIRECTORY_SEPARATOR . 'appForm' . DIRECTORY_SEPARATOR;
		$this->options['fullAppPath'] = Yii::getPathOfAlias('application') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $appFolder;
		
		if (!$this->options['companyLinkUrl'] OR $this->options['companyLinkUrl'] == 'undefined') {
			$this->options['companyLinkUrl'] = 'medi';
		}
		if (!$this->options['addressLinkUrl']) {
			$this->options['addressLinkUrl'] = '';
		}
		if (!$this->options['appType']) {
			$this->options['appType'] = '';
		}
		$this->render('appForm', $this->options);
    }
	
	public static function isLocalVersion()
	{
		$localFilePath = Yii::getPathOfAlias('application') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
		$hasFileLocalTxt = file_exists($localFilePath . 'local.txt');
		$hasFileLocal = file_exists($localFilePath . 'local');
		return (!$hasFileLocalTxt && !$hasFileLocal) ? false : true;
	}
}
?>