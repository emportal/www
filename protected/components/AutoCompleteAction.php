<?php

/**
 * Действие для полей с автозаполнением
 *
 * <pre><code>
 * return array(
 * 		...
 *      'aclist'=>array(
 *          'class'=>'AutoCompleteAction',
 * 		'model'=>'City', //My model's class name
 * 		'attributeValue'=>'name', //The attribute of the model will search
 *		'attributeLabel'=>'name', //The attribute of the model will show as label
 *		'attributeId'=>'name', //The attribute of the model will be as Id
 * 		)
 * 		...
 * )
 * </code></pre>
 */
class AutoCompleteAction extends CAction {

	/**
	 * @var CActiveRecord|string Модель из которой происходит выборка
	 */
	public $model;

	/**
	 * @var string Атрибут модели со значениями
	 */
	public $attributeValue;
	/**
	 * @var string Атрибут модели с названиями
	 */
	public $attributeLabel;
	/**
	 * @var string Атрибут модели с ИД
	 */
	public $attributeId = 'id';
	private $results = array();

	public function run() {
		if ( isset($this->model) && isset($this->attributeValue) ) {

			if ( is_string($this->model) )
				$this->model = new $this->model;

			$term		 = trim(Yii::app()->request->getParam('term', false));
			$criteria	 = new CDbCriteria();
			if ( $term )
				$criteria->compare($this->attributeValue, $term, true);

			$this->results = array_map(
					function($val) {
						return array(
							'id' => $val->{$this->attributeId},
							'value' => $val->{$this->attributeValue},
							'label' => $this->attributeLabel ? $val->{$this->attributeLabel} : $val->{$this->attributeValue},
						);
					}, $this->model->findAll($criteria));
		}

		echo CJSON::encode($this->results);
	}

}

?>
