<?php

Yii::import('zii.widgets.CPortlet');

class SearchBox extends CPortlet {

	const S_TYPE_PATIENT = Search::S_PATIENT;
	const S_TYPE_PATIENT_NEW = Search::S_PATIENT_NEW;
	const S_TYPE_PATIENT_CLINIC = Search::S_PATIENT_CLINIC;
	const S_TYPE_HANDBOOK = Search::S_HANDBOOK;
	const S_TYPE_PRODUCER = Search::S_PRODUCER;
	const S_TYPE_INSURANCE = Search::S_INSURANCE;
	const S_TYPE_TOURISM = Search::S_TOURISM;
	const S_TYPE_BEAUTY = Search::S_BEAUTY;
	const S_TYPE_SERVICE = Search::S_SERVICE;
	const S_TYPE_SMALL = Search::S_SMALL;
	const S_TYPE_ACTION = Search::S_ACTION;

	const TAB_DOCTOR = 1;
	const TAB_OMSCLINIC = 2;
	const TAB_SERVICE = 3;

	/**
	 * @var array
	 */
	public static $CLASSES = [
		'disease' => 'DiseaseGroup',
		'doctorSpecialty' => 'DoctorSpecialty',
		'companyActivity' => 'CompanyActivite',
	];
	public static $S_TYPES = array(
		self::S_TYPE_PATIENT,
		self::S_TYPE_PATIENT_NEW,
		self::S_TYPE_HANDBOOK,
		self::S_TYPE_PRODUCER,
		self::S_TYPE_INSURANCE,
		self::S_TYPE_BEAUTY,
		self::S_TYPE_SERVICE,
		self::S_TYPE_TOURISM
	);
	public static $S_TABS = array(
		self::S_TYPE_SERVICE => array(
			#Search::S_CLINIC => 'Поиск клиники',
			#Search::S_DOCTOR => 'Поиск врача',
			Search::S_SERVICE => 'Поиск услуги',
			#Search::S_ACTION => 'Поиск акций'
		),
		self::S_TYPE_PATIENT => array(
			#Search::S_CLINIC => 'Поиск клиники',
			Search::S_DOCTOR => 'Поиск врача',
			#Search::S_SERVICE => 'Поиск услуги',
			#Search::S_ACTION => 'Поиск акций'
		),
		self::S_TYPE_PATIENT_NEW => array(
			#Search::S_CLINIC => 'Поиск клиники',
			Search::S_DOCTOR => 'Поиск врача',
			#Search::S_SERVICE => 'Поиск услуги',
			#Search::S_ACTION => 'Поиск акций'
		),
		self::S_TYPE_PATIENT_CLINIC => array(
			Search::S_CLINIC => 'Поиск клиники',
			#Search::S_DOCTOR => 'Поиск врача',
			#Search::S_SERVICE => 'Поиск услуги',
			#Search::S_ACTION => 'Поиск акций'
		),
		self::S_TYPE_HANDBOOK => array(
			Search::S_DISEASE => 'Болезни',
			#Search::S_MEDICAMENT => 'Лекарства',
			Search::S_DOCTORSPECIALTY => 'Специальности врачей',
			Search::S_COMPANYACTIVITY => 'Профили клиник',
		),
		self::S_TYPE_PRODUCER => array(
			Search::S_PRODUCER => 'Каталог поставщиков'
		),
		self::S_TYPE_INSURANCE => array(
			Search::S_INSURANCE => 'Страховые компании'
		),
		self::S_TYPE_TOURISM => array(
			Search::S_TOURISM => 'Тур. фирмы и тур. операторы с оздоровительными турами'
		),
		self::S_TYPE_BEAUTY => array(
			Search::S_BEAUTY => 'Салоны красоты'
		),
		self::S_TYPE_SMALL => array(
			Search::S_CLINIC => 'Поиск клиники',
			Search::S_DOCTOR => 'Поиск врача',
			Search::S_SERVICE => 'Поиск услуги'
		),
		self::S_TYPE_ACTION => array(
			Search::S_ACTION => 'Поиск акций'
		)
	);
	public $title = null;
	public $type = self::S_TYPE_PATIENT;
	public $tab = '';
	public $tabNumber = self::TAB_DOCTOR; //либо частные клиники, либо ОМС (самозапись)
	public $cityId = 1;
	public $pushHistoryUrl = [
		'/search/clinic',
		'/search/doctor',
		'/search/diagnostics',
	];
	public $searchModel = null;
	public $controller = null;
    protected $enableSearchBoxCache = true;

	public function init() {
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}

	public function run() {
        if (Yii::app()->session['selectedRegion'] === 'moskva' AND Yii::app()->params['samozapis'])
            $this->enableSearchBoxCache = false;
		$this->renderContent();
		$content = ob_get_clean();
		echo $content;
	}

	protected function renderContent() {
		if($this->searchModel !== null) {
			$model = &$this->searchModel;
		} else {
			$model = new Search($this->type);
		}
		$model->attributes = Yii::app()->request->getParam('Search');
		if (Yii::app()->params['samozapis']) {
			$model->initOMSForm();
		}
		/* parse URL string */
		$req = $_REQUEST;
		#preg_match('/(metro-\w+)/iu', $path,$data);
		
		if(!empty($req['metro'])) {
			$model->metroStationId = array($req['metro']);
		} elseif(!empty($req['district'])) {
			$model->metroStationId = array($req['district']);
		}
		if(!empty($req['specialty'])) {
			$model->doctorSpecialtyId = $req['specialty'];
		}
		if(!empty($req['profile'])) {
			$model->companyActiviteId = $req['profile'];
		}
		if(!empty($req['service'])) {
			$model->companyServiceId = $req['service'];
		}
		
		if (in_array($model->companyTypeId, Address::$arrGMU)) {
			$model->isGMU = 1;
		} else {
			$model->isGMU = 0;
		}
		
		if (!is_array($_REQUEST['Search'])) {
			$_REQUEST['Search'] = array();
		}

		$search = Yii::app()->request->getParam("Search");
		if (is_array($search)) {
			$CTID = $search['companyTypeId'];
			if ($CTID) {
				$criteria = new CDbCriteria();
				$criteria->with = [
					'companyActivite' => [
						'together' => true,
						'with' => [
							'addressServices' => [
								'select' => false,
								'together' => true,
								'joinType' => 'INNER JOIN',
								'with' => [
									'address' => [
										'select' => false,
										'together' => true,
										'with' => [
											'company' => [
												'select' => false,
												'together' => true,
											]
										]
									]
								]
							]
						]
					]
				];
				$criteria->compare('company.companyTypeId', $CTID);
				$criteria->compare('t.companyTypeId', $CTID);
				$CA = CompanyTypeActivitesId::model()->findAll($criteria);

				$caList = array();
				foreach ($CA as $row) {
					$caList[] = $row->companyActivite;
				}
			}
		}

		#var_dump(Yii::app()->request->getParam("Search"));

		$params = array();

		switch ($this->type) {
			case self::S_TYPE_ACTION:

				if (!$model->cityId)
					$model->cityId = $this->cityId;
				
				$searchBoxCache = Yii::app()->cache->get('searchBoxCache');
				if (!$searchBoxCache)
				{
					$tmp_arr = [
						'MetroStationList' => $model->metroStations,
						'CityDistrictList' => $model->cityDistricts,
						'CompanyActivitiesList' => $model->CompanyActivities
					];
					$searchBoxCache = $tmp_arr;
					Yii::app()->cache->set('searchBoxCache', $searchBoxCache, 60);
					unset($tmp_arr);
				}
				$MetroStationList = $searchBoxCache['MetroStationList'];
				$CityDistrictList = $searchBoxCache['CityDistrictList'];
				$CompanyActivitiesList = $searchBoxCache['CompanyActivitiesList'];
				

				$params = array(
					'MetroStationList' => $MetroStationList,
					'CityDistrictList' => $CityDistrictList,
					'CompanyActivitiesList' => $CompanyActivitiesList,
				);

				break;

			case self::S_TYPE_PATIENT:

				if (!$model->cityId) {
					$model->cityId = $this->cityId;
				}
				
				$retrieveParams = function() use ($model, $CTID, $caList) {
                    return [
						'MetroStationList' => $model->metroStations,
						'CityDistrictList' => $model->cityDistricts,
						'CompanyActivitiesList' => (!$CTID) ? $model->CompanyActivities : CHtml::listData($caList, 'id', 'name'),
						'DoctorSpecialtyList' => $model->DoctorSpecialties,
						'CompanyTypesList' => $model->CompanyTypes,
						'sexTypesList' => $model->sexTypes,
					];
                };
                
                if ($this->enableSearchBoxCache) {
                    $searchBoxCacheIndex = $this->searchBoxCacheIndex;
                    $params = Yii::app()->cache->get($searchBoxCacheIndex);
                    
                    if (!$params) {
                        $params = $retrieveParams();
                        Yii::app()->cache->set($searchBoxCacheIndex, $params, 600);
                    }
                } else {
					$params = $retrieveParams();
                    $params['DoctorSpecialtyList'] = $model->DoctorSpecialties;
                }
				break;

			case self::S_TYPE_PATIENT_NEW:

				if (!$model->cityId) {
					$model->cityId = $this->cityId;
				}
                $retrieveParams = function() use ($model) {
                    return [
                        'MetroStationList' => $model->metroStations,
                        'CityDistrictList' => $model->cityDistricts,
                        'CompanyActivitiesList' => ($this->tabNumber == SearchBox::TAB_OMSCLINIC) ? ((!$CTID) ? $model->CompanyActivities : CHtml::listData($caList, 'id', 'name')) : null,
                        'DoctorSpecialtyList' => $model->DoctorSpecialties,
                        'CompanyTypesList' => $model->CompanyTypes,
                        'sexTypesList' => $model->sexTypes,
                        'CompanyServiceList' => ($this->tabNumber == SearchBox::TAB_SERVICE) ? $model->CompanyServices : null,
                    ];
                };
                
                if ($this->enableSearchBoxCache) {
                    $searchBoxCacheIndex = $this->searchBoxCacheIndex;
                    $params = Yii::app()->cache->get($searchBoxCacheIndex);
                    
                    if (!$params) {
                        $params = $retrieveParams();
                        Yii::app()->cache->set($searchBoxCacheIndex, $params, 600);
                    }
                } else {
					$params = $retrieveParams();
                    $params['DoctorSpecialtyList'] = $model->DoctorSpecialties;
                }
				break;
			case self::S_TYPE_PATIENT_CLINIC:

				if (!$model->cityId) {
					$model->cityId = $this->cityId;
				}
				$retrieveParams = function() use ($model, $CTID, $caList) {
                    return [
						'MetroStationList' => $model->metroStations,
						'CityDistrictList' => $model->cityDistricts,
						'CompanyActivitiesList' => (!$CTID) ? $model->CompanyActivities : CHtml::listData($caList, 'id', 'name'),
						'CompanyTypesList' => $model->CompanyTypes,
						'sexTypesList' => $model->sexTypes,
					];
                };
                
                if ($this->enableSearchBoxCache) {
                    $searchBoxCacheIndex = $this->searchBoxCacheIndex;
                    $params = Yii::app()->cache->get($searchBoxCacheIndex);
                    
                    if (!$params) {
                        $params = $retrieveParams();
                        Yii::app()->cache->set($searchBoxCacheIndex, $params, 600);
                    }
                } else {
					$params = $retrieveParams();
                }
				break;
			case self::S_TYPE_HANDBOOK:

				break;
			case self::S_TYPE_PRODUCER:

				break;
			case self::S_TYPE_INSURANCE:
				if (!$model->cityId)
					$model->cityId = $this->cityId;

				$MetroStationList = $model->metroStations;
				$CityDistrictList = $model->cityDistricts;
				$DoctorSpecialtyList = $model->DoctorSpecialties;
				$CompanyActivitiesList = $model->CompanyActivities;
				$CompanyTypesList = $model->CompanyTypes;

				$params = array(
					'MetroStationList' => $MetroStationList,
					'CityDistrictList' => $CityDistrictList,
					'DoctorSpecialtyList' => $DoctorSpecialtyList,
					'CompanyActivitiesList' => $CompanyActivitiesList,
					'CompanyTypesList' => $CompanyTypesList,
				);
				break;
			case self::S_TYPE_TOURISM:
				if (!$model->cityId)
					$model->cityId = $this->cityId;

				$MetroStationList = $model->metroStations;
				$CityDistrictList = $model->cityDistricts;
				$DoctorSpecialtyList = $model->DoctorSpecialties;
				$CompanyActivitiesList = $model->CompanyActivities;
				$CompanyTypesList = $model->CompanyTypes;

				$params = array(
					'MetroStationList' => $MetroStationList,
					'CityDistrictList' => $CityDistrictList,
					'DoctorSpecialtyList' => $DoctorSpecialtyList,
					'CompanyActivitiesList' => $CompanyActivitiesList,
					'CompanyTypesList' => $CompanyTypesList,
				);
				break;
			case self::S_TYPE_BEAUTY:
				if (!$model->cityId)
					$model->cityId = $this->cityId;

				$MetroStationList = $model->metroStations;
				$CityDistrictList = $model->cityDistricts;
				$DoctorSpecialtyList = $model->DoctorSpecialties;
				$CompanyActivitiesList = $model->CompanyActivities;
				$CompanyTypesList = $model->CompanyTypes;

				$params = array(
					'MetroStationList' => $MetroStationList,
					'CityDistrictList' => $CityDistrictList,
					'DoctorSpecialtyList' => $DoctorSpecialtyList,
					'CompanyActivitiesList' => $CompanyActivitiesList,
					'CompanyTypesList' => $CompanyTypesList,
				);
				break;
			case self::S_TYPE_SERVICE:
				if (!$model->cityId) {
					$model->cityId = $this->cityId;
				}
				
				$retrieveParams = function() use ($model) {
                    return [
						'MetroStationList' => $model->metroStations,
						'CityDistrictList' => $model->cityDistricts,
						'CompanyServiceList' => $model->CompanyServices,
						'CompanyTypesList' => $model->CompanyTypes,
						'sexTypesList' => $model->sexTypes,
					];
                };
                
                if ($this->enableSearchBoxCache) {
                    $searchBoxCacheIndex = $this->searchBoxCacheIndex;
                    $params = Yii::app()->cache->get($searchBoxCacheIndex);
                    
                    if (!$params) {
                        $params = $retrieveParams();
                        Yii::app()->cache->set($searchBoxCacheIndex, $params, 600);
                    }
                } else {
					$params = $retrieveParams();
                }
				break;
		}



		switch ($this->tab) {
			case 'disease':
				Search::setLetters(array(
						#'ru_new' => $this->getNotEmptyLetters('DiseaseGroup'),
				));
				break;
			case 'medicament':
				Search::setLetters(array(
					/* 'ru' => array(
					  'А-В' => 'абв',
					  'Г-Е' => 'гдеё',
					  'Ж-И' => 'жзий',
					  'К-М' => 'клм',
					  'Н-П' => 'ноп',
					  'Р-Т' => 'рст',
					  'У-Х' => 'уфх',
					  'Ц-Ш' => 'цчшщ',
					  'Э-Я' => 'эюя',
					  ),
					  'en' => array(
					  'A-C' => 'abc',
					  'D-F' => 'def',
					  'G-I' => 'ghi',
					  'J-L' => 'jkl',
					  'M-O' => 'mno',
					  'P-R' => 'pqr',
					  'S-U' => 'stu',
					  'V-X' => 'vwx',
					  'Y-Z' => 'yz',
					  ) */
					'ru_new' => array(
						'А' => 'а', 'Б' => 'б', 'В' => 'в',
						'Г' => 'г', 'Д' => 'д', 'Е' => 'её',
						'Ж' => 'ж', 'З' => 'з', 'И' => 'ий',
						'К' => 'к', 'Л' => 'л', 'М' => 'м',
						'Н' => 'н', 'О' => 'о', 'П' => 'п',
						'Р' => 'р', 'С' => 'с', 'Т' => 'т',
						'У' => 'у', 'Ф' => 'ф', 'Х' => 'х',
						'Ц' => 'ц', 'Ч' => 'ч', 'Ш' => 'ш',
						'Щ' => 'щ', 'Э' => 'э', 'Ю' => 'ю',
						'Я' => 'я'
					),
					'en_new' => array(
						'A' => 'a', 'B' => 'b', 'C' => 'c', 'D' => 'd', 'E' => 'e',
						'F' => 'f', 'G' => 'g', 'H' => 'h', 'I' => 'i', 'J' => 'j',
						'K' => 'k', 'L' => 'l', 'M' => 'm', 'N' => 'n', 'O' => 'o',
						'P' => 'p', 'Q' => 'q', 'R' => 'r', 'S' => 's', 'T' => 't',
						'U' => 'u', 'V' => 'v', 'W' => 'w', 'X' => 'x', 'Y' => 'y',
						'Z' => 'z',
					),
				));

				break;
			case 'doctorSpecialty':
				Search::setLetters(array(
						/* 	'ru' => array(
						  'А-В' => 'абв',
						  'Г-Е' => 'гдеё',
						  'Ж-И' => 'жзий',
						  'К-М' => 'клм',
						  'Н-П' => 'ноп',
						  'Р-Т' => 'рст',
						  'У-Х' => 'уфх',
						  'Ц-Ш' => 'цчшщ',
						  'Э-Я' => 'эюя',
						  ), */
						#'ru_new' => $this->getNotEmptyLetters('DoctorSpecialty')
						/* array(
						  'А' => 'а', 'Б' => 'б', 'В' => 'в',
						  'Г' => 'г', 'Д' => 'д', 'Е' => 'её',
						  'Ж' => 'ж', 'З' => 'з', 'И' => 'ий',
						  'К' => 'к', 'Л' => 'л', 'М' => 'м',
						  'Н' => 'н', 'О' => 'о', 'П' => 'п',
						  'Р' => 'р', 'С' => 'с', 'Т' => 'т',
						  'У' => 'у', 'Ф' => 'ф', 'Х' => 'х',
						  'Ц' => 'ц', 'Ч' => 'ч', 'Ш' => 'ш',
						  'Щ' => 'щ', 'Э' => 'э', 'Ю' => 'ю',
						  'Я' => 'я'
						  ), */
				));
				break;
			case 'companyActivity':
				Search::setLetters(array(
						/* 	'ru' => array(
						  'А-В' => 'абв',
						  'Г-Е' => 'гдеё',
						  'Ж-И' => 'жзий',
						  'К-М' => 'клм',
						  'Н-П' => 'ноп',
						  'Р-Т' => 'рст',
						  'У-Х' => 'уфх',
						  'Ц-Ш' => 'цчшщ',
						  'Э-Я' => 'эюя',
						  ), */
						#'ru_new' => $this->getNotEmptyLetters('companyActivite')
						/* array(
						  'А' => 'а', 'Б' => 'б', 'В' => 'в',
						  'Г' => 'г', 'Д' => 'д', 'Е' => 'её',
						  'Ж' => 'ж', 'З' => 'з', 'И' => 'ий',
						  'К' => 'к', 'Л' => 'л', 'М' => 'м',
						  'Н' => 'н', 'О' => 'о', 'П' => 'п',
						  'Р' => 'р', 'С' => 'с', 'Т' => 'т',
						  'У' => 'у', 'Ф' => 'ф', 'Х' => 'х',
						  'Ц' => 'ц', 'Ч' => 'ч', 'Ш' => 'ш',
						  'Щ' => 'щ', 'Э' => 'э', 'Ю' => 'ю',
						  'Я' => 'я'
						  ), */
				));
				break;
		}

		$this->render('searchBox/' . $this->type, array_merge(
						array(
			'model' => $model,
			'type' => $this->type,
			'tabs' => self::$S_TABS[$this->type]
						), $params
				)
		);
	}

	protected function letterList($link, $chars, $tab) {

		Search::setLetters(array(
			'ru_new' => $this->getNotEmptyLetters(SearchBox::$CLASSES[$tab])
		));
		$output = "<div>Искать по первой букве: </div>";

		$output .= "<div>";

		foreach (Search::$letters as $lang) {
			foreach ($lang as $k => $v) {
				$output .= CHtml::link($k, $this->controller->createUrl($link, array('Search[chars]' => $k)), $chars !== $k ? array() : array('class' => 'active'));
				$output .= ', ';
			}
			$output = preg_replace('/, $/', "<br>", $output);
		}
		$output .= "</div>";

		return $output;
	}

	protected function getNotEmptyLetters($class) {

		$values = array_keys($class::model()->findAll(array(
					'index' => 'name',
					'select' => 'name',
					'order' => 'name ASC'
		)));
		$letters = [];
		foreach ($values as $value) {
			$letter = mb_strtoupper(mb_substr($value, 0, 1, 'utf-8'), 'utf-8');
			if (empty($letters[$letter]))
				$letters[$letter] = $letter;
		}
		return $letters;
	}
    
    public function getSearchBoxCacheIndex() {
        return 'searchBoxCache_' . Yii::app()->session['selectedRegion'] . '_' . Yii::app()->params['samozapis'] . '_' . $this->type . '_' . $this->tabNumber;
    }
}

?>