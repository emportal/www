<?php
Yii::import('zii.widgets.CPortlet');

class OMSpopup extends CPortlet
{
    public $id = null;

	public function init()
	{
		ob_start();
		ob_implicit_flush(false);
		ob_clean();
	}
	public function run()
	{
		$this->renderContent();
		$content=ob_get_clean();
		echo $content;
	}

    protected function renderContent()
    {
    	$userModel = Yii::app()->user->model;
    	$omsForm = new OMSForm();
    	$this->render('OMSpopupView',array('OMSForm'=>$omsForm, 'userModel'=>$userModel));
    }
}
?>