<?php

class ReviewBlock extends CWidget {

	public $options = [];
	private $settings = [
		'reviewTextLimit' => 1200 /* при превышении этого числа символов отзыв обрезается */
	];

	public function init() {
	}

	public function run() {
		
		$reviewTextLimit = $this->settings['reviewTextLimit'];
		
		$review = $this->options['review'];	
		$reviewName = $review[ $this->options['type'] ]['name'];
		$reviewText = CHtml::encode($review['reviewText']);
		
		if ($this->options['type'] == 'doctor') {
			$this->options['reviewCssClass'] = 'review_block_doctor';
			$this->options['reviewIconCssClass'] = 'icon_doctor_mini';
			$this->options['reviewTitle'] = 'О врачах';
			$this->options['reviewLink'] = CHtml::normalizeUrl(array('/klinika/'.$review['company']['linkUrl'].'/'.$review['address']['linkUrl'].'/'.$review['doctor']['linkUrl'].'#reviews'));	
		}
		if ($this->options['type'] == 'company') {
			$this->options['reviewCssClass'] = 'review_block_clinic';
			$this->options['reviewIconCssClass'] = 'icon_hosp_mini';
			$this->options['reviewTitle'] = 'О клиниках';
			$this->options['reviewLink'] = CHtml::normalizeUrl(array('/klinika/'.$review['company']['linkUrl'].'/'.$review['address']['linkUrl'].'#reviews'));
		}
		
		if (strlen($reviewText) > $reviewTextLimit)
		{
			$watchMoreLink = '<a href="' . $this->options['reviewLink'] . '">читать далее</a>';				
			$reviewText = substr($reviewText, 0, strripos(substr($reviewText, 0, $reviewTextLimit), ' ')) . '...' . $watchMoreLink;
		}
		
		$tmp_options = [
			'reviewName' => $reviewName,
			'reviewText' => $reviewText,
			'reviewUserAvatar' => $review->userAvatarSrc,
		];
		
		$this->render('reviewBlock', [
			'options' => array_merge($tmp_options, $this->options)
		]);
	}
}

?>