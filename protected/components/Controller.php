<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 *
 * @property-read string $assetsUrl Description
 * @property-read string $assetsJsUrl Description
 * @property-read string $assetsCssUrl Description
 * @property-read string $assetsImageUrl Description
 */
class Controller extends CController {

	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '/layouts/column1';
	public $seoParams = [];

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	/**
	 *
	 */
	private $_assetsUrl;
	private $_assetsCssUrl;
	private $_assetsJsUrl;
	private $_assetsImageUrl;

	/**
	 *
	 * @var string|null
	 */
	public $searchbox_type = null;
	public $searchModel = null;

	/**
	 *
	 * @var string|null
	 */
	public $searchbox_tab = null;

	/**
	 * @var string|null
	 */
	public $infoBox_type = InfoBox::T_QUICKLINK;

	public function init() {
		parent::init();
		$file = Yii::getPathOfAlias('uploads') . '/redirects.txt';
		if (is_file($file)) {
			$redirectArr = unserialize(file_get_contents($file));
			if (isset($redirectArr[$_SERVER['REQUEST_URI']])) {
				$this->redirect($redirectArr[$_SERVER['REQUEST_URI']]);
			}
		}
		//if ( !Yii::app()->request->isAjaxRequest )
		Yii::app()->clientScript->registerScriptFile("jquery.js", CClientScript::POS_HEAD);
		Yii::app()->clientScript->registerPackage('main');
		

		Yii::app()->user->loginUrl = $this->createUrl("/site/login");
	}

	public function getAssetsUrl() {
		if ($this->_assetsUrl === null)
			$this->_assetsUrl = Yii::app()->clientScript->getPackageBaseUrl('main');
		return $this->_assetsUrl;
	}

	public function getAssetsCssUrl() {
		if ($this->_assetsCssUrl === null)
			$this->_assetsCssUrl = $this->assetsUrl . '/css';
		return $this->_assetsCssUrl;
	}

	public function getAssetsJsUrl() {
		if ($this->_assetsJsUrl === null)
			$this->_assetsJsUrl = $this->assetsUrl . '/js';
		return $this->_assetsJsUrl;
	}

	public function getAssetsImageUrl() {
		if ($this->_assetsImageUrl === null)
			$this->_assetsImageUrl = $this->assetsUrl . '/images';
		return $this->_assetsImageUrl;
	}

	public function beforeAction($action) {
		$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$subdomains = array_reverse(explode('.', $_SERVER['HTTP_HOST']));
		$controllerName = get_class($action->controller);
		if($action->id == "robots" && ($controllerName == "SiteController") && !isset($this->module)) {
			// Выводим robots.txt
		}
		elseif(!MyTools::validateHOST(Yii::app()->params)) {
			if($action->id == "view" && ($controllerName == "DoctorController" || $controllerName == "ClinicController")) {
				//header("Location: ".$protocol.$subdomains[1].".".$subdomains[0].$_SERVER['REQUEST_URI']);
			} else  {
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
				echo 'Страница не найдена!';
				exit;
			}
		} else {
			if(!empty($subdomains[2]) && $subdomains[2] == "spb") {
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
				echo 'Страница не найдена';
				exit;
			}
			elseif($subdomains[2] == "blog") {
				Yii::app()->params['isBlogDomen'] = true;
				/*
				switch ($controllerName) {
					case "AjaxController":
					case "NewsController":
						break;
					default:
						header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
						echo 'Страница не найдена';
						exit;
				}
				*/
			} elseif($controllerName == "NewsController" && !isset($this->module)) {
				$url = ((!empty($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')?'https':'http') . '://blog.emportal.ru';
				$url .= mb_substr($_SERVER['REQUEST_URI'], 5);
				$this->redirect($url);
			}
		}

		$refererId = Yii::app()->request->getParam("partner_refererId");
		if($refererId && !Yii::app()->request->isAjaxRequest && Yii::app()->request->getRequestType()==="GET") {
			Yii::app()->request->cookies['partner_refererId'] =  new CHttpCookie('partner_refererId', $refererId, ['path'=>'/', 'domain'=>'.emportal.ru']);
		}
		
		if( preg_match("/^(\/index.php($|\?))/", $_SERVER['REQUEST_URI']) ) {
			$url = preg_replace("/^(\/index.php)/", "/", $_SERVER['REQUEST_URI']);
			$this->redirect($url);
		}

		if (parent::beforeAction($action)) {
			PhoneVerification::cleanSessionPhone();

			if (Yii::app()->request->getParam("JSON") == 1) {
				if (isset($_REQUEST) && count($_REQUEST) && $_SERVER['HTTP_HOST'] == 'emportal.ru') {
					$filename = $_SERVER['DOCUMENT_ROOT'] . '/post_log.txt';
					$somecontent = "\n-------------------------------\n URL: " . $_SERVER['REQUEST_URI'] . " IP " . $_SERVER['REMOTE_ADDR'] . ' | ' . date("Y-m-d H:i:s") . " - REQUEST данные \n";
					foreach ($_REQUEST as $key => $value) {
						$somecontent.=$key . " - " . print_r($value, true) . "\n";
					}


					// Вначале давайте убедимся, что файл существует и доступен для записи.
					if (is_writable($filename)) {

						// В нашем примере мы открываем $filename в режиме "записи в конец".
						// Таким образом, смещение установлено в конец файла и
						// наш $somecontent допишется в конец при использовании fwrite().
						if (!$handle = fopen($filename, 'a')) {
							echo "Не могу открыть файл ($filename)";
							exit;
						}

						// Записываем $somecontent в наш открытый файл.
						if (fwrite($handle, $somecontent) === FALSE) {
							echo "Не могу произвести запись в файл ($filename)";
							exit;
						}

						fclose($handle);
					} else {
						echo "Файл $filename недоступен для записи";
					}
				}
			}
			
			$baseUrl = Yii::app()->getBaseUrl(true);
			$urlArr = explode('emportal.ru', $baseUrl);
			$subdomains = $urlArr[0];
			$subdomains = str_replace(['http://', 'https://'], '', $subdomains);
			$regions = Yii::app()->params['regions'];
			$defaultSubdomain = '';
			$selectedSubdomain = '';
			foreach($regions as $region) 
			{
				if (isset($region['default'])) $defaultSubdomain = $region['subdomain'];
				if (strpos($subdomains, $region['subdomain']) !== false) 
				{
					$selectedSubdomain = $region['subdomain'];
				}
			}
            //$selectedSubdomain = 'moskva';
            //Yii::app()->params['samozapis'] = true;
            
			if (empty($selectedSubdomain)) $selectedSubdomain = $defaultSubdomain;			
			City::model()->changeRegion($selectedSubdomain);
			
			return true;
		}
	}

	public function beforeRender($view) {

		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam("defaultLayout") != 1) {
			$this->layout = FALSE;
		}

		/* Почему-то тут всегда объект SiteController */
		if (isset($this->_model) && ($this->_model)) {

			$this->pageTitle = $this->_model->name;
		}
		return true;
	}

	public function invalidActionParams($action) {
		throw new CHttpException(404, Yii::t('yii', 'Your request is invalid.'));
	}

	public function redirect($url, $terminate = true, $statusCode = 301) {
		if (is_array($url)) {
			$route = isset($url[0]) ? $url[0] : '';
			$url = $this->createUrl($route, array_splice($url, 1));
		}
		Yii::app()->getRequest()->redirect($url, $terminate, $statusCode);
	}
	
	public function registerSeoTags($params = [
		'title' => '',
		'description' => '',
	]) {
		if ($params['title'])
			$this->pageTitle = $params['title'];
		if ($params['description'])
			Yii::app()->clientScript->registerMetaTag($params['description'], 'description');
		if ($params['seo_block']) {
			$this->seoParams['seo_block'] = $params['seo_block'];
		}
	}
	
	public static function clearCache($cacheIndexes = [])
	{
		foreach($cacheIndexes as $cacheIndex)
		{
			Yii::app()->cache->set($cacheIndex, '', 0);
		}
		return true;
	}
	
	public static function login($telefon, $password, $rememberMe=true) {
		$model = new LoginForm;
		$model->username = $telefon;
		$model->password = $password;
		$model->rememberMe = $rememberMe;
		$success = false;
		for ($i = 0; $i < 5; $i++) {
			try {
				if(!$success) $success = $model->login(true);
				break;
			} catch (Exception $e) {
				usleep(100000);
			}
		}
		return $success;
	}
}
