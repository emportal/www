<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class LeftMenuController extends Controller {

	public $layout = '//layouts/lm_column';
	public $menu = array();

	public function init() {
		parent::init();
	}

	public function getLeftMenu() {
		
		$menuItems = array();		
		$extraItems = array();
		$model = Yii::app()->user->model;
		
		foreach($model->rights as $right) {
			if ($right->zRightId == RightType::ACCESS_NEW_ADMIN) {
				array_unshift($this->menu, [
					'label' => 'Админка менеджера',
					'url' => '/newAdmin',
					'itemOptions' => array(
						'class' => 'btn w100',
						'style' => 'border: 1px solid darkgray; box-shadow: 1px 1px 0px #fff inset, -1px -1px 0px #fff inset;'
					),
				]);
			}
			if ($right->zRightId == RightType::CAN_UPLOAD_DOCUMENTS_FROM_1C) {
				array_push($this->menu, [
						'label' => "Загрузить документ из 1С<br>
						<form id='upload1CFile' action='/site/upload1CFile'>
							<input name='document' type='file' /><br>
							<div class='form_responce'></div>
						</form>
						<script>
							$(window).load(function() {
								$('#upload1CFile input[type=file]').live('change', function(){
									var form = $(this).closest('form');
									var formData = new FormData(form.get(0));
									$.ajax({
										url: form.attr('action'),
										async: true,
										data: formData,
										cache:false,
										contentType: false,
										processData: false,
										dataType: 'json',
										type: 'POST',
										beforeSend: function() {
											
											form.find('.form_responce').html('Загрузка...');
										},
										success: function(response) {
											if(response.success) {
												form.find('.form_responce').html('Документ загружен!<br>Повторы: '+response.result.exists+'<br>Новые: '+response.result.new);
											} else {
												this.error(null,response.errors.join('<br>'),null);
											}
										},
										error: function(XHR,errorText,errorThrown) {
											form.find('.form_responce').html('Произошла ошибка: '+errorText);
										},
										complete: function() {
											$('#upload1CFile input[type=file]').val('');
										},
									});
								});
							});
						</script>
						",
						'url' => 'javascript:void(0);',
						'itemOptions' => array(
								'class' => 'btn w100',
								'style' => 'border: 1px solid darkgray; box-shadow: 1px 1px 0px #fff inset, -1px -1px 0px #fff inset;'
						),
				]);
			}
		}

		if(Yii::app()->session['selectedRegion'] === "moskva" && Yii::app()->params['samozapis']) {
			array_push($this->menu, [
					'label' => 'Мои направления',
					'url' => '/doctor?showReferrals=1',
					'itemOptions' => array(
							'class' => 'btn w100',
							'style' => 'border: 1px solid darkgray; box-shadow: 1px 1px 0px #fff inset, -1px -1px 0px #fff inset;'
					),
			]);
		}
		foreach($model->rights as $right) {
			if ($right->zRightId == RightType::IS_ADMIN_IN_ADDRESS || $right->zRightId == RightType::IS_OWNER_OF_ADDRESS) {
				array_push($this->menu, [
					'label' => 'Моя клиника',
					'url' => 'administrate',
					'itemOptions' => array(
						'class' => 'btn w100',
						'style' => 'border: 1px solid darkgray; box-shadow: 1px 1px 0px #fff inset, -1px -1px 0px #fff inset;'
					),
				]);
				break;
			}
		}
		
		$this->menu = array_merge($extraItems, $this->menu);

		foreach ($this->menu as $key => $value) {
			if(is_array($value)) {
				$menuItems[] = $value;
			}
			else {
				$key == 'Админка менеджера' ? $extraCssStyle = 'border: 1px solid darkgray; box-shadow: 1px 1px 0px #fff inset, -1px -1px 0px #fff inset;' : $extraCssStyle = '';
				$menuItems[] = array(
						'label' => $key,
						'url' => is_array($value) ? $value : array($this->createUrl($value)),
						'itemOptions' => array(
								'class' => 'btn w100',
								'style' => ''.$extraCssStyle
						),
				);
			}
		}

		return $menuItems;
	}
}