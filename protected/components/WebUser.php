<?php

/**
 * @property User $model
 */
class WebUser extends CWebUser {

	// Store model to not repeat query.
	private $_model;
	private $_cityId;
	
	public function init() {
		if(isset($_REQUEST['session'])) {
			$_COOKIE['session'] = $_REQUEST['session'];
		}
		if(isset($_REQUEST['sessionId'])) {
			$_COOKIE['session'] = $_REQUEST['sessionId'];
		}
		parent::init();

		$this->setStateKeyPrefix('user_');
	}

	public function getModel() {
		if (!$this->isGuest && $this->_model === null) {
			$this->_model = User::model()->findByPk($this->id);
		}
		return $this->_model;
	}
	
	public function getCityId() {
		if ($this->_cityId === null) {
			$this->_cityId = (new Search())->cityId;
		}
		return $this->_cityId;
	}
	
	public function getFullname() {
		if (!$this->isGuest && $this->_model === null) {
			$this->_model = User::model()->findByPk($this->id);
		}
		return $this->_model->name;
		#return $this->_model->surName." ".$this->_model->firstName." ".$this->_model->fatherName;
	}
    
    public function getShortname() {
        return (mb_strlen($this->fullname) > 25) ? mb_substr($this->fullname, 0, 22) . '...' : $this->fullname;
    }
}

?>