<?php

Yii::import('zii.widgets.CListView');

class ColumnListView extends CListView {
	
	/**
	 * @var string the HTML tag name for the container of all data item display. Defaults to 'div'.
	 * @since 1.1.4
	 */
	public $itemsTagName='table';
	
	/**
	 * @var string the URL of the CSS file used by this list view. Defaults to null, meaning using the integrated
	 * CSS file. If this is set false, you are responsible to explicitly include the necessary CSS file in your page.
	 */
	public $cssFile = false;

	public $columns = array("leftblock", "midblock", "rightblock");

	//@override)
	public function renderItems() {
		if ( $column_count = count($this->columns) ) {
			echo CHtml::openTag($this->itemsTagName,array('class'=>$this->itemsCssClass))."\n";
			echo CHtml::openTag('tr') . "\n";

			$data		 = $this->dataProvider->getData();
			if ( $data_count	 = count($data) ) {

				$data_per_column = (int) ceil($data_count / $column_count);

				$owner=$this->getOwner();
				$viewFile=$owner->getViewFile($this->itemView);

				foreach ($this->columns as $key => $column) {
					echo CHtml::openTag('td') . "\n";
					for ($i = 0; $i < $data_per_column; $i++) {
						$d			 = $this->viewData;
						$d['index']	 = $key*$data_per_column + $i;
						$d['data']	 = $data[$d['index']];
						$d['widget'] = $this;
						$owner->renderFile($viewFile,$d);
					}
					echo CHtml::closeTag('td');
				}
			}
			else
				$this->renderEmptyText();
			echo CHtml::closeTag('tr');
			echo CHtml::closeTag('table');
		}
	}

}